﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SognoLib.Core;
using SognoLib.Park.ParkCarIO;

namespace SognoLib.Park.LPR
{
    public class LPRManager
    {
        public static string[] BarStatusText = { "연결실패", "연결완료", "열림", "열림중", "닫힘", "닫힘중", "카운트", "고정", "고정해제", "초기화" };
    }
    public class SMsgLPRResult : SogNo_Message
    {
        static string msgID { get; set; } = "LPRResult";
        public int GateNo { get; set; } = 0;
        public ECarInOut CarInOut { get; set; } = ECarInOut.CarIn;
        public ECarOrder CarOrder { get; set; } = ECarOrder.Front;
        public string CarNumber { get; set; }
        public string ImagePath { get; set; }
        public DateTime? OccurTime { get; set; } = null;
        public Point[] Corners { get; set; } = null;

        public override byte[] Pack()
        {
            string location = "";
            if (this.Corners != null)
            {
                foreach (Point pt in this.Corners)
                {
                    if (location.Length > 0)
                        location += ",";

                    location += string.Format("{0},{1}", (int)pt.x, (int)pt.y);
                }
            }

            string msgBody = string.Format("{0}#{1}#{2}#{3}#{4}#{5}#{6}#{7}", msgID, this.GateNo, this.CarInOut, this.CarOrder, this.CarNumber, location, this.ImagePath, DateTimeToString(this.OccurTime));

            int bodyLen = Encoding.Default.GetBytes(msgBody).Length;

            string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, CharTextSign, bodyLen, msgBody, CharETX);

            return Encoding.Default.GetBytes(message);
        }

        public override void UnPack(string messageBody)
        {
            if (GetMsgID(messageBody) != msgID)
            {
                throw new Exception("메시지 ID가 일치하지 않음.");
            }

            string[] tokens = messageBody.Split(new char[1] { '#' });
            if (tokens.Length != 8)
            {
                throw new Exception("토큰의 갯수가 일치하지 않음.");
            }

            try
            {
                this.GateNo = Int32.Parse(tokens[1]);
                this.CarInOut = (ECarInOut)Enum.Parse(typeof(ECarInOut), tokens[2]);
                this.CarOrder = (ECarOrder)Enum.Parse(typeof(ECarOrder), tokens[3]);
                this.CarNumber = tokens[4];
                string location = tokens[5];
                this.ImagePath = tokens[6];
                this.OccurTime = StringToDateTime(tokens[7]);

                if (location.Length > 0)
                {
                    string[] locTokens = location.Split(new char[1] { ',' });
                    if (locTokens.Length != 8)
                    {
                        throw new Exception("번호판의 네 모서리 점 정보가 부적절함.");
                    }

                    this.Corners = new Point[4];
                    for (int i = 0; i < locTokens.Length; i += 2)
                    {
                        Point pt;
                        pt.x = int.Parse(locTokens[i]);
                        pt.y = int.Parse(locTokens[i + 1]);

                        this.Corners[i / 2] = pt;
                    }
                }
            }
            catch (Exception)
            {
                throw new Exception("메시지 형식이 일치하지 않습니다.");
            }
        }
    }
    public enum EBarStatus : int
    {
        NotConnected = 0,
        Connected = 1,
        BarOpen = 2,
        BarOpenAct = 3,
        BarClose = 4,
        BarCloseAct = 5,
        BarCount = 6,
        UpLock = 7,
        UnLock = 8,
        BarInit = 9
    }
    public class SMsgBarCtrl : SogNo_Message
    {
        static string msgID { get; set; } = "BarCtrl";
        public int GateNo { get; set; } = 0;
        public EMsgType eMsgType { get; set; } = EMsgType.Command;
        public ECarInOut ECarInOut { get; set; } = ECarInOut.CarIn;
        public EBarStatus eBarStatus { get; set; } = EBarStatus.NotConnected;

        public override byte[] Pack()
        {
            string msgBody = string.Format("{0}#{1}#{2}#{3}#{4}", msgID, this.eMsgType, this.GateNo, this.ECarInOut, this.eBarStatus);

            int bodyLen = Encoding.Default.GetBytes(msgBody).Length;

            string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, CharTextSign, bodyLen, msgBody, CharETX);

            return Encoding.Default.GetBytes(message);
        }

        public override void UnPack(string messageBody)
        {
            if (GetMsgID(messageBody) != msgID)
            {
                throw new Exception("메시지 ID가 일치하지 않음.");
            }

            string[] tokens = messageBody.Split(new char[1] { '#' });
            if (tokens.Length != 5)
            {
                throw new Exception("토큰의 갯수가 일치하지 않음.");
            }

            try
            {
                this.eMsgType = (EMsgType)Enum.Parse(typeof(EMsgType), tokens[1]);
                this.GateNo = Int32.Parse(tokens[2]);
                this.ECarInOut = (ECarInOut)Enum.Parse(typeof(ECarInOut), tokens[3]);
                this.eBarStatus = (EBarStatus)Enum.Parse(typeof(EBarStatus), tokens[4]);
            }
            catch (Exception)
            {
                throw new Exception("메시지 형식이 일치하지 않습니다.");
            }
        }
    }
    public enum ESCRMsgType : int
    {
        Normal = 0,
        Emergency = 1
    }
    public class SMsgScrCtrl : SogNo_Message
    {
        static string msgID { get; set; } = "ScrCtrl";
        public int GateNo { get; set; } = 0;
        public ECarInOut ECarInOut { get; set; } = ECarInOut.CarIn;

        public ESCRMsgType MsgType { get; set; } = ESCRMsgType.Emergency;
        public string UText { get; set; } = "";
        public int UColor;
        public string DText { get; set; } = "";
        public int DColor;

        public override byte[] Pack()
        {
            string msgBody = string.Format("{0}#{1}#{2}#{3}#{4}#{5}#{6}", msgID, this.GateNo, this.MsgType, this.UText, this.UColor, this.DText, this.DColor);

            int bodyLen = Encoding.Default.GetBytes(msgBody).Length;

            string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, CharTextSign, bodyLen, msgBody, CharETX);

            return Encoding.Default.GetBytes(message);
        }

        public override void UnPack(string messageBody)
        {
            if (GetMsgID(messageBody) != msgID)
            {
                throw new Exception("메시지 ID가 일치하지 않음.");
            }

            string[] tokens = messageBody.Split(new char[1] { '#' });
            if (tokens.Length != 3)
            {
                throw new Exception("토큰의 갯수가 일치하지 않음.");
            }

            try
            {
                this.GateNo = Int32.Parse(tokens[1]);
                this.MsgType = (ESCRMsgType)Enum.Parse(typeof(ESCRMsgType), tokens[2]);
                this.UText = tokens[3];
                this.UColor = Int32.Parse(tokens[4]);
                this.DText = tokens[5];
                this.DColor = Int32.Parse(tokens[6]);
            }
            catch (Exception)
            {
                throw new Exception("메시지 형식이 일치하지 않습니다.");
            }
        }
    }
}
