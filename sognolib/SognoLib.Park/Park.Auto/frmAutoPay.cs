﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SognoLib.Park.ParkCarIO;

namespace SognoLib.Park.Park.Auto
{
    public partial class frmAutoPay : Form
    {
        SMsgParkingPayPopup sMsgParkingPayPopup;
        public frmAutoPay()
        {
            InitializeComponent();
        }

        public void SetParkingPayData(SMsgParkingPayPopup sMsgParkingPayPopup)
        {
            this.sMsgParkingPayPopup = sMsgParkingPayPopup;

            this.picOutCarImg.ImageLocation = sMsgParkingPayPopup.ImgPath;
            this.lblParkingFee.Text = sMsgParkingPayPopup.ParkingFee.ToString();
            this.lblDCFee.Text = sMsgParkingPayPopup.DCFee.ToString();
            this.lblRealFee.Text = (sMsgParkingPayPopup.ParkingFee - sMsgParkingPayPopup.DCFee).ToString();

            this.lblParkingTime.Text = sMsgParkingPayPopup.ParkingTime.ToString();
            this.lblOutCarNum.Text = sMsgParkingPayPopup.CarNum;

            if (!this.Visible)
                this.ShowDialog();
        }
    }
}
