﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SognoLib.Park.ParkCarIO;

namespace SognoLib.Park.Park.Auto
{
    public partial class frmAutoCarSearch : Form
    {
        public delegate void evtInCarSearch(SMsgInCarSearchPopup sMsgInCarSearchPopup);
        public event evtInCarSearch AcceptEvent;

        int ParkNo = 0; 
        CarIODBManager carIODBManager;
        public frmAutoCarSearch(int ParkNo, CarIODBManager carIODBManager)
        {
            InitializeComponent();

            this.ParkNo = ParkNo;
            this.carIODBManager = carIODBManager;
        }

        private void btnNumberClick(object sender, EventArgs e)
        {
            Button tmpBtn = (Button)sender;

            if (lblSearchStr.Text.Length > 3) return;

            lblSearchStr.Text += tmpBtn.Tag.ToString();
        }

        private void btnInit_Click(object sender, EventArgs e)
        {
            lblSearchStr.Text = "";
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            /// 리스트 초기화
            dgvInCar.Rows.Clear(); picInCar.ImageLocation = "";

            /// 입차 중인 차량 검색
            CarIODataRecord[] carIODataRecords = carIODBManager.GetNoOutCarLikeData(ParkNo, lblSearchStr.Text);
            for (int i = 0; i < carIODataRecords.Length; i++)
            {
                DataGridViewRow NewRow = (DataGridViewRow)dgvInCar.Rows[0].Clone();

                NewRow.Cells[0].Value = carIODataRecords[i].TKNo;
                NewRow.Cells[1].Value = carIODataRecords[i].InImage1;
                NewRow.Cells[2].Value = carIODataRecords[i].InCarNum1;
                NewRow.Cells[3].Value = carIODataRecords[i].InDate;

                dgvInCar.Rows.Add(NewRow);

                if (i == 0) picInCar.ImageLocation = carIODataRecords[i].InImage1;
            }
        }

        private void dgvInCar_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DataGridViewRow CarInRow = dgvInCar.SelectedRows[0];
                picInCar.ImageLocation = CarInRow.Cells[1].Value.ToString() == "" ? "" : CarInRow.Cells[1].Value.ToString();
            }
            catch { }
        }

        private void acceptButton_Click(object sender, EventArgs e)
        {
            SMsgInCarSearchPopup sMsgInCarSearchPopup = new SMsgInCarSearchPopup();
            sMsgInCarSearchPopup.eInCarSearchPopupType = EInCarSearchPopupType.Search;

            try
            {
                DataGridViewRow CarInRow = dgvInCar.SelectedRows[0];
                sMsgInCarSearchPopup.CarNum = CarInRow.Cells[2].Value.ToString();

                if (sMsgInCarSearchPopup.CarNum != "")
                {
                    AcceptEvent?.Invoke(sMsgInCarSearchPopup);
                }
            }
            catch
            {
                MessageBox.Show("차량을 선택해 주십시오.");
            }
        }
    }
}
