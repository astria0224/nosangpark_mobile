﻿namespace SognoLib.Park.ParkCarIO
{
    partial class frmInCarSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btn30m = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.txtParkingTime = new System.Windows.Forms.TextBox();
            this.btnCreate = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.btn120m = new System.Windows.Forms.Button();
            this.btn60m = new System.Windows.Forms.Button();
            this.btn15m = new System.Windows.Forms.Button();
            this.lblGateName = new System.Windows.Forms.Label();
            this.layout_outcar = new System.Windows.Forms.TableLayoutPanel();
            this.layout_search = new System.Windows.Forms.TableLayoutPanel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSearchCarNum = new System.Windows.Forms.TextBox();
            this.layout_outcar_inner = new System.Windows.Forms.TableLayoutPanel();
            this.lblOutDate = new System.Windows.Forms.Label();
            this.layout_main = new System.Windows.Forms.TableLayoutPanel();
            this.layout_incar = new System.Windows.Forms.TableLayoutPanel();
            this.dgvInCar = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label_incar_title = new System.Windows.Forms.Label();
            this.layout_buttons = new System.Windows.Forms.TableLayoutPanel();
            this.btnAccept = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.layout_incar_matched = new System.Windows.Forms.TableLayoutPanel();
            this.picOutCar = new System.Windows.Forms.PictureBox();
            this.picInCar = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.layout_outcar.SuspendLayout();
            this.layout_search.SuspendLayout();
            this.layout_outcar_inner.SuspendLayout();
            this.layout_main.SuspendLayout();
            this.layout_incar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInCar)).BeginInit();
            this.layout_buttons.SuspendLayout();
            this.layout_incar_matched.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picOutCar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picInCar)).BeginInit();
            this.SuspendLayout();
            // 
            // btn30m
            // 
            this.btn30m.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btn30m.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn30m.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn30m.Location = new System.Drawing.Point(4, 53);
            this.btn30m.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btn30m.Name = "btn30m";
            this.btn30m.Size = new System.Drawing.Size(223, 38);
            this.btn30m.TabIndex = 16;
            this.btn30m.TabStop = false;
            this.btn30m.Text = "30 분";
            this.btn30m.UseVisualStyleBackColor = true;
            this.btn30m.Click += new System.EventHandler(this.btn30m_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 6);
            this.tableLayoutPanel3.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.groupBox1, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.groupBox2, 0, 2);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(950, 14);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel3.RowCount = 7;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(253, 622);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.btnCancel, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(11, 551);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(8, 3, 8, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(231, 64);
            this.tableLayoutPanel4.TabIndex = 4;
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(77)))), ((int)(((byte)(54)))));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(6, 6);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(6);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(219, 52);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.TabStop = false;
            this.btnCancel.Text = "취 소";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.Color.LightGray;
            this.label4.Location = new System.Drawing.Point(7, 4);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(239, 35);
            this.label4.TabIndex = 3;
            this.label4.Text = "수동 출차";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel5);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Location = new System.Drawing.Point(7, 323);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(239, 190);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "직접 입력";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtParkingTime, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.btnCreate, 0, 2);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(4, 31);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(10, 14, 10, 14);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 3;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(231, 154);
            this.tableLayoutPanel5.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label3.Location = new System.Drawing.Point(4, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(223, 35);
            this.label3.TabIndex = 6;
            this.label3.Text = "주차 시간 (분) : ";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtParkingTime
            // 
            this.txtParkingTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtParkingTime.Font = new System.Drawing.Font("맑은 고딕", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtParkingTime.Location = new System.Drawing.Point(4, 40);
            this.txtParkingTime.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtParkingTime.Name = "txtParkingTime";
            this.txtParkingTime.Size = new System.Drawing.Size(223, 43);
            this.txtParkingTime.TabIndex = 5;
            this.txtParkingTime.TabStop = false;
            this.txtParkingTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnCreate
            // 
            this.btnCreate.BackColor = System.Drawing.Color.DarkOrange;
            this.btnCreate.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnCreate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCreate.FlatAppearance.BorderSize = 0;
            this.btnCreate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreate.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCreate.ForeColor = System.Drawing.Color.White;
            this.btnCreate.Location = new System.Drawing.Point(8, 95);
            this.btnCreate.Margin = new System.Windows.Forms.Padding(8, 10, 8, 10);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(215, 49);
            this.btnCreate.TabIndex = 4;
            this.btnCreate.TabStop = false;
            this.btnCreate.Text = "생 성";
            this.btnCreate.UseVisualStyleBackColor = false;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel6);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(7, 64);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(239, 219);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.btn120m, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this.btn60m, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.btn30m, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.btn15m, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(4, 19);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 4;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(231, 195);
            this.tableLayoutPanel6.TabIndex = 0;
            // 
            // btn120m
            // 
            this.btn120m.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btn120m.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn120m.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn120m.Location = new System.Drawing.Point(4, 149);
            this.btn120m.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btn120m.Name = "btn120m";
            this.btn120m.Size = new System.Drawing.Size(223, 41);
            this.btn120m.TabIndex = 18;
            this.btn120m.TabStop = false;
            this.btn120m.Text = "2 시간";
            this.btn120m.UseVisualStyleBackColor = true;
            this.btn120m.Click += new System.EventHandler(this.btn120m_Click);
            // 
            // btn60m
            // 
            this.btn60m.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btn60m.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn60m.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn60m.Location = new System.Drawing.Point(4, 101);
            this.btn60m.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btn60m.Name = "btn60m";
            this.btn60m.Size = new System.Drawing.Size(223, 38);
            this.btn60m.TabIndex = 17;
            this.btn60m.TabStop = false;
            this.btn60m.Text = "1 시간";
            this.btn60m.UseVisualStyleBackColor = true;
            this.btn60m.Click += new System.EventHandler(this.btn60m_Click);
            // 
            // btn15m
            // 
            this.btn15m.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btn15m.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn15m.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn15m.Location = new System.Drawing.Point(4, 5);
            this.btn15m.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btn15m.Name = "btn15m";
            this.btn15m.Size = new System.Drawing.Size(223, 38);
            this.btn15m.TabIndex = 15;
            this.btn15m.TabStop = false;
            this.btn15m.Text = "15 분";
            this.btn15m.UseVisualStyleBackColor = true;
            this.btn15m.Click += new System.EventHandler(this.btn15m_Click);
            // 
            // lblGateName
            // 
            this.lblGateName.AutoSize = true;
            this.lblGateName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGateName.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblGateName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(133)))), ((int)(((byte)(0)))));
            this.lblGateName.Location = new System.Drawing.Point(7, 4);
            this.lblGateName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGateName.Name = "lblGateName";
            this.lblGateName.Size = new System.Drawing.Size(542, 35);
            this.lblGateName.TabIndex = 1;
            this.lblGateName.Text = "출차 차량";
            this.lblGateName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // layout_outcar
            // 
            this.layout_outcar.ColumnCount = 1;
            this.layout_outcar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout_outcar.Controls.Add(this.lblGateName, 0, 0);
            this.layout_outcar.Controls.Add(this.layout_search, 0, 3);
            this.layout_outcar.Controls.Add(this.layout_outcar_inner, 0, 1);
            this.layout_outcar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layout_outcar.Location = new System.Drawing.Point(10, 14);
            this.layout_outcar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.layout_outcar.Name = "layout_outcar";
            this.layout_outcar.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layout_outcar.RowCount = 4;
            this.layout_outcar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.layout_outcar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout_outcar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.layout_outcar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.layout_outcar.Size = new System.Drawing.Size(556, 622);
            this.layout_outcar.TabIndex = 0;
            // 
            // layout_search
            // 
            this.layout_search.BackColor = System.Drawing.Color.WhiteSmoke;
            this.layout_search.ColumnCount = 3;
            this.layout_search.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.layout_search.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout_search.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.layout_search.Controls.Add(this.btnSearch, 2, 0);
            this.layout_search.Controls.Add(this.label1, 0, 0);
            this.layout_search.Controls.Add(this.txtSearchCarNum, 1, 0);
            this.layout_search.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layout_search.Location = new System.Drawing.Point(6, 551);
            this.layout_search.Name = "layout_search";
            this.layout_search.RowCount = 1;
            this.layout_search.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout_search.Size = new System.Drawing.Size(544, 64);
            this.layout_search.TabIndex = 3;
            // 
            // btnSearch
            // 
            this.btnSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnSearch.Location = new System.Drawing.Point(390, 6);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(6);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(148, 52);
            this.btnSearch.TabIndex = 1;
            this.btnSearch.TabStop = false;
            this.btnSearch.Text = "조 회";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(4, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 64);
            this.label1.TabIndex = 1;
            this.label1.Text = "차량번호 :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtSearchCarNum
            // 
            this.txtSearchCarNum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSearchCarNum.Font = new System.Drawing.Font("맑은 고딕", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtSearchCarNum.Location = new System.Drawing.Point(143, 7);
            this.txtSearchCarNum.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.txtSearchCarNum.Name = "txtSearchCarNum";
            this.txtSearchCarNum.Size = new System.Drawing.Size(238, 50);
            this.txtSearchCarNum.TabIndex = 0;
            this.txtSearchCarNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // layout_outcar_inner
            // 
            this.layout_outcar_inner.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.layout_outcar_inner.ColumnCount = 1;
            this.layout_outcar_inner.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout_outcar_inner.Controls.Add(this.picOutCar, 0, 0);
            this.layout_outcar_inner.Controls.Add(this.lblOutDate, 0, 1);
            this.layout_outcar_inner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layout_outcar_inner.Location = new System.Drawing.Point(7, 44);
            this.layout_outcar_inner.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.layout_outcar_inner.Name = "layout_outcar_inner";
            this.layout_outcar_inner.RowCount = 2;
            this.layout_outcar_inner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout_outcar_inner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.layout_outcar_inner.Size = new System.Drawing.Size(542, 489);
            this.layout_outcar_inner.TabIndex = 4;
            // 
            // lblOutDate
            // 
            this.lblOutDate.AutoSize = true;
            this.lblOutDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblOutDate.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblOutDate.ForeColor = System.Drawing.Color.White;
            this.lblOutDate.Location = new System.Drawing.Point(5, 453);
            this.lblOutDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOutDate.Name = "lblOutDate";
            this.lblOutDate.Size = new System.Drawing.Size(532, 35);
            this.lblOutDate.TabIndex = 6;
            this.lblOutDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // layout_main
            // 
            this.layout_main.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.layout_main.ColumnCount = 3;
            this.layout_main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.layout_main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.layout_main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 261F));
            this.layout_main.Controls.Add(this.layout_outcar, 0, 0);
            this.layout_main.Controls.Add(this.layout_incar, 1, 0);
            this.layout_main.Controls.Add(this.tableLayoutPanel3, 2, 0);
            this.layout_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layout_main.Location = new System.Drawing.Point(0, 0);
            this.layout_main.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.layout_main.Name = "layout_main";
            this.layout_main.Padding = new System.Windows.Forms.Padding(6, 9, 6, 9);
            this.layout_main.RowCount = 1;
            this.layout_main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout_main.Size = new System.Drawing.Size(1213, 650);
            this.layout_main.TabIndex = 1;
            // 
            // layout_incar
            // 
            this.layout_incar.BackColor = System.Drawing.Color.Transparent;
            this.layout_incar.ColumnCount = 1;
            this.layout_incar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout_incar.Controls.Add(this.dgvInCar, 0, 3);
            this.layout_incar.Controls.Add(this.label_incar_title, 0, 0);
            this.layout_incar.Controls.Add(this.layout_buttons, 0, 5);
            this.layout_incar.Controls.Add(this.label2, 0, 2);
            this.layout_incar.Controls.Add(this.layout_incar_matched, 0, 1);
            this.layout_incar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layout_incar.Location = new System.Drawing.Point(574, 14);
            this.layout_incar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.layout_incar.Name = "layout_incar";
            this.layout_incar.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layout_incar.RowCount = 6;
            this.layout_incar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.layout_incar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout_incar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.layout_incar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 190F));
            this.layout_incar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.layout_incar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.layout_incar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.layout_incar.Size = new System.Drawing.Size(368, 622);
            this.layout_incar.TabIndex = 1;
            // 
            // dgvInCar
            // 
            this.dgvInCar.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.dgvInCar.AllowUserToResizeColumns = false;
            this.dgvInCar.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgvInCar.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvInCar.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgvInCar.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvInCar.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvInCar.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvInCar.ColumnHeadersHeight = 30;
            this.dgvInCar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvInCar.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.Column1,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn3,
            this.Column9,
            this.Column8});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.ControlDarkDark;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvInCar.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvInCar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvInCar.EnableHeadersVisualStyles = false;
            this.dgvInCar.GridColor = System.Drawing.SystemColors.ScrollBar;
            this.dgvInCar.Location = new System.Drawing.Point(6, 352);
            this.dgvInCar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dgvInCar.MultiSelect = false;
            this.dgvInCar.Name = "dgvInCar";
            this.dgvInCar.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvInCar.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvInCar.RowHeadersVisible = false;
            this.dgvInCar.RowHeadersWidth = 50;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvInCar.RowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvInCar.RowTemplate.Height = 28;
            this.dgvInCar.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvInCar.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvInCar.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvInCar.Size = new System.Drawing.Size(356, 182);
            this.dgvInCar.TabIndex = 186;
            this.dgvInCar.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInCar_CellClick);
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn2.HeaderText = "TKNo";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Visible = false;
            this.dataGridViewTextBoxColumn2.Width = 160;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "이미지경로";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column1.Visible = false;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn4.FillWeight = 80F;
            this.dataGridViewTextBoxColumn4.HeaderText = "차량번호";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn3.FillWeight = 170F;
            this.dataGridViewTextBoxColumn3.HeaderText = "입차시간";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Column9
            // 
            this.Column9.HeaderText = "후방이미지경로";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Visible = false;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "후방번호";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Visible = false;
            // 
            // label_incar_title
            // 
            this.label_incar_title.AutoSize = true;
            this.label_incar_title.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_incar_title.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label_incar_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(236)))), ((int)(((byte)(219)))));
            this.label_incar_title.Location = new System.Drawing.Point(7, 4);
            this.label_incar_title.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_incar_title.Name = "label_incar_title";
            this.label_incar_title.Size = new System.Drawing.Size(354, 35);
            this.label_incar_title.TabIndex = 2;
            this.label_incar_title.Text = "입차 기록";
            this.label_incar_title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // layout_buttons
            // 
            this.layout_buttons.BackColor = System.Drawing.Color.WhiteSmoke;
            this.layout_buttons.ColumnCount = 1;
            this.layout_buttons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout_buttons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.layout_buttons.Controls.Add(this.btnAccept, 0, 0);
            this.layout_buttons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layout_buttons.Location = new System.Drawing.Point(11, 551);
            this.layout_buttons.Margin = new System.Windows.Forms.Padding(8, 3, 8, 3);
            this.layout_buttons.Name = "layout_buttons";
            this.layout_buttons.RowCount = 1;
            this.layout_buttons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout_buttons.Size = new System.Drawing.Size(346, 64);
            this.layout_buttons.TabIndex = 0;
            // 
            // btnAccept
            // 
            this.btnAccept.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(134)))), ((int)(((byte)(128)))));
            this.btnAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAccept.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAccept.FlatAppearance.BorderSize = 0;
            this.btnAccept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAccept.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAccept.ForeColor = System.Drawing.Color.White;
            this.btnAccept.Location = new System.Drawing.Point(6, 6);
            this.btnAccept.Margin = new System.Windows.Forms.Padding(6);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(334, 52);
            this.btnAccept.TabIndex = 1;
            this.btnAccept.TabStop = false;
            this.btnAccept.Text = "선 택";
            this.btnAccept.UseVisualStyleBackColor = false;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(236)))), ((int)(((byte)(219)))));
            this.label2.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label2.Location = new System.Drawing.Point(7, 313);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(354, 35);
            this.label2.TabIndex = 5;
            this.label2.Text = "조회 결과";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // layout_incar_matched
            // 
            this.layout_incar_matched.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.layout_incar_matched.ColumnCount = 1;
            this.layout_incar_matched.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout_incar_matched.Controls.Add(this.picInCar, 0, 0);
            this.layout_incar_matched.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layout_incar_matched.Location = new System.Drawing.Point(7, 44);
            this.layout_incar_matched.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.layout_incar_matched.Name = "layout_incar_matched";
            this.layout_incar_matched.RowCount = 1;
            this.layout_incar_matched.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout_incar_matched.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 263F));
            this.layout_incar_matched.Size = new System.Drawing.Size(354, 264);
            this.layout_incar_matched.TabIndex = 6;
            // 
            // picOutCar
            // 
            this.picOutCar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picOutCar.Location = new System.Drawing.Point(5, 6);
            this.picOutCar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.picOutCar.Name = "picOutCar";
            this.picOutCar.Size = new System.Drawing.Size(532, 441);
            this.picOutCar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picOutCar.TabIndex = 5;
            this.picOutCar.TabStop = false;
            // 
            // picInCar
            // 
            this.picInCar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picInCar.Location = new System.Drawing.Point(5, 6);
            this.picInCar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.picInCar.Name = "picInCar";
            this.picInCar.Size = new System.Drawing.Size(344, 252);
            this.picInCar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picInCar.TabIndex = 7;
            this.picInCar.TabStop = false;
            // 
            // frmInCarSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1213, 650);
            this.Controls.Add(this.layout_main);
            this.Name = "frmInCarSearch";
            this.Text = "frmInCarSearch";
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.layout_outcar.ResumeLayout(false);
            this.layout_outcar.PerformLayout();
            this.layout_search.ResumeLayout(false);
            this.layout_search.PerformLayout();
            this.layout_outcar_inner.ResumeLayout(false);
            this.layout_outcar_inner.PerformLayout();
            this.layout_main.ResumeLayout(false);
            this.layout_incar.ResumeLayout(false);
            this.layout_incar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInCar)).EndInit();
            this.layout_buttons.ResumeLayout(false);
            this.layout_incar_matched.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picOutCar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picInCar)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn30m;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtParkingTime;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Button btn120m;
        private System.Windows.Forms.Button btn60m;
        private System.Windows.Forms.Button btn15m;
        private System.Windows.Forms.Label lblGateName;
        private System.Windows.Forms.TableLayoutPanel layout_outcar;
        private System.Windows.Forms.TableLayoutPanel layout_search;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSearchCarNum;
        private System.Windows.Forms.TableLayoutPanel layout_outcar_inner;
        private System.Windows.Forms.PictureBox picOutCar;
        private System.Windows.Forms.Label lblOutDate;
        private System.Windows.Forms.TableLayoutPanel layout_main;
        private System.Windows.Forms.TableLayoutPanel layout_incar;
        private System.Windows.Forms.Label label_incar_title;
        private System.Windows.Forms.TableLayoutPanel layout_buttons;
        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel layout_incar_matched;
        private System.Windows.Forms.DataGridView dgvInCar;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.PictureBox picInCar;
    }
}