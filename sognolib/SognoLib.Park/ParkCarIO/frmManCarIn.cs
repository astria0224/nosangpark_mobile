﻿using System;
using System.Windows.Forms;
using SognoLib.Park.LPR;

namespace SognoLib.Park.ParkCarIO
{
    public partial class frmManCarIn : Form
    {
        public delegate void evtManCarIn(SMsgLPRResult sMsgLPRResult);
        public event evtManCarIn ManCarInEvent;

        public frmManCarIn()
        {
            InitializeComponent();

            for (int i = 0; i < ParkCarIOManager.CarTypeText.Length; i++)
            {
                cbCarType.Items.Add(ParkCarIOManager.CarTypeText[i]);
            }
            if (ParkCarIOManager.CarTypeText.Length >= 2) cbCarType.SelectedIndex = 1;
        }

        private void btnManCarIn_Click(object sender, System.EventArgs e)
        {
            SMsgLPRResult sMsgLPRResult = new SMsgLPRResult();

            if(txtCarNum.Text == "")
            {
                MessageBox.Show("차량번호를 입력해 주십시오."); return;
            }
            /// !!!!! 차량번호형식확인 !!!!! ///
            sMsgLPRResult.CarNumber = txtCarNum.Text;
            sMsgLPRResult.GateNo = 999;
            sMsgLPRResult.CarInOut = cbCarType.SelectedIndex == 0 ? ECarInOut.LightCarIn : ECarInOut.CarIn;
            sMsgLPRResult.OccurTime = dtpCarIn.Value; 

            ManCarInEvent?.Invoke(sMsgLPRResult);

            txtCarNum.Text = ""; dtpCarIn.Value = DateTime.Now; this.Hide();
        }
    }
}
