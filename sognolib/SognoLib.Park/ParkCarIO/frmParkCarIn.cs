﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SognoLib.Park.ParkCarIO
{
    public partial class frmParkCarIn : Form
    {
        SMsgCarInPopup msgCarInPopup = new SMsgCarInPopup();

        public delegate void evtCarInAccept(SMsgCarInPopup msgCarInPopup);
        public event evtCarInAccept AcceptEvent;

        public frmParkCarIn(string GateName)
        {
            InitializeComponent();
            this.lblGateName.Text = GateName;
        }

        public void SetParkCarInData(SMsgCarInPopup msgCarInPopup)
        {
            this.msgCarInPopup.GateNo = msgCarInPopup.GateNo;
            this.picCarIn.ImageLocation = msgCarInPopup.ImgPath;
            this.txtCarNum.Text = msgCarInPopup.CarNum;

            if (!this.Visible)
                this.ShowDialog();
        }

        private void btnCarInAccept_Click(object sender, EventArgs e)
        {
            msgCarInPopup.eCarInPopupType = ECarInPopupType.Result;
            msgCarInPopup.CarNum = txtCarNum.Text;
            msgCarInPopup.Memo = txtMemo.Text;
            msgCarInPopup.PrintVisit = chkPrintVisit.Checked;

            AcceptEvent?.Invoke(msgCarInPopup);
        }
        private void btnCarInDeny_Click(object sender, EventArgs e)
        {
            msgCarInPopup.eCarInPopupType = ECarInPopupType.Close;
            AcceptEvent?.Invoke(msgCarInPopup);
        }
    }
}
