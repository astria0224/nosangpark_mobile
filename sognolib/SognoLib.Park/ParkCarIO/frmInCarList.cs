﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SognoLib.Park.ParkCarIO;

namespace SognoLib.Park.ParkCarIO
{
    public partial class frmInCarList : Form
    {
        public delegate void evtInCarList(int EvtType, List<CarIODataRecord> carIODataRecords);
        public event evtInCarList InCarLIstEvt;

        int ParkNo = 0; CarIODBManager carIODBManager;
        private CarIODataRecord[] carIODataRecords;
        public frmInCarList(int ParkNo, CarIODBManager carIODBManager)
        {
            InitializeComponent();

            this.ParkNo = ParkNo;
            this.carIODBManager = carIODBManager;

            /// 데이터 불러오기
            LoadData();
        }

        private void LoadData()
        {
            dgvInCar.Rows.Clear();

            /// 미출차리스트 업데이트
            carIODataRecords = carIODBManager.GetNoOutCarData(0, "");

            for (int i = 0; i < carIODataRecords.Length; i++)
            {
                ///
                DataGridViewRow NewRow = dgvInCar.Rows[dgvInCar.Rows.Add()];
                NewRow.Height = 30;
                NewRow.Cells[0].Value = carIODataRecords[i].TKNo;
                NewRow.Cells[2].Value = (carIODataRecords[i].GroupName == null) ? "방문차량" : carIODataRecords[i].GroupName;
                NewRow.Cells[3].Value = carIODataRecords[i].InCarNum1;
                NewRow.Cells[4].Value = carIODataRecords[i].InDate?.ToString("yyyy-MM-dd HH:mm:ss");
            }
        }

        private void evtInCarListProcess(object sender, EventArgs e)
        {
            int EvtType = Convert.ToInt32(((Button)sender).Tag);

            List<CarIODataRecord> ListcarIODataRecords = new List<CarIODataRecord>();
            if (EvtType == 2)
            {
                ListcarIODataRecords.Add(carIODataRecords[dgvInCar.CurrentCell.RowIndex]);
            }
            else
            {
                for (int i = 0; i < dgvInCar.Rows.Count; i++)
                {
                    if (Convert.ToBoolean(dgvInCar.Rows[i].Cells[1].Value))
                        ListcarIODataRecords.Add(carIODataRecords[i]);
                }
            }

            InCarLIstEvt?.Invoke(EvtType, ListcarIODataRecords);

            if (EvtType == 0) this.Hide();
        }
    }
}
