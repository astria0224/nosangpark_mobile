﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SognoLib.Core;
using SognoLib.Park.Toll;
using SognoLib.Park.LPR;
using SognoLib.Customer.Core;

using System.Windows.Forms;

namespace SognoLib.Park.ParkCarIO
{
    public class ParkCarIOManager
    {
        public static string CreateTKNo(int GateNo, DateTime datetime)
        {
            return string.Format("G{0:D3}{1}", GateNo, datetime.ToString("yyyyMMddHHmmssf"));
        }
        public static string[] CarInTypeText = { "입차거부", "자동입차", "입차허용", "자리비움", "입차누락" };
        public static string[] CarOutTypeText = { "자동출차", "수동출차", "자리비움", "출차누락" };
        public static string[] CarTypeText = { "경차", "일반" };
        public static string[] PayTypeText = { "회차", "할인", "현금", "카드" };
    }
    public enum EMsgType : int
    {
        Command = 0,
        Notice = 1
    }
    public enum EGateInType : int
    {
        Always = 0,
        NormalOnly = 1,
        NormalDayDrive = 2,
        CustOnly = 3,
        CustDayDrive = 4,
        DayDrive = 5,
        NoCarIn = 6
    }
    public enum EGateOutType : int
    {
        Always = 0,
        Pay = 1
    }
    public enum ECarInOut : int
    {
        CarIn = 0,
        LightCarIn = 1,
        CarOut = 2
    }
    public enum ECarOrder : int
    {
        Front = 0,
        Rear = 1
    }
    public enum EIOProcType : int
    {
        InCar = 0,
        InCarRear = 1,
        OutCar = 2,
        OutCarNoData = 3,
        Done = 4
    }
    public enum ECarInPopupType : int
    {
        Popup = 0,
        Result = 1,
        Update = 2,
        Close = 3
    }
    public enum EParkingPayPopup : int
    {
        Popup = 0,
        DC = 1,
        Pay = 2,
        Close = 3
    }
    public enum EPayType : int
    {
        Turn = 0,
        DC = 1,
        Cash = 2,
        Card = 3
    }
    public enum ECarIOType : int
    {
        CarInInsert = 0,
        CarInUpdate = 1,
        CarOutInsert = 2
    }
    public enum EInCarSearchPopupType : int
    {
        Popup = 0,
        Search = 1,
        SetTime = 2,
        Close = 3
    }
    public class SMsgCarInPopup : SogNo_Message
    {
        static string msgID { get; set; } = "CarInPopup";
        public ECarInPopupType eCarInPopupType { get; set; } = ECarInPopupType.Popup;
        public int GateNo { get; set; } = 0;
        public string CarNum { get; set; }
        public string ImgPath { get; set; }
        public string Memo { get; set; }
        public bool PrintVisit { get; set; }

        public override byte[] Pack()
        {
            string msgBody = string.Format("{0}#{1}#{2}#{3}#{4}#{5}#{6}", msgID, eCarInPopupType, this.GateNo, this.CarNum, this.ImgPath, this.Memo, this.PrintVisit);

            int bodyLen = Encoding.Default.GetBytes(msgBody).Length;

            string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, CharTextSign, bodyLen, msgBody, CharETX);
            return Encoding.Default.GetBytes(message);
        }
        public override void UnPack(string messageBody)
        {
            if (GetMsgID(messageBody) != msgID)
            {
                throw new Exception("메시지 ID가 일치하지 않음.");
            }

            string[] tokens = messageBody.Split(new char[1] { '#' });
            if (tokens.Length != 7)
            {
                throw new Exception("토큰의 갯수가 일치하지 않음.");
            }

            try
            {
                this.eCarInPopupType = (ECarInPopupType)Enum.Parse(typeof(ECarInPopupType), tokens[1]);
                this.GateNo = Int32.Parse(tokens[2]); this.CarNum = tokens[3];
                this.ImgPath = tokens[4]; this.Memo = tokens[5];
                this.PrintVisit = Boolean.Parse(tokens[6]);
            }
            catch (Exception)
            {
                throw new Exception("메시지 형식이 일치하지 않습니다.");
            }
        }
    }
    public class SMsgInCarSearchPopup : SogNo_Message
    {
        static string msgID { get; set; } = "InCarSearchPopup";
        public EInCarSearchPopupType eInCarSearchPopupType { get; set; } = EInCarSearchPopupType.Popup;
        public int GateNo { get; set; } = 0;
        public string CarNum { get; set; }
        public string ImgPath { get; set; }
        public int Time { get; set; }

        public override byte[] Pack()
        {
            string msgBody = string.Format("{0}#{1}#{2}#{3}#{4}#{5}", msgID, eInCarSearchPopupType, this.GateNo, this.CarNum, this.ImgPath, this.Time);

            int bodyLen = Encoding.Default.GetBytes(msgBody).Length;

            string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, CharTextSign, bodyLen, msgBody, CharETX);
            return Encoding.Default.GetBytes(message);
        }
        public override void UnPack(string messageBody)
        {
            if (GetMsgID(messageBody) != msgID)
            {
                throw new Exception("메시지 ID가 일치하지 않음.");
            }

            string[] tokens = messageBody.Split(new char[1] { '#' });
            if (tokens.Length != 6)
            {
                throw new Exception("토큰의 갯수가 일치하지 않음.");
            }

            try
            {
                this.eInCarSearchPopupType = (EInCarSearchPopupType)Enum.Parse(typeof(EInCarSearchPopupType), tokens[1]);
                this.GateNo = Int32.Parse(tokens[2]); this.CarNum = tokens[3];
                this.ImgPath = tokens[4]; this.Time = Int32.Parse(tokens[5]);
            }
            catch (Exception)
            {
                throw new Exception("메시지 형식이 일치하지 않습니다.");
            }
        }
    }
    public class SMsgParkingPayPopup : SogNo_Message
    {
        static string msgID { get; set; } = "ParkPayPopup";
        public int GateNo { get; set; } = 0;
        public EParkingPayPopup eParkPayPopup { get; set; } = EParkingPayPopup.Popup;
        public EPayType ePayType { get; set; } = EPayType.Cash;
        public string CarNum { get; set; }
        public string ImgPath { get; set; }
        public int ParkingTime { get; set; }
        public int ParkingFee { get; set; }
        public int DCFee { get; set; }
        public string DCNos { get; set; }

        public override byte[] Pack()
        {
            string msgBody = string.Format("{0}#{1}#{2}#{3}#{4}#{5}#{6}#{7}#{8}#{9}", msgID, this.GateNo, this.eParkPayPopup,
                this.ePayType, this.CarNum, this.ImgPath, this.ParkingTime, this.ParkingFee, this.DCFee, this.DCNos);

            int bodyLen = Encoding.Default.GetBytes(msgBody).Length;

            string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, CharTextSign, bodyLen, msgBody, CharETX);
            return Encoding.Default.GetBytes(message);
        }
        public override void UnPack(string messageBody)
        {
            if (GetMsgID(messageBody) != msgID)
            {
                throw new Exception("메시지 ID가 일치하지 않음.");
            }

            string[] tokens = messageBody.Split(new char[1] { '#' });
            if (tokens.Length != 10)
            {
                throw new Exception("토큰의 갯수가 일치하지 않음.");
            }

            try
            {
                this.GateNo = Int32.Parse(tokens[1]);
                this.eParkPayPopup = (EParkingPayPopup)Enum.Parse(typeof(EParkingPayPopup), tokens[2]);
                this.ePayType = (EPayType)Enum.Parse(typeof(EPayType), tokens[3]);
                this.CarNum = tokens[4];
                this.ImgPath = tokens[5];
                this.ParkingTime = int.Parse(tokens[6]);
                this.ParkingFee = int.Parse(tokens[7]);
                this.DCFee = int.Parse(tokens[8]);
                this.DCNos = tokens[9];
            }
            catch (Exception)
            {
                throw new Exception("메시지 형식이 일치하지 않습니다.");
            }
        }
    }
    public class SMsgCarIOData : SogNo_Message
    {
        static string msgID { get; set; } = "CarIOData";
        public ECarIOType eCarIOType { get; set; } = ECarIOType.CarInInsert;
        public string TKNo { get; set; } = "";
        public string GroupInfo { get; set; } = "";
        public string CarNum1 { get; set; } = "";
        public string ImgPath1 { get; set; } = "";
        public string CarNum2 { get; set; } = "";
        public string ImgPath2 { get; set; } = "";
        public DateTime? IOTime { get; set; } = DateTime.Now;
        public int? GateNo { get; set; } = 0;
        public int? EInType { get; set; } = 0;
        public EPayType ePayType { get; set; } = EPayType.Card;
        public int ParkingTime { get; set; } = 0;
        public int ParkingFee { get; set; } = 0;
        public string Memo { get; set; } = "";

        public void SetData(CarIODataRecord carIODataRecord)
        {
            this.TKNo = carIODataRecord.TKNo;

            CarNum2 = carIODataRecord.InCarNum2;
            ImgPath2 = carIODataRecord.InImage2;
            GroupInfo = carIODataRecord.GroupName;
            GateNo = carIODataRecord.InGateNo;
            EInType = carIODataRecord.InType;
            Memo = carIODataRecord.Memo;
            if (eCarIOType == ECarIOType.CarOutInsert)
            {
                CarNum1 = carIODataRecord.OutCarNum1;
                ImgPath1 = carIODataRecord.OutImage1;
                ParkingTime = (int)carIODataRecord.ParkingTime;
                ParkingFee = (int)carIODataRecord.ParkingFee;
                IOTime = carIODataRecord.OutDate;
                GateNo = carIODataRecord.OutGateNo;
            }
            else
            {
                CarNum1 = carIODataRecord.InCarNum1;
                ImgPath1 = carIODataRecord.InImage1;
                IOTime = carIODataRecord.InDate;
                GateNo = carIODataRecord.InGateNo;
            }
        }
        public CarIODataRecord GetData()
        {
            CarIODataRecord carIODataRecord = new CarIODataRecord();
            this.TKNo = carIODataRecord.TKNo;

            carIODataRecord.InCarNum2 = CarNum2;
            carIODataRecord.InImage2 = ImgPath2;
            carIODataRecord.GroupName = GroupInfo;
            carIODataRecord.InGateNo = GateNo;
            carIODataRecord.InType = EInType;
            carIODataRecord.Memo = Memo;
            if (eCarIOType == ECarIOType.CarOutInsert)
            {
                carIODataRecord.OutCarNum1 = CarNum1;
                carIODataRecord.OutImage1 = ImgPath1;
                carIODataRecord.ParkingTime = ParkingTime;
                carIODataRecord.ParkingFee = ParkingFee;
                carIODataRecord.OutDate = IOTime;
                carIODataRecord.OutGateNo = GateNo;
            }
            else
            {
                carIODataRecord.InCarNum1 = CarNum1;
                carIODataRecord.InImage1 = ImgPath1;
                carIODataRecord.InDate = IOTime;
                carIODataRecord.InGateNo = GateNo;
            }

            return carIODataRecord;
        }
        public override byte[] Pack()
        {
            string msgBody = string.Format("{0}#{1}#{2}#{3}#{4}#{5}#{6}#{7}#{8}#{9}#{10}#{11}#{12}#{13}", msgID, this.eCarIOType, this.TKNo, this.GroupInfo,
                this.CarNum1, this.ImgPath1, this.CarNum2, this.ImgPath2, DateTimeToString(this.IOTime), this.ePayType, this.ParkingTime, this.ParkingFee, this.EInType, this.Memo);

            int bodyLen = Encoding.Default.GetBytes(msgBody).Length;

            string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, CharTextSign, bodyLen, msgBody, CharETX);
            return Encoding.Default.GetBytes(message);
        }

        public override void UnPack(string messageBody)
        {
            if (GetMsgID(messageBody) != msgID)
            {
                throw new Exception("메시지 ID가 일치하지 않음.");
            }

            string[] tokens = messageBody.Split(new char[1] { '#' });
            if (tokens.Length != 14)
            {
                throw new Exception("토큰의 갯수가 일치하지 않음.");
            }

            try
            {
                this.eCarIOType = (ECarIOType)Enum.Parse(typeof(ECarIOType), tokens[1]);
                this.TKNo = tokens[2];
                this.GroupInfo = tokens[3];
                this.CarNum1 = tokens[4];
                this.ImgPath1 = tokens[5];
                this.CarNum2 = tokens[6];
                this.ImgPath2 = tokens[7];
                this.IOTime = StringToDateTime(tokens[8]);
                this.ePayType = (EPayType)Enum.Parse(typeof(EPayType), tokens[9]);
                this.ParkingTime = int.Parse(tokens[10]);
                this.ParkingFee = int.Parse(tokens[11]);
                this.EInType = int.Parse(tokens[12]);
            }
            catch (Exception)
            {
                throw new Exception("메시지 형식이 일치하지 않습니다.");
            }
        }
    }
    public enum CarIODataFields
    {
        ParkNo,
        TKNo,
        TKType,
        CarType,
        GroupNo,
        GroupName,
        Comp,
        Dept,
        Name,
        InDate,
        InType,
        InGateNo,
        InImage1,
        InCarNum1,
        InImage2,
        InCarNum2,
        OutDate,
        OutType,
        OutGateNo,
        OutImage1,
        OutCarNum1,
        ParkingTime,
        ParkingFee,
        Memo,
        DCName,
        Reserve0
    }
    public class CarIODataRecord
    {
        public int? ParkNo { get; set; } = null;
        public string TKNo { get; set; } = null;
        public int? TKType { get; set; } = null;            // 0: 일반차량, 1: 등록차량, 2: 사전등록, 3:장기주차
        public int? CarType { get; set; } = 1;              // 0: 경차, 1: 중형차, 2: 대형차, 3: 특수차
        public int? GroupNo { get; set; } = null;
        public string GroupName { get; set; } = null;       // 그룹이름
        public string Comp { get; set; } = null;            // 회사
        public string Dept { get; set; } = null;            // 부서
        public string Name { get; set; } = null;            // 이름
        public DateTime? InDate { get; set; } = null;       // 입차일시
        public int? InType { get; set; } = 0;               // 입차타입 (0: 거부, 1: 자동, 2: 팝업, 3: 자리비움, 4: 입차누락)
        public int? InGateNo { get; set; } = null;
        public string InImage1 { get; set; } = null;
        public string InCarNum1 { get; set; } = null;
        public string InImage2 { get; set; } = null;
        public string InCarNum2 { get; set; } = null;
        public DateTime? OutDate { get; set; } = null;      // 출차일시
        public int? OutType { get; set; } = null;           // 출차타입 (0: 미출차, 1: 자동, 2: 팝업, 3: 자리비움, 4: 출차누락)
        public int? OutGateNo { get; set; } = null;
        public string OutImage1 { get; set; } = null;
        public string OutCarNum1 { get; set; } = null;
        public int? ParkingTime { get; set; } = null;       // 주차시간
        public int? ParkingFee { get; set; } = null;        // 주차요금
        public string Memo { get; set; } = null;
        public string DCName { get; set; } = null;          // 할인권이름
        public string Reserve0 { get; set; } = null;

        public bool IsValid()
        {
            return (this.ParkNo != null && this.TKNo != null && this.TKNo.Length > 0 && this.InCarNum1 != null && this.InDate != null);
        }
    }
    public enum TKProcClosingFields
    {
        TKPCNo,
        ParkNo,
        SysNo,
        MemNo,
        ProcDate,
        ProcCnt,
        ProcFee,
        ProcDCCnt,
        ProcDCFee,
        ProcCashCnt,
        ProcCash,
        ProcCardCnt,
        ProcCard,
        TurningCnt,
        ProcDCInfos,
        Reserve0,
    }
    public class TKProcClosingRecord
    {
        public int TKPCNo { get; set; } = 0;
        public int ParkNo { get; set; } = 0;
        public int SysNo { get; set; } = 0;
        public int MemNo { get; set; } = 0;
        public DateTime? ProcDate { get; set; } = null;
        public int ProcCnt { get; set; } = 0;
        public int ProcFee { get; set; } = 0;
        public int ProcDCCnt { get; set; } = 0;
        public int ProcDCFee { get; set; } = 0;
        public int ProcCashCnt { get; set; } = 0;
        public int ProcCash { get; set; } = 0;
        public int ProcCardCnt { get; set; } = 0;
        public int ProcCard { get; set; } = 0;
        public int TurningCnt { get; set; } = 0;
        public string ProcDCInfos { get; set; } = null;
        public string Reserve0 { get; set; } = null;
    }

    public class CarIOGateInfo
    {
        private CarIODBManager carIODBManager = null;

        public delegate void evtGateCarInProcess(SMsgCarInPopup msgCarInPopup);
        public event evtGateCarInProcess GateCarInProcess;
        public delegate void evtInCarSearchProcess(SMsgInCarSearchPopup sMsgInCarSearchPopup);
        public event evtInCarSearchProcess InCarSearchProcess;
        public delegate void evtParkingPayProcess(SMsgParkingPayPopup sMsgParkingPayPopup);
        public event evtParkingPayProcess ParkingPayProcess;

        public CarIOGateInfo(CarIODBManager carIODBManager)
        {
            this.carIODBManager = carIODBManager;
        }

        public int ParkNo { get; set; } = 0;
        public int GateNo { get; set; } = 0;
        public string GateName { get; set; } = "";
        public EGateInType GateInType { get; set; } = EGateInType.Always;
        public EGateOutType GateOutType { get; set; } = EGateOutType.Always;
        public List<string> TKNos { get; set; } = new List<string>();
        public frmParkCarIn CarInForm { get; set; } = null;
        public frmParkingPay ParkPayForm { get; set; } = null;
        public frmInCarSearch InCarSearchForm { get; set; } = null;

        public EIOProcType CarIOProcType { get; set; } = EIOProcType.InCar;
        public CarIODataRecord carIODataRecord { get; set; }
        public SogNo_Bill SogNo_Bill { get; set; }
        public List<SogNo_TollProcess.ParkingDetail> parkingDetails { get; set; } = new List<SogNo_TollProcess.ParkingDetail>();
        public List<ISogNo_Discount> discounts { get; set; } = null;
        public CustomerRecord CustInfo { get; set; } = null;
        public GroupInfoRecord GroupInfoRecord { get; set; } = null;
        public ParkInfoRecord MyParkInfo { get; set; } = null;
        public SPCDCInfo sPCDCInfo { get; set; } = null;
        public void CreateCarIODataRecord()
        {
            carIODataRecord = new CarIODataRecord();
            carIODataRecord.ParkNo = this.ParkNo;
            carIODataRecord.InGateNo = this.GateNo;
            carIODataRecord.InType = 0;

            CustInfo = null;
            GroupInfoRecord = null;
            SogNo_Bill = null;
            discounts = null;
            sPCDCInfo = null;
        }
        public bool isInCust()
        {
            if (CustInfo == null || CustInfo.EndDate < DateTime.Now)
                return false;

            return true;
        }
        public bool isOutCust()
        {
            if (CustInfo != null && CustInfo.EndDate > carIODataRecord.InDate)
                return true;

            return false;
        }
        public string GetStrDCInfos()
        {
            if (sPCDCInfo == null) return null;
            return String.Format("{0}:{1}:{2}", sPCDCInfo.DCNo, sPCDCInfo.DCName, SogNo_Bill.OriginalPrice - SogNo_Bill.Price);
        }

        public void SetCarIODataRecord(int Type, SMsgLPRResult sMsgLPRResult)
        {
            /// 입차전방 && 입차후방 시 전장 TKNo를 찾지 못한다면 
            if (Type == 0)
            {
                carIODataRecord.InGateNo = sMsgLPRResult.GateNo;
                carIODataRecord.TKNo = ParkCarIOManager.CreateTKNo(sMsgLPRResult.GateNo, (DateTime)sMsgLPRResult.OccurTime);
                carIODataRecord.InCarNum1 = sMsgLPRResult.CarNumber;
                carIODataRecord.InImage1 = sMsgLPRResult.ImagePath;
                carIODataRecord.InDate = sMsgLPRResult.OccurTime;
                carIODataRecord.InType = 0;
            }
            /// 입차 후방 데이터 입력
            else if (Type == 1)
            {
                carIODataRecord.InCarNum2 = sMsgLPRResult.CarNumber;
                carIODataRecord.InImage2 = sMsgLPRResult.ImagePath;
            }
            else if (Type == 2)
            {
                carIODataRecord.OutGateNo = sMsgLPRResult.GateNo;
                carIODataRecord.OutCarNum1 = sMsgLPRResult.CarNumber;
                carIODataRecord.OutImage1 = sMsgLPRResult.ImagePath;
                carIODataRecord.OutDate = sMsgLPRResult.OccurTime;
            }
        }
        public void CarInDataMatch(CarIODataRecord carIODataRecord)
        {
            this.carIODataRecord.TKNo = carIODataRecord.TKNo;
            this.carIODataRecord.InCarNum1 = carIODataRecord.InCarNum1;
            this.carIODataRecord.InImage1 = carIODataRecord.InImage1;
            this.carIODataRecord.InDate = carIODataRecord.InDate;

            SetParkingDetail();
        }
        public void CarInDataMatch(int ParkingTime)
        {
            this.carIODataRecord.InDate = carIODataRecord.OutDate?.AddMinutes(ParkingTime * -1);
            this.carIODataRecord.InCarNum1 = carIODataRecord.OutCarNum1;
            this.carIODataRecord.InImage1 = carIODataRecord.OutImage1;

            SetParkingDetail();
        }
        private void SetParkingDetail()
        {
            carIODataRecord.OutType = 0;
            carIODataRecord.ParkingTime = (int?)(carIODataRecord.OutDate - carIODataRecord.InDate)?.TotalMinutes;

            DateTime inCarDT = (DateTime)this.carIODataRecord.InDate;
            DateTime outCarDT = (DateTime)this.carIODataRecord.OutDate;
            DateTime memberStartDT = DateTime.Now;
            DateTime memberEndDT = DateTime.Now;

            parkingDetails.Clear();
            if (isOutCust())
            {
                memberStartDT = (DateTime)CustInfo.StartDate;
                memberEndDT = (DateTime)CustInfo.EndDate;
            }

            if (!isOutCust() || memberEndDT <= inCarDT || outCarDT <= memberStartDT)
            {
                // 정기권 차량이 아니거나 주차 기간이 정기권 기간과 겹치지 않음
                parkingDetails.Add(new SogNo_TollProcess.ParkingDetail(MyParkInfo.DefaultTrfNo.ToString(), inCarDT, outCarDT));
            }
            else if (memberStartDT <= inCarDT && outCarDT <= memberEndDT)
            {
                // 주차 기간 전체가 정기권 기간에 포함
                parkingDetails.Add(new SogNo_TollProcess.ParkingDetail(GroupInfoRecord.TrfNo.ToString(), inCarDT, outCarDT));
            }
            else if (inCarDT < memberStartDT && outCarDT <= memberEndDT)
            {
                // 주차 구간의 후반부가 정기권 기간에 포함
                parkingDetails.Add(new SogNo_TollProcess.ParkingDetail(MyParkInfo.DefaultTrfNo.ToString(), inCarDT, memberStartDT));
                parkingDetails.Add(new SogNo_TollProcess.ParkingDetail(GroupInfoRecord.TrfNo.ToString(), memberStartDT, outCarDT));
            }
            else if (memberStartDT <= inCarDT && memberEndDT < outCarDT)
            {
                // 주차 구간의 전반부가 정기권 기간에 포함
                parkingDetails.Add(new SogNo_TollProcess.ParkingDetail(GroupInfoRecord.TrfNo.ToString(), inCarDT, memberEndDT));
                parkingDetails.Add(new SogNo_TollProcess.ParkingDetail(MyParkInfo.DefaultTrfNo.ToString(), memberEndDT, outCarDT));
            }
            else
            {
                // 주차 구간의 중반부가 정기권 기간에 포함
                parkingDetails.Add(new SogNo_TollProcess.ParkingDetail(MyParkInfo.DefaultTrfNo.ToString(), inCarDT, memberStartDT));
                parkingDetails.Add(new SogNo_TollProcess.ParkingDetail(GroupInfoRecord.TrfNo.ToString(), memberStartDT, memberEndDT));
                parkingDetails.Add(new SogNo_TollProcess.ParkingDetail(MyParkInfo.DefaultTrfNo.ToString(), memberEndDT, outCarDT));
            }
        }

        public void SetDisCount(SPCDCInfo sPCDCInfo)
        {
            this.sPCDCInfo = sPCDCInfo;

            if (discounts == null) discounts = new List<ISogNo_Discount>();
            else discounts.Clear();

            discounts.Add(sPCDCInfo.sogNo_Discount);
        }
        public bool PopTKNo()
        {
            if (TKNos.Count == 0) return false;

            carIODataRecord.TKNo = TKNos[0];
            TKNos.RemoveAt(0);

            return true;
        }
        public SMsgCarIOData GetSMsgCarIOData(ECarIOType eCarIOType)
        {
            SMsgCarIOData sMsgCarIOData = new SMsgCarIOData();

            sMsgCarIOData.TKNo = carIODataRecord.TKNo;

            sMsgCarIOData.CarNum2 = carIODataRecord.InCarNum2;
            sMsgCarIOData.ImgPath2 = carIODataRecord.InImage2;
            sMsgCarIOData.GroupInfo = carIODataRecord.GroupName;
            sMsgCarIOData.GateNo = carIODataRecord.InGateNo;
            sMsgCarIOData.EInType = carIODataRecord.InType;
            sMsgCarIOData.Memo = carIODataRecord.Memo;
            sMsgCarIOData.eCarIOType = eCarIOType;

            if (eCarIOType == ECarIOType.CarOutInsert)
            {
                sMsgCarIOData.CarNum1 = carIODataRecord.OutCarNum1;
                sMsgCarIOData.ImgPath1 = carIODataRecord.OutImage1;
                sMsgCarIOData.ParkingTime = (int)carIODataRecord.ParkingTime;
                sMsgCarIOData.ParkingFee = (int)carIODataRecord.ParkingFee;
                sMsgCarIOData.IOTime = carIODataRecord.OutDate;
                sMsgCarIOData.GateNo = carIODataRecord.OutGateNo;
            }
            else
            {
                sMsgCarIOData.CarNum1 = carIODataRecord.InCarNum1;
                sMsgCarIOData.ImgPath1 = carIODataRecord.InImage1;
                sMsgCarIOData.IOTime = carIODataRecord.InDate;
                sMsgCarIOData.GateNo = carIODataRecord.InGateNo;
            }
            return sMsgCarIOData;
        }
        public SMsgCarIOData GetSMsgCarIOData(ECarIOType eCarIOType, EPayType ePayType)
        {
            SMsgCarIOData sMsgCarIOData = GetSMsgCarIOData(eCarIOType);
            sMsgCarIOData.ePayType = ePayType;

            return sMsgCarIOData;
        }
        public SMsgCarInPopup GetSMsgCarInPopup()
        {
            SMsgCarInPopup msgCarInPopup = new SMsgCarInPopup();
            msgCarInPopup.GateNo = (int)carIODataRecord.InGateNo;
            msgCarInPopup.CarNum = carIODataRecord.InCarNum1;
            msgCarInPopup.ImgPath = carIODataRecord.InImage1;

            return msgCarInPopup;
        }
        public SMsgParkingPayPopup GetSMsgParkingPayPopup()
        {
            SMsgParkingPayPopup sMsgParkingPayPopup = new SMsgParkingPayPopup();

            sMsgParkingPayPopup.GateNo = GateNo;
            sMsgParkingPayPopup.ImgPath = carIODataRecord.OutImage1;
            sMsgParkingPayPopup.ParkingFee = SogNo_Bill.Price;
            sMsgParkingPayPopup.ParkingTime = (int)carIODataRecord.ParkingTime;
            sMsgParkingPayPopup.CarNum = carIODataRecord.OutCarNum1;

            return sMsgParkingPayPopup;
        }
        public void AddTKNo(string TKNo) { TKNos.Add(TKNo); }
        public void AddTKNo() { if (carIODataRecord != null) TKNos.Add(carIODataRecord.TKNo); }
        public void Hidefrms()
        {
            try { CarInForm.Hide(); } catch { }
            try { InCarSearchForm.Hide(); } catch { }
            try { ParkPayForm.Hide(); } catch { }
        }
        public void ShowfrmParkCarIn(SMsgCarInPopup msgCarInPopup)
        {
            try { CarInForm.SetParkCarInData(msgCarInPopup); }
            catch
            {
                CarInForm = new frmParkCarIn(GateName);
                CarInForm.AcceptEvent += CarInAcceptEvt;
                CarInForm.SetParkCarInData(msgCarInPopup);
            }
        }
        public void HidefrmParkCarIn() { try { CarInForm.Hide(); } catch { } }
        public void CarInAcceptEvt(SMsgCarInPopup msgCarInPopup)
        {
            GateCarInProcess?.Invoke(msgCarInPopup);
        }
        public SMsgInCarSearchPopup GetSMsgInCarSearchPopup()
        {
            SMsgInCarSearchPopup sMsgInCarSearchPopup = new SMsgInCarSearchPopup();

            sMsgInCarSearchPopup.GateNo = (int)carIODataRecord.OutGateNo;
            sMsgInCarSearchPopup.CarNum = carIODataRecord.OutCarNum1;
            sMsgInCarSearchPopup.ImgPath = carIODataRecord.OutImage1;

            return sMsgInCarSearchPopup;
        }
        public void ShowfrmInCarSearch(SMsgInCarSearchPopup sMsgInCarSearchPopup)
        {
            try { InCarSearchForm.SetInCarSearchData(sMsgInCarSearchPopup); }
            catch
            {
                InCarSearchForm = new frmInCarSearch(GateName, this.carIODBManager);
                InCarSearchForm.AcceptEvent += InCarSearchEvt;
                InCarSearchForm.SetInCarSearchData(sMsgInCarSearchPopup);
            }
        }
        public void HidefrmInCarSearch() { try { InCarSearchForm.Hide(); } catch { } }
        public void InCarSearchEvt(SMsgInCarSearchPopup sMsgInCarSearchPopup)
        {
            InCarSearchProcess?.Invoke(sMsgInCarSearchPopup);
        }
        public void ShowfrmParkingPay(SMsgParkingPayPopup sMsgParkingPayPopup, List<SPCDCInfo> UsedSPCDCInfos)
        {
            try { ParkPayForm.SetParkingPayData(sMsgParkingPayPopup); }
            catch
            {
                ParkPayForm = new frmParkingPay(UsedSPCDCInfos);
                ParkPayForm.ParkingPayEvent += ParkingPayEvt;

                ParkPayForm.SetParkingPayData(sMsgParkingPayPopup);
            }
        }
        public void HidefrmParkingPay() { try { ParkPayForm.Hide(); } catch { } }
        public void ParkingPayEvt(SMsgParkingPayPopup sMsgParkingPayPopup)
        {
            ParkingPayProcess?.Invoke(sMsgParkingPayPopup);
        }

        public void TollProcess(SogNo_TollProcess tollProcess)
        {
            if (discounts == null)
                SogNo_Bill = tollProcess.Toll(parkingDetails.ToArray(), null);
            else
                SogNo_Bill = tollProcess.Toll(parkingDetails.ToArray(), discounts.ToArray());

            carIODataRecord.ParkingFee = SogNo_Bill.Price;
        }
    }
    public enum TKProcFields
    {
        ParkNo,
        SysNo,
        ProcDate,
        TKNo,
        TKType,
        CarType,
        CarNum,
        InDate,
        ParkingTime,
        TotalFee,
        RealFee,
        DCInfos,
        MemNo,
        ChkClosing,
        PayType,
        Reserve0
    }
    public class TKProcRecord
    {
        public int ParkNo { get; set; } = 0;                // 주차장번호
        public int SysNo { get; set; } = 0;                 // 시스템번호
        public DateTime? OutDate { get; set; } = null;      // 출차시간
        public DateTime? InDate { get; set; } = null;       // 입차시간
        public string TKNo { get; set; } = null;            // TKNo
        public int? TKType { get; set; } = 0;               // 0: 일반차량, 1: 등록차량, 2: 사전등록, 3:장기주차
        public int? CarType { get; set; } = 1;              // 0: 경차, 1: 중형차, 2: 대형차, 3: 특수차
        public string CarNum { get; set; } = null;          // 차량번호
        public int? ParkingTime { get; set; } = null;       // 주차시간
        public int? ParkingFee { get; set; } = null;        // 주차요금
        public int? Card { get; set; } = null;              // 카드
        public int? Cash { get; set; } = null;              // 현금
        public string DCInfos { get; set; } = null;         // 할인형식 및 할인요금
        public int? MemNo { get; set; } = null;             // 관리자번호
        public string ChkClosing { get; set; } = null;      // 정산정보
        public int PayType { get; set; } = 0;               // 결제형식
        public string Reserve0 { get; set; } = null;        // 비고
    }
    public class CarIODBManager
    {
        private SogNo_DBManager dbManager = null;
        public CarIODBManager(SogNo_DBInfo sogNo_DBInfo)
        {
            try
            {
                this.dbManager = new SogNo_DBManager(sogNo_DBInfo);
            }
            catch
            {
                this.dbManager = null;
                throw;
            }
        }

        public void ResetCarIOData()
        {
            this.dbManager.Update("truncate cariodata");
        }
        public void InsertCarIOData(CarIODataRecord carIODataRecord)
        {
            /// !!!!! 기존 입차 정보 중, 미출차 내역 출차 처리 !!!!!
            if (carIODataRecord.OutCarNum1 == null)
                UpdateAlreadyInCarData(carIODataRecord.InCarNum1);

            /// 차량 입차 정보 입력
            this.dbManager.Insert(carIODataRecord, "cariodata");
        }
        private void UpdateAlreadyInCarData(string CarNum)
        {
            if (this.dbManager == null) { return; }
            if (CarNum == "xxxxxxxx") return;
            /// 입차 형식 데이터 업데이트
            this.dbManager.Update(String.Format("UPDATE `cariodata` SET OutType = '4' WHERE OutType is null and (InCarNum1 = '{0}' or  InCarNum2 = '{0}')", CarNum));
        }
        public void UpdateCarOutData(CarIODataRecord carIODataRecord)
        {
            if (this.dbManager == null) { return; }

            /// 입차 형식 데이터 업데이트
            string[] whereFields = {
                CarIODataFields.TKNo.ToString()
            };
            string[] setFields = {
                CarIODataFields.OutImage1.ToString(),
                CarIODataFields.OutCarNum1.ToString(),
                CarIODataFields.OutDate.ToString(),
                CarIODataFields.OutType.ToString(),
                CarIODataFields.ParkingTime.ToString(),
                CarIODataFields.ParkingFee.ToString(),
                CarIODataFields.OutGateNo.ToString()
            };
            this.dbManager.Update(carIODataRecord, "cariodata", setFields, whereFields);
        }
        public void UpdateInType(CarIODataRecord carIODataRecord)
        {
            if (this.dbManager == null) { return; }

            /// 입차 형식 데이터 업데이트
            string[] whereFields = {
                CarIODataFields.TKNo.ToString()
            };
            string[] setFields = {
                CarIODataFields.InType.ToString(),
                CarIODataFields.InCarNum1.ToString(),
                CarIODataFields.Memo.ToString()
            };
            this.dbManager.Update(carIODataRecord, "cariodata", setFields, whereFields);
        }
        public void UpdateCarInRear(CarIODataRecord carIODataRecord)
        {
            if (this.dbManager == null) { return; }

            /// 입차 형식 데이터 업데이트
            string[] whereFields = {
                CarIODataFields.TKNo.ToString()
            };
            string[] setFields = {
                CarIODataFields.InCarNum2.ToString(),
                CarIODataFields.InImage2.ToString()
            };
            this.dbManager.Update(carIODataRecord, "cariodata", setFields, whereFields);
        }
        public CarIODataRecord[] GetOutCarData(int ParkNo)
        {
            if (this.dbManager == null) { return null; }

            string query = String.Format("select * from cariodata inner join tkproc on cariodata.tkno = tkproc.tkno  where tkproc.chkclosing is null and tkproc.parkno = {0}", ParkNo);

            return this.dbManager.GetRecords<CarIODataRecord>(query);
        }
        public CarIODataRecord[] GetNoOutCarData(int ParkNo, string CarNum)
        {
            if (this.dbManager == null) { return null; }

            string query = String.Format("select * from cariodata where ParkNo = '{0}' and OutType is null and not InType = '0' ", ParkNo);

            if (CarNum != "")
                query += String.Format("and ((InCarNum1 = '{0}' and not InCarNum1 = 'xxxxxxxx') or ( InCarNum2 = '{0}' and not InCarNum2 = 'xxxxxxxx')) ", CarNum);

            query += "order by InDate desc";

            return this.dbManager.GetRecords<CarIODataRecord>(query);
        }
        public CarIODataRecord[] GetNoOutCarLikeData(int ParkNo, string CarNum)
        {
            if (this.dbManager == null) { return null; }

            string query = String.Format("select * from cariodata where ParkNo = '{0}' and OutType is null and not InType = '0'", ParkNo);
            query += String.Format("and ((InCarNum1 LIKE '%{0}%') or ( InCarNum2 LIKE '%{0}%'))", CarNum);
            return this.dbManager.GetRecords<CarIODataRecord>(query);
        }
        public CarIODataRecord[] GetCarIOData(CarIOSearchData carIOSearchData)
        {
            if (this.dbManager == null) { return null; }

            string query = String.Format("select * from cariodata where ParkNo = '{0}' and ({1} BETWEEN '{2}' AND '{3}')",
                carIOSearchData.ParkNo, carIOSearchData.IOType == 0 ? "InDate" : "OutDate", carIOSearchData.StartTime, carIOSearchData.EndTime);
            query += String.Format("and ((InCarNum1 LIKE '%{0}%') or ( InCarNum2 LIKE '%{0}%') or ( OutCarNum1 LIKE '%{0}%'))", carIOSearchData.CarNum);
            return this.dbManager.GetRecords<CarIODataRecord>(query);
        }
        public long InsertTKProcClosing(TKProcClosingRecord tKProcClosingRecord)
        {
            /// 차량 입차 정보 입력
            return this.dbManager.Insert(tKProcClosingRecord, "tkprocclosing");
        }
        public void InsertTKProc(TKProcRecord tKProcRecord)
        {
            /// 차량 입차 정보 입력
            this.dbManager.Insert(tKProcRecord, "tkproc");
        }

        public TKProcRecord[] GetTKProcData(int ParkNo, int? SysNo, DateTime ProcDate)
        {
            if (this.dbManager == null) { return null; }

            string query = String.Format("select * from tkproc where ParkNo = '{0}' and OutDate <= '{1}' and ChkClosing is null",
                ParkNo, ProcDate.ToString("yyyy-MM-dd HH:mm:ss"));
            if (SysNo != null)
                query += String.Format(" and SysNo = '{0}'", SysNo);

            return this.dbManager.GetRecords<TKProcRecord>(query);
        }
        public void UpdateTKProcClosing(int ParkNo, int? SysNo, DateTime ProcDate, long TKPCIndex)
        {
            if (this.dbManager == null) { return; }

            /// 마감 정보 업데이트
            string query = String.Format("UPDATE `tkproc` SET ChkClosing = '{0}' WHERE ParkNo = '{1}' and OutDate <= '{2}'",
                TKPCIndex, ParkNo, ProcDate.ToString("yyyy-MM-dd hh:mm:ss"));

            if (SysNo != null)
                query += String.Format(" and SysNo = '{0}'", SysNo);


            this.dbManager.Update(query);
        }
    }


    public class CarIOSearchData
    {
        public int ParkNo { get; set; } = 0;
        public int IOType { get; set; } = 0;
        public string StartTime { get; set; } = "";
        public string EndTime { get; set; } = "";
        public string CarNum { get; set; } = "";
    }
    public class SPCDCInfo
    {
        public int DCNo;
        public string DCName;
        public int DCCnt;
        public int DCFee;
        public ISogNo_Discount sogNo_Discount;

        public SPCDCInfo() { }
        public SPCDCInfo(string strDCInfo)
        {
            try
            {
                string[] strATmp = strDCInfo.Split(':');
                DCNo = Convert.ToInt32(strATmp[0]);
                DCName = strATmp[1];
                DCFee = Convert.ToInt32(strATmp[2]);
            }
            catch
            {
                DCNo = 0;
                DCName = "";
                DCFee = 0;
            }
        }
        public string GetStrDCInfo()
        {
            return String.Format("{0}:{1}:{2}", DCNo, DCName, DCFee);
        }
    }
}
