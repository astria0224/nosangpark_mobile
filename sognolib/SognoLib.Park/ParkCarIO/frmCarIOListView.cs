﻿using System;
using System.Windows.Forms;
using SognoLib.Park.ParkCarIO;

namespace SognoLib.Park.ParkCarIO
{
    public partial class frmCarIOListView : Form
    {
        int ParkNo = 0; CarIODBManager CarIODBManager;
        public frmCarIOListView(int ParkNo, CarIODBManager CarIODBManager)
        {
            InitializeComponent();

            /// 
            cbIOType.SelectedIndex = 0;

            /// 
            this.ParkNo = ParkNo; this.CarIODBManager = CarIODBManager;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            CarIOSearchData carIOSearchData = new CarIOSearchData();
            carIOSearchData.IOType = cbIOType.SelectedIndex;
            carIOSearchData.StartTime = dtpStartDate.Value.ToString("yyyy-MM-dd");
            carIOSearchData.EndTime = dtpEndDate.Value.ToString("yyyy-MM-dd");
            carIOSearchData.CarNum = txtCarNum.Text;

            CarIODataRecord[] carIODataRecords = CarIODBManager.GetCarIOData(carIOSearchData);

            dgvCarIOData.Rows.Clear();
            for (int i = 0; i < carIODataRecords.Length; i++)
            {
                DataGridViewRow NewRow = (DataGridViewRow)dgvCarIOData.Rows[0].Clone(); NewRow.Height = 30;
                NewRow.Cells[0].Value = carIODataRecords[i].TKNo;
                NewRow.Cells[1].Value = carIODataRecords[i].InDate;
                NewRow.Cells[2].Value = carIODataRecords[i].InCarNum1;
                NewRow.Cells[3].Value = carIODataRecords[i].InCarNum2;
                NewRow.Cells[4].Value = carIODataRecords[i].OutDate;
                NewRow.Cells[5].Value = carIODataRecords[i].OutCarNum1;
                NewRow.Cells[6].Value = carIODataRecords[i].ParkingTime;
                NewRow.Cells[7].Value = carIODataRecords[i].ParkingFee;

                ///
                dgvCarIOData.Invoke((MethodInvoker)delegate
                {
                    dgvCarIOData.Rows.Add(NewRow);
                });
            }
        }
    }
}
