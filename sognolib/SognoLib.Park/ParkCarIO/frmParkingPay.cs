﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SognoLib.Park.ParkCarIO
{
    public partial class frmParkingPay : Form
    {
        private SMsgParkingPayPopup sMsgParkingPayPopup;

        public delegate void evtParkingPay(SMsgParkingPayPopup sMsgParkingPayPopup);
        public event evtParkingPay ParkingPayEvent;

        public frmParkingPay(List<SPCDCInfo> UsedSPCDCInfos)
        {
            InitializeComponent();

            sMsgParkingPayPopup = new SMsgParkingPayPopup();
            sMsgParkingPayPopup.ePayType = EPayType.Card;

            Button[] buttons = new Button[10];
            buttons[0] = btnDCF1; buttons[1] = btnDCF2; buttons[2] = btnDCF3; buttons[3] = btnDCF4; buttons[4] = btnDCF5;
            buttons[5] = btnDCF6; buttons[6] = btnDCF7; buttons[7] = btnDCF8; buttons[8] = btnDCF9; buttons[9] = btnDCF10;

            for (int i = 0; i < UsedSPCDCInfos.Count; i++)
            {
                if (i >= 10) break;

                buttons[i].Tag = UsedSPCDCInfos[i].DCNo;
                buttons[i].Text = String.Format("[F{0}] {1}", i + 1, UsedSPCDCInfos[i].DCName);
            }
        }

        public void SetParkingPayData(SMsgParkingPayPopup sMsgParkingPayPopup)
        {
            this.sMsgParkingPayPopup = sMsgParkingPayPopup;

            this.picOutCarImg.ImageLocation = sMsgParkingPayPopup.ImgPath;
            this.lblParkingFee.Text = sMsgParkingPayPopup.ParkingFee.ToString();
            this.lblParkingTime.Text = sMsgParkingPayPopup.ParkingTime.ToString();
            this.lblOutCarNum.Text = sMsgParkingPayPopup.CarNum;

            if (!this.Visible)
                this.ShowDialog();
        }
        private void btnDC_Click(object sender, EventArgs e)
        {
            sMsgParkingPayPopup.eParkPayPopup = EParkingPayPopup.DC;
            sMsgParkingPayPopup.ePayType = EPayType.DC;
            sMsgParkingPayPopup.DCNos = ((Button)sender).Tag.ToString();

            ParkingPayEvent?.Invoke(sMsgParkingPayPopup);
        }
        private void btnCash_Click(object sender, EventArgs e)
        {
            sMsgParkingPayPopup.eParkPayPopup = EParkingPayPopup.Pay;
            sMsgParkingPayPopup.ePayType = EPayType.Cash;
            ParkingPayEvent?.Invoke(sMsgParkingPayPopup); 
        }
        private void btnCard_Click(object sender, EventArgs e)
        {
            sMsgParkingPayPopup.eParkPayPopup = EParkingPayPopup.Pay;
            sMsgParkingPayPopup.ePayType = EPayType.Card;
            ParkingPayEvent?.Invoke(sMsgParkingPayPopup);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            sMsgParkingPayPopup.eParkPayPopup = EParkingPayPopup.Close;
            ParkingPayEvent?.Invoke(sMsgParkingPayPopup);
        }
    }
}
