﻿namespace SognoLib.Park.ParkCarIO
{
    partial class frmParkingPay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDCF10 = new System.Windows.Forms.Button();
            this.btnDCF9 = new System.Windows.Forms.Button();
            this.btnDCF8 = new System.Windows.Forms.Button();
            this.btnDCF7 = new System.Windows.Forms.Button();
            this.btnDCF6 = new System.Windows.Forms.Button();
            this.btnDCF5 = new System.Windows.Forms.Button();
            this.btnDCF4 = new System.Windows.Forms.Button();
            this.btnDCF3 = new System.Windows.Forms.Button();
            this.btnDCF2 = new System.Windows.Forms.Button();
            this.gbox_outcar = new System.Windows.Forms.GroupBox();
            this.layout_outcar_inner = new System.Windows.Forms.TableLayoutPanel();
            this.picOutCarImg = new System.Windows.Forms.PictureBox();
            this.listview_outcar_info = new System.Windows.Forms.ListView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblOutCarNum = new System.Windows.Forms.Label();
            this.lblOutCarDate = new System.Windows.Forms.Label();
            this.btnDCF1 = new System.Windows.Forms.Button();
            this.layout_main = new System.Windows.Forms.TableLayoutPanel();
            this.layout_outcar = new System.Windows.Forms.TableLayoutPanel();
            this.layout_toll_main = new System.Windows.Forms.TableLayoutPanel();
            this.gbox_actions = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnReceptPrint = new System.Windows.Forms.Button();
            this.btnCard = new System.Windows.Forms.Button();
            this.btnCash = new System.Windows.Forms.Button();
            this.gbox_parking_price = new System.Windows.Forms.GroupBox();
            this.layout_parking_price = new System.Windows.Forms.TableLayoutPanel();
            this.lblParkingFee = new System.Windows.Forms.Label();
            this.listview_parking_price_detail = new System.Windows.Forms.ListView();
            this.layout_parking_time = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.lblParkingTime = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.gbox_discount = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.gbox_outcar.SuspendLayout();
            this.layout_outcar_inner.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picOutCarImg)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.layout_main.SuspendLayout();
            this.layout_outcar.SuspendLayout();
            this.layout_toll_main.SuspendLayout();
            this.gbox_actions.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.gbox_parking_price.SuspendLayout();
            this.layout_parking_price.SuspendLayout();
            this.layout_parking_time.SuspendLayout();
            this.gbox_discount.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDCF10
            // 
            this.btnDCF10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDCF10.ForeColor = System.Drawing.Color.Black;
            this.btnDCF10.Location = new System.Drawing.Point(248, 349);
            this.btnDCF10.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDCF10.Name = "btnDCF10";
            this.btnDCF10.Size = new System.Drawing.Size(236, 80);
            this.btnDCF10.TabIndex = 9;
            this.btnDCF10.TabStop = false;
            this.btnDCF10.Tag = "9";
            this.btnDCF10.Text = "[F10]";
            this.btnDCF10.UseVisualStyleBackColor = true;
            this.btnDCF10.Click += new System.EventHandler(this.btnDC_Click);
            // 
            // btnDCF9
            // 
            this.btnDCF9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDCF9.ForeColor = System.Drawing.Color.Black;
            this.btnDCF9.Location = new System.Drawing.Point(4, 349);
            this.btnDCF9.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDCF9.Name = "btnDCF9";
            this.btnDCF9.Size = new System.Drawing.Size(236, 80);
            this.btnDCF9.TabIndex = 8;
            this.btnDCF9.TabStop = false;
            this.btnDCF9.Tag = "8";
            this.btnDCF9.Text = "[F9]";
            this.btnDCF9.UseVisualStyleBackColor = true;
            this.btnDCF9.Click += new System.EventHandler(this.btnDC_Click);
            // 
            // btnDCF8
            // 
            this.btnDCF8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDCF8.ForeColor = System.Drawing.Color.Black;
            this.btnDCF8.Location = new System.Drawing.Point(248, 263);
            this.btnDCF8.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDCF8.Name = "btnDCF8";
            this.btnDCF8.Size = new System.Drawing.Size(236, 76);
            this.btnDCF8.TabIndex = 7;
            this.btnDCF8.TabStop = false;
            this.btnDCF8.Tag = "7";
            this.btnDCF8.Text = "[F8]";
            this.btnDCF8.UseVisualStyleBackColor = true;
            this.btnDCF8.Click += new System.EventHandler(this.btnDC_Click);
            // 
            // btnDCF7
            // 
            this.btnDCF7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDCF7.ForeColor = System.Drawing.Color.Black;
            this.btnDCF7.Location = new System.Drawing.Point(4, 263);
            this.btnDCF7.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDCF7.Name = "btnDCF7";
            this.btnDCF7.Size = new System.Drawing.Size(236, 76);
            this.btnDCF7.TabIndex = 6;
            this.btnDCF7.TabStop = false;
            this.btnDCF7.Tag = "6";
            this.btnDCF7.Text = "[F7]";
            this.btnDCF7.UseVisualStyleBackColor = true;
            this.btnDCF7.Click += new System.EventHandler(this.btnDC_Click);
            // 
            // btnDCF6
            // 
            this.btnDCF6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDCF6.ForeColor = System.Drawing.Color.Black;
            this.btnDCF6.Location = new System.Drawing.Point(248, 177);
            this.btnDCF6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDCF6.Name = "btnDCF6";
            this.btnDCF6.Size = new System.Drawing.Size(236, 76);
            this.btnDCF6.TabIndex = 5;
            this.btnDCF6.TabStop = false;
            this.btnDCF6.Tag = "5";
            this.btnDCF6.Text = "[F6]";
            this.btnDCF6.UseVisualStyleBackColor = true;
            this.btnDCF6.Click += new System.EventHandler(this.btnDC_Click);
            // 
            // btnDCF5
            // 
            this.btnDCF5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDCF5.ForeColor = System.Drawing.Color.Black;
            this.btnDCF5.Location = new System.Drawing.Point(4, 177);
            this.btnDCF5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDCF5.Name = "btnDCF5";
            this.btnDCF5.Size = new System.Drawing.Size(236, 76);
            this.btnDCF5.TabIndex = 4;
            this.btnDCF5.TabStop = false;
            this.btnDCF5.Tag = "4";
            this.btnDCF5.Text = "[F5]";
            this.btnDCF5.UseVisualStyleBackColor = true;
            this.btnDCF5.Click += new System.EventHandler(this.btnDC_Click);
            // 
            // btnDCF4
            // 
            this.btnDCF4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDCF4.ForeColor = System.Drawing.Color.Black;
            this.btnDCF4.Location = new System.Drawing.Point(248, 91);
            this.btnDCF4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDCF4.Name = "btnDCF4";
            this.btnDCF4.Size = new System.Drawing.Size(236, 76);
            this.btnDCF4.TabIndex = 3;
            this.btnDCF4.TabStop = false;
            this.btnDCF4.Tag = "3";
            this.btnDCF4.Text = "[F4]";
            this.btnDCF4.UseVisualStyleBackColor = true;
            this.btnDCF4.Click += new System.EventHandler(this.btnDC_Click);
            // 
            // btnDCF3
            // 
            this.btnDCF3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDCF3.ForeColor = System.Drawing.Color.Black;
            this.btnDCF3.Location = new System.Drawing.Point(4, 91);
            this.btnDCF3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDCF3.Name = "btnDCF3";
            this.btnDCF3.Size = new System.Drawing.Size(236, 76);
            this.btnDCF3.TabIndex = 2;
            this.btnDCF3.TabStop = false;
            this.btnDCF3.Tag = "2";
            this.btnDCF3.Text = "[F3]";
            this.btnDCF3.UseVisualStyleBackColor = true;
            this.btnDCF3.Click += new System.EventHandler(this.btnDC_Click);
            // 
            // btnDCF2
            // 
            this.btnDCF2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDCF2.ForeColor = System.Drawing.Color.Black;
            this.btnDCF2.Location = new System.Drawing.Point(248, 5);
            this.btnDCF2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDCF2.Name = "btnDCF2";
            this.btnDCF2.Size = new System.Drawing.Size(236, 76);
            this.btnDCF2.TabIndex = 1;
            this.btnDCF2.TabStop = false;
            this.btnDCF2.Tag = "1";
            this.btnDCF2.Text = "[F2]";
            this.btnDCF2.UseVisualStyleBackColor = true;
            this.btnDCF2.Click += new System.EventHandler(this.btnDC_Click);
            // 
            // gbox_outcar
            // 
            this.gbox_outcar.Controls.Add(this.layout_outcar_inner);
            this.gbox_outcar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbox_outcar.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_outcar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(133)))), ((int)(((byte)(0)))));
            this.gbox_outcar.Location = new System.Drawing.Point(13, 18);
            this.gbox_outcar.Margin = new System.Windows.Forms.Padding(13, 18, 13, 18);
            this.gbox_outcar.Name = "gbox_outcar";
            this.gbox_outcar.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbox_outcar.Size = new System.Drawing.Size(684, 807);
            this.gbox_outcar.TabIndex = 1;
            this.gbox_outcar.TabStop = false;
            this.gbox_outcar.Text = "출차 정보";
            // 
            // layout_outcar_inner
            // 
            this.layout_outcar_inner.ColumnCount = 1;
            this.layout_outcar_inner.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout_outcar_inner.Controls.Add(this.picOutCarImg, 0, 0);
            this.layout_outcar_inner.Controls.Add(this.listview_outcar_info, 0, 3);
            this.layout_outcar_inner.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.layout_outcar_inner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layout_outcar_inner.Location = new System.Drawing.Point(4, 33);
            this.layout_outcar_inner.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.layout_outcar_inner.Name = "layout_outcar_inner";
            this.layout_outcar_inner.RowCount = 4;
            this.layout_outcar_inner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 57.42574F));
            this.layout_outcar_inner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.layout_outcar_inner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.layout_outcar_inner.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 42.57426F));
            this.layout_outcar_inner.Size = new System.Drawing.Size(676, 769);
            this.layout_outcar_inner.TabIndex = 0;
            // 
            // picOutCarImg
            // 
            this.picOutCarImg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picOutCarImg.Location = new System.Drawing.Point(4, 5);
            this.picOutCarImg.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.picOutCarImg.Name = "picOutCarImg";
            this.picOutCarImg.Size = new System.Drawing.Size(668, 401);
            this.picOutCarImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picOutCarImg.TabIndex = 0;
            this.picOutCarImg.TabStop = false;
            // 
            // listview_outcar_info
            // 
            this.listview_outcar_info.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listview_outcar_info.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.listview_outcar_info.FullRowSelect = true;
            this.listview_outcar_info.GridLines = true;
            this.listview_outcar_info.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listview_outcar_info.HideSelection = false;
            this.listview_outcar_info.Location = new System.Drawing.Point(4, 468);
            this.listview_outcar_info.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.listview_outcar_info.MultiSelect = false;
            this.listview_outcar_info.Name = "listview_outcar_info";
            this.listview_outcar_info.Size = new System.Drawing.Size(668, 296);
            this.listview_outcar_info.TabIndex = 1;
            this.listview_outcar_info.TabStop = false;
            this.listview_outcar_info.UseCompatibleStateImageBehavior = false;
            this.listview_outcar_info.View = System.Windows.Forms.View.Details;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel1.Controls.Add(this.lblOutCarNum, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblOutCarDate, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(4, 416);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(668, 32);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // lblOutCarNum
            // 
            this.lblOutCarNum.AutoSize = true;
            this.lblOutCarNum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblOutCarNum.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblOutCarNum.ForeColor = System.Drawing.Color.White;
            this.lblOutCarNum.Location = new System.Drawing.Point(371, 1);
            this.lblOutCarNum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblOutCarNum.Name = "lblOutCarNum";
            this.lblOutCarNum.Size = new System.Drawing.Size(292, 30);
            this.lblOutCarNum.TabIndex = 1;
            this.lblOutCarNum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblOutCarDate
            // 
            this.lblOutCarDate.AutoSize = true;
            this.lblOutCarDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblOutCarDate.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblOutCarDate.ForeColor = System.Drawing.Color.White;
            this.lblOutCarDate.Location = new System.Drawing.Point(4, 4);
            this.lblOutCarDate.Margin = new System.Windows.Forms.Padding(3);
            this.lblOutCarDate.Name = "lblOutCarDate";
            this.lblOutCarDate.Size = new System.Drawing.Size(359, 24);
            this.lblOutCarDate.TabIndex = 0;
            this.lblOutCarDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnDCF1
            // 
            this.btnDCF1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDCF1.ForeColor = System.Drawing.Color.Black;
            this.btnDCF1.Location = new System.Drawing.Point(4, 5);
            this.btnDCF1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDCF1.Name = "btnDCF1";
            this.btnDCF1.Size = new System.Drawing.Size(236, 76);
            this.btnDCF1.TabIndex = 0;
            this.btnDCF1.TabStop = false;
            this.btnDCF1.Tag = "0";
            this.btnDCF1.Text = "[F1]";
            this.btnDCF1.UseVisualStyleBackColor = true;
            this.btnDCF1.Click += new System.EventHandler(this.btnDC_Click);
            // 
            // layout_main
            // 
            this.layout_main.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.layout_main.ColumnCount = 2;
            this.layout_main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.layout_main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55F));
            this.layout_main.Controls.Add(this.layout_outcar, 0, 0);
            this.layout_main.Controls.Add(this.layout_toll_main, 1, 0);
            this.layout_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layout_main.Location = new System.Drawing.Point(0, 0);
            this.layout_main.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.layout_main.Name = "layout_main";
            this.layout_main.RowCount = 1;
            this.layout_main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout_main.Size = new System.Drawing.Size(1597, 853);
            this.layout_main.TabIndex = 1;
            // 
            // layout_outcar
            // 
            this.layout_outcar.ColumnCount = 1;
            this.layout_outcar.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout_outcar.Controls.Add(this.gbox_outcar, 0, 0);
            this.layout_outcar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layout_outcar.Location = new System.Drawing.Point(4, 5);
            this.layout_outcar.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.layout_outcar.Name = "layout_outcar";
            this.layout_outcar.RowCount = 1;
            this.layout_outcar.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.layout_outcar.Size = new System.Drawing.Size(710, 843);
            this.layout_outcar.TabIndex = 0;
            // 
            // layout_toll_main
            // 
            this.layout_toll_main.ColumnCount = 2;
            this.layout_toll_main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.layout_toll_main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.layout_toll_main.Controls.Add(this.gbox_actions, 0, 1);
            this.layout_toll_main.Controls.Add(this.gbox_parking_price, 0, 0);
            this.layout_toll_main.Controls.Add(this.gbox_discount, 0, 1);
            this.layout_toll_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layout_toll_main.Location = new System.Drawing.Point(722, 5);
            this.layout_toll_main.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.layout_toll_main.Name = "layout_toll_main";
            this.layout_toll_main.RowCount = 2;
            this.layout_toll_main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.layout_toll_main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.layout_toll_main.Size = new System.Drawing.Size(871, 843);
            this.layout_toll_main.TabIndex = 1;
            // 
            // gbox_actions
            // 
            this.gbox_actions.Controls.Add(this.tableLayoutPanel3);
            this.gbox_actions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbox_actions.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_actions.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(236)))), ((int)(((byte)(219)))));
            this.gbox_actions.Location = new System.Drawing.Point(535, 355);
            this.gbox_actions.Margin = new System.Windows.Forms.Padding(13, 18, 13, 18);
            this.gbox_actions.Name = "gbox_actions";
            this.gbox_actions.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbox_actions.Size = new System.Drawing.Size(323, 470);
            this.gbox_actions.TabIndex = 3;
            this.gbox_actions.TabStop = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel3.Controls.Add(this.btnCancel, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.btnReceptPrint, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.btnCard, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.btnCash, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(4, 31);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(315, 434);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(77)))), ((int)(((byte)(54)))));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(4, 329);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(307, 100);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "  취소 [ESC]";
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnReceptPrint
            // 
            this.btnReceptPrint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(134)))), ((int)(((byte)(128)))));
            this.btnReceptPrint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnReceptPrint.FlatAppearance.BorderSize = 0;
            this.btnReceptPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReceptPrint.ForeColor = System.Drawing.Color.White;
            this.btnReceptPrint.Location = new System.Drawing.Point(4, 221);
            this.btnReceptPrint.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnReceptPrint.Name = "btnReceptPrint";
            this.btnReceptPrint.Size = new System.Drawing.Size(307, 98);
            this.btnReceptPrint.TabIndex = 2;
            this.btnReceptPrint.Text = "  영수증 출력 [F12]";
            this.btnReceptPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnReceptPrint.UseVisualStyleBackColor = false;
            // 
            // btnCard
            // 
            this.btnCard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(134)))), ((int)(((byte)(128)))));
            this.btnCard.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnCard.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCard.FlatAppearance.BorderSize = 0;
            this.btnCard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCard.ForeColor = System.Drawing.Color.White;
            this.btnCard.Location = new System.Drawing.Point(4, 113);
            this.btnCard.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCard.Name = "btnCard";
            this.btnCard.Size = new System.Drawing.Size(307, 98);
            this.btnCard.TabIndex = 1;
            this.btnCard.Text = "  카드계산";
            this.btnCard.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCard.UseVisualStyleBackColor = false;
            this.btnCard.Click += new System.EventHandler(this.btnCard_Click);
            // 
            // btnCash
            // 
            this.btnCash.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(134)))), ((int)(((byte)(128)))));
            this.btnCash.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnCash.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCash.FlatAppearance.BorderSize = 0;
            this.btnCash.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCash.ForeColor = System.Drawing.Color.White;
            this.btnCash.Location = new System.Drawing.Point(4, 5);
            this.btnCash.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnCash.Name = "btnCash";
            this.btnCash.Size = new System.Drawing.Size(307, 98);
            this.btnCash.TabIndex = 0;
            this.btnCash.Text = "  현금계산 [Enter]";
            this.btnCash.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCash.UseVisualStyleBackColor = false;
            this.btnCash.Click += new System.EventHandler(this.btnCash_Click);
            // 
            // gbox_parking_price
            // 
            this.layout_toll_main.SetColumnSpan(this.gbox_parking_price, 2);
            this.gbox_parking_price.Controls.Add(this.layout_parking_price);
            this.gbox_parking_price.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbox_parking_price.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_parking_price.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(236)))), ((int)(((byte)(219)))));
            this.gbox_parking_price.Location = new System.Drawing.Point(13, 18);
            this.gbox_parking_price.Margin = new System.Windows.Forms.Padding(13, 18, 13, 18);
            this.gbox_parking_price.Name = "gbox_parking_price";
            this.gbox_parking_price.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbox_parking_price.Size = new System.Drawing.Size(845, 301);
            this.gbox_parking_price.TabIndex = 1;
            this.gbox_parking_price.TabStop = false;
            this.gbox_parking_price.Text = "주차 요금";
            // 
            // layout_parking_price
            // 
            this.layout_parking_price.ColumnCount = 2;
            this.layout_parking_price.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.layout_parking_price.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.layout_parking_price.Controls.Add(this.lblParkingFee, 0, 0);
            this.layout_parking_price.Controls.Add(this.listview_parking_price_detail, 1, 0);
            this.layout_parking_price.Controls.Add(this.layout_parking_time, 0, 1);
            this.layout_parking_price.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layout_parking_price.Location = new System.Drawing.Point(4, 31);
            this.layout_parking_price.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.layout_parking_price.Name = "layout_parking_price";
            this.layout_parking_price.RowCount = 2;
            this.layout_parking_price.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout_parking_price.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.layout_parking_price.Size = new System.Drawing.Size(837, 265);
            this.layout_parking_price.TabIndex = 0;
            // 
            // lblParkingFee
            // 
            this.lblParkingFee.AutoSize = true;
            this.lblParkingFee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblParkingFee.Font = new System.Drawing.Font("맑은 고딕", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblParkingFee.ForeColor = System.Drawing.Color.White;
            this.lblParkingFee.Location = new System.Drawing.Point(4, 0);
            this.lblParkingFee.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblParkingFee.Name = "lblParkingFee";
            this.lblParkingFee.Size = new System.Drawing.Size(494, 223);
            this.lblParkingFee.TabIndex = 0;
            this.lblParkingFee.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listview_parking_price_detail
            // 
            this.listview_parking_price_detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listview_parking_price_detail.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.listview_parking_price_detail.FullRowSelect = true;
            this.listview_parking_price_detail.GridLines = true;
            this.listview_parking_price_detail.HideSelection = false;
            this.listview_parking_price_detail.Location = new System.Drawing.Point(506, 0);
            this.listview_parking_price_detail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 5);
            this.listview_parking_price_detail.MultiSelect = false;
            this.listview_parking_price_detail.Name = "listview_parking_price_detail";
            this.layout_parking_price.SetRowSpan(this.listview_parking_price_detail, 2);
            this.listview_parking_price_detail.Size = new System.Drawing.Size(327, 260);
            this.listview_parking_price_detail.TabIndex = 1;
            this.listview_parking_price_detail.TabStop = false;
            this.listview_parking_price_detail.UseCompatibleStateImageBehavior = false;
            this.listview_parking_price_detail.View = System.Windows.Forms.View.Details;
            // 
            // layout_parking_time
            // 
            this.layout_parking_time.ColumnCount = 3;
            this.layout_parking_time.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.layout_parking_time.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.layout_parking_time.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.layout_parking_time.Controls.Add(this.label4, 2, 0);
            this.layout_parking_time.Controls.Add(this.lblParkingTime, 1, 0);
            this.layout_parking_time.Controls.Add(this.label2, 0, 0);
            this.layout_parking_time.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layout_parking_time.Location = new System.Drawing.Point(4, 228);
            this.layout_parking_time.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.layout_parking_time.Name = "layout_parking_time";
            this.layout_parking_time.RowCount = 1;
            this.layout_parking_time.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout_parking_time.Size = new System.Drawing.Size(494, 32);
            this.layout_parking_time.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label4.Location = new System.Drawing.Point(398, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 32);
            this.label4.TabIndex = 2;
            this.label4.Text = "분";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblParkingTime
            // 
            this.lblParkingTime.AutoSize = true;
            this.lblParkingTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblParkingTime.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblParkingTime.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblParkingTime.Location = new System.Drawing.Point(201, 0);
            this.lblParkingTime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblParkingTime.Name = "lblParkingTime";
            this.lblParkingTime.Size = new System.Drawing.Size(189, 32);
            this.lblParkingTime.TabIndex = 1;
            this.lblParkingTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label2.Location = new System.Drawing.Point(4, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(189, 32);
            this.label2.TabIndex = 0;
            this.label2.Text = "주차시간 :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gbox_discount
            // 
            this.gbox_discount.Controls.Add(this.tableLayoutPanel2);
            this.gbox_discount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbox_discount.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_discount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(236)))), ((int)(((byte)(219)))));
            this.gbox_discount.Location = new System.Drawing.Point(13, 355);
            this.gbox_discount.Margin = new System.Windows.Forms.Padding(13, 18, 13, 18);
            this.gbox_discount.Name = "gbox_discount";
            this.gbox_discount.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gbox_discount.Size = new System.Drawing.Size(496, 470);
            this.gbox_discount.TabIndex = 2;
            this.gbox_discount.TabStop = false;
            this.gbox_discount.Text = "할인";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.btnDCF10, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.btnDCF9, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.btnDCF8, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.btnDCF7, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.btnDCF6, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.btnDCF5, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.btnDCF4, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnDCF3, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnDCF2, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnDCF1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(4, 31);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(488, 434);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // frmParkingPay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1597, 853);
            this.Controls.Add(this.layout_main);
            this.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.Name = "frmParkingPay";
            this.Text = "frmParkPay";
            this.gbox_outcar.ResumeLayout(false);
            this.layout_outcar_inner.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picOutCarImg)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.layout_main.ResumeLayout(false);
            this.layout_outcar.ResumeLayout(false);
            this.layout_toll_main.ResumeLayout(false);
            this.gbox_actions.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.gbox_parking_price.ResumeLayout(false);
            this.layout_parking_price.ResumeLayout(false);
            this.layout_parking_price.PerformLayout();
            this.layout_parking_time.ResumeLayout(false);
            this.layout_parking_time.PerformLayout();
            this.gbox_discount.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDCF10;
        private System.Windows.Forms.Button btnDCF9;
        private System.Windows.Forms.Button btnDCF8;
        private System.Windows.Forms.Button btnDCF7;
        private System.Windows.Forms.Button btnDCF6;
        private System.Windows.Forms.Button btnDCF5;
        private System.Windows.Forms.Button btnDCF4;
        private System.Windows.Forms.Button btnDCF3;
        private System.Windows.Forms.Button btnDCF2;
        private System.Windows.Forms.GroupBox gbox_outcar;
        private System.Windows.Forms.TableLayoutPanel layout_outcar_inner;
        private System.Windows.Forms.PictureBox picOutCarImg;
        private System.Windows.Forms.ListView listview_outcar_info;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblOutCarNum;
        private System.Windows.Forms.Label lblOutCarDate;
        private System.Windows.Forms.Button btnDCF1;
        private System.Windows.Forms.TableLayoutPanel layout_main;
        private System.Windows.Forms.TableLayoutPanel layout_outcar;
        private System.Windows.Forms.TableLayoutPanel layout_toll_main;
        private System.Windows.Forms.GroupBox gbox_actions;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnReceptPrint;
        private System.Windows.Forms.Button btnCard;
        private System.Windows.Forms.Button btnCash;
        private System.Windows.Forms.GroupBox gbox_parking_price;
        private System.Windows.Forms.TableLayoutPanel layout_parking_price;
        private System.Windows.Forms.Label lblParkingFee;
        private System.Windows.Forms.ListView listview_parking_price_detail;
        private System.Windows.Forms.TableLayoutPanel layout_parking_time;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblParkingTime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox gbox_discount;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
    }
}