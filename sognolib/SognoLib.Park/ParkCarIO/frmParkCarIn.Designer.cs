﻿namespace SognoLib.Park.ParkCarIO
{
    partial class frmParkCarIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblGateName = new System.Windows.Forms.Label();
            this.label_extrainfo_title = new System.Windows.Forms.Label();
            this.label_carnumber_title = new System.Windows.Forms.Label();
            this.txtCarNum = new System.Windows.Forms.TextBox();
            this.chkPrintVisit = new System.Windows.Forms.CheckBox();
            this.layout_inputs1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtMemo = new System.Windows.Forms.TextBox();
            this.btnCarInDeny = new System.Windows.Forms.Button();
            this.btnCarInAccept = new System.Windows.Forms.Button();
            this.layout_inputs2 = new System.Windows.Forms.TableLayoutPanel();
            this.layout_inputs = new System.Windows.Forms.TableLayoutPanel();
            this.label_custinfo = new System.Windows.Forms.Label();
            this.label_indate = new System.Windows.Forms.Label();
            this.layout_carnumber = new System.Windows.Forms.TableLayoutPanel();
            this.picCarIn = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.layout_main1 = new System.Windows.Forms.TableLayoutPanel();
            this.layout_main = new System.Windows.Forms.TableLayoutPanel();
            this.layout_inputs1.SuspendLayout();
            this.layout_inputs2.SuspendLayout();
            this.layout_inputs.SuspendLayout();
            this.layout_carnumber.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCarIn)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.layout_main1.SuspendLayout();
            this.layout_main.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblGateName
            // 
            this.lblGateName.AutoSize = true;
            this.lblGateName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGateName.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblGateName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(236)))), ((int)(((byte)(219)))));
            this.lblGateName.Location = new System.Drawing.Point(3, 0);
            this.lblGateName.Name = "lblGateName";
            this.lblGateName.Size = new System.Drawing.Size(638, 35);
            this.lblGateName.TabIndex = 0;
            this.lblGateName.Text = "입차 차량";
            this.lblGateName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_extrainfo_title
            // 
            this.label_extrainfo_title.AutoSize = true;
            this.label_extrainfo_title.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_extrainfo_title.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label_extrainfo_title.Location = new System.Drawing.Point(3, 37);
            this.label_extrainfo_title.Name = "label_extrainfo_title";
            this.label_extrainfo_title.Size = new System.Drawing.Size(103, 37);
            this.label_extrainfo_title.TabIndex = 1;
            this.label_extrainfo_title.Text = "비     고:";
            this.label_extrainfo_title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_carnumber_title
            // 
            this.label_carnumber_title.AutoSize = true;
            this.label_carnumber_title.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_carnumber_title.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label_carnumber_title.Location = new System.Drawing.Point(3, 0);
            this.label_carnumber_title.Name = "label_carnumber_title";
            this.label_carnumber_title.Size = new System.Drawing.Size(103, 37);
            this.label_carnumber_title.TabIndex = 0;
            this.label_carnumber_title.Text = "차량번호:";
            this.label_carnumber_title.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCarNum
            // 
            this.txtCarNum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCarNum.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtCarNum.Location = new System.Drawing.Point(112, 3);
            this.txtCarNum.Name = "txtCarNum";
            this.txtCarNum.Size = new System.Drawing.Size(198, 30);
            this.txtCarNum.TabIndex = 2;
            this.txtCarNum.TabStop = false;
            // 
            // chkPrintVisit
            // 
            this.chkPrintVisit.AutoSize = true;
            this.layout_inputs1.SetColumnSpan(this.chkPrintVisit, 2);
            this.chkPrintVisit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkPrintVisit.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chkPrintVisit.Location = new System.Drawing.Point(10, 79);
            this.chkPrintVisit.Margin = new System.Windows.Forms.Padding(10, 5, 3, 3);
            this.chkPrintVisit.Name = "chkPrintVisit";
            this.chkPrintVisit.Size = new System.Drawing.Size(300, 31);
            this.chkPrintVisit.TabIndex = 4;
            this.chkPrintVisit.Text = "방문증 출력";
            this.chkPrintVisit.UseVisualStyleBackColor = true;
            // 
            // layout_inputs1
            // 
            this.layout_inputs1.ColumnCount = 2;
            this.layout_inputs1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.layout_inputs1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.layout_inputs1.Controls.Add(this.txtMemo, 1, 1);
            this.layout_inputs1.Controls.Add(this.label_extrainfo_title, 0, 1);
            this.layout_inputs1.Controls.Add(this.label_carnumber_title, 0, 0);
            this.layout_inputs1.Controls.Add(this.txtCarNum, 1, 0);
            this.layout_inputs1.Controls.Add(this.chkPrintVisit, 0, 2);
            this.layout_inputs1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layout_inputs1.Location = new System.Drawing.Point(3, 3);
            this.layout_inputs1.Name = "layout_inputs1";
            this.layout_inputs1.RowCount = 3;
            this.layout_inputs1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.layout_inputs1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.layout_inputs1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.layout_inputs1.Size = new System.Drawing.Size(313, 113);
            this.layout_inputs1.TabIndex = 0;
            // 
            // txtMemo
            // 
            this.txtMemo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMemo.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.txtMemo.Location = new System.Drawing.Point(112, 40);
            this.txtMemo.Name = "txtMemo";
            this.txtMemo.Size = new System.Drawing.Size(198, 30);
            this.txtMemo.TabIndex = 3;
            // 
            // btnCarInDeny
            // 
            this.btnCarInDeny.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(77)))), ((int)(((byte)(54)))));
            this.btnCarInDeny.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCarInDeny.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCarInDeny.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCarInDeny.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCarInDeny.ForeColor = System.Drawing.Color.White;
            this.btnCarInDeny.Location = new System.Drawing.Point(160, 8);
            this.btnCarInDeny.Margin = new System.Windows.Forms.Padding(4, 8, 8, 8);
            this.btnCarInDeny.Name = "btnCarInDeny";
            this.btnCarInDeny.Size = new System.Drawing.Size(145, 97);
            this.btnCarInDeny.TabIndex = 1;
            this.btnCarInDeny.Text = "거  부";
            this.btnCarInDeny.UseVisualStyleBackColor = false;
            this.btnCarInDeny.Click += new System.EventHandler(this.btnCarInDeny_Click);
            // 
            // btnCarInAccept
            // 
            this.btnCarInAccept.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(4)))), ((int)(((byte)(134)))), ((int)(((byte)(128)))));
            this.btnCarInAccept.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnCarInAccept.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCarInAccept.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCarInAccept.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCarInAccept.ForeColor = System.Drawing.Color.White;
            this.btnCarInAccept.Location = new System.Drawing.Point(8, 8);
            this.btnCarInAccept.Margin = new System.Windows.Forms.Padding(8, 8, 4, 8);
            this.btnCarInAccept.Name = "btnCarInAccept";
            this.btnCarInAccept.Size = new System.Drawing.Size(144, 97);
            this.btnCarInAccept.TabIndex = 0;
            this.btnCarInAccept.Text = "입  차";
            this.btnCarInAccept.UseVisualStyleBackColor = false;
            this.btnCarInAccept.Click += new System.EventHandler(this.btnCarInAccept_Click);
            // 
            // layout_inputs2
            // 
            this.layout_inputs2.ColumnCount = 2;
            this.layout_inputs2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layout_inputs2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layout_inputs2.Controls.Add(this.btnCarInDeny, 1, 0);
            this.layout_inputs2.Controls.Add(this.btnCarInAccept, 0, 0);
            this.layout_inputs2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layout_inputs2.Location = new System.Drawing.Point(322, 3);
            this.layout_inputs2.Name = "layout_inputs2";
            this.layout_inputs2.RowCount = 1;
            this.layout_inputs2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layout_inputs2.Size = new System.Drawing.Size(313, 113);
            this.layout_inputs2.TabIndex = 1;
            // 
            // layout_inputs
            // 
            this.layout_inputs.BackColor = System.Drawing.Color.WhiteSmoke;
            this.layout_inputs.ColumnCount = 2;
            this.layout_inputs.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layout_inputs.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layout_inputs.Controls.Add(this.layout_inputs1, 0, 0);
            this.layout_inputs.Controls.Add(this.layout_inputs2, 1, 0);
            this.layout_inputs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layout_inputs.Location = new System.Drawing.Point(3, 516);
            this.layout_inputs.Name = "layout_inputs";
            this.layout_inputs.RowCount = 1;
            this.layout_inputs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout_inputs.Size = new System.Drawing.Size(638, 119);
            this.layout_inputs.TabIndex = 2;
            // 
            // label_custinfo
            // 
            this.label_custinfo.AutoSize = true;
            this.label_custinfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_custinfo.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label_custinfo.ForeColor = System.Drawing.Color.White;
            this.label_custinfo.Location = new System.Drawing.Point(286, 0);
            this.label_custinfo.Name = "label_custinfo";
            this.label_custinfo.Size = new System.Drawing.Size(341, 29);
            this.label_custinfo.TabIndex = 1;
            this.label_custinfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_indate
            // 
            this.label_indate.AutoSize = true;
            this.label_indate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_indate.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label_indate.ForeColor = System.Drawing.Color.White;
            this.label_indate.Location = new System.Drawing.Point(3, 0);
            this.label_indate.Name = "label_indate";
            this.label_indate.Size = new System.Drawing.Size(277, 29);
            this.label_indate.TabIndex = 0;
            this.label_indate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // layout_carnumber
            // 
            this.layout_carnumber.ColumnCount = 2;
            this.layout_carnumber.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.layout_carnumber.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55F));
            this.layout_carnumber.Controls.Add(this.label_custinfo, 1, 0);
            this.layout_carnumber.Controls.Add(this.label_indate, 0, 0);
            this.layout_carnumber.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layout_carnumber.Location = new System.Drawing.Point(4, 439);
            this.layout_carnumber.Name = "layout_carnumber";
            this.layout_carnumber.RowCount = 1;
            this.layout_carnumber.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout_carnumber.Size = new System.Drawing.Size(630, 29);
            this.layout_carnumber.TabIndex = 5;
            // 
            // picCarIn
            // 
            this.picCarIn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picCarIn.Location = new System.Drawing.Point(4, 4);
            this.picCarIn.Name = "picCarIn";
            this.picCarIn.Size = new System.Drawing.Size(630, 428);
            this.picCarIn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picCarIn.TabIndex = 6;
            this.picCarIn.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.layout_carnumber, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.picCarIn, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 38);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(638, 472);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // layout_main1
            // 
            this.layout_main1.BackColor = System.Drawing.Color.Transparent;
            this.layout_main1.ColumnCount = 1;
            this.layout_main1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout_main1.Controls.Add(this.lblGateName, 0, 0);
            this.layout_main1.Controls.Add(this.layout_inputs, 0, 2);
            this.layout_main1.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.layout_main1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layout_main1.Location = new System.Drawing.Point(8, 8);
            this.layout_main1.Name = "layout_main1";
            this.layout_main1.RowCount = 3;
            this.layout_main1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.layout_main1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layout_main1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.layout_main1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.layout_main1.Size = new System.Drawing.Size(644, 638);
            this.layout_main1.TabIndex = 0;
            // 
            // layout_main
            // 
            this.layout_main.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.layout_main.ColumnCount = 1;
            this.layout_main.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layout_main.Controls.Add(this.layout_main1, 0, 0);
            this.layout_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layout_main.Location = new System.Drawing.Point(0, 0);
            this.layout_main.Name = "layout_main";
            this.layout_main.Padding = new System.Windows.Forms.Padding(5);
            this.layout_main.RowCount = 1;
            this.layout_main.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layout_main.Size = new System.Drawing.Size(660, 654);
            this.layout_main.TabIndex = 1;
            // 
            // frmParkCarIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 654);
            this.Controls.Add(this.layout_main);
            this.Font = new System.Drawing.Font("맑은 고딕", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.Name = "frmParkCarIn";
            this.Text = "차량입차확인";
            this.layout_inputs1.ResumeLayout(false);
            this.layout_inputs1.PerformLayout();
            this.layout_inputs2.ResumeLayout(false);
            this.layout_inputs.ResumeLayout(false);
            this.layout_carnumber.ResumeLayout(false);
            this.layout_carnumber.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picCarIn)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.layout_main1.ResumeLayout(false);
            this.layout_main1.PerformLayout();
            this.layout_main.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblGateName;
        private System.Windows.Forms.Label label_extrainfo_title;
        private System.Windows.Forms.Label label_carnumber_title;
        private System.Windows.Forms.TextBox txtCarNum;
        private System.Windows.Forms.CheckBox chkPrintVisit;
        private System.Windows.Forms.TableLayoutPanel layout_inputs1;
        private System.Windows.Forms.TextBox txtMemo;
        private System.Windows.Forms.Button btnCarInDeny;
        private System.Windows.Forms.Button btnCarInAccept;
        private System.Windows.Forms.TableLayoutPanel layout_inputs2;
        private System.Windows.Forms.TableLayoutPanel layout_inputs;
        private System.Windows.Forms.Label label_custinfo;
        private System.Windows.Forms.Label label_indate;
        private System.Windows.Forms.TableLayoutPanel layout_carnumber;
        private System.Windows.Forms.PictureBox picCarIn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel layout_main1;
        private System.Windows.Forms.TableLayoutPanel layout_main;
    }
}