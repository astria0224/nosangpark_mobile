﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SognoLib.Park.ParkCarIO
{
    public partial class frmInCarSearch : Form
    {
        private SMsgInCarSearchPopup sMsgInCarSearchPopup;

        public delegate void evtInCarSearch(SMsgInCarSearchPopup sMsgInCarSearchPopup);
        public event evtInCarSearch AcceptEvent;

        private CarIODBManager carIODBManager = null;

        public frmInCarSearch(string GateName, CarIODBManager carIODBManager)
        {
            InitializeComponent();

            this.lblGateName.Text = GateName;
            this.carIODBManager = carIODBManager;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            /// 리스트 초기화
            dgvInCar.Rows.Clear(); picInCar.ImageLocation = "";

            /// 입차 중인 차량 검색
            CarIODataRecord[] carIODataRecords = carIODBManager.GetNoOutCarLikeData(0, txtSearchCarNum.Text);
            for (int i = 0; i < carIODataRecords.Length; i++)
            {
                DataGridViewRow NewRow = (DataGridViewRow)dgvInCar.Rows[0].Clone();

                NewRow.Cells[0].Value = carIODataRecords[i].TKNo;
                NewRow.Cells[1].Value = carIODataRecords[i].InImage1;
                NewRow.Cells[2].Value = carIODataRecords[i].InCarNum1;
                NewRow.Cells[3].Value = carIODataRecords[i].InDate;

                dgvInCar.Rows.Add(NewRow);

                if (i == 0) picInCar.ImageLocation = carIODataRecords[i].InImage1;
            }
        }

        public void SetInCarSearchData(SMsgInCarSearchPopup sMsgInCarSearchPopup)
        {
            dgvInCar.Rows.Clear();
            this.sMsgInCarSearchPopup = sMsgInCarSearchPopup;

            picOutCar.ImageLocation = sMsgInCarSearchPopup.ImgPath;
            txtSearchCarNum.Text = sMsgInCarSearchPopup.CarNum;

            if (!this.Visible)
                this.ShowDialog();
        }

        private void dgvInCar_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DataGridViewRow CarInRow = dgvInCar.SelectedRows[0];
                picInCar.ImageLocation = CarInRow.Cells[1].Value.ToString() == "" ? "" : CarInRow.Cells[1].Value.ToString();
            }
            catch { }
        }

        private void btn15m_Click(object sender, EventArgs e)
        {
            sMsgInCarSearchPopup.eInCarSearchPopupType = EInCarSearchPopupType.SetTime;
            sMsgInCarSearchPopup.Time = 15;

            AcceptEvent?.Invoke(sMsgInCarSearchPopup);
        }

        private void btn30m_Click(object sender, EventArgs e)
        {
            sMsgInCarSearchPopup.eInCarSearchPopupType = EInCarSearchPopupType.SetTime;
            sMsgInCarSearchPopup.Time = 30;

            AcceptEvent?.Invoke(sMsgInCarSearchPopup);
        }

        private void btn60m_Click(object sender, EventArgs e)
        {
            sMsgInCarSearchPopup.eInCarSearchPopupType = EInCarSearchPopupType.SetTime;
            sMsgInCarSearchPopup.Time = 60;

            AcceptEvent?.Invoke(sMsgInCarSearchPopup);
        }

        private void btn120m_Click(object sender, EventArgs e)
        {
            sMsgInCarSearchPopup.eInCarSearchPopupType = EInCarSearchPopupType.SetTime;
            sMsgInCarSearchPopup.Time = 120;

            AcceptEvent?.Invoke(sMsgInCarSearchPopup);
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            try
            {
                sMsgInCarSearchPopup.eInCarSearchPopupType = EInCarSearchPopupType.SetTime;
                sMsgInCarSearchPopup.Time = int.Parse(txtParkingTime.Text);
                AcceptEvent?.Invoke(sMsgInCarSearchPopup);
            }
            catch
            {
                MessageBox.Show("숫자를 넣어주셔야 합니다.");
            }
        }

        private void btnAccept_Click(object sender, EventArgs e)
        {
            sMsgInCarSearchPopup.eInCarSearchPopupType = EInCarSearchPopupType.Search;

            try
            {
                DataGridViewRow CarInRow = dgvInCar.SelectedRows[0];
                sMsgInCarSearchPopup.CarNum = CarInRow.Cells[2].Value.ToString();

                if (sMsgInCarSearchPopup.CarNum != "")
                {
                    AcceptEvent?.Invoke(sMsgInCarSearchPopup);
                }
            }
            catch
            {
                MessageBox.Show("차량을 선택해 주십시오.");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            sMsgInCarSearchPopup.eInCarSearchPopupType = EInCarSearchPopupType.Close;
            AcceptEvent?.Invoke(sMsgInCarSearchPopup);
        }
    }
}
