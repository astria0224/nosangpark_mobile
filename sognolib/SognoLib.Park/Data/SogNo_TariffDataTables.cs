﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SognoLib.Park.Data
{
    public enum ParkTables
    {
        tariff,
        day_tariff,
        time_tariff,
        dcinfo
    }


    // ~~~~~
    public enum TariffFields
    {
        ParkNo,
        TrfNo,
        TrfName,
        MaxRates,
        DailyMaxRates,
        DailyCriterion,
        RoundType,
        RoundDecimal,
        DayTariffs,
        HolidayTariff,
        HolidayPrior,
        DefaultDCs,
        Reserve0
    }


    public class TariffRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? TrfNo { get; set; } = null;             // 요금 체계 번호
        public string TrfName { get; set; } = null;         // 요금 체계 이름
        public int? MaxRates { get; set; } = null;          // 전체 최대 요금
        public int? DailyMaxRates { get; set; } = null;     // 일일 최대 요금
        public int? DailyCriterion { get; set; } = null;
        public int? RoundType { get; set; } = null;         // 반올림 방식, 0: 없음, 1: 올림, 2: 내름, 3: 반올림
        public int? RoundDecimal { get; set; } = null;      // 반올림 자릿수, 예: 100
        public string DayTariffs { get; set; } = null;      // 
        public int? HolidayTariff { get; set; } = null;     // 
        public string HolidayPrior { get; set; } = null;    // 
        public string DefaultDCs { get; set; } = null;      // 
        public string Reserve0 { get; set; } = null;


        // 상수
        public const int DailyCriterion_Day = 0;
        public const int DailyCriterion_InTime = 1;

        public const int RoundType_None = 0;
        public const int RoundType_Ceiling = 1;
        public const int RoundType_Floor = 2;
        public const int RoundType_Round = 3;

        public const int DayIndex_Sunday = 0;
        public const int DayIndex_Monday = 1;
        public const int DayIndex_Tuesday = 2;
        public const int DayIndex_Wednesday = 3;
        public const int DayIndex_Thursday = 4;
        public const int DayIndex_Friday = 5;
        public const int DayIndex_Saturday = 6;
    }
         

    // ~~~~~
    public enum DayTariffFields
    {
        ParkNo,
        DayTrfNo,
        TimeTariffs,
        DefaultDCs,
        Memo,
        Reserve0
    }


    public class DayTariffRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? DayTrfNo { get; set; } = null;          // 일 요금 체계 번호
        public string TimeTariffs { get; set; } = null;     //
        public string DefaultDCs { get; set; } = null;      // 
        public string Memo { get; set; } = null;
        public string Reserve0 { get; set; } = null;
    }


    // ~~~~~
    public enum TimeTariffFields
    {
        ParkNo,
        TimeTrfNo,
        StartTime,
        EndTime,
        BaseTime,
        BaseRates,
        AddTime,
        AddRates,
        DefaultDCs,
        Memo,
        Reserve0
    }


    public class TimeTariffRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? TimeTrfNo { get; set; } = null;         // 상세 요금 체계 번호
        public TimeSpan? StartTime { get; set; } = null;
        public TimeSpan? EndTime { get; set; } = null;
        public int? BaseTime { get; set; } = null;
        public int? BaseRates { get; set; } = null;
        public int? AddTime { get; set; } = null;
        public int? AddRates { get; set; } = null;
        public string DefaultDCs { get; set; } = null;
        public string Memo { get; set; } = null;
        public string Reserve0 { get; set; } = null;
    }


    // ~~~~~
    public enum DCInfoFields
    {
        ParkNo,
        DCNo,
        DCName,
        DCType,
        DCValue1,
        DCValue2,
        ButtonIndex,
        Memo,
        Reserve0
    }

    public class DCInfoRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? DCNo { get; set; } = null;
        public string DCName { get; set; } = null;
        public int? DCType { get; set; } = null;
        public string DCValue1 { get; set; } = null;
        public string DCValue2 { get; set; } = null;
        public int? ButtonIndex { get; set; } = null;
        public string Memo { get; set; } = null;
        public string Reserve0 { get; set; } = null;


        // 상수
        public const int DCType_TurningDC = 0;
        public const int DCType_TimeDC = 1;
        public const int DCType_PeriodDC_Free = 2;
        public const int DCType_PeriodDC_Percent = 3;
        public const int DCType_TimeSlotDC_Free = 4;
        public const int DCType_TimeSlotDC_Percent = 5;
        public const int DCType_PercentDC = 6;
        public const int DCType_PriceDC = 7;


        public string DiscountTypeString()
        {
            if (this.DCType == null)
                return "정보없음";
            else
                return DiscountTypeString(this.DCType.Value);
        }


        public static string DiscountTypeString(int dcType)
        {
            switch (dcType)
            {
                case DCType_TurningDC:
                    return "회차 무료";
                case DCType_TimeDC:
                    return "시간 감면";
                case DCType_PeriodDC_Free:
                    return "기간 무료";
                case DCType_PeriodDC_Percent:
                    return "기간 할인";
                case DCType_TimeSlotDC_Free:
                    return "시간대 무료";
                case DCType_TimeSlotDC_Percent:
                    return "시간대 할인";
                case DCType_PercentDC:
                    return "요금 할인";
                case DCType_PriceDC:
                    return "요금 감면";
                default:
                    return "알수없는할인";
            }
        }
    }

}
