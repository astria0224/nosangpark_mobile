﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SognoLib.Core;
using SognoLib.Park.Toll;


namespace SognoLib.Park.Data
{
    public class SogNo_TariffDataAccess
    {
        private static NLog.Logger nlog = NLog.LogManager.GetCurrentClassLogger();

        private SogNo_DBManager dbManager = null;

        
        public SogNo_TariffDataAccess(SogNo_DBManager dbManager)
        {
            this.dbManager = dbManager;
        }


        public SogNo_TariffDataAccess(string ip, int port, string db)
        {
            try
            {
                this.dbManager = new SogNo_DBManager(ip, port, "sogno", "sogno1234", db);
            }
            catch
            {
                this.dbManager = null;
                throw;
            }
        }


        public TariffRecord ReadTariff(int parkNo, int trfNo)
        {
            return Read_TariffRecord(parkNo, trfNo);
        }

        public TariffRecord[] ReadAllTariffs(int parkNo)
        {
            return ReadAll_TariffRecords(parkNo);
        }

        public int InsertTariff_WithReturn(TariffRecord trf)
        {
            if (!Insert_Tariff(trf))
                return -1;

            return LastInsertedID_Tariff(trf.ParkNo.Value);
        }

        public bool UpdateTariff(TariffRecord trf)
        {
            return Update_TariffRecord(trf);
        }

        
        public DayTariffRecord[] ReadAllDayTariffs(int parkNo)
        {
            return ReadAll_DayTariffRecords(parkNo);
        }

        public DayTariffRecord ReadDayTariff(int parkNo, int dayTrfNo)
        {
            return Read_DayTariffRecord(parkNo, dayTrfNo);
        }

        public int InsertDayTariff_WithReturn(DayTariffRecord dayTrf)
        {
            if (!Insert_DayTariff(dayTrf))
                return -1;

            return LastInsertedID_DayTariff(dayTrf.ParkNo.Value);
        }


        public TimeTariffRecord[] ReadAllTimeTariffs(int parkNo)
        {
            return ReadAll_TimeTariffRecords(parkNo);
        }

        public TimeTariffRecord ReadTimeTariff(int parkNo, int timeTrfNo)
        {
            return Read_TimeTariffRecord(parkNo, timeTrfNo);
        }

        public int InsertTimeTariff_WithReturn(TimeTariffRecord timeTrf)
        {
            if (!Insert_TimeTariffRecord(timeTrf))
                return -1;

            return LastInsertedID_TimeTariff(timeTrf.ParkNo.Value);
        }

        public bool UpdateTimeTariff(TimeTariffRecord trf)
        {
            return Update_TimeTariffRecord(trf);
        }



        public DCInfoRecord ReadDiscount(int parkNo, int dcNo)
        {
            return Read_DCInfoRecord(parkNo, dcNo);
        }

        public DCInfoRecord[] ReadAllDiscounts(int parkNo)
        {
            return ReadAll_DCInfoRecords(parkNo);
        }

        public int InsertDiscount_WithReturn(DCInfoRecord dc)
        {
            if (!Insert_Discount(dc))
                return -1;

            return LastInsertedID_Discount(dc.ParkNo.Value);
        }

        public bool UpdateDiscount(DCInfoRecord dcinfo)
        {
            return Update_DiscountRecord(dcinfo);
        }

                                    

        private TariffRecord Read_TariffRecord(int parkNo, int trfNo)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            string query = string.Format("SELECT * FROM `{0}` WHERE `{1}`=\'{2}\' AND `{3}`=\'{4}\'", ParkTables.tariff, TariffFields.ParkNo, parkNo, TariffFields.TrfNo, trfNo);

            TariffRecord[] records = this.dbManager.GetRecords<TariffRecord>(query);
            if (records != null && records.Length > 0)
                return records[0];

            return null;
        }


        private TariffRecord[] ReadAll_TariffRecords(int parkNo)
        {
            if (this.dbManager == null)
            {
                return null;
            }
            
            string query = string.Format("SELECT * FROM `{0}` WHERE `{1}`=\'{2}\'", ParkTables.tariff, TariffFields.ParkNo, parkNo);

            return this.dbManager.GetRecords<TariffRecord>(query);
        }


        private DayTariffRecord Read_DayTariffRecord(int parkNo, int dayTrfNo)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            string query = string.Format("SELECT * FROM `{0}` WHERE `{1}`=\'{2}\' AND `{3}`=\'{4}\'", ParkTables.day_tariff, DayTariffFields.ParkNo, parkNo, DayTariffFields.DayTrfNo, dayTrfNo);

            DayTariffRecord[] records = this.dbManager.GetRecords<DayTariffRecord>(query);
            if (records != null && records.Length > 0)
                return records[0];

            return null;
        }


        private DayTariffRecord[] ReadAll_DayTariffRecords(int parkNo)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            string query = string.Format("SELECT * FROM `{0}` WHERE `{1}`=\'{2}\'", ParkTables.day_tariff, TariffFields.ParkNo, parkNo);

            return this.dbManager.GetRecords<DayTariffRecord>(query);
        }


        private TimeTariffRecord Read_TimeTariffRecord(int parkNo, int timeTrfNo)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            string query = string.Format("SELECT * FROM `{0}` WHERE `{1}`=\'{2}\' AND `{3}`=\'{4}\'", ParkTables.time_tariff, TimeTariffFields.ParkNo, parkNo, TimeTariffFields.TimeTrfNo, timeTrfNo);

            TimeTariffRecord[] records = this.dbManager.GetRecords<TimeTariffRecord>(query);
            if (records != null && records.Length > 0)
                return records[0];

            return null;
        }


        private TimeTariffRecord[] ReadAll_TimeTariffRecords(int parkNo)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            string query = string.Format("SELECT * FROM `{0}` WHERE `{1}`=\'{2}\'", ParkTables.time_tariff, TariffFields.ParkNo, parkNo);

            return this.dbManager.GetRecords<TimeTariffRecord>(query);
        }


        private DCInfoRecord Read_DCInfoRecord(int parkNo, int dcNo)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            string query = string.Format("SELECT * FROM `{0}` WHERE `{1}`=\'{2}\' AND `{3}`=\'{4}\'", ParkTables.dcinfo, DCInfoFields.ParkNo, parkNo, DCInfoFields.DCNo, dcNo);

            DCInfoRecord[] records = this.dbManager.GetRecords<DCInfoRecord>(query);
            if (records != null && records.Length > 0)
                return records[0];

            return null;
        }


        private DCInfoRecord[] ReadAll_DCInfoRecords(int parkNo)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            string query = string.Format("SELECT * FROM `{0}` WHERE `{1}`=\'{2}\'", ParkTables.dcinfo, TariffFields.ParkNo, parkNo);

            return this.dbManager.GetRecords<DCInfoRecord>(query);
        }

        
        private bool Insert_TimeTariffRecord(TimeTariffRecord timeTariff)
        {
            try
            {
                this.dbManager.Insert(timeTariff, ParkTables.time_tariff.ToString(), TimeTariffFields.TimeTrfNo.ToString());
                return true;
            }
            catch
            {
                return false;
            }            
        }


        private int LastInsertedID_TimeTariff(int parkNo)
        {
            try
            {
                string query = string.Format("SELECT * FROM `{0}` WHERE `{1}`=\'{2}\' ORDER BY `{3}` DESC LIMIT 1", ParkTables.time_tariff, TimeTariffFields.ParkNo, parkNo, TimeTariffFields.TimeTrfNo);
                
                TimeTariffRecord[] records = this.dbManager.GetRecords<TimeTariffRecord>(query);

                return records[0].TimeTrfNo.Value;
            }
            catch
            {
                return -1;
            }
        }


        private bool Insert_DayTariff(DayTariffRecord dayTariff)
        {
            try
            {
                this.dbManager.Insert(dayTariff, ParkTables.day_tariff.ToString(), DayTariffFields.DayTrfNo.ToString());
                return true;
            }
            catch
            {
                return false;
            }
        }


        private int LastInsertedID_DayTariff(int parkNo)
        {
            try
            {
                string query = string.Format("SELECT * FROM `{0}` WHERE `{1}`=\'{2}\' ORDER BY `{3}` DESC LIMIT 1", ParkTables.day_tariff, DayTariffFields.ParkNo, parkNo, DayTariffFields.DayTrfNo);

                DayTariffRecord[] records = this.dbManager.GetRecords<DayTariffRecord>(query);

                return records[0].DayTrfNo.Value;
            }
            catch
            {
                return -1;
            }
        }


        private bool Insert_Tariff(TariffRecord tariff)
        {
            try
            {
                this.dbManager.Insert(tariff, ParkTables.tariff.ToString(), TariffFields.TrfNo.ToString());
                return true;
            }
            catch
            {
                return false;
            }
        }


        private int LastInsertedID_Tariff(int parkNo)
        {
            try
            {
                string query = string.Format("SELECT * FROM `{0}` WHERE `{1}`=\'{2}\' ORDER BY `{3}` DESC LIMIT 1", ParkTables.tariff, TariffFields.ParkNo, parkNo, TariffFields.TrfNo);

                TariffRecord[] records = this.dbManager.GetRecords<TariffRecord>(query);

                return records[0].TrfNo.Value;
            }
            catch
            {
                return -1;
            }
        }


        private bool Insert_Discount(DCInfoRecord dc)
        {
            try
            {
                this.dbManager.Insert(dc, ParkTables.dcinfo.ToString(), DCInfoFields.DCNo.ToString());
                return true;
            }
            catch
            {
                return false;
            }
        }


        private int LastInsertedID_Discount(int parkNo)
        {
            try
            {
                string query = string.Format("SELECT * FROM `{0}` WHERE `{1}`=\'{2}\' ORDER BY `{3}` DESC LIMIT 1", ParkTables.dcinfo, DCInfoFields.ParkNo, parkNo, DCInfoFields.DCNo);

                DCInfoRecord[] records = this.dbManager.GetRecords<DCInfoRecord>(query);

                return records[0].DCNo.Value;
            }
            catch
            {
                return -1;
            }
        }


        private bool Update_TimeTariffRecord(TimeTariffRecord timeTrf)
        {
            try
            {
                string[] whereFields = {
                    TimeTariffFields.ParkNo.ToString(),
                    TimeTariffFields.TimeTrfNo.ToString()
                };
                string[] setFields = {
                    TimeTariffFields.StartTime.ToString(),
                    TimeTariffFields.EndTime.ToString(),
                    TimeTariffFields.BaseTime.ToString(),
                    TimeTariffFields.BaseRates.ToString(),
                    TimeTariffFields.AddTime.ToString(),
                    TimeTariffFields.AddRates.ToString(),
                    TimeTariffFields.DefaultDCs.ToString(),
                    TimeTariffFields.Memo.ToString(),
                    TimeTariffFields.Reserve0.ToString()
                };

                this.dbManager.Update(timeTrf, ParkTables.time_tariff.ToString(), setFields, whereFields);
                
                return true;
            }
            catch
            {
                return false;
            }
        }


        private bool Update_DiscountRecord(DCInfoRecord dcinfo)
        {
            try
            {
                string[] whereFields = {
                    DCInfoFields.ParkNo.ToString(),
                    DCInfoFields.DCNo.ToString()
                };
                string[] setFields = {
                    DCInfoFields.DCName.ToString(),
                    DCInfoFields.DCType.ToString(),
                    DCInfoFields.DCValue1.ToString(),
                    DCInfoFields.DCValue2.ToString(),
                    DCInfoFields.ButtonIndex.ToString(),
                    DCInfoFields.Memo.ToString(),
                    DCInfoFields.Reserve0.ToString()
                };

                this.dbManager.Update(dcinfo, ParkTables.dcinfo.ToString(), setFields, whereFields);

                return true;
            }
            catch
            {
                return false;
            }
        }


        private bool Update_TariffRecord(TariffRecord trf)
        {
            try
            {
                string[] whereFields = {
                    TariffFields.ParkNo.ToString(),
                    TariffFields.TrfNo.ToString()
                };
                string[] setFields = {
                    TariffFields.TrfName.ToString(),
                    TariffFields.MaxRates.ToString(),
                    TariffFields.DailyMaxRates.ToString(),
                    TariffFields.DailyCriterion.ToString(),
                    TariffFields.RoundType.ToString(),
                    TariffFields.RoundDecimal.ToString(),
                    TariffFields.DayTariffs.ToString(),
                    TariffFields.HolidayTariff.ToString(),
                    TariffFields.HolidayPrior.ToString(),
                    TariffFields.DefaultDCs.ToString(),
                    TariffFields.Reserve0.ToString()
                };

                this.dbManager.Update(trf, ParkTables.tariff.ToString(), setFields, whereFields);

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
