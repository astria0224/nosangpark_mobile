﻿namespace SognoLib.Park.Gui
{
    partial class SogNo_frmSetTariff_Simple1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.tbxTariffName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.cbxFreeDay_Saturday = new System.Windows.Forms.CheckBox();
            this.cbxFreeDay_Sunday = new System.Windows.Forms.CheckBox();
            this.cbxFreeDay_Holiday = new System.Windows.Forms.CheckBox();
            this.label56 = new System.Windows.Forms.Label();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.label27 = new System.Windows.Forms.Label();
            this.tbxPriceDC = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.label29 = new System.Windows.Forms.Label();
            this.tbxPercentDC = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.rbtRoundDecimal_10 = new System.Windows.Forms.RadioButton();
            this.rbtRoundDecimal_100 = new System.Windows.Forms.RadioButton();
            this.label32 = new System.Windows.Forms.Label();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.rbtRoundType_None = new System.Windows.Forms.RadioButton();
            this.rbtRoundType_Floor = new System.Windows.Forms.RadioButton();
            this.rbtRoundType_Ceiling = new System.Windows.Forms.RadioButton();
            this.rbtRoundType_Round = new System.Windows.Forms.RadioButton();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.label35 = new System.Windows.Forms.Label();
            this.tbxTotalMaxRates = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.label36 = new System.Windows.Forms.Label();
            this.tbxDailyMaxRates = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.label39 = new System.Windows.Forms.Label();
            this.tbxAddRates = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.tbxAddTime = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.label42 = new System.Windows.Forms.Label();
            this.tbxBaseRates = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.tbxBaseTime = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.label49 = new System.Windows.Forms.Label();
            this.tbxTurningTime = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.rbtDailyCriterion_Date = new System.Windows.Forms.RadioButton();
            this.rbtDailyCriterion_InTime = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.btnApplyTariff = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.flowLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.flowLayoutPanel6.SuspendLayout();
            this.flowLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.flowLayoutPanel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(584, 761);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel10, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnApplyTariff, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(8, 8);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(568, 745);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Controls.Add(this.tbxTariffName, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.flowLayoutPanel11, 1, 9);
            this.tableLayoutPanel10.Controls.Add(this.label56, 0, 9);
            this.tableLayoutPanel10.Controls.Add(this.tableLayoutPanel11, 1, 11);
            this.tableLayoutPanel10.Controls.Add(this.label28, 0, 11);
            this.tableLayoutPanel10.Controls.Add(this.tableLayoutPanel12, 1, 10);
            this.tableLayoutPanel10.Controls.Add(this.label30, 0, 10);
            this.tableLayoutPanel10.Controls.Add(this.flowLayoutPanel6, 1, 8);
            this.tableLayoutPanel10.Controls.Add(this.label32, 0, 8);
            this.tableLayoutPanel10.Controls.Add(this.flowLayoutPanel7, 1, 7);
            this.tableLayoutPanel10.Controls.Add(this.label33, 0, 7);
            this.tableLayoutPanel10.Controls.Add(this.label34, 0, 6);
            this.tableLayoutPanel10.Controls.Add(this.tableLayoutPanel13, 1, 5);
            this.tableLayoutPanel10.Controls.Add(this.tableLayoutPanel14, 1, 4);
            this.tableLayoutPanel10.Controls.Add(this.label37, 0, 5);
            this.tableLayoutPanel10.Controls.Add(this.label38, 0, 4);
            this.tableLayoutPanel10.Controls.Add(this.tableLayoutPanel15, 1, 3);
            this.tableLayoutPanel10.Controls.Add(this.label41, 0, 3);
            this.tableLayoutPanel10.Controls.Add(this.tableLayoutPanel16, 1, 2);
            this.tableLayoutPanel10.Controls.Add(this.label44, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.label45, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.tableLayoutPanel17, 1, 1);
            this.tableLayoutPanel10.Controls.Add(this.flowLayoutPanel8, 1, 6);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(5, 65);
            this.tableLayoutPanel10.Margin = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 13;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(558, 595);
            this.tableLayoutPanel10.TabIndex = 9;
            // 
            // tbxTariffName
            // 
            this.tbxTariffName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxTariffName.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbxTariffName.Location = new System.Drawing.Point(158, 7);
            this.tbxTariffName.Margin = new System.Windows.Forms.Padding(6, 6, 6, 3);
            this.tbxTariffName.Name = "tbxTariffName";
            this.tbxTariffName.Size = new System.Drawing.Size(393, 33);
            this.tbxTariffName.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(4, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(144, 44);
            this.label2.TabIndex = 33;
            this.label2.Text = "요금제 이름";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.Controls.Add(this.cbxFreeDay_Saturday);
            this.flowLayoutPanel11.Controls.Add(this.cbxFreeDay_Sunday);
            this.flowLayoutPanel11.Controls.Add(this.cbxFreeDay_Holiday);
            this.flowLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel11.Location = new System.Drawing.Point(155, 377);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(399, 34);
            this.flowLayoutPanel11.TabIndex = 32;
            // 
            // cbxFreeDay_Saturday
            // 
            this.cbxFreeDay_Saturday.AutoSize = true;
            this.cbxFreeDay_Saturday.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbxFreeDay_Saturday.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cbxFreeDay_Saturday.ForeColor = System.Drawing.Color.White;
            this.cbxFreeDay_Saturday.Location = new System.Drawing.Point(3, 3);
            this.cbxFreeDay_Saturday.Name = "cbxFreeDay_Saturday";
            this.cbxFreeDay_Saturday.Size = new System.Drawing.Size(77, 25);
            this.cbxFreeDay_Saturday.TabIndex = 3;
            this.cbxFreeDay_Saturday.Text = "토요일";
            this.cbxFreeDay_Saturday.UseVisualStyleBackColor = true;
            // 
            // cbxFreeDay_Sunday
            // 
            this.cbxFreeDay_Sunday.AutoSize = true;
            this.cbxFreeDay_Sunday.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbxFreeDay_Sunday.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cbxFreeDay_Sunday.ForeColor = System.Drawing.Color.White;
            this.cbxFreeDay_Sunday.Location = new System.Drawing.Point(86, 3);
            this.cbxFreeDay_Sunday.Name = "cbxFreeDay_Sunday";
            this.cbxFreeDay_Sunday.Size = new System.Drawing.Size(77, 25);
            this.cbxFreeDay_Sunday.TabIndex = 4;
            this.cbxFreeDay_Sunday.Text = "일요일";
            this.cbxFreeDay_Sunday.UseVisualStyleBackColor = true;
            // 
            // cbxFreeDay_Holiday
            // 
            this.cbxFreeDay_Holiday.AutoSize = true;
            this.cbxFreeDay_Holiday.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbxFreeDay_Holiday.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cbxFreeDay_Holiday.ForeColor = System.Drawing.Color.White;
            this.cbxFreeDay_Holiday.Location = new System.Drawing.Point(169, 3);
            this.cbxFreeDay_Holiday.Name = "cbxFreeDay_Holiday";
            this.cbxFreeDay_Holiday.Size = new System.Drawing.Size(77, 25);
            this.cbxFreeDay_Holiday.TabIndex = 5;
            this.cbxFreeDay_Holiday.Text = "공휴일";
            this.cbxFreeDay_Holiday.UseVisualStyleBackColor = true;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label56.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label56.ForeColor = System.Drawing.Color.White;
            this.label56.Location = new System.Drawing.Point(4, 374);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(144, 40);
            this.label56.TabIndex = 31;
            this.label56.Text = "무료";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 2;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.Controls.Add(this.label27, 1, 0);
            this.tableLayoutPanel11.Controls.Add(this.tbxPriceDC, 0, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(155, 459);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(399, 34);
            this.tableLayoutPanel11.TabIndex = 29;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label27.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label27.ForeColor = System.Drawing.Color.White;
            this.label27.Location = new System.Drawing.Point(122, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(274, 34);
            this.label27.TabIndex = 10;
            this.label27.Text = "원   (0~)";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxPriceDC
            // 
            this.tbxPriceDC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxPriceDC.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbxPriceDC.Location = new System.Drawing.Point(3, 3);
            this.tbxPriceDC.Name = "tbxPriceDC";
            this.tbxPriceDC.Size = new System.Drawing.Size(113, 29);
            this.tbxPriceDC.TabIndex = 3;
            this.tbxPriceDC.Text = "0";
            this.tbxPriceDC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label28.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(4, 456);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(144, 40);
            this.label28.TabIndex = 28;
            this.label28.Text = "요금 감면";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 2;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel12.Controls.Add(this.label29, 1, 0);
            this.tableLayoutPanel12.Controls.Add(this.tbxPercentDC, 0, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(155, 418);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(399, 34);
            this.tableLayoutPanel12.TabIndex = 27;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label29.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label29.ForeColor = System.Drawing.Color.White;
            this.label29.Location = new System.Drawing.Point(122, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(274, 34);
            this.label29.TabIndex = 10;
            this.label29.Text = "%   (0~99)";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxPercentDC
            // 
            this.tbxPercentDC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxPercentDC.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbxPercentDC.Location = new System.Drawing.Point(3, 3);
            this.tbxPercentDC.Name = "tbxPercentDC";
            this.tbxPercentDC.Size = new System.Drawing.Size(113, 29);
            this.tbxPercentDC.TabIndex = 3;
            this.tbxPercentDC.Text = "0";
            this.tbxPercentDC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label30.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label30.ForeColor = System.Drawing.Color.White;
            this.label30.Location = new System.Drawing.Point(4, 415);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(144, 40);
            this.label30.TabIndex = 25;
            this.label30.Text = "요금 할인";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.Controls.Add(this.rbtRoundDecimal_10);
            this.flowLayoutPanel6.Controls.Add(this.rbtRoundDecimal_100);
            this.flowLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(155, 336);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(399, 34);
            this.flowLayoutPanel6.TabIndex = 22;
            // 
            // rbtRoundDecimal_10
            // 
            this.rbtRoundDecimal_10.AutoSize = true;
            this.rbtRoundDecimal_10.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtRoundDecimal_10.ForeColor = System.Drawing.Color.White;
            this.rbtRoundDecimal_10.Location = new System.Drawing.Point(3, 3);
            this.rbtRoundDecimal_10.Name = "rbtRoundDecimal_10";
            this.rbtRoundDecimal_10.Size = new System.Drawing.Size(68, 25);
            this.rbtRoundDecimal_10.TabIndex = 0;
            this.rbtRoundDecimal_10.Text = "10 원";
            this.rbtRoundDecimal_10.UseVisualStyleBackColor = true;
            // 
            // rbtRoundDecimal_100
            // 
            this.rbtRoundDecimal_100.AutoSize = true;
            this.rbtRoundDecimal_100.Checked = true;
            this.rbtRoundDecimal_100.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtRoundDecimal_100.ForeColor = System.Drawing.Color.White;
            this.rbtRoundDecimal_100.Location = new System.Drawing.Point(77, 3);
            this.rbtRoundDecimal_100.Name = "rbtRoundDecimal_100";
            this.rbtRoundDecimal_100.Size = new System.Drawing.Size(77, 25);
            this.rbtRoundDecimal_100.TabIndex = 1;
            this.rbtRoundDecimal_100.TabStop = true;
            this.rbtRoundDecimal_100.Text = "100 원";
            this.rbtRoundDecimal_100.UseVisualStyleBackColor = true;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label32.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label32.ForeColor = System.Drawing.Color.White;
            this.label32.Location = new System.Drawing.Point(4, 333);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(144, 40);
            this.label32.TabIndex = 21;
            this.label32.Text = "반올림 자릿수";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.Controls.Add(this.rbtRoundType_None);
            this.flowLayoutPanel7.Controls.Add(this.rbtRoundType_Floor);
            this.flowLayoutPanel7.Controls.Add(this.rbtRoundType_Ceiling);
            this.flowLayoutPanel7.Controls.Add(this.rbtRoundType_Round);
            this.flowLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(155, 295);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(399, 34);
            this.flowLayoutPanel7.TabIndex = 20;
            // 
            // rbtRoundType_None
            // 
            this.rbtRoundType_None.AutoSize = true;
            this.rbtRoundType_None.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtRoundType_None.ForeColor = System.Drawing.Color.White;
            this.rbtRoundType_None.Location = new System.Drawing.Point(3, 3);
            this.rbtRoundType_None.Name = "rbtRoundType_None";
            this.rbtRoundType_None.Size = new System.Drawing.Size(60, 25);
            this.rbtRoundType_None.TabIndex = 0;
            this.rbtRoundType_None.Text = "없음";
            this.rbtRoundType_None.UseVisualStyleBackColor = true;
            // 
            // rbtRoundType_Floor
            // 
            this.rbtRoundType_Floor.AutoSize = true;
            this.rbtRoundType_Floor.Checked = true;
            this.rbtRoundType_Floor.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtRoundType_Floor.ForeColor = System.Drawing.Color.White;
            this.rbtRoundType_Floor.Location = new System.Drawing.Point(69, 3);
            this.rbtRoundType_Floor.Name = "rbtRoundType_Floor";
            this.rbtRoundType_Floor.Size = new System.Drawing.Size(60, 25);
            this.rbtRoundType_Floor.TabIndex = 1;
            this.rbtRoundType_Floor.TabStop = true;
            this.rbtRoundType_Floor.Text = "내림";
            this.rbtRoundType_Floor.UseVisualStyleBackColor = true;
            // 
            // rbtRoundType_Ceiling
            // 
            this.rbtRoundType_Ceiling.AutoSize = true;
            this.rbtRoundType_Ceiling.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtRoundType_Ceiling.ForeColor = System.Drawing.Color.White;
            this.rbtRoundType_Ceiling.Location = new System.Drawing.Point(135, 3);
            this.rbtRoundType_Ceiling.Name = "rbtRoundType_Ceiling";
            this.rbtRoundType_Ceiling.Size = new System.Drawing.Size(60, 25);
            this.rbtRoundType_Ceiling.TabIndex = 2;
            this.rbtRoundType_Ceiling.Text = "올림";
            this.rbtRoundType_Ceiling.UseVisualStyleBackColor = true;
            // 
            // rbtRoundType_Round
            // 
            this.rbtRoundType_Round.AutoSize = true;
            this.rbtRoundType_Round.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtRoundType_Round.ForeColor = System.Drawing.Color.White;
            this.rbtRoundType_Round.Location = new System.Drawing.Point(201, 3);
            this.rbtRoundType_Round.Name = "rbtRoundType_Round";
            this.rbtRoundType_Round.Size = new System.Drawing.Size(76, 25);
            this.rbtRoundType_Round.TabIndex = 3;
            this.rbtRoundType_Round.Text = "반올림";
            this.rbtRoundType_Round.UseVisualStyleBackColor = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label33.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label33.ForeColor = System.Drawing.Color.White;
            this.label33.Location = new System.Drawing.Point(4, 292);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(144, 40);
            this.label33.TabIndex = 19;
            this.label33.Text = "반올림 형식";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label34.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label34.ForeColor = System.Drawing.Color.White;
            this.label34.Location = new System.Drawing.Point(4, 251);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(144, 40);
            this.label34.TabIndex = 17;
            this.label34.Text = "일일 기준";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 2;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Controls.Add(this.label35, 1, 0);
            this.tableLayoutPanel13.Controls.Add(this.tbxTotalMaxRates, 0, 0);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(155, 213);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 1;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(399, 34);
            this.tableLayoutPanel13.TabIndex = 16;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label35.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label35.ForeColor = System.Drawing.Color.White;
            this.label35.Location = new System.Drawing.Point(202, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(194, 34);
            this.label35.TabIndex = 7;
            this.label35.Text = "원   (-1: 무제한)";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxTotalMaxRates
            // 
            this.tbxTotalMaxRates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxTotalMaxRates.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbxTotalMaxRates.Location = new System.Drawing.Point(3, 3);
            this.tbxTotalMaxRates.Name = "tbxTotalMaxRates";
            this.tbxTotalMaxRates.Size = new System.Drawing.Size(193, 29);
            this.tbxTotalMaxRates.TabIndex = 7;
            this.tbxTotalMaxRates.Text = "50000";
            this.tbxTotalMaxRates.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 2;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.Controls.Add(this.label36, 1, 0);
            this.tableLayoutPanel14.Controls.Add(this.tbxDailyMaxRates, 0, 0);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(155, 172);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 1;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(399, 34);
            this.tableLayoutPanel14.TabIndex = 15;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label36.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label36.ForeColor = System.Drawing.Color.White;
            this.label36.Location = new System.Drawing.Point(202, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(194, 34);
            this.label36.TabIndex = 7;
            this.label36.Text = "원   (-1: 무제한)";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxDailyMaxRates
            // 
            this.tbxDailyMaxRates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxDailyMaxRates.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbxDailyMaxRates.Location = new System.Drawing.Point(3, 3);
            this.tbxDailyMaxRates.Name = "tbxDailyMaxRates";
            this.tbxDailyMaxRates.Size = new System.Drawing.Size(193, 29);
            this.tbxDailyMaxRates.TabIndex = 6;
            this.tbxDailyMaxRates.Text = "5000";
            this.tbxDailyMaxRates.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label37.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label37.ForeColor = System.Drawing.Color.White;
            this.label37.Location = new System.Drawing.Point(4, 210);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(144, 40);
            this.label37.TabIndex = 14;
            this.label37.Text = "전체 최대요금";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label38.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label38.ForeColor = System.Drawing.Color.White;
            this.label38.Location = new System.Drawing.Point(4, 169);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(144, 40);
            this.label38.TabIndex = 12;
            this.label38.Text = "일일 최대요금";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 4;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel15.Controls.Add(this.label39, 3, 0);
            this.tableLayoutPanel15.Controls.Add(this.tbxAddRates, 2, 0);
            this.tableLayoutPanel15.Controls.Add(this.label40, 1, 0);
            this.tableLayoutPanel15.Controls.Add(this.tbxAddTime, 0, 0);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(155, 131);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 1;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(399, 34);
            this.tableLayoutPanel15.TabIndex = 11;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label39.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label39.ForeColor = System.Drawing.Color.White;
            this.label39.Location = new System.Drawing.Point(340, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(56, 34);
            this.label39.TabIndex = 9;
            this.label39.Text = "원";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxAddRates
            // 
            this.tbxAddRates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxAddRates.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbxAddRates.Location = new System.Drawing.Point(221, 3);
            this.tbxAddRates.Name = "tbxAddRates";
            this.tbxAddRates.Size = new System.Drawing.Size(113, 29);
            this.tbxAddRates.TabIndex = 5;
            this.tbxAddRates.Text = "200";
            this.tbxAddRates.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label40.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label40.ForeColor = System.Drawing.Color.White;
            this.label40.Location = new System.Drawing.Point(122, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(93, 34);
            this.label40.TabIndex = 7;
            this.label40.Text = "분 당";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxAddTime
            // 
            this.tbxAddTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxAddTime.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbxAddTime.Location = new System.Drawing.Point(3, 3);
            this.tbxAddTime.Name = "tbxAddTime";
            this.tbxAddTime.Size = new System.Drawing.Size(113, 29);
            this.tbxAddTime.TabIndex = 4;
            this.tbxAddTime.Text = "10";
            this.tbxAddTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label41.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label41.ForeColor = System.Drawing.Color.White;
            this.label41.Location = new System.Drawing.Point(4, 128);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(144, 40);
            this.label41.TabIndex = 10;
            this.label41.Text = "추가요금";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 4;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel16.Controls.Add(this.label42, 3, 0);
            this.tableLayoutPanel16.Controls.Add(this.tbxBaseRates, 2, 0);
            this.tableLayoutPanel16.Controls.Add(this.label43, 1, 0);
            this.tableLayoutPanel16.Controls.Add(this.tbxBaseTime, 0, 0);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(155, 90);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 1;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(399, 34);
            this.tableLayoutPanel16.TabIndex = 9;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label42.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label42.ForeColor = System.Drawing.Color.White;
            this.label42.Location = new System.Drawing.Point(340, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(56, 34);
            this.label42.TabIndex = 9;
            this.label42.Text = "원";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxBaseRates
            // 
            this.tbxBaseRates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxBaseRates.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbxBaseRates.Location = new System.Drawing.Point(221, 3);
            this.tbxBaseRates.Name = "tbxBaseRates";
            this.tbxBaseRates.Size = new System.Drawing.Size(113, 29);
            this.tbxBaseRates.TabIndex = 3;
            this.tbxBaseRates.Text = "600";
            this.tbxBaseRates.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label43.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label43.ForeColor = System.Drawing.Color.White;
            this.label43.Location = new System.Drawing.Point(122, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(93, 34);
            this.label43.TabIndex = 7;
            this.label43.Text = "분 당";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxBaseTime
            // 
            this.tbxBaseTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxBaseTime.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbxBaseTime.Location = new System.Drawing.Point(3, 3);
            this.tbxBaseTime.Name = "tbxBaseTime";
            this.tbxBaseTime.Size = new System.Drawing.Size(113, 29);
            this.tbxBaseTime.TabIndex = 2;
            this.tbxBaseTime.Text = "30";
            this.tbxBaseTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label44.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label44.ForeColor = System.Drawing.Color.White;
            this.label44.Location = new System.Drawing.Point(4, 87);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(144, 40);
            this.label44.TabIndex = 8;
            this.label44.Text = "기본요금";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label45.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label45.ForeColor = System.Drawing.Color.White;
            this.label45.Location = new System.Drawing.Point(4, 46);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(144, 40);
            this.label45.TabIndex = 6;
            this.label45.Text = "회차";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 2;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel17.Controls.Add(this.label49, 1, 0);
            this.tableLayoutPanel17.Controls.Add(this.tbxTurningTime, 0, 0);
            this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel17.Location = new System.Drawing.Point(155, 49);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 1;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(399, 34);
            this.tableLayoutPanel17.TabIndex = 7;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label49.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label49.ForeColor = System.Drawing.Color.White;
            this.label49.Location = new System.Drawing.Point(122, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(274, 34);
            this.label49.TabIndex = 7;
            this.label49.Text = "분   (0: 회차 없음)";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxTurningTime
            // 
            this.tbxTurningTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxTurningTime.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbxTurningTime.Location = new System.Drawing.Point(3, 3);
            this.tbxTurningTime.Name = "tbxTurningTime";
            this.tbxTurningTime.Size = new System.Drawing.Size(113, 29);
            this.tbxTurningTime.TabIndex = 1;
            this.tbxTurningTime.Text = "15";
            this.tbxTurningTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.Controls.Add(this.rbtDailyCriterion_Date);
            this.flowLayoutPanel8.Controls.Add(this.rbtDailyCriterion_InTime);
            this.flowLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(155, 254);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(399, 34);
            this.flowLayoutPanel8.TabIndex = 18;
            // 
            // rbtDailyCriterion_Date
            // 
            this.rbtDailyCriterion_Date.AutoSize = true;
            this.rbtDailyCriterion_Date.Checked = true;
            this.rbtDailyCriterion_Date.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtDailyCriterion_Date.ForeColor = System.Drawing.Color.White;
            this.rbtDailyCriterion_Date.Location = new System.Drawing.Point(3, 3);
            this.rbtDailyCriterion_Date.Name = "rbtDailyCriterion_Date";
            this.rbtDailyCriterion_Date.Size = new System.Drawing.Size(60, 25);
            this.rbtDailyCriterion_Date.TabIndex = 8;
            this.rbtDailyCriterion_Date.TabStop = true;
            this.rbtDailyCriterion_Date.Text = "날자";
            this.rbtDailyCriterion_Date.UseVisualStyleBackColor = true;
            // 
            // rbtDailyCriterion_InTime
            // 
            this.rbtDailyCriterion_InTime.AutoSize = true;
            this.rbtDailyCriterion_InTime.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtDailyCriterion_InTime.ForeColor = System.Drawing.Color.White;
            this.rbtDailyCriterion_InTime.Location = new System.Drawing.Point(69, 3);
            this.rbtDailyCriterion_InTime.Name = "rbtDailyCriterion_InTime";
            this.rbtDailyCriterion_InTime.Size = new System.Drawing.Size(92, 25);
            this.rbtDailyCriterion_InTime.TabIndex = 9;
            this.rbtDailyCriterion_InTime.Text = "입차시각";
            this.rbtDailyCriterion_InTime.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(236)))), ((int)(((byte)(219)))));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(562, 60);
            this.label1.TabIndex = 2;
            this.label1.Text = "▶ 요금 체계 설정";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnApplyTariff
            // 
            this.btnApplyTariff.BackColor = System.Drawing.Color.SlateGray;
            this.btnApplyTariff.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnApplyTariff.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApplyTariff.Font = new System.Drawing.Font("굴림", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnApplyTariff.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnApplyTariff.Location = new System.Drawing.Point(5, 670);
            this.btnApplyTariff.Margin = new System.Windows.Forms.Padding(5);
            this.btnApplyTariff.Name = "btnApplyTariff";
            this.btnApplyTariff.Size = new System.Drawing.Size(558, 70);
            this.btnApplyTariff.TabIndex = 3;
            this.btnApplyTariff.Text = "적 용";
            this.btnApplyTariff.UseVisualStyleBackColor = false;
            // 
            // SogNo_frmSetTariff_Simple1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 761);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SogNo_frmSetTariff_Simple1";
            this.Text = "요금 체계 설정";
            this.TopMost = true;
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.flowLayoutPanel11.ResumeLayout(false);
            this.flowLayoutPanel11.PerformLayout();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.flowLayoutPanel6.ResumeLayout(false);
            this.flowLayoutPanel6.PerformLayout();
            this.flowLayoutPanel7.ResumeLayout(false);
            this.flowLayoutPanel7.PerformLayout();
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel16.PerformLayout();
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel17.PerformLayout();
            this.flowLayoutPanel8.ResumeLayout(false);
            this.flowLayoutPanel8.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnApplyTariff;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.TextBox tbxTariffName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private System.Windows.Forms.CheckBox cbxFreeDay_Saturday;
        private System.Windows.Forms.CheckBox cbxFreeDay_Sunday;
        private System.Windows.Forms.CheckBox cbxFreeDay_Holiday;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox tbxPriceDC;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox tbxPercentDC;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.RadioButton rbtRoundDecimal_10;
        private System.Windows.Forms.RadioButton rbtRoundDecimal_100;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        private System.Windows.Forms.RadioButton rbtRoundType_None;
        private System.Windows.Forms.RadioButton rbtRoundType_Floor;
        private System.Windows.Forms.RadioButton rbtRoundType_Ceiling;
        private System.Windows.Forms.RadioButton rbtRoundType_Round;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox tbxTotalMaxRates;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox tbxDailyMaxRates;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox tbxAddRates;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox tbxAddTime;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox tbxBaseRates;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox tbxBaseTime;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox tbxTurningTime;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.RadioButton rbtDailyCriterion_Date;
        private System.Windows.Forms.RadioButton rbtDailyCriterion_InTime;
    }
}