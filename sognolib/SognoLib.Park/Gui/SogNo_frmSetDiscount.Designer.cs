﻿namespace SognoLib.Park.Gui
{
    partial class SogNo_frmSetDiscount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.tbxMemo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblDiscountValue2_Exam = new System.Windows.Forms.Label();
            this.tbxDiscountName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblDiscountValue2_Name = new System.Windows.Forms.Label();
            this.lblDiscountValue1_Name = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.cbxDiscountType = new System.Windows.Forms.ComboBox();
            this.lblDiscountValue1_Exam = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.rbtShortCut_F10 = new System.Windows.Forms.RadioButton();
            this.rbtShortCut_F9 = new System.Windows.Forms.RadioButton();
            this.rbtShortCut_F8 = new System.Windows.Forms.RadioButton();
            this.rbtShortCut_F7 = new System.Windows.Forms.RadioButton();
            this.rbtShortCut_F6 = new System.Windows.Forms.RadioButton();
            this.rbtShortCut_F5 = new System.Windows.Forms.RadioButton();
            this.rbtShortCut_F4 = new System.Windows.Forms.RadioButton();
            this.rbtShortCut_F3 = new System.Windows.Forms.RadioButton();
            this.rbtShortCut_F2 = new System.Windows.Forms.RadioButton();
            this.rbtShortCut_F1 = new System.Windows.Forms.RadioButton();
            this.rbtShortCut_None = new System.Windows.Forms.RadioButton();
            this.layoutDiscountValue1 = new System.Windows.Forms.TableLayoutPanel();
            this.tbxDiscountValue1 = new System.Windows.Forms.TextBox();
            this.lblDiscountValue1_Unit = new System.Windows.Forms.Label();
            this.layoutDiscountValue2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblDiscountValue2_Unit = new System.Windows.Forms.Label();
            this.tbxDiscountValue2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnApplyDiscount = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.layoutDiscountValue1.SuspendLayout();
            this.layoutDiscountValue2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(584, 761);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel10, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnApplyDiscount, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(8, 8);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(568, 745);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Controls.Add(this.tbxMemo, 1, 7);
            this.tableLayoutPanel10.Controls.Add(this.label5, 0, 7);
            this.tableLayoutPanel10.Controls.Add(this.label6, 0, 6);
            this.tableLayoutPanel10.Controls.Add(this.lblDiscountValue2_Exam, 1, 5);
            this.tableLayoutPanel10.Controls.Add(this.tbxDiscountName, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.lblDiscountValue2_Name, 0, 4);
            this.tableLayoutPanel10.Controls.Add(this.lblDiscountValue1_Name, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.label45, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.cbxDiscountType, 1, 1);
            this.tableLayoutPanel10.Controls.Add(this.lblDiscountValue1_Exam, 1, 3);
            this.tableLayoutPanel10.Controls.Add(this.tableLayoutPanel3, 1, 6);
            this.tableLayoutPanel10.Controls.Add(this.layoutDiscountValue1, 1, 2);
            this.tableLayoutPanel10.Controls.Add(this.layoutDiscountValue2, 1, 4);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(5, 65);
            this.tableLayoutPanel10.Margin = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 8;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(558, 595);
            this.tableLayoutPanel10.TabIndex = 9;
            // 
            // tbxMemo
            // 
            this.tbxMemo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxMemo.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbxMemo.Location = new System.Drawing.Point(158, 501);
            this.tbxMemo.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.tbxMemo.Multiline = true;
            this.tbxMemo.Name = "tbxMemo";
            this.tbxMemo.Size = new System.Drawing.Size(393, 90);
            this.tbxMemo.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(1, 498);
            this.label5.Margin = new System.Windows.Forms.Padding(0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(150, 96);
            this.label5.TabIndex = 43;
            this.label5.Text = "메모";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(1, 247);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(150, 250);
            this.label6.TabIndex = 41;
            this.label6.Text = "단축키 설정";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDiscountValue2_Exam
            // 
            this.lblDiscountValue2_Exam.AutoSize = true;
            this.lblDiscountValue2_Exam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDiscountValue2_Exam.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblDiscountValue2_Exam.ForeColor = System.Drawing.Color.White;
            this.lblDiscountValue2_Exam.Location = new System.Drawing.Point(155, 206);
            this.lblDiscountValue2_Exam.Name = "lblDiscountValue2_Exam";
            this.lblDiscountValue2_Exam.Size = new System.Drawing.Size(399, 40);
            this.lblDiscountValue2_Exam.TabIndex = 38;
            this.lblDiscountValue2_Exam.Text = "예)";
            this.lblDiscountValue2_Exam.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxDiscountName
            // 
            this.tbxDiscountName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxDiscountName.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbxDiscountName.Location = new System.Drawing.Point(158, 4);
            this.tbxDiscountName.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.tbxDiscountName.Name = "tbxDiscountName";
            this.tbxDiscountName.Size = new System.Drawing.Size(393, 33);
            this.tbxDiscountName.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(4, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(144, 40);
            this.label2.TabIndex = 33;
            this.label2.Text = "할인 이름";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDiscountValue2_Name
            // 
            this.lblDiscountValue2_Name.AutoSize = true;
            this.lblDiscountValue2_Name.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDiscountValue2_Name.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblDiscountValue2_Name.ForeColor = System.Drawing.Color.White;
            this.lblDiscountValue2_Name.Location = new System.Drawing.Point(1, 165);
            this.lblDiscountValue2_Name.Margin = new System.Windows.Forms.Padding(0);
            this.lblDiscountValue2_Name.Name = "lblDiscountValue2_Name";
            this.tableLayoutPanel10.SetRowSpan(this.lblDiscountValue2_Name, 2);
            this.lblDiscountValue2_Name.Size = new System.Drawing.Size(150, 81);
            this.lblDiscountValue2_Name.TabIndex = 12;
            this.lblDiscountValue2_Name.Text = "할인 값2";
            this.lblDiscountValue2_Name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDiscountValue1_Name
            // 
            this.lblDiscountValue1_Name.AutoSize = true;
            this.lblDiscountValue1_Name.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDiscountValue1_Name.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblDiscountValue1_Name.ForeColor = System.Drawing.Color.White;
            this.lblDiscountValue1_Name.Location = new System.Drawing.Point(1, 83);
            this.lblDiscountValue1_Name.Margin = new System.Windows.Forms.Padding(0);
            this.lblDiscountValue1_Name.Name = "lblDiscountValue1_Name";
            this.tableLayoutPanel10.SetRowSpan(this.lblDiscountValue1_Name, 2);
            this.lblDiscountValue1_Name.Size = new System.Drawing.Size(150, 81);
            this.lblDiscountValue1_Name.TabIndex = 8;
            this.lblDiscountValue1_Name.Text = "할인 값1";
            this.lblDiscountValue1_Name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label45.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label45.ForeColor = System.Drawing.Color.White;
            this.label45.Location = new System.Drawing.Point(4, 42);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(144, 40);
            this.label45.TabIndex = 6;
            this.label45.Text = "할인 유형";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxDiscountType
            // 
            this.cbxDiscountType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbxDiscountType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxDiscountType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxDiscountType.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cbxDiscountType.FormattingEnabled = true;
            this.cbxDiscountType.Location = new System.Drawing.Point(158, 45);
            this.cbxDiscountType.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.cbxDiscountType.Name = "cbxDiscountType";
            this.cbxDiscountType.Size = new System.Drawing.Size(393, 33);
            this.cbxDiscountType.TabIndex = 1;
            // 
            // lblDiscountValue1_Exam
            // 
            this.lblDiscountValue1_Exam.AutoSize = true;
            this.lblDiscountValue1_Exam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDiscountValue1_Exam.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblDiscountValue1_Exam.ForeColor = System.Drawing.Color.White;
            this.lblDiscountValue1_Exam.Location = new System.Drawing.Point(155, 124);
            this.lblDiscountValue1_Exam.Name = "lblDiscountValue1_Exam";
            this.lblDiscountValue1_Exam.Size = new System.Drawing.Size(399, 40);
            this.lblDiscountValue1_Exam.TabIndex = 37;
            this.lblDiscountValue1_Exam.Text = "예)";
            this.lblDiscountValue1_Exam.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.rbtShortCut_F10, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.rbtShortCut_F9, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.rbtShortCut_F8, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.rbtShortCut_F7, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.rbtShortCut_F6, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.rbtShortCut_F5, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.rbtShortCut_F4, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.rbtShortCut_F3, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.rbtShortCut_F2, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.rbtShortCut_F1, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.rbtShortCut_None, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(159, 254);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(7);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 6;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(391, 236);
            this.tableLayoutPanel3.TabIndex = 42;
            // 
            // rbtShortCut_F10
            // 
            this.rbtShortCut_F10.AutoSize = true;
            this.rbtShortCut_F10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtShortCut_F10.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtShortCut_F10.ForeColor = System.Drawing.Color.White;
            this.rbtShortCut_F10.Location = new System.Drawing.Point(198, 198);
            this.rbtShortCut_F10.Name = "rbtShortCut_F10";
            this.rbtShortCut_F10.Size = new System.Drawing.Size(190, 35);
            this.rbtShortCut_F10.TabIndex = 11;
            this.rbtShortCut_F10.Text = "[ F10 ]";
            this.rbtShortCut_F10.UseVisualStyleBackColor = true;
            // 
            // rbtShortCut_F9
            // 
            this.rbtShortCut_F9.AutoSize = true;
            this.rbtShortCut_F9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtShortCut_F9.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtShortCut_F9.ForeColor = System.Drawing.Color.White;
            this.rbtShortCut_F9.Location = new System.Drawing.Point(3, 198);
            this.rbtShortCut_F9.Name = "rbtShortCut_F9";
            this.rbtShortCut_F9.Size = new System.Drawing.Size(189, 35);
            this.rbtShortCut_F9.TabIndex = 10;
            this.rbtShortCut_F9.Text = "[ F9 ]";
            this.rbtShortCut_F9.UseVisualStyleBackColor = true;
            // 
            // rbtShortCut_F8
            // 
            this.rbtShortCut_F8.AutoSize = true;
            this.rbtShortCut_F8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtShortCut_F8.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtShortCut_F8.ForeColor = System.Drawing.Color.White;
            this.rbtShortCut_F8.Location = new System.Drawing.Point(198, 159);
            this.rbtShortCut_F8.Name = "rbtShortCut_F8";
            this.rbtShortCut_F8.Size = new System.Drawing.Size(190, 33);
            this.rbtShortCut_F8.TabIndex = 9;
            this.rbtShortCut_F8.Text = "[ F8 ]";
            this.rbtShortCut_F8.UseVisualStyleBackColor = true;
            // 
            // rbtShortCut_F7
            // 
            this.rbtShortCut_F7.AutoSize = true;
            this.rbtShortCut_F7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtShortCut_F7.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtShortCut_F7.ForeColor = System.Drawing.Color.White;
            this.rbtShortCut_F7.Location = new System.Drawing.Point(3, 159);
            this.rbtShortCut_F7.Name = "rbtShortCut_F7";
            this.rbtShortCut_F7.Size = new System.Drawing.Size(189, 33);
            this.rbtShortCut_F7.TabIndex = 8;
            this.rbtShortCut_F7.Text = "[ F7 ]";
            this.rbtShortCut_F7.UseVisualStyleBackColor = true;
            // 
            // rbtShortCut_F6
            // 
            this.rbtShortCut_F6.AutoSize = true;
            this.rbtShortCut_F6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtShortCut_F6.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtShortCut_F6.ForeColor = System.Drawing.Color.White;
            this.rbtShortCut_F6.Location = new System.Drawing.Point(198, 120);
            this.rbtShortCut_F6.Name = "rbtShortCut_F6";
            this.rbtShortCut_F6.Size = new System.Drawing.Size(190, 33);
            this.rbtShortCut_F6.TabIndex = 7;
            this.rbtShortCut_F6.Text = "[ F6 ]";
            this.rbtShortCut_F6.UseVisualStyleBackColor = true;
            // 
            // rbtShortCut_F5
            // 
            this.rbtShortCut_F5.AutoSize = true;
            this.rbtShortCut_F5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtShortCut_F5.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtShortCut_F5.ForeColor = System.Drawing.Color.White;
            this.rbtShortCut_F5.Location = new System.Drawing.Point(3, 120);
            this.rbtShortCut_F5.Name = "rbtShortCut_F5";
            this.rbtShortCut_F5.Size = new System.Drawing.Size(189, 33);
            this.rbtShortCut_F5.TabIndex = 6;
            this.rbtShortCut_F5.Text = "[ F5 ]";
            this.rbtShortCut_F5.UseVisualStyleBackColor = true;
            // 
            // rbtShortCut_F4
            // 
            this.rbtShortCut_F4.AutoSize = true;
            this.rbtShortCut_F4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtShortCut_F4.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtShortCut_F4.ForeColor = System.Drawing.Color.White;
            this.rbtShortCut_F4.Location = new System.Drawing.Point(198, 81);
            this.rbtShortCut_F4.Name = "rbtShortCut_F4";
            this.rbtShortCut_F4.Size = new System.Drawing.Size(190, 33);
            this.rbtShortCut_F4.TabIndex = 5;
            this.rbtShortCut_F4.Text = "[ F4 ]";
            this.rbtShortCut_F4.UseVisualStyleBackColor = true;
            // 
            // rbtShortCut_F3
            // 
            this.rbtShortCut_F3.AutoSize = true;
            this.rbtShortCut_F3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtShortCut_F3.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtShortCut_F3.ForeColor = System.Drawing.Color.White;
            this.rbtShortCut_F3.Location = new System.Drawing.Point(3, 81);
            this.rbtShortCut_F3.Name = "rbtShortCut_F3";
            this.rbtShortCut_F3.Size = new System.Drawing.Size(189, 33);
            this.rbtShortCut_F3.TabIndex = 4;
            this.rbtShortCut_F3.Text = "[ F3 ]";
            this.rbtShortCut_F3.UseVisualStyleBackColor = true;
            // 
            // rbtShortCut_F2
            // 
            this.rbtShortCut_F2.AutoSize = true;
            this.rbtShortCut_F2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtShortCut_F2.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtShortCut_F2.ForeColor = System.Drawing.Color.White;
            this.rbtShortCut_F2.Location = new System.Drawing.Point(198, 42);
            this.rbtShortCut_F2.Name = "rbtShortCut_F2";
            this.rbtShortCut_F2.Size = new System.Drawing.Size(190, 33);
            this.rbtShortCut_F2.TabIndex = 3;
            this.rbtShortCut_F2.Text = "[ F2 ]";
            this.rbtShortCut_F2.UseVisualStyleBackColor = true;
            // 
            // rbtShortCut_F1
            // 
            this.rbtShortCut_F1.AutoSize = true;
            this.rbtShortCut_F1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtShortCut_F1.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtShortCut_F1.ForeColor = System.Drawing.Color.White;
            this.rbtShortCut_F1.Location = new System.Drawing.Point(3, 42);
            this.rbtShortCut_F1.Name = "rbtShortCut_F1";
            this.rbtShortCut_F1.Size = new System.Drawing.Size(189, 33);
            this.rbtShortCut_F1.TabIndex = 2;
            this.rbtShortCut_F1.Text = "[ F1 ]";
            this.rbtShortCut_F1.UseVisualStyleBackColor = true;
            // 
            // rbtShortCut_None
            // 
            this.rbtShortCut_None.AutoSize = true;
            this.rbtShortCut_None.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rbtShortCut_None.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.rbtShortCut_None.ForeColor = System.Drawing.Color.White;
            this.rbtShortCut_None.Location = new System.Drawing.Point(3, 3);
            this.rbtShortCut_None.Name = "rbtShortCut_None";
            this.rbtShortCut_None.Size = new System.Drawing.Size(189, 33);
            this.rbtShortCut_None.TabIndex = 0;
            this.rbtShortCut_None.Text = "없음";
            this.rbtShortCut_None.UseVisualStyleBackColor = true;
            // 
            // layoutDiscountValue1
            // 
            this.layoutDiscountValue1.ColumnCount = 2;
            this.layoutDiscountValue1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layoutDiscountValue1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layoutDiscountValue1.Controls.Add(this.tbxDiscountValue1, 0, 0);
            this.layoutDiscountValue1.Controls.Add(this.lblDiscountValue1_Unit, 1, 0);
            this.layoutDiscountValue1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutDiscountValue1.Location = new System.Drawing.Point(152, 83);
            this.layoutDiscountValue1.Margin = new System.Windows.Forms.Padding(0);
            this.layoutDiscountValue1.Name = "layoutDiscountValue1";
            this.layoutDiscountValue1.RowCount = 1;
            this.layoutDiscountValue1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layoutDiscountValue1.Size = new System.Drawing.Size(405, 40);
            this.layoutDiscountValue1.TabIndex = 45;
            // 
            // tbxDiscountValue1
            // 
            this.tbxDiscountValue1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxDiscountValue1.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbxDiscountValue1.Location = new System.Drawing.Point(6, 3);
            this.tbxDiscountValue1.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.tbxDiscountValue1.Name = "tbxDiscountValue1";
            this.tbxDiscountValue1.Size = new System.Drawing.Size(190, 33);
            this.tbxDiscountValue1.TabIndex = 3;
            // 
            // lblDiscountValue1_Unit
            // 
            this.lblDiscountValue1_Unit.AutoSize = true;
            this.lblDiscountValue1_Unit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDiscountValue1_Unit.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblDiscountValue1_Unit.ForeColor = System.Drawing.Color.White;
            this.lblDiscountValue1_Unit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblDiscountValue1_Unit.Location = new System.Drawing.Point(205, 0);
            this.lblDiscountValue1_Unit.Name = "lblDiscountValue1_Unit";
            this.lblDiscountValue1_Unit.Size = new System.Drawing.Size(197, 40);
            this.lblDiscountValue1_Unit.TabIndex = 37;
            this.lblDiscountValue1_Unit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // layoutDiscountValue2
            // 
            this.layoutDiscountValue2.ColumnCount = 2;
            this.layoutDiscountValue2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layoutDiscountValue2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layoutDiscountValue2.Controls.Add(this.lblDiscountValue2_Unit, 0, 0);
            this.layoutDiscountValue2.Controls.Add(this.tbxDiscountValue2, 0, 0);
            this.layoutDiscountValue2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutDiscountValue2.Location = new System.Drawing.Point(152, 165);
            this.layoutDiscountValue2.Margin = new System.Windows.Forms.Padding(0);
            this.layoutDiscountValue2.Name = "layoutDiscountValue2";
            this.layoutDiscountValue2.RowCount = 1;
            this.layoutDiscountValue2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layoutDiscountValue2.Size = new System.Drawing.Size(405, 40);
            this.layoutDiscountValue2.TabIndex = 46;
            // 
            // lblDiscountValue2_Unit
            // 
            this.lblDiscountValue2_Unit.AutoSize = true;
            this.lblDiscountValue2_Unit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDiscountValue2_Unit.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lblDiscountValue2_Unit.ForeColor = System.Drawing.Color.White;
            this.lblDiscountValue2_Unit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblDiscountValue2_Unit.Location = new System.Drawing.Point(205, 0);
            this.lblDiscountValue2_Unit.Name = "lblDiscountValue2_Unit";
            this.lblDiscountValue2_Unit.Size = new System.Drawing.Size(197, 40);
            this.lblDiscountValue2_Unit.TabIndex = 38;
            this.lblDiscountValue2_Unit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxDiscountValue2
            // 
            this.tbxDiscountValue2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbxDiscountValue2.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tbxDiscountValue2.Location = new System.Drawing.Point(6, 3);
            this.tbxDiscountValue2.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.tbxDiscountValue2.Name = "tbxDiscountValue2";
            this.tbxDiscountValue2.Size = new System.Drawing.Size(190, 33);
            this.tbxDiscountValue2.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(236)))), ((int)(((byte)(219)))));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(562, 60);
            this.label1.TabIndex = 2;
            this.label1.Text = "▶ 할인 체계 설정";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnApplyDiscount
            // 
            this.btnApplyDiscount.BackColor = System.Drawing.Color.SlateGray;
            this.btnApplyDiscount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnApplyDiscount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApplyDiscount.Font = new System.Drawing.Font("굴림", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnApplyDiscount.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnApplyDiscount.Location = new System.Drawing.Point(5, 670);
            this.btnApplyDiscount.Margin = new System.Windows.Forms.Padding(5);
            this.btnApplyDiscount.Name = "btnApplyDiscount";
            this.btnApplyDiscount.Size = new System.Drawing.Size(558, 70);
            this.btnApplyDiscount.TabIndex = 6;
            this.btnApplyDiscount.TabStop = false;
            this.btnApplyDiscount.Text = "적 용";
            this.btnApplyDiscount.UseVisualStyleBackColor = false;
            // 
            // SogNo_frmSetDiscount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 761);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SogNo_frmSetDiscount";
            this.Text = "SogNo_frmSetDiscount";
            this.TopMost = true;
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.layoutDiscountValue1.ResumeLayout(false);
            this.layoutDiscountValue1.PerformLayout();
            this.layoutDiscountValue2.ResumeLayout(false);
            this.layoutDiscountValue2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.TextBox tbxDiscountName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblDiscountValue2_Name;
        private System.Windows.Forms.Label lblDiscountValue1_Name;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnApplyDiscount;
        private System.Windows.Forms.ComboBox cbxDiscountType;
        private System.Windows.Forms.Label lblDiscountValue2_Exam;
        private System.Windows.Forms.Label lblDiscountValue1_Exam;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.RadioButton rbtShortCut_None;
        private System.Windows.Forms.RadioButton rbtShortCut_F10;
        private System.Windows.Forms.RadioButton rbtShortCut_F9;
        private System.Windows.Forms.RadioButton rbtShortCut_F8;
        private System.Windows.Forms.RadioButton rbtShortCut_F7;
        private System.Windows.Forms.RadioButton rbtShortCut_F6;
        private System.Windows.Forms.RadioButton rbtShortCut_F5;
        private System.Windows.Forms.RadioButton rbtShortCut_F4;
        private System.Windows.Forms.RadioButton rbtShortCut_F3;
        private System.Windows.Forms.RadioButton rbtShortCut_F2;
        private System.Windows.Forms.RadioButton rbtShortCut_F1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbxMemo;
        private System.Windows.Forms.TableLayoutPanel layoutDiscountValue1;
        private System.Windows.Forms.TextBox tbxDiscountValue1;
        private System.Windows.Forms.Label lblDiscountValue1_Unit;
        private System.Windows.Forms.TableLayoutPanel layoutDiscountValue2;
        private System.Windows.Forms.TextBox tbxDiscountValue2;
        private System.Windows.Forms.Label lblDiscountValue2_Unit;
    }
}