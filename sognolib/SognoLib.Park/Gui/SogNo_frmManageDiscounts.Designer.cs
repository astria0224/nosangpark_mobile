﻿namespace SognoLib.Park.Gui
{
    partial class SogNo_frmManageDiscounts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lvwDiscounts = new System.Windows.Forms.ListView();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.btnDiscountDetail = new System.Windows.Forms.Button();
            this.btnDiscountDelete = new System.Windows.Forms.Button();
            this.btnDiscountAdd = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(784, 761);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(8, 8);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 188F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(768, 745);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(236)))), ((int)(((byte)(219)))));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(762, 60);
            this.label1.TabIndex = 2;
            this.label1.Text = "할인 체계 관리";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel13, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.lvwDiscounts, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 63);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(762, 491);
            this.tableLayoutPanel3.TabIndex = 3;
            // 
            // lvwDiscounts
            // 
            this.lvwDiscounts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwDiscounts.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lvwDiscounts.FullRowSelect = true;
            this.lvwDiscounts.GridLines = true;
            this.lvwDiscounts.HideSelection = false;
            this.lvwDiscounts.Location = new System.Drawing.Point(3, 3);
            this.lvwDiscounts.MultiSelect = false;
            this.lvwDiscounts.Name = "lvwDiscounts";
            this.lvwDiscounts.Size = new System.Drawing.Size(676, 485);
            this.lvwDiscounts.TabIndex = 4;
            this.lvwDiscounts.UseCompatibleStateImageBehavior = false;
            this.lvwDiscounts.View = System.Windows.Forms.View.Details;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(204)))), ((int)(((byte)(197)))));
            this.tableLayoutPanel13.ColumnCount = 1;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Controls.Add(this.btnDiscountDetail, 0, 4);
            this.tableLayoutPanel13.Controls.Add(this.btnDiscountDelete, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.btnDiscountAdd, 0, 0);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(685, 3);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 5;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(74, 485);
            this.tableLayoutPanel13.TabIndex = 6;
            // 
            // btnDiscountDetail
            // 
            this.btnDiscountDetail.BackColor = System.Drawing.Color.Transparent;
            this.btnDiscountDetail.BackgroundImage = global::SognoLib.Park.Properties.Resources.next_128_default;
            this.btnDiscountDetail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDiscountDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDiscountDetail.FlatAppearance.BorderSize = 0;
            this.btnDiscountDetail.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.btnDiscountDetail.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnDiscountDetail.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnDiscountDetail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDiscountDetail.Location = new System.Drawing.Point(7, 422);
            this.btnDiscountDetail.Margin = new System.Windows.Forms.Padding(7);
            this.btnDiscountDetail.Name = "btnDiscountDetail";
            this.btnDiscountDetail.Size = new System.Drawing.Size(60, 56);
            this.btnDiscountDetail.TabIndex = 3;
            this.btnDiscountDetail.TabStop = false;
            this.btnDiscountDetail.UseVisualStyleBackColor = false;
            // 
            // btnDiscountDelete
            // 
            this.btnDiscountDelete.BackColor = System.Drawing.Color.Transparent;
            this.btnDiscountDelete.BackgroundImage = global::SognoLib.Park.Properties.Resources.minus_128_default;
            this.btnDiscountDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnDiscountDelete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDiscountDelete.FlatAppearance.BorderSize = 0;
            this.btnDiscountDelete.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.btnDiscountDelete.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnDiscountDelete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnDiscountDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDiscountDelete.Location = new System.Drawing.Point(7, 77);
            this.btnDiscountDelete.Margin = new System.Windows.Forms.Padding(7);
            this.btnDiscountDelete.Name = "btnDiscountDelete";
            this.btnDiscountDelete.Size = new System.Drawing.Size(60, 56);
            this.btnDiscountDelete.TabIndex = 1;
            this.btnDiscountDelete.TabStop = false;
            this.btnDiscountDelete.UseVisualStyleBackColor = false;
            // 
            // btnDiscountAdd
            // 
            this.btnDiscountAdd.BackColor = System.Drawing.Color.Transparent;
            this.btnDiscountAdd.BackgroundImage = global::SognoLib.Park.Properties.Resources.plus_128_default;
            this.btnDiscountAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnDiscountAdd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDiscountAdd.FlatAppearance.BorderSize = 0;
            this.btnDiscountAdd.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.btnDiscountAdd.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnDiscountAdd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnDiscountAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDiscountAdd.Location = new System.Drawing.Point(7, 7);
            this.btnDiscountAdd.Margin = new System.Windows.Forms.Padding(7);
            this.btnDiscountAdd.Name = "btnDiscountAdd";
            this.btnDiscountAdd.Size = new System.Drawing.Size(60, 56);
            this.btnDiscountAdd.TabIndex = 0;
            this.btnDiscountAdd.TabStop = false;
            this.btnDiscountAdd.UseVisualStyleBackColor = false;
            // 
            // SogNo_frmManageDiscounts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 761);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SogNo_frmManageDiscounts";
            this.Text = "Form1";
            this.TopMost = true;
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.ListView lvwDiscounts;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Button btnDiscountDetail;
        private System.Windows.Forms.Button btnDiscountDelete;
        private System.Windows.Forms.Button btnDiscountAdd;
    }
}