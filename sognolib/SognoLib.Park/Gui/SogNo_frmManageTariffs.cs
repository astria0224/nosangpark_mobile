﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SognoLib.Park.Data;


namespace SognoLib.Park.Gui
{
    public partial class SogNo_frmManageTariffs : Form
    {
        private int ParkNo { get; set; }

        SogNo_TariffDataAccess dataAccess = null;

        SogNo_frmSetTariff_Simple1 frmTariffDetail = null;

        public bool Modified { get; private set; } = false;


        public SogNo_frmManageTariffs(int parkNo, SogNo_TariffDataAccess dataAccess)
        {
            this.ParkNo = parkNo;

            InitializeComponent();
            
            InitializeListview();

            this.dataAccess = dataAccess;

            this.lvwTariffs.ItemSelectionChanged += OnItemSelectionChanged_Tariffs;
            this.lvwTariffs.MouseDoubleClick += OnMouseDoubleClicked_Tariffs;
                        
            this.btnTariffAdd.MouseClick += OnButtonClicked_TariffAdd;
            this.btnTariffAdd.EnabledChanged += OnEnabledChanged_TariffAdd;
            this.btnTariffAdd.MouseEnter += OnMouseEnter_TariffAdd;
            this.btnTariffAdd.MouseLeave += OnEnabledChanged_TariffAdd;

            this.btnTariffDelete.EnabledChanged += OnEnabledChanged_TariffDelete;
            this.btnTariffDelete.MouseEnter += OnMouseEnter_TariffDelete;
            this.btnTariffDelete.MouseLeave += OnEnabledChanged_TariffDelete;

            this.btnTariffDetail.MouseClick += OnButtonClicked_TariffDetail;
            this.btnTariffDetail.EnabledChanged += OnEnabledChanged_TariffDetail;
            this.btnTariffDetail.MouseEnter += OnMouseEnter_TariffDetail;
            this.btnTariffDetail.MouseLeave += OnEnabledChanged_TariffDetail;

            this.btnTariffDelete.Enabled = false;
            this.btnTariffDetail.Enabled = false;

            ReadData();
        }


        private void InitializeListview()
        {
            this.lvwTariffs.Columns.Add("번호", 50, HorizontalAlignment.Left);
            this.lvwTariffs.Columns.Add("이름", 300, HorizontalAlignment.Left);
            this.lvwTariffs.Columns.Add("일일 기준", 100, HorizontalAlignment.Center);
            this.lvwTariffs.Columns.Add("일 최대요금", 100, HorizontalAlignment.Center);
            this.lvwTariffs.Columns.Add("총 최대요금", 100, HorizontalAlignment.Center);
        }


        public bool ReadData()
        {
            this.lvwTariffs.Items.Clear();
            this.btnTariffDelete.Enabled = false;
            this.btnTariffDetail.Enabled = false;

            if (this.dataAccess == null)
            {
                return false;
            }

            TariffRecord[] tariffs = this.dataAccess.ReadAllTariffs(this.ParkNo) ?? new TariffRecord[0];
            foreach (var trf in tariffs)
            {
                ListViewItem item = new ListViewItem(trf.TrfNo?.ToString());
                item.SubItems.Add(trf.TrfName);
                item.SubItems.Add(trf.DailyCriterion == TariffRecord.DailyCriterion_Day ? "날짜" : "시간");
                item.SubItems.Add(trf.DailyMaxRates?.ToString());
                item.SubItems.Add(trf.MaxRates?.ToString());

                this.lvwTariffs.Items.Add(item);
            }

            return true;
        }


        public void OnItemSelectionChanged_Tariffs(object sender, ListViewItemSelectionChangedEventArgs args)
        {
            this.btnTariffDetail.Enabled = args.IsSelected;
        }


        public void OnMouseDoubleClicked_Tariffs(object sender, MouseEventArgs args)
        {
            ListView.SelectedIndexCollection selected = this.lvwTariffs.SelectedIndices;
            if (selected.Count == 1)
            {
                OnButtonClicked_TariffDetail(null, null);
            }
        }


        public void OnButtonClicked_TariffAdd(object sender, MouseEventArgs args)
        {
            if (this.frmTariffDetail != null)
            {
                this.frmTariffDetail.Close();
            }
            
            this.frmTariffDetail = new SogNo_frmSetTariff_Simple1(this.ParkNo, this.dataAccess);
            this.frmTariffDetail.FormClosed += OnFormClosed_TariffDetail;
            this.frmTariffDetail.StartPosition = FormStartPosition.Manual;
            this.frmTariffDetail.Location = new Point(this.Location.X + this.ClientSize.Width + 5, this.Location.Y);

            this.frmTariffDetail.Show();
        }


        public void OnEnabledChanged_TariffAdd(object sender, EventArgs args)
        {
            if (this.btnTariffAdd.Enabled)
                this.btnTariffAdd.BackgroundImage = Properties.Resources.plus_128_default;
            else
                this.btnTariffAdd.BackgroundImage = Properties.Resources.plus_128_disabled;
        }


        public void OnMouseEnter_TariffAdd(object sender, EventArgs args)
        {
            if (this.btnTariffAdd.Enabled)
                this.btnTariffAdd.BackgroundImage = Properties.Resources.plus_128_mouseover;
        }

        
        public void OnEnabledChanged_TariffDelete(object sender, EventArgs args)
        {
            if (this.btnTariffDelete.Enabled)
                this.btnTariffDelete.BackgroundImage = Properties.Resources.minus_128_default;
            else
                this.btnTariffDelete.BackgroundImage = Properties.Resources.minus_128_disabled;
        }


        public void OnMouseEnter_TariffDelete(object sender, EventArgs args)
        {
            if (this.btnTariffDelete.Enabled)
                this.btnTariffDelete.BackgroundImage = Properties.Resources.minus_128_mouseover;
        }


        public void OnButtonClicked_TariffDetail(object sender, MouseEventArgs args)
        {
            ListView.SelectedIndexCollection selected = this.lvwTariffs.SelectedIndices;
            if (selected.Count == 0 || this.dataAccess == null)
            {
                return;
            }
            
            if (this.frmTariffDetail == null)
            {
                this.frmTariffDetail = new SogNo_frmSetTariff_Simple1(this.ParkNo, this.dataAccess);
                this.frmTariffDetail.FormClosed += OnFormClosed_TariffDetail;
                this.frmTariffDetail.StartPosition = FormStartPosition.Manual;
                this.frmTariffDetail.Location = new Point(this.Location.X + this.ClientSize.Width + 5, this.Location.Y);
            }

            ListViewItem item = this.lvwTariffs.Items[selected[0]];
            int trfNo = int.Parse(item.SubItems[0].Text);

            this.frmTariffDetail.ReadData(trfNo);

            this.frmTariffDetail.Show();
        }


        public void OnEnabledChanged_TariffDetail(object sender, EventArgs args)
        {
            if (this.btnTariffDetail.Enabled)
                this.btnTariffDetail.BackgroundImage = Properties.Resources.next_128_default;
            else
                this.btnTariffDetail.BackgroundImage = Properties.Resources.next_128_disabled;
        }


        public void OnMouseEnter_TariffDetail(object sender, EventArgs args)
        {
            if (this.btnTariffDetail.Enabled)
                this.btnTariffDetail.BackgroundImage = Properties.Resources.next_128_mouseover;
        }


        public void OnFormClosed_TariffDetail(object sender, FormClosedEventArgs args)
        {
            if (this.frmTariffDetail.DialogResult == DialogResult.OK &&
                this.frmTariffDetail.Modified)
            {
                ReadData();

                this.Modified = true;
            }

            this.frmTariffDetail = null;
        }
    }
}
