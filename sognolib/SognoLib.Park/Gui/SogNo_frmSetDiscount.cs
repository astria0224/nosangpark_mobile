﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SognoLib.Park.Data;


namespace SognoLib.Park.Gui
{
    public partial class SogNo_frmSetDiscount : Form
    {
        private int ParkNo { get; set; }
        private int? DiscountNo { get; set; } = null;

        SogNo_TariffDataAccess dataAccess = null;

        DCInfoRecord DCInfo = null;

        public bool Modified { get; private set; } = false;
                

        public SogNo_frmSetDiscount(int parkNo, SogNo_TariffDataAccess dataAccess)
        {
            this.ParkNo = parkNo;

            InitializeComponent();

            InitializeDCTypeComboBox();

            this.dataAccess = dataAccess;

            this.btnApplyDiscount.MouseClick += OnButtonClicked_ApplyDiscount;

            this.rbtShortCut_None.CheckedChanged += OnRadioButtonColorChange;
            this.rbtShortCut_F1.CheckedChanged += OnRadioButtonColorChange;
            this.rbtShortCut_F2.CheckedChanged += OnRadioButtonColorChange;
            this.rbtShortCut_F3.CheckedChanged += OnRadioButtonColorChange;
            this.rbtShortCut_F4.CheckedChanged += OnRadioButtonColorChange;
            this.rbtShortCut_F5.CheckedChanged += OnRadioButtonColorChange;
            this.rbtShortCut_F6.CheckedChanged += OnRadioButtonColorChange;
            this.rbtShortCut_F7.CheckedChanged += OnRadioButtonColorChange;
            this.rbtShortCut_F8.CheckedChanged += OnRadioButtonColorChange;
            this.rbtShortCut_F9.CheckedChanged += OnRadioButtonColorChange;
            this.rbtShortCut_F10.CheckedChanged += OnRadioButtonColorChange;

            this.cbxDiscountType.SelectedIndexChanged += OnSelectedIndexChanged_DiscountType;

            this.rbtShortCut_None.Checked = true;
            this.cbxDiscountType.SelectedIndex = 0;
        }


        private void InitializeDCTypeComboBox()
        {
            this.cbxDiscountType.Items.Add(DCInfoRecord.DiscountTypeString(0));
            this.cbxDiscountType.Items.Add(DCInfoRecord.DiscountTypeString(1));
            this.cbxDiscountType.Items.Add(DCInfoRecord.DiscountTypeString(2));
            this.cbxDiscountType.Items.Add(DCInfoRecord.DiscountTypeString(3));
            this.cbxDiscountType.Items.Add(DCInfoRecord.DiscountTypeString(4));
            this.cbxDiscountType.Items.Add(DCInfoRecord.DiscountTypeString(5));
            this.cbxDiscountType.Items.Add(DCInfoRecord.DiscountTypeString(6));
            this.cbxDiscountType.Items.Add(DCInfoRecord.DiscountTypeString(7));
        }


        public bool ReadData(int dcNo)
        {
            this.DiscountNo = dcNo;
            
            try
            {
                DCInfoRecord dcInfo = this.dataAccess.ReadDiscount(this.ParkNo, dcNo);
                if (dcInfo != null)
                {
                    return ReadData(dcInfo);
                }
            }
            catch
            {
                return false;
            }

            return false;
        }


        private bool ReadData(DCInfoRecord dcInfo)
        {
            this.tbxDiscountName.Text = dcInfo.DCName;
            this.cbxDiscountType.SelectedIndex = dcInfo.DCType.Value;
            this.tbxDiscountValue1.Text = dcInfo.DCValue1;
            this.tbxDiscountValue2.Text = dcInfo.DCValue2 ?? "";


            if (dcInfo.ButtonIndex == 1) this.rbtShortCut_F1.Checked = true;
            else if (dcInfo.ButtonIndex == 2) this.rbtShortCut_F2.Checked = true;
            else if (dcInfo.ButtonIndex == 3) this.rbtShortCut_F3.Checked = true;
            else if (dcInfo.ButtonIndex == 4) this.rbtShortCut_F4.Checked = true;
            else if (dcInfo.ButtonIndex == 5) this.rbtShortCut_F5.Checked = true;
            else if (dcInfo.ButtonIndex == 6) this.rbtShortCut_F6.Checked = true;
            else if (dcInfo.ButtonIndex == 7) this.rbtShortCut_F7.Checked = true;
            else if (dcInfo.ButtonIndex == 8) this.rbtShortCut_F8.Checked = true;
            else if (dcInfo.ButtonIndex == 9) this.rbtShortCut_F9.Checked = true;
            else if (dcInfo.ButtonIndex == 10) this.rbtShortCut_F10.Checked = true;
            else this.rbtShortCut_None.Checked = true;

            this.tbxMemo.Text = dcInfo.Memo;

            this.DCInfo = dcInfo;

            return true;
        }


        private void OnButtonClicked_ApplyDiscount(object sender, MouseEventArgs args)
        {
            if (!CheckForm())
                return;

            if (this.DiscountNo == null || this.DiscountNo < 0)
            {
                bool ret = AddDiscount();
                if (ret)
                {
                    this.DialogResult = DialogResult.OK;
                    Close();
                }
            }
            else
            {
                bool ret = UpdateDiscount();
                if (ret)
                {
                    this.DialogResult = DialogResult.OK;
                    Close();
                }
            }
        }


        private void OnSelectedIndexChanged_DiscountType(object sender, EventArgs args)
        {
            int dcType = this.cbxDiscountType.SelectedIndex;

            if (dcType == DCInfoRecord.DCType_TurningDC)
            {
                this.lblDiscountValue1_Name.Text = "회차 시간 (분)";
                this.lblDiscountValue1_Unit.Text = "분   (1~ )";
                this.lblDiscountValue1_Exam.Text = "예) 15 (15분 이내 출차 시 무료)";
                this.layoutDiscountValue1.ColumnStyles[1].Width = 70;
                this.layoutDiscountValue1.ColumnStyles[0].Width = 30;

                //this.lblDiscountValue2_Name.Text = "무료 시간 (분)";
                //this.lblDiscountValue2_Unit.Text = "분";
                //this.lblDiscountValue2_Exam.Text = "예) 15 (첫 15분은 요금을 받지 않음)";
                //this.layoutDiscountValue2.ColumnStyles[1].Width = 70;
                //this.layoutDiscountValue2.ColumnStyles[0].Width = 30;
                this.lblDiscountValue2_Name.Text = "";
                this.lblDiscountValue2_Unit.Text = "";
                this.lblDiscountValue2_Exam.Text = "";
                this.layoutDiscountValue2.ColumnStyles[1].Width = 100;
                this.layoutDiscountValue2.ColumnStyles[0].Width = 0;
            }
            else if (dcType == DCInfoRecord.DCType_TimeDC)
            {
                this.lblDiscountValue1_Name.Text = "감면 시간 (분)";
                this.lblDiscountValue1_Unit.Text = "분   (1~ )";
                this.lblDiscountValue1_Exam.Text = "예) 60 (주차 시간에서 60분 감면)";
                this.layoutDiscountValue1.ColumnStyles[1].Width = 70;
                this.layoutDiscountValue1.ColumnStyles[0].Width = 30;

                this.lblDiscountValue2_Name.Text = "";
                this.lblDiscountValue2_Unit.Text = "";
                this.lblDiscountValue2_Exam.Text = "";
                this.layoutDiscountValue2.ColumnStyles[1].Width = 100;
                this.layoutDiscountValue2.ColumnStyles[0].Width = 0;
            }
            else if (dcType == DCInfoRecord.DCType_PeriodDC_Free)
            {
                this.lblDiscountValue1_Name.Text = "무료 기간";
                this.lblDiscountValue1_Unit.Text = "";
                this.lblDiscountValue1_Exam.Text = "예) 2020-01-24 00:00:00~2020-01-28 00:00:00";
                this.layoutDiscountValue1.ColumnStyles[1].Width = 0;
                this.layoutDiscountValue1.ColumnStyles[0].Width = 100;

                this.lblDiscountValue2_Name.Text = "";
                this.lblDiscountValue2_Unit.Text = "";
                this.lblDiscountValue2_Exam.Text = "";
                this.layoutDiscountValue2.ColumnStyles[1].Width = 100;
                this.layoutDiscountValue2.ColumnStyles[0].Width = 0;
            }
            else if (dcType == DCInfoRecord.DCType_PeriodDC_Percent)
            {
                this.lblDiscountValue1_Name.Text = "할인 기간";
                this.lblDiscountValue1_Unit.Text = "";
                this.lblDiscountValue1_Exam.Text = "예) 2020-01-24 00:00:00~2020-01-28 00:00:00";
                this.layoutDiscountValue1.ColumnStyles[1].Width = 0;
                this.layoutDiscountValue1.ColumnStyles[0].Width = 100;

                this.lblDiscountValue2_Name.Text = "할인율 (%)";
                this.lblDiscountValue2_Unit.Text = "%   (1~99)";
                this.lblDiscountValue2_Exam.Text = "예) 50 (해당 기간 동안 주차요금 50% 할인)";
                this.layoutDiscountValue2.ColumnStyles[1].Width = 70;
                this.layoutDiscountValue2.ColumnStyles[0].Width = 30;
            }
            else if (dcType == DCInfoRecord.DCType_TimeSlotDC_Free)
            {
                this.lblDiscountValue1_Name.Text = "무료 시간대";
                this.lblDiscountValue1_Unit.Text = "";
                this.lblDiscountValue1_Exam.Text = "예) 12:00:00~14:00:00";
                this.layoutDiscountValue1.ColumnStyles[1].Width = 0;
                this.layoutDiscountValue1.ColumnStyles[0].Width = 100;

                this.lblDiscountValue2_Name.Text = "";
                this.lblDiscountValue2_Unit.Text = "";
                this.lblDiscountValue2_Exam.Text = "";
                this.layoutDiscountValue2.ColumnStyles[1].Width = 100;
                this.layoutDiscountValue2.ColumnStyles[0].Width = 0;
            }
            else if (dcType == DCInfoRecord.DCType_TimeSlotDC_Percent)
            {
                this.lblDiscountValue1_Name.Text = "무료 시간대";
                this.lblDiscountValue1_Unit.Text = "";
                this.lblDiscountValue1_Exam.Text = "예) 12:00:00~14:00:00";
                this.layoutDiscountValue1.ColumnStyles[1].Width = 0;
                this.layoutDiscountValue1.ColumnStyles[0].Width = 100;

                this.lblDiscountValue2_Name.Text = "할인율 (%)";
                this.lblDiscountValue2_Unit.Text = "%   (1~99)";
                this.lblDiscountValue2_Exam.Text = "예) 50 (해당 시간대 주차요금 50% 할인)";
                this.layoutDiscountValue2.ColumnStyles[1].Width = 70;
                this.layoutDiscountValue2.ColumnStyles[0].Width = 30;
            }
            else if (dcType == DCInfoRecord.DCType_PercentDC)
            {
                this.lblDiscountValue1_Name.Text = "할인율 (%)";
                this.lblDiscountValue1_Unit.Text = "%   (1~99)";
                this.lblDiscountValue1_Exam.Text = "예) 50 (계산된 주차요금에서 50% 할인)";
                this.layoutDiscountValue1.ColumnStyles[1].Width = 0;
                this.layoutDiscountValue1.ColumnStyles[0].Width = 100;

                this.lblDiscountValue2_Name.Text = "";
                this.lblDiscountValue2_Unit.Text = "";
                this.lblDiscountValue2_Exam.Text = "";
                this.layoutDiscountValue2.ColumnStyles[1].Width = 100;
                this.layoutDiscountValue2.ColumnStyles[0].Width = 0;
            }
            else if (dcType == DCInfoRecord.DCType_PriceDC)
            {
                this.lblDiscountValue1_Name.Text = "감면 요금 (원)";
                this.lblDiscountValue1_Unit.Text = "원   (1~ )";
                this.lblDiscountValue1_Exam.Text = "예) 3000 (계산된 주차요금에서 3000원 감면)";
                this.layoutDiscountValue1.ColumnStyles[1].Width = 0;
                this.layoutDiscountValue1.ColumnStyles[0].Width = 100;

                this.lblDiscountValue2_Name.Text = "";
                this.lblDiscountValue2_Unit.Text = "";
                this.lblDiscountValue2_Exam.Text = "";
                this.layoutDiscountValue2.ColumnStyles[1].Width = 100;
                this.layoutDiscountValue2.ColumnStyles[0].Width = 0;
            }
        }


        private void OnRadioButtonColorChange(object sender, EventArgs e)
        {
            RadioButton btn = sender as RadioButton;
            if (!btn.Checked)
            {
                btn.ForeColor = Color.WhiteSmoke;
                btn.Font = new Font(btn.Font, FontStyle.Regular);
            }
            else
            {
                btn.ForeColor = Color.Turquoise;
                btn.Font = new Font(btn.Font, FontStyle.Bold);
            }
        }


        private bool AddDiscount()
        {
            DCInfoRecord dcInfo = new DCInfoRecord();
            dcInfo.ParkNo = this.ParkNo;
            dcInfo.DCName = this.tbxDiscountName.Text.Trim();
            dcInfo.DCType = this.cbxDiscountType.SelectedIndex;
            dcInfo.DCValue1 = this.tbxDiscountValue1.Text.Trim();
            if (dcInfo.DCType == DCInfoRecord.DCType_PeriodDC_Percent ||
                dcInfo.DCType == DCInfoRecord.DCType_TimeSlotDC_Percent)
                dcInfo.DCValue2 = this.tbxDiscountValue2.Text.Trim();
            dcInfo.ButtonIndex = (this.rbtShortCut_F1.Checked ? 1 :
                                    this.rbtShortCut_F2.Checked ? 2 :
                                    this.rbtShortCut_F3.Checked ? 3 :
                                    this.rbtShortCut_F4.Checked ? 4 :
                                    this.rbtShortCut_F5.Checked ? 5 :
                                    this.rbtShortCut_F6.Checked ? 6 :
                                    this.rbtShortCut_F7.Checked ? 7 :
                                    this.rbtShortCut_F8.Checked ? 8 :
                                    this.rbtShortCut_F9.Checked ? 9 :
                                    this.rbtShortCut_F10.Checked ? 10 : -1);
            dcInfo.Memo = this.tbxMemo.Text.Trim();
            dcInfo.Memo = dcInfo.Memo.Length == 0 ? null : dcInfo.Memo;


            int dcNo = this.dataAccess.InsertDiscount_WithReturn(dcInfo);
            if (dcNo < 0)
            {
                return false;
            }
            dcInfo.DCNo = dcNo;

            this.DiscountNo = dcNo;
            this.DCInfo = dcInfo;
            
            this.Modified = true;

            return true;
        }


        private bool UpdateDiscount()
        {
            DCInfoRecord dcInfo = new DCInfoRecord();
            dcInfo.ParkNo = this.ParkNo;
            dcInfo.DCNo = this.DCInfo.DCNo;
            dcInfo.DCName = this.tbxDiscountName.Text.Trim();
            dcInfo.DCType = this.cbxDiscountType.SelectedIndex;
            dcInfo.DCValue1 = this.tbxDiscountValue1.Text.Trim();
            if (dcInfo.DCType == DCInfoRecord.DCType_PeriodDC_Percent ||
                dcInfo.DCType == DCInfoRecord.DCType_TimeSlotDC_Percent)
                dcInfo.DCValue2 = this.tbxDiscountValue2.Text.Trim();
            dcInfo.ButtonIndex = (this.rbtShortCut_F1.Checked ? 1 :
                                    this.rbtShortCut_F2.Checked ? 2 :
                                    this.rbtShortCut_F3.Checked ? 3 :
                                    this.rbtShortCut_F4.Checked ? 4 :
                                    this.rbtShortCut_F5.Checked ? 5 :
                                    this.rbtShortCut_F6.Checked ? 6 :
                                    this.rbtShortCut_F7.Checked ? 7 :
                                    this.rbtShortCut_F8.Checked ? 8 :
                                    this.rbtShortCut_F9.Checked ? 9 :
                                    this.rbtShortCut_F10.Checked ? 10 : -1);
            dcInfo.Memo = this.tbxMemo.Text.Trim();
            dcInfo.Memo = dcInfo.Memo.Length == 0 ? null : dcInfo.Memo;

            if (dcInfo.DCName != this.DCInfo.DCName ||
                dcInfo.DCType != this.DCInfo.DCType ||
                dcInfo.DCValue1 != this.DCInfo.DCValue1 ||
                dcInfo.DCValue2 != this.DCInfo.DCValue2 ||
                dcInfo.ButtonIndex != this.DCInfo.ButtonIndex ||
                dcInfo.Memo != this.DCInfo.Memo)
            {
                if (!this.dataAccess.UpdateDiscount(dcInfo))
                    return false;

                this.DCInfo = dcInfo;
                this.Modified = true;
            }

            return true;
        }


        private bool CheckForm()
        {
            int dcType = this.cbxDiscountType.SelectedIndex;

            bool result = true;

            result &= CheckTextBox_NotEmpty(this.tbxDiscountName);

            if (dcType == DCInfoRecord.DCType_TurningDC)
            {
                result &= CheckTextBox_Int(this.tbxDiscountValue1, 1);
            }
            else if (dcType == DCInfoRecord.DCType_TimeDC)
            {
                result &= CheckTextBox_Int(this.tbxDiscountValue1, 1);
            }
            else if (dcType == DCInfoRecord.DCType_PeriodDC_Free)
            {
                result &= CheckTextBox_Period(this.tbxDiscountValue1);
            }
            else if (dcType == DCInfoRecord.DCType_PeriodDC_Percent)
            {
                result &= CheckTextBox_Period(this.tbxDiscountValue1);
                result &= CheckTextBox_Int(this.tbxDiscountValue2, 1, 99);
            }
            else if (dcType == DCInfoRecord.DCType_TimeSlotDC_Free)
            {
                result &= CheckTextBox_TimeSlot(this.tbxDiscountValue1);
            }
            else if (dcType == DCInfoRecord.DCType_TimeSlotDC_Percent)
            {
                result &= CheckTextBox_TimeSlot(this.tbxDiscountValue1);
                result &= CheckTextBox_Int(this.tbxDiscountValue2, 1, 99);
            }
            else if (dcType == DCInfoRecord.DCType_PercentDC)
            {
                result &= CheckTextBox_Int(this.tbxDiscountValue1, 1, 99);
            }
            else if (dcType == DCInfoRecord.DCType_PriceDC)
            {
                result &= CheckTextBox_Int(this.tbxDiscountValue1, 1);
            }

            return result;
        }


        private bool CheckTextBox_NotEmpty(TextBox tbox)
        {
            if (tbox.Text.Trim().Length == 0)
            {
                tbox.BackColor = Color.LightPink;
                return false;
            }
            else
            {
                tbox.BackColor = Color.White;
                return true;
            }
        }


        private bool CheckTextBox_Int(TextBox tbox, int? min = null, int? max = null)
        {
            if (!int.TryParse(tbox.Text.Trim(), out int r) || (min != null && r < min) || (max != null && r > max))
            {
                tbox.BackColor = Color.LightPink;
                return false;
            }
            else
            {
                tbox.BackColor = Color.White;
                return true;
            }
        }


        private bool CheckTextBox_Period(TextBox tbox)
        {
            try
            {
                string[] tokens = tbox.Text.Trim().Split(new char[] { '~' });
                DateTime dt1 = DateTime.Parse(tokens[0]);
                DateTime dt2 = DateTime.Parse(tokens[1]);

                if (tokens.Length > 2 || dt2 <= dt1)
                    throw null;

                tbox.BackColor = Color.White;
                return true;
            }
            catch
            {
                tbox.BackColor = Color.LightPink;
                return false;
            }
        }


        private bool CheckTextBox_TimeSlot(TextBox tbox)
        {
            try
            {
                string[] tokens = tbox.Text.Trim().Split(new char[] { '~' });
                TimeSpan ts1 = ParseTimeSpan(tokens[0].Trim()).Value;
                TimeSpan ts2 = ParseTimeSpan(tokens[1].Trim()).Value;

                if (ts1 < new TimeSpan(0, 0, 0, 0) || ts1 > new TimeSpan(1, 0, 0, 0))
                    throw null;

                if (ts2 < new TimeSpan(0, 0, 0, 0) || ts2 > new TimeSpan(1, 0, 0, 0))
                    throw null;

                if (tokens.Length > 2 || ts2 <= ts1)
                    throw null;

                tbox.BackColor = Color.White;
                return true;
            }
            catch
            {
                tbox.BackColor = Color.LightPink;
                return false;
            }
        }


        public TimeSpan? ParseTimeSpan(string str)
        {
            try
            {
                string[] tokens = str.Split(new char[] { ':' });

                int hours = 0;
                int minutes = 0;
                int seconds = 0;

                if (tokens.Length > 0)
                {
                    hours = int.Parse(tokens[0]);
                }

                if (tokens.Length > 1)
                {
                    minutes = int.Parse(tokens[1]);
                }

                if (tokens.Length > 2)
                {
                    seconds = int.Parse(tokens[2]);
                }

                return new TimeSpan(hours, minutes, seconds);
            }
            catch
            {
                return null;
            }
        }
    }
}
