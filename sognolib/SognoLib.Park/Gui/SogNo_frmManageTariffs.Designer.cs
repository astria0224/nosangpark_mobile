﻿namespace SognoLib.Park.Gui
{
    partial class SogNo_frmManageTariffs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lvwTariffs = new System.Windows.Forms.ListView();
            this.btnTariffAdd = new System.Windows.Forms.Button();
            this.btnTariffDelete = new System.Windows.Forms.Button();
            this.btnTariffDetail = new System.Windows.Forms.Button();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(784, 761);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(8, 8);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 188F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(768, 745);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(236)))), ((int)(((byte)(219)))));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(762, 60);
            this.label1.TabIndex = 2;
            this.label1.Text = "요금 체계 관리";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel13, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.lvwTariffs, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 63);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(762, 491);
            this.tableLayoutPanel3.TabIndex = 3;
            // 
            // lvwTariffs
            // 
            this.lvwTariffs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwTariffs.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lvwTariffs.FullRowSelect = true;
            this.lvwTariffs.GridLines = true;
            this.lvwTariffs.HideSelection = false;
            this.lvwTariffs.Location = new System.Drawing.Point(3, 3);
            this.lvwTariffs.MultiSelect = false;
            this.lvwTariffs.Name = "lvwTariffs";
            this.lvwTariffs.Size = new System.Drawing.Size(676, 485);
            this.lvwTariffs.TabIndex = 4;
            this.lvwTariffs.UseCompatibleStateImageBehavior = false;
            this.lvwTariffs.View = System.Windows.Forms.View.Details;
            // 
            // btnTariffAdd
            // 
            this.btnTariffAdd.BackColor = System.Drawing.Color.Transparent;
            this.btnTariffAdd.BackgroundImage = global::SognoLib.Park.Properties.Resources.plus_128_default;
            this.btnTariffAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnTariffAdd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTariffAdd.FlatAppearance.BorderSize = 0;
            this.btnTariffAdd.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.btnTariffAdd.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnTariffAdd.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnTariffAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTariffAdd.Location = new System.Drawing.Point(7, 7);
            this.btnTariffAdd.Margin = new System.Windows.Forms.Padding(7);
            this.btnTariffAdd.Name = "btnTariffAdd";
            this.btnTariffAdd.Size = new System.Drawing.Size(60, 56);
            this.btnTariffAdd.TabIndex = 0;
            this.btnTariffAdd.TabStop = false;
            this.btnTariffAdd.UseVisualStyleBackColor = false;
            // 
            // btnTariffDelete
            // 
            this.btnTariffDelete.BackColor = System.Drawing.Color.Transparent;
            this.btnTariffDelete.BackgroundImage = global::SognoLib.Park.Properties.Resources.minus_128_default;
            this.btnTariffDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnTariffDelete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTariffDelete.FlatAppearance.BorderSize = 0;
            this.btnTariffDelete.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.btnTariffDelete.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnTariffDelete.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnTariffDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTariffDelete.Location = new System.Drawing.Point(7, 77);
            this.btnTariffDelete.Margin = new System.Windows.Forms.Padding(7);
            this.btnTariffDelete.Name = "btnTariffDelete";
            this.btnTariffDelete.Size = new System.Drawing.Size(60, 56);
            this.btnTariffDelete.TabIndex = 1;
            this.btnTariffDelete.TabStop = false;
            this.btnTariffDelete.UseVisualStyleBackColor = false;
            // 
            // btnTariffDetail
            // 
            this.btnTariffDetail.BackColor = System.Drawing.Color.Transparent;
            this.btnTariffDetail.BackgroundImage = global::SognoLib.Park.Properties.Resources.next_128_default;
            this.btnTariffDetail.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnTariffDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTariffDetail.FlatAppearance.BorderSize = 0;
            this.btnTariffDetail.FlatAppearance.CheckedBackColor = System.Drawing.Color.Transparent;
            this.btnTariffDetail.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnTariffDetail.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnTariffDetail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTariffDetail.Location = new System.Drawing.Point(7, 422);
            this.btnTariffDetail.Margin = new System.Windows.Forms.Padding(7);
            this.btnTariffDetail.Name = "btnTariffDetail";
            this.btnTariffDetail.Size = new System.Drawing.Size(60, 56);
            this.btnTariffDetail.TabIndex = 3;
            this.btnTariffDetail.TabStop = false;
            this.btnTariffDetail.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(204)))), ((int)(((byte)(197)))));
            this.tableLayoutPanel13.ColumnCount = 1;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Controls.Add(this.btnTariffDetail, 0, 4);
            this.tableLayoutPanel13.Controls.Add(this.btnTariffDelete, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.btnTariffAdd, 0, 0);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(685, 3);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 5;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(74, 485);
            this.tableLayoutPanel13.TabIndex = 5;
            // 
            // SogNo_frmManageTariffs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 761);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SogNo_frmManageTariffs";
            this.Text = "요금 체계 관리";
            this.TopMost = true;
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.ListView lvwTariffs;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Button btnTariffDetail;
        private System.Windows.Forms.Button btnTariffDelete;
        private System.Windows.Forms.Button btnTariffAdd;
    }
}