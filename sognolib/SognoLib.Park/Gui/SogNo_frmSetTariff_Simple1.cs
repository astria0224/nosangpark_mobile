﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SognoLib.Park.Data;


namespace SognoLib.Park.Gui
{
    public partial class SogNo_frmSetTariff_Simple1 : Form
    {
        private int ParkNo { get; set; }
        private int? TariffNo { get; set; } = null;

        SogNo_TariffDataAccess dataAccess = null;

        TariffRecord Tariff = null;
        DayTariffRecord DayTariff = null;
        TimeTariffRecord TimeTariff = null;
        DCInfoRecord TurningDC = null;
        DCInfoRecord PercentDC = null;
        DCInfoRecord PriceDC = null;

        public bool Modified { get; private set; } = false;


        public SogNo_frmSetTariff_Simple1(int parkNo, SogNo_TariffDataAccess dataAccess)
        {
            this.ParkNo = parkNo;

            InitializeComponent();

            this.dataAccess = dataAccess;

            this.btnApplyTariff.MouseClick += OnButtonClicked_ApplyTariff;
        }


        public bool ReadData(int tariffNo)
        {
            this.TariffNo = tariffNo;

            try
            {
                TariffRecord trf = this.dataAccess.ReadTariff(this.ParkNo, tariffNo);
                if (trf != null)
                {
                    return ReadData(trf);
                }
            }
            catch
            {
                return false;
            }

            return false;
        }


        private bool ReadData(TariffRecord tariff)
        {
            if (tariff.DayTariffs == null || tariff.DayTariffs.Length == 0)
                return false;

            List<DayTariffRecord> dayTrfs = ReadDayTariffs(tariff, out DayTariffRecord dayTrf);
            if (dayTrfs == null || dayTrfs.Count != 7)
                return false;

            if (dayTrf.TimeTariffs == null || dayTrf.TimeTariffs.Length == 0)
                return false;

            TimeTariffRecord timeTrf = ReadTimeTariff(dayTrf);
            if (timeTrf == null)
                return false;

            List<DCInfoRecord> discounts = null;
            if (tariff.DefaultDCs != null && tariff.DefaultDCs.Trim().Length > 0)
                discounts = ReadDefaultDiscounts(tariff);
            else
                discounts = new List<DCInfoRecord>();

            if (discounts == null)
                return false;


            DCInfoRecord turningDC = discounts.Find(it => it.DCType == DCInfoRecord.DCType_TurningDC);
            DCInfoRecord percentDC = discounts.Find(it => it.DCType == DCInfoRecord.DCType_PercentDC);
            DCInfoRecord priceDC = discounts.Find(it => it.DCType == DCInfoRecord.DCType_PriceDC);


            this.tbxTariffName.Text = tariff.TrfName;
            this.tbxTurningTime.Text = (turningDC == null ? "0" : turningDC.DCValue1);
            this.tbxBaseTime.Text = timeTrf.BaseTime?.ToString();
            this.tbxBaseRates.Text = timeTrf.BaseRates?.ToString();
            this.tbxAddTime.Text = timeTrf.AddTime?.ToString();
            this.tbxAddRates.Text = timeTrf.AddRates?.ToString();
            this.tbxDailyMaxRates.Text = tariff.DailyMaxRates?.ToString();
            this.tbxTotalMaxRates.Text = tariff.MaxRates?.ToString();

            if (tariff.DailyCriterion == TariffRecord.DailyCriterion_Day) this.rbtDailyCriterion_Date.Checked = true;
            else if (tariff.DailyCriterion == TariffRecord.DailyCriterion_InTime) this.rbtDailyCriterion_InTime.Checked = true;
            else
            {
                return false;
            }

            if (tariff.RoundType == TariffRecord.RoundType_None) this.rbtRoundType_None.Checked = true;
            else if (tariff.RoundType == TariffRecord.RoundType_Ceiling) this.rbtRoundType_Ceiling.Checked = true;
            else if (tariff.RoundType == TariffRecord.RoundType_Floor) this.rbtRoundType_Floor.Checked = true;
            else if (tariff.RoundType == TariffRecord.RoundType_Round) this.rbtRoundType_Round.Checked = true;
            else
            {
                return false;
            }

            if (tariff.RoundDecimal == 10) this.rbtRoundDecimal_10.Checked = true;
            else if (tariff.RoundDecimal == 100) this.rbtRoundDecimal_100.Checked = true;
            else
            {
                return false;
            }

            this.cbxFreeDay_Saturday.Checked = (dayTrfs[TariffRecord.DayIndex_Saturday] == null);
            this.cbxFreeDay_Sunday.Checked = (dayTrfs[TariffRecord.DayIndex_Sunday] == null);
            this.cbxFreeDay_Holiday.Checked = (tariff.HolidayTariff == null || tariff.HolidayTariff.Value < 0);

            this.tbxPercentDC.Text = (percentDC == null ? "0" : percentDC.DCValue1);
            this.tbxPriceDC.Text = (priceDC == null ? "0" : priceDC.DCValue1);

            this.Tariff = tariff;
            this.DayTariff = dayTrf;
            this.TimeTariff = timeTrf;
            this.TurningDC = turningDC;
            this.PercentDC = percentDC;
            this.PriceDC = priceDC;

            return true;
        }


        private List<DayTariffRecord> ReadDayTariffs(TariffRecord trf, out DayTariffRecord dayTrf)
        {
            dayTrf = null;
            string[] tokens = trf.DayTariffs.Split(new char[] { ',' });
            if (tokens.Length != 7)
                return null;

            List<int> dayTrfIDs = new List<int>();
            int dayTrfID = -1;
            foreach (var token in tokens)
            {
                int no = int.Parse(token);
                dayTrfIDs.Add(no);

                if (no < 0)
                    continue;
                else if (dayTrfID < 0)
                    dayTrfID = no;
                else if (dayTrfID != no)
                    return null;
            }

            if (dayTrfID < 0 || (trf.HolidayTariff != null && trf.HolidayTariff.Value >= 0 && trf.HolidayTariff != dayTrfID))
                return null;


            dayTrf = this.dataAccess.ReadDayTariff(this.ParkNo, dayTrfID);
            if (dayTrf == null)
                return null;

            List<DayTariffRecord> dayTrfs = new List<DayTariffRecord>();
            foreach (int id in dayTrfIDs)
            {
                if (id < 0)
                    dayTrfs.Add(null);
                else
                    dayTrfs.Add(dayTrf);
            }

            return dayTrfs;
        }


        private TimeTariffRecord ReadTimeTariff(DayTariffRecord dayTrf)
        {
            string[] tokens = dayTrf.TimeTariffs.Split(new char[] { ',' });
            if (tokens.Length != 1)
                return null;

            int timeTrfID = int.Parse(tokens[0]);
            if (timeTrfID < 0)
                return null;
            
            TimeTariffRecord timeTrf = this.dataAccess.ReadTimeTariff(this.ParkNo, timeTrfID);

            return timeTrf;
        }


        private List<DCInfoRecord> ReadDefaultDiscounts(TariffRecord trf)
        {
            if (trf.DefaultDCs == null || trf.DefaultDCs.Trim().Length == 0)
                return null;

            List<DCInfoRecord> dcs = new List<DCInfoRecord>();

            string[] tokens = trf.DefaultDCs.Trim().Split(new char[] { ',' });
            foreach (var token in tokens)
            {
                int no = int.Parse(token);

                DCInfoRecord dc = this.dataAccess.ReadDiscount(this.ParkNo, no);
                if (dc == null)
                    return null;

                dcs.Add(dc);
            }

            return dcs;
        }


        private void OnButtonClicked_ApplyTariff(object sender, MouseEventArgs args)
        {
            if (!CheckForm())
                return;

            if (this.TariffNo == null || this.TariffNo < 0)
            {
                bool ret = AddTariff();
                if (ret)
                {
                    this.DialogResult = DialogResult.OK;
                    Close();
                }
            }
            else
            {
                bool ret = UpdateTariff();
                if (ret)
                {
                    this.DialogResult = DialogResult.OK;
                    Close();
                }
            }
        }


        private bool AddTariff()
        {
            TimeTariffRecord timeTrf = new TimeTariffRecord();
            timeTrf.ParkNo = this.ParkNo;
            timeTrf.StartTime = new TimeSpan(0, 0, 0, 0);
            timeTrf.EndTime = new TimeSpan(1, 0, 0, 0);
            timeTrf.BaseTime = int.Parse(this.tbxBaseTime.Text.Trim());
            timeTrf.BaseRates = int.Parse(this.tbxBaseRates.Text.Trim());
            timeTrf.AddTime = int.Parse(this.tbxAddTime.Text.Trim());
            timeTrf.AddRates = int.Parse(this.tbxAddRates.Text.Trim());

            int timeTrfNo = this.dataAccess.InsertTimeTariff_WithReturn(timeTrf);
            if (timeTrfNo < 0)
            {
                return false;
            }
            timeTrf.TimeTrfNo = timeTrfNo;

            DayTariffRecord dayTrf = new DayTariffRecord();
            dayTrf.ParkNo = this.ParkNo;
            dayTrf.TimeTariffs = timeTrfNo.ToString();

            int dayTrfNo = this.dataAccess.InsertDayTariff_WithReturn(dayTrf);
            if (dayTrfNo < 0)
            {
                return false;
            }
            dayTrf.DayTrfNo = dayTrfNo;

            int turnDCNo = -1;
            int turnTime = int.Parse(this.tbxTurningTime.Text.Trim());
            DCInfoRecord turningDC = null;
            if (turnTime > 0)
            {
                turningDC = new DCInfoRecord();
                turningDC.ParkNo = this.ParkNo;
                turningDC.DCName = "(요금제) 회차";
                turningDC.DCType = DCInfoRecord.DCType_TurningDC;
                turningDC.DCValue1 = turnTime.ToString();

                turnDCNo = this.dataAccess.InsertDiscount_WithReturn(turningDC);
                if (turnDCNo < 0)
                {
                    return false;
                }
                turningDC.DCNo = turnDCNo;
            }


            int percentDCNo = -1;
            int percentDCVal = int.Parse(this.tbxPercentDC.Text.Trim());
            DCInfoRecord percentDC = null;
            if (percentDCVal > 0)
            {
                percentDC = new DCInfoRecord();
                percentDC.ParkNo = this.ParkNo;
                percentDC.DCName = "(요금제) 퍼센트 할인";
                percentDC.DCType = DCInfoRecord.DCType_PercentDC;
                percentDC.DCValue1 = percentDCVal.ToString();

                percentDCNo = this.dataAccess.InsertDiscount_WithReturn(percentDC);
                if (percentDCNo < 0)
                {
                    return false;
                }
                percentDC.DCNo = percentDCNo;
            }

            int priceDCNo = -1;
            int priceDCVal = int.Parse(this.tbxPriceDC.Text.Trim());
            DCInfoRecord priceDC = null;
            if (priceDCVal > 0)
            {
                priceDC = new DCInfoRecord();
                priceDC.ParkNo = this.ParkNo;
                priceDC.DCName = "(요금제) 요금 감면";
                priceDC.DCType = DCInfoRecord.DCType_PriceDC;
                priceDC.DCValue1 = priceDCVal.ToString();

                priceDCNo = this.dataAccess.InsertDiscount_WithReturn(priceDC);
                if (priceDCNo < 0)
                {
                    return false;
                }
                priceDC.DCNo = priceDCNo;
            }


            TariffRecord trf = new TariffRecord();
            trf.ParkNo = this.ParkNo;
            trf.TrfName = this.tbxTariffName.Text.Trim();
            trf.MaxRates = int.Parse(this.tbxTotalMaxRates.Text.Trim());
            trf.DailyMaxRates = int.Parse(this.tbxDailyMaxRates.Text.Trim());
            trf.DailyCriterion = (this.rbtDailyCriterion_Date.Checked ? TariffRecord.DailyCriterion_Day : TariffRecord.DailyCriterion_InTime);
            trf.RoundType = (this.rbtRoundType_None.Checked ? TariffRecord.RoundType_None :
                                this.rbtRoundType_Ceiling.Checked ? TariffRecord.RoundType_Ceiling :
                                this.rbtRoundType_Floor.Checked ? TariffRecord.RoundType_Floor : TariffRecord.RoundType_Round);
            trf.RoundDecimal = (this.rbtRoundDecimal_10.Checked ? 10 : 100);

            int[] dayTrfIDs = new int[7];
            dayTrfIDs[TariffRecord.DayIndex_Sunday] = (this.cbxFreeDay_Sunday.Checked ? -1 : dayTrfNo);
            dayTrfIDs[TariffRecord.DayIndex_Monday] = dayTrfNo;
            dayTrfIDs[TariffRecord.DayIndex_Tuesday] = dayTrfNo;
            dayTrfIDs[TariffRecord.DayIndex_Wednesday] = dayTrfNo;
            dayTrfIDs[TariffRecord.DayIndex_Thursday] = dayTrfNo;
            dayTrfIDs[TariffRecord.DayIndex_Friday] = dayTrfNo;
            dayTrfIDs[TariffRecord.DayIndex_Saturday] = (this.cbxFreeDay_Saturday.Checked ? -1 : dayTrfNo);

            trf.DayTariffs = string.Format("{0},{1},{2},{3},{4},{5},{6}", dayTrfIDs[0], dayTrfIDs[1], dayTrfIDs[2], dayTrfIDs[3], dayTrfIDs[4], dayTrfIDs[5], dayTrfIDs[6]);

            if (!this.cbxFreeDay_Holiday.Checked)
                trf.HolidayTariff = dayTrfNo;
            trf.HolidayPrior = "1111111";
            
            foreach (int no in new int[3] { turnDCNo, percentDCNo, priceDCNo })
            {
                if (no >= 0)
                {
                    if (trf.DefaultDCs == null || trf.DefaultDCs.Length == 0)
                    {
                        trf.DefaultDCs = no.ToString();
                    }
                    else
                    {
                        trf.DefaultDCs += ",";
                        trf.DefaultDCs += no.ToString();
                    }
                }
            }

            int trfNo = this.dataAccess.InsertTariff_WithReturn(trf);
            if (trfNo < 0)
            {
                return false;
            }
            trf.TrfNo = trfNo;

            this.TariffNo = trfNo;
            this.Tariff = trf;
            this.DayTariff = dayTrf;
            this.TimeTariff = timeTrf;
            this.TurningDC = turningDC;
            this.PercentDC = percentDC;
            this.PriceDC = priceDC;

            this.Modified = true;

            return true;
        }


        private bool UpdateTariff()
        {
            TimeTariffRecord timeTrf = new TimeTariffRecord();
            timeTrf.ParkNo = this.ParkNo;
            timeTrf.TimeTrfNo = this.TimeTariff.TimeTrfNo;
            timeTrf.StartTime = this.TimeTariff.StartTime;
            timeTrf.EndTime = this.TimeTariff.EndTime;
            timeTrf.BaseTime = int.Parse(this.tbxBaseTime.Text.Trim());
            timeTrf.BaseRates = int.Parse(this.tbxBaseRates.Text.Trim());
            timeTrf.AddTime = int.Parse(this.tbxAddTime.Text.Trim());
            timeTrf.AddRates = int.Parse(this.tbxAddRates.Text.Trim());
            
            if (timeTrf.BaseTime != this.TimeTariff.BaseTime ||
                timeTrf.BaseRates != this.TimeTariff.BaseRates ||
                timeTrf.AddTime != this.TimeTariff.AddTime ||
                timeTrf.AddRates != this.TimeTariff.AddRates)
            {
                if (!this.dataAccess.UpdateTimeTariff(timeTrf))
                    return false;
            }

            int turnTime = int.Parse(this.tbxTurningTime.Text.Trim());
            DCInfoRecord turningDC = null;
            if (turnTime > 0)
            {
                turningDC = new DCInfoRecord();
                turningDC.ParkNo = this.ParkNo;
                turningDC.DCNo = this.TurningDC?.DCNo;
                turningDC.DCName = "(요금제) 회차";
                turningDC.DCType = DCInfoRecord.DCType_TurningDC;
                turningDC.DCValue1 = turnTime.ToString();

                if (this.TurningDC == null)
                {
                    int no = this.dataAccess.InsertDiscount_WithReturn(turningDC);
                    if (no < 0)
                    {
                        return false;
                    }
                    turningDC.DCNo = no;
                }
                else if (turningDC.DCValue1 != this.TurningDC.DCValue1)
                {
                    if (!this.dataAccess.UpdateDiscount(turningDC))
                        return false;
                }
            }


            int percentDCVal = int.Parse(this.tbxPercentDC.Text.Trim());
            DCInfoRecord percentDC = null;
            if (percentDCVal > 0)
            {
                percentDC = new DCInfoRecord();
                percentDC.ParkNo = this.ParkNo;
                percentDC.DCNo = this.PercentDC?.DCNo;
                percentDC.DCName = "(요금제) 퍼센트 할인";
                percentDC.DCType = DCInfoRecord.DCType_PercentDC;
                percentDC.DCValue1 = percentDCVal.ToString();

                if (this.PercentDC == null)
                {
                    int no = this.dataAccess.InsertDiscount_WithReturn(percentDC);
                    if (no < 0)
                    {
                        return false;
                    }
                    percentDC.DCNo = no;
                }
                else if (percentDC.DCValue1 != this.PercentDC.DCValue1)
                {
                    if (!this.dataAccess.UpdateDiscount(percentDC))
                        return false;
                }
            }


            int priceDCVal = int.Parse(this.tbxPriceDC.Text.Trim());
            DCInfoRecord priceDC = null;
            if (priceDCVal > 0)
            {
                priceDC = new DCInfoRecord();
                priceDC.ParkNo = this.ParkNo;
                priceDC.DCNo = this.PriceDC?.DCNo;
                priceDC.DCName = "(요금제) 요금 감면";
                priceDC.DCType = DCInfoRecord.DCType_PriceDC;
                priceDC.DCValue1 = priceDCVal.ToString();

                if (this.PriceDC == null)
                {
                    int no = this.dataAccess.InsertDiscount_WithReturn(priceDC);
                    if (no < 0)
                    {
                        return false;
                    }
                    priceDC.DCNo = no;
                }
                else if (priceDC.DCValue1 != this.PriceDC?.DCValue1)
                {
                    if (!this.dataAccess.UpdateDiscount(priceDC))
                        return false;
                }
            }


            TariffRecord trf = new TariffRecord();
            trf.ParkNo = this.ParkNo;
            trf.TrfNo = this.Tariff.TrfNo;
            trf.TrfName = this.tbxTariffName.Text.Trim();
            trf.MaxRates = int.Parse(this.tbxTotalMaxRates.Text.Trim());
            trf.DailyMaxRates = int.Parse(this.tbxDailyMaxRates.Text.Trim());
            trf.DailyCriterion = (this.rbtDailyCriterion_Date.Checked ? TariffRecord.DailyCriterion_Day : TariffRecord.DailyCriterion_InTime);
            trf.RoundType = (this.rbtRoundType_None.Checked ? TariffRecord.RoundType_None :
                                this.rbtRoundType_Ceiling.Checked ? TariffRecord.RoundType_Ceiling :
                                this.rbtRoundType_Floor.Checked ? TariffRecord.RoundType_Floor : TariffRecord.RoundType_Round);
            trf.RoundDecimal = (this.rbtRoundDecimal_10.Checked ? 10 : 100);

            int[] dayTrfIDs = new int[7];
            dayTrfIDs[TariffRecord.DayIndex_Sunday] = (this.cbxFreeDay_Sunday.Checked ? -1 : this.DayTariff.DayTrfNo.Value);
            dayTrfIDs[TariffRecord.DayIndex_Monday] = this.DayTariff.DayTrfNo.Value;
            dayTrfIDs[TariffRecord.DayIndex_Tuesday] = this.DayTariff.DayTrfNo.Value;
            dayTrfIDs[TariffRecord.DayIndex_Wednesday] = this.DayTariff.DayTrfNo.Value;
            dayTrfIDs[TariffRecord.DayIndex_Thursday] = this.DayTariff.DayTrfNo.Value;
            dayTrfIDs[TariffRecord.DayIndex_Friday] = this.DayTariff.DayTrfNo.Value;
            dayTrfIDs[TariffRecord.DayIndex_Saturday] = (this.cbxFreeDay_Saturday.Checked ? -1 : this.DayTariff.DayTrfNo.Value);

            trf.DayTariffs = string.Format("{0},{1},{2},{3},{4},{5},{6}", dayTrfIDs[0], dayTrfIDs[1], dayTrfIDs[2], dayTrfIDs[3], dayTrfIDs[4], dayTrfIDs[5], dayTrfIDs[6]);

            if (!this.cbxFreeDay_Holiday.Checked)
                trf.HolidayTariff = this.DayTariff.DayTrfNo.Value;
            trf.HolidayPrior = "1111111";


            int turnDCNo = (turningDC == null ? -1 : turningDC.DCNo.Value);
            int percentDCNo = (percentDC == null ? -1 : percentDC.DCNo.Value);
            int priceDCNo = (priceDC == null ? -1 : priceDC.DCNo.Value);
            foreach (int no in new int[3] { turnDCNo, percentDCNo, priceDCNo })
            {
                if (no >= 0)
                {
                    if (trf.DefaultDCs == null || trf.DefaultDCs.Length == 0)
                    {
                        trf.DefaultDCs = no.ToString();
                    }
                    else
                    {
                        trf.DefaultDCs += ",";
                        trf.DefaultDCs += no.ToString();
                    }
                }
            }

            if (trf.TrfName != this.Tariff.TrfName ||
                trf.MaxRates != this.Tariff.MaxRates ||
                trf.DailyMaxRates != this.Tariff.DailyMaxRates ||
                trf.DailyCriterion != this.Tariff.DailyCriterion ||
                trf.RoundType != this.Tariff.RoundType ||
                trf.RoundDecimal != this.Tariff.RoundDecimal ||
                trf.DayTariffs != this.Tariff.DayTariffs ||
                trf.HolidayTariff != this.Tariff.HolidayTariff ||
                trf.DefaultDCs != this.Tariff.DefaultDCs)
            {
                if (!this.dataAccess.UpdateTariff(trf))
                    return false;

                this.Modified = true;
            }
            
            this.Tariff = trf;
            this.TimeTariff = timeTrf;
            this.TurningDC = turningDC;
            this.PercentDC = percentDC;
            this.PriceDC = priceDC;

            return true;
        }


        private bool CheckForm()
        {
            bool result = true;

            result &= CheckTextBox_NotEmpty(this.tbxTariffName);
            result &= CheckTextBox_UnsignedInt(this.tbxTurningTime);
            result &= CheckTextBox_UnsignedInt(this.tbxBaseTime);
            result &= CheckTextBox_UnsignedInt(this.tbxBaseRates);
            result &= CheckTextBox_UnsignedInt(this.tbxAddTime, 1);
            result &= CheckTextBox_UnsignedInt(this.tbxAddRates);
            result &= CheckTextBox_Int(this.tbxDailyMaxRates);
            result &= CheckTextBox_Int(this.tbxTotalMaxRates);
            result &= CheckTextBox_UnsignedInt(this.tbxPercentDC, null, 99);
            result &= CheckTextBox_UnsignedInt(this.tbxPriceDC);

            return result;
        }


        private bool CheckTextBox_NotEmpty(TextBox tbox)
        {
            if (tbox.Text.Trim().Length == 0)
            {
                tbox.BackColor = Color.LightPink;
                return false;
            }
            else
            {
                tbox.BackColor = Color.White;
                return true;
            }
        }


        private bool CheckTextBox_UnsignedInt(TextBox tbox, int? min = null, int? max = null)
        {
            if (!int.TryParse(tbox.Text.Trim(), out int r) || r < 0 || (min != null && r < min) || (max != null && r > max))
            {
                tbox.BackColor = Color.LightPink;
                return false;
            }
            else
            {
                tbox.BackColor = Color.White;
                return true;
            }
        }


        private bool CheckTextBox_Int(TextBox tbox, int? min = null, int? max = null)
        {
            if (!int.TryParse(tbox.Text.Trim(), out int r) || (min != null && r < min) || (max != null && r > max))
            {
                tbox.BackColor = Color.LightPink;
                return false;
            }
            else
            {
                tbox.BackColor = Color.White;
                return true;
            }
        }
    }
}
