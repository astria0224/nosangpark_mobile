﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using SognoLib.Park.Data;


namespace SognoLib.Park.Gui
{
    public partial class SogNo_frmManageDiscounts : Form
    {
        private int ParkNo { get; set; }

        SogNo_TariffDataAccess dataAccess = null;

        SogNo_frmSetDiscount frmDiscountDetail = null;

        public bool Modified { get; private set; } = false;


        public SogNo_frmManageDiscounts(int parkNo, SogNo_TariffDataAccess dataAccess)
        {
            this.ParkNo = parkNo;

            InitializeComponent();

            InitializeListview();

            this.dataAccess = dataAccess;

            this.lvwDiscounts.ItemSelectionChanged += OnItemSelectionChanged_Discounts;
            this.lvwDiscounts.MouseDoubleClick += OnMouseDoubleClicked_Discounts;

            this.btnDiscountAdd.MouseClick += OnButtonClicked_DiscountAdd;
            this.btnDiscountAdd.EnabledChanged += OnEnabledChanged_DiscountAdd;
            this.btnDiscountAdd.MouseEnter += OnMouseEnter_DiscountAdd;
            this.btnDiscountAdd.MouseLeave += OnEnabledChanged_DiscountAdd;

            this.btnDiscountDelete.EnabledChanged += OnEnabledChanged_DiscountDelete;
            this.btnDiscountDelete.MouseEnter += OnMouseEnter_DiscountDelete;
            this.btnDiscountDelete.MouseLeave += OnEnabledChanged_DiscountDelete;

            this.btnDiscountDetail.MouseClick += OnButtonClicked_DiscountDetail;
            this.btnDiscountDetail.EnabledChanged += OnEnabledChanged_DiscountDetail;
            this.btnDiscountDetail.MouseEnter += OnMouseEnter_DiscountDetail;
            this.btnDiscountDetail.MouseLeave += OnEnabledChanged_DiscountDetail;

            this.btnDiscountDelete.Enabled = false;
            this.btnDiscountDetail.Enabled = false;
        }


        private void InitializeListview()
        {
            this.lvwDiscounts.Columns.Add("번호", 50, HorizontalAlignment.Left);
            this.lvwDiscounts.Columns.Add("단축키", -2, HorizontalAlignment.Center);
            this.lvwDiscounts.Columns.Add("이름", 200, HorizontalAlignment.Left);
            this.lvwDiscounts.Columns.Add("종류", 120, HorizontalAlignment.Center);
            this.lvwDiscounts.Columns.Add("값1", 160, HorizontalAlignment.Center);
            this.lvwDiscounts.Columns.Add("값2", -2, HorizontalAlignment.Center);
        }
        

        public bool ReadData()
        {
            this.lvwDiscounts.Items.Clear();

            if (this.dataAccess == null)
            {
                return false;
            }

            DCInfoRecord[] discounts = this.dataAccess.ReadAllDiscounts(this.ParkNo) ?? new DCInfoRecord[0];
            foreach (var dc in discounts)
            {
                ListViewItem item = new ListViewItem(dc.DCNo?.ToString());
                item.SubItems.Add((dc.ButtonIndex ?? -1) == -1 ? "" : "F" + dc.ButtonIndex.ToString());
                item.SubItems.Add(dc.DCName);
                item.SubItems.Add(dc.DiscountTypeString());
                item.SubItems.Add(dc.DCValue1);
                item.SubItems.Add(dc.DCValue2);

                this.lvwDiscounts.Items.Add(item);
            }

            return true;
        }


        public void OnItemSelectionChanged_Discounts(object sender, ListViewItemSelectionChangedEventArgs args)
        {
            this.btnDiscountDetail.Enabled = args.IsSelected;
        }


        public void OnMouseDoubleClicked_Discounts(object sender, MouseEventArgs args)
        {
            ListView.SelectedIndexCollection selected = this.lvwDiscounts.SelectedIndices;
            if (selected.Count == 1)
            {
                OnButtonClicked_DiscountDetail(null, null);
            }
        }


        public void OnButtonClicked_DiscountAdd(object sender, MouseEventArgs args)
        {
            if (this.frmDiscountDetail != null)
            {
                this.frmDiscountDetail.Close();
            }

            this.frmDiscountDetail = new SogNo_frmSetDiscount(this.ParkNo, this.dataAccess);
            this.frmDiscountDetail.FormClosed += OnFormClosed_DiscountDetail;
            this.frmDiscountDetail.StartPosition = FormStartPosition.Manual;
            this.frmDiscountDetail.Location = new Point(this.Location.X + this.ClientSize.Width + 5, this.Location.Y);

            this.frmDiscountDetail.Show();
        }


        public void OnEnabledChanged_DiscountAdd(object sender, EventArgs args)
        {
            if (this.btnDiscountAdd.Enabled)
                this.btnDiscountAdd.BackgroundImage = Properties.Resources.plus_128_default;
            else
                this.btnDiscountAdd.BackgroundImage = Properties.Resources.plus_128_disabled;
        }


        public void OnMouseEnter_DiscountAdd(object sender, EventArgs args)
        {
            if (this.btnDiscountAdd.Enabled)
                this.btnDiscountAdd.BackgroundImage = Properties.Resources.plus_128_mouseover;
        }


        public void OnEnabledChanged_DiscountDelete(object sender, EventArgs args)
        {
            if (this.btnDiscountDelete.Enabled)
                this.btnDiscountDelete.BackgroundImage = Properties.Resources.minus_128_default;
            else
                this.btnDiscountDelete.BackgroundImage = Properties.Resources.minus_128_disabled;
        }


        public void OnMouseEnter_DiscountDelete(object sender, EventArgs args)
        {
            if (this.btnDiscountDelete.Enabled)
                this.btnDiscountDelete.BackgroundImage = Properties.Resources.minus_128_mouseover;
        }


        public void OnButtonClicked_DiscountDetail(object sender, MouseEventArgs args)
        {
            ListView.SelectedIndexCollection selected = this.lvwDiscounts.SelectedIndices;
            if (selected.Count == 0 || this.dataAccess == null)
            {
                return;
            }

            if (this.frmDiscountDetail == null)
            {
                this.frmDiscountDetail = new SogNo_frmSetDiscount(this.ParkNo, this.dataAccess);
                this.frmDiscountDetail.FormClosed += OnFormClosed_DiscountDetail;
                this.frmDiscountDetail.StartPosition = FormStartPosition.Manual;
                this.frmDiscountDetail.Location = new Point(this.Location.X + this.ClientSize.Width + 5, this.Location.Y);
            }

            ListViewItem item = this.lvwDiscounts.Items[selected[0]];
            int dcNo = int.Parse(item.SubItems[0].Text);

            this.frmDiscountDetail.ReadData(dcNo);

            this.frmDiscountDetail.Show();
        }


        public void OnEnabledChanged_DiscountDetail(object sender, EventArgs args)
        {
            if (this.btnDiscountDetail.Enabled)
                this.btnDiscountDetail.BackgroundImage = Properties.Resources.next_128_default;
            else
                this.btnDiscountDetail.BackgroundImage = Properties.Resources.next_128_disabled;
        }


        public void OnMouseEnter_DiscountDetail(object sender, EventArgs args)
        {
            if (this.btnDiscountDetail.Enabled)
                this.btnDiscountDetail.BackgroundImage = Properties.Resources.next_128_mouseover;
        }


        public void OnFormClosed_DiscountDetail(object sender, FormClosedEventArgs args)
        {
            if (this.frmDiscountDetail.DialogResult == DialogResult.OK &&
                this.frmDiscountDetail.Modified)
            {
                ReadData();

                this.Modified = true;
            }

            this.frmDiscountDetail = null;
        }
    }
}
