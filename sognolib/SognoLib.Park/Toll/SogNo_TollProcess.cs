﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SognoLib.Park.Toll
{
    /// <summary>
    /// 요금제 관리 및 주차요금 계산 클래스
    /// </summary>
    public class SogNo_TollProcess
    {
        /// <summary>
        /// 주차 기간을 요금제별로 나눈 세부 구간
        /// </summary>
        public class ParkingDetail
        {
            public string TrfKey { get; private set; }              // 세부 구간에 적용할 요금제ID
            public DateTime StartDateTime { get; private set; }     // 세부 구간의 시작 시각
            public DateTime EndDateTime { get; private set; }       // 세부 구간의 종료 시각

            public ParkingDetail(string trfKey, DateTime startDateTime, DateTime endDateTime)
            {
                this.TrfKey = trfKey;
                this.StartDateTime = startDateTime;
                this.EndDateTime = endDateTime;
            }
        }

        // 요금제 목록
        private Dictionary<string, SogNo_Tariff> Tariffs = new Dictionary<string, SogNo_Tariff>();

        // 미정의 구간에서 사용할 무료 요금제
        private SogNo_Tariff DefaultFreeTariff = null;


        public SogNo_TollProcess()
        {
            this.DefaultFreeTariff = new SogNo_Tariff("요금제 없음", null, null, null, null, null, null, null, null);
        }

        /// <summary>
        /// 요금제 등록
        /// </summary>
        /// <param name="key">요금제를 구분할 수 있는 요금제ID</param>
        /// <param name="tariff">요금제</param>
        public void AddTariff(string key, SogNo_Tariff tariff)
        {
            if (this.Tariffs.ContainsKey(key))
                this.Tariffs[key] = tariff;
            else
                this.Tariffs.Add(key, tariff);
        }

        /// <summary>
        /// 요금제 제거
        /// </summary>
        /// <param name="key">요금제ID</param>
        public void RemoveTariff(string key)
        {
            if (this.Tariffs.ContainsKey(key))
                this.Tariffs.Remove(key);
        }

        /// <summary>
        /// 요금제 전체 제거
        /// </summary>
        public void RemoveAllTariffs()
        {
            this.Tariffs.Clear();
        }

        /// <summary>
        /// 주차 요금을 계산
        /// </summary>
        /// <param name="parkingDetails">주차한 기간과 적용할 요금제 정보</param>
        /// <param name="discounts">추가로 적용할 할인 목록</param>
        /// <returns>요금을 계산한 계산서</returns>
        public SogNo_Bill Toll(ParkingDetail[] parkingDetails, ISogNo_Discount[] discounts)
        {
            if (parkingDetails == null || parkingDetails.Length == 0)
                return null;

            SogNo_Bill bill = _Toll(parkingDetails, true, discounts);
            SogNo_Bill bill_nodc = _Toll(parkingDetails, false, null);

            // 할인 안한 요금
            bill.OriginalPrice = bill_nodc.Price + bill_nodc.Discount_RoundDC;

            int totalNoDC = bill_nodc.TotalDailyPrices();
            if (bill_nodc.MaxRates >= 0 && totalNoDC > bill_nodc.MaxRates)
                totalNoDC = bill_nodc.MaxRates;

            int totalDC = bill.TotalDailyPrices();
            if (bill.MaxRates >= 0 && totalDC > bill.MaxRates)
                totalDC = bill.MaxRates;

            // 기간 무료, 기간 할인, 시간대 무료, 시간대 할인, 시간 감면으로 인한 할인액.
            bill.Discount_TimePeriodDC = totalNoDC - totalDC;

            return bill;
        }

        /// <summary>
        /// 주차 요금을 계산
        /// </summary>
        /// <param name="parkingDetails">주차한 기간과 적용할 요금제 정보</param>
        /// <param name="applyDiscounts">할인(추가할인 및 기본할인)을 적용할지 여부</param>
        /// <param name="discounts">추가 할인 목록</param>
        /// <returns>요금을 계산한 계산서</returns>
        private SogNo_Bill _Toll(ParkingDetail[] parkingDetails, bool applyDiscounts, ISogNo_Discount[] discounts)
        {
            DateTime inDateTime = parkingDetails.First().StartDateTime;
            DateTime outDateTime = parkingDetails.Last().EndDateTime;

            SogNo_Bill bill = new SogNo_Bill(inDateTime, outDateTime);

            DateTime last = bill.InDateTime;
            SogNo_Tariff lastTariff = null;

            foreach (var info in parkingDetails)
            {
                if (last < info.StartDateTime)
                {
                    last = this.DefaultFreeTariff.Apply(bill, last, info.StartDateTime, applyDiscounts, discounts);
                    lastTariff = this.DefaultFreeTariff;
                }

                if (Tariffs.TryGetValue(info.TrfKey, out SogNo_Tariff tariff))
                {
                    last = tariff.Apply(bill, last, info.EndDateTime, applyDiscounts, discounts);
                    lastTariff = tariff;
                }
                else
                {
                    last = this.DefaultFreeTariff.Apply(bill, last, info.EndDateTime, applyDiscounts, discounts);
                    lastTariff = this.DefaultFreeTariff;
                }
            }

            lastTariff.Toll(bill, applyDiscounts, discounts);

            return bill;
        }
    }
}
