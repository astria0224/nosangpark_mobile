﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SognoLib.Park.Toll
{
    /// <summary>
    /// 일 요금제
    /// 
    /// - 각 요일 혹은 공휴일의 요금을 계산하는 방식을 정의
    /// </summary>
    public class SogNo_TariffForDay
    {
        public SogNo_TariffForTime[] TariffsForTime { get; private set; }   // 시간대별 요금제
        public ISogNo_Discount[] DefaultDiscounts { get; private set; }     // 각 요일에 기본 적용되는 할인 (회차, 시간대 무료, 시간대 할인)
        
        public SogNo_TariffForDay(SogNo_TariffForTime[] tariffsForTime, ISogNo_Discount[] defaultDiscounts = null)
        {
            this.TariffsForTime = tariffsForTime ?? new SogNo_TariffForTime[0];
            this.DefaultDiscounts = defaultDiscounts ?? new ISogNo_Discount[0];
        }
        
        /// <summary>
        /// 해당 시간에 적용할 시간대 요금제를 찾음
        /// </summary>
        /// <param name="timeOfDay">시간대 요금제를 찾을 시간</param>
        /// <returns>시간대 요금제. 시간대 요금제가 없는 경우 null.</returns>
        public SogNo_TariffForTime FindTariffForTime(TimeSpan timeOfDay)
        {
            if (this.TariffsForTime != null)
            {
                foreach (var tariff in this.TariffsForTime)
                {
                    if (tariff.StartTime <= timeOfDay && timeOfDay < tariff.EndTime)
                    {
                        return tariff;
                    }
                }
            }

            return null;
        }
    }
}
