﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SognoLib.Park.Toll
{
    using HolidayListItem = Tuple<DateTime, string>;
    using HolidayList = List<Tuple<DateTime, string>>;

    /// <summary>
    /// 공휴일 정보 조회 클래스로 매해 반복되는 법정공휴일과 대체공휴일은 자동 계산하며, 
    /// 대통령 선거나 국회의원 선거와 같은 임시 공휴일은 AddSpecialHoliday() 함수로 추가할 수 있음.
    /// </summary>
    public class SogNo_Holiday
    {
        /// <summary>
        /// 공휴일 정보 클래스
        /// </summary>
        class RepeatedHolidayInfo
        {
            // 대체공휴일 적용 여부
            public enum ESubstituteHoliday
            {
                None,                       // 대체공휴일 적용 안함
                ToHolidayAndSunday,         // 일요일과 공휴일과 겹칠 경우 대체공휴일 적용
                ToAllIncludingSaturday      // 토요일, 일요일 및 공휴일과 겹칠 경우 대체공휴일 적용
            };

            // 음력 양력 구분
            public enum ELunarSolar
            {
                SolarDate,                  // 양력
                LunarDate                   // 음력
            };

            /// <summary>
            /// 생성자
            /// </summary>
            /// <param name="lunarSolar">음력/양력 구분</param>
            /// <param name="month">월</param>
            /// <param name="day">일</param>
            /// <param name="desc">설명</param>
            /// <param name="substitute">대체공휴일 적용 여부</param>
            /// <param name="prev">앞 연휴 일수</param>
            /// <param name="next">뒤 연휴 일수</param>
            public RepeatedHolidayInfo(ELunarSolar lunarSolar, int month, int day, string desc, ESubstituteHoliday substitute = ESubstituteHoliday.None, int prev=0, int next=0)
            {
                this.LunarSolar = lunarSolar;
                this.Month = month;
                this.Day = day;
                this.Description = desc;
                this.ApplySubstituteHoliday = substitute;
                this.PrevContHolidays = prev;
                this.NextContHolidays = next;
            }

            public ELunarSolar LunarSolar { get; private set; }     // 음력 or 양력
            public int Month { get; private set; }                  // 월
            public int Day { get; private set; }                    // 일
            public string Description { get; private set; }         // 설명
            public ESubstituteHoliday ApplySubstituteHoliday { get; private set; }        // 대체공휴일 적용 여부
            public int PrevContHolidays { get; private set; }       // 앞 연휴 일수
            public int NextContHolidays { get; private set; }       // 뒤 연휴 일수
        }


        private readonly RepeatedHolidayInfo[] RepeatedHolidays = null;     // 매해 반복되는 국가 지정 공휴일 정보
        private readonly Dictionary<int, HolidayList> PublicHolidays = new Dictionary<int, HolidayList>();      // 연도별 공휴일 목록
        private readonly HolidayList SpecialHolidays = new HolidayList();   // 매해 반복되지 않는 임시 공휴일 (예: 대통령 선거, 국회의원 선거, 임시 공휴일 등)
        
        
        public SogNo_Holiday()
        {
            this.RepeatedHolidays = new RepeatedHolidayInfo[]
            {
                new RepeatedHolidayInfo(RepeatedHolidayInfo.ELunarSolar.LunarDate, 1, 1, "설날", RepeatedHolidayInfo.ESubstituteHoliday.ToHolidayAndSunday, 1, 1),
                new RepeatedHolidayInfo(RepeatedHolidayInfo.ELunarSolar.LunarDate, 4, 8, "부처님 오신 날"),
                new RepeatedHolidayInfo(RepeatedHolidayInfo.ELunarSolar.LunarDate, 8, 15, "추석", RepeatedHolidayInfo.ESubstituteHoliday.ToHolidayAndSunday, 1, 1),

                new RepeatedHolidayInfo(RepeatedHolidayInfo.ELunarSolar.SolarDate, 1, 1, "신정"),
                new RepeatedHolidayInfo(RepeatedHolidayInfo.ELunarSolar.SolarDate, 3, 1, "삼일절"),
                new RepeatedHolidayInfo(RepeatedHolidayInfo.ELunarSolar.SolarDate, 5, 5, "어린이날", RepeatedHolidayInfo.ESubstituteHoliday.ToAllIncludingSaturday),
                new RepeatedHolidayInfo(RepeatedHolidayInfo.ELunarSolar.SolarDate, 6, 6, "현충일"),
                new RepeatedHolidayInfo(RepeatedHolidayInfo.ELunarSolar.SolarDate, 8, 15, "광복절"),
                new RepeatedHolidayInfo(RepeatedHolidayInfo.ELunarSolar.SolarDate, 10, 3, "개천절"),
                new RepeatedHolidayInfo(RepeatedHolidayInfo.ELunarSolar.SolarDate, 10, 9, "한글날"),
                new RepeatedHolidayInfo(RepeatedHolidayInfo.ELunarSolar.SolarDate, 12, 25, "크리스마스")
            };
        }

        /// <summary>
        /// 대통령 선거나 국회의원 선거 등의 특수 공휴일을 등록함. 대체공휴일은 등력할 필요 없이 자동 계산됨.
        /// </summary>
        /// <param name="date">날자</param>
        /// <param name="holidayName">설명</param>
        public void AddSpecialHoliday(DateTime date, string holidayName)
        {
            this.SpecialHolidays.Add(new HolidayListItem(date, holidayName));
        }

        /// <summary>
        /// 해당 연도의 모든 등록된 공휴일을 반환함
        /// </summary>
        /// <param name="year">조회할 연도</param>
        /// <returns>해당 연도의 공휴일 리스트. 반환 타입은 List<Tuple<DateTime, string>>으로 Tuple<날자, 설명>의 리스트임.</날자></DateTime></returns>
        public HolidayList ListHolidays(int year)
        {
            if (!this.PublicHolidays.ContainsKey(year))
            {
                this.PublicHolidays.Add(year, BuildHolidayList(year));
            }

            HolidayList publicHolidays = this.PublicHolidays[year];

            HolidayList list = new HolidayList();
            foreach (var it in publicHolidays)
            {
                list.Add(it);
            }

            foreach (var it in this.SpecialHolidays)
            {
                if (it.Item1.Year == year)
                    list.Add(it);
            }

            list.Sort((p1, p2) => p1.Item1.CompareTo(p2.Item1));

            return list;
        }

        /// <summary>
        /// 해당 날자가 공휴일인지 조회함. 매해 반복되는 법정공휴일과 대체공휴일, 별도 등록한 특수공휴일을 대상으로 조회.
        /// </summary>
        /// <param name="date">조회할 날자</param>
        /// <param name="holidayName">공휴일일 경우 공휴일 설명을 반환</param>
        /// <returns>공휴일 여부</returns>
        public bool IsPublicHoliday(DateTime date, out string holidayName)
        {
            holidayName = "";

            if (!this.PublicHolidays.ContainsKey(date.Year))
            {
                this.PublicHolidays.Add(date.Year, BuildHolidayList(date.Year));
            }

            HolidayList holidayList = this.PublicHolidays[date.Year];
            int id = FindHoliday(holidayList, date);
            if (id >= 0)
            {
                holidayName = holidayList[id].Item2;
                return true;
            }

            id = FindHoliday(this.SpecialHolidays, date);
            if (id >= 0)
            {
                holidayName = this.SpecialHolidays[id].Item2;
                return true;
            }

            return false;
        }
        
        /// <summary>
        /// 해당 날자가 토요일인지 조회
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public bool IsSaturday(DateTime date)
        {
            return (date.DayOfWeek == DayOfWeek.Saturday);
        }

        /// <summary>
        /// 해당 날자가 일요일인지 조회
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public bool IsSunday(DateTime date)
        {
            return (date.DayOfWeek == DayOfWeek.Sunday);
        }

        /// <summary>
        /// 해당 날자가 주말(토요일, 일요일)인지 조회
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public bool IsWeekend(DateTime date)
        {
            return (IsSaturday(date) || IsSunday(date));
        }
        
        /// <summary>
        /// 해당 날자가 휴일 목록에 포함되어 있는지 조회
        /// </summary>
        /// <param name="list">휴일 목록</param>
        /// <param name="date">조회할 날자</param>
        /// <returns>해당 날자가 휴일 목록에 포함되어 있으면 true, 아니면 false</returns>
        private bool IsInHolidayList(HolidayList list, DateTime date)
        {
            return (FindHoliday(list, date) >= 0);
        }

        /// <summary>
        /// 해당 날자를 휴일 목록에서 찾음
        /// </summary>
        /// <param name="list">휴일 목록</param>
        /// <param name="date">찾을 날자</param>
        /// <returns>해당 날자가 휴일 목록에 있으면 목록의 인덱스를 반환하고 없으면 -1 반환</returns>
        private int FindHoliday(HolidayList list, DateTime date)
        {
            return list.FindIndex(it => it.Item1.Date == date.Date);
        }
        
        /// <summary>
        /// 해당 연도의 공휴일(법정 공휴일과 대체공휴일) 목록을 생성
        /// </summary>
        /// <param name="year">공휴일 목록을 만들 연도</param>
        /// <returns></returns>
        private HolidayList BuildHolidayList(int year)
        {
            HolidayList list = new HolidayList();

            // 대체공휴일 적용 안되는 휴일
            foreach (var it in this.RepeatedHolidays)
            {
                if (it.ApplySubstituteHoliday != RepeatedHolidayInfo.ESubstituteHoliday.None)
                {
                    continue;
                }

                DateTime refDate;
                if (it.LunarSolar == RepeatedHolidayInfo.ELunarSolar.SolarDate)
                    refDate = new DateTime(year, it.Month, it.Day, 0, 0, 0, 0);
                else
                    refDate = LunarToSolarDate(year, it.Month, it.Day).Value;

                HolidayListItem item = new HolidayListItem(refDate, it.Description);
                list.Add(item);

                for (int k = 1; k <= it.PrevContHolidays; k++)
                {
                    item = new HolidayListItem(refDate.AddDays(-k), it.Description + " 연휴");
                    list.Add(item);
                }

                for (int k = 1; k <= it.NextContHolidays; k++)
                {
                    item = new HolidayListItem(refDate.AddDays(k), it.Description + " 연휴");
                    list.Add(item);
                }
            }

            // 대체공휴일 적용되는 휴일
            foreach (var it in this.RepeatedHolidays)
            {
                if (it.ApplySubstituteHoliday == RepeatedHolidayInfo.ESubstituteHoliday.None)
                {
                    continue;
                }

                int substituteDays = 0;     // 대체공휴일 일수

                DateTime refDate;
                if (it.LunarSolar == RepeatedHolidayInfo.ELunarSolar.SolarDate)
                    refDate = new DateTime(year, it.Month, it.Day, 0, 0, 0, 0);
                else
                    refDate = LunarToSolarDate(year, it.Month, it.Day).Value;


                for (int k = -it.PrevContHolidays; k <= it.NextContHolidays; k++)
                {
                    DateTime date = refDate.AddDays(k);
                    if (IsInHolidayList(list, date) || IsSunday(date) ||
                        (it.ApplySubstituteHoliday == RepeatedHolidayInfo.ESubstituteHoliday.ToAllIncludingSaturday && IsSaturday(date)))
                    {
                        substituteDays++;
                    }

                    string desc = (k == 0 ? it.Description : it.Description + " 연휴");
                    HolidayListItem item = new HolidayListItem(date, desc);
                    list.Add(item);
                }

                // 대체공휴일 추가
                for (int j = 0; j < substituteDays; j++)
                {
                    for (int k = j + 1; k < 15; k++)
                    {
                        DateTime date = refDate.AddDays(k);
                        if (!IsInHolidayList(list, date) && !IsWeekend(date))
                        {
                            string desc = (k == 0 ? it.Description : it.Description + " (대체공휴일)");
                            HolidayListItem item = new HolidayListItem(date, desc);
                            list.Add(item);
                            break;
                        }
                    }
                }
            }

            list.Sort((p1, p2) => p1.Item1.CompareTo(p2.Item1));

            return list;
        }

        /// <summary>
        /// 음력을 양력으로 변환
        /// </summary>
        /// <param name="year">음력 년</param>
        /// <param name="month">음력 월</param>
        /// <param name="day">음력 일</param>
        /// <param name="isLeapMonth">윤달 여부. 이 값이 true이면 윤달 음력 날자를 양력으로 변환하고 false이면 윤달이 아닌 음력 날자를 양력으로 변환.</param>
        /// <returns>양력 날자. 입력한 해에 입력한 달을 찾을 수 없는 경우(윤달 설정하였으나 윤달이 없는 경우) null.</returns>        
        private DateTime? LunarToSolarDate(int year, int month, int day, bool isLeapMonth = false)
        {
            var ksc = new System.Globalization.KoreanLunisolarCalendar();

            DateTime? solarDate = null;
            if (ksc.GetMonthsInYear(year) > 12)
            {
                int leapMonth = ksc.GetLeapMonth(year);   // 예) 4월 윤달은 leapMonth=5이고, 5월은 6, ...

                if (isLeapMonth && month != leapMonth - 1)  // 입력한 달이 윤달이 아닌경우
                {
                }
                else if (isLeapMonth || month >= leapMonth)  // 윤달이 낀 해의 윤달 이후 달은 +1 함
                {
                    solarDate = ksc.ToDateTime(year, month + 1, day, 0, 0, 0, 0);
                }
                else
                {
                    solarDate = ksc.ToDateTime(year, month, day, 0, 0, 0, 0);
                }
            }
            else
            {
                if (isLeapMonth)    // 입력한 달이 윤달이 아닌경우
                {
                }
                else
                {
                    solarDate = ksc.ToDateTime(year, month, day, 0, 0, 0, 0);
                }
            }

            return solarDate;
        }
    }
}
