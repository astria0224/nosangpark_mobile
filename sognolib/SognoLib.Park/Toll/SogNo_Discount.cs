﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SognoLib.Park.Toll
{
    /// <summary>
    /// 주차요금 할인 관련 추상 클래스
    /// </summary>
    public abstract class ISogNo_Discount
    {
        // 요금 할인 유형
        public enum EDiscountType
        {
            TurningDC,              // 회차
            TimeDC,                 // 시간 감면 (주차 시간을 줄여줌)
            PeriodDC_Free,          // 기간 무료 (특정 기간동안 무료)
            PeriodDC_Percent,       // 기간 할인 (특정 기간동안 % 할인 요금을 받음)
            TimeSlotDC_Free,        // 시간대 무료 (매일 혹은 매주 특정 요일의 특정 시간대에 요금을 받지 않음)
            TimeSlotDC_Percent,     // 시간대 할인 (매일 혹은 매주 특정 요일의 특정 시간대에 % 할인 요금을 받음)
            PercentDC,              // 요금 할인 (계산된 총 요금에서 % 할인을 적용함)
            PriceDC,                // 요금 감면 (계산된 총 요금에서 일정 금액을 감면함)
            Tariff_Free             // 요금제 무료 (요금제에서 특정 구간을 무료로 지정함)
        };

        /// <summary>
        /// 요금 할인 유형에 대한 설명
        /// </summary>
        /// <returns></returns>
        public virtual string DiscountTypeString()
        {
            switch(this.DiscountType)
            {
                case EDiscountType.TurningDC:
                    return "회차 무료";
                case EDiscountType.TimeDC:
                    return "시간 감면";
                case EDiscountType.PeriodDC_Free:
                    return "기간 무료";
                case EDiscountType.PeriodDC_Percent:
                    return "기간 할인";
                case EDiscountType.TimeSlotDC_Free:
                    return "시간대 무료";
                case EDiscountType.TimeSlotDC_Percent:
                    return "시간대 할인";
                case EDiscountType.PercentDC:
                    return "요금 할인";
                case EDiscountType.PriceDC:
                    return "요금 감면";
                case EDiscountType.Tariff_Free:
                    return "요금제(무료)";
                default:
                    return "알수없는할인";
            }
        }

        public EDiscountType DiscountType { get; protected set; }       // 할인 유형
        public int DiscountPriority { get; protected set; } = 100;      // 할인의 적용 우선순위, 무료는 100, 할인은 할인율을 값으로 설정.

        public abstract void Apply(SogNo_Bill bill);                    // 할인을 적용
    }

    /// <summary>
    /// 회차 무료
    /// 
    /// - 입차 후 TurnTime 이내에 출차하면 요금을 부과하지 않음
    /// - 입차 후 TurnTime 이후에 출차하면 입차 시점부터 요금을 부과함
    /// - 이때, DiscountTime이 0이 아니면 처음 DiscountTime(분) 동안은 요금을 받지 않음
    /// </summary>
    public class SogNo_TurningDC : ISogNo_Discount
    {
        public int TurnTime { get; private set; }           // 회차 시간
        public int DiscountTime { get; private set; }       // 입차 후 무료주차 시간. 기본은 0.

        /// <summary>
        /// 생성자
        /// </summary>
        /// <param name="turnTime">회차 시간</param>
        /// <param name="discountTime">입차 후 무료주차 시간</param>
        public SogNo_TurningDC(int turnTime, int discountTime = 0)
        {
            this.DiscountType = EDiscountType.TurningDC;
            this.TurnTime = turnTime;
            this.DiscountTime = discountTime;
        }

        /// <summary>
        /// 회차 무료 적용
        /// 
        /// - bill을 생성(new)한 후 처음으로 호출해야 함.
        /// </summary>
        /// <param name="bill">요금 계산 내역서</param>
        public override void Apply(SogNo_Bill bill)
        {
            if (bill.Details.Count == 1 && !bill.Details[0].Done)
            {
                var item = bill.Details[0];
                double parkingMinutes = item.TotalMinutes;

                if (this.TurnTime >= parkingMinutes)
                {
                    bill.IsTurning = true;
                    item.SetDone(SogNo_Bill.DetailItem.EItemType.Free, this, 0, DiscountTypeString());
                }
                else if (this.DiscountTime > 0)
                {
                    DateTime dividePoint = item.StartDateTime.AddMinutes(this.DiscountTime);

                    bill.RemoveDetail(item);
                    bill.AddDetail(new SogNo_Bill.DetailItem(SogNo_Bill.DetailItem.EItemType.Free, item.StartDateTime, dividePoint, null, false, this, 0, "회차 할인", true));
                    bill.AddDetail(new SogNo_Bill.DetailItem(dividePoint, item.EndDateTime, item));
                }
            }
        }
    }

    /// <summary>
    /// 시간 감면
    /// 
    /// - DiscountTime(분) 만큼의 주차 시간을 무료로 처리함
    /// - 전체 주차 기간 중 무료 기간이 포함된 경우 무료인 기간은 제외하고 계산함
    /// - 즉, 실제 요금이 부과되는 시간 중 DiscountTime(분) 만큼을 무료 처리함
    /// </summary>
    public class SogNo_TimeDC : ISogNo_Discount
    {
        public int DiscountTime { get; private set; }

        /// <summary>
        /// 생성자
        /// </summary>
        /// <param name="discountTime">무료로 처리할 시간 (분)</param>
        public SogNo_TimeDC(int discountTime)
        {
            this.DiscountType = EDiscountType.TimeDC;
            this.DiscountTime = discountTime;
        }

        public override void Apply(SogNo_Bill bill)
        {
            double remainingMinutes = this.DiscountTime;

            bill.SortDetails();
            for (int i = bill.Details.Count - 1; i >= 0; i--)
            {
                if (remainingMinutes <= 0)
                    break;

                var item = bill.Details[i];
                if (item.ItemType == SogNo_Bill.DetailItem.EItemType.Free)
                    continue;

                double itemTotalMinutes = item.TotalMinutes;
                if (remainingMinutes >= itemTotalMinutes)
                {
                    item.SetDone(SogNo_Bill.DetailItem.EItemType.Free, this, 0, DiscountTypeString());
                    remainingMinutes -= itemTotalMinutes;
                }
                else
                {
                    DateTime dividePoint = item.StartDateTime.AddMinutes(itemTotalMinutes - remainingMinutes);
                    remainingMinutes = 0;

                    bill.RemoveDetail(item);
                    bill.AddDetail(new SogNo_Bill.DetailItem(item.StartDateTime, dividePoint, item));
                    bill.AddDetail(new SogNo_Bill.DetailItem(SogNo_Bill.DetailItem.EItemType.Free, dividePoint, item.EndDateTime, null, false, this, 0, DiscountTypeString(), true));
                }
            }
        }
    }

    /// <summary>
    /// 시간대 무료를 적용하기 위해 기간 무료로 변환한 개체
    /// 
    /// - 시간대 무료를 기간 무료 할인의 처리 방식을 빌려 처리함
    /// </summary>
    class SogNo_PeriodFromTimeSlotDC_Free : SogNo_PeriodDC_Free
    {
        public SogNo_PeriodFromTimeSlotDC_Free(DateTime startDateTime, DateTime endDateTime) :
            base(startDateTime, endDateTime)
        {
            this.DiscountType = EDiscountType.TimeSlotDC_Free;
        }
    }

    /// <summary>
    /// 요금제 무료를 적용하기 위해 기간 무료로 변환한 개체
    /// 
    /// - 요금제에서 무료로 설정된 구간을 기간 무료 할인의 처리 방식을 빌려 처리함
    /// </summary>
    class SogNo_PeriodFromTariff_Free : SogNo_PeriodDC_Free
    {
        public string TariffDescription { get; private set; }

        /// <summary>
        /// 생성자
        /// </summary>
        /// <param name="startDateTime">무료 할인 시작 시각</param>
        /// <param name="endDateTime">무료 할인 종료 시각</param>
        /// <param name="tariffDescription">적용한 요금제</param>
        public SogNo_PeriodFromTariff_Free(DateTime startDateTime, DateTime endDateTime, string tariffDescription) :
            base(startDateTime, endDateTime)
        {
            this.DiscountType = EDiscountType.Tariff_Free;
            this.TariffDescription = tariffDescription;
        }

        public override string DiscountTypeString()
        {
            return string.Format("요금제({0}-무료)", this.TariffDescription);
        }
    }


    /// <summary>
    /// 기간 무료
    /// 
    /// - StartDateTime부터 EndDateTime까지의 기간을 무료로 처리함
    /// </summary>
    public class SogNo_PeriodDC_Free : ISogNo_Discount
    {
        public DateTime StartDateTime { get; private set; }
        public DateTime EndDateTime { get; private set; }

        public SogNo_PeriodDC_Free(DateTime startDateTime, DateTime endDateTime)
        {
            this.DiscountType = EDiscountType.PeriodDC_Free;
            this.StartDateTime = startDateTime;
            this.EndDateTime = endDateTime;
        }

        public override void Apply(SogNo_Bill bill)
        {
            for (int i = bill.Details.Count - 1; i >= 0; i--)
            {
                var item = bill.Details[i];

                if (item.Done)
                    continue;

                if (this.EndDateTime <= item.StartDateTime ||
                    item.EndDateTime <= this.StartDateTime)
                {
                    // item과 DC 구간이 겹치지 않는 경우
                    continue;
                }
                else if (this.StartDateTime <= item.StartDateTime && item.EndDateTime <= this.EndDateTime)
                {
                    // item 전체가 DC 구간에 포함.
                    item.SetDone(SogNo_Bill.DetailItem.EItemType.Free, this, 0, DiscountTypeString());
                }
                else if (item.StartDateTime < this.StartDateTime && item.EndDateTime <= this.EndDateTime)
                {
                    // item 후반부가 DC 구간에 포함
                    bill.RemoveDetail(item);

                    bill.AddDetail(new SogNo_Bill.DetailItem(item.StartDateTime, this.StartDateTime, item));
                    bill.AddDetail(new SogNo_Bill.DetailItem(SogNo_Bill.DetailItem.EItemType.Free, this.StartDateTime, item.EndDateTime, null, false, this, 0, DiscountTypeString(), true));
                }
                else if (this.StartDateTime <= item.StartDateTime && this.EndDateTime < item.EndDateTime)
                {
                    // item 전반부가 DC 구간에 포함
                    bill.RemoveDetail(item);

                    bill.AddDetail(new SogNo_Bill.DetailItem(SogNo_Bill.DetailItem.EItemType.Free, item.StartDateTime, this.EndDateTime, null, false, this, 0, DiscountTypeString(), true));
                    bill.AddDetail(new SogNo_Bill.DetailItem(this.EndDateTime, item.EndDateTime, item));
                }
                else
                {
                    // item 중반부가 DC 구간에 포함
                    bill.RemoveDetail(item);

                    bill.AddDetail(new SogNo_Bill.DetailItem(item.StartDateTime, this.StartDateTime, item));
                    bill.AddDetail(new SogNo_Bill.DetailItem(SogNo_Bill.DetailItem.EItemType.Free, this.StartDateTime, this.EndDateTime, null, false, this, 0, DiscountTypeString(), true));
                    bill.AddDetail(new SogNo_Bill.DetailItem(this.EndDateTime, item.EndDateTime, item));
                }
            }
        }
    }

    /// <summary>
    /// 시간대 무료
    /// 
    /// - 할인 설정 위치에 따라 매일, 혹은 특정 요일, 공휴일마다 StartTime 부터 EndTime까지 무료로 처리함.
    /// </summary>
    public class SogNo_TimeSlotDC_Free : ISogNo_Discount
    {
        public TimeSpan StartTime { get; private set; }
        public TimeSpan EndTime { get; private set; }

        public SogNo_TimeSlotDC_Free(TimeSpan startTime, TimeSpan endTime)
        {
            this.DiscountType = EDiscountType.TimeSlotDC_Free;
            this.StartTime = startTime;
            this.EndTime = endTime;
        }

        public override void Apply(SogNo_Bill bill)
        {
            throw new NotImplementedException("시간대 무료(TimeSlotDC)는 바로 적용하지 않고 기간 할인(PeriodDC)으로 변환 후 적용해야 함.");
        }
    }

    /// <summary>
    /// 시간대 할인을 적용하기 위해 기간 할인으로 변환한 개체
    /// 
    /// - 시간대 할인을 기간 할인의 처리 방식을 빌려 처리함
    /// </summary>
    class SogNo_PeriodFromTimeSlotDC_Percent : SogNo_PeriodDC_Percent
    {
        public SogNo_PeriodFromTimeSlotDC_Percent(DateTime startDateTime, DateTime endDateTime, int percent) :
            base(startDateTime, endDateTime, percent)
        {
            this.DiscountType = EDiscountType.TimeSlotDC_Percent;
        }
    }

    /// <summary>
    /// 기간 할인
    /// 
    /// - StartDateTime부터 EndDateTime까지의 기간을 Percent % 할인 처리함
    /// </summary>
    public class SogNo_PeriodDC_Percent : ISogNo_Discount
    {
        public DateTime StartDateTime { get; private set; }
        public DateTime EndDateTime { get; private set; }
        public int Percent { get; private set; }

        public SogNo_PeriodDC_Percent(DateTime startDateTime, DateTime endDateTime, int percent)
        {
            this.DiscountType = EDiscountType.PeriodDC_Percent;
            this.StartDateTime = startDateTime;
            this.EndDateTime = endDateTime;
            this.Percent = percent;

            this.DiscountPriority = this.Percent;
        }

        public override void Apply(SogNo_Bill bill)
        {
            foreach (var item in bill.Details)
            {
                if (this.StartDateTime <= item.StartDateTime && item.StartDateTime < this.EndDateTime)
                {
                    if (item.Discount == null || item.Discount.DiscountPriority < this.DiscountPriority)
                    {
                        item.SetDiscount(this);
                    }
                }
            }
        }
    }

    /// <summary>
    /// 시간대 할인
    /// 
    /// - 할인 설정 위치에 따라 매일, 혹은 특정 요일, 공휴일마다 StartTime 부터 EndTime까지 요금 할인(%) 처리함.
    /// </summary>
    public class SogNo_TimeSlotDC_Percent : ISogNo_Discount
    {
        public TimeSpan StartTime { get; private set; }
        public TimeSpan EndTime { get; private set; }
        public int Percent { get; private set; }

        public SogNo_TimeSlotDC_Percent(TimeSpan startTime, TimeSpan endTime, int percent)
        {
            this.DiscountType = EDiscountType.TimeSlotDC_Percent;
            this.StartTime = startTime;
            this.EndTime = endTime;
            this.Percent = percent;

            this.DiscountPriority = this.Percent;
        }

        public override void Apply(SogNo_Bill bill)
        {
            throw new NotImplementedException("시간대 할인(TimeSlotDC)은 바로 적용하지 않고 기간 할인(PeriodDC)으로 변환 후 적용해야 함.");
        }
    }

    /// <summary>
    /// 요금 할인
    /// 
    /// - 총 계산된 주차 요금에서 Percent % 할인을 적용함
    /// </summary>
    public class SogNo_PercentDC : ISogNo_Discount
    {
        public int Percent { get; private set; }

        public SogNo_PercentDC(int percent)
        {
            this.DiscountType = EDiscountType.PercentDC;
            this.Percent = percent;

            this.DiscountPriority = this.Percent;
        }

        public override void Apply(SogNo_Bill bill)
        {
            bill.PercentDC = this;

            int origin = bill.Price;
            bill.Price = bill.Price * (100 - this.Percent) / 100;

            bill.Discount_PercentDC = origin - bill.Price;
        }
    }

    /// <summary>
    /// 요금 감면
    /// 
    /// - 총 계산된 주차 요금에서 Price 금액 만큼 감면
    /// - 요금 할인(SogNo_PercentDC)보다 나중에 실행
    /// </summary>
    public class SogNo_PriceDC : ISogNo_Discount
    {
        public int Price { get; private set; }
        public SogNo_PriceDC(int price)
        {
            this.DiscountType = EDiscountType.PriceDC;
            this.Price = price;
        }

        public override void Apply(SogNo_Bill bill)
        {
            bill.PriceDC = this;

            int origin = bill.Price;
            bill.Price = bill.Price > this.Price ? bill.Price - this.Price : 0;

            bill.Discount_PriceDC = origin - bill.Price;
        }
    }
}
