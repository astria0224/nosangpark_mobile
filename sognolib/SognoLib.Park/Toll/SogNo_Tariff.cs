﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SognoLib.Park.Toll
{
    /// <summary>
    /// 요금 체계 정의
    /// </summary>
    public class SogNo_Tariff
    {
        public enum EDailyCriterion { Day, InTime }     // 일일 적용 기준 { 날자 기준, 입차 시각 기준 }
        public enum ERoundType { None, Ceiling, Floor, Round }     // 반올림 적용 기준 { 없음, 올림, 내림, 반올림 }

        public string Description { get; private set; }                 // 요금제 설명
        public int MaxRates { get; private set; }                       // 전체 최대 요금
        public int DailyMaxRates { get; private set; }                  // 일 최대 요금
        public EDailyCriterion DailyCriterion { get; private set; }     // 일일 적용 기준
        public ERoundType RoundType { get; private set; }               // 반올림 설정
        public int RoundDecimal { get; private set; }                   // 반올림 적용 단위
        public bool[] HolidayPriority { get; private set; }             // 공휴일 적용 우선순위, [일, 월, 화, 수, 목, 금, 토] 순서의 7개 값. 값이 true면 공휴일 요금제를 우선 적용하고 false면 요일 요금제를 우선 적용함.

        public SogNo_TariffForDay MondayTariff { get; private set; }        // 월요일 요금제
        public SogNo_TariffForDay TuesdayTariff { get; private set; }       // 화요일 요금제
        public SogNo_TariffForDay WednesdayTariff { get; private set; }     // 수요일 요금제
        public SogNo_TariffForDay ThursdayTariff { get; private set; }      // 목요일 요금제
        public SogNo_TariffForDay FridayTariff { get; private set; }        // 금요일 요금제
        public SogNo_TariffForDay SaturdayTariff { get; private set; }      // 토요일 요금제
        public SogNo_TariffForDay SundayTariff { get; private set; }        // 일요일 요금제
        public SogNo_TariffForDay HolidayTariff { get; private set; }       // 공휴일 요금제

        public ISogNo_Discount[] DefaultDiscounts { get; private set; }     // 요금제에 포함된 기본 할인 정보 (회차, 시간대 무료, 시간대 할인, 요금 할인, 요금 감면)

        public SogNo_Holiday HolidayIdentifier { get; } = new SogNo_Holiday();  // 공휴일 정보 조회 클래스

        /// <summary>
        /// 생성자
        /// </summary>
        /// <param name="description">요금제 설명</param>
        /// <param name="mondayTariff">월요일 요금제</param>
        /// <param name="tuesdayTariff">화요일 요금제</param>
        /// <param name="wednesdayTariff">수요일 요금제</param>
        /// <param name="thursdayTariff">목요일 요금제</param>
        /// <param name="fridayTariff">금요일 요금제</param>
        /// <param name="saturdayTariff">토요일 요금제</param>
        /// <param name="sundayTariff">일요일 요금제</param>
        /// <param name="holidayTariff">공휴일 요금제</param>
        /// <param name="holidayPriority">공휴일 적용 우선순위, [일, 월, 화, 수, 목, 금, 토] 순서의 7개 값. 값이 true면 공휴일 요금제를 우선 적용하고 false면 요일 요금제를 우선 적용함.</param>
        /// <param name="defaultDiscounts">요금제 기본 할인 정보 (회차, 시간대 무료, 시간대 할인, 요금 할인, 요금 감면)</param>
        /// <param name="maxRates">전체 최대 요금</param>
        /// <param name="dailyMaxRates">일일 최대 요금</param>
        /// <param name="dailyCriterion">일일 적용 기준</param>
        /// <param name="roundType">반올림 유형</param>
        /// <param name="roundDecimal">반올림 자릿수</param>
        public SogNo_Tariff(string description,
                            SogNo_TariffForDay mondayTariff,
                            SogNo_TariffForDay tuesdayTariff,
                            SogNo_TariffForDay wednesdayTariff,
                            SogNo_TariffForDay thursdayTariff,
                            SogNo_TariffForDay fridayTariff,
                            SogNo_TariffForDay saturdayTariff,
                            SogNo_TariffForDay sundayTariff,
                            SogNo_TariffForDay holidayTariff,
                            bool[] holidayPriority = null,
                            ISogNo_Discount[] defaultDiscounts = null,
                            int maxRates = -1, 
                            int dailyMaxRates = -1,                             
                            EDailyCriterion dailyCriterion = EDailyCriterion.Day,
                            ERoundType roundType = ERoundType.Floor,
                            int roundDecimal = 100)
        {
            this.Description = description;

            this.MondayTariff = mondayTariff;
            this.TuesdayTariff = tuesdayTariff;
            this.WednesdayTariff = wednesdayTariff;
            this.ThursdayTariff = thursdayTariff;
            this.FridayTariff = fridayTariff;
            this.SaturdayTariff = saturdayTariff;
            this.SundayTariff = sundayTariff;
            this.HolidayTariff = holidayTariff;
            this.HolidayPriority = holidayPriority ?? new bool[] { true, true, true, true, true, true, true };

            this.DefaultDiscounts = defaultDiscounts ?? new ISogNo_Discount[0];

            this.MaxRates = maxRates;
            this.DailyMaxRates = dailyMaxRates;
            this.DailyCriterion = dailyCriterion;
            this.RoundType = roundType;
            this.RoundDecimal = roundDecimal;
        }

        /// <summary>
        /// 주차 요금 요금제 적용
        /// 
        /// - 회차, 기간 무료, 시간대 무료, 요금제(무료), 요금제(유료), 기간 할인, 시간대 할인 적용
        /// - 주차 시간을 요금 계산을 위한 세부 구간으로 잘라서 요금을 계산
        /// - 주차 요금 계산 순서
        ///    - 1개의 요금제 적용: SogNo_Bill 생성 -> trf1.Apply() -> trf1.Tall()
        ///    - 2개의 요금제 적용: SogNo_Bill 생성 -> trf1.Apply() -> trf2.Apply() -> trf2.Tall()
        ///    - 3개의 요금제 적용: SogNo_Bill 생성 -> trf1.Apply() -> trf2.Apply() -> trf3.Apply() -> trf3.Tall()
        /// </summary>
        /// <param name="bill">주차 요금 계산서</param>
        /// <param name="applyStart">요금 계산 시작 시각</param>
        /// <param name="applyEnd">요금 계산 종료 시각</param>
        /// <param name="applyDiscounts">할인 적용 여부</param>
        /// <param name="discounts">사용자 지정 추가 할인 정보</param>
        /// <returns>요금 계산에 포함된 최종 시각</returns>
        public DateTime Apply(SogNo_Bill bill, DateTime applyStart, DateTime applyEnd, bool applyDiscounts, ISogNo_Discount[] discounts = null)
        {
            applyStart = applyStart > bill.InDateTime ? applyStart : bill.InDateTime;
            applyEnd = applyEnd < bill.OutDateTime ? applyEnd : bill.OutDateTime;

            // 회차무료 적용
            if (applyStart == bill.InDateTime)
            {
                ApplyTurningDC(bill);
            }
            
            // 날자별 분할
            DailySeparate(bill, applyStart, applyEnd);

            // 기간무료 적용
            if (applyDiscounts)
                ApplyPeriodDC_Free(bill, discounts);

            // 시간대 무료 적용
            if (applyDiscounts)
                ApplyTimeSlotDC_Free(bill, discounts, applyStart, applyEnd);

            // 요금제(무료인 구간) 적용
            ApplyTariff_Free(bill, applyStart, applyEnd);

            // 요금제(유료) 적용 - 요금제 표시만
            DateTime last = ApplyTariff_Pay(bill, applyStart, applyEnd);

            // 기간할인 적용 - 할인 표시만
            if (applyDiscounts)
                ApplyPeriodDC_Percent(bill, discounts, applyStart, applyEnd);

            // 시간대 할인 적용 - 할인 표시만
            if (applyDiscounts)
                ApplyTimeSlotDC_Percent(bill, discounts, applyStart, applyEnd);

            // 요금 정산 - 요금제와 할인 적용하여 계산
            CalculateTariff_Pay(bill, applyStart, applyEnd);

            return last;
        }

        /// <summary>
        /// 주차 요금 계산
        /// 
        /// - 시간감면, 일 요금 정산, 일 최대요금, 총 최대요금, 요금할인, 요금감면 적용
        /// - 요금제 적용한 결과를 합산하고 정산
        /// </summary>
        /// <param name="bill">주차 요금 계산서</param>
        /// <param name="discounts">사용자 지정 추가 할인 정보</param>
        public void Toll(SogNo_Bill bill, bool applyDiscounts, ISogNo_Discount[] discounts = null)
        {
            // 시간감면 적용
            if (applyDiscounts)
                ApplyTimeDC(bill, discounts);
            
            // 내역 병합 - 잘게 잘라진 연결된 항목들을 병합
            MergeBillDetails(bill);

            // 내역 정리 - 날자별로 요금을 정산
            DailySummary(bill);

            // 요금 합산
            CalculateTotalPrice(bill);

            // 요금할인 적용
            if (applyDiscounts)
                ApplyPercentDC(bill, discounts);

            // 요금감면 적용
            if (applyDiscounts)
                ApplyPriceDC(bill, discounts);

            ApplyRoundDC(bill);
        }

        /// <summary>
        /// 요금 합산
        /// 
        /// - Price = 날자별로 정산된 요금을 합산하고 전체 최대 요금(MaxRates) 적용
        /// - 이후 요금할인과 요금감면이 적용되면 Price의 값은 변할 수 있음
        /// </summary>
        /// <param name="bill">주차 요금 계산서</param>
        private void CalculateTotalPrice(SogNo_Bill bill)
        {
            int origin = bill.TotalDailyPrices();

            bill.MaxRates = this.MaxRates;
            bill.Price = (this.MaxRates >= 0 && this.MaxRates < origin ? this.MaxRates : origin);
        }

        /// <summary>
        /// 요금감면 적용
        /// 
        /// - 사용자 지정 추가 할인과 요금제에 설정된 기본 할인 중 요금감면을 적용
        /// - 요금감면이 둘 이상 설정되어 있을 경우 감면액을 누적하여 적용
        /// - Price = 기존 Price에 요금 감면 적용
        /// </summary>
        /// <param name="bill">주차 요금 계산서</param>
        /// <param name="discounts">사용자 지정 추가 할인 정보</param>
        private void ApplyPriceDC(SogNo_Bill bill, ISogNo_Discount[] discounts)
        {
            int discountPrice = 0;
            var dcs = FindDCs(discounts, ISogNo_Discount.EDiscountType.PriceDC);
            foreach (var dc in dcs)
            {
                discountPrice += (dc as SogNo_PriceDC).Price;
            }

            dcs = FindDCs(this.DefaultDiscounts, ISogNo_Discount.EDiscountType.PercentDC);
            foreach (var dc in dcs)
            {
                discountPrice += (dc as SogNo_PriceDC).Price;
            }
            
            SogNo_PriceDC priceDC = null;
            if (discountPrice > 0)
                priceDC = new SogNo_PriceDC(discountPrice);
            priceDC?.Apply(bill);
        }

        /// <summary>
        /// 요금할인 적용
        /// 
        /// - 사용자 지정 추가 할인과 요금제에 설정된 기본 할인 중 요금 %할인을 적용
        /// - 요금할인이 둘 이상 설정되어 있을 경우 할인 %가 큰 값을 적용
        /// - Price = 기존 Price에 요금 할인 적용
        /// </summary>
        /// <param name="bill">주차 요금 계산서</param>
        /// <param name="discounts">사용자 지정 추가 할인 정보</param>
        private void ApplyPercentDC(SogNo_Bill bill, ISogNo_Discount[] discounts)
        {
            ISogNo_Discount percentDC = null;
            var dcs = FindDCs(discounts, ISogNo_Discount.EDiscountType.PercentDC);
            foreach (var dc in dcs)
            {
                if (percentDC == null || percentDC.DiscountPriority < dc.DiscountPriority)
                    percentDC = dc;
            }

            dcs = FindDCs(this.DefaultDiscounts, ISogNo_Discount.EDiscountType.PercentDC);
            foreach (var dc in dcs)
            {
                if (percentDC == null || percentDC.DiscountPriority < dc.DiscountPriority)
                    percentDC = dc;
            }

            percentDC?.Apply(bill);
        }

        /// <summary>
        /// 일일 요금을 정산
        /// 
        /// - DailyCriterion == Day인 경우 날자별 요금을 정산
        /// - DailyCriterion == InTime인 경우 입차 시각을 기준으로 만 1일을 합산하여 일일 요금으로 정산
        /// - 일일 최대 요금 적용
        /// </summary>
        /// <param name="bill">주차 요금 계산서</param>
        private void DailySummary(SogNo_Bill bill)
        {
            bill.SortDetails();

            TimeSpan inTime = bill.InDateTime.TimeOfDay;
            bool summaryStarted = false;
            DateTime date = new DateTime();
            List<SogNo_Bill.DetailItem> details = new List<SogNo_Bill.DetailItem>();

            for (int i = 0; i < bill.Details.Count; i++)
            {
                var item = bill.Details[i];

                if (!summaryStarted)
                {
                    date = item.StartDateTime.Date;
                    details.Add(item);
                    summaryStarted = true;
                }
                else if (date == item.StartDateTime.Date || (this.DailyCriterion == EDailyCriterion.InTime && item.StartDateTime.TimeOfDay < inTime))
                {
                    details.Add(item);
                }
                else
                {
                    bill.AddSummary(new SogNo_Bill.SummaryItem(date, details.ToArray(), this.DailyMaxRates));

                    details.Clear();
                    summaryStarted = false;
                    i--;
                }
            }

            bill.AddSummary(new SogNo_Bill.SummaryItem(date, details.ToArray(), this.DailyMaxRates));
        }

        /// <summary>
        /// Details의 항목 중 인접한 동종의 항목을 병합하여 간략화
        /// 
        /// - 요금제 적용 시 Details는 요금 계산에 사용되는 가장 작은 시간 단위로 조각나 있음
        /// - 인접한 항목 중 같은 종류인 것들을 병합하여 Details를 간략화 함
        /// - 이때 일일 기준이 다른 두 항목은 병합하지 않음
        /// </summary>
        /// <param name="bill">주차 요금 계산서</param>
        private void MergeBillDetails(SogNo_Bill bill)
        {
            bill.SortDetails();

            for (int i = 0; i < bill.Details.Count - 1; i++)
            {
                var item_i = bill.Details[i];

                while (i + 1 < bill.Details.Count)
                {
                    var item_j = bill.Details[i + 1];

                    // 두 항목의 설명이 다르면 병합하지 않음.
                    if (item_i.Description != item_j.Description)
                    {
                        break;
                    }

                    // 일일 기준이 날자인 경우 두 항목의 시작 날자가 다르면 병합하지 않음
                    if (this.DailyCriterion == EDailyCriterion.Day)
                    {
                        if (item_i.StartDateTime.Date != item_j.StartDateTime.Date)
                            break;
                    }

                    // 일일 기준이 입차 시각인 경우 두 항목이 각각 입차 시각 전과 후인 경우 병합하지 않음
                    if (this.DailyCriterion == EDailyCriterion.InTime)
                    {
                        if (item_j.StartDateTime.TimeOfDay == bill.InDateTime.TimeOfDay)
                            break;
                    }

                    item_i.Merge(item_j);
                    bill.RemoveDetail(item_j);
                }
            }
        }

        /// <summary>
        /// 요금 정산
        /// 
        /// - 정산이 완료되지 않은 Details의 항목을 설정된 요금제와 할인을 적용하여 정산
        /// </summary>
        /// <param name="bill">주차 요금 계산서</param>
        /// <param name="applyStart">요금 계산 시작 시각</param>
        /// <param name="applyEnd">요금 계산 종료 시각</param>
        private void CalculateTariff_Pay(SogNo_Bill bill, DateTime applyStart, DateTime applyEnd)
        {
            bill.SortDetails();
            
            for (int i = 0; i < bill.Details.Count; i++)
            {
                var item = bill.Details[i];
                if (item.Done || item.EndDateTime <= applyStart)
                    continue;
                else if (item.StartDateTime >= applyEnd)
                    break;

                if (item.IsBaseRates)
                {
                    if (item.Discount == null)
                    {
                        item.SetDone(SogNo_Bill.DetailItem.EItemType.Rates, null, item.Tariff.BaseRates, "요금제(" + this.Description + "-기본)");
                    } 
                    else
                    {
                        int rates = item.Tariff.BaseRates * (100 - item.Discount.DiscountPriority) / 100;
                        string desc = string.Format("요금제({0}-기본) + {1}({2}%)", this.Description, item.Discount.DiscountTypeString(), item.Discount.DiscountPriority);
                        item.SetDone(SogNo_Bill.DetailItem.EItemType.Rates, item.Discount, rates, desc);
                    }
                }
                else
                {
                    if (item.Discount == null)
                    {
                        item.SetDone(SogNo_Bill.DetailItem.EItemType.Rates, null, item.Tariff.AddRates, "요금제(" + this.Description + "-추가)");
                    }
                    else
                    {
                        int rates = item.Tariff.AddRates * (100 - item.Discount.DiscountPriority) / 100;
                        string desc = string.Format("요금제({0}-추가) + {1}({2}%)", this.Description, item.Discount.DiscountTypeString(), item.Discount.DiscountPriority);
                        item.SetDone(SogNo_Bill.DetailItem.EItemType.Rates, item.Discount, rates, desc);
                    }                   
                }
            }
        }

        /// <summary>
        /// 시간대 할인 적용
        /// 
        /// - 사용자 지정 추가할인과 요금제에 포함된 시간대 할인은 매일 반복됨
        /// - 일 요금제에 포함된 시간대 할인은 해당 요일이나 공휴일에 반복됨
        /// - 반복되는 시간대 할인을 날자 정보를 추가하여 기간 할인으로 변환함
        /// - 변환한 기간 할인을 적용함
        /// </summary>
        /// <param name="bill">주차 요금 계산서</param>
        /// <param name="discounts">사용자 지정 추가 할인 정보</param>
        /// <param name="applyStart">요금 계산 시작 시각</param>
        /// <param name="applyEnd">요금 계산 종료 시각</param>
        private void ApplyTimeSlotDC_Percent(SogNo_Bill bill, ISogNo_Discount[] discounts, DateTime applyStart, DateTime applyEnd)
        {
            List<ISogNo_Discount> periodDCs = new List<ISogNo_Discount>();

            // 사용자 지정 할인에 포함된 시간대 할인을 기간 할인으로 변환
            var dcs = FindDCs(discounts, ISogNo_Discount.EDiscountType.TimeSlotDC_Percent);
            dcs = ConvertTimeSlotToPeriodDC_Percent(applyStart.Date, applyStart.Date, dcs);
            periodDCs.AddRange(dcs);

            // 요금제에 포함된 시간대 할인을 기간 할인으로 변환
            dcs = FindDCs(this.DefaultDiscounts, ISogNo_Discount.EDiscountType.TimeSlotDC_Percent);
            dcs = ConvertTimeSlotToPeriodDC_Percent(applyStart.Date, applyStart.Date, dcs);
            periodDCs.AddRange(dcs);

            // 일 요금제에 포함된 시간대 할인을 기간 할인으로 변환
            for (var date = applyStart.Date; date <= applyStart.Date; date = date.AddDays(1))
            {
                var tariff = FindTariffForDay(date);
                dcs = FindDCs(tariff?.DefaultDiscounts, ISogNo_Discount.EDiscountType.TimeSlotDC_Percent);
                dcs = ConvertTimeSlotToPeriodDC_Percent(date, date, dcs);
                periodDCs.AddRange(dcs);
            }

            // 할인 적용
            foreach (var dc in periodDCs)
            {
                dc.Apply(bill);
            }

            foreach (var it in periodDCs)
            {
                SogNo_PeriodFromTimeSlotDC_Percent dc = it as SogNo_PeriodFromTimeSlotDC_Percent;
                if (applyStart <= dc.StartDateTime && dc.EndDateTime <= applyEnd)
                {
                    dc.Apply(bill);
                }
                else
                {
                    DateTime dcStart = applyStart > dc.StartDateTime ? applyStart : dc.StartDateTime;
                    DateTime dcEnd = applyEnd < dc.EndDateTime ? applyEnd : dc.EndDateTime;

                    if (dcStart < dcEnd)
                    {
                        dc = new SogNo_PeriodFromTimeSlotDC_Percent(dcStart, dcEnd, dc.Percent);
                        dc.Apply(bill);
                    }
                }
            }
        }

        /// <summary>
        /// 시간대 할인을 기간 할인으로 변환함
        /// </summary>
        /// <param name="startDate">시간대 할인을 반복할 시작 날자</param>
        /// <param name="endDate">시간대 할인을 반복할 종료 날자</param>
        /// <param name="timeSlotDCs">반복할 시간대 할인들</param>
        /// <returns>변환된 기간 할인 리스트</returns>
        private ISogNo_Discount[] ConvertTimeSlotToPeriodDC_Percent(DateTime startDate, DateTime endDate, ISogNo_Discount[] timeSlotDCs)
        {
            startDate = startDate.Date;
            endDate = endDate.Date;

            List<ISogNo_Discount> periodDCs = new List<ISogNo_Discount>();
            if (timeSlotDCs != null)
            {
                foreach (var dc in timeSlotDCs)
                {
                    SogNo_TimeSlotDC_Percent timeSlotDC = dc as SogNo_TimeSlotDC_Percent;
                    if (timeSlotDC != null)
                    {
                        for (var date = startDate; date <= endDate; date = date.AddDays(1))
                        {
                            if (timeSlotDC.StartTime < timeSlotDC.EndTime)
                            {
                                periodDCs.Add(new SogNo_PeriodFromTimeSlotDC_Percent(date + timeSlotDC.StartTime, date + timeSlotDC.EndTime, timeSlotDC.Percent));
                            }
                            else
                            {
                                periodDCs.Add(new SogNo_PeriodFromTimeSlotDC_Percent(date + new TimeSpan(0, 0, 0, 0), date + timeSlotDC.EndTime, timeSlotDC.Percent));
                                periodDCs.Add(new SogNo_PeriodFromTimeSlotDC_Percent(date + timeSlotDC.StartTime, date + new TimeSpan(1, 0, 0, 0), timeSlotDC.Percent));
                            }
                        }
                    }
                }
            }

            return periodDCs.ToArray();
        }

        /// <summary>
        /// 사용자 지정 할인에 포함된 기간 할인을 적용함
        /// </summary>
        /// <param name="bill">주차 요금 계산서</param>
        /// <param name="discounts">사용자 지정 추가 할인 정보</param>
        /// <param name="applyStart">요금 계산 시작 시각</param>
        /// <param name="applyEnd">요금 계산 종료 시각</param>
        private void ApplyPeriodDC_Percent(SogNo_Bill bill, ISogNo_Discount[] discounts, DateTime applyStart, DateTime applyEnd)
        {
            var dcs = FindDCs(discounts, ISogNo_Discount.EDiscountType.PeriodDC_Percent);

            foreach (var it in dcs)
            {
                SogNo_PeriodDC_Percent dc = it as SogNo_PeriodDC_Percent;
                if (applyStart <= dc.StartDateTime && dc.EndDateTime <= applyEnd)
                {
                    dc.Apply(bill);
                }
                else
                {
                    DateTime dcStart = applyStart > dc.StartDateTime ? applyStart : dc.StartDateTime;
                    DateTime dcEnd = applyEnd < dc.EndDateTime ? applyEnd : dc.EndDateTime;

                    if (dcStart < dcEnd)
                    {
                        dc = new SogNo_PeriodDC_Percent(dcStart, dcEnd, dc.Percent);
                        dc.Apply(bill);
                    }
                }
            }
        }

        /// <summary>
        /// 요금제를 적용
        /// 
        /// - 날자별로 잘린 Details의 각 항목에 요금제를 적용
        /// </summary>
        /// <param name="bill">주차 요금 계산서</param>
        /// <param name="applyStart">요금 계산 시작 시각</param>
        /// <param name="applyEnd">요금 계산 종료 시각</param>
        private DateTime ApplyTariff_Pay(SogNo_Bill bill, DateTime applyStart, DateTime applyEnd)
        {
            bill.SortDetailsDesc();

            bool isBase = true;
            DateTime last = applyStart;

            for (int i = bill.Details.Count - 1; i >= 0; i--)
            {
                var item = bill.Details[i];


                if (item.Done || item.EndDateTime <= applyStart)
                {
                    if (item.Price > 0)
                        isBase = false;

                    last = item.EndDateTime;
                    continue;
                }
                else if (item.StartDateTime >= applyEnd)
                {
                    break;
                }

                SogNo_TariffForDay trfDay = FindTariffForDay(item.StartDateTime.Date);

                List<SogNo_Bill.DetailItem> appliedItems = new List<SogNo_Bill.DetailItem>();
                last = ApplySubTariff_Pay(item, trfDay, last, isBase, appliedItems);
                isBase = false;

                bill.RemoveDetail(item);
                bill.AddDetails(appliedItems.ToArray());
            }

            return last;
        }

        /// <summary>
        /// 세부 요금제를 적용
        /// </summary>
        /// <param name="item">주차 요금을 정산할 세부 구간. 시작 시각과 종료 시각은 동일 날자임.</param>
        /// <param name="trf">해당 날자의 일 요금제</param>
        /// <param name="last">먼저 처리한 세부 구간에서 요금 계산 시 포함된 종료 시각. 즉, 이 세부 구간에서 요금 계산할 시작 시각.</param>
        /// <param name="isBase">요금을 부과할 첫 세부 구간인지? 즉, 기본 요금을 적용해야 하는지 여부.</param>
        /// <param name="appliedItems">item을 더 잘게 나누어 요금제를 적용한 결과</param>
        /// <returns>요금 계산에 포함된 종료 시각 (last)</returns>
        private DateTime ApplySubTariff_Pay(SogNo_Bill.DetailItem item, SogNo_TariffForDay trf, DateTime last, bool isBase, List<SogNo_Bill.DetailItem> appliedItems)
        {
            if (last > item.StartDateTime)
            {
                appliedItems.Add(new SogNo_Bill.DetailItem(SogNo_Bill.DetailItem.EItemType.Rates, item.StartDateTime, last, null, false, null, 0, "요금제(" + this.Description + "-계속)", true));
            }

            while (last < item.EndDateTime)
            {
                SogNo_TariffForTime trfTime = trf.FindTariffForTime(last.TimeOfDay);
                
                DateTime beg = last;

                bool isBaseRates = (isBase && trfTime.BaseTime > 0);

                if (isBaseRates)
                    last = last.AddMinutes(trfTime.BaseTime);
                else
                    last = last.AddMinutes(trfTime.AddTime);

                DateTime end = last < item.EndDateTime ? last : item.EndDateTime;

                appliedItems.Add(new SogNo_Bill.DetailItem(SogNo_Bill.DetailItem.EItemType.Rates, beg, end, trfTime, isBaseRates, item.Discount));
                
                isBase = false;
            }

            return last;
        }

        /// <summary>
        /// 요금제의 무료인 구간을 적용
        /// 
        /// - 요금제 중 무료인 구간을 기간무료 할인으로 변환하여 적용
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="applyStart">요금 계산 시작 시각</param>
        /// <param name="applyEnd">요금 계산 종료 시각</param>
        private void ApplyTariff_Free(SogNo_Bill bill, DateTime applyStart, DateTime applyEnd)
        {
            List<ISogNo_Discount> periodDCs = new List<ISogNo_Discount>();

            // 일 요금제에 포함된 무료 구간을 기간무료 할인으로 변환
            for (var date = applyStart.Date; date <= applyEnd.Date; date = date.AddDays(1))
            {
                var tariff = FindTariffForDay(date);

                var dcs = ConvertFreeTariffToPeriodDC(date, tariff);
                periodDCs.AddRange(dcs);
            }

            // 할인 적용
            foreach (var it in periodDCs)
            {
                SogNo_PeriodFromTariff_Free dc = it as SogNo_PeriodFromTariff_Free;
                if (applyStart <= dc.StartDateTime && dc.EndDateTime <= applyEnd)
                {
                    dc.Apply(bill);
                }
                else
                {
                    DateTime dcStart = applyStart > dc.StartDateTime ? applyStart : dc.StartDateTime;
                    DateTime dcEnd = applyEnd < dc.EndDateTime ? applyEnd : dc.EndDateTime;

                    if (dcStart < dcEnd)
                    {
                        dc = new SogNo_PeriodFromTariff_Free(dcStart, dcEnd, this.Description);
                        dc.Apply(bill);
                    }
                }
            }
        }

        /// <summary>
        /// 일일 요금제 중 무료인 구간을 기간무료 할인으로 변환
        /// </summary>
        /// <param name="date">할인 일</param>
        /// <param name="tariff">일 요금제</param>
        /// <returns>변환된 기간뮤료 할인 리스트</returns>
        private ISogNo_Discount[] ConvertFreeTariffToPeriodDC(DateTime date, SogNo_TariffForDay tariff)
        {
            TimeSpan dayStartTime = new TimeSpan(0, 0, 0);
            TimeSpan dayEndTime = new TimeSpan(24, 0, 0);

            date = date.Date;

            List<ISogNo_Discount> periodDCs = new List<ISogNo_Discount>();
            if (tariff?.TariffsForTime != null)
            {
                TimeSpan end = dayStartTime;
                foreach (var subtrf in tariff.TariffsForTime)
                {
                    if (end < subtrf.StartTime)
                    {
                        periodDCs.Add(new SogNo_PeriodFromTariff_Free(date + end, date + subtrf.StartTime, this.Description));
                    }

                    if ((subtrf.BaseTime == 0 || subtrf.BaseRates == 0) && subtrf.AddRates == 0)
                    {
                        periodDCs.Add(new SogNo_PeriodFromTariff_Free(date + subtrf.StartTime, date + subtrf.EndTime, this.Description));
                    }

                    end = subtrf.EndTime;
                }

                if (end < dayEndTime)
                {
                    periodDCs.Add(new SogNo_PeriodFromTariff_Free(date + end, date + dayEndTime, this.Description));
                }
            }
            else
            {
                periodDCs.Add(new SogNo_PeriodFromTariff_Free(date + dayStartTime, date + dayEndTime, this.Description));
            }

            return periodDCs.ToArray();
        }
        
        /// <summary>
        /// 시간감면 할인 적용
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="discounts"></param>
        private void ApplyTimeDC(SogNo_Bill bill, ISogNo_Discount[] discounts)
        {
            var dcs = FindDCs(discounts, ISogNo_Discount.EDiscountType.TimeDC);
            foreach (var dc in dcs)
            {
                dc.Apply(bill);
            }
        }

        /// <summary>
        /// bill의 Details를 요금 계산이 편하도록 날자별로 분할
        /// 
        /// - 일일 기준이 입차 시각 기준일 경우, 날자 기준뿐만 아니라 입차 시각 기준으로도 나눔.
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="applyStart">요금 계산 시작 시각</param>
        /// <param name="applyEnd">요금 계산 종료 시각</param>
        private void DailySeparate(SogNo_Bill bill, DateTime applyStart, DateTime applyEnd)
        {
            TimeSpan dayStartTime = new TimeSpan(0, 0, 0);
            TimeSpan dayEndTime = new TimeSpan(24, 0, 0);
            TimeSpan inCarTime = bill.InDateTime.TimeOfDay;
            
            // applyStart, applyEnd로 우선 자름
            for (int i = bill.Details.Count - 1; i >= 0; i--)
            {
                var item = bill.Details[i];
                if (item.Done)
                    continue;

                DateTime itemStart = item.StartDateTime;
                DateTime itemEnd = item.EndDateTime;

                if (itemStart < applyStart && applyEnd < itemEnd)
                {
                    bill.AddDetail(new SogNo_Bill.DetailItem(itemStart, applyStart, item));
                    bill.AddDetail(new SogNo_Bill.DetailItem(applyStart, applyEnd, item));
                    bill.AddDetail(new SogNo_Bill.DetailItem(applyEnd, itemEnd, item));
                    bill.RemoveDetail(item);
                }
                else if (itemStart < applyStart && applyStart < itemEnd)
                {
                    bill.AddDetail(new SogNo_Bill.DetailItem(itemStart, applyStart, item));
                    bill.AddDetail(new SogNo_Bill.DetailItem(applyStart, itemEnd, item));
                    bill.RemoveDetail(item);
                }
                else if (itemStart < applyEnd && applyEnd < itemEnd)
                {
                    bill.AddDetail(new SogNo_Bill.DetailItem(itemStart, applyEnd, item));
                    bill.AddDetail(new SogNo_Bill.DetailItem(applyEnd, itemEnd, item));
                    bill.RemoveDetail(item);
                }
            }

            // 날자와 입차 시간으로 나눔.
            for (int i = bill.Details.Count - 1; i >= 0; i--)
            {
                var item = bill.Details[i];
                if (item.Done)
                    continue;

                DateTime itemStart = item.StartDateTime;
                DateTime itemEnd = item.EndDateTime;

                if (itemStart.Date == itemEnd.Date
                    && (/*this.DailyCriterion == EDailyCriterion.Day || */inCarTime <= itemStart.TimeOfDay || inCarTime >= itemEnd.TimeOfDay))
                    continue;

                for (DateTime date = itemStart.Date; date <= itemEnd.Date; date = date.AddDays(1))
                {
                    TimeSpan start = (date == itemStart.Date ? itemStart.TimeOfDay : dayStartTime);
                    TimeSpan end = (date == itemEnd.Date ? itemEnd.TimeOfDay : dayEndTime);

                    if (/*this.DailyCriterion == EDailyCriterion.InTime && */start < inCarTime && inCarTime < end)
                    {
                        bill.AddDetail(new SogNo_Bill.DetailItem(date + start, date + inCarTime, item));
                        bill.AddDetail(new SogNo_Bill.DetailItem(date + inCarTime, date + end, item));
                    }
                    else if (start != end)
                    {
                        bill.AddDetail(new SogNo_Bill.DetailItem(date + start, date + end, item));
                    }
                }

                bill.RemoveDetail(item);
            }
        }

        /// <summary>
        /// 기간무료 할인을 적용
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="discounts"></param>
        private void ApplyPeriodDC_Free(SogNo_Bill bill, ISogNo_Discount[] discounts)
        {
            var dcs = FindDCs(discounts, ISogNo_Discount.EDiscountType.PeriodDC_Free);
            foreach (var dc in dcs)
            {
                dc.Apply(bill);
            }
        }

        /// <summary>
        /// 시간대 무료 할인을 적용
        /// 
        /// - 시간대 무료 할인을 기간대 무료 할인으로 변환하여 적용
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="discounts"></param>
        private void ApplyTimeSlotDC_Free(SogNo_Bill bill, ISogNo_Discount[] discounts, DateTime applyStart, DateTime applyEnd)
        {
            List<ISogNo_Discount> periodDCs = new List<ISogNo_Discount>();

            // 사용자 지정 할인에 포함된 시간대 무료를 기간 무료로 변환
            var dcs = FindDCs(discounts, ISogNo_Discount.EDiscountType.TimeSlotDC_Free);
            dcs = ConvertTimeSlotToPeriodDC_Free(applyStart.Date, applyEnd.Date, dcs);
            periodDCs.AddRange(dcs);

            // 요금제에 포함된 시간대 무료를 기간 무료로 변환
            dcs = FindDCs(this.DefaultDiscounts, ISogNo_Discount.EDiscountType.TimeSlotDC_Free);
            dcs = ConvertTimeSlotToPeriodDC_Free(applyStart.Date, applyEnd.Date, dcs);
            periodDCs.AddRange(dcs);

            // 일 요금제에 포함된 시간대 무료를 기간 무료로 변환
            for (var date = applyStart.Date; date <= applyEnd.Date; date = date.AddDays(1))
            {
                var tariff = FindTariffForDay(date);
                dcs = FindDCs(tariff?.DefaultDiscounts, ISogNo_Discount.EDiscountType.TimeSlotDC_Free);
                dcs = ConvertTimeSlotToPeriodDC_Free(date, date, dcs);
                periodDCs.AddRange(dcs);
            }

            // 할인 적용
            foreach (var it in periodDCs)
            {
                SogNo_PeriodFromTimeSlotDC_Free dc = it as SogNo_PeriodFromTimeSlotDC_Free;
                if (applyStart <= dc.StartDateTime && dc.EndDateTime <= applyEnd)
                {
                    dc.Apply(bill);
                }
                else
                {
                    DateTime dcStart = applyStart > dc.StartDateTime ? applyStart : dc.StartDateTime;
                    DateTime dcEnd = applyEnd < dc.EndDateTime ? applyEnd : dc.EndDateTime;

                    if (dcStart < dcEnd)
                    {
                        dc = new SogNo_PeriodFromTimeSlotDC_Free(dcStart, dcEnd);
                        dc.Apply(bill);
                    }
                }
            }
        }

        /// <summary>
        /// 시간대 무료 할인을 기간 무료 할인으로 변환
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="timeSlotDCs"></param>
        /// <returns></returns>
        private ISogNo_Discount[] ConvertTimeSlotToPeriodDC_Free(DateTime startDate, DateTime endDate, ISogNo_Discount[] timeSlotDCs)
        {
            startDate = startDate.Date;
            endDate = endDate.Date;

            List<ISogNo_Discount> periodDCs = new List<ISogNo_Discount>();
            if (timeSlotDCs != null)
            {
                foreach (var dc in timeSlotDCs)
                {
                    SogNo_TimeSlotDC_Free timeSlotDC = dc as SogNo_TimeSlotDC_Free;
                    if (timeSlotDC != null)
                    {
                        for (var date = startDate; date <= endDate; date = date.AddDays(1))
                        {
                            if (timeSlotDC.StartTime < timeSlotDC.EndTime)
                            {
                                periodDCs.Add(new SogNo_PeriodFromTimeSlotDC_Free(date + timeSlotDC.StartTime, date + timeSlotDC.EndTime));
                            }
                            else
                            {
                                periodDCs.Add(new SogNo_PeriodFromTimeSlotDC_Free(date + new TimeSpan(0, 0, 0, 0), date + timeSlotDC.EndTime));
                                periodDCs.Add(new SogNo_PeriodFromTimeSlotDC_Free(date + timeSlotDC.StartTime, date + new TimeSpan(1, 0, 0, 0)));
                            }
                        }
                    }
                }
            }

            return periodDCs.ToArray();
        }


        /// <summary>
        /// 요금정산에 회차할인을 처리함
        /// </summary>
        /// <param name="bill"></param>
        private void ApplyTurningDC(SogNo_Bill bill)
        {
            ISogNo_Discount turningDC = FindTurningDC(bill.InDateTime);

            turningDC?.Apply(bill);
        }


        /// <summary>
        /// 회차할인 정보를 탐색
        /// 
        /// - Tariff, TariffForDay, TariffForTime에 회차할인 정보가 포함될 수 있음.
        /// - Tariff의 회차 정보는 해당 요금 체계에 전반에 적용되는 회차할인 정보임.
        /// - TariffForDay는 해당 요일(혹은 공휴일)에 적용되는 회차할인 정보임. Tariff의 회차할인 정보보다 우선함.
        /// - TariffForTime은 해당 요일의 해당 시간대에 적용되는 회차할인 정보임. Tariff 및 TariffForDay의 회차할인 정보보다 우선함.
        /// - 회차할인 정보는 Tariff, TariffForDay, TariffForTime 중 원하는 곳에 DefaultDiscount로 설정하면 됨. 
        ///     - 중복 설정도 가능하나 이 경우 세부 정보의 것을 사용함. 
        ///     - 회차할인 정보가 적용되지 않는 빈 시간대가 존재하면 안됨. 
        ///     - (예: Tariff에 회차할인 정보가 없고, TariffForDay 중 월화수목금에만 회차할인 정보를 설정한 경우, 휴일에 입차한 차량에 대해서는 회차 처리가 안됨)
        /// </summary>
        /// <param name="inDateTime">입차 시각</param>
        /// <returns>입차 시각 기준 회차할인 정보. 설정되어 있지 않은 경우 null.</returns>
        private ISogNo_Discount FindTurningDC(DateTime inDateTime)
        {
            ISogNo_Discount turningDC = null;

            var dcs = FindDCs(this.DefaultDiscounts, ISogNo_Discount.EDiscountType.TurningDC);
            if (dcs.Length > 0)
            {
                turningDC = dcs[0];
            }
            
            SogNo_TariffForDay tariffForDay = FindTariffForDay(inDateTime.Date);
            if (tariffForDay != null)
            {
                dcs = FindDCs(tariffForDay.DefaultDiscounts, ISogNo_Discount.EDiscountType.TurningDC);
                if (dcs.Length > 0)
                {
                    turningDC = dcs[0];
                }

                SogNo_TariffForTime tariffForTime = tariffForDay.FindTariffForTime(inDateTime.TimeOfDay);
                if (tariffForTime != null)
                {
                    dcs = FindDCs(tariffForTime.DefaultDiscounts, ISogNo_Discount.EDiscountType.TurningDC);
                    if (dcs.Length > 0)
                    {
                        turningDC = dcs[0];
                    }
                }
            }

            return turningDC;
        }


        /// <summary>
        /// 할인 리스트에서 특정 할인 유형을 전부 찾음
        /// </summary>
        /// <param name="discounts">할인 리스트</param>
        /// <param name="dcType">찾고자 하는 할인 유형</param>
        /// <returns>찾은 할인들</returns>
        private ISogNo_Discount[] FindDCs(ISogNo_Discount[] discounts, ISogNo_Discount.EDiscountType dcType)
        {
            if (discounts == null)
            {
                return new ISogNo_Discount[0];
            }

            return Array.FindAll(discounts, it => it.DiscountType == dcType);
        }

        /// <summary>
        /// 특정 날자에 적용되는 일 요금체계를 찾음
        /// </summary>
        /// <param name="date">찾고자 하는 날자</param>
        /// <returns>일 요금 체계</returns>
        private SogNo_TariffForDay FindTariffForDay(DateTime date)
        {
            bool isHoliday = this.HolidayIdentifier.IsPublicHoliday(date, out string holidayName);
            DayOfWeek dayOfWeek = date.DayOfWeek;
            if (isHoliday && this.HolidayPriority[(int)dayOfWeek])
            {
                return this.HolidayTariff;
            }
            else
            {
                switch (dayOfWeek)
                {
                    case DayOfWeek.Monday:
                        return this.MondayTariff;
                    case DayOfWeek.Tuesday:
                        return this.TuesdayTariff;
                    case DayOfWeek.Wednesday:
                        return this.WednesdayTariff;
                    case DayOfWeek.Thursday:
                        return this.ThursdayTariff;
                    case DayOfWeek.Friday:
                        return this.FridayTariff;
                    case DayOfWeek.Saturday:
                        return this.SaturdayTariff;
                    case DayOfWeek.Sunday:
                        return this.SundayTariff;
                    default:
                        return null;
                }
            }
        }


        private void ApplyRoundDC(SogNo_Bill bill)
        {
            int origin = bill.Price;

            if (this.RoundType == ERoundType.Floor)
                bill.Price = FloorInt(bill.Price, this.RoundDecimal);
            else if (this.RoundType == ERoundType.Round)
                bill.Price = RoundInt(bill.Price, this.RoundDecimal);
            else if (this.RoundType == ERoundType.Ceiling)
                bill.Price = CeilingInt(bill.Price, this.RoundDecimal);

            bill.Discount_RoundDC = origin - bill.Price;
        }

        private int CeilingInt(int value, int deci)
        {
            if (value >= 0)
            {
                int mod = value % deci;
                if (mod != 0)
                {
                    value += deci - mod;
                }
            }
            else
            {
                int mod = value % deci;
                value -= mod;
            }

            return value;
        }

        private int FloorInt(int value, int deci)
        {
            if (value > 0)
            {
                int mod = value % deci;
                value -= mod;
            }
            else
            {
                int mod = value % deci;
                if (mod != 0)
                {
                    value -= deci + mod;
                }
            }

            return value;
        }

        private int RoundInt(int value, int deci)
        {
            double half = deci / 2.0;

            if (value > 0)
            {
                int mod = value % deci;
                if (mod >= half)
                {
                    return CeilingInt(value, deci);
                }
                else
                {
                    return FloorInt(value, deci);
                }
            }
            else
            {
                int mod = value % deci;
                if (mod >= -half)
                {
                    return CeilingInt(value, deci);
                }
                else
                {
                    return FloorInt(value, deci);
                }
            }
        }
    }
}
