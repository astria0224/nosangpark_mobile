﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SognoLib.Park.Toll
{
    public class SogNo_TariffForTime
    {
        public TimeSpan StartTime { get; private set; }     // 요금제 적용 시작 시각
        public TimeSpan EndTime { get; private set; }       // 요금제 적용 종료 시각
        public int BaseTime { get; private set; }           // 기본요금 적용 시간 (분)
        public int BaseRates { get; private set; }          // 기본 요금 (원)
        public int AddTime { get; private set; }            // 추가요금 적용 시간 (분)
        public int AddRates { get; private set; }           // 추가 요금 (원)

        public ISogNo_Discount[] DefaultDiscounts { get; private set; }     // 기본으로 적용되는 할인 정보 (회차)

        /// <summary>
        /// 생성자
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="baseTime"></param>
        /// <param name="baseRates"></param>
        /// <param name="addTime"></param>
        /// <param name="addRates"></param>
        /// <param name="defaultDiscounts">기본으로 적용되는 할인 정보 (회차)</param>
        public SogNo_TariffForTime(TimeSpan startTime, TimeSpan endTime, int baseTime, int baseRates, int addTime, int addRates, ISogNo_Discount[] defaultDiscounts = null)
        {
            this.StartTime = startTime;
            this.EndTime = endTime;
            this.BaseTime = baseTime;
            this.BaseRates = baseRates;
            this.AddTime = addTime;
            this.AddRates = addRates;

            this.DefaultDiscounts = defaultDiscounts ?? new ISogNo_Discount[0];
        }
    }
}
