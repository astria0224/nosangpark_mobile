﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SognoLib.Park.Toll
{
    /// <summary>
    /// 주차 요금 계산서 클래스
    /// 
    /// - Details에 전체 주차 기간을 시간대별로 잘라 가면서 주차 요금을 계산
    /// </summary>
    public class SogNo_Bill
    {
        public DateTime InDateTime { get; private set; }        // 입차 시각
        public DateTime OutDateTime { get; private set; }       // 출차 시각

        public List<DetailItem> Details { get; private set; } = new List<DetailItem>();         // 시간대별 상세 요금 계산 내역
        public List<SummaryItem> DailySummary { get; private set; } = new List<SummaryItem>();  // 일별 요금 계산 내역

        public int OriginalPrice { get; set; } = 0;             // 할인 안한 주차 요금 (일 최대, 총 요금 한도는 적용)

        public int MaxRates { get; set; } = -1;
        public ISogNo_Discount PercentDC { get; set; } = null;  // 요금 할인 정보
        public ISogNo_Discount PriceDC { get; set; } = null;    // 요금 감면 정보
        public int Price { get; set; } = 0;                     // 최종 주차 요금 = OriginalPrice에 총 요금 한도, 요금 할인, 요금 감면 적용한 금액

        public bool IsTurning { get; set; } = false;
        public int Discount_TimePeriodDC { get; set; } = 0;
        public int Discount_PercentDC { get; set; } = 0;
        public int Discount_PriceDC { get; set; } = 0;
        public int Discount_RoundDC { get; set; } = 0;


        /// <summary>
        /// 생성자
        /// </summary>
        /// <param name="inDateTime">입차 시각</param>
        /// <param name="outDateTime">출차 시각</param>
        public SogNo_Bill(DateTime inDateTime, DateTime outDateTime)
        {
            this.InDateTime = inDateTime;
            this.OutDateTime = outDateTime;

            AddDetail(new DetailItem(DetailItem.EItemType.NotIdentified, this.InDateTime, this.OutDateTime));
        }
        
        public void AddDetail(DetailItem item)
        {
            this.Details.Add(item);
        }

        public void AddDetails(DetailItem[] items)
        {
            this.Details.AddRange(items);
        }

        public void RemoveDetail(DetailItem item)
        {
            this.Details.Remove(item);
        }

        public void SortDetails()
        {
            this.Details.Sort((a, b) => a.StartDateTime.CompareTo(b.StartDateTime));
        }

        public void SortDetailsDesc()
        {
            this.Details.Sort((a, b) => b.StartDateTime.CompareTo(a.StartDateTime));
        }

        public void AddSummary(SummaryItem item)
        {
            this.DailySummary.Add(item);
        }

        public int TotalDailyPrices()
        {
            int total = 0;
            foreach (var it in this.DailySummary)
            {
                total += it.Price;
            }

            return total;
        }


        public ISogNo_Discount.EDiscountType[] GetAppliedDiscounts()
        {
            HashSet<ISogNo_Discount.EDiscountType> set = new HashSet<ISogNo_Discount.EDiscountType>();
            foreach (var item in this.Details)
            {
                if (item.Discount != null)
                {
                    set.Add(item.Discount.DiscountType);
                }
            }

            return set.ToArray();
        }

        public string ToString_Details()
        {
            string summary = "";

            foreach (var it in this.Details)
            {
                summary += string.Format("{0} ~ {1} : {2, 6} 원, {3}, {4}, ({5}, {6}, {7})\n", it.StartDateTime.ToString("yyyy-MM-dd HH:mm:ss.fff"), it.EndDateTime.ToString("yyyy-MM-dd HH:mm:ss.fff"), it.Price, it.Description, it.Done, it.ItemType, it.Tariff != null, it.Discount != null);
            }

            return summary;
        }

        public string ToString_Summary()
        {
            string summary = "";

            foreach (var daily in this.DailySummary)
            {
                summary += "===========================================\n";
                summary += string.Format("* {0}\n", daily.Date.ToString("yyyy-MM-dd"));
                foreach (var it in daily.Details)
                {
                    summary += string.Format("   - {0} ~ {1} : {2, 6} 원, {3}\n", it.StartDateTime.ToString("yyyy-MM-dd HH:mm:ss.fff"), it.EndDateTime.ToString("yyyy-MM-dd HH:mm:ss.fff"), it.Price, it.Description);
                }
                summary += string.Format("   * daily total: {0, 6} 원\n", daily.OriginalPrice);
                if (daily.OriginalPrice != daily.Price)
                {
                    summary += string.Format("   * daily cap  : {0, 6} 원\n", daily.DailyMaxPrice);
                }
                summary += string.Format("   * price      : {0, 6} 원\n", daily.Price);
            }

            summary += string.Format("===========================================\n");
            int totalDailyPrices = TotalDailyPrices();
            summary += string.Format(" total price    : {0, 6} 원\n", totalDailyPrices);
            if (this.MaxRates >= 0 && totalDailyPrices > this.MaxRates)
            {
                summary += string.Format("   - price cap  : {0, 6} 원\n", this.MaxRates);
            }

            if (this.PercentDC != null)
            {
                SogNo_PercentDC dc = this.PercentDC as SogNo_PercentDC;
                summary += string.Format("   - {0}  : {1, 6} %\n", dc.DiscountTypeString(), dc.Percent);
            }

            if (this.PriceDC != null)
            {
                SogNo_PriceDC dc = this.PriceDC as SogNo_PriceDC;
                summary += string.Format("   - {0}  : {1, 6} 원\n", dc.DiscountTypeString(), dc.Price);
            }

            summary += string.Format(" final price    : {0, 6} 원\n", this.Price);

            summary += string.Format("===========================================\n");

            return summary;
        }

        public string ToString_Discounts()
        {
            string summary = "";

            if (this.OriginalPrice > 0)
                summary += string.Format("  이용요금     :  {0, 6} 원\n", this.OriginalPrice);
            if (this.IsTurning)
                summary += string.Format("  회차무료\n");
            if (this.Discount_TimePeriodDC > 0)
                summary += string.Format("  시간(대)할인 : -{0, 6} 원\n", this.Discount_TimePeriodDC);
            if (this.Discount_PercentDC > 0)
                summary += string.Format("  요금할인     : -{0, 6} 원\n", this.Discount_PercentDC);
            if (this.Discount_PriceDC > 0)
                summary += string.Format("  요금감면     : -{0, 6} 원\n", this.Discount_PriceDC);
            if (this.Discount_RoundDC > 0)
                summary += string.Format("  반올림       : -{0, 6} 원\n", this.Discount_RoundDC);

            return summary;
        }


        /// <summary>
        /// 상세 요금 정산 내역
        /// 
        /// - 시간대별로 잘라낸 부분 구간의 주차요금 정산 내역
        /// </summary>
        public class DetailItem
        {
            public enum EItemType
            {
                NotIdentified,      // 결정되지 않음
                Free,               // 무료인 구간 (요금제, 할인 등으로 인한)
                Rates               // 요금 발생하는 구간
            }

            public EItemType ItemType { get; private set; }             // 잘라낸 구간의 유/무료 종류
            public DateTime StartDateTime { get; private set; }         // 잘라낸 구간의 시작 시각
            public DateTime EndDateTime { get; private set; }           // 잘라낸 구간의 종료 시각

            public SogNo_TariffForTime Tariff { get; private set; }     // 적용할 요금제
            public ISogNo_Discount Discount { get; private set; }       // 적용할 할인

            public int Price { get; private set; }                      // 계산한 주차 요금
            public string Description { get; private set; }             // 계산 내용 설명
            public bool IsBaseRates { get; private set; }               // 요금 계산 시 기본요금 사용 여부
            public bool Done { get; private set; }                      // 계산 종료 여부

            public double TotalMinutes { get { return (EndDateTime - StartDateTime).TotalMinutes; } }       // 잘라낸 구간의 주차 시간 (분)

            /// <summary>
            /// 생성자
            /// 
            /// - rhs와 동일하나 시작 시각과 종료 시각만 다른 개체 생성
            /// </summary>
            /// <param name="startDateTime"></param>
            /// <param name="endDateTime"></param>
            /// <param name="rhs"></param>
            public DetailItem(DateTime startDateTime, DateTime endDateTime, DetailItem rhs)
            {
                this.ItemType = rhs.ItemType;
                this.StartDateTime = startDateTime;
                this.EndDateTime = endDateTime;
                this.Tariff = rhs.Tariff;
                this.Discount = rhs.Discount;
                this.Price = rhs.Price;
                this.Description = rhs.Description;
                this.IsBaseRates = rhs.IsBaseRates;
                this.Done = rhs.Done;
            }

            /// <summary>
            /// 생성자
            /// </summary>
            /// <param name="type"></param>
            /// <param name="startDateTime"></param>
            /// <param name="endDateTime"></param>
            /// <param name="subtrf"></param>
            /// <param name="isBaseRates"></param>
            /// <param name="discount"></param>
            /// <param name="price"></param>
            /// <param name="desc"></param>
            /// <param name="done"></param>
            public DetailItem(EItemType type, DateTime startDateTime, DateTime endDateTime, 
                SogNo_TariffForTime subtrf = null, bool isBaseRates = false, ISogNo_Discount discount = null, int price = 0, string desc = "", bool done = false)
            {
                this.ItemType = type;
                this.StartDateTime = startDateTime;
                this.EndDateTime = endDateTime;
                this.Tariff = subtrf;
                this.Discount = discount;
                this.Price = price;
                this.Description = desc;
                this.IsBaseRates = isBaseRates;
                this.Done = done;
            }

            public void SetDiscount(ISogNo_Discount discount)
            {
                this.Discount = discount;
            }
            
            public void SetDone(EItemType itemType, ISogNo_Discount discount, int price, string desc)
            {
                this.ItemType = itemType;
                this.Discount = discount;
                this.Price = price;
                this.Description = desc;
                this.Done = true;
            }

            /// <summary>
            /// 두 DetailItem을 병합
            /// 
            /// - 두 DetailItem의 속성(요금제, 할인 등)이 동일하다고 가정함.
            /// </summary>
            /// <param name="rhs"></param>
            public void Merge(DetailItem rhs)
            {
                System.Diagnostics.Debug.Assert(this.Description == rhs.Description);
                System.Diagnostics.Debug.Assert(this.EndDateTime == rhs.StartDateTime || this.StartDateTime == rhs.EndDateTime);

                this.StartDateTime = this.StartDateTime < rhs.StartDateTime ? this.StartDateTime : rhs.StartDateTime;
                this.EndDateTime = this.EndDateTime > rhs.EndDateTime ? this.EndDateTime : rhs.EndDateTime;
                this.Price += rhs.Price;
            }
        }


        /// <summary>
        /// 일별 요금 정산 내역
        /// </summary>
        public class SummaryItem
        {
            public DateTime Date { get; private set; }          // 기준 일
            public DetailItem[] Details { get; private set; }   // 기준 일에 포함된 상세 요금 정산 내역
            public int DailyMaxPrice { get; private set; }      // 일 최대 요금
            public int OriginalPrice { get; private set; }      // 상세 요금 정산 내역의 총합 (일 최대 요금을 적요하지 않은 금액)
            public int Price { get; private set; }              // 일 최대 요금을 적용한 최종 주차 요금
            

            public SummaryItem(DateTime date, DetailItem[] details, int dailyMaxPrice)
            {
                this.Date = date;
                this.Details = details ?? new DetailItem[0];
                this.DailyMaxPrice = dailyMaxPrice;

                this.OriginalPrice = GetOriginalPrice();
                this.Price = (this.DailyMaxPrice >= 0 && this.DailyMaxPrice < this.OriginalPrice) ? this.DailyMaxPrice : this.OriginalPrice;
            }

            /// <summary>
            /// 일일 부과된 요금을 합산한 요금 계산
            /// 
            /// - 전체 최대 요금은 적용하지 않음.
            /// </summary>
            /// <returns></returns>
            private int GetOriginalPrice()
            {
                int price = 0;
                foreach (var it in this.Details)
                {
                    price += it.Price;
                }

                return price;
            }

            public DateTime GetStartDateTime()
            {
                return this.Details.First().StartDateTime;
            }

            public DateTime GetEndDateTime()
            {
                return this.Details.Last().EndDateTime;
            }
        }
    }
}
