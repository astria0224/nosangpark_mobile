﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SognoLib.Park.Data;


namespace SognoLib.Park.Toll
{
    public class SogNo_TariffReader
    {
        private static NLog.Logger nlog = NLog.LogManager.GetCurrentClassLogger();

        private SogNo_TariffDataAccess dataAccess = null;


        public SogNo_TariffReader(int parkNo, SogNo_TariffDataAccess dataAccess)
        {
            this.dataAccess = dataAccess;
        }

        public Dictionary<int, SogNo_Tariff> ReadAllTariffs(int parkNo)
        {
            Dictionary<int, ISogNo_Discount> discounts = ReadAllDiscounts(parkNo);
            Dictionary<int, SogNo_TariffForTime> timeTariffs = ReadAllTimeTariffs(parkNo, discounts);
            Dictionary<int, SogNo_TariffForDay> dayTariffs = ReadAllDayTariffs(parkNo, timeTariffs, discounts);
            Dictionary<int, SogNo_Tariff> tariffs = ReadAllTariffs(parkNo, dayTariffs, discounts);

            return tariffs;
        }


        public Dictionary<int, SogNo_Tariff> ReadAllTariffs(int parkNo, Dictionary<int, SogNo_TariffForDay> dayTariffs, Dictionary<int, ISogNo_Discount> discounts)
        {
            TariffRecord[] trfRecords = this.dataAccess.ReadAllTariffs(parkNo);

            Dictionary<int, SogNo_Tariff> result = new Dictionary<int, SogNo_Tariff>();
            if (trfRecords != null)
            {
                foreach (var trfRecord in trfRecords)
                {
                    SogNo_Tariff trf = CvtDB2Sogno(trfRecord, dayTariffs, discounts);
                    if (trf != null)
                    {
                        result.Add(trfRecord.TrfNo.Value, trf);
                    }
                    else
                    {
                        nlog.Warn("** 요금제 변환 실패. DCNo = {0}", trfRecord.TrfNo);
                    }
                }
            }

            return result;
        }


        public Dictionary<int, SogNo_TariffForDay> ReadAllDayTariffs(int parkNo, Dictionary<int, SogNo_TariffForTime> timeTariffs,  Dictionary<int, ISogNo_Discount> discounts)
        {
            DayTariffRecord[] dayTrfRecords = this.dataAccess.ReadAllDayTariffs(parkNo);

            Dictionary<int, SogNo_TariffForDay> result = new Dictionary<int, SogNo_TariffForDay>();
            if (dayTrfRecords != null)
            {
                foreach (var dayTrfRecord in dayTrfRecords)
                {
                    SogNo_TariffForDay dayTrf = CvtDB2Sogno(dayTrfRecord, timeTariffs, discounts);
                    if (dayTrf != null)
                    {
                        result.Add(dayTrfRecord.DayTrfNo.Value, dayTrf);
                    }
                    else
                    {
                        nlog.Warn("** 일 요금제 변환 실패. DCNo = {0}", dayTrfRecord.DayTrfNo);
                    }
                }
            }

            return result;
        }


        private SogNo_Tariff CvtDB2Sogno(TariffRecord trfRecord, Dictionary<int, SogNo_TariffForDay> dayTrfs, Dictionary<int, ISogNo_Discount> discounts)
        {
            try
            {
                List<SogNo_TariffForDay> usingDayTrfs = new List<SogNo_TariffForDay>();

                if (trfRecord.DayTariffs != null && trfRecord.DayTariffs.Trim().Length > 0)
                {
                    string[] tokens = trfRecord.DayTariffs.Trim().Split(new char[] { ',' });
                    foreach (var token in tokens)
                    {
                        int no = int.Parse(token);
                        
                        if (no < 0)
                        {
                            usingDayTrfs.Add(null);
                        }
                        else if (dayTrfs.ContainsKey(no))
                        {
                            usingDayTrfs.Add(dayTrfs[no]);
                        }
                        else
                        {
                            nlog.Warn("** 일 요금제 정보가 없음. DayTrfNo = {0}", no);
                            usingDayTrfs.Add(null);
                        }
                    }
                }


                List<ISogNo_Discount> defaultDCs = new List<ISogNo_Discount>();

                if (trfRecord.DefaultDCs != null && trfRecord.DefaultDCs.Trim().Length > 0)
                {
                    string[] tokens = trfRecord.DefaultDCs.Trim().Split(new char[] { ',' });
                    foreach (var token in tokens)
                    {
                        int no = int.Parse(token);

                        if (discounts.ContainsKey(no))
                        {
                            defaultDCs.Add(discounts[no]);
                        }
                        else
                        {
                            nlog.Warn("** 할인 정보가 없음. DCNo = {0}", no);
                        }
                    }
                }


                bool[] holidayPriority = new bool[7] { true, true, true, true, true, true, true };
                if (trfRecord.HolidayPrior != null)
                {
                    for (int i = 0; i < trfRecord.HolidayPrior.Length; i++)
                    {
                        if (trfRecord.HolidayPrior[i] == '0')
                            holidayPriority[i] = false;
                    }
                }

                SogNo_Tariff.EDailyCriterion dailyCriterion = (trfRecord.DailyCriterion == TariffRecord.DailyCriterion_Day ?
                                                SogNo_Tariff.EDailyCriterion.Day : SogNo_Tariff.EDailyCriterion.InTime);

                SogNo_Tariff.ERoundType roundType = (trfRecord.RoundType == TariffRecord.RoundType_None ? SogNo_Tariff.ERoundType.None :
                                                        trfRecord.RoundType == TariffRecord.RoundType_Ceiling ? SogNo_Tariff.ERoundType.Ceiling :
                                                        trfRecord.RoundType == TariffRecord.RoundType_Floor ? SogNo_Tariff.ERoundType.Floor : SogNo_Tariff.ERoundType.Round);


                SogNo_Tariff tariff = new SogNo_Tariff(
                    trfRecord.TrfName,
                    usingDayTrfs.Count > TariffRecord.DayIndex_Monday ? usingDayTrfs[TariffRecord.DayIndex_Monday] : null,
                    usingDayTrfs.Count > TariffRecord.DayIndex_Tuesday ? usingDayTrfs[TariffRecord.DayIndex_Tuesday] : null,
                    usingDayTrfs.Count > TariffRecord.DayIndex_Wednesday ? usingDayTrfs[TariffRecord.DayIndex_Wednesday] : null,
                    usingDayTrfs.Count > TariffRecord.DayIndex_Thursday ? usingDayTrfs[TariffRecord.DayIndex_Thursday] : null,
                    usingDayTrfs.Count > TariffRecord.DayIndex_Friday ? usingDayTrfs[TariffRecord.DayIndex_Friday] : null,
                    usingDayTrfs.Count > TariffRecord.DayIndex_Saturday ? usingDayTrfs[TariffRecord.DayIndex_Saturday] : null,
                    usingDayTrfs.Count > TariffRecord.DayIndex_Sunday ? usingDayTrfs[TariffRecord.DayIndex_Sunday] : null,
                    trfRecord.HolidayTariff != null && dayTrfs.ContainsKey(trfRecord.HolidayTariff.Value) ? dayTrfs[trfRecord.HolidayTariff.Value] : null,
                    holidayPriority,
                    defaultDCs.Count == 0 ? null : defaultDCs.ToArray(),
                    trfRecord.MaxRates.Value,
                    trfRecord.DailyMaxRates.Value,
                    dailyCriterion,
                    roundType,
                    trfRecord.RoundDecimal.Value);

                return tariff;
            }
            catch
            {

            }

            return null;
        }


        private SogNo_TariffForDay CvtDB2Sogno(DayTariffRecord dayTrfRecord, Dictionary<int, SogNo_TariffForTime> timeTrfs, Dictionary<int, ISogNo_Discount> discounts)
        {
            try
            {
                List<SogNo_TariffForTime> usingTimeTrfs = new List<SogNo_TariffForTime>();

                if (dayTrfRecord.TimeTariffs != null && dayTrfRecord.TimeTariffs.Trim().Length > 0)
                {
                    string[] tokens = dayTrfRecord.TimeTariffs.Trim().Split(new char[] { ',' });
                    foreach (var token in tokens)
                    {
                        int no = int.Parse(token);

                        if (timeTrfs.ContainsKey(no))
                        {
                            usingTimeTrfs.Add(timeTrfs[no]);
                        }
                        else
                        {
                            nlog.Warn("** 시간대 요금제 정보가 없음. TimeTrfNo = {0}", no);
                        }
                    }
                }


                List<ISogNo_Discount> defaultDCs = new List<ISogNo_Discount>();

                if (dayTrfRecord.DefaultDCs != null && dayTrfRecord.DefaultDCs.Trim().Length > 0)
                {
                    string[] tokens = dayTrfRecord.DefaultDCs.Trim().Split(new char[] { ',' });
                    foreach (var token in tokens)
                    {
                        int no = int.Parse(token);

                        if (discounts.ContainsKey(no))
                        {
                            defaultDCs.Add(discounts[no]);
                        }
                        else
                        {
                            nlog.Warn("** 할인 정보가 없음. DCNo = {0}", no);
                        }
                    }
                }

                SogNo_TariffForDay dayTrf = new SogNo_TariffForDay(
                    usingTimeTrfs.Count == 0 ? null : usingTimeTrfs.ToArray(),
                    defaultDCs.Count == 0 ? null : defaultDCs.ToArray());

                return dayTrf;
            }
            catch
            {

            }

            return null;
        }


        public Dictionary<int, SogNo_TariffForTime> ReadAllTimeTariffs(int parkNo, Dictionary<int, ISogNo_Discount> discounts)
        {
            TimeTariffRecord[] timeTrfRecords = this.dataAccess.ReadAllTimeTariffs(parkNo);

            Dictionary<int, SogNo_TariffForTime> result = new Dictionary<int, SogNo_TariffForTime>();
            if (timeTrfRecords != null)
            {
                foreach (var timeTrfRecord in timeTrfRecords)
                {
                    SogNo_TariffForTime timeTrf = CvtDB2Sogno(timeTrfRecord, discounts);
                    if (timeTrf != null)
                    {
                        result.Add(timeTrfRecord.TimeTrfNo.Value, timeTrf);
                    }
                    else
                    {
                        nlog.Warn("** 시간대 요금제 변환 실패. DCNo = {0}", timeTrfRecord.TimeTrfNo);
                    }
                }
            }

            return result;
        }


        private SogNo_TariffForTime CvtDB2Sogno(TimeTariffRecord timeTrfRecord, Dictionary<int, ISogNo_Discount> discounts)
        {
            try
            {
                List<ISogNo_Discount> defaultDCs = new List<ISogNo_Discount>();

                if (timeTrfRecord.DefaultDCs != null && timeTrfRecord.DefaultDCs.Trim().Length > 0)
                {
                    string[] tokens = timeTrfRecord.DefaultDCs.Trim().Split(new char[] { ',' });
                    foreach (var token in tokens)
                    {
                        int no = int.Parse(token);

                        if (discounts.ContainsKey(no))
                        {
                            defaultDCs.Add(discounts[no]);
                        }
                        else
                        {
                            nlog.Warn("** 할인 정보가 없음. DCNo = {0}", no);
                        }
                    }
                }

                SogNo_TariffForTime timeTrf = new SogNo_TariffForTime(
                    timeTrfRecord.StartTime.Value,
                    timeTrfRecord.EndTime.Value,
                    timeTrfRecord.BaseTime.Value,
                    timeTrfRecord.BaseRates.Value,
                    timeTrfRecord.AddTime.Value,
                    timeTrfRecord.AddRates.Value,
                    defaultDCs.Count == 0 ? null : defaultDCs.ToArray());

                return timeTrf;
            }
            catch
            {

            }

            return null;
        }

        public Dictionary<int, ISogNo_Discount> ReadAllDiscounts(int parkNo)
        {
            DCInfoRecord[] dcInfos = this.dataAccess.ReadAllDiscounts(parkNo);

            Dictionary<int, ISogNo_Discount> result = new Dictionary<int, ISogNo_Discount>();
            if (dcInfos != null)
            {
                foreach (var dcInfo in dcInfos)
                {
                    ISogNo_Discount dc = CvtDB2Sogno(dcInfo);
                    if (dc != null)
                    {
                        result.Add(dcInfo.DCNo.Value, dc);
                    }
                    else
                    {
                        nlog.Warn("** 할인 변환 실패. DCNo = {0}", dcInfo.DCNo);
                    }
                }
            }

            return result;
        }


        private ISogNo_Discount CvtDB2Sogno(DCInfoRecord dcInfo)
        {
            try
            {
                if (dcInfo.DCType == DCInfoRecord.DCType_TurningDC)
                {
                    int val1 = int.Parse(dcInfo.DCValue1.Trim());
                    int val2 = 0;
                    if (dcInfo.DCValue2 != null && dcInfo.DCValue2.Length > 0)
                        val2 = int.Parse(dcInfo.DCValue2.Trim());

                    ISogNo_Discount dc = new SogNo_TurningDC(val1, val2);
                    return dc;
                }
                else if (dcInfo.DCType == DCInfoRecord.DCType_TimeDC)
                {
                    int val1 = int.Parse(dcInfo.DCValue1.Trim());

                    ISogNo_Discount dc = new SogNo_TimeDC(val1);
                    return dc;
                }
                else if (dcInfo.DCType == DCInfoRecord.DCType_PeriodDC_Free)
                {
                    string val1 = dcInfo.DCValue1.Trim();
                    string[] tokens = val1.Split(new char[] { '~' });
                    DateTime startDateTime = DateTime.Parse(tokens[0].Trim());
                    DateTime endDateTime = DateTime.Parse(tokens[1].Trim());

                    ISogNo_Discount dc = new SogNo_PeriodDC_Free(startDateTime, endDateTime);
                    return dc;
                }
                else if (dcInfo.DCType == DCInfoRecord.DCType_PeriodDC_Percent)
                {
                    string val1 = dcInfo.DCValue1.Trim();
                    string[] tokens = val1.Split(new char[] { '~' });
                    DateTime startDateTime = DateTime.Parse(tokens[0].Trim());
                    DateTime endDateTime = DateTime.Parse(tokens[1].Trim());

                    int val2 = int.Parse(dcInfo.DCValue2.Trim());

                    ISogNo_Discount dc = new SogNo_PeriodDC_Percent(startDateTime, endDateTime, val2);
                    return dc;
                }
                else if (dcInfo.DCType == DCInfoRecord.DCType_TimeSlotDC_Free)
                {
                    string val1 = dcInfo.DCValue1.Trim();
                    string[] tokens = val1.Split(new char[] { '~' });
                    TimeSpan startTime = ParseTimeSpan(tokens[0].Trim()).Value;
                    TimeSpan endTime = ParseTimeSpan(tokens[1].Trim()).Value;

                    ISogNo_Discount dc = new SogNo_TimeSlotDC_Free(startTime, endTime);
                    return dc;
                }
                else if (dcInfo.DCType == DCInfoRecord.DCType_TimeSlotDC_Percent)
                {
                    string val1 = dcInfo.DCValue1.Trim();
                    string[] tokens = val1.Split(new char[] { '~' });
                    TimeSpan startTime = ParseTimeSpan(tokens[0].Trim()).Value;
                    TimeSpan endTime = ParseTimeSpan(tokens[1].Trim()).Value;

                    int val2 = int.Parse(dcInfo.DCValue2.Trim());

                    ISogNo_Discount dc = new SogNo_TimeSlotDC_Percent(startTime, endTime, val2);
                    return dc;
                }
                else if (dcInfo.DCType == DCInfoRecord.DCType_PercentDC)
                {
                    int val1 = int.Parse(dcInfo.DCValue1.Trim());

                    ISogNo_Discount dc = new SogNo_PercentDC(val1);
                    return dc;
                }
                else if (dcInfo.DCType == DCInfoRecord.DCType_PriceDC)
                {
                    int val1 = int.Parse(dcInfo.DCValue1.Trim());

                    ISogNo_Discount dc = new SogNo_PriceDC(val1);
                    return dc;
                }
            }
            catch
            {

            }

            return null;
        }


        public TimeSpan? ParseTimeSpan(string str)
        {
            try
            {
                string[] tokens = str.Split(new char[] { ':' });

                int hours = 0;
                int minutes = 0;
                int seconds = 0;

                if (tokens.Length > 0)
                {
                    hours = int.Parse(tokens[0]);
                }

                if (tokens.Length > 1)
                {
                    minutes = int.Parse(tokens[1]);
                }

                if (tokens.Length > 2)
                {
                    seconds = int.Parse(tokens[2]);
                }

                return new TimeSpan(hours, minutes, seconds);
            }
            catch
            {
                return null;
            }
        }
    }
}
