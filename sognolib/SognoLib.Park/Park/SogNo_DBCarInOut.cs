﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SognoLib.Core;


namespace SognoLib.Park
{
    public class SogNo_DBCarInOut
    {
        private static NLog.Logger _log = NLog.LogManager.GetCurrentClassLogger();

        private readonly ParkInfoRecord park = null;
        private SogNo_DBAccessor parkDB = null;
        private int ParkNo
        {
            get
            {
                return this.park.ParkNo.Value;
            }
        }


        public class InCarBeginResults
        {
            public CarIODataRecord CarIOData { get; set; } = null;
            public CustomerInfoRecord CustomerInfo { get; set; } = null;
            public GroupInfoRecord GroupInfo { get; set; } = null;

            public string TKNo { get { return this.CarIOData?.TKNo; } }
            public string GroupName { get { return this.CarIOData?.GroupName;  } }
            public string CarNumber
            {
                get
                {
                    if (Functions.IsRead(this.CarIOData.InCarNum1) || Functions.IsNoread(this.CarIOData.InCarNum2))
                        return this.CarIOData.InCarNum1;
                    else
                        return this.CarIOData.InCarNum2;
                }
            }
            public DateTime? InDate { get { return this.CarIOData?.InDate; } }
            public int? CustNo { get { return this.CustomerInfo?.CustNo; } }
            public int? GroupNo { get { return this.GroupInfo?.GroupNo; } }
            public bool? IsInBlackList { get { return this.GroupInfo?.GroupType == Constant.GroupType_BlackList; } }
        }


        public class InCarBackResults
        {
            public enum EStatus : int
            {
                Ignored = 0,
                Inserted = 1,
                Updated = 2
            }

            public EStatus Status { get; set; } = EStatus.Ignored;
            public CarIODataRecord CarIOData { get; set; } = null;
            
            public string TKNo { get { return this.CarIOData?.TKNo; } }
            public string GroupName { get { return this.CarIOData?.GroupName; } }
            public string CarNumber
            {
                get
                {
                    if (Functions.IsRead(this.CarIOData.InCarNum1) || Functions.IsNoread(this.CarIOData.InCarNum2))
                        return this.CarIOData.InCarNum1;
                    else
                        return this.CarIOData.InCarNum2;
                }
            }
            public DateTime? CheckInTime { get { return this.CarIOData?.InDate; } }
        }


        public class OutCarBeginResults
        {
            public enum EStatus : int
            {
                NoIOData = 1,
                ExistIOData = 2
            }

            public EStatus Status { get; set; } = EStatus.NoIOData;
            public CarIODataRecord CarIOData { get; set; } = null;
            public CustomerInfoRecord CustomerInfo { get; set; } = null;
            public GroupInfoRecord GroupInfo { get; set; } = null;

            public string TKNo { get { return this.CarIOData?.TKNo; } }
            public string GroupName { get { return this.CarIOData?.GroupName; } }
            public string CarNumber
            {
                get
                {
                    if (Functions.IsRead(this.CarIOData?.OutCarNum1))
                        return this.CarIOData?.OutCarNum1;
                    else if (Functions.IsRead(this.CarIOData?.InCarNum1))
                        return this.CarIOData?.InCarNum1;
                    else if (Functions.IsRead(this.CarIOData?.InCarNum2))
                        return this.CarIOData?.InCarNum2;
                    else
                        return this.CarIOData?.OutCarNum1;
                }
            }
            public DateTime? CheckInTime { get { return this.CarIOData?.InDate; } }
            public int? ParkingTime { get { return this.CarIOData?.ParkingTime; } }
        }


        /// <summary>
        /// 생성자
        /// </summary>
        /// <param name="parkInfo">주차장 정보 테이블에 등록된 레코드</param>
        /// <param name="dbAccessor">DB 접근자</param>
        public SogNo_DBCarInOut(ParkInfoRecord parkInfo, SogNo_DBAccessor dbAccessor)
        {
            this.park = parkInfo;
            this.parkDB = dbAccessor;
        }


        /// <summary>
        /// 전면 카메라 입차 처리 시작
        /// </summary>
        /// <param name="camNo">영상을 촬영한 카메라 번호</param>
        /// <param name="carNumber">인식한 차량 번호. 미인식인 경우 'x'나 'X'가 포함됨</param>
        /// <param name="carType">차량의 유형. Constant.CarType_* 참고</param>
        /// <param name="imagePath">저장한 영상 경로</param>
        /// <returns>
        /// - CarIODataRecord
        ///     - ParkNo, TKNo, [ TKType, CarType, GroupNo, GroupName, Comp, Dept, Name, ] InDate, InCamNo, InCarNum1, InImage1 필드의 값을 설정하여 반환
        ///     - 동일 레코드를 DB에 기록함
        /// - CustomerInfoRecord
        ///     - carNumber로 등록된 정기권 차량 정보를 DB에서 조회하여 반환. 없을 시 null.
        /// - GroupInfoRecord
        ///     - 정기권 차량 정보의 GroupNo 값을 DB에서 조회하여 정기권 그룹 정보를 반환. 없을 시 null.
        /// CustomerInfo는 있으나 GroupInfo 가 없을 경우 둘 다 null 반환. (DB 정보 이상)
        /// </returns>
        /// 전면 카메라의 입차 처리를 시작함. InCarEnd()를 호출하여 입차 처리를 완료 해야함.
        /// 양방향 인식인 경우 InCarBack()은 후면 카메라의 인식 결과를 입차 정보에 설정함.
        /// 호출 순서는 정상적인 경우 InCarBegin() -> InCarEnd() [ -> InCarBack() ]
        public InCarBeginResults InCarBegin(int camNo, string carNumber, int carType, string imagePath, DateTime? occurTime)
        {
            bool isRead = Functions.IsRead(carNumber);
            DateTime localtime = occurTime ?? DateTime.Now;

            bool isRegistered = false;
            bool isBlackList = false;
            CustomerInfoRecord customerInfo = null;
            GroupInfoRecord groupInfo = null;

            if (isRead)
            {
                // 등록 여부 검색
                this.parkDB.SelectRegisterInfo(this.ParkNo, carNumber, ref customerInfo, ref groupInfo);

                if (customerInfo != null && groupInfo != null)
                {
                    isRegistered = true;
                    isBlackList = (groupInfo.GroupType == Constant.GroupType_BlackList);
                }
            }

            // 입차 처리
            CarIODataRecord newRecord = new CarIODataRecord();
            newRecord.ParkNo = this.ParkNo;
            newRecord.TKNo = Functions.CreateTKNo(camNo, localtime);
            if (isRead)
            {
                newRecord.TKType = (isRegistered == true ? groupInfo.GroupType : Constant.TKType_NotRegistered);
            }
            newRecord.CarType = carType;
            if (isRegistered)
            {
                newRecord.GroupNo = groupInfo.GroupNo.Value;
                newRecord.GroupName = groupInfo.GroupName;
                newRecord.Comp = customerInfo.Comp;
                newRecord.Dept = customerInfo.Dept;
                newRecord.Name = customerInfo.Name;
            }
            newRecord.InDate = localtime;
            newRecord.InCamNo = camNo;
            newRecord.InCarNum1 = carNumber;
            newRecord.InImage1 = imagePath.Replace('\\', '/');

            this.parkDB.InsertCarInData(newRecord);

            // 반환값 정리
            InCarBeginResults inCarResult = new InCarBeginResults();
            inCarResult.CarIOData = newRecord;
            if (isRegistered)
            {
                inCarResult.CustomerInfo = customerInfo;
                inCarResult.GroupInfo = groupInfo;
            }

            return inCarResult;
        }


        /// <summary>
        /// 후면 카메라의 인식 결과로 입차 정보 업데이트
        /// </summary>
        /// <param name="camNo">영상을 촬영한 카메라 번호</param>
        /// <param name="carNumber">인식한 차량 번호. 미인식인 경우 'x'나 'X'가 포함됨</param>
        /// <param name="imagePath">저장한 영상 경로</param>
        /// <returns>
        /// - CarIODataRecord
        ///     - 같은 camNo를 가진 마지막 CarIODataRecord를 찾음. 입차 데이터가 없을 경우 null. (이상 상황)
        ///     - [ TKType, GroupNo, GroupName, Comp, Dept, Name, ] InCarNum2, InImage2 필드를 설정
        ///     - 차량 번호를 전면이 미인식, 후면이 인식한 경우 [ TKType, ... ] 등을 설정
        ///     - 동일한 내용을 DB에 업데이트
        /// </returns>
        public InCarBackResults InCarBack(int camNo, string carNumber, string imagePath, DateTime? occurTime)
        {
            InCarBackResults inCarResult = new InCarBackResults();

            bool isRead = Functions.IsRead(carNumber);
            DateTime localtime = occurTime ?? DateTime.Now;

            CarIODataRecord[] carIODataRecords = this.parkDB.SelectLastCarIOData_Cam(this.ParkNo, camNo, 1);

            if (carIODataRecords.Length == 0 || carIODataRecords[0].InImage2 != null || 
                (carIODataRecords[0].OutType != null && carIODataRecords[0].OutType.Value != Constant.OutType_NotOut))
            {
                _log.Warn("** 입차(후면) 정보를 업데이트할 입차(전면) 데이터가 없습니다.");
            }
            else if (carIODataRecords[0].InDate != null && ((TimeSpan)(localtime - carIODataRecords[0].InDate)).Minutes >= 10)
            {
                _log.Warn("** 입차(후면) 정보를 업데이트할 입차(전면) 데이터가 10분 이상 차이납니다.");
            }
            else
            {
                bool isRegistered = false;

                CustomerInfoRecord customerInfo = null;
                GroupInfoRecord groupInfo = null;

                var ioData = carIODataRecords[0];
                bool frontNoread = Functions.IsNoread(ioData.InCarNum1);
                if (frontNoread && isRead)
                {
                    // 미출차 데이터 처리
                    this.parkDB.ClearCarOutMissingRecords(this.ParkNo, carNumber, localtime, ioData.TKNo);

                    // 등록 여부 검색
                    this.parkDB.SelectRegisterInfo(this.ParkNo, carNumber, ref customerInfo, ref groupInfo);

                    if (customerInfo != null && groupInfo != null)
                    {
                        isRegistered = true;
                    }

                    ioData.TKType = (isRegistered == true ? groupInfo.GroupType : Constant.TKType_NotRegistered);
                    ioData.GroupNo = groupInfo?.GroupNo;
                    ioData.GroupName = groupInfo?.GroupName;
                    ioData.Comp = customerInfo?.Comp;
                    ioData.Dept = customerInfo?.Dept;
                    ioData.Name = customerInfo?.Name;
                }

                ioData.InCarNum2 = carNumber;
                ioData.InImage2 = imagePath.Replace('\\', '/');

                this.parkDB.UpdateCarInData_AtBack(ioData);

                if (!frontNoread && isRead && ioData.InCarNum1 != ioData.InCarNum2)
                {
                    _log.Warn("** 전면/후면 인식 결과 다름. TKNo={0}, 전면={1}, 후면={2}", ioData.TKNo, ioData.InCarNum1, ioData.InCarNum2);
                }

                // 반환값 정리
                inCarResult.Status = InCarBackResults.EStatus.Updated;
                inCarResult.CarIOData = ioData;
            }

            return inCarResult;
        }


        /// <summary>
        /// 전면 카메라 입차 처리 완료
        /// </summary>
        /// <param name="updateIOData">업데이트된 입출차 데이터. [ TKType, GroupNo, GroupName, Comp, Dept, Name, ] InCarNum1의 수정된 정보를 적용함.</param>
        /// <param name="inType">입차 타입을 설정함. 입차 허용한 경우 OutType=NotOut으로, 입차 거절한 경우는 OutType=Missing으로 설정함</param>
        /// <param name="isCarNumChanged">차량 번호의 변경 여부. LPR이 인식한 번호를 팝업창 등에서 수정했는지 여부</param>
        /// - if isCarNumberChanged == true
        ///     - [ TKType, GroupNo, GroupName, Comp, Dept, Name, ] InCarNum1, InType, OutType을 DB에 업데이트함
        /// - else
        ///     - InType, OutType을 DB에 업데이트함
        public void InCarEnd(CarIODataRecord updateIOData, int inType, bool isCarNumChanged = false)
        {
            if (Functions.IsRead(updateIOData.InCarNum1))
            {
                this.parkDB.ClearCarOutMissingRecords(this.ParkNo, updateIOData.InCarNum1, updateIOData.InDate.Value, updateIOData.TKNo);
            }
             
            updateIOData.InType = inType;
            if (updateIOData.InType != Constant.InType_Deny)
            {
                updateIOData.OutType = Constant.OutType_NotOut;
            }
            else
            {
                updateIOData.OutType = Constant.OutType_Missing;
            }

            if (isCarNumChanged)
            {
                this.parkDB.UpdateCarInData_AtEnd(updateIOData);
            }
            else
            {
                this.parkDB.UpdateCarInOutType(updateIOData);
            }
        }


        /// <summary>
        /// 출차 처리 시작
        /// </summary>
        /// <param name="camNo">영상을 촬영한 카메라 번호</param>
        /// <param name="carNumber">인식한 차량 번호. 미인식인 경우 'x'나 'X'가 포함됨</param>
        /// <param name="carType">차량의 유형. Constant.CarType_* 참고</param>
        /// <param name="imagePath">저장한 영상 경로</param>
        /// <returns>
        /// - CarIODataRecord
        ///     - 미인식: ParkNo, OutDate, OutCamNo, OutCarNum1, OutImage1, CarType
        ///     - 입차 정보가 없는 경우: ParkNo, [ TKType, GroupNo, GroupName, Comp, Dept, Name, ], OutDate, OutCamNo, OutCarNum1, OutImage1, CarType을 설정하여 반환.
        ///     - 입차 정보가 있는 경우: [ TKType, GroupNo, GroupName, Comp, Dept, Name, ], OutDate, OutCamNo, OutCarNum1, OutImage1를 추가하여 반환
        /// - CustomerInfoRecord
        ///     - 미인식: null
        ///     - 입차 정보가 없는 경우: carNumber로 등록된 정기권 차량 정보를 DB에서 조회하여 반환. 없을 시 null.
        ///     - 입차 정보가 있는 경우: InCarNum1, InCarNum2, OutCarNum1 순으로 인식한 번호를 사용하여 DB에서 조회. 없을 시 null.
        /// - GroupInfoRecord
        ///     - 정기권 차량 정보의 GroupNo 값을 DB에서 조회하여 정기권 그룹 정보를 반환. 없을 시 null.
        /// CustomerInfo는 있으나 GroupInfo 가 없을 경우 둘 다 null 반환. (DB 정보 이상)
        /// </returns>
        public OutCarBeginResults OutCarBegin(int camNo, string carNumber, int carType, string imagePath, DateTime? occurTime)
        {
            OutCarBeginResults outCarResult = new OutCarBeginResults();

            bool isRead = Functions.IsRead(carNumber);

            DateTime localtime = occurTime ?? DateTime.Now;

            if (isRead)
            {
                CarIODataRecord[] carIODataRecords = this.parkDB.SelectLastCarIOData_Car(this.ParkNo, carNumber, 1);

                if (carIODataRecords.Length == 0 || 
                    carIODataRecords[0].InType == null || 
                    carIODataRecords[0].InType == Constant.InType_Deny || 
                    carIODataRecords[0].OutDate != null || 
                    (carIODataRecords[0].OutType != null && carIODataRecords[0].OutType.Value != Constant.OutType_NotOut))
                {
                    _log.Warn("** 출차 정보를 업데이트할 입차 데이터가 없습니다.");
                    foreach (var record in carIODataRecords)
                    {
                        _log.Debug("  - TKNo={0}, CarNum=({1}, {2}), InDate={3}, InType={4}, OutType={5}", record.TKNo, record.InCarNum1, record.InCarNum2, record.InDate, record.InType, record.OutType);
                    }

                    // 등록 여부 검색
                    bool isRegistered = false;
                    CustomerInfoRecord customerInfo = null;
                    GroupInfoRecord groupInfo = null;
                    this.parkDB.SelectRegisterInfo(this.ParkNo, carNumber, ref customerInfo, ref groupInfo);

                    if (customerInfo != null && groupInfo != null)
                    {
                        isRegistered = true;
                    }

                    CarIODataRecord ioData = new CarIODataRecord();
                    ioData.ParkNo = this.ParkNo;
                    ioData.OutDate = localtime;
                    ioData.OutCamNo = camNo;
                    ioData.OutCarNum1 = carNumber;
                    ioData.OutImage1 = imagePath.Replace('\\', '/');
                    ioData.CarType = carType;

                    ioData.TKType = (isRegistered == true ? groupInfo.GroupType : Constant.TKType_NotRegistered);
                    ioData.GroupNo = groupInfo?.GroupNo;
                    ioData.GroupName = groupInfo?.GroupName;
                    ioData.Comp = customerInfo?.Comp;
                    ioData.Dept = customerInfo?.Dept;
                    ioData.Name = customerInfo?.Name;

                    // 반환값 정리
                    outCarResult.Status = OutCarBeginResults.EStatus.NoIOData;
                    outCarResult.CarIOData = ioData;
                    if (isRegistered)
                    {
                        outCarResult.CustomerInfo = customerInfo;
                        outCarResult.GroupInfo = groupInfo;
                    }
                }
                else
                {
                    var ioData = carIODataRecords[0];
                    ioData.OutDate = localtime;
                    ioData.OutCamNo = camNo;
                    ioData.OutCarNum1 = carNumber;
                    ioData.OutImage1 = imagePath.Replace('\\', '/');

                    // 등록 여부 검색
                    bool isRegistered = false;
                    CustomerInfoRecord customerInfo = null;
                    GroupInfoRecord groupInfo = null;
                    this.parkDB.SelectRegisterInfo(this.ParkNo, ioData.GetValidCarNumber(), ref customerInfo, ref groupInfo);

                    if (customerInfo != null && groupInfo != null)
                    {
                        isRegistered = true;
                    }

                    if (ioData.TKType == null)
                    {
                        ioData.TKType = (isRegistered == true ? groupInfo.GroupType : Constant.TKType_NotRegistered);
                        ioData.GroupNo = groupInfo?.GroupNo;
                        ioData.GroupName = groupInfo?.GroupName;
                        ioData.Comp = customerInfo?.Comp;
                        ioData.Dept = customerInfo?.Dept;
                        ioData.Name = customerInfo?.Name;
                    }

                    // 반환값 정리
                    outCarResult.Status = OutCarBeginResults.EStatus.ExistIOData;
                    outCarResult.CarIOData = ioData;
                    if (customerInfo != null && groupInfo != null)
                    {
                        outCarResult.CustomerInfo = customerInfo;
                        outCarResult.GroupInfo = groupInfo;
                    }
                }
            } // end if (isRead)
            else // if isNoread
            {
                CarIODataRecord ioData = new CarIODataRecord();
                ioData.ParkNo = this.ParkNo;
                ioData.OutDate = localtime;
                ioData.OutCamNo = camNo;
                ioData.OutCarNum1 = carNumber;
                ioData.OutImage1 = imagePath.Replace('\\', '/');
                ioData.CarType = carType;

                outCarResult.Status = OutCarBeginResults.EStatus.NoIOData;
                outCarResult.CarIOData = ioData;
            }

            return outCarResult;
        }


        /// <summary>
        /// 출차 처리 완료 (입차 정보가 있는 경우)
        /// </summary>
        /// <param name="updateIoData">업데이트된 입출차 데이터. 다음 필드를 DB에 업데이트.
        ///     - OutCarBegin에서 수정한 [ TKType, GroupNo, GroupName, Comp, Dept, Name, ], OutDate, OutCamNo, OutCarNum1, OutImage1
        ///     - OutType, ParkingTime, ParkingRates</param>
        public void OutCarEnd_Update(CarIODataRecord updateIoData)
        {
            this.parkDB.UpdateCarOutData(updateIoData);

            if (Functions.IsRead(updateIoData.OutCarNum1) && 
                updateIoData.OutCarNum1 != updateIoData.InCarNum1 && updateIoData.OutCarNum1 != updateIoData.InCarNum2)
            {
                _log.Warn("** 입차/출차 인식 결과 다름. TKNo={0}, 입차전면={1}, 입차후면={2}, 출차={3}", updateIoData.TKNo, updateIoData.InCarNum1, updateIoData.InCarNum2, updateIoData.OutCarNum1);
            }
        }


        /// <summary>
        /// 출차 처리 완료 (입차 정보가 없는 경우)
        /// </summary>
        /// <param name="updateIoData">생성할 입출차 데이터.
        ///     - ParkNo, TKNo, [ TKType, GroupNo, GroupName, Comp, Dept, Name, ], InType, InDate, OutType, OutDate, OutCamNo, OutCarNum1, OutImage1, CarType, ParkingTime, ParkingRate</param>
        public void OutCarEnd_Insert(CarIODataRecord ioData)
        {
            this.parkDB.InsertCarOutData(ioData);
        }
    }
}
