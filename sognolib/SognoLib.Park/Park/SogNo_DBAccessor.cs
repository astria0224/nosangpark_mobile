﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using SognoLib.Core;
using System.Text.RegularExpressions;

namespace SognoLib.Park
{
    public class SogNo_ParkDBAccessor
    {
        /// private static NLog.Logger _log = NLog.LogManager.GetCurrentClassLogger();

        private SogNo_DBManager dbManager = null;

        public SogNo_ParkDBAccessor(string ip, int port, string db)
        {
            try
            {
                SogNo_DBInfo dbInfo = new SogNo_DBInfo();
                dbInfo.DBIP = ip;
                dbInfo.DBPort = port;
                dbInfo.DBName = db;
                this.dbManager = new SogNo_DBManager(dbInfo);
            }
            catch
            {
                this.dbManager = null;
                throw;
            }
        }
        public SogNo_ParkDBAccessor(SogNo_DBInfo dbInfo)
        {
            try
            {
                this.dbManager = new SogNo_DBManager(dbInfo);
            }
            catch
            {
                this.dbManager = null;
                throw;
            }
        }
        // **************** Park Info *********************
        public ParkInfoRecord[] SelectParkInfo(int? parkNo = null)
        {
            if (this.dbManager == null)
            {
                return null;
            }
            
            var where = new List<Tuple<string, string>>();
            if (parkNo != null)
            {
                where.Add(Tuple.Create(ParkInfoFields.ParkNo.ToString(), parkNo.ToString()));
            }

            string query = MakeNormalSelectQuery(Tables.parkinfo, where);
            return this.dbManager.GetRecords<ParkInfoRecord>(query);
        }

        public ParkInfoRecord[] SearchParkList(string SearchWord) /// 검색
		{
            if (this.dbManager == null) { return null; }

            string query = string.Format("SELECT * from `{0}` WHERE `ParkName` LIKE \'%{1}%\'", Tables.parkinfo, SearchWord);
            return this.dbManager.GetRecords<ParkInfoRecord>(query);
        }

        public void InsertParkInfo(ParkInfoRecord record) /// 주차장 정보를 DB에 INSERT
		{
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.parkinfo.ToString());
            }
        }

        public void UpdateParkInfo(ParkInfoRecord record) /// 주차장 정보를 DB에 UPDATE
		{
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    ParkInfoFields.ParkNo.ToString(),
                };
                string[] setFields = {
                    ParkInfoFields.ParkName.ToString(),
                    ParkInfoFields.ParkOwner.ToString(),
                    ParkInfoFields.ParkRegNum.ToString(),
                    ParkInfoFields.ParkPhone.ToString(),
                    ParkInfoFields.ParkAddr.ToString(),
                    ParkInfoFields.ParkNote.ToString(),
                    ParkInfoFields.ParkIPAddr.ToString(),
                    ParkInfoFields.ParkDomain.ToString(),
                    ParkInfoFields.ParkPort.ToString(),
                    ParkInfoFields.DefaultTrfNo.ToString()
                };


                this.dbManager.Update(record, Tables.parkinfo.ToString(), setFields, whereFields);
            }
        }

        public void DeleteParkInfo(ParkInfoRecord record) /// 주차장 정보를 DB에서 Delete
		{
            string[] whereFields = {
                ParkInfoFields.ParkNo.ToString()
            };

            if (this.dbManager != null)
            {
                this.dbManager.Delete(record, Tables.parkinfo.ToString(), whereFields);
            }
        }

        // **************** gategroup Info ********************* // 주차장 한 면을 의미
        public GateGroupInfoRecord[] SelectGateGroupInfo(int parkNo, int? GategroupNo = null)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            where.Add(Tuple.Create(GateGroupInfoFields.ParkNo.ToString(), parkNo.ToString()));
            if (GategroupNo != null)
            {
                where.Add(Tuple.Create(GateGroupInfoFields.GateGroupNo.ToString(), GategroupNo?.ToString()));
            }

            string query = MakeNormalSelectQuery(Tables.gategroupinfo, where);
            return this.dbManager.GetRecords<GateGroupInfoRecord>(query);
        }

        public void InsertGateGroupInfo(GateGroupInfoRecord record)/// 해당 주차장의 게이트그룹을 DB에 INSERT
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.gategroupinfo.ToString());
            }
        }
        public void UpdateGateGroupInfo(GateGroupInfoRecord record)/// 게이트 그룹 정보를 DB에 UPDATE
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    GateGroupInfoFields.ParkNo.ToString(),
                    GateGroupInfoFields.GateGroupNo.ToString()
                };
                string[] setFields = {
                    GateGroupInfoFields.GateGroupName.ToString()
                };

                this.dbManager.Update(record, Tables.gategroupinfo.ToString(), setFields, whereFields);
            }
        }

        public void DeleteGateGroupInfo(GateGroupInfoRecord record) /// 주차장 정보를 DB에서 Delete
		{
            string[] whereFields = {
                GateGroupInfoFields.ParkNo.ToString(),
                GateGroupInfoFields.GateGroupNo.ToString(),
            };

            if (this.dbManager != null)
            {
                this.dbManager.Delete(record, Tables.gategroupinfo.ToString(), whereFields);
            }
        }

        // **************** GATE Info *********************  // 주차장 들어오는 차선 하나씩을 의미
        public GateInfoRecord[] SelectGateInfo(int parkNo, int? gateGroupNo = null , int? gateNo = null)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            where.Add(Tuple.Create(GateInfoFields.ParkNo.ToString(), parkNo.ToString()));
            if (gateGroupNo != null)
            {
                where.Add(Tuple.Create(GateInfoFields.GateGroupNo.ToString(), gateGroupNo?.ToString()));
            }
            if (gateNo != null)
            {
                where.Add(Tuple.Create(GateInfoFields.GateNo.ToString(), gateNo?.ToString()));
            }

            string query = MakeNormalSelectQuery(Tables.gateinfo, where);
            return this.dbManager.GetRecords<GateInfoRecord>(query);
        }
        public void InsertGateInfo(GateInfoRecord record)/// 해당 주차장의 게이트를 DB에 INSERT
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.gateinfo.ToString());
            }
        }
        public void UpdateGateInfo(GateInfoRecord record) /// 게이트 정보를 DB에 UPDATE
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    GateInfoFields.ParkNo.ToString(),
                    GateInfoFields.GateGroupNo.ToString(),
                    GateInfoFields.GateNo.ToString()
                };
                string[] setFields = {
                    GateInfoFields.GateName.ToString(),
                    GateInfoFields.GateType.ToString(),
                    GateInfoFields.GateCarInType.ToString(),
                    GateInfoFields.GateCarOutType.ToString(),
                    GateInfoFields.Reserve0.ToString()
                };

                this.dbManager.Update(record, Tables.gateinfo.ToString(), setFields, whereFields);
            }
        }
        public void DeleteGateInfo(GateInfoRecord record) /// 주차장 정보를 DB에서 Delete
		{
            string[] whereFields = {
                GateInfoFields.ParkNo.ToString(),
                GateInfoFields.GateNo.ToString(),
            };

            if (this.dbManager != null)
            {
                this.dbManager.Delete(record, Tables.gategroupinfo.ToString(), whereFields);
            }
        }

        // **************** PC Info *********************
        public PCInfoRecord[] SelectPCInfo(int parkNo, int? gateNo = null, int? pcNo = null)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            where.Add(Tuple.Create(PCInfoFields.ParkNo.ToString(), parkNo.ToString()));
            if (gateNo != null)
            {
                where.Add(Tuple.Create(PCInfoFields.GateNo.ToString(), gateNo?.ToString()));
            }
            if (pcNo != null)
            {
                where.Add(Tuple.Create(PCInfoFields.PCNo.ToString(), pcNo?.ToString()));
            }

            string query = MakeNormalSelectQuery(Tables.pcinfo, where);
            return this.dbManager.GetRecords<PCInfoRecord>(query);
        }
        public void InsertPcInfo(PCInfoRecord record)/// 해당 주차장의 컴퓨터 정보를 DB에 INSERT
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.pcinfo.ToString());
            }
        }
        public void UpdatePcInfo(PCInfoRecord record, int? change_ParkNo = null, int? change_GateNo = null, int? change_PCNo = null)
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    PCInfoFields.ParkNo.ToString(),
                    PCInfoFields.PCNo.ToString()
                };
                string[] setFields = {
                    PCInfoFields.PCIP.ToString(),
                    PCInfoFields.PCName.ToString(),
                    PCInfoFields.PCLocation.ToString(),
                    PCInfoFields.PCInfo.ToString(),
                    PCInfoFields.PCType.ToString(),
                    PCInfoFields.PCRemoteType.ToString(),
                    PCInfoFields.PCRemoteAddr.ToString(),
                    PCInfoFields.Reserve0.ToString()
                };
                if (change_ParkNo != null || change_GateNo != null || change_PCNo != null)
                {

                }

                this.dbManager.Update(record, Tables.pcinfo.ToString(), setFields, whereFields);
            }
        }
        public void DeletePcInfo(PCInfoRecord record) /// 주차장 정보를 DB에서 Delete
		{
            string[] whereFields = {
                PCInfoFields.ParkNo.ToString(),
                PCInfoFields.PCNo.ToString(),
            };

            if (this.dbManager != null)
            {
                this.dbManager.Delete(record, Tables.pcinfo.ToString(), whereFields);
            }
        }

        // **************** Sys Info *********************
        public SysInfoRecord[] SelectSysInfo(int parkNo, int? pcNo = null, int? sysNo = null)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            where.Add(Tuple.Create(SysInfoFields.ParkNo.ToString(), parkNo.ToString()));
            if (pcNo != null)
            {
                where.Add(Tuple.Create(SysInfoFields.PCNo.ToString(), pcNo?.ToString()));
            }
            if (sysNo != null)
            {
                where.Add(Tuple.Create(SysInfoFields.SysNo.ToString(), sysNo?.ToString()));
            }

            string query = MakeNormalSelectQuery(Tables.sysinfo, where);
            return this.dbManager.GetRecords<SysInfoRecord>(query);
        }
        public SysInfoRecord[] SelectSysInfo(int parkNo, int sysNo)
        {
            if (this.dbManager == null)
            {
                return null;
            }
            var where = new List<Tuple<string, string>>();
            where.Add(Tuple.Create(SysInfoFields.ParkNo.ToString(), parkNo.ToString()));
            where.Add(Tuple.Create(SysInfoFields.ParentSysNo.ToString(), sysNo.ToString()));
            where.Add(Tuple.Create(SysInfoFields.SysType.ToString(), "0"));

            string query = MakeNormalSelectQuery(Tables.sysinfo, where);
            return this.dbManager.GetRecords<SysInfoRecord>(query);
        }
        public void InsertSysInfo(SysInfoRecord record)/// 해당 주차장의 게이트를 DB에 INSERT
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.sysinfo.ToString());
            }
        }
        public void UpdateSysInfo(SysInfoRecord record) /// 게이트 정보를 DB에 UPDATE
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    SysInfoFields.ParkNo.ToString(),
                    SysInfoFields.PCNo.ToString(),
                    SysInfoFields.SysNo.ToString()
                };
                string[] setFields = {
                    SysInfoFields.SysType.ToString(),
                    SysInfoFields.SysName.ToString(),
                    SysInfoFields.SysPort.ToString(),
                    SysInfoFields.ParentSysNo.ToString(),
                    SysInfoFields.GateNos.ToString(),
                    SysInfoFields.Reserve0.ToString()
                };

                this.dbManager.Update(record, Tables.sysinfo.ToString(), setFields, whereFields);
            }
        }
        public void DeleteSysInfo(SysInfoRecord record) /// 주차장 정보를 DB에서 Delete
		{
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    SysInfoFields.ParkNo.ToString(),
                    SysInfoFields.PCNo.ToString(),
                    SysInfoFields.SysNo.ToString()
                };

                this.dbManager.Delete(record, Tables.sysinfo.ToString(), whereFields);
            }
        }

        // **************** Bar Info ********************* // 차단기
        public BarInfoRecord[] SelectBarInfo(int? parkNo, int? GateNo = null, int? BarNo = null)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            where.Add(Tuple.Create(BarInfoFields.ParkNo.ToString(), parkNo.ToString()));
            if (GateNo != null)
            {
                where.Add(Tuple.Create(BarInfoFields.GateNo.ToString(), GateNo?.ToString()));
            }
            if (BarNo != null)
            {
                where.Add(Tuple.Create(BarInfoFields.BarNo.ToString(), BarNo?.ToString()));
            }

            string query = MakeNormalSelectQuery(Tables.barinfo, where);
            return this.dbManager.GetRecords<BarInfoRecord>(query);
        }
        public void InsertBarInfo(BarInfoRecord record)/// 해당 주차장의 차단기 정보를 DB에 INSERT
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.barinfo.ToString());
            }
        }
        public void UpdateBarInfo(BarInfoRecord record) /// 게이트 정보를 DB에 UPDATE
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    BarInfoFields.ParkNo.ToString(),
                    BarInfoFields.BarNo.ToString(),
                    BarInfoFields.GateNo.ToString()
                };
                string[] setFields = {
                    BarInfoFields.BarIP.ToString(),
                    BarInfoFields.BarPort.ToString(),
                    BarInfoFields.BarName.ToString(),
                    BarInfoFields.BarACTime.ToString(),
                    BarInfoFields.BarConnType.ToString(),
                    BarInfoFields.BarCountType.ToString(),
                    BarInfoFields.BarOpenCmd.ToString(),
                    BarInfoFields.BarCloseCmd.ToString(),
                    BarInfoFields.BarLockCmd.ToString(),
                    BarInfoFields.BarOneWay.ToString(),
                    BarInfoFields.BarAOpen.ToString(),
                    BarInfoFields.Reserve0.ToString()
                };

                this.dbManager.Update(record, Tables.barinfo.ToString(), setFields, whereFields);
            }
        }
        public void DeleteBarInfo(BarInfoRecord record) /// 주차장 정보를 DB에서 Delete
		{
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    BarInfoFields.ParkNo.ToString(),
                    BarInfoFields.BarNo.ToString(),
                    BarInfoFields.GateNo.ToString()
                };

                this.dbManager.Delete(record, Tables.barinfo.ToString(), whereFields);
            }
        }

        // **************** Cam Info ********************* // 카메라
        public CamInfoRecord[] SelectCamInfo(int? parkNo, int? GateNo = null, int? CamNo = null)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            where.Add(Tuple.Create(CamInfoFields.ParkNo.ToString(), parkNo.ToString()));
            if (GateNo != null)
            {
                where.Add(Tuple.Create(CamInfoFields.GateNo.ToString(), GateNo?.ToString()));
            }
            if (CamNo != null)
            {
                where.Add(Tuple.Create(CamInfoFields.CamNo.ToString(), CamNo?.ToString()));
            }

            string query = MakeNormalSelectQuery(Tables.caminfo, where);
            return this.dbManager.GetRecords<CamInfoRecord>(query);
        }
        public void InsertCamInfo(CamInfoRecord record)/// 해당 주차장의 카메라 정보를 DB에 INSERT
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.caminfo.ToString());
            }
        }

        public void UpdateCamInfo(CamInfoRecord record) /// 게이트 정보를 DB에 UPDATE
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    CamInfoFields.ParkNo.ToString(),
                    CamInfoFields.GateNo.ToString(),
                    CamInfoFields.CamNo.ToString()
                };
                string[] setFields = {
                    CamInfoFields.CamName.ToString(),
                    CamInfoFields.CamIP.ToString(),
                    CamInfoFields.CamType.ToString(),
                    CamInfoFields.Target.ToString(),
                    CamInfoFields.EMax.ToString(),
                    CamInfoFields.EMin.ToString(),
                    CamInfoFields.CapCount.ToString(),
                    CamInfoFields.FrameRate.ToString(),
                    CamInfoFields.RecnArea.ToString(),
                    CamInfoFields.RecnType.ToString(),
                    CamInfoFields.Reserve0.ToString(),
                    CamInfoFields.Reserve1.ToString()
                };

                this.dbManager.Update(record, Tables.caminfo.ToString(), setFields, whereFields);
            }
        }
        public void DeleteCamInfo(CamInfoRecord record) /// 주차장 정보를 DB에서 Delete
		{
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    CamInfoFields.ParkNo.ToString(),
                    CamInfoFields.CamNo.ToString(),
                    CamInfoFields.GateNo.ToString()
                };

                this.dbManager.Delete(record, Tables.caminfo.ToString(), whereFields);
            }
        }

        // **************** SCR Info ********************* // 전광판
        public ScrInfoRecord[] SelectSCRInfo(int? parkNo, int? GateNo = null, int? SCRNo = null)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            where.Add(Tuple.Create(ScrInfoFields.ParkNo.ToString(), parkNo.ToString()));
            if (GateNo != null)
            {
                where.Add(Tuple.Create(ScrInfoFields.GateNo.ToString(), GateNo?.ToString()));
            }
            if (SCRNo != null)
            {
                where.Add(Tuple.Create(ScrInfoFields.SCRNo.ToString(), SCRNo?.ToString()));
            }

            string query = MakeNormalSelectQuery(Tables.scrinfo, where);
            return this.dbManager.GetRecords<ScrInfoRecord>(query);
        }
        public void InsertSCRInfo(ScrInfoRecord record)/// 해당 주차장의 전광판 정보를 DB에 INSERT
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.scrinfo.ToString());
            }
        }
        public void UpdateSCRInfo(ScrInfoRecord record) /// 게이트 정보를 DB에 UPDATE
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    ScrInfoFields.ParkNo.ToString(),
                    ScrInfoFields.GateNo.ToString(),
                    ScrInfoFields.SCRNo.ToString()
                };
                string[] setFields = {
                    ScrInfoFields.SCRType.ToString(),
                    ScrInfoFields.SCRIP.ToString(),
                    ScrInfoFields.SCRPort.ToString(),
                    ScrInfoFields.SCRName.ToString(),
                    ScrInfoFields.SCRTime.ToString(),
                    ScrInfoFields.UpText.ToString(),
                    ScrInfoFields.UpColor.ToString(),
                    ScrInfoFields.DownText.ToString(),
                    ScrInfoFields.DownColor.ToString(),
                    ScrInfoFields.Reserve0.ToString()
                };

                this.dbManager.Update(record, Tables.scrinfo.ToString(), setFields, whereFields);
            }
        }
        public void DeleteSCRInfo(ScrInfoRecord record) /// 주차장 정보를 DB에서 Delete
		{
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    ScrInfoFields.ParkNo.ToString(),
                    ScrInfoFields.GateNo.ToString(),
                    ScrInfoFields.SCRNo.ToString()
                };

                this.dbManager.Delete(record, Tables.scrinfo.ToString(), whereFields);
            }
        }

        // **************** RPI Info ********************* // 라즈베리파이
        public RpiInfoRecord[] SelectRpiInfo(int? parkNo, int? GateNo = null, int? RpiNo = null)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            where.Add(Tuple.Create(RpiInfoFields.ParkNo.ToString(), parkNo.ToString()));
            if (GateNo != null)
            {
                where.Add(Tuple.Create(RpiInfoFields.GateNo.ToString(), GateNo?.ToString()));
            }
            if (RpiNo != null)
            {
                where.Add(Tuple.Create(RpiInfoFields.RpiNo.ToString(), RpiNo?.ToString()));
            }

            string query = MakeNormalSelectQuery(Tables.rpiinfo, where);
            return this.dbManager.GetRecords<RpiInfoRecord>(query);
        }
        public void InsertRpiInfo(RpiInfoRecord record)/// 해당 주차장의 라즈베리파이 정보를 DB에 INSERT
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.rpiinfo.ToString());
            }
        }

        public void UpdateRpiInfo(RpiInfoRecord record) /// 게이트 정보를 DB에 UPDATE
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    RpiInfoFields.ParkNo.ToString(),
                    RpiInfoFields.GateNo.ToString(),
                    RpiInfoFields.RpiNo.ToString()
                };
                string[] setFields = {
                    RpiInfoFields.RpiName.ToString(),
                    RpiInfoFields.RpiType.ToString(),
                    RpiInfoFields.RpiTypeComment.ToString(),
                    RpiInfoFields.RpiIP.ToString(),
                    RpiInfoFields.RpiPort.ToString(),
                    RpiInfoFields.Reserve0.ToString()
                };

                this.dbManager.Update(record, Tables.rpiinfo.ToString(), setFields, whereFields);
            }
        }
        public void DeleteRpiInfo(RpiInfoRecord record) /// 주차장 정보를 DB에서 Delete
		{
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    RpiInfoFields.ParkNo.ToString(),
                    RpiInfoFields.GateNo.ToString(),
                    RpiInfoFields.RpiNo.ToString()
                };

                this.dbManager.Delete(record, Tables.rpiinfo.ToString(), whereFields);
            }
        }

        //================== ETC ======================

        private string MakeNormalSelectQuery(Tables table, List<Tuple<string, string>> where)
        {
            string query = string.Format("SELECT * FROM `{0}`", table);

            if (where != null && where.Count > 0)
            {
                for (int i = 0; i < where.Count; i++)
                {
                    if (i == 0)
                    {
                        query += " WHERE ";
                    }
                    else
                    {
                        query += " AND ";
                    }

                    if (where[i].Item2 == null)
                    {
                        query += string.Format("`{0}` is null", where[i].Item1);
                    }
                    else
                    {
                        query += string.Format("`{0}`=\'{1}\'", where[i].Item1, where[i].Item2);
                    }
                }
            }

            return query;
        }




       





       

       
       



    }
}
