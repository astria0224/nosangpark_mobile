﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SognoLib.Park
{
    public enum Tables
    {
        barinfo,
        caminfo,
        cariodata,
        custamtinfo,
        custinfo,
        dcimage,
        dcinfo,
        dcticket,
        dcticketuse,
        gategroupinfo,
        gateinfo,
        groupinfo,
        holiday,
        parkinfo,
        pcinfo,
        precash,
        precashinfos,
        pretkproc,
        rpiinfo,
        rscaminfo,
        rscamrainfo,
        rscarinfo,
        rscminfo,
        rsinfo,
        scrinfo,
        subtariff,
        sysinfo,
        tariff,
        tkpcardinfo,
        tkproc,
        tkprocclosing,
        userinfo
    }


    // ~~~~~

    public enum ParkInfoFields
    {
        ParkNo,
        ParkName,
        ParkOwner,
        ParkRegNum,
        ParkPhone,
        ParkAddr,
        ParkNote,
        ParkIPAddr,
        ParkDomain,
        ParkPort,
        DefaultTrfNo,
        Reserve0
    }

    public class ParkInfoRecord
    {
        public int? ParkNo { get; set; } = null;            // 주차장 번호: auto_increment
        public string ParkName { get; set; } = null;        // 주차장 이름
        public string ParkOwner { get; set; } = null;       // 운영자
        public string ParkRegNum { get; set; } = null;      // 사업자등록번호
        public string ParkPhone { get; set; } = null;       // 전화번호
        public string ParkAddr { get; set; } = null;        // 주차장 주소
        public string ParkNote { get; set; } = null;        // 주차장 전화번호
        public string ParkIPAddr { get; set; } = null;      // 주차장 메인 IP
        public string ParkDomain { get; set; } = null;      // 주차장 도메인
        public int? ParkPort { get; set; } = null;          // 주차장 포트
        public int? DefaultTrfNo { get; set; } = null;      // 기본 요금 체계
        public string Reserve0 { get; set; } = null;
    }
    public enum GateGroupInfoFields
    {
        ParkNo,
        GateGroupNo,
        GateGroupName
    }
    public class GateGroupInfoRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? GateGroupNo { get; set; } = null;
        public string GateGroupName { get; set; } = null;        // 게이트 이름
    }

    // ~~~~~
    public enum GateInfoFields
    {
        ParkNo,
        GateGroupNo,
        GateNo,
        GateName,
        GateType,
        GateCarInType,
        GateCarOutType,
        Reserve0
    }

    public class GateInfoRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? GateGroupNo { get; set; } = null;
        public int? GateNo { get; set; } = null;
        public string GateName { get; set; } = null;        // 게이트 이름
        public int? GateType { get; set; } = null;
        public int? GateCarInType { get; set; } = null;
        public int? GateCarOutType { get; set; } = null;
        public string Reserve0 { get; set; } = null;
    }

    // ~~~~~
    public enum PCInfoFields
    {
        ParkNo,
        GateNo,
        PCNo,
        PCIP,
        PCName,
        PCLocation,
        PCInfo,
        PCType,
        PCRemoteType,
        PCRemoteAddr,
        Reserve0
    }

    public class PCInfoRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? GateNo { get; set; } = null;
        public int? PCNo { get; set; } = null;
        public string PCIP { get; set; } = null;
        public string PCName { get; set; } = null;
        public string PCLocation { get; set; } = null;
        public string PCInfo { get; set; } = null;
        public int? PCType { get; set; } = null;
        public int? PCRemoteType { get; set; } = null;
        public string PCRemoteAddr { get; set; } = null;
        public string Reserve0 { get; set; } = null;
    }

    // ~~~~~
    public enum SysInfoFields
    {
        ParkNo,
        PCNo,
        SysNo,
        SysType,
        SysName,
        SysPort,
        ParentSysNo,
        GateNos,
        Reserve0
    }

    public class SysInfoRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? PCNo { get; set; } = null;
        public int? SysNo { get; set; } = null;
        public int? SysType { get; set; } = null;
        public string SysName { get; set; } = null;
        public int? SysPort { get; set; } = null;
        public int? ParentSysNo { get; set; } = null;
        public string GateNos { get; set; } = null;
        public string Reserve0 { get; set; } = null;
    }

    // ~~~~~
    public enum BarInfoFields
    {
        ParkNo,
        BarNo,
        GateNo,
        BarIP,
        BarPort,
        BarName,
        BarACTime,
        BarConnType,
        BarCountType,
        BarOpenCmd,
        BarCloseCmd,
        BarLockCmd,
        BarOneWay,
        BarAOpen,
        Reserve0
    }
    public class BarInfoRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? BarNo { get; set; } = null;
        public int? GateNo { get; set; } = null;
        public string BarIP { get; set; } = null;
        public int? BarPort { get; set; } = null;
        public string BarName { get; set; } = null;
        public int? BarACTime { get; set; } = null;
        public int? BarConnType { get; set; } = null;
        public int? BarCountType { get; set; } = null;
        public string BarOpenCmd { get; set; } = null;
        public string BarCloseCmd { get; set; } = null;
        public string BarLockCmd { get; set; } = null;
        public int? BarOneWay { get; set; } = null;
        public int? BarAOpen { get; set; } = null;
        public string Reserve0 { get; set; } = null;

    }
    public enum CamInfoFields
    {
        ParkNo,
        GateNo,
        CamNo,
        CamName,
        CamIP,
        CamType,
        Target,
        EMax,
        EMin,
        CapCount,
        FrameRate,
        RecnArea,
        RecnType,
        Reserve0,
        Reserve1
    }
    public class CamInfoRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? GateNo { get; set; } = null;
        public int? CamNo { get; set; } = null;
        public string CamName { get; set; } = null;
        public string CamIP { get; set; } = null;
        public int? CamType { get; set; } = null;
        public int? Target { get; set; } = null;
        public int? EMax { get; set; } = null;
        public int? EMin { get; set; } = null;
        public int? CapCount { get; set; } = null;
        public string FrameRate { get; set; } = null;
        public string RecnArea { get; set; } = null;
        public int? RecnType { get; set; } = null;
        public string Reserve0 { get; set; } = null;
        public string Reserve1 { get; set; } = null;
    }

    public enum ScrInfoFields
    {
        ParkNo,
        GateNo,
        SCRNo,
        SCRType,
        SCRIP,
        SCRPort,
        SCRName,
        SCRTime,
        UpText,
        UpColor,
        DownText,
        DownColor,
        Reserve0
    }

    public class ScrInfoRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? GateNo { get; set; } = null;
        public int? SCRNo { get; set; } = null;
        public int? SCRType { get; set; } = null;
        public string SCRIP { get; set; } = null;
        public int? SCRPort { get; set; } = null;
        public string SCRName { get; set; } = null;
        public int? SCRTime { get; set; } = null;
        public string UpText { get; set; } = null;
        public int? UpColor { get; set; } = null;
        public string DownText { get; set; } = null;
        public int? DownColor { get; set; } = null;
        public string Reserve0 { get; set; } = null;
    }
    public enum RpiInfoFields
    {
        ParkNo,
        GateNo,
        RpiNo,
        RpiName,
        RpiType,
        RpiTypeComment,
        RpiIP,
        RpiPort,
        Reserve0
    }
    public class RpiInfoRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? GateNo { get; set; } = null;
        public int? RpiNo { get; set; } = null;
        public string RpiName { get; set; } = null;
        public int? RpiType { get; set; } = null;
        public string RpiTypeComment { get; set; } = null;
        public string RpiIP { get; set; } = null;
        public int? RpiPort { get; set; } = null;
        public string Reserve0 { get; set; } = null;
    }
}
