﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;


namespace SogNoLib.Park
{
    class SogNo_Tariff
    {
        private static NLog.Logger _log = NLog.LogManager.GetCurrentClassLogger();

        public TariffRecord TrfRecord { get; private set; } = null;
        public SubTariffRecord[] SubTrfRecords { get; private set; } = null;
        private HolidayRecord[] holidayRecords = null;

        private SogNo_SubTariffs weekdaySubTrfs = null;
        private SogNo_SubTariffs saturdaySubTrfs = null;
        private SogNo_SubTariffs sundaySubTrfs = null;
        private SogNo_SubTariffs holidaySubTrfs = null;

        public int? TrfNo { get { return this.TrfRecord?.TrfNo; } }

        public SogNo_Tariff()
        {
        }

        public void Init(TariffRecord trfRecord, SubTariffRecord[] subtrfRecords, HolidayRecord[] holidayRecords)
        {
            this.TrfRecord = trfRecord;
            this.SubTrfRecords = subtrfRecords;
            this.holidayRecords = holidayRecords;

            // 평일 요금 설정
            if (TrfRecord.WeekdaySetting == Constant.WeekdaySettings_Specific)
            {
                SubTariffRecord[] records = Array.FindAll(this.SubTrfRecords, item => item.SubTrfType == (int)Constant.SubTrfType_Weekday);
                if (records.Length > 0)
                {
                    this.weekdaySubTrfs = new SogNo_SubTariffs(this.TrfRecord, records);
                }
            }

            // 토요일 요금 설정
            if (TrfRecord.SaturdaySetting == Constant.SaturdaySettings_Specific)
            {
                SubTariffRecord[] records = Array.FindAll(this.SubTrfRecords, item => item.SubTrfType == (int)Constant.SubTrfType_Saturday);
                if (records.Length > 0)
                {
                    this.saturdaySubTrfs = new SogNo_SubTariffs(this.TrfRecord, records);
                }
            }
            else if (TrfRecord.SaturdaySetting == Constant.SaturdaySettings_Weekday)
            {
                this.saturdaySubTrfs = this.weekdaySubTrfs;
            }
            
            // 일요일 요금 설정
            if (TrfRecord.SundaySetting == Constant.SundaySettings_Specific)
            {
                SubTariffRecord[] records = Array.FindAll(this.SubTrfRecords, item => item.SubTrfType == (int)Constant.SubTrfType_Sunday);
                if (records.Length > 0)
                {
                    this.sundaySubTrfs = new SogNo_SubTariffs(this.TrfRecord, records);
                }
            }
            else if (TrfRecord.SundaySetting == Constant.SundaySettings_Weekday)
            {
                this.sundaySubTrfs = this.weekdaySubTrfs;
            }
            else if (TrfRecord.SundaySetting == Constant.SundaySettings_Saturday)
            {
                this.sundaySubTrfs = this.saturdaySubTrfs;
            }
            
            // 공휴일 요금 설정
            if (TrfRecord.HolidaySetting == Constant.HolidaySettings_Specific)
            {
                SubTariffRecord[] records = Array.FindAll(this.SubTrfRecords, item => item.SubTrfType == (int)Constant.SubTrfType_Holiday);
                if (records.Length > 0)
                {
                    this.holidaySubTrfs = new SogNo_SubTariffs(this.TrfRecord, records);
                }
            }
            else if (TrfRecord.HolidaySetting == Constant.HolidaySettings_Weekday)
            {
                this.holidaySubTrfs = this.weekdaySubTrfs;
            }
            else if (TrfRecord.HolidaySetting == Constant.HolidaySettings_Saturday)
            {
                this.holidaySubTrfs = this.saturdaySubTrfs;
            }
            else if (TrfRecord.HolidaySetting == Constant.HolidaySettings_Sunday)
            {
                this.holidaySubTrfs = this.sundaySubTrfs;
            }
        }

        public DateTime Toll(DateTime startDateTime, DateTime endDateTime, DCInfoRecord dcInfo, List<Tuple<string, int>> dailyFee, DateTime inCarDateTime)
        {
            void AppendParkingRates(DateTime date, int rates)
            {
                int dailyMaxRates = this.TrfRecord.DailyMaxRates ?? 0;

                if (dailyFee.Count == 0 || dailyFee.Last().Item1 != date.ToString("yyyy-MM-dd"))
                {
                    if (dailyMaxRates > 0 && rates > dailyMaxRates)
                    {
                        rates = dailyMaxRates;
                    }
                    dailyFee.Add(Tuple.Create(date.ToString("yyyy-MM-dd"), rates));
                }
                else
                {
                    rates += dailyFee.Last().Item2;
                    if (dailyMaxRates > 0 && rates > dailyMaxRates)
                    {
                        rates = dailyMaxRates;
                    }
                    dailyFee[dailyFee.Count-1] = Tuple.Create(dailyFee.Last().Item1, rates);
                }
            }

            DateTime inCarDate = inCarDateTime.Date;
            TimeSpan inCarTime = inCarDateTime.TimeOfDay;

            DateTime startDate = startDateTime.Date;
            TimeSpan startTime = startDateTime.TimeOfDay;

            // 기간 할인
            TimeSpan freeParkStart = new TimeSpan(0, 0, 0);
            TimeSpan freeParkEnd = new TimeSpan(0, 0, 0);
            if (dcInfo?.DCType == Constant.DCType_TimePeriod)
            {
                string[] tokens = dcInfo.DCValue.Split(new char[1] { '~' });
                freeParkStart = Functions.ParseTimeSpan(tokens[0]) ?? freeParkStart;
                freeParkEnd = Functions.ParseTimeSpan(tokens[1]) ?? freeParkEnd;

                _log.Debug("freeParkStart = {0}, freeParkEnd = {1}", freeParkStart, freeParkEnd);
            }


            DateTime endDate = endDateTime.Date;
            TimeSpan endTime = endDateTime.TimeOfDay;

            TimeSpan dayStartTime = new TimeSpan(0, 0, 0);
            TimeSpan dayEndTime = new TimeSpan(24, 0, 0);


            TimeSpan start = startTime;
            for (DateTime date = startDate; date <= endDate; date = date.AddDays(1))
            {
                TimeSpan end = (date == endDate ? endTime : dayEndTime);
                bool isBegin = (date == inCarDate && start == inCarTime);

                start = (start >= dayEndTime ? start - dayEndTime : start);

                if (this.TrfRecord.DailyCriterion == Constant.DailyCriterion_Day)
                {
                    Tuple<int, TimeSpan> tollResult = Toll_OneDay(date, start, end, freeParkStart, freeParkEnd, isBegin);
                    Debug.Assert(tollResult.Item2 >= end);
                    AppendParkingRates(date, tollResult.Item1);

                    start = tollResult.Item2;
                }
                else
                {
                    if (start >= inCarTime)
                    {
                        Tuple<int, TimeSpan> tollResult = Toll_OneDay(date, start, end, freeParkStart, freeParkEnd, isBegin);
                        Debug.Assert(tollResult.Item2 >= end);
                        AppendParkingRates(date, tollResult.Item1);

                        start = tollResult.Item2;
                    }
                    else if (end <= inCarTime)
                    {
                        Tuple<int, TimeSpan> tollResult = Toll_OneDay(date, start, end, freeParkStart, freeParkEnd, isBegin);
                        Debug.Assert(tollResult.Item2 >= end);
                        AppendParkingRates(date.AddDays(-1), tollResult.Item1);

                        start = tollResult.Item2;
                    }
                    else
                    {
                        Tuple<int, TimeSpan> tollResult = Toll_OneDay(date, start, inCarTime, freeParkStart, freeParkEnd, isBegin);
                        Debug.Assert(tollResult.Item2 >= inCarTime);
                        AppendParkingRates(date.AddDays(-1), tollResult.Item1);
                        start = tollResult.Item2;

                        tollResult = Toll_OneDay(date, start, end, freeParkStart, freeParkEnd, isBegin);
                        Debug.Assert(tollResult.Item2 >= end);
                        AppendParkingRates(date, tollResult.Item1);
                        start = tollResult.Item2;
                    }
                }
            }

            return endDate + start;
        }


        private Tuple<int, TimeSpan> Toll_OneDay(DateTime date, TimeSpan startTime, TimeSpan endTime, TimeSpan freeStartTime, TimeSpan freeEndTime, bool isBegin)
        {
            // 무료 시간 없음
            if (freeStartTime == freeEndTime)
            {
                return Toll_OneDay(date, startTime, endTime, isBegin);
            }

            // 무료 시간과 겹치지 않음
            if ((freeStartTime < startTime && freeEndTime <= startTime) ||
                (freeStartTime >= endTime && freeEndTime > endTime) ||
                (freeStartTime >= endTime && freeEndTime <= startTime))
            {
                return Toll_OneDay(date, startTime, endTime, isBegin);
            }


            // 무료 시간과 겹침
            List<Tuple<TimeSpan, TimeSpan>> timePeriods = new List<Tuple<TimeSpan, TimeSpan>>();
            if (freeStartTime < freeEndTime)
            {
                if (freeStartTime <= startTime && freeEndTime < endTime)
                {
                    timePeriods.Add(Tuple.Create(freeEndTime, endTime));
                }
                else if (freeStartTime > startTime && freeEndTime >= endTime)
                {
                    timePeriods.Add(Tuple.Create(startTime, freeStartTime));
                }
                else if (freeStartTime > startTime && freeEndTime < endTime)
                {
                    timePeriods.Add(Tuple.Create(startTime, freeStartTime));
                    timePeriods.Add(Tuple.Create(freeEndTime, endTime));
                }
                else
                {
                    // 완전히 겹침
                }
            }
            else
            {
                if (freeStartTime <= startTime || freeEndTime >= endTime)
                {
                    // 완전히 겹침
                }
                else if (freeEndTime <= startTime)
                {
                    timePeriods.Add(Tuple.Create(startTime, freeStartTime));
                }
                else if (freeStartTime >= endTime)
                {
                    timePeriods.Add(Tuple.Create(freeEndTime, endTime));
                }
                else
                {
                    timePeriods.Add(Tuple.Create(freeEndTime, freeStartTime));
                }
            }

            if (timePeriods.Count == 1)
            {
                return Toll_OneDay(date, timePeriods[0].Item1, timePeriods[0].Item2, isBegin);
            }
            else if (timePeriods.Count == 2)
            {
                int parkingRates = 0;

                Tuple<int, TimeSpan> tollResult = Toll_OneDay(date, timePeriods[0].Item1, timePeriods[0].Item2, isBegin);
                parkingRates += tollResult.Item1;

                TimeSpan start = timePeriods[1].Item1;
                if (start < tollResult.Item2)
                {
                    start = tollResult.Item2;
                }

                tollResult = Toll_OneDay(date, start, timePeriods[1].Item2, false);
                parkingRates += tollResult.Item1;

                return Tuple.Create(parkingRates, tollResult.Item2);
            }
            else
            {
                return Tuple.Create(0, endTime);
            }
        }

        private Tuple<int, TimeSpan> Toll_OneDay(DateTime date, TimeSpan startTime, TimeSpan endTime, bool isBegin)
        {
            bool isHoliday = IsHoliday(date);
            SogNo_SubTariffs subTariffs = GetSubTariffs(date);

            Tuple<int, TimeSpan> tollResult = subTariffs?.Toll_OneDay(startTime, endTime, isBegin);
            if (tollResult == null)
            {
                tollResult = Tuple.Create(0, endTime);
            }
           
            _log.Debug("{0}, {1}, {2}~{3} : {4} KRW", date.ToString("yyyy-MM-dd"), date.DayOfWeek, 
                string.Format("{0}:{1:mm}:{1:ss}", (int)startTime.TotalHours, startTime), string.Format("{0}:{1:mm}:{1:ss}", (int)endTime.TotalHours, endTime), tollResult.Item1);

            return tollResult;
        }

        public SubTariffRecord GetSubTariff(DateTime datetime)
        {
            return GetSubTariffs(datetime.Date)?.FindSubTariff(datetime.TimeOfDay);
        }

        public SogNo_SubTariffs GetSubTariffs(DateTime date)
        {
            bool isHoliday = IsHoliday(date);

            SogNo_SubTariffs subtrfs = null;

            if (date.DayOfWeek == DayOfWeek.Sunday)
            {
                if (isHoliday && this.TrfRecord.SundayPrior == Constant.SundayPrior_Holiday)
                {
                    subtrfs = this.holidaySubTrfs;
                }
                else
                {
                    subtrfs = this.sundaySubTrfs;
                }
            }
            else if (date.DayOfWeek == DayOfWeek.Saturday)
            {
                if (isHoliday && this.TrfRecord.SaturdayPrior == Constant.SaturdayPrior_Holiday)
                {
                    subtrfs = this.holidaySubTrfs;
                }
                else
                {
                    subtrfs = this.saturdaySubTrfs;
                }
            }
            else // weekday
            {
                if (isHoliday)
                {
                    subtrfs = this.holidaySubTrfs;
                }
                else
                {
                    subtrfs = this.weekdaySubTrfs;
                }
            }

            return subtrfs;
        }

        private bool IsHoliday(DateTime date)
        {
            if (this.holidayRecords != null && this.holidayRecords.Length > 0)
            {
                int idx = Array.FindIndex(this.holidayRecords, item => (item.Type.Value == Constant.HolidayType_Holiday && item.Date.Value.Date == date.Date));
                if (idx >= 0)
                {
                    return true;
                }
            }

            return false;
        }
    }

    class SogNo_SubTariffs
    {
        private static NLog.Logger _log = NLog.LogManager.GetCurrentClassLogger();

        private TariffRecord trfRecord = null;
        private SubTariffRecord[] subtrfRecords = null;

        public SogNo_SubTariffs(TariffRecord trfRecord, SubTariffRecord[] subtrfRecords)
        {
            this.trfRecord = trfRecord;
            this.subtrfRecords = subtrfRecords;

            if (this.subtrfRecords.Length > 0)
            {
                SortSubTariff();
            }
        }

        
        /// <summary>
        /// 하루 startTime ~ endTime 구간의 요금 계산
        /// </summary>
        /// <param name="startTime">요금 계산 시작 시간</param>
        /// <param name="endTime">요금 계산 종료 시간</param>
        /// <param name="isBegin">주차 시작일인지 여부로 기본 시간 요금 적용 여부 판단에 사용.</param>
        /// <returns>
        /// item1 : 주차 요금
        /// item2 : 주차 요금이 계산된 최종 시간
        /// </returns>
        public Tuple<int, TimeSpan> Toll_OneDay(TimeSpan startTime, TimeSpan endTime, bool isBegin)
        {
            int parkingRates = 0;

            SubTariffRecord subtrf = FindSubTariff(startTime);
            if (subtrf == null)
            {
                subtrf = FindNextSubTariff(startTime);
                if (subtrf != null)
                {
                    startTime = subtrf.StartTime.Value;
                }
            }

            if (subtrf == null || startTime >= endTime)
            {   // subtrf 없음
                return Tuple.Create(0, endTime);
            }

            // 기본 시간 요금
            if (isBegin)
            {
                //_log.Debug("  + {0} : {1} + {2}", subtrf.BaseRates, startTime, subtrf.BaseTime);
                parkingRates += subtrf.BaseRates.Value;
                startTime = startTime.Add(TimeSpan.FromMinutes(subtrf.BaseTime.Value));
            }

            // 추가 시간당 요금
            while (startTime < endTime)
            {
                if (startTime >= subtrf.EndTime)
                {
                    subtrf = FindSubTariff(startTime);
                    if (subtrf == null)
                    {
                        subtrf = FindNextSubTariff(startTime);
                        if (subtrf != null)
                        {
                            startTime = subtrf.StartTime.Value;
                        }
                    }

                    if (subtrf == null)
                    {
                        startTime = endTime;
                        break;
                    }
                }

                //_log.Debug("  + {0} : {1} + {2}", subtrf.AddRates, startTime, subtrf.AddTime);
                parkingRates += subtrf.AddRates.Value;
                startTime = startTime.Add(TimeSpan.FromMinutes(subtrf.AddTime.Value));
            }

            //_log.Debug(" = {0} : {1}", parkingRates, startTime);

            return Tuple.Create(parkingRates, startTime);
        }


        /// <summary>
        /// subtrfRecords 정렬. StartTime으로 오름차순 정렬
        /// </summary>
        private void SortSubTariff()
        {
            Array.Sort(this.subtrfRecords, (x, y) => x.StartTime.Value.CompareTo(y.StartTime));
        }


        /// <summary>
        /// time을 포함하는 subtrf 레코드를 찾음
        /// </summary>
        /// <param name="time">subtrf 레코드를 찾을 기준 시각. 0:00 ~ 24:00 사이의 값</param>
        /// <returns>time을 포함하는 subtrf 레코드. 없을 시 null 반환.</returns>
        public SubTariffRecord FindSubTariff(TimeSpan time)
        {
            int subtrfIndex = Array.FindIndex(this.subtrfRecords, item => (item.StartTime <= time && time < item.EndTime));
            if (subtrfIndex < 0)
            {
                return null;
            }

            return this.subtrfRecords[subtrfIndex];
        }


        /// <summary>
        /// time 포함하지 않는 다음 구간의 subtrf 레코드를 찾음
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        private SubTariffRecord FindNextSubTariff(TimeSpan time)
        {
            int subtrfIndex = Array.FindIndex(this.subtrfRecords, item => item.StartTime >= time);
            if (subtrfIndex < 0)
            {
                return null;
            }

            return this.subtrfRecords[subtrfIndex];
        }
    }
}
