﻿namespace SognoLib.Park.GUI
{
    partial class ParkAddform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.parkDefaultTrfNoTxtBox = new System.Windows.Forms.TextBox();
            this.parkPortTxtBox = new System.Windows.Forms.TextBox();
            this.parkDomainTxtBox = new System.Windows.Forms.TextBox();
            this.parkIPAddrTxtBox = new System.Windows.Forms.TextBox();
            this.parkNoteTxtBox = new System.Windows.Forms.TextBox();
            this.parkAddrTxtBox = new System.Windows.Forms.TextBox();
            this.parkPhoneTxtBox = new System.Windows.Forms.TextBox();
            this.parkRegNumTxtBox = new System.Windows.Forms.TextBox();
            this.parkOwnerTxtBox = new System.Windows.Forms.TextBox();
            this.parkNameTxtBox = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.parkNoTxtBox = new System.Windows.Forms.TextBox();
            this.addButton = new System.Windows.Forms.Button();
            this.exitbutton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.46789F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.53211F));
            this.tableLayoutPanel1.Controls.Add(this.parkDefaultTrfNoTxtBox, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.parkPortTxtBox, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.parkDomainTxtBox, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.parkIPAddrTxtBox, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.parkNoteTxtBox, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.parkAddrTxtBox, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.parkPhoneTxtBox, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.parkRegNumTxtBox, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.parkOwnerTxtBox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.parkNameTxtBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label17, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label15, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label13, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label18, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.label16, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.parkNoTxtBox, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 11;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.333333F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(524, 549);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // parkDefaultTrfNoTxtBox
            // 
            this.parkDefaultTrfNoTxtBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.parkDefaultTrfNoTxtBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.parkDefaultTrfNoTxtBox.Location = new System.Drawing.Point(194, 501);
            this.parkDefaultTrfNoTxtBox.Name = "parkDefaultTrfNoTxtBox";
            this.parkDefaultTrfNoTxtBox.Size = new System.Drawing.Size(327, 36);
            this.parkDefaultTrfNoTxtBox.TabIndex = 28;
            // 
            // parkPortTxtBox
            // 
            this.parkPortTxtBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.parkPortTxtBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.parkPortTxtBox.Location = new System.Drawing.Point(194, 447);
            this.parkPortTxtBox.Name = "parkPortTxtBox";
            this.parkPortTxtBox.Size = new System.Drawing.Size(327, 36);
            this.parkPortTxtBox.TabIndex = 27;
            // 
            // parkDomainTxtBox
            // 
            this.parkDomainTxtBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.parkDomainTxtBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.parkDomainTxtBox.Location = new System.Drawing.Point(194, 398);
            this.parkDomainTxtBox.Name = "parkDomainTxtBox";
            this.parkDomainTxtBox.Size = new System.Drawing.Size(327, 36);
            this.parkDomainTxtBox.TabIndex = 26;
            // 
            // parkIPAddrTxtBox
            // 
            this.parkIPAddrTxtBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.parkIPAddrTxtBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.parkIPAddrTxtBox.Location = new System.Drawing.Point(194, 349);
            this.parkIPAddrTxtBox.Name = "parkIPAddrTxtBox";
            this.parkIPAddrTxtBox.Size = new System.Drawing.Size(327, 36);
            this.parkIPAddrTxtBox.TabIndex = 25;
            // 
            // parkNoteTxtBox
            // 
            this.parkNoteTxtBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.parkNoteTxtBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.parkNoteTxtBox.Location = new System.Drawing.Point(194, 300);
            this.parkNoteTxtBox.Name = "parkNoteTxtBox";
            this.parkNoteTxtBox.Size = new System.Drawing.Size(327, 36);
            this.parkNoteTxtBox.TabIndex = 24;
            // 
            // parkAddrTxtBox
            // 
            this.parkAddrTxtBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.parkAddrTxtBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.parkAddrTxtBox.Location = new System.Drawing.Point(194, 251);
            this.parkAddrTxtBox.Name = "parkAddrTxtBox";
            this.parkAddrTxtBox.Size = new System.Drawing.Size(327, 36);
            this.parkAddrTxtBox.TabIndex = 23;
            // 
            // parkPhoneTxtBox
            // 
            this.parkPhoneTxtBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.parkPhoneTxtBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.parkPhoneTxtBox.Location = new System.Drawing.Point(194, 202);
            this.parkPhoneTxtBox.Name = "parkPhoneTxtBox";
            this.parkPhoneTxtBox.Size = new System.Drawing.Size(327, 36);
            this.parkPhoneTxtBox.TabIndex = 22;
            // 
            // parkRegNumTxtBox
            // 
            this.parkRegNumTxtBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.parkRegNumTxtBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.parkRegNumTxtBox.Location = new System.Drawing.Point(194, 153);
            this.parkRegNumTxtBox.Name = "parkRegNumTxtBox";
            this.parkRegNumTxtBox.Size = new System.Drawing.Size(327, 36);
            this.parkRegNumTxtBox.TabIndex = 21;
            // 
            // parkOwnerTxtBox
            // 
            this.parkOwnerTxtBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.parkOwnerTxtBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.parkOwnerTxtBox.Location = new System.Drawing.Point(194, 104);
            this.parkOwnerTxtBox.Name = "parkOwnerTxtBox";
            this.parkOwnerTxtBox.Size = new System.Drawing.Size(327, 36);
            this.parkOwnerTxtBox.TabIndex = 20;
            // 
            // parkNameTxtBox
            // 
            this.parkNameTxtBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.parkNameTxtBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.parkNameTxtBox.Location = new System.Drawing.Point(194, 55);
            this.parkNameTxtBox.Name = "parkNameTxtBox";
            this.parkNameTxtBox.Size = new System.Drawing.Size(327, 36);
            this.parkNameTxtBox.TabIndex = 19;
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label17.Location = new System.Drawing.Point(30, 406);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(130, 21);
            this.label17.TabIndex = 16;
            this.label17.Text = "주차장 도메인";
            // 
            // label15
            // 
            this.label15.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label15.Location = new System.Drawing.Point(28, 357);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(134, 21);
            this.label15.TabIndex = 14;
            this.label15.Text = "주차장 메인 IP";
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label13.Location = new System.Drawing.Point(21, 308);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(149, 21);
            this.label13.TabIndex = 12;
            this.label13.Text = "주차장 전화번호";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label10.Location = new System.Drawing.Point(52, 210);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(86, 21);
            this.label10.TabIndex = 9;
            this.label10.Text = "전화번호";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label8.Location = new System.Drawing.Point(24, 161);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(143, 21);
            this.label8.TabIndex = 7;
            this.label8.Text = "사업자등록번호";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(40, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "주차장 번호";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label2.Location = new System.Drawing.Point(52, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "주차장명";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label3.Location = new System.Drawing.Point(62, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 21);
            this.label3.TabIndex = 2;
            this.label3.Text = "운영자";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label4.Location = new System.Drawing.Point(40, 259);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 21);
            this.label4.TabIndex = 3;
            this.label4.Text = "주차장 주소";
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label18.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label18.Location = new System.Drawing.Point(40, 455);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(111, 21);
            this.label18.TabIndex = 17;
            this.label18.Text = "주차장 포트";
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label16.Location = new System.Drawing.Point(33, 509);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(124, 21);
            this.label16.TabIndex = 15;
            this.label16.Text = "기본요금체계";
            // 
            // parkNoTxtBox
            // 
            this.parkNoTxtBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.parkNoTxtBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.parkNoTxtBox.Location = new System.Drawing.Point(194, 6);
            this.parkNoTxtBox.Name = "parkNoTxtBox";
            this.parkNoTxtBox.Size = new System.Drawing.Size(327, 36);
            this.parkNoTxtBox.TabIndex = 18;
            // 
            // addButton
            // 
            this.addButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.addButton.FlatAppearance.BorderSize = 0;
            this.addButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addButton.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.addButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.addButton.Location = new System.Drawing.Point(12, 579);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(202, 60);
            this.addButton.TabIndex = 1;
            this.addButton.Text = "추가";
            this.addButton.UseVisualStyleBackColor = false;
            this.addButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // exitbutton
            // 
            this.exitbutton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(37)))), ((int)(((byte)(122)))));
            this.exitbutton.FlatAppearance.BorderSize = 0;
            this.exitbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exitbutton.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.exitbutton.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.exitbutton.Location = new System.Drawing.Point(334, 579);
            this.exitbutton.Name = "exitbutton";
            this.exitbutton.Size = new System.Drawing.Size(202, 60);
            this.exitbutton.TabIndex = 2;
            this.exitbutton.Text = "취소";
            this.exitbutton.UseVisualStyleBackColor = false;
            this.exitbutton.Click += new System.EventHandler(this.Exitbutton_Click);
            // 
            // ParkAddform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.ClientSize = new System.Drawing.Size(548, 651);
            this.Controls.Add(this.exitbutton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ParkAddform";
            this.Text = "주차장 추가";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox parkDefaultTrfNoTxtBox;
        private System.Windows.Forms.TextBox parkPortTxtBox;
        private System.Windows.Forms.TextBox parkDomainTxtBox;
        private System.Windows.Forms.TextBox parkIPAddrTxtBox;
        private System.Windows.Forms.TextBox parkNoteTxtBox;
        private System.Windows.Forms.TextBox parkAddrTxtBox;
        private System.Windows.Forms.TextBox parkPhoneTxtBox;
        private System.Windows.Forms.TextBox parkRegNumTxtBox;
        private System.Windows.Forms.TextBox parkOwnerTxtBox;
        private System.Windows.Forms.TextBox parkNameTxtBox;
        private System.Windows.Forms.TextBox parkNoTxtBox;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button exitbutton;
    }
}