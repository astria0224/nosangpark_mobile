﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SognoLib.Park.GUI
{
    public partial class SognoBarView : Form
    {
        private int? selectedParkNo;// 선택된 주차장 번호
        private int? selectedGateNo; // 선택된 게이트 번호
        private int? selectedBarNo; // 선택된 전광판 번호

        private BarInfoRecord selectedBarInfoRecord = null;

        private ParkInfoRecord[] parkList; // 전체 주차장 리스트 데이터
        private GateInfoRecord[] gateInfoRecords = null; // 전체 게이트 데이터
        private BarInfoRecord[] barInfoRecords = null; // 전체 차단기 데이터

        private SogNo_ParkDBAccessor DBAcessor = null; // DB 액세서

        public SognoBarView(
            SogNo_ParkDBAccessor DBAcessor,
            int? selectedParkNo,
            ParkInfoRecord[] parkList,
            int? selectedGateNo = null,
            int? selectedBarNo = null,
            int? reQuest = null
            )
        {
            InitializeComponent();
            this.selectedParkNo = selectedParkNo;
            this.DBAcessor = DBAcessor;

            //주차장 콤보박스에 주차장명 전체 삽입
            this.parkList = parkList;
            this.ParkComboBox.DataSource = this.parkList;
            this.ParkComboBox.DisplayMember = ParkInfoFields.ParkName.ToString();
            this.ParkComboBox.ValueMember = ParkInfoFields.ParkNo.ToString();
            this.ParkComboBox.SelectedValue = this.selectedParkNo;

            //게이트 콤보박스에 주차장 전체의 게이트 데이터 삽입
            this.gateInfoRecords = this.DBAcessor.SelectGateInfo(this.selectedParkNo.GetValueOrDefault());
            this.GateComboBox.DataSource = this.gateInfoRecords;
            this.GateComboBox.DisplayMember = GateInfoFields.GateName.ToString();
            this.GateComboBox.ValueMember = GateInfoFields.GateNo.ToString();
            this.GateComboBox.SelectedIndex = -1;

            if (selectedGateNo != null) this.selectedGateNo = selectedGateNo;
            

            Init_Barlistview();
            RefreshBarList();

            if (selectedBarNo != null)
            {
                this.selectedBarNo = selectedBarNo;
                this.selectedBarInfoRecord = DBAcessor.SelectBarInfo(selectedParkNo.GetValueOrDefault(), this.selectedGateNo, this.selectedBarNo)[0];
                fill_textboxes(selectedBarInfoRecord);
            }
            if (reQuest == 1)
            {
                if (this.selectedBarInfoRecord != null)
                {
                    DialogResult dialog = MessageBox.Show("정말 삭제하시겠습니까?", "삭제확인메시지", MessageBoxButtons.YesNo);
                    if (dialog == DialogResult.Yes)
                    {
                        this.DBAcessor.DeleteBarInfo(selectedBarInfoRecord);
                        MessageBox.Show("삭제를 진행하였습니다.");
                    }
                    else if (dialog == DialogResult.No)
                    {
                        MessageBox.Show("삭제를 취소하였습니다.");
                    }
                }
                RefreshBarList();
            }



            }
        private void Init_Barlistview()
        {
            //PCListView.BeginUpdate();   이거 사이에 쓰면 업데이트 다 되고 나서 로드됨 (오류방지용)
            //PCListView.EndUpdate();

            BarListView.View = View.Details;
            BarListView.GridLines = false;
            BarListView.FullRowSelect = true; // 선택 시 라인 전체 선택
        }
        private void RefreshBarList()
        {
            BarListView.Clear(); // 전체 지우기

            BarListView.Columns.Add("Index");
            BarListView.Columns.Add("GateNo");
            BarListView.Columns.Add("BarNo");
            BarListView.Columns.Add("BarName");

            for (int i = 0; i < BarListView.Columns.Count; i++)
            {
                BarListView.Columns[i].Width = -2;
            }

            if (selectedGateNo != null)
            {
                barInfoRecords = DBAcessor.SelectBarInfo(selectedParkNo.GetValueOrDefault(), this.selectedGateNo); // 레코드에 데이터 넣기
            }
            else
            {
                barInfoRecords = DBAcessor.SelectBarInfo(selectedParkNo.GetValueOrDefault()); // 레코드에 데이터 넣기
            }

            // 폼에 데이터 뿌리기
            if (barInfoRecords.Length > 0)
            {
                for (int i = 0; barInfoRecords.Length > i; i++)
                {
                    ListViewItem a = new ListViewItem(i.ToString());
                    a.SubItems.Add(NullCheck(barInfoRecords[i].GateNo).ToString());
                    a.SubItems.Add(NullCheck(barInfoRecords[i].BarNo).ToString());
                    a.SubItems.Add(NullCheck(barInfoRecords[i].BarName).ToString());
                    if (i % 2 == 0) a.BackColor = Color.FromArgb(70, 70, 70);
                    BarListView.Items.Add(a);
                }
            }
        }
        private object NullCheck(object nullitem)
        {
            if (nullitem == null)
            {
                return " ";
            }
            else
            {
                return nullitem;
            }
        }




        private void AddButton_Click(object sender, EventArgs e)
        {
            insert_BarInfo();
        }
        private void insert_BarInfo()
        {
            try
            {
                BarInfoRecord insert_BarRecord = new BarInfoRecord
                {
                    ParkNo = selectedParkNo,
                    GateNo = 0,
                    BarNo = 0,
                    BarIP = "172.16.10.100",
                    BarPort = 1001,
                    BarName = "기본 차단기",
                    BarACTime = 0,
                    BarConnType = 0,
                    BarCountType = 0,
                    BarOpenCmd = "BarOpen",
                    BarCloseCmd = "BarClose",
                    BarLockCmd = "BarLock",
                    BarOneWay = 0,
                    BarAOpen = 0,
                    Reserve0 = null,
                };

                if (ParkComboBox.Items.Count > 0) insert_BarRecord.ParkNo = int.Parse(ParkComboBox.SelectedValue.ToString());
                if (GateComboBox.Text != "") insert_BarRecord.GateNo = int.Parse(GateComboBox.SelectedValue.ToString());
                if (BarNoTextBox.Text != "") insert_BarRecord.BarNo = int.Parse(BarNoTextBox.Text.ToString());
                if (BarIPTextBox.Text != "") insert_BarRecord.BarIP = BarIPTextBox.Text.ToString();
                if (BarPortTextBox.Text != "") insert_BarRecord.BarPort = int.Parse(BarPortTextBox.Text.ToString());
                if (BarNameTextBox.Text != "") insert_BarRecord.BarName = BarNameTextBox.Text.ToString();
                if (BarACTimeTextBox.Text != "") insert_BarRecord.BarACTime = int.Parse(BarACTimeTextBox.Text.ToString());
                if (BarConnTypeTextBox.Text != "") insert_BarRecord.BarConnType = int.Parse(BarConnTypeTextBox.Text.ToString());
                if (BarCountTypeTextBox.Text != "") insert_BarRecord.BarCountType = int.Parse(BarCountTypeTextBox.Text.ToString());
                if (BarOpenCmdTextBox.Text != "") insert_BarRecord.BarOpenCmd = BarOpenCmdTextBox.Text.ToString();
                if (BarCloseCmdTextBox.Text != "") insert_BarRecord.BarCloseCmd = BarCloseCmdTextBox.Text.ToString();
                if (BarLockCmdTextBox.Text != "") insert_BarRecord.BarLockCmd = BarLockCmdTextBox.Text.ToString();
                if (BarOneWayTextBox.Text != "") insert_BarRecord.BarOneWay = int.Parse(BarOneWayTextBox.Text.ToString());
                if (BarAOpenTextBox.Text != "") insert_BarRecord.BarAOpen = int.Parse(BarAOpenTextBox.Text.ToString());
                //if (Reserve0TextBox.Text != "") insert_BarRecord.Reserve0 = Reserve0TextBox.Text.ToString();

                this.DBAcessor.InsertBarInfo(insert_BarRecord);
                MessageBox.Show("추가를 진행하였습니다.");
                RefreshBarList();

            }
            catch (Exception ex)
            {
                MessageBox.Show("추가에 실패하였습니다.");
                MessageBox.Show(ex.ToString());
            }
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            try
            {
                BarInfoRecord update_BarRecord = new BarInfoRecord();

                if (ParkComboBox.Items.Count > 0) update_BarRecord.ParkNo = int.Parse(ParkComboBox.SelectedValue.ToString());
                if (GateComboBox.Text != "") update_BarRecord.GateNo = int.Parse(GateComboBox.SelectedValue.ToString());
                if (BarNoTextBox.Text != "") update_BarRecord.BarNo = int.Parse(BarNoTextBox.Text.ToString());
                if (BarIPTextBox.Text != "") update_BarRecord.BarIP = BarIPTextBox.Text.ToString();
                if (BarPortTextBox.Text != "") update_BarRecord.BarPort = int.Parse(BarPortTextBox.Text.ToString());
                if (BarNameTextBox.Text != "") update_BarRecord.BarName = BarNameTextBox.Text.ToString();
                if (BarACTimeTextBox.Text != "") update_BarRecord.BarACTime = int.Parse(BarACTimeTextBox.Text.ToString());
                if (BarConnTypeTextBox.Text != "") update_BarRecord.BarConnType = int.Parse(BarConnTypeTextBox.Text.ToString());
                if (BarCountTypeTextBox.Text != "") update_BarRecord.BarCountType = int.Parse(BarCountTypeTextBox.Text.ToString());
                if (BarOpenCmdTextBox.Text != "") update_BarRecord.BarOpenCmd = BarOpenCmdTextBox.Text.ToString();
                if (BarCloseCmdTextBox.Text != "") update_BarRecord.BarCloseCmd = BarCloseCmdTextBox.Text.ToString();
                if (BarLockCmdTextBox.Text != "") update_BarRecord.BarLockCmd = BarLockCmdTextBox.Text.ToString();
                if (BarOneWayTextBox.Text != "") update_BarRecord.BarOneWay = int.Parse(BarOneWayTextBox.Text.ToString());
                if (BarAOpenTextBox.Text != "") update_BarRecord.BarAOpen = int.Parse(BarAOpenTextBox.Text.ToString());
                //if (Reserve0TextBox.Text != "") update_BarRecord.Reserve0 = Reserve0TextBox.Text.ToString();

                this.DBAcessor.UpdateBarInfo(update_BarRecord);
                MessageBox.Show("수정을 진행하였습니다.");
                RefreshBarList();
            }
            catch (Exception ex)
            {
                MessageBox.Show("수정에 실패하였습니다.");
                MessageBox.Show(ex.ToString());
            }
        }



        private void DeleteButton_Click(object sender, EventArgs e)
        {
            if (this.selectedBarInfoRecord != null)
            {
                DialogResult dialog = MessageBox.Show("정말 삭제하시겠습니까?", "삭제확인메시지", MessageBoxButtons.YesNo);
                if (dialog == DialogResult.Yes)
                {
                    this.DBAcessor.DeleteBarInfo(selectedBarInfoRecord);
                    MessageBox.Show("삭제를 진행하였습니다.");
                }
                else if (dialog == DialogResult.No)
                {
                    MessageBox.Show("삭제를 취소하였습니다.");
                }
            }
            RefreshBarList();
        }

        private void BarListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (BarListView.SelectedItems.Count > 0)
            {
                int index = int.Parse(BarListView.SelectedItems[0].Text);
                this.selectedBarInfoRecord = this.barInfoRecords[index];
                fill_textboxes(this.barInfoRecords[index]);
            }
        }

        public void fill_textboxes(BarInfoRecord record) //텍스트 박스를 채우는 함수
        {
            this.ParkComboBox.SelectedValue = record.ParkNo;
            this.GateComboBox.SelectedValue = record.GateNo;
            if (record.BarNo != null) this.BarNoTextBox.Text = record.BarNo.ToString();
            if (record.BarPort != null) this.BarPortTextBox.Text = record.BarPort.ToString();
            if (record.BarName != null) this.BarNameTextBox.Text = record.BarName.ToString();
            if (record.BarACTime != null) this.BarACTimeTextBox.Text = record.BarACTime.ToString();
            if (record.BarConnType != null) this.BarConnTypeTextBox.Text = record.BarConnType.ToString();
            if (record.BarCountType != null) this.BarCountTypeTextBox.Text = record.BarCountType.ToString();
            if (record.BarOpenCmd != null) this.BarOpenCmdTextBox.Text = record.BarOpenCmd.ToString();
            if (record.BarCloseCmd != null) this.BarCloseCmdTextBox.Text = record.BarCloseCmd.ToString();
            if (record.BarLockCmd != null) this.BarLockCmdTextBox.Text = record.BarLockCmd.ToString();
            if (record.BarOneWay != null) this.BarOneWayTextBox.Text = record.BarOneWay.ToString();
            if (record.BarAOpen != null) this.BarAOpenTextBox.Text = record.BarAOpen.ToString();
            //if (record.Reserve0 != null) this.Reserve0TextBox.Text = record.Reserve0.ToString();
        }
    }
}
