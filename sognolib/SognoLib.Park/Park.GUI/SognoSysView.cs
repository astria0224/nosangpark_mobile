﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SognoLib.Park.GUI
{
    public partial class SognoSysView : Form
    {

        private int? selectedParkNo;// 선택된 주차장 번호
        private int? selectedPCNo; // 선택된 PC 번호
        private int? selectedSysNo; // 선택된 시스템 번호

        private SysInfoRecord selectedSysInfoRecord = null;

        private ParkInfoRecord[] parkList; // 전체 주차장 리스트 데이터
        private PCInfoRecord[] pcInfoRecords = null; // 전체 PC 데이터
        private SysInfoRecord[] sysInfoRecords = null; // 전체 전광판 데이터

        private SogNo_ParkDBAccessor DBAcessor = null; // DB 액세서

        public SognoSysView(
            SogNo_ParkDBAccessor DBAcessor,
            int? selectedParkNo,
            ParkInfoRecord[] parkList,
            int? selectedPCNo = null,
            int? selectedSysNo = null,
            int? reQuest = null
            )
        {
            InitializeComponent();
            this.selectedParkNo = selectedParkNo;
            this.DBAcessor = DBAcessor;

            //주차장 콤보박스에 주차장명 전체 삽입
            this.parkList = parkList;
            this.ParkComboBox.DataSource = this.parkList;
            this.ParkComboBox.DisplayMember = ParkInfoFields.ParkName.ToString();
            this.ParkComboBox.ValueMember = ParkInfoFields.ParkNo.ToString();
            this.ParkComboBox.SelectedValue = this.selectedParkNo;

            //PC 콤보박스에 주차장 전체의 PC 데이터 삽입
            this.pcInfoRecords = this.DBAcessor.SelectPCInfo(this.selectedParkNo.GetValueOrDefault());
            this.PCComboBox.DataSource = this.pcInfoRecords;
            this.PCComboBox.DisplayMember = PCInfoFields.PCName.ToString();
            this.PCComboBox.ValueMember = PCInfoFields.PCNo.ToString();
            this.PCComboBox.SelectedIndex = -1;

            if (selectedPCNo != null)
            {
                this.selectedPCNo = selectedPCNo;
                this.PCComboBox.SelectedValue = this.selectedPCNo;
            }
            if (selectedSysNo != null)
            {
                this.selectedSysNo = selectedSysNo;
                selectedSysInfoRecord = DBAcessor.SelectSysInfo(selectedParkNo.GetValueOrDefault(), this.selectedPCNo, this.selectedSysNo)[0]; // 레코드에 데이터 넣기
                fill_textboxes(selectedSysInfoRecord);
            }
            Init_Syslistview();
            RefreshSysList();

            if(reQuest == 1)
            {
                if (this.selectedSysInfoRecord != null)
                {
                    DialogResult dialog = MessageBox.Show("정말 삭제하시겠습니까?", "삭제확인메시지", MessageBoxButtons.YesNo);
                    if (dialog == DialogResult.Yes)
                    {
                        this.DBAcessor.DeleteSysInfo(selectedSysInfoRecord);
                        MessageBox.Show("삭제를 진행하였습니다.");
                        this.Close();
                    }
                    else if (dialog == DialogResult.No)
                    {
                        MessageBox.Show("삭제를 취소하였습니다.");
                    }
                }
                RefreshSysList();
            }
        }

        private void Init_Syslistview()
        {
            //PCListView.BeginUpdate();   이거 사이에 쓰면 업데이트 다 되고 나서 로드됨 (오류방지용)
            //PCListView.EndUpdate();

            SysListView.View = View.Details;
            SysListView.GridLines = false;
            SysListView.FullRowSelect = true; // 선택 시 라인 전체 선택
        }
        private void RefreshSysList()
        {
            SysListView.Clear(); // 전체 지우기

            SysListView.Columns.Add("Index");
            SysListView.Columns.Add("PCNo");
            SysListView.Columns.Add("SysNo");
            SysListView.Columns.Add("SysName");

            for (int i = 0; i < SysListView.Columns.Count; i++)
            {
                SysListView.Columns[i].Width = -2;
            }

            if (selectedSysNo != null)
            {
                sysInfoRecords = DBAcessor.SelectSysInfo(selectedParkNo.GetValueOrDefault(), this.selectedPCNo); // 레코드에 데이터 넣기
            }
            else
            {
                sysInfoRecords = DBAcessor.SelectSysInfo(selectedParkNo.GetValueOrDefault()); // 레코드에 데이터 넣기
            }

            // 폼에 데이터 뿌리기
            if (sysInfoRecords.Length > 0)
            {
                for (int i = 0; sysInfoRecords.Length > i; i++)
                {
                    ListViewItem a = new ListViewItem(i.ToString());
                    a.SubItems.Add(NullCheck(sysInfoRecords[i].PCNo).ToString());
                    a.SubItems.Add(NullCheck(sysInfoRecords[i].SysNo).ToString());
                    a.SubItems.Add(NullCheck(sysInfoRecords[i].SysName).ToString());
                    if (i % 2 == 0) a.BackColor = Color.FromArgb(70, 70, 70);
                    SysListView.Items.Add(a);
                }
            }
        }
        private object NullCheck(object nullitem)
        {
            if (nullitem == null)
            {
                return " ";
            }
            else
            {
                return nullitem;
            }
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            insert_SysInfo();
        }
        private void insert_SysInfo()
        {
            try
            {
                SysInfoRecord insert_SysRecord = new SysInfoRecord
                {
                    ParkNo = selectedParkNo,
                    PCNo = 0,
                    SysNo = 0,
                    SysType = 0,
                    SysName = "기본 시스템",
                    SysPort = 1000,
                    ParentSysNo = 0,
                    GateNos = "",
                    Reserve0 = null
                };

                if (ParkComboBox.Items.Count > 0) insert_SysRecord.ParkNo = int.Parse(ParkComboBox.SelectedValue.ToString());
                if (PCComboBox.Text != "") insert_SysRecord.PCNo = int.Parse(PCComboBox.SelectedValue.ToString());
                if (SysNoTextBox.Text != "") insert_SysRecord.SysNo = int.Parse(SysNoTextBox.Text.ToString());
                if (SysTypeTextBox.Text != "") insert_SysRecord.SysType = int.Parse(SysTypeTextBox.Text.ToString());
                if (SysNameTextBox.Text != "") insert_SysRecord.SysName = SysNameTextBox.Text.ToString();
                if (SysPortTextBox.Text != "") insert_SysRecord.SysPort = int.Parse(SysPortTextBox.Text.ToString());
                if (ParentSysNoTextBox.Text != "") insert_SysRecord.ParentSysNo = int.Parse(ParentSysNoTextBox.Text.ToString());
                if (GateNosTextBox.Text != "") insert_SysRecord.GateNos = GateNosTextBox.Text.ToString();
                //if (Reserve0TextBox.Text != "") insert_SysRecord.Reserve0 = Reserve0TextBox.Text.ToString();

                this.DBAcessor.InsertSysInfo(insert_SysRecord);
                MessageBox.Show("추가를 진행하였습니다.");
                RefreshSysList();

            }
            catch (Exception ex)
            {
                MessageBox.Show("추가에 실패하였습니다.");
                MessageBox.Show(ex.ToString());
            }
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            try
            {
                SysInfoRecord update_SysRecord = new SysInfoRecord();

                if (ParkComboBox.Items.Count > 0) update_SysRecord.ParkNo = int.Parse(ParkComboBox.SelectedValue.ToString());
                if (PCComboBox.Text != "") update_SysRecord.PCNo = int.Parse(PCComboBox.SelectedValue.ToString());
                if (SysNoTextBox.Text != "") update_SysRecord.SysNo = int.Parse(SysNoTextBox.Text.ToString());
                if (SysTypeTextBox.Text != "") update_SysRecord.SysType = int.Parse(SysTypeTextBox.Text.ToString());
                if (SysNameTextBox.Text != "") update_SysRecord.SysName = SysNameTextBox.Text.ToString();
                if (SysPortTextBox.Text != "") update_SysRecord.SysPort = int.Parse(SysPortTextBox.Text.ToString());
                if (ParentSysNoTextBox.Text != "") update_SysRecord.ParentSysNo = int.Parse(ParentSysNoTextBox.Text.ToString());
                if (GateNosTextBox.Text != "") update_SysRecord.GateNos = GateNosTextBox.Text.ToString();
                //if (Reserve0TextBox.Text != "") update_SysRecord.Reserve0 = Reserve0TextBox.Text.ToString();

                this.DBAcessor.UpdateSysInfo(update_SysRecord);
                MessageBox.Show("수정을 진행하였습니다.");
                RefreshSysList();
            }
            catch (Exception ex)
            {
                MessageBox.Show("수정에 실패하였습니다.");
                MessageBox.Show(ex.ToString());
            }
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            if (this.selectedSysInfoRecord != null)
            {
                DialogResult dialog = MessageBox.Show("정말 삭제하시겠습니까?", "삭제확인메시지", MessageBoxButtons.YesNo);
                if (dialog == DialogResult.Yes)
                {
                    this.DBAcessor.DeleteSysInfo(selectedSysInfoRecord);
                    MessageBox.Show("삭제를 진행하였습니다.");
                }
                else if (dialog == DialogResult.No)
                {
                    MessageBox.Show("삭제를 취소하였습니다.");
                }
            }
            RefreshSysList();
        }

        private void SysListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SysListView.SelectedItems.Count > 0)
            {
                int index = int.Parse(SysListView.SelectedItems[0].Text);
                this.selectedSysInfoRecord = this.sysInfoRecords[index];
                fill_textboxes(this.sysInfoRecords[index]);
            }
        }

        public void fill_textboxes(SysInfoRecord record) //텍스트 박스를 채우는 함수
        {
            this.ParkComboBox.SelectedValue = record.ParkNo;
            this.PCComboBox.SelectedValue = record.PCNo;
            if (record.SysNo != null) this.SysNoTextBox.Text = record.SysNo.ToString();
            if (record.SysType != null) this.SysTypeTextBox.Text = record.SysType.ToString();
            if (record.SysName != null) this.SysNameTextBox.Text = record.SysName.ToString();
            if (record.SysPort != null) this.SysPortTextBox.Text = record.SysPort.ToString();
            if (record.ParentSysNo != null) this.ParentSysNoTextBox.Text = record.ParentSysNo.ToString();
            if (record.GateNos != null) this.GateNosTextBox.Text = record.GateNos.ToString();
            //if (record.Reserve0 != null) this.Reserve0TextBox.Text = record.Reserve0.ToString();
        }
    }
}
