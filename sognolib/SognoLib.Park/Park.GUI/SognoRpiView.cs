﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SognoLib.Park.GUI
{
    public partial class SognoRpiView : Form
    {
        private int? selectedParkNo;// 선택된 주차장 번호
        private int? selectedGateNo; // 선택된 게이트 번호
        private int? selectedRpiNo; // 선택된 전광판 번호

        private RpiInfoRecord selectedRpiInfoRecord = null;

        private ParkInfoRecord[] parkList; // 전체 주차장 리스트 데이터
        private GateInfoRecord[] gateInfoRecords = null; // 전체 게이트 데이터
        private RpiInfoRecord[] rpiInfoRecords = null; // 전체 전광판 데이터

        private SogNo_ParkDBAccessor DBAcessor = null; // DB 액세서
        public SognoRpiView(
            SogNo_ParkDBAccessor DBAcessor,
            int? selectedParkNo,
            ParkInfoRecord[] parkList,
            int? selectedGateNo = null,
            int? selectedRpiNo = null,
            int? reQuest = null
            )
        {
            InitializeComponent();
            this.selectedParkNo = selectedParkNo;
            this.DBAcessor = DBAcessor;

            //주차장 콤보박스에 주차장명 전체 삽입
            this.parkList = parkList;
            this.ParkComboBox.DataSource = this.parkList;
            this.ParkComboBox.DisplayMember = ParkInfoFields.ParkName.ToString();
            this.ParkComboBox.ValueMember = ParkInfoFields.ParkNo.ToString();
            this.ParkComboBox.SelectedValue = this.selectedParkNo;

            //게이트 콤보박스에 주차장 전체의 게이트 데이터 삽입
            this.gateInfoRecords = this.DBAcessor.SelectGateInfo(this.selectedParkNo.GetValueOrDefault());
            this.GateComboBox.DataSource = this.gateInfoRecords;
            this.GateComboBox.DisplayMember = GateInfoFields.GateName.ToString();
            this.GateComboBox.ValueMember = GateInfoFields.GateNo.ToString();
            this.GateComboBox.SelectedIndex = -1;

            if (selectedGateNo != null) this.selectedGateNo = selectedGateNo;
            Init_Rpilistview();
            RefreshRpiList();
            if (selectedRpiNo != null)
            {
                this.selectedRpiNo = selectedRpiNo;
                this.selectedRpiInfoRecord = DBAcessor.SelectRpiInfo(selectedParkNo.GetValueOrDefault(), this.selectedGateNo, this.selectedRpiNo)[0];
                fill_textboxes(selectedRpiInfoRecord);
            }
            if(reQuest == 1)
            {
                if (this.selectedRpiInfoRecord != null)
                {
                    DialogResult dialog = MessageBox.Show("정말 삭제하시겠습니까?", "삭제확인메시지", MessageBoxButtons.YesNo);
                    if (dialog == DialogResult.Yes)
                    {
                        this.DBAcessor.DeleteRpiInfo(selectedRpiInfoRecord);
                        MessageBox.Show("삭제를 진행하였습니다.");
                    }
                    else if (dialog == DialogResult.No)
                    {
                        MessageBox.Show("삭제를 취소하였습니다.");
                    }
                }
                RefreshRpiList();
            }


        }
        private void Init_Rpilistview()
        {
            //PCListView.BeginUpdate();   이거 사이에 쓰면 업데이트 다 되고 나서 로드됨 (오류방지용)
            //PCListView.EndUpdate();

            RpiListView.View = View.Details;
            RpiListView.GridLines = false;
            RpiListView.FullRowSelect = true; // 선택 시 라인 전체 선택
        }
        private void RefreshRpiList()
        {
            RpiListView.Clear(); // 전체 지우기

            RpiListView.Columns.Add("Index");
            RpiListView.Columns.Add("GateNo");
            RpiListView.Columns.Add("RpiNo");
            RpiListView.Columns.Add("RpiName");

            for (int i = 0; i < RpiListView.Columns.Count; i++)
            {
                RpiListView.Columns[i].Width = -2;
            }

            if (selectedGateNo != null)
            {
                rpiInfoRecords = DBAcessor.SelectRpiInfo(selectedParkNo.GetValueOrDefault(), this.selectedGateNo); // 레코드에 데이터 넣기
            }
            else
            {
                rpiInfoRecords = DBAcessor.SelectRpiInfo(selectedParkNo.GetValueOrDefault()); // 레코드에 데이터 넣기
            }

            // 폼에 데이터 뿌리기
            if (rpiInfoRecords.Length > 0)
            {
                for (int i = 0; rpiInfoRecords.Length > i; i++)
                {
                    ListViewItem a = new ListViewItem(i.ToString());
                    a.SubItems.Add(NullCheck(rpiInfoRecords[i].GateNo).ToString());
                    a.SubItems.Add(NullCheck(rpiInfoRecords[i].RpiNo).ToString());
                    a.SubItems.Add(NullCheck(rpiInfoRecords[i].RpiName).ToString());
                    if (i % 2 == 0) a.BackColor = Color.FromArgb(70, 70, 70);
                    RpiListView.Items.Add(a);
                }
            }
        }
        private object NullCheck(object nullitem)
        {
            if (nullitem == null)
            {
                return " ";
            }
            else
            {
                return nullitem;
            }
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            insert_RpiInfo();
        }

        private void insert_RpiInfo()
        {
            try
            {
                RpiInfoRecord insert_RpiRecord = new RpiInfoRecord
                {
                    ParkNo = selectedParkNo,
                    GateNo = 0,
                    RpiNo = 0,
                    RpiName = "기본 라즈베리파이",
                    RpiType = 0,
                    RpiTypeComment = "기본 타입",
                    RpiIP = "172.16.10.100",
                    RpiPort = 0,
                    Reserve0 = null
                };

                if (ParkComboBox.Items.Count > 0) insert_RpiRecord.ParkNo = int.Parse(ParkComboBox.SelectedValue.ToString());
                if (GateComboBox.Text != "") insert_RpiRecord.GateNo = int.Parse(GateComboBox.SelectedValue.ToString());
                if (RpiNoTextBox.Text != "") insert_RpiRecord.RpiNo = int.Parse(RpiNoTextBox.Text.ToString());
                if (RpiNameTextBox.Text != "") insert_RpiRecord.RpiName = RpiNameTextBox.Text.ToString();
                if (RpiTypeTextBox.Text != "") insert_RpiRecord.RpiType = int.Parse(RpiTypeTextBox.Text.ToString());
                if (RpiTypeCommentTextBox.Text != "") insert_RpiRecord.RpiTypeComment = RpiTypeCommentTextBox.Text.ToString();
                if (RpiIPTextBox.Text != "") insert_RpiRecord.RpiIP = RpiIPTextBox.Text.ToString();
                if (RpiPortTextBox.Text != "") insert_RpiRecord.RpiPort = int.Parse(RpiPortTextBox.Text.ToString());
                //if (Reserve0TextBox.Text != "") insert_RpiRecord.Reserve0 = Reserve0TextBox.Text.ToString();

                this.DBAcessor.InsertRpiInfo(insert_RpiRecord);
                MessageBox.Show("추가를 진행하였습니다.");
                RefreshRpiList();

            }
            catch (Exception ex)
            {
                MessageBox.Show("추가에 실패하였습니다.");
                MessageBox.Show(ex.ToString());
            }
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            try
            {
                RpiInfoRecord update_RpiRecord = new RpiInfoRecord();
                if (ParkComboBox.Items.Count > 0) update_RpiRecord.ParkNo = int.Parse(ParkComboBox.SelectedValue.ToString());
                if (GateComboBox.Text != "") update_RpiRecord.GateNo = int.Parse(GateComboBox.SelectedValue.ToString());
                if (RpiNoTextBox.Text != "") update_RpiRecord.RpiNo = int.Parse(RpiNoTextBox.Text.ToString());
                if (RpiNameTextBox.Text != "") update_RpiRecord.RpiName = RpiNameTextBox.Text.ToString();
                if (RpiTypeTextBox.Text != "") update_RpiRecord.RpiType = int.Parse(RpiTypeTextBox.Text.ToString());
                if (RpiTypeCommentTextBox.Text != "") update_RpiRecord.RpiTypeComment = RpiTypeCommentTextBox.Text.ToString();
                if (RpiIPTextBox.Text != "") update_RpiRecord.RpiIP = RpiIPTextBox.Text.ToString();
                if (RpiPortTextBox.Text != "") update_RpiRecord.RpiPort = int.Parse(RpiPortTextBox.Text.ToString());
                //if (Reserve0TextBox.Text != "") update_RpiRecord.Reserve0 = Reserve0TextBox.Text.ToString();

                this.DBAcessor.UpdateRpiInfo(update_RpiRecord);
                MessageBox.Show("수정을 진행하였습니다.");
                RefreshRpiList();
            }
            catch (Exception ex)
            {
                MessageBox.Show("수정에 실패하였습니다.");
                MessageBox.Show(ex.ToString());
            }
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            if (this.selectedRpiInfoRecord != null)
            {
                DialogResult dialog = MessageBox.Show("정말 삭제하시겠습니까?", "삭제확인메시지", MessageBoxButtons.YesNo);
                if (dialog == DialogResult.Yes)
                {
                    this.DBAcessor.DeleteRpiInfo(selectedRpiInfoRecord);
                    MessageBox.Show("삭제를 진행하였습니다.");
                }
                else if (dialog == DialogResult.No)
                {
                    MessageBox.Show("삭제를 취소하였습니다.");
                }
            }
            RefreshRpiList();
        }

        private void RpiListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RpiListView.SelectedItems.Count > 0)
            {
                int index = int.Parse(RpiListView.SelectedItems[0].Text);
                this.selectedRpiInfoRecord = this.rpiInfoRecords[index];
                fill_textboxes(this.rpiInfoRecords[index]);
            }
        }
        public void fill_textboxes(RpiInfoRecord record) //텍스트 박스를 채우는 함수
        {
            this.ParkComboBox.SelectedValue = record.ParkNo;
            this.GateComboBox.SelectedValue = record.GateNo;
            if (record.RpiNo != null) this.RpiNoTextBox.Text = record.RpiNo.ToString();
            if (record.RpiName != null) this.RpiNameTextBox.Text = record.RpiName.ToString();
            if (record.RpiType != null) this.RpiTypeTextBox.Text = record.RpiType.ToString();
            if (record.RpiTypeComment != null) this.RpiTypeCommentTextBox.Text = record.RpiTypeComment.ToString();
            if (record.RpiIP != null) this.RpiIPTextBox.Text = record.RpiIP.ToString();
            if (record.RpiPort != null) this.RpiPortTextBox.Text = record.RpiPort.ToString();
            //if (record.Reserve0 != null) this.Reserve0TextBox.Text = record.Reserve0.ToString();
        }
    }
}
