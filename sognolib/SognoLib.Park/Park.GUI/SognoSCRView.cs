﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SognoLib.Park.GUI
{
    public partial class SognoSCRView : Form
    {
        private int? selectedParkNo;// 선택된 주차장 번호
        private int? selectedGateNo; // 선택된 게이트 번호
        private int? selectedSCRNo; // 선택된 전광판 번호

        private ScrInfoRecord selectedSCRInfoRecord = null;

        private ParkInfoRecord[] parkList; // 전체 주차장 리스트 데이터
        private GateInfoRecord[] gateInfoRecords = null; // 전체 게이트 데이터
        private ScrInfoRecord[] scrInfoRecords = null; // 전체 전광판 데이터

        private SogNo_ParkDBAccessor DBAcessor = null; // DB 액세서

        public SognoSCRView(
            SogNo_ParkDBAccessor DBAcessor,
            int? selectedParkNo,
            ParkInfoRecord[] parkList,
            int? selectedGateNo = null,
            int? selectedSCRNo = null,
            int? reQuest = null
            )
        {
            InitializeComponent();
            this.selectedParkNo = selectedParkNo;
            this.DBAcessor = DBAcessor;

            //주차장 콤보박스에 주차장명 전체 삽입
            this.parkList = parkList;
            this.ParkComboBox.DataSource = this.parkList;
            this.ParkComboBox.DisplayMember = ParkInfoFields.ParkName.ToString();
            this.ParkComboBox.ValueMember = ParkInfoFields.ParkNo.ToString();
            this.ParkComboBox.SelectedValue = this.selectedParkNo;

            //게이트 콤보박스에 주차장 전체의 게이트 데이터 삽입
            this.gateInfoRecords = this.DBAcessor.SelectGateInfo(this.selectedParkNo.GetValueOrDefault());
            this.GateComboBox.DataSource = this.gateInfoRecords;
            this.GateComboBox.DisplayMember = GateInfoFields.GateName.ToString();
            this.GateComboBox.ValueMember = GateInfoFields.GateNo.ToString();
            this.GateComboBox.SelectedIndex = -1;

            if (selectedGateNo != null) this.selectedGateNo = selectedGateNo;
           
            Init_SCRlistview();
            RefreshSCRList();
            if (selectedSCRNo != null)
            {
                this.selectedSCRNo = selectedSCRNo;
                this.selectedSCRInfoRecord = DBAcessor.SelectSCRInfo(selectedParkNo.GetValueOrDefault(),this.selectedGateNo, this.selectedSCRNo)[0];
                fill_textboxes(selectedSCRInfoRecord);
            }

            if(reQuest == 1)
            {
                if (this.selectedSCRInfoRecord != null)
                {
                    DialogResult dialog = MessageBox.Show("정말 삭제하시겠습니까?", "삭제확인메시지", MessageBoxButtons.YesNo);
                    if (dialog == DialogResult.Yes)
                    {
                        this.DBAcessor.DeleteSCRInfo(selectedSCRInfoRecord);
                        MessageBox.Show("삭제를 진행하였습니다.");
                    }
                    else if (dialog == DialogResult.No)
                    {
                        MessageBox.Show("삭제를 취소하였습니다.");
                    }
                }
                RefreshSCRList();
            }
        }
        private void Init_SCRlistview()
        {
            //PCListView.BeginUpdate();   이거 사이에 쓰면 업데이트 다 되고 나서 로드됨 (오류방지용)
            //PCListView.EndUpdate();

            SCRListView.View = View.Details;
            SCRListView.GridLines = false;
            SCRListView.FullRowSelect = true; // 선택 시 라인 전체 선택
        }

        private void RefreshSCRList()
        {
            SCRListView.Clear(); // 전체 지우기

            SCRListView.Columns.Add("Index");
            SCRListView.Columns.Add("GateNo");
            SCRListView.Columns.Add("SCRNo");
            SCRListView.Columns.Add("SCRName");

            for (int i = 0; i < SCRListView.Columns.Count; i++)
            {
                SCRListView.Columns[i].Width = -2;
            }

            if (selectedGateNo != null)
            {
                scrInfoRecords = DBAcessor.SelectSCRInfo(selectedParkNo.GetValueOrDefault(), this.selectedGateNo); // 레코드에 데이터 넣기
            }
            else
            {
                scrInfoRecords = DBAcessor.SelectSCRInfo(selectedParkNo.GetValueOrDefault()); // 레코드에 데이터 넣기
            }

            // 폼에 데이터 뿌리기
            if (scrInfoRecords.Length > 0)
            {
                for (int i = 0; scrInfoRecords.Length > i; i++)
                {
                    ListViewItem a = new ListViewItem(i.ToString());
                    a.SubItems.Add(NullCheck(scrInfoRecords[i].GateNo).ToString());
                    a.SubItems.Add(NullCheck(scrInfoRecords[i].SCRNo).ToString());
                    a.SubItems.Add(NullCheck(scrInfoRecords[i].SCRName).ToString());
                    if (i % 2 == 0) a.BackColor = Color.FromArgb(70, 70, 70);
                    SCRListView.Items.Add(a);
                }
            }
        }
        private object NullCheck(object nullitem)
        {
            if (nullitem == null)
            {
                return " ";
            }
            else
            {
                return nullitem;
            }
        }


        private void AddButton_Click(object sender, EventArgs e)
        {
            insert_SCRInfo();
        }

        private void insert_SCRInfo()
        {
            try
            {
                ScrInfoRecord insert_SCRRecord = new ScrInfoRecord
                {
                    ParkNo = selectedParkNo,
                    GateNo = 0,
                    SCRNo = 0,
                    SCRType = 0,
                    SCRIP = "172.16.10.103",
                    SCRPort = 1000,
                    SCRName = "기본 전광판",                    
                    SCRTime = 0,
                    UpText = "기본상단문구",
                    UpColor = 0,
                    DownText = "기본하단문구",
                    DownColor = 0,
                    Reserve0 = null,
                };

                if (ParkComboBox.Items.Count > 0) insert_SCRRecord.ParkNo = int.Parse(ParkComboBox.SelectedValue.ToString());
                if (GateComboBox.Text != "") insert_SCRRecord.GateNo = int.Parse(GateComboBox.SelectedValue.ToString());
                if (SCRNoTextBox.Text != "") insert_SCRRecord.SCRNo = int.Parse(SCRNoTextBox.Text.ToString());
                if (SCRTypeTextBox.Text != "") insert_SCRRecord.SCRType = int.Parse(SCRTypeTextBox.Text.ToString());
                if (SCRIPTextBox.Text != "") insert_SCRRecord.SCRIP = SCRIPTextBox.Text.ToString();
                if (SCRPortTextBox.Text != "") insert_SCRRecord.SCRPort = int.Parse(SCRPortTextBox.Text.ToString());
                if (SCRNameTextBox.Text != "") insert_SCRRecord.SCRName = SCRNameTextBox.Text.ToString();
                if (SCRTimeTextBox.Text != "") insert_SCRRecord.SCRTime = int.Parse(SCRTimeTextBox.Text.ToString());
                if (UpTextTextBox.Text != "") insert_SCRRecord.UpText = UpTextTextBox.Text.ToString();
                if (UpColorTextBox.Text != "") insert_SCRRecord.UpColor = int.Parse(UpColorTextBox.Text.ToString());
                if (DownTextTextBox.Text != "") insert_SCRRecord.DownText = DownTextTextBox.Text.ToString();
                if (DownColorTextBox.Text != "") insert_SCRRecord.DownColor = int.Parse(DownColorTextBox.Text.ToString());
                //if (Reserve0TextBox.Text != "") insert_SCRRecord.Reserve0 = Reserve0TextBox.Text.ToString();
            
                this.DBAcessor.InsertSCRInfo(insert_SCRRecord);
                MessageBox.Show("추가를 진행하였습니다.");
                RefreshSCRList();

            }
            catch (Exception ex)
            {
                MessageBox.Show("추가에 실패하였습니다.");
                MessageBox.Show(ex.ToString());
            }
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            try
            {
                ScrInfoRecord update_SCRRecord = new ScrInfoRecord();

                if (ParkComboBox.Items.Count > 0) update_SCRRecord.ParkNo = int.Parse(ParkComboBox.SelectedValue.ToString());
                if (GateComboBox.Text != "") update_SCRRecord.GateNo = int.Parse(GateComboBox.SelectedValue.ToString());
                if (SCRNoTextBox.Text != "") update_SCRRecord.SCRNo = int.Parse(SCRNoTextBox.Text.ToString());
                if (SCRTypeTextBox.Text != "") update_SCRRecord.SCRType = int.Parse(SCRTypeTextBox.Text.ToString());
                if (SCRIPTextBox.Text != "") update_SCRRecord.SCRIP = SCRIPTextBox.Text.ToString();
                if (SCRPortTextBox.Text != "") update_SCRRecord.SCRPort = int.Parse(SCRPortTextBox.Text.ToString());
                if (SCRNameTextBox.Text != "") update_SCRRecord.SCRName = SCRNameTextBox.Text.ToString();
                if (SCRTimeTextBox.Text != "") update_SCRRecord.SCRTime = int.Parse(SCRTimeTextBox.Text.ToString());
                if (UpTextTextBox.Text != "") update_SCRRecord.UpText = UpTextTextBox.Text.ToString();
                if (UpColorTextBox.Text != "") update_SCRRecord.UpColor = int.Parse(UpColorTextBox.Text.ToString());
                if (DownTextTextBox.Text != "") update_SCRRecord.DownText = DownTextTextBox.Text.ToString();
                if (DownColorTextBox.Text != "") update_SCRRecord.DownColor = int.Parse(DownColorTextBox.Text.ToString());
                //if (Reserve0TextBox.Text != "") update_SCRRecord.Reserve0 = Reserve0TextBox.Text.ToString();

                this.DBAcessor.UpdateSCRInfo(update_SCRRecord);
                MessageBox.Show("수정을 진행하였습니다.");
                RefreshSCRList();
            }
            catch (Exception ex)
            {
                MessageBox.Show("수정에 실패하였습니다.");
                MessageBox.Show(ex.ToString());
            }
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            if (this.selectedSCRInfoRecord != null)
            {
                DialogResult dialog = MessageBox.Show("정말 삭제하시겠습니까?", "삭제확인메시지", MessageBoxButtons.YesNo);
                if (dialog == DialogResult.Yes)
                {
                    this.DBAcessor.DeleteSCRInfo(selectedSCRInfoRecord);
                    MessageBox.Show("삭제를 진행하였습니다.");
                }
                else if (dialog == DialogResult.No)
                {
                    MessageBox.Show("삭제를 취소하였습니다.");
                }
            }
            RefreshSCRList();
        }

        private void SCRListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SCRListView.SelectedItems.Count > 0)
            {
                int index = int.Parse(SCRListView.SelectedItems[0].Text);
                this.selectedSCRInfoRecord = this.scrInfoRecords[index];
                fill_textboxes(this.scrInfoRecords[index]);
            }
        }

        public void fill_textboxes(ScrInfoRecord record) //텍스트 박스를 채우는 함수
        {
            this.ParkComboBox.SelectedValue = record.ParkNo;
            this.GateComboBox.SelectedValue = record.GateNo;
            if (record.SCRNo != null) this.SCRNoTextBox.Text = record.SCRNo.ToString();
            if (record.SCRType != null) this.SCRTypeTextBox.Text = record.SCRType.ToString();
            if (record.SCRIP != null) this.SCRIPTextBox.Text = record.SCRIP.ToString();
            if (record.SCRPort != null) this.SCRPortTextBox.Text = record.SCRPort.ToString();
            if (record.SCRName != null) this.SCRNameTextBox.Text = record.SCRName.ToString();
            if (record.SCRTime != null) this.SCRTimeTextBox.Text = record.SCRTime.ToString();
            if (record.UpText != null) this.UpTextTextBox.Text = record.UpText.ToString();
            if (record.UpColor != null) this.UpColorTextBox.Text = record.UpColor.ToString();
            if (record.DownText != null) this.DownTextTextBox.Text = record.DownText.ToString();
            if (record.DownColor != null) this.DownColorTextBox.Text = record.DownColor.ToString();
            //if (record.Reserve0 != null) this.Reserve0TextBox.Text = record.Reserve0.ToString();
        }
    }
}
