﻿namespace SognoLib.Park.GUI
{
    partial class SognoRpiView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.updateButton = new System.Windows.Forms.Button();
            this.RpiPortTextBox = new System.Windows.Forms.TextBox();
            this.RpiIPTextBox = new System.Windows.Forms.TextBox();
            this.addButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.RpiNoTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ParkComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.GateComboBox = new System.Windows.Forms.ComboBox();
            this.RpiNameTextBox = new System.Windows.Forms.TextBox();
            this.RpiTypeCommentTextBox = new System.Windows.Forms.TextBox();
            this.RpiTypeTextBox = new System.Windows.Forms.TextBox();
            this.RpiListView = new System.Windows.Forms.ListView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // updateButton
            // 
            this.updateButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(84)))), ((int)(((byte)(136)))));
            this.updateButton.FlatAppearance.BorderSize = 0;
            this.updateButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.updateButton.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.updateButton.ForeColor = System.Drawing.SystemColors.Control;
            this.updateButton.Location = new System.Drawing.Point(544, 380);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(112, 45);
            this.updateButton.TabIndex = 26;
            this.updateButton.Text = "수정";
            this.updateButton.UseVisualStyleBackColor = false;
            this.updateButton.Click += new System.EventHandler(this.UpdateButton_Click);
            // 
            // RpiPortTextBox
            // 
            this.RpiPortTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.RpiPortTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.RpiPortTextBox.Location = new System.Drawing.Point(195, 312);
            this.RpiPortTextBox.Name = "RpiPortTextBox";
            this.RpiPortTextBox.Size = new System.Drawing.Size(273, 36);
            this.RpiPortTextBox.TabIndex = 40;
            // 
            // RpiIPTextBox
            // 
            this.RpiIPTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.RpiIPTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.RpiIPTextBox.Location = new System.Drawing.Point(195, 268);
            this.RpiIPTextBox.Name = "RpiIPTextBox";
            this.RpiIPTextBox.Size = new System.Drawing.Size(273, 36);
            this.RpiIPTextBox.TabIndex = 39;
            // 
            // addButton
            // 
            this.addButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(124)))), ((int)(((byte)(113)))));
            this.addButton.FlatAppearance.BorderSize = 0;
            this.addButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addButton.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.addButton.ForeColor = System.Drawing.SystemColors.Control;
            this.addButton.Location = new System.Drawing.Point(426, 380);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(112, 45);
            this.addButton.TabIndex = 24;
            this.addButton.Text = "추가";
            this.addButton.UseVisualStyleBackColor = false;
            this.addButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(49)))), ((int)(((byte)(83)))));
            this.deleteButton.FlatAppearance.BorderSize = 0;
            this.deleteButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deleteButton.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.deleteButton.ForeColor = System.Drawing.SystemColors.Control;
            this.deleteButton.Location = new System.Drawing.Point(801, 380);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(112, 45);
            this.deleteButton.TabIndex = 27;
            this.deleteButton.Text = "삭제";
            this.deleteButton.UseVisualStyleBackColor = false;
            this.deleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // RpiNoTextBox
            // 
            this.RpiNoTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.RpiNoTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.RpiNoTextBox.Location = new System.Drawing.Point(195, 92);
            this.RpiNoTextBox.Name = "RpiNoTextBox";
            this.RpiNoTextBox.Size = new System.Drawing.Size(273, 36);
            this.RpiNoTextBox.TabIndex = 35;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(33, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "소속 주차장";
            // 
            // ParkComboBox
            // 
            this.ParkComboBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ParkComboBox.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ParkComboBox.FormattingEnabled = true;
            this.ParkComboBox.Location = new System.Drawing.Point(195, 7);
            this.ParkComboBox.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.ParkComboBox.Name = "ParkComboBox";
            this.ParkComboBox.Size = new System.Drawing.Size(273, 32);
            this.ParkComboBox.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.SystemColors.Control;
            this.label4.Location = new System.Drawing.Point(33, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 21);
            this.label4.TabIndex = 24;
            this.label4.Text = "소속 게이트";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.SystemColors.Control;
            this.label5.Location = new System.Drawing.Point(46, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 21);
            this.label5.TabIndex = 25;
            this.label5.Text = "Rpi 번호";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.SystemColors.Control;
            this.label6.Location = new System.Drawing.Point(46, 143);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 21);
            this.label6.TabIndex = 26;
            this.label6.Text = "Rpi 명칭";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.ForeColor = System.Drawing.SystemColors.Control;
            this.label7.Location = new System.Drawing.Point(46, 187);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 21);
            this.label7.TabIndex = 27;
            this.label7.Text = "Rpi 타입";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.ForeColor = System.Drawing.SystemColors.Control;
            this.label8.Location = new System.Drawing.Point(24, 231);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(129, 21);
            this.label8.TabIndex = 28;
            this.label8.Text = "Rpi 타입 설명";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.ForeColor = System.Drawing.SystemColors.Control;
            this.label9.Location = new System.Drawing.Point(36, 275);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(104, 21);
            this.label9.TabIndex = 29;
            this.label9.Text = "Rpi 아이피";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.ForeColor = System.Drawing.SystemColors.Control;
            this.label10.Location = new System.Drawing.Point(46, 320);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(85, 21);
            this.label10.TabIndex = 30;
            this.label10.Text = "Rpi 포트";
            // 
            // GateComboBox
            // 
            this.GateComboBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.GateComboBox.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.GateComboBox.FormattingEnabled = true;
            this.GateComboBox.Location = new System.Drawing.Point(195, 51);
            this.GateComboBox.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.GateComboBox.Name = "GateComboBox";
            this.GateComboBox.Size = new System.Drawing.Size(273, 32);
            this.GateComboBox.TabIndex = 43;
            // 
            // RpiNameTextBox
            // 
            this.RpiNameTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.RpiNameTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.RpiNameTextBox.Location = new System.Drawing.Point(195, 136);
            this.RpiNameTextBox.Name = "RpiNameTextBox";
            this.RpiNameTextBox.Size = new System.Drawing.Size(273, 36);
            this.RpiNameTextBox.TabIndex = 38;
            // 
            // RpiTypeCommentTextBox
            // 
            this.RpiTypeCommentTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.RpiTypeCommentTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.RpiTypeCommentTextBox.Location = new System.Drawing.Point(195, 224);
            this.RpiTypeCommentTextBox.Name = "RpiTypeCommentTextBox";
            this.RpiTypeCommentTextBox.Size = new System.Drawing.Size(273, 36);
            this.RpiTypeCommentTextBox.TabIndex = 37;
            // 
            // RpiTypeTextBox
            // 
            this.RpiTypeTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.RpiTypeTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.RpiTypeTextBox.Location = new System.Drawing.Point(195, 180);
            this.RpiTypeTextBox.Name = "RpiTypeTextBox";
            this.RpiTypeTextBox.Size = new System.Drawing.Size(273, 36);
            this.RpiTypeTextBox.TabIndex = 36;
            // 
            // RpiListView
            // 
            this.RpiListView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.RpiListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RpiListView.ForeColor = System.Drawing.SystemColors.Control;
            this.RpiListView.HideSelection = false;
            this.RpiListView.Location = new System.Drawing.Point(12, 12);
            this.RpiListView.Name = "RpiListView";
            this.RpiListView.Size = new System.Drawing.Size(408, 353);
            this.RpiListView.TabIndex = 25;
            this.RpiListView.UseCompatibleStateImageBehavior = false;
            this.RpiListView.SelectedIndexChanged += new System.EventHandler(this.RpiListView_SelectedIndexChanged);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.46789F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.53211F));
            this.tableLayoutPanel1.Controls.Add(this.RpiPortTextBox, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.RpiIPTextBox, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.RpiNoTextBox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ParkComboBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.GateComboBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.RpiNameTextBox, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.RpiTypeCommentTextBox, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.RpiTypeTextBox, 1, 4);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(426, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(487, 353);
            this.tableLayoutPanel1.TabIndex = 23;
            // 
            // SognoRpiView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.ClientSize = new System.Drawing.Size(925, 440);
            this.Controls.Add(this.updateButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.RpiListView);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "SognoRpiView";
            this.Text = "SognoRpiView";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.TextBox RpiPortTextBox;
        private System.Windows.Forms.TextBox RpiIPTextBox;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.TextBox RpiNoTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ParkComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox GateComboBox;
        private System.Windows.Forms.TextBox RpiNameTextBox;
        private System.Windows.Forms.TextBox RpiTypeCommentTextBox;
        private System.Windows.Forms.TextBox RpiTypeTextBox;
        private System.Windows.Forms.ListView RpiListView;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}