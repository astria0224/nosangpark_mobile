﻿namespace SognoLib.Park.GUI
{
    partial class SognoSysView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SysListView = new System.Windows.Forms.ListView();
            this.GateNosTextBox = new System.Windows.Forms.TextBox();
            this.ParentSysNoTextBox = new System.Windows.Forms.TextBox();
            this.SysTypeTextBox = new System.Windows.Forms.TextBox();
            this.SysPortTextBox = new System.Windows.Forms.TextBox();
            this.SysNameTextBox = new System.Windows.Forms.TextBox();
            this.SysNoTextBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.ParkComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.PCComboBox = new System.Windows.Forms.ComboBox();
            this.addButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.updateButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SysListView
            // 
            this.SysListView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.SysListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SysListView.ForeColor = System.Drawing.SystemColors.Control;
            this.SysListView.HideSelection = false;
            this.SysListView.Location = new System.Drawing.Point(12, 12);
            this.SysListView.Name = "SysListView";
            this.SysListView.Size = new System.Drawing.Size(406, 353);
            this.SysListView.TabIndex = 20;
            this.SysListView.UseCompatibleStateImageBehavior = false;
            this.SysListView.SelectedIndexChanged += new System.EventHandler(this.SysListView_SelectedIndexChanged);
            // 
            // GateNosTextBox
            // 
            this.GateNosTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.GateNosTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.GateNosTextBox.Location = new System.Drawing.Point(179, 312);
            this.GateNosTextBox.Name = "GateNosTextBox";
            this.GateNosTextBox.Size = new System.Drawing.Size(273, 36);
            this.GateNosTextBox.TabIndex = 40;
            // 
            // ParentSysNoTextBox
            // 
            this.ParentSysNoTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ParentSysNoTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ParentSysNoTextBox.Location = new System.Drawing.Point(179, 268);
            this.ParentSysNoTextBox.Name = "ParentSysNoTextBox";
            this.ParentSysNoTextBox.Size = new System.Drawing.Size(273, 36);
            this.ParentSysNoTextBox.TabIndex = 39;
            // 
            // SysTypeTextBox
            // 
            this.SysTypeTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SysTypeTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.SysTypeTextBox.Location = new System.Drawing.Point(179, 136);
            this.SysTypeTextBox.Name = "SysTypeTextBox";
            this.SysTypeTextBox.Size = new System.Drawing.Size(273, 36);
            this.SysTypeTextBox.TabIndex = 38;
            // 
            // SysPortTextBox
            // 
            this.SysPortTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SysPortTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.SysPortTextBox.Location = new System.Drawing.Point(179, 224);
            this.SysPortTextBox.Name = "SysPortTextBox";
            this.SysPortTextBox.Size = new System.Drawing.Size(273, 36);
            this.SysPortTextBox.TabIndex = 37;
            // 
            // SysNameTextBox
            // 
            this.SysNameTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SysNameTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.SysNameTextBox.Location = new System.Drawing.Point(179, 180);
            this.SysNameTextBox.Name = "SysNameTextBox";
            this.SysNameTextBox.Size = new System.Drawing.Size(273, 36);
            this.SysNameTextBox.TabIndex = 36;
            // 
            // SysNoTextBox
            // 
            this.SysNoTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SysNoTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.SysNoTextBox.Location = new System.Drawing.Point(179, 92);
            this.SysNoTextBox.Name = "SysNoTextBox";
            this.SysNoTextBox.Size = new System.Drawing.Size(273, 36);
            this.SysNoTextBox.TabIndex = 35;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.46789F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.53211F));
            this.tableLayoutPanel1.Controls.Add(this.GateNosTextBox, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.ParentSysNoTextBox, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.SysNoTextBox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ParkComboBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.PCComboBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.SysTypeTextBox, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.SysPortTextBox, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.SysNameTextBox, 1, 4);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(424, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(463, 353);
            this.tableLayoutPanel1.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(28, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "소속 주차장";
            // 
            // ParkComboBox
            // 
            this.ParkComboBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ParkComboBox.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ParkComboBox.FormattingEnabled = true;
            this.ParkComboBox.Location = new System.Drawing.Point(179, 7);
            this.ParkComboBox.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.ParkComboBox.Name = "ParkComboBox";
            this.ParkComboBox.Size = new System.Drawing.Size(273, 32);
            this.ParkComboBox.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.SystemColors.Control;
            this.label4.Location = new System.Drawing.Point(44, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 21);
            this.label4.TabIndex = 24;
            this.label4.Text = "소속 PC";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.SystemColors.Control;
            this.label5.Location = new System.Drawing.Point(28, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 21);
            this.label5.TabIndex = 25;
            this.label5.Text = "시스템 번호";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.SystemColors.Control;
            this.label6.Location = new System.Drawing.Point(28, 143);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 21);
            this.label6.TabIndex = 26;
            this.label6.Text = "시스템 형식";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.ForeColor = System.Drawing.SystemColors.Control;
            this.label7.Location = new System.Drawing.Point(28, 187);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 21);
            this.label7.TabIndex = 27;
            this.label7.Text = "시스템 이름";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.ForeColor = System.Drawing.SystemColors.Control;
            this.label8.Location = new System.Drawing.Point(28, 231);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 21);
            this.label8.TabIndex = 28;
            this.label8.Text = "시스템 포트";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.ForeColor = System.Drawing.SystemColors.Control;
            this.label9.Location = new System.Drawing.Point(6, 275);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(155, 21);
            this.label9.TabIndex = 29;
            this.label9.Text = "부모 시스템 번호";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.ForeColor = System.Drawing.SystemColors.Control;
            this.label10.Location = new System.Drawing.Point(6, 320);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(155, 21);
            this.label10.TabIndex = 30;
            this.label10.Text = "연동 게이트 번호";
            // 
            // PCComboBox
            // 
            this.PCComboBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PCComboBox.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.PCComboBox.FormattingEnabled = true;
            this.PCComboBox.Location = new System.Drawing.Point(179, 51);
            this.PCComboBox.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.PCComboBox.Name = "PCComboBox";
            this.PCComboBox.Size = new System.Drawing.Size(273, 32);
            this.PCComboBox.TabIndex = 43;
            // 
            // addButton
            // 
            this.addButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(124)))), ((int)(((byte)(113)))));
            this.addButton.FlatAppearance.BorderSize = 0;
            this.addButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addButton.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.addButton.ForeColor = System.Drawing.SystemColors.Control;
            this.addButton.Location = new System.Drawing.Point(424, 380);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(112, 45);
            this.addButton.TabIndex = 19;
            this.addButton.Text = "추가";
            this.addButton.UseVisualStyleBackColor = false;
            this.addButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(49)))), ((int)(((byte)(83)))));
            this.deleteButton.FlatAppearance.BorderSize = 0;
            this.deleteButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deleteButton.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.deleteButton.ForeColor = System.Drawing.SystemColors.Control;
            this.deleteButton.Location = new System.Drawing.Point(773, 380);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(112, 45);
            this.deleteButton.TabIndex = 22;
            this.deleteButton.Text = "삭제";
            this.deleteButton.UseVisualStyleBackColor = false;
            this.deleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // updateButton
            // 
            this.updateButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(84)))), ((int)(((byte)(136)))));
            this.updateButton.FlatAppearance.BorderSize = 0;
            this.updateButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.updateButton.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.updateButton.ForeColor = System.Drawing.SystemColors.Control;
            this.updateButton.Location = new System.Drawing.Point(542, 380);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(112, 45);
            this.updateButton.TabIndex = 21;
            this.updateButton.Text = "수정";
            this.updateButton.UseVisualStyleBackColor = false;
            this.updateButton.Click += new System.EventHandler(this.UpdateButton_Click);
            // 
            // SognoSysView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.ClientSize = new System.Drawing.Size(899, 439);
            this.Controls.Add(this.SysListView);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.updateButton);
            this.Name = "SognoSysView";
            this.Text = "SognoSysView";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView SysListView;
        private System.Windows.Forms.TextBox GateNosTextBox;
        private System.Windows.Forms.TextBox ParentSysNoTextBox;
        private System.Windows.Forms.TextBox SysTypeTextBox;
        private System.Windows.Forms.TextBox SysPortTextBox;
        private System.Windows.Forms.TextBox SysNameTextBox;
        private System.Windows.Forms.TextBox SysNoTextBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ParkComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox PCComboBox;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button updateButton;
    }
}