﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SognoLib.Park.GUI
{
    public partial class SognoPCView : Form
    {
        private int? selectedParkNo;
        private int? selectedGateNo;
        private int? selectedPCNo;

        private SogNo_ParkDBAccessor DBAcessor = null; // DB액세서
        private ParkInfoRecord[] parkList; // 전체 주차장 데이터
        private GateInfoRecord[] gateInfoRecords; // 전체 게이트 데이터 
        private PCInfoRecord[] pcInfoRecords = null; // 전체 pc 데이터

        private PCInfoRecord selectedPcInfoRecord = null;

        private int? reQuest = null; // 폼 불러왔을 때 수정폼이거나 삭제하는 경우 사용 0  : update / 1 : delete

        public SognoPCView(SogNo_ParkDBAccessor DBAcessor, int? selectedParkNo, ParkInfoRecord[] parkList, int? selectedGateNo = null, GateInfoRecord[] gateList = null, int? selectedPCNo = null, int? reQuest = null)
        {
            InitializeComponent();
            this.selectedParkNo = selectedParkNo;
            this.parkList = parkList;
            this.DBAcessor = DBAcessor;
            // 전달된 PC레코드가 있으면 해당 내용으로 텍스트박스를 채움
            if (selectedPCNo != null)
            {
                this.selectedPCNo = selectedPCNo;
                selectedPcInfoRecord = DBAcessor.SelectPCInfo(selectedParkNo.GetValueOrDefault(), selectedGateNo, selectedPCNo)[0];
                fill_textboxes(selectedPcInfoRecord);
            }

            // 콤보박스에 주차장명 삽입
            this.ParkComboBox.DataSource = this.parkList;
            this.ParkComboBox.DisplayMember = ParkInfoFields.ParkName.ToString();
            this.ParkComboBox.ValueMember = ParkInfoFields.ParkNo.ToString();
            this.ParkComboBox.SelectedValue = this.selectedParkNo;

            //게이트 콤보박스에 주차장명 삽입
            this.gateInfoRecords = this.DBAcessor.SelectGateInfo(this.selectedParkNo.GetValueOrDefault());
            this.GateComboBox.DataSource = this.gateInfoRecords;
            this.GateComboBox.DisplayMember = GateInfoFields.GateName.ToString();
            this.GateComboBox.ValueMember = GateInfoFields.GateNo.ToString();

            if (selectedGateNo != null)
            {
                this.selectedGateNo = selectedGateNo;
                this.GateComboBox.SelectedValue = this.selectedGateNo;
            }
            else
            {
                this.GateComboBox.SelectedIndex = -1;
            }

            Init_parlistview();
            RefreshPCList();

            // 전달된 reQuest가 있으면 해당 내용 진행

            if(reQuest == 1) // 삭제 진행
            {
                if (this.selectedPcInfoRecord != null)
                {
                    DialogResult dialog = MessageBox.Show("정말 삭제하시겠습니까?", "삭제확인메시지", MessageBoxButtons.YesNo);
                    if (dialog == DialogResult.Yes)
                    {
                        this.DBAcessor.DeletePcInfo(selectedPcInfoRecord);
                        MessageBox.Show("삭제를 진행하였습니다.");
                        this.Close();
                    }
                    else if (dialog == DialogResult.No)
                    {
                        MessageBox.Show("삭제를 취소하였습니다.");
                    }
                }
                RefreshPCList();
            }
        }

        public void fill_textboxes(PCInfoRecord record) //텍스트 박스를 채우는 함수
        {
            this.ParkComboBox.SelectedValue = record.ParkNo;
            if (record.GateNo != null) this.GateComboBox.SelectedValue = record.GateNo;
            if (record.PCNo != null) this.PCNoTextBox.Text = record.PCNo.ToString();
            if (record.PCIP != null) this.PCIPTextBox.Text = record.PCIP;
            if (record.PCName != null) this.PCNameTextBox.Text = record.PCName;
            if (record.PCLocation != null) this.PCLocationTextBox.Text = record.PCLocation;
            if (record.PCInfo != null) this.PCInfoTextBox.Text = record.PCInfo;
            if (record.PCType != null) this.PCTypeTextBox.Text = record.PCType.ToString();
            if (record.PCRemoteType != null) this.PCRemoteTypeTextBox.Text = record.PCRemoteType.ToString();
            if (record.PCRemoteAddr != null) this.PCRemoteAddrTextBox.Text = record.PCRemoteAddr;
            //if (record.Reserve0 != null) this.Reserve0TextBox.Text = record.Reserve0;

        }

        public void Init_parlistview() // 리스트뷰 초기 설정
        {
            //PCListView.BeginUpdate();   이거 사이에 쓰면 업데이트 다 되고 나서 로드됨 (오류방지용)
            //PCListView.EndUpdate();

            PCListView.View = View.Details;
            PCListView.GridLines = false;
            PCListView.FullRowSelect = true; // 선택 시 라인 전체 선택

        }
        private void RefreshPCList()
        {
            PCListView.Clear(); // 전체 지우기

            PCListView.Columns.Add("", 0);
            PCListView.Columns.Add("Index");
            PCListView.Columns.Add("ParkNo");
            PCListView.Columns.Add("PCNo");
            PCListView.Columns.Add("PCName");

            for (int i=0; i < PCListView.Columns.Count - 1; i++)
            {
                PCListView.Columns[i].Width = -2;
            }

            if (this.selectedPCNo != null)
            {
                pcInfoRecords = DBAcessor.SelectPCInfo(selectedParkNo.GetValueOrDefault(), this.selectedPCNo);
            }
            else
            {
                pcInfoRecords = DBAcessor.SelectPCInfo(selectedParkNo.GetValueOrDefault()); // 레코드에 데이터 넣기
            }

            // 폼에 데이터 뿌리기
            if (pcInfoRecords.Length > 0)
            {
                for (int i = 0; pcInfoRecords.Length > i; i++)
                {
                    ListViewItem a = new ListViewItem();
                    a.SubItems.Add(i.ToString());
                    a.SubItems.Add(pcInfoRecords[i].ParkNo.ToString());
                    a.SubItems.Add(NullCheck(pcInfoRecords[i].PCNo).ToString());
                    a.SubItems.Add(NullCheck(pcInfoRecords[i].PCName).ToString());
                    a.SubItems.Add(NullCheck(pcInfoRecords[i].GateNo).ToString());
                    a.SubItems.Add(NullCheck(pcInfoRecords[i].PCIP).ToString());
                    a.SubItems.Add(NullCheck(pcInfoRecords[i].PCLocation).ToString());
                    a.SubItems.Add(NullCheck(pcInfoRecords[i].PCInfo).ToString());
                    a.SubItems.Add(NullCheck(pcInfoRecords[i].PCType).ToString());
                    a.SubItems.Add(NullCheck(pcInfoRecords[i].PCRemoteType).ToString());
                    a.SubItems.Add(NullCheck(pcInfoRecords[i].PCRemoteAddr).ToString());
                    a.SubItems.Add(NullCheck(pcInfoRecords[i].Reserve0).ToString());
                    if (i % 2 == 0) a.BackColor = Color.FromArgb(70, 70, 70);
                    a.Font = new Font("제주 고딕", 12);
                    PCListView.Items.Add(a);
                }  
            }
        }
        private object NullCheck(object nullitem)
        {
            if (nullitem == null)
            {
                return " ";
            }
            else
            {
                return nullitem;
            }
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            insert_PCInfo();
        }
        public void insert_PCInfo()
        {
            PCInfoRecord insertPCInfoRecord = new PCInfoRecord();
            insertPCInfoRecord.ParkNo = selectedParkNo;
            insertPCInfoRecord.GateNo = 0;
            insertPCInfoRecord.PCIP = "172.16.10.113";
            insertPCInfoRecord.PCName = "PC 이름";
            insertPCInfoRecord.PCLocation = "PC위치";
            insertPCInfoRecord.PCInfo = "PC정보";
            insertPCInfoRecord.PCType = 0; // 피씨 타입, 0: 서버, 1: 부스 , 2: 무인, 3: 모니터링
            insertPCInfoRecord.PCRemoteType = 0; // 피씨 원격 형식 0: VNC, 1: ANY
            insertPCInfoRecord.PCRemoteAddr = "PC 리모팅 주소";
            insertPCInfoRecord.Reserve0 = null;

            // 입력칸이 비어있나 체크하고 해당 내용 입력
            if (ParkComboBox.Items.Count > 0) insertPCInfoRecord.ParkNo = int.Parse(ParkComboBox.SelectedValue.ToString());
            if (GateComboBox.Items.Count > 0) insertPCInfoRecord.GateNo = int.Parse(GateComboBox.SelectedValue.ToString()); 
            if (PCNoTextBox.Text != "") insertPCInfoRecord.PCNo = int.Parse(PCNoTextBox.Text.ToString());
            if (PCIPTextBox.Text != "") insertPCInfoRecord.PCIP = PCIPTextBox.Text.ToString(); 
            if (PCNameTextBox.Text != "") insertPCInfoRecord.PCName = PCNameTextBox.Text.ToString(); 
            if (PCLocationTextBox.Text != "") insertPCInfoRecord.PCLocation = PCLocationTextBox.Text.ToString(); 
            if (PCInfoTextBox.Text != "") insertPCInfoRecord.PCInfo = PCInfoTextBox.Text.ToString(); 
            if (PCTypeTextBox.Text != "") insertPCInfoRecord.PCType = int.Parse(PCTypeTextBox.Text.ToString()); 
            if (PCRemoteTypeTextBox.Text != "") insertPCInfoRecord.PCRemoteType = int.Parse(PCRemoteTypeTextBox.Text.ToString()); 
            if (PCRemoteAddrTextBox.Text != "") insertPCInfoRecord.PCRemoteAddr = PCRemoteAddrTextBox.Text.ToString(); 
            try
            {
                DBAcessor.InsertPcInfo(insertPCInfoRecord);
                MessageBox.Show("추가하였습니다.");
            }
            catch (Exception ex)
            {
                MessageBox.Show("추가에 실패하였습니다.");
                MessageBox.Show(ex.ToString());
            }
            RefreshPCList();
        }

        private void PCListView_SelectedIndexChanged(object sender, EventArgs e) // 리스트뷰에서 하나 골랐을 때
        {
            if (PCListView.SelectedItems.Count > 0)
            {
                int index = int.Parse(PCListView.SelectedItems[0].SubItems[1].Text);
                this.selectedPcInfoRecord = this.pcInfoRecords[index];
                fill_textboxes(this.pcInfoRecords[index]);
            }
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            if (PCListView.SelectedItems.Count > 0)
            {
                try
                {
                    PCInfoRecord UpdateRecord = new PCInfoRecord();
                    UpdateRecord.ParkNo = selectedPcInfoRecord.ParkNo;
                    if(GateComboBox.Items.Count > 0) UpdateRecord.GateNo = int.Parse(GateComboBox.SelectedValue.ToString()); //
                    UpdateRecord.PCNo = int.Parse(PCNoTextBox.Text.ToString());
                    UpdateRecord.PCIP = PCIPTextBox.Text.ToString(); //
                    UpdateRecord.PCName = PCNameTextBox.Text.ToString(); //
                    UpdateRecord.PCLocation = PCLocationTextBox.Text.ToString(); //
                    UpdateRecord.PCInfo = PCInfoTextBox.Text.ToString(); //
                    UpdateRecord.PCType = int.Parse(PCTypeTextBox.Text.ToString()); //
                    UpdateRecord.PCRemoteType = int.Parse(PCRemoteTypeTextBox.Text.ToString()); //
                    UpdateRecord.PCRemoteAddr = PCRemoteAddrTextBox.Text.ToString(); //

                    this.DBAcessor.UpdatePcInfo(UpdateRecord);
                    MessageBox.Show("수정이 진행되었습니다.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("수정에 실패하였습니다.");
                    MessageBox.Show(ex.ToString());
                }
                RefreshPCList();
            }
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            if (this.selectedPcInfoRecord != null)
            {
                DialogResult dialog = MessageBox.Show("정말 삭제하시겠습니까?", "삭제확인메시지", MessageBoxButtons.YesNo);
                if (dialog == DialogResult.Yes)
                {
                    this.DBAcessor.DeletePcInfo(selectedPcInfoRecord);
                    MessageBox.Show("삭제를 진행하였습니다.");
                }
                else if (dialog == DialogResult.No)
                {
                    MessageBox.Show("삭제를 취소하였습니다.");
                }
            }
            RefreshPCList();
        }
    }
}
