﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SognoLib.Park.GUI
{
    public partial class SognoGateView : Form
    {
        private int? selectedParkNo;// 여기 오기전 선택된 주차장
        private int? selectedGateGroupNo; //여기 오기 전 게이트 그룹 넘버
        private int? selectedGateNo; //오기 전 게이트 넘버

        private ParkInfoRecord[] parkList;
        private SogNo_ParkDBAccessor DBAcessor = null; // DB액세서
        private GateGroupInfoRecord[] gateGroupInfoRecords = null; // 전체 게이트그룹 데이터
        private GateGroupInfoRecord selectedGateGroupInfoRecord = null; // 선택된 게이트 그룹데이터
        private GateInfoRecord[] gateInfoRecords = null; // 전체 게이트 데이터
        private GateInfoRecord selectedGateInfoRecord = null; // 선택된 게이트 데이터

        public SognoGateView
            (
                SogNo_ParkDBAccessor DBAcessor,
                int? selectedParkNo,
                ParkInfoRecord[] parkList,
                GateGroupInfoRecord[] gateGroupInfoRecords,
                int? selectedGateGroupNo = null,
                int? selectedGateNo = null,
                int? reQuest = null
            )
        {
            InitializeComponent();
            this.selectedParkNo = selectedParkNo;
            this.DBAcessor = DBAcessor;

            if (gateGroupInfoRecords != null) this.gateGroupInfoRecords = gateGroupInfoRecords;
            if (selectedGateGroupNo != null) this.selectedGateGroupNo = selectedGateGroupNo;
            
            //주차장 콤보박스에 주차장명 전체 삽입
            this.parkList = parkList;
            this.ParkComboBox.DataSource = this.parkList;
            this.ParkComboBox.DisplayMember = ParkInfoFields.ParkName.ToString();
            this.ParkComboBox.ValueMember = ParkInfoFields.ParkNo.ToString();
            this.ParkComboBox.SelectedValue = this.selectedParkNo;

            //게이트그룹 콤보박스에 주차장명 전체 삽입
            this.GateGroupComboBox.DataSource = this.gateGroupInfoRecords;
            this.GateGroupComboBox.DisplayMember = GateGroupInfoFields.GateGroupName.ToString();
            this.GateGroupComboBox.ValueMember = GateGroupInfoFields.GateGroupNo.ToString();
            this.GateGroupComboBox.SelectedValue = -1;

            Init_Gatelistview();
            RefreshGateList();

            if (selectedGateNo != null)
            {
                this.selectedGateNo = selectedGateNo;
                this.selectedGateInfoRecord = DBAcessor.SelectGateInfo(selectedParkNo.GetValueOrDefault(), this.selectedGateGroupNo, this.selectedGateNo)[0];
                fill_textboxes(selectedGateInfoRecord);
            }

            if(reQuest == 1)
            {
                if (this.selectedGateInfoRecord != null)
                {
                    DialogResult dialog = MessageBox.Show("정말 삭제하시겠습니까?", "삭제확인메시지", MessageBoxButtons.YesNo);
                    if (dialog == DialogResult.Yes)
                    {
                        this.DBAcessor.DeleteGateInfo(selectedGateInfoRecord);
                        MessageBox.Show("삭제를 진행하였습니다.");
                    }
                    else if (dialog == DialogResult.No)
                    {
                        MessageBox.Show("삭제를 취소하였습니다.");
                    }
                }
                RefreshGateList();
            }
        }

        private void Init_Gatelistview()
        {
            //PCListView.BeginUpdate();   이거 사이에 쓰면 업데이트 다 되고 나서 로드됨 (오류방지용)
            //PCListView.EndUpdate();

            GateListView.View = View.Details;
            GateListView.GridLines = false;
            GateListView.FullRowSelect = true; // 선택 시 라인 전체 선택
        }
        private void RefreshGateList()
        {
            GateListView.Clear(); // 전체 지우기
            GateListView.Columns.Add("Index");
            GateListView.Columns.Add("GateGroupNo");
            GateListView.Columns.Add("GateNo");
            GateListView.Columns.Add("GateName");

            for (int i = 0; i < GateListView.Columns.Count; i++)
            {
                GateListView.Columns[i].Width = -2;
            }

            if (selectedGateGroupInfoRecord != null)
            {
                gateInfoRecords = DBAcessor.SelectGateInfo(selectedParkNo.GetValueOrDefault(), this.selectedGateGroupNo); // 레코드에 데이터 넣기
            }
            else
            {
                gateInfoRecords = DBAcessor.SelectGateInfo(selectedParkNo.GetValueOrDefault()); // 레코드에 데이터 넣기
            }

            // 폼에 데이터 뿌리기
            if (gateInfoRecords.Length > 0)
            {
                for (int i = 0; gateInfoRecords.Length > i; i++)
                {
                    ListViewItem a = new ListViewItem(i.ToString());
                    a.SubItems.Add(NullCheck(gateInfoRecords[i].GateGroupNo).ToString());
                    a.SubItems.Add(NullCheck(gateInfoRecords[i].GateNo).ToString());
                    a.SubItems.Add(NullCheck(gateInfoRecords[i].GateName).ToString());
                    if (i % 2 == 0) a.BackColor = Color.FromArgb(70, 70, 70);
                    GateListView.Items.Add(a);
                }
            }
        }
        private object NullCheck(object nullitem)
        {
            if (nullitem == null)
            {
                return " ";
            }
            else
            {
                return nullitem;
            }
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            insert_GateInfo();
        }
        private void insert_GateInfo()
        {
            try
            {
                GateInfoRecord insert_GateRecord = new GateInfoRecord
                {
                    ParkNo = selectedParkNo,
                    GateGroupNo = 0,
                    GateNo = 0,
                    GateName = "기본 게이트",
                    GateType = 0,
                    GateCarInType = 0,
                    GateCarOutType = 0
                };

                if (ParkComboBox.Items.Count > 0) insert_GateRecord.ParkNo = int.Parse(ParkComboBox.SelectedValue.ToString());
                if (GateGroupComboBox.Items.Count > 0) insert_GateRecord.GateGroupNo = int.Parse(GateGroupComboBox.SelectedValue.ToString());
                if (GateNoTextBox.Text != "") insert_GateRecord.GateNo = int.Parse(GateNoTextBox.Text.ToString());
                if (GateNameTextBox.Text != "") insert_GateRecord.GateName = GateNameTextBox.Text.ToString();
                if (GateTypeTextBox.Text != "") insert_GateRecord.GateType = int.Parse(GateTypeTextBox.Text.ToString());
                if (GateCarInTypeTextBox.Text != "") insert_GateRecord.GateCarInType = int.Parse(GateCarInTypeTextBox.Text.ToString());
                if (GateCarOutTypeTextBox.Text != "") insert_GateRecord.GateCarOutType = int.Parse(GateCarOutTypeTextBox.Text.ToString());

                this.DBAcessor.InsertGateInfo(insert_GateRecord);
                MessageBox.Show("추가를 진행하였습니다.");
                RefreshGateList();

            }
            catch (Exception ex)
            {
                MessageBox.Show("추가에 실패하였습니다.");
                MessageBox.Show(ex.ToString());
            }
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            try
            {
                GateInfoRecord update_GateRecord = new GateInfoRecord();

                if (ParkComboBox.Items.Count > 0) update_GateRecord.ParkNo = int.Parse(ParkComboBox.SelectedValue.ToString());
                if (GateGroupComboBox.Items.Count > 0) update_GateRecord.GateGroupNo = int.Parse(GateGroupComboBox.SelectedValue.ToString());
                if (GateNoTextBox.Text != "") update_GateRecord.GateNo = int.Parse(GateNoTextBox.Text.ToString());
                if (GateNameTextBox.Text != "") update_GateRecord.GateName = GateNameTextBox.Text.ToString();
                if (GateTypeTextBox.Text != "") update_GateRecord.GateType = int.Parse(GateTypeTextBox.Text.ToString());
                if (GateCarInTypeTextBox.Text != "") update_GateRecord.GateCarInType = int.Parse(GateCarInTypeTextBox.Text.ToString());
                if (GateCarOutTypeTextBox.Text != "") update_GateRecord.GateCarOutType = int.Parse(GateCarOutTypeTextBox.Text.ToString());

                this.DBAcessor.UpdateGateInfo(update_GateRecord);
                MessageBox.Show("수정을 진행하였습니다.");
                RefreshGateList();
            }
            catch (Exception ex)
            {
                MessageBox.Show("수정에 실패하였습니다.");
                MessageBox.Show(ex.ToString());
            }
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            if (this.selectedGateInfoRecord != null)
            {
                DialogResult dialog = MessageBox.Show("정말 삭제하시겠습니까?", "삭제확인메시지", MessageBoxButtons.YesNo);
                if (dialog == DialogResult.Yes)
                {
                    this.DBAcessor.DeleteGateInfo(selectedGateInfoRecord);
                    MessageBox.Show("삭제를 진행하였습니다.");
                }
                else if (dialog == DialogResult.No)
                {
                    MessageBox.Show("삭제를 취소하였습니다.");
                }
            }
            RefreshGateList();
        }
        private void GateListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (GateListView.SelectedItems.Count > 0)
            {
                int index = int.Parse(GateListView.SelectedItems[0].Text);
                this.selectedGateInfoRecord = this.gateInfoRecords[index];
                //주차장 선택되면 그거 따라서 GateGroupComboBox 도 소속 주차장 맞춰서 바뀜
                this.gateGroupInfoRecords = DBAcessor.SelectGateGroupInfo(selectedGateInfoRecord.ParkNo.GetValueOrDefault());
                GateGroupComboBox.DataSource = this.gateGroupInfoRecords;
                GateGroupComboBox.DisplayMember = GateGroupInfoFields.GateGroupName.ToString();
                GateGroupComboBox.ValueMember = GateGroupInfoFields.GateGroupNo.ToString();

                fill_textboxes(this.gateInfoRecords[index]);
            }
        }
        public void fill_textboxes(GateInfoRecord record) //텍스트 박스를 채우는 함수
        {
            this.ParkComboBox.SelectedValue = record.ParkNo;
            this.GateGroupComboBox.SelectedValue = record.GateGroupNo;
            if (record.GateNo != null) this.GateNoTextBox.Text = record.GateNo.ToString();
            if (record.GateName != null) this.GateNameTextBox.Text = record.GateName.ToString();
            if (record.GateType != null) this.GateTypeTextBox.Text = record.GateType.ToString();
            if (record.GateCarInType != null) this.GateCarInTypeTextBox.Text = record.GateCarInType.ToString();
            if (record.GateCarOutType != null) this.GateCarOutTypeTextBox.Text = record.GateCarOutType.ToString();
            //if (record.Reserve0 != null) this.Reserve0.Text = record.Reserve0.ToString();

        }
    }
}
