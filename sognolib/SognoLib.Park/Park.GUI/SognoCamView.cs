﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SognoLib.Park.GUI
{
    public partial class SognoCamView : Form
    {
        private int? selectedParkNo;// 선택된 주차장 번호
        private int? selectedGateNo; // 선택된 게이트 번호
        private int? selectedCamNo; // 선택된 카메라 번호

        private CamInfoRecord selectedCamInfoRecord = null;

        private ParkInfoRecord[] parkList; // 전체 주차장 리스트 데이터
        private GateInfoRecord[] gateInfoRecords = null; // 전체 게이트 데이터
        private CamInfoRecord[] camInfoRecords = null; // 전체 카메라 데이터

        private SogNo_ParkDBAccessor DBAcessor = null; // DB 액세서

        public SognoCamView(
              SogNo_ParkDBAccessor DBAcessor,
                int? selectedParkNo,
                ParkInfoRecord[] parkList,
                int? selectedGateNo = null,
                int? selectedCamNo = null,
                int? reQuest = null
            )
        {
            InitializeComponent();
            this.selectedParkNo = selectedParkNo;
            this.DBAcessor = DBAcessor;

            //주차장 콤보박스에 주차장명 전체 삽입
            this.parkList = parkList;
            this.ParkComboBox.DataSource = this.parkList;
            this.ParkComboBox.DisplayMember = ParkInfoFields.ParkName.ToString();
            this.ParkComboBox.ValueMember = ParkInfoFields.ParkNo.ToString();
            this.ParkComboBox.SelectedValue = this.selectedParkNo;

            //게이트 콤보박스에 주차장 전체의 게이트 데이터 삽입
            this.gateInfoRecords = this.DBAcessor.SelectGateInfo(this.selectedParkNo.GetValueOrDefault());
            this.GateComboBox.DataSource = this.gateInfoRecords;
            this.GateComboBox.DisplayMember = GateInfoFields.GateName.ToString();
            this.GateComboBox.ValueMember = GateInfoFields.GateNo.ToString();
            this.GateComboBox.SelectedIndex = -1;

            if (selectedGateNo != null) this.selectedGateNo = selectedGateNo;
            if (selectedCamNo != null)
            {
                this.selectedCamNo = selectedCamNo;
                selectedCamInfoRecord = DBAcessor.SelectCamInfo(selectedParkNo.GetValueOrDefault(), this.selectedCamNo)[0];
                fill_textboxes(selectedCamInfoRecord);
            }

            if(reQuest == 1)
            {
                if (this.selectedCamInfoRecord != null)
                {
                    DialogResult dialog = MessageBox.Show("정말 삭제하시겠습니까?", "삭제확인메시지", MessageBoxButtons.YesNo);
                    if (dialog == DialogResult.Yes)
                    {
                        this.DBAcessor.DeleteCamInfo(selectedCamInfoRecord);
                        MessageBox.Show("삭제를 진행하였습니다.");
                    }
                    else if (dialog == DialogResult.No)
                    {
                        MessageBox.Show("삭제를 취소하였습니다.");
                    }
                }
                RefreshCamList();
            }

            Init_Camlistview();
            RefreshCamList();
        }

        private void Init_Camlistview()
        {
            //PCListView.BeginUpdate();   이거 사이에 쓰면 업데이트 다 되고 나서 로드됨 (오류방지용)
            //PCListView.EndUpdate();

            CamListView.View = View.Details;
            CamListView.GridLines = false;
            CamListView.FullRowSelect = true; // 선택 시 라인 전체 선택
        }

        private void RefreshCamList()
        {
            CamListView.Clear(); // 전체 지우기

            CamListView.Columns.Add("Index");
            CamListView.Columns.Add("GateNo");
            CamListView.Columns.Add("CamNo");
            CamListView.Columns.Add("CamName");

            for (int i = 0; i < CamListView.Columns.Count; i++)
            {
                CamListView.Columns[i].Width = -2;
            }

            if (selectedCamNo != null)
            {
                camInfoRecords = DBAcessor.SelectCamInfo(selectedParkNo.GetValueOrDefault(), this.selectedCamNo); // 레코드에 데이터 넣기
            }
            else
            {
                camInfoRecords = DBAcessor.SelectCamInfo(selectedParkNo.GetValueOrDefault()); // 레코드에 데이터 넣기
            }

            // 폼에 데이터 뿌리기
            if (camInfoRecords.Length > 0)
            {
                for (int i = 0; camInfoRecords.Length > i; i++)
                {
                    ListViewItem a = new ListViewItem(i.ToString());
                    a.SubItems.Add(NullCheck(camInfoRecords[i].GateNo).ToString());
                    a.SubItems.Add(NullCheck(camInfoRecords[i].CamNo).ToString());
                    a.SubItems.Add(NullCheck(camInfoRecords[i].CamName).ToString());
                    if (i % 2 == 0) a.BackColor = Color.FromArgb(70, 70, 70);
                    CamListView.Items.Add(a);
                }
            }
        }
        private object NullCheck(object nullitem)
        {
            if (nullitem == null)
            {
                return " ";
            }
            else
            {
                return nullitem;
            }
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            insert_CamInfo();
        }

        private void insert_CamInfo()
        {
            try
            {
                CamInfoRecord insert_CamRecord = new CamInfoRecord
                {
                    ParkNo = selectedParkNo,
                    GateNo = 0,
                    CamNo = 0,
                    CamName = "기본 카메라",
                    CamIP = "172.16.10.101",
                    CamType = 0,
                    Target = 0,
                    EMax = 0,
                    EMin = 0,
                    CapCount = 0,
                    FrameRate = "0",
                    RecnArea = "0",
                    RecnType = 0,
                    Reserve0 = null,
                    Reserve1 = null,
                };

                if (ParkComboBox.Items.Count > 0) insert_CamRecord.ParkNo = int.Parse(ParkComboBox.SelectedValue.ToString());
                if (GateComboBox.Text != "") insert_CamRecord.GateNo = int.Parse(GateComboBox.SelectedValue.ToString());
                if (CamNoTextBox.Text != "") insert_CamRecord.CamNo = int.Parse(CamNoTextBox.Text.ToString());
                if (CamNameTextBox.Text != "") insert_CamRecord.CamName = CamNameTextBox.Text.ToString();
                if (CamIPTextBox.Text != "") insert_CamRecord.CamIP = CamIPTextBox.Text.ToString();
                if (CamTypeTextBox.Text != "") insert_CamRecord.CamType = int.Parse(CamTypeTextBox.Text.ToString());
                if (TargetTextBox.Text != "") insert_CamRecord.Target = int.Parse(TargetTextBox.Text.ToString());
                if (EMaxTextBox.Text != "") insert_CamRecord.EMax = int.Parse(EMaxTextBox.Text.ToString());
                if (EMinTextBox.Text != "") insert_CamRecord.EMin = int.Parse(EMinTextBox.Text.ToString());
                if (CapCountTextBox.Text != "") insert_CamRecord.CapCount = int.Parse(CapCountTextBox.Text.ToString());
                if (FrameRateTextBox.Text != "") insert_CamRecord.FrameRate = FrameRateTextBox.Text.ToString();
                if (RecnAreaTextBox.Text != "") insert_CamRecord.RecnArea = RecnAreaTextBox.Text.ToString();
                if (RecnTypeTextBox.Text != "") insert_CamRecord.RecnType = int.Parse(RecnTypeTextBox.Text.ToString());
                //if (Reserve0TextBox.Text != "") insert_CamRecord.Reserve0 = Reserve0TextBox.Text.ToString();
                //if (Reserve1TextBox.Text != "") insert_CamRecord.Reserve1 = Reserve1TextBox.Text.ToString();

                this.DBAcessor.InsertCamInfo(insert_CamRecord);
                MessageBox.Show("추가를 진행하였습니다.");
                RefreshCamList();

            }
            catch (Exception ex)
            {
                MessageBox.Show("추가에 실패하였습니다.");
                MessageBox.Show(ex.ToString());
            }
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            try
            {
                CamInfoRecord update_CamRecord = new CamInfoRecord();

                if (ParkComboBox.Items.Count > 0) update_CamRecord.ParkNo = int.Parse(ParkComboBox.SelectedValue.ToString());
                if (GateComboBox.Text != "") update_CamRecord.GateNo = int.Parse(GateComboBox.SelectedValue.ToString());
                if (CamNoTextBox.Text != "") update_CamRecord.CamNo = int.Parse(CamNoTextBox.Text.ToString());
                if (CamNameTextBox.Text != "") update_CamRecord.CamName = CamNameTextBox.Text.ToString();
                if (CamIPTextBox.Text != "") update_CamRecord.CamIP = CamIPTextBox.Text.ToString();
                if (CamTypeTextBox.Text != "") update_CamRecord.CamType = int.Parse(CamTypeTextBox.Text.ToString());
                if (TargetTextBox.Text != "") update_CamRecord.Target = int.Parse(TargetTextBox.Text.ToString());
                if (EMaxTextBox.Text != "") update_CamRecord.EMax = int.Parse(EMaxTextBox.Text.ToString());
                if (EMinTextBox.Text != "") update_CamRecord.EMin = int.Parse(EMinTextBox.Text.ToString());
                if (CapCountTextBox.Text != "") update_CamRecord.CapCount = int.Parse(CapCountTextBox.Text.ToString());
                if (FrameRateTextBox.Text != "") update_CamRecord.FrameRate = FrameRateTextBox.Text.ToString();
                if (RecnAreaTextBox.Text != "") update_CamRecord.RecnArea = RecnAreaTextBox.Text.ToString();
                if (RecnTypeTextBox.Text != "") update_CamRecord.RecnType = int.Parse(RecnTypeTextBox.Text.ToString());
                //if (Reserve0TextBox.Text != "") update_CamRecord.Reserve0 = Reserve0TextBox.Text.ToString();
                //if (Reserve1TextBox.Text != "") update_CamRecord.Reserve1 = Reserve1TextBox.Text.ToString();

                this.DBAcessor.UpdateCamInfo(update_CamRecord);
                MessageBox.Show("수정을 진행하였습니다.");
                RefreshCamList();
            }
            catch (Exception ex)
            {
                MessageBox.Show("수정에 실패하였습니다.");
                MessageBox.Show(ex.ToString());
            }
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            if (this.selectedCamInfoRecord != null)
            {
                DialogResult dialog = MessageBox.Show("정말 삭제하시겠습니까?", "삭제확인메시지", MessageBoxButtons.YesNo);
                if (dialog == DialogResult.Yes)
                {
                    this.DBAcessor.DeleteCamInfo(selectedCamInfoRecord);
                    MessageBox.Show("삭제를 진행하였습니다.");
                }
                else if (dialog == DialogResult.No)
                {
                    MessageBox.Show("삭제를 취소하였습니다.");
                }
            }
            RefreshCamList();
        }

        private void CamListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (CamListView.SelectedItems.Count > 0)
            {
                int index = int.Parse(CamListView.SelectedItems[0].Text);
                this.selectedCamInfoRecord = this.camInfoRecords[index];
                fill_textboxes(this.camInfoRecords[index]);
            }
        }

        public void fill_textboxes(CamInfoRecord record) //텍스트 박스를 채우는 함수
        {
            this.ParkComboBox.SelectedValue = record.ParkNo;
            this.GateComboBox.SelectedValue = record.GateNo;
            if (record.CamNo != null) this.CamNoTextBox.Text = record.CamNo.ToString();
            if (record.CamName != null) this.CamNameTextBox.Text = record.CamName.ToString();
            if (record.CamIP != null) this.CamIPTextBox.Text = record.CamIP.ToString();
            if (record.CamType != null) this.CamTypeTextBox.Text = record.CamType.ToString();
            if (record.Target != null) this.TargetTextBox.Text = record.Target.ToString();
            if (record.EMax != null) this.EMaxTextBox.Text = record.EMax.ToString();
            if (record.EMin != null) this.EMinTextBox.Text = record.EMin.ToString();
            if (record.CapCount != null) this.CapCountTextBox.Text = record.CapCount.ToString();
            if (record.FrameRate != null) this.FrameRateTextBox.Text = record.FrameRate.ToString();
            if (record.RecnArea != null) this.RecnAreaTextBox.Text = record.RecnArea.ToString();
            if (record.RecnType != null) this.RecnTypeTextBox.Text = record.RecnType.ToString();
            //if (record.Reserve0 != null) this.Reserve0TextBox.Text = record.Reserve0.ToString();
            //if (record.Reserve1 != null) this.Reserve1TextBox.Text = record.Reserve1.ToString();
        }
    }
}
