﻿namespace SognoLib.Park.GUI
{
    partial class SognoCamView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CapCountTextBox = new System.Windows.Forms.TextBox();
            this.EMinTextBox = new System.Windows.Forms.TextBox();
            this.EMaxTextBox = new System.Windows.Forms.TextBox();
            this.TargetTextBox = new System.Windows.Forms.TextBox();
            this.CamTypeTextBox = new System.Windows.Forms.TextBox();
            this.CamIPTextBox = new System.Windows.Forms.TextBox();
            this.CamNameTextBox = new System.Windows.Forms.TextBox();
            this.CamNoTextBox = new System.Windows.Forms.TextBox();
            this.Reserve0TxtBox = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ParkComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.deleteButton = new System.Windows.Forms.Button();
            this.updateButton = new System.Windows.Forms.Button();
            this.CamListView = new System.Windows.Forms.ListView();
            this.addButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.GateComboBox = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.FrameRateTextBox = new System.Windows.Forms.TextBox();
            this.RecnAreaTextBox = new System.Windows.Forms.TextBox();
            this.RecnTypeTextBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // CapCountTextBox
            // 
            this.CapCountTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CapCountTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.CapCountTextBox.Location = new System.Drawing.Point(179, 419);
            this.CapCountTextBox.Name = "CapCountTextBox";
            this.CapCountTextBox.Size = new System.Drawing.Size(273, 36);
            this.CapCountTextBox.TabIndex = 42;
            // 
            // EMinTextBox
            // 
            this.EMinTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.EMinTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.EMinTextBox.Location = new System.Drawing.Point(179, 373);
            this.EMinTextBox.Name = "EMinTextBox";
            this.EMinTextBox.Size = new System.Drawing.Size(273, 36);
            this.EMinTextBox.TabIndex = 41;
            // 
            // EMaxTextBox
            // 
            this.EMaxTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.EMaxTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.EMaxTextBox.Location = new System.Drawing.Point(179, 327);
            this.EMaxTextBox.Name = "EMaxTextBox";
            this.EMaxTextBox.Size = new System.Drawing.Size(273, 36);
            this.EMaxTextBox.TabIndex = 40;
            // 
            // TargetTextBox
            // 
            this.TargetTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TargetTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.TargetTextBox.Location = new System.Drawing.Point(179, 281);
            this.TargetTextBox.Name = "TargetTextBox";
            this.TargetTextBox.Size = new System.Drawing.Size(273, 36);
            this.TargetTextBox.TabIndex = 39;
            // 
            // CamTypeTextBox
            // 
            this.CamTypeTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CamTypeTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.CamTypeTextBox.Location = new System.Drawing.Point(179, 235);
            this.CamTypeTextBox.Name = "CamTypeTextBox";
            this.CamTypeTextBox.Size = new System.Drawing.Size(273, 36);
            this.CamTypeTextBox.TabIndex = 38;
            // 
            // CamIPTextBox
            // 
            this.CamIPTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CamIPTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.CamIPTextBox.Location = new System.Drawing.Point(179, 189);
            this.CamIPTextBox.Name = "CamIPTextBox";
            this.CamIPTextBox.Size = new System.Drawing.Size(273, 36);
            this.CamIPTextBox.TabIndex = 37;
            // 
            // CamNameTextBox
            // 
            this.CamNameTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CamNameTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.CamNameTextBox.Location = new System.Drawing.Point(179, 143);
            this.CamNameTextBox.Name = "CamNameTextBox";
            this.CamNameTextBox.Size = new System.Drawing.Size(273, 36);
            this.CamNameTextBox.TabIndex = 36;
            // 
            // CamNoTextBox
            // 
            this.CamNoTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CamNoTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.CamNoTextBox.Location = new System.Drawing.Point(179, 97);
            this.CamNoTextBox.Name = "CamNoTextBox";
            this.CamNoTextBox.Size = new System.Drawing.Size(273, 36);
            this.CamNoTextBox.TabIndex = 35;
            // 
            // Reserve0TxtBox
            // 
            this.Reserve0TxtBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Reserve0TxtBox.AutoSize = true;
            this.Reserve0TxtBox.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Reserve0TxtBox.ForeColor = System.Drawing.SystemColors.Control;
            this.Reserve0TxtBox.Location = new System.Drawing.Point(6, 426);
            this.Reserve0TxtBox.Name = "Reserve0TxtBox";
            this.Reserve0TxtBox.Size = new System.Drawing.Size(155, 21);
            this.Reserve0TxtBox.TabIndex = 32;
            this.Reserve0TxtBox.Text = "이미지 캡쳐 개수";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(28, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "소속 주차장";
            // 
            // ParkComboBox
            // 
            this.ParkComboBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ParkComboBox.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ParkComboBox.FormattingEnabled = true;
            this.ParkComboBox.Location = new System.Drawing.Point(179, 7);
            this.ParkComboBox.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.ParkComboBox.Name = "ParkComboBox";
            this.ParkComboBox.Size = new System.Drawing.Size(273, 32);
            this.ParkComboBox.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.SystemColors.Control;
            this.label4.Location = new System.Drawing.Point(28, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 21);
            this.label4.TabIndex = 24;
            this.label4.Text = "소속 게이트";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.SystemColors.Control;
            this.label5.Location = new System.Drawing.Point(28, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 21);
            this.label5.TabIndex = 25;
            this.label5.Text = "카메라 번호";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.SystemColors.Control;
            this.label6.Location = new System.Drawing.Point(41, 150);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 21);
            this.label6.TabIndex = 26;
            this.label6.Text = "카메라명";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.ForeColor = System.Drawing.SystemColors.Control;
            this.label7.Location = new System.Drawing.Point(42, 196);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 21);
            this.label7.TabIndex = 27;
            this.label7.Text = "카메라IP";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.ForeColor = System.Drawing.SystemColors.Control;
            this.label8.Location = new System.Drawing.Point(28, 242);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 21);
            this.label8.TabIndex = 28;
            this.label8.Text = "카메라 타입";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.ForeColor = System.Drawing.SystemColors.Control;
            this.label9.Location = new System.Drawing.Point(38, 288);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 21);
            this.label9.TabIndex = 29;
            this.label9.Text = "밝기 기준";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.ForeColor = System.Drawing.SystemColors.Control;
            this.label10.Location = new System.Drawing.Point(38, 334);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(92, 21);
            this.label10.TabIndex = 30;
            this.label10.Text = "밝기 최대";
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.ForeColor = System.Drawing.SystemColors.Control;
            this.label11.Location = new System.Drawing.Point(38, 380);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 21);
            this.label11.TabIndex = 31;
            this.label11.Text = "밝기 최소";
            // 
            // deleteButton
            // 
            this.deleteButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(49)))), ((int)(((byte)(83)))));
            this.deleteButton.FlatAppearance.BorderSize = 0;
            this.deleteButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deleteButton.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.deleteButton.ForeColor = System.Drawing.SystemColors.Control;
            this.deleteButton.Location = new System.Drawing.Point(813, 637);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(112, 45);
            this.deleteButton.TabIndex = 17;
            this.deleteButton.Text = "삭제";
            this.deleteButton.UseVisualStyleBackColor = false;
            this.deleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // updateButton
            // 
            this.updateButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(84)))), ((int)(((byte)(136)))));
            this.updateButton.FlatAppearance.BorderSize = 0;
            this.updateButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.updateButton.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.updateButton.ForeColor = System.Drawing.SystemColors.Control;
            this.updateButton.Location = new System.Drawing.Point(582, 637);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(112, 45);
            this.updateButton.TabIndex = 16;
            this.updateButton.Text = "수정";
            this.updateButton.UseVisualStyleBackColor = false;
            this.updateButton.Click += new System.EventHandler(this.UpdateButton_Click);
            // 
            // CamListView
            // 
            this.CamListView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.CamListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.CamListView.ForeColor = System.Drawing.SystemColors.Control;
            this.CamListView.HideSelection = false;
            this.CamListView.Location = new System.Drawing.Point(12, 12);
            this.CamListView.Name = "CamListView";
            this.CamListView.Size = new System.Drawing.Size(444, 606);
            this.CamListView.TabIndex = 15;
            this.CamListView.UseCompatibleStateImageBehavior = false;
            this.CamListView.SelectedIndexChanged += new System.EventHandler(this.CamListView_SelectedIndexChanged);
            // 
            // addButton
            // 
            this.addButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(124)))), ((int)(((byte)(113)))));
            this.addButton.FlatAppearance.BorderSize = 0;
            this.addButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addButton.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.addButton.ForeColor = System.Drawing.SystemColors.Control;
            this.addButton.Location = new System.Drawing.Point(464, 637);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(112, 45);
            this.addButton.TabIndex = 14;
            this.addButton.Text = "추가";
            this.addButton.UseVisualStyleBackColor = false;
            this.addButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.46789F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.53211F));
            this.tableLayoutPanel1.Controls.Add(this.EMinTextBox, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.EMaxTextBox, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.TargetTextBox, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.CamTypeTextBox, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.CamIPTextBox, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.CamNameTextBox, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.CamNoTextBox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ParkComboBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.GateComboBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.CapCountTextBox, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.Reserve0TxtBox, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.label14, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.FrameRateTextBox, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.RecnAreaTextBox, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.RecnTypeTextBox, 1, 12);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(462, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 13;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(463, 606);
            this.tableLayoutPanel1.TabIndex = 13;
            // 
            // GateComboBox
            // 
            this.GateComboBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.GateComboBox.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.GateComboBox.FormattingEnabled = true;
            this.GateComboBox.Location = new System.Drawing.Point(179, 53);
            this.GateComboBox.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.GateComboBox.Name = "GateComboBox";
            this.GateComboBox.Size = new System.Drawing.Size(273, 32);
            this.GateComboBox.TabIndex = 43;
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.ForeColor = System.Drawing.SystemColors.Control;
            this.label12.Location = new System.Drawing.Point(38, 518);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(92, 21);
            this.label12.TabIndex = 45;
            this.label12.Text = "인식 영역";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.SystemColors.Control;
            this.label3.Location = new System.Drawing.Point(19, 472);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(130, 21);
            this.label3.TabIndex = 44;
            this.label3.Text = "카메라 프레임";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.ForeColor = System.Drawing.SystemColors.Control;
            this.label14.Location = new System.Drawing.Point(33, 568);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(102, 21);
            this.label14.TabIndex = 47;
            this.label14.Text = "RecnType";
            // 
            // FrameRateTextBox
            // 
            this.FrameRateTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.FrameRateTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.FrameRateTextBox.Location = new System.Drawing.Point(179, 465);
            this.FrameRateTextBox.Name = "FrameRateTextBox";
            this.FrameRateTextBox.Size = new System.Drawing.Size(273, 36);
            this.FrameRateTextBox.TabIndex = 49;
            // 
            // RecnAreaTextBox
            // 
            this.RecnAreaTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.RecnAreaTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.RecnAreaTextBox.Location = new System.Drawing.Point(179, 511);
            this.RecnAreaTextBox.Name = "RecnAreaTextBox";
            this.RecnAreaTextBox.Size = new System.Drawing.Size(273, 36);
            this.RecnAreaTextBox.TabIndex = 50;
            // 
            // RecnTypeTextBox
            // 
            this.RecnTypeTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.RecnTypeTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.RecnTypeTextBox.Location = new System.Drawing.Point(179, 561);
            this.RecnTypeTextBox.Name = "RecnTypeTextBox";
            this.RecnTypeTextBox.Size = new System.Drawing.Size(273, 36);
            this.RecnTypeTextBox.TabIndex = 51;
            // 
            // SognoCamView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.ClientSize = new System.Drawing.Size(937, 694);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.updateButton);
            this.Controls.Add(this.CamListView);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "SognoCamView";
            this.Text = "SognoCameraView";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox CapCountTextBox;
        private System.Windows.Forms.TextBox EMinTextBox;
        private System.Windows.Forms.TextBox EMaxTextBox;
        private System.Windows.Forms.TextBox TargetTextBox;
        private System.Windows.Forms.TextBox CamTypeTextBox;
        private System.Windows.Forms.TextBox CamIPTextBox;
        private System.Windows.Forms.TextBox CamNameTextBox;
        private System.Windows.Forms.TextBox CamNoTextBox;
        private System.Windows.Forms.Label Reserve0TxtBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ParkComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.ListView CamListView;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ComboBox GateComboBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox FrameRateTextBox;
        private System.Windows.Forms.TextBox RecnAreaTextBox;
        private System.Windows.Forms.TextBox RecnTypeTextBox;
    }
}