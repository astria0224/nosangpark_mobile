﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SognoLib.Park;
using SognoLib.Core;


namespace SognoLib.Park.GUI
{
    public partial class ParkViewer : Form
    {
        public ParkInfoRecord[] ParklistData = null;
        public ParkInfoRecord selectedParkInfoRecord;
        private SogNo_ParkDBAccessor DBAcessor = null;
        private SogNo_DBInfo Select_DBInfo = new SogNo_DBInfo();
        public string ListViewSelectedParkNo;
        public ParkViewer()
        {
            InitializeComponent();
            //this.Select_DBInfo.DBIP = "127.0.0.1";
            this.Select_DBInfo.DBIP = "218.158.210.43";
            this.Select_DBInfo.DBPort = 8011;
            this.DBAcessor = new SogNo_ParkDBAccessor(Select_DBInfo.DBIP, Select_DBInfo.DBPort, Select_DBInfo.DBName);

            Init_parlistview();
            RefreshParkList();
        }

        public void Init_parlistview() // 리스트뷰 초기 설정
        {
//            parkListView.BeginUpdate();   이거 사이에 쓰면 업데이트 다 되고 나서 로드됨 (오류방지용)
//            parkListView.EndUpdate();

            parkListView.View = View.Details;
            parkListView.FullRowSelect = true; // 선택 시 라인 전체 선택
        }

        public void RefreshParkList()
        {

            parkListView.Clear(); // 전체 지우기

            parkListView.Columns.Add("주차장번호");
            parkListView.Columns.Add("이름");
            parkListView.Columns.Add("운영자");
            parkListView.Columns.Add("사업자등록번호");
            parkListView.Columns.Add("운영자전화번호");
            parkListView.Columns.Add("주소");
            parkListView.Columns.Add("주차장전화번호");
            parkListView.Columns.Add("IP");
            parkListView.Columns.Add("도메인");
            parkListView.Columns.Add("포트");
            parkListView.Columns.Add("기본요금체계");

            for (int i = 0; i < parkListView.Columns.Count; i++)
            {
                parkListView.Columns[i].Width = -2;
            }

            ParklistData = DBAcessor.SelectParkInfo(); // 레코드에 데이터 넣기
            int n = ParklistData.Count();

            // 폼에 데이터 뿌리기
            for (int i = 0; n > i; i++)
            {
                ListViewItem a = new ListViewItem(ParklistData[i].ParkNo.ToString());
                a.SubItems.Add(NullCheck(ParklistData[i].ParkName).ToString());
                a.SubItems.Add(NullCheck(ParklistData[i].ParkOwner).ToString());
                a.SubItems.Add(NullCheck(ParklistData[i].ParkRegNum).ToString());
                a.SubItems.Add(NullCheck(ParklistData[i].ParkPhone).ToString());
                a.SubItems.Add(NullCheck(ParklistData[i].ParkAddr).ToString());
                a.SubItems.Add(NullCheck(ParklistData[i].ParkNote).ToString());
                a.SubItems.Add(NullCheck(ParklistData[i].ParkIPAddr).ToString());
                a.SubItems.Add(NullCheck(ParklistData[i].ParkDomain).ToString());
                a.SubItems.Add(NullCheck(ParklistData[i].ParkPort).ToString());
                a.SubItems.Add(NullCheck(ParklistData[i].DefaultTrfNo).ToString());
                a.Font = new Font("제주 고딕", 12);
                if (i % 2 == 0) a.BackColor = Color.FromArgb(70, 70, 70);
                parkListView.Items.Add(a);
            }
        }

        private object NullCheck(object nullitem)
        {
            if (nullitem == null)
            {
                return " ";
            }
            else
            {
                return nullitem;
            }
        }
        private void Advance_View_Click(object sender, EventArgs e) // 수정버튼 클릭
        {
            // 선택되어있는 행의 데이터를 보내줘서 해당 화면에 뿌릴 수 있게 해줌
            if (selectedParkInfoRecord != null)
            {
                ParkUpdate_form advance = new ParkUpdate_form(this, this.DBAcessor, selectedParkInfoRecord);
                advance.Show();
            }
        }
        private void ParkListView_SelectedIndexChanged(object sender, EventArgs e) // 셀 선택될 때 해당 번호 가져오기
        {
            int indexnum;
            indexnum = parkListView.FocusedItem.Index; //선택된 아이템 인덱스 번호 얻기
            if (indexnum >= 0)
            {
                //ListViewSelectedParkNo = parkListView.Items[indexnum].SubItems[0].Text; //인덱스 번호의 n번째 아이템 얻기
                selectedParkInfoRecord = ParklistData[indexnum];
            }
        }

        private void ParkDeleteBt_Click(object sender, EventArgs e)
        {
            if(selectedParkInfoRecord != null)
            {
                DialogResult dialog = MessageBox.Show("정말 삭제하시겠습니까?", "삭제확인메시지", MessageBoxButtons.YesNo);
                if (dialog == DialogResult.Yes)
                {
                    this.DBAcessor.DeleteParkInfo(selectedParkInfoRecord);
                    MessageBox.Show("삭제를 진행하였습니다.");
                    RefreshParkList();
                }
                else if (dialog == DialogResult.No)
                {
                    MessageBox.Show("삭제를 취소하였습니다.");
                }
            }
        }

        private void NewParkAddButton_Click(object sender, EventArgs e)
        {
            ParkAddform advance = new ParkAddform(this,this.DBAcessor);
            advance.ShowDialog();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            RefreshParkList();
        }

        private void ParkListView_DoubleClick(object sender, EventArgs e)
        {

        }

        private void ParkListView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (selectedParkInfoRecord != null)
            {
                SognoTreeView gateGroupView = new SognoTreeView(this.DBAcessor, selectedParkInfoRecord);
                gateGroupView.Show();
            }
        }
    }
}
