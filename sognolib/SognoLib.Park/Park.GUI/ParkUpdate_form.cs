﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SognoLib.Core;
using SognoLib.Park;

namespace SognoLib.Park.GUI
{
    public partial class ParkUpdate_form : Form
    {
        ParkInfoRecord parkInfo;
        SogNo_ParkDBAccessor dbAccessor;
        ParkViewer parkViewer;
        public ParkUpdate_form(ParkViewer parkViewer, SogNo_ParkDBAccessor dbAccessor, ParkInfoRecord parkInfo)
        {
            InitializeComponent();
            this.parkInfo = parkInfo;
            this.dbAccessor = dbAccessor;
            this.parkViewer = parkViewer;
            if (parkInfo != null)
            {

                // 해당하는 주차장 정보를 가져옵니다.
                if (parkInfo.ParkNo != null) parkNoTxtBox.Text = parkInfo.ParkNo.ToString();
                if (parkInfo.ParkName != null) parkNameTxtBox.Text = parkInfo.ParkName.ToString();
                if (parkInfo.ParkOwner != null) parkOwnerTxtBox.Text = parkInfo.ParkOwner.ToString();
                if (parkInfo.ParkRegNum != null) parkRegNumTxtBox.Text = parkInfo.ParkRegNum.ToString();
                if (parkInfo.ParkPhone != null) parkPhoneTxtBox.Text = parkInfo.ParkPhone.ToString();
                if (parkInfo.ParkAddr != null) parkAddrTxtBox.Text = parkInfo.ParkAddr.ToString();
                if (parkInfo.ParkNote != null) parkNoteTxtBox.Text = parkInfo.ParkNote.ToString();
                if (parkInfo.ParkIPAddr != null) parkIPAddrTxtBox.Text = parkInfo.ParkIPAddr.ToString();
                if (parkInfo.ParkDomain != null) parkDomainTxtBox.Text = parkInfo.ParkDomain.ToString();
                if (parkInfo.ParkPort != null) parkPortTxtBox.Text = parkInfo.ParkPort.ToString();
                if (parkInfo.DefaultTrfNo != null) parkDefaultTrfNoTxtBox.Text = parkInfo.DefaultTrfNo.ToString();
                //if (parkInfo.Reserve0 != null) Reserve0TxtBox.Text = parkInfo.Reserve0.ToString();
            }
            else
            {
                MessageBox.Show("불러오는 과정에서 실패하였습니다.");
                this.Close();
            }
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            try
            {
                ParkInfoRecord UpdateRecord = new ParkInfoRecord
                {
                    ParkNo = parkInfo.ParkNo,
                    ParkName = parkNameTxtBox.Text, //주차장명
                    ParkOwner = parkOwnerTxtBox.Text, //운영자
                    ParkRegNum = parkRegNumTxtBox.Text, //사업자등록번호
                    ParkPhone = parkPhoneTxtBox.Text, //전화번호
                    ParkAddr = parkAddrTxtBox.Text, //주차장 주소
                    ParkNote = parkNoteTxtBox.Text, //주차장 전화번호
                    ParkIPAddr = parkIPAddrTxtBox.Text, //주차장 메인IP
                    ParkDomain = parkDomainTxtBox.Text //주차장 도메인
                };

                if (parkPortTxtBox.Text != "") UpdateRecord.ParkPort = Int32.Parse(parkPortTxtBox.Text); //주차장 포트
                if (parkDefaultTrfNoTxtBox.Text != "") UpdateRecord.DefaultTrfNo = Int32.Parse(parkDefaultTrfNoTxtBox.Text); //기본 요금체계 번호
                //UpdateRecord.Reserve0 = Reserve0TxtBox.Text.ToString(); // 보조 데이터

                this.dbAccessor.UpdateParkInfo(UpdateRecord);

                MessageBox.Show("수정이 진행되었습니다.");
                parkViewer.RefreshParkList();
                this.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show("수정에 실패하였습니다.");
                MessageBox.Show(ex.ToString());

            }
        }

        private void Deletebutton_Click(object sender, EventArgs e)
        {
            if (this.parkInfo != null)
            {
                DialogResult dialog = MessageBox.Show("정말 삭제하시겠습니까?", "삭제확인메시지", MessageBoxButtons.YesNo);
                if (dialog == DialogResult.Yes)
                {
                    this.dbAccessor.DeleteParkInfo(parkInfo);
                    MessageBox.Show("삭제를 진행하였습니다.");
                    this.Close();
                }
                else if (dialog == DialogResult.No)
                {
                    MessageBox.Show("삭제를 취소하였습니다.");
                }
            }
        }
    }
}
