﻿namespace SognoLib.Park.GUI
{
    partial class SognoBarView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BarListView = new System.Windows.Forms.ListView();
            this.BarCountTypeTextBox = new System.Windows.Forms.TextBox();
            this.BarConnTypeTextBox = new System.Windows.Forms.TextBox();
            this.BarACTimeTextBox = new System.Windows.Forms.TextBox();
            this.BarNameTextBox = new System.Windows.Forms.TextBox();
            this.BarPortTextBox = new System.Windows.Forms.TextBox();
            this.BarIPTextBox = new System.Windows.Forms.TextBox();
            this.BarNoTextBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.ParkComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.GateComboBox = new System.Windows.Forms.ComboBox();
            this.BarOpenCmdTextBox = new System.Windows.Forms.TextBox();
            this.Reserve0TxtBox = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.BarCloseCmdTextBox = new System.Windows.Forms.TextBox();
            this.BarLockCmdTextBox = new System.Windows.Forms.TextBox();
            this.BarOneWayTextBox = new System.Windows.Forms.TextBox();
            this.BarAOpenTextBox = new System.Windows.Forms.TextBox();
            this.addButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.updateButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BarListView
            // 
            this.BarListView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.BarListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.BarListView.ForeColor = System.Drawing.SystemColors.Window;
            this.BarListView.HideSelection = false;
            this.BarListView.Location = new System.Drawing.Point(12, 12);
            this.BarListView.Name = "BarListView";
            this.BarListView.Size = new System.Drawing.Size(414, 606);
            this.BarListView.TabIndex = 20;
            this.BarListView.UseCompatibleStateImageBehavior = false;
            this.BarListView.SelectedIndexChanged += new System.EventHandler(this.BarListView_SelectedIndexChanged);
            // 
            // BarCountTypeTextBox
            // 
            this.BarCountTypeTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BarCountTypeTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.BarCountTypeTextBox.Location = new System.Drawing.Point(260, 347);
            this.BarCountTypeTextBox.Name = "BarCountTypeTextBox";
            this.BarCountTypeTextBox.Size = new System.Drawing.Size(271, 36);
            this.BarCountTypeTextBox.TabIndex = 41;
            // 
            // BarConnTypeTextBox
            // 
            this.BarConnTypeTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BarConnTypeTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.BarConnTypeTextBox.Location = new System.Drawing.Point(260, 304);
            this.BarConnTypeTextBox.Name = "BarConnTypeTextBox";
            this.BarConnTypeTextBox.Size = new System.Drawing.Size(271, 36);
            this.BarConnTypeTextBox.TabIndex = 40;
            // 
            // BarACTimeTextBox
            // 
            this.BarACTimeTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BarACTimeTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.BarACTimeTextBox.Location = new System.Drawing.Point(260, 261);
            this.BarACTimeTextBox.Name = "BarACTimeTextBox";
            this.BarACTimeTextBox.Size = new System.Drawing.Size(271, 36);
            this.BarACTimeTextBox.TabIndex = 39;
            // 
            // BarNameTextBox
            // 
            this.BarNameTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BarNameTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.BarNameTextBox.Location = new System.Drawing.Point(260, 218);
            this.BarNameTextBox.Name = "BarNameTextBox";
            this.BarNameTextBox.Size = new System.Drawing.Size(271, 36);
            this.BarNameTextBox.TabIndex = 38;
            // 
            // BarPortTextBox
            // 
            this.BarPortTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BarPortTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.BarPortTextBox.Location = new System.Drawing.Point(260, 175);
            this.BarPortTextBox.Name = "BarPortTextBox";
            this.BarPortTextBox.Size = new System.Drawing.Size(271, 36);
            this.BarPortTextBox.TabIndex = 37;
            // 
            // BarIPTextBox
            // 
            this.BarIPTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BarIPTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.BarIPTextBox.Location = new System.Drawing.Point(260, 132);
            this.BarIPTextBox.Name = "BarIPTextBox";
            this.BarIPTextBox.Size = new System.Drawing.Size(271, 36);
            this.BarIPTextBox.TabIndex = 36;
            // 
            // BarNoTextBox
            // 
            this.BarNoTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BarNoTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.BarNoTextBox.Location = new System.Drawing.Point(260, 89);
            this.BarNoTextBox.Name = "BarNoTextBox";
            this.BarNoTextBox.Size = new System.Drawing.Size(271, 36);
            this.BarNoTextBox.TabIndex = 35;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.12734F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.87266F));
            this.tableLayoutPanel1.Controls.Add(this.BarCountTypeTextBox, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.BarConnTypeTextBox, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.BarACTimeTextBox, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.BarNameTextBox, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.BarPortTextBox, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.BarIPTextBox, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.BarNoTextBox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ParkComboBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.GateComboBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.BarOpenCmdTextBox, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.Reserve0TxtBox, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.label14, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.label13, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.BarCloseCmdTextBox, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.BarLockCmdTextBox, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.BarOneWayTextBox, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.BarAOpenTextBox, 1, 13);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(432, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 14;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(534, 606);
            this.tableLayoutPanel1.TabIndex = 18;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(73, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "소속 주차장";
            // 
            // ParkComboBox
            // 
            this.ParkComboBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ParkComboBox.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ParkComboBox.FormattingEnabled = true;
            this.ParkComboBox.Location = new System.Drawing.Point(260, 7);
            this.ParkComboBox.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.ParkComboBox.Name = "ParkComboBox";
            this.ParkComboBox.Size = new System.Drawing.Size(271, 32);
            this.ParkComboBox.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.SystemColors.Control;
            this.label4.Location = new System.Drawing.Point(73, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 21);
            this.label4.TabIndex = 24;
            this.label4.Text = "소속 게이트";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.SystemColors.Control;
            this.label5.Location = new System.Drawing.Point(73, 97);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 21);
            this.label5.TabIndex = 25;
            this.label5.Text = "차단기 번호";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.SystemColors.Control;
            this.label6.Location = new System.Drawing.Point(63, 140);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(130, 21);
            this.label6.TabIndex = 26;
            this.label6.Text = "차단기 아이피";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.ForeColor = System.Drawing.SystemColors.Control;
            this.label7.Location = new System.Drawing.Point(73, 183);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(111, 21);
            this.label7.TabIndex = 27;
            this.label7.Text = "차단기 포트";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.ForeColor = System.Drawing.SystemColors.Control;
            this.label8.Location = new System.Drawing.Point(73, 226);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 21);
            this.label8.TabIndex = 28;
            this.label8.Text = "차단기 이름";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.ForeColor = System.Drawing.SystemColors.Control;
            this.label9.Location = new System.Drawing.Point(60, 269);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(136, 21);
            this.label9.TabIndex = 29;
            this.label9.Text = "자동 닫힘 시간";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.ForeColor = System.Drawing.SystemColors.Control;
            this.label10.Location = new System.Drawing.Point(51, 312);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(155, 21);
            this.label10.TabIndex = 30;
            this.label10.Text = "차단기 연결 형태";
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.ForeColor = System.Drawing.SystemColors.Control;
            this.label11.Location = new System.Drawing.Point(41, 355);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(174, 21);
            this.label11.TabIndex = 31;
            this.label11.Text = "차단기 카운터 형태";
            // 
            // GateComboBox
            // 
            this.GateComboBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.GateComboBox.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.GateComboBox.FormattingEnabled = true;
            this.GateComboBox.Location = new System.Drawing.Point(260, 50);
            this.GateComboBox.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.GateComboBox.Name = "GateComboBox";
            this.GateComboBox.Size = new System.Drawing.Size(271, 32);
            this.GateComboBox.TabIndex = 43;
            // 
            // BarOpenCmdTextBox
            // 
            this.BarOpenCmdTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BarOpenCmdTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.BarOpenCmdTextBox.Location = new System.Drawing.Point(260, 390);
            this.BarOpenCmdTextBox.Name = "BarOpenCmdTextBox";
            this.BarOpenCmdTextBox.Size = new System.Drawing.Size(271, 36);
            this.BarOpenCmdTextBox.TabIndex = 42;
            // 
            // Reserve0TxtBox
            // 
            this.Reserve0TxtBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Reserve0TxtBox.AutoSize = true;
            this.Reserve0TxtBox.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Reserve0TxtBox.ForeColor = System.Drawing.SystemColors.Control;
            this.Reserve0TxtBox.Location = new System.Drawing.Point(19, 398);
            this.Reserve0TxtBox.Name = "Reserve0TxtBox";
            this.Reserve0TxtBox.Size = new System.Drawing.Size(218, 21);
            this.Reserve0TxtBox.TabIndex = 32;
            this.Reserve0TxtBox.Text = "접점시 차단기 열기 명령";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.ForeColor = System.Drawing.SystemColors.Control;
            this.label12.Location = new System.Drawing.Point(19, 484);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(218, 21);
            this.label12.TabIndex = 45;
            this.label12.Text = "접점시 차단기 고정 명령";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.SystemColors.Control;
            this.label3.Location = new System.Drawing.Point(19, 441);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(218, 21);
            this.label3.TabIndex = 44;
            this.label3.Text = "접점시 차단기 닫기 명령";
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.ForeColor = System.Drawing.SystemColors.Control;
            this.label14.Location = new System.Drawing.Point(65, 527);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(126, 21);
            this.label14.TabIndex = 47;
            this.label14.Text = "BarOneWay";
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.ForeColor = System.Drawing.SystemColors.Control;
            this.label13.Location = new System.Drawing.Point(72, 572);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(112, 21);
            this.label13.TabIndex = 46;
            this.label13.Text = "BarAOpen";
            // 
            // BarCloseCmdTextBox
            // 
            this.BarCloseCmdTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BarCloseCmdTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.BarCloseCmdTextBox.Location = new System.Drawing.Point(260, 433);
            this.BarCloseCmdTextBox.Name = "BarCloseCmdTextBox";
            this.BarCloseCmdTextBox.Size = new System.Drawing.Size(271, 36);
            this.BarCloseCmdTextBox.TabIndex = 49;
            // 
            // BarLockCmdTextBox
            // 
            this.BarLockCmdTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BarLockCmdTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.BarLockCmdTextBox.Location = new System.Drawing.Point(260, 476);
            this.BarLockCmdTextBox.Name = "BarLockCmdTextBox";
            this.BarLockCmdTextBox.Size = new System.Drawing.Size(271, 36);
            this.BarLockCmdTextBox.TabIndex = 50;
            // 
            // BarOneWayTextBox
            // 
            this.BarOneWayTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BarOneWayTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.BarOneWayTextBox.Location = new System.Drawing.Point(260, 519);
            this.BarOneWayTextBox.Name = "BarOneWayTextBox";
            this.BarOneWayTextBox.Size = new System.Drawing.Size(271, 36);
            this.BarOneWayTextBox.TabIndex = 51;
            // 
            // BarAOpenTextBox
            // 
            this.BarAOpenTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BarAOpenTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.BarAOpenTextBox.Location = new System.Drawing.Point(260, 564);
            this.BarAOpenTextBox.Name = "BarAOpenTextBox";
            this.BarAOpenTextBox.Size = new System.Drawing.Size(271, 36);
            this.BarAOpenTextBox.TabIndex = 53;
            // 
            // addButton
            // 
            this.addButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(124)))), ((int)(((byte)(113)))));
            this.addButton.FlatAppearance.BorderSize = 0;
            this.addButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addButton.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.addButton.ForeColor = System.Drawing.SystemColors.Control;
            this.addButton.Location = new System.Drawing.Point(432, 637);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(112, 45);
            this.addButton.TabIndex = 19;
            this.addButton.Text = "추가";
            this.addButton.UseVisualStyleBackColor = false;
            this.addButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(49)))), ((int)(((byte)(83)))));
            this.deleteButton.FlatAppearance.BorderSize = 0;
            this.deleteButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deleteButton.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.deleteButton.ForeColor = System.Drawing.SystemColors.Control;
            this.deleteButton.Location = new System.Drawing.Point(854, 637);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(112, 45);
            this.deleteButton.TabIndex = 22;
            this.deleteButton.Text = "삭제";
            this.deleteButton.UseVisualStyleBackColor = false;
            this.deleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // updateButton
            // 
            this.updateButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(84)))), ((int)(((byte)(136)))));
            this.updateButton.FlatAppearance.BorderSize = 0;
            this.updateButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.updateButton.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.updateButton.ForeColor = System.Drawing.SystemColors.Control;
            this.updateButton.Location = new System.Drawing.Point(550, 637);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(112, 45);
            this.updateButton.TabIndex = 21;
            this.updateButton.Text = "수정";
            this.updateButton.UseVisualStyleBackColor = false;
            this.updateButton.Click += new System.EventHandler(this.UpdateButton_Click);
            // 
            // SognoBarView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.ClientSize = new System.Drawing.Size(978, 699);
            this.Controls.Add(this.BarListView);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.updateButton);
            this.Name = "SognoBarView";
            this.Text = "SognoBarView";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView BarListView;
        private System.Windows.Forms.TextBox BarCountTypeTextBox;
        private System.Windows.Forms.TextBox BarConnTypeTextBox;
        private System.Windows.Forms.TextBox BarACTimeTextBox;
        private System.Windows.Forms.TextBox BarNameTextBox;
        private System.Windows.Forms.TextBox BarPortTextBox;
        private System.Windows.Forms.TextBox BarIPTextBox;
        private System.Windows.Forms.TextBox BarNoTextBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ParkComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox GateComboBox;
        private System.Windows.Forms.TextBox BarOpenCmdTextBox;
        private System.Windows.Forms.Label Reserve0TxtBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox BarCloseCmdTextBox;
        private System.Windows.Forms.TextBox BarLockCmdTextBox;
        private System.Windows.Forms.TextBox BarOneWayTextBox;
        private System.Windows.Forms.TextBox BarAOpenTextBox;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button updateButton;
    }
}