﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SognoLib.Park.GUI
{
    public partial class SognoGateGroupView : Form
    {
        private int? selectedParkNo;// 여기 오기전 선택된 주차장
        private int? selectedGateGroupNo; // 오기 전 선택된 게이트 그룹 번호
        private int? reQuest;
        private SogNo_ParkDBAccessor DBAcessor = null; // DB액세서
        private GateGroupInfoRecord[] gateGroupInfoRecords = null; // 전체 게이트 그룹 데이터
        private GateGroupInfoRecord selectedGateGroupInfoRecord = null; // 선택된 게이트 그룹데이터
        private ParkInfoRecord[] parkList; // 전체 주차장 데이터
        public SognoGateGroupView
            (
                SogNo_ParkDBAccessor DBAcessor, 
                int? selectedParkNo, 
                ParkInfoRecord[] parkList,
                int? selectedGateGroupNo = null,
                int? reQuest = null
            )
        {
            InitializeComponent();
            this.selectedParkNo = selectedParkNo;
            this.DBAcessor = DBAcessor;
            this.selectedGateGroupNo = selectedGateGroupNo;
            this.reQuest = reQuest;

            //주차장 콤보박스에 주차장명 전체 삽입
            this.parkList = parkList;
            this.ParkComboBox.DataSource = this.parkList;
            this.ParkComboBox.DisplayMember = ParkInfoFields.ParkName.ToString();
            this.ParkComboBox.ValueMember = ParkInfoFields.ParkNo.ToString();
            this.ParkComboBox.SelectedValue = this.selectedParkNo;

            this.gateGroupInfoRecords = this.DBAcessor.SelectGateGroupInfo(this.selectedParkNo.GetValueOrDefault());
            if (selectedGateGroupNo != null)
            {
                this.selectedGateGroupNo = selectedGateGroupNo;
                selectedGateGroupInfoRecord = DBAcessor.SelectGateGroupInfo(selectedParkNo.GetValueOrDefault(),this.selectedGateGroupNo)[0]; // 레코드에 데이터 넣기
                fill_textboxes(selectedGateGroupInfoRecord);
            }


            Init_gategrouplistview();
            RefreshGateGroupList();

            if(reQuest == 1)
            {
                if (this.selectedGateGroupInfoRecord != null)
                {
                    DialogResult dialog = MessageBox.Show("정말 삭제하시겠습니까?", "삭제확인메시지", MessageBoxButtons.YesNo);
                    if (dialog == DialogResult.Yes)
                    {
                        this.DBAcessor.DeleteGateGroupInfo(selectedGateGroupInfoRecord);
                        MessageBox.Show("삭제를 진행하였습니다.");
                    }
                    else if (dialog == DialogResult.No)
                    {
                        MessageBox.Show("삭제를 취소하였습니다.");
                    }
                }
                RefreshGateGroupList();
            }

        }

        public void fill_textboxes(GateGroupInfoRecord record) //텍스트 박스를 채우는 함수
        {
            this.ParkComboBox.SelectedValue = record.ParkNo;
            if (record.GateGroupNo != null) this.GateGroupNoTextBox.Text = record.GateGroupNo.ToString();
            if (record.GateGroupName != null) this.GateGroupNameTextBox.Text = record.GateGroupName.ToString();
        }

        public void Init_gategrouplistview() // 리스트뷰 초기 설정
        {
            //PCListView.BeginUpdate();   이거 사이에 쓰면 업데이트 다 되고 나서 로드됨 (오류방지용)
            //PCListView.EndUpdate();

            GateGroupListView.View = View.Details;
            GateGroupListView.GridLines = false;
            GateGroupListView.FullRowSelect = true; // 선택 시 라인 전체 선택

        }
        private void RefreshGateGroupList()
        {
            GateGroupListView.Clear(); // 전체 지우기

            GateGroupListView.Columns.Add("Index");
            GateGroupListView.Columns.Add("ParkNo");
            GateGroupListView.Columns.Add("GateGroupNo");
            GateGroupListView.Columns.Add("GateGroupName");

            for (int i = 0; i < GateGroupListView.Columns.Count; i++)
            {
                GateGroupListView.Columns[i].Width = -2;
            }

            gateGroupInfoRecords = DBAcessor.SelectGateGroupInfo(selectedParkNo.GetValueOrDefault()); // 레코드에 데이터 넣기

            // 폼에 데이터 뿌리기
            if (gateGroupInfoRecords.Length > 0)
            {
                for (int i = 0; gateGroupInfoRecords.Length > i; i++)
                {
                    ListViewItem a = new ListViewItem(i.ToString());
                    a.SubItems.Add(gateGroupInfoRecords[i].ParkNo.ToString());
                    a.SubItems.Add(NullCheck(gateGroupInfoRecords[i].GateGroupNo).ToString());
                    a.SubItems.Add(NullCheck(gateGroupInfoRecords[i].GateGroupName).ToString());
                    if (i % 2 == 0) a.BackColor = Color.FromArgb(70, 70, 70);
                    GateGroupListView.Items.Add(a);
                }
            }
        }
        private object NullCheck(object nullitem)
        {
            if (nullitem == null)
            {
                return " ";
            }
            else
            {
                return nullitem;
            }
        }
        private void AddButton_Click(object sender, EventArgs e)
        {
            insert_GateGroupInfo();
        }
        private void insert_GateGroupInfo()
        {
            try
            {
                GateGroupInfoRecord insert_GateGroupRecord = new GateGroupInfoRecord
                {
                    ParkNo = selectedParkNo,
                    GateGroupNo = 0,
                    GateGroupName = "기본 게이트그룹"
                };

                if (ParkComboBox.Items.Count > 0) insert_GateGroupRecord.ParkNo = int.Parse(ParkComboBox.SelectedValue.ToString());
                if (GateGroupNoTextBox.Text != "") insert_GateGroupRecord.GateGroupNo = int.Parse(GateGroupNoTextBox.Text.ToString());
                if (GateGroupNameTextBox.Text != "") insert_GateGroupRecord.GateGroupName = GateGroupNameTextBox.Text.ToString();

                this.DBAcessor.InsertGateGroupInfo(insert_GateGroupRecord);
                RefreshGateGroupList();
                MessageBox.Show("추가에 성공하였습니다.");

            }
            catch (Exception ex)
            {
                MessageBox.Show("추가에 실패하였습니다.");
                MessageBox.Show(ex.ToString());
            }
            
        }

        private void GateGroupListView_SelectedIndexChanged(object sender, EventArgs e) // 리스트뷰에서 하나 골랐을 때
        {
            if (GateGroupListView.SelectedItems.Count > 0)
            {
                int index = int.Parse(GateGroupListView.SelectedItems[0].Text);
                this.selectedGateGroupInfoRecord = this.gateGroupInfoRecords[index];
                fill_textboxes(this.gateGroupInfoRecords[index]);
            }
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            if (GateGroupListView.SelectedItems.Count > 0)
            {
                try
                {
                    GateGroupInfoRecord UpdateRecord = new GateGroupInfoRecord();
                    UpdateRecord.ParkNo = selectedGateGroupInfoRecord.ParkNo;
                    UpdateRecord.GateGroupNo = int.Parse(GateGroupNoTextBox.Text.ToString());
                    UpdateRecord.GateGroupName = GateGroupNameTextBox.Text.ToString();

                    this.DBAcessor.UpdateGateGroupInfo(UpdateRecord);
                    MessageBox.Show("수정이 진행되었습니다.");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("수정에 실패하였습니다.");
                    MessageBox.Show(ex.ToString());
                }
                RefreshGateGroupList();
            }
        }

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            if (this.selectedGateGroupInfoRecord != null)
            {
                DialogResult dialog = MessageBox.Show("정말 삭제하시겠습니까?", "삭제확인메시지", MessageBoxButtons.YesNo);
                if (dialog == DialogResult.Yes)
                {
                    this.DBAcessor.DeleteGateGroupInfo(selectedGateGroupInfoRecord);
                    MessageBox.Show("삭제를 진행하였습니다.");
                }
                else if (dialog == DialogResult.No)
                {
                    MessageBox.Show("삭제를 취소하였습니다.");
                }
            }
            RefreshGateGroupList();
        }
    }
}