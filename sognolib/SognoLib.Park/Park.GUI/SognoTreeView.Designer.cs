﻿namespace SognoLib.Park.GUI
{
    partial class SognoTreeView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GateTreeView = new System.Windows.Forms.TreeView();
            this.ParkListComboBox = new System.Windows.Forms.ComboBox();
            this.gateGroupViewBT = new System.Windows.Forms.Button();
            this.gateViewBT = new System.Windows.Forms.Button();
            this.SognoPCButton = new System.Windows.Forms.Button();
            this.cameraViewBT = new System.Windows.Forms.Button();
            this.barViewBT = new System.Windows.Forms.Button();
            this.screenViewBT = new System.Windows.Forms.Button();
            this.treeRefreshBT = new System.Windows.Forms.Button();
            this.systemViewBT = new System.Windows.Forms.Button();
            this.RpiViewBT = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // GateTreeView
            // 
            this.GateTreeView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.GateTreeView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.GateTreeView.Font = new System.Drawing.Font("맑은 고딕", 9F);
            this.GateTreeView.ForeColor = System.Drawing.SystemColors.Window;
            this.GateTreeView.LineColor = System.Drawing.Color.White;
            this.GateTreeView.Location = new System.Drawing.Point(12, 46);
            this.GateTreeView.Name = "GateTreeView";
            this.GateTreeView.Size = new System.Drawing.Size(671, 556);
            this.GateTreeView.TabIndex = 0;
            this.GateTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.GateTreeView_AfterSelect);
            this.GateTreeView.Click += new System.EventHandler(this.GateTreeView_Click);
            this.GateTreeView.DoubleClick += new System.EventHandler(this.GateTreeView_DoubleClick);
            this.GateTreeView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.GateTreeView_MouseDown);
            // 
            // ParkListComboBox
            // 
            this.ParkListComboBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.ParkListComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ParkListComboBox.Font = new System.Drawing.Font("제주고딕", 16F);
            this.ParkListComboBox.ForeColor = System.Drawing.SystemColors.Window;
            this.ParkListComboBox.FormattingEnabled = true;
            this.ParkListComboBox.Location = new System.Drawing.Point(12, 11);
            this.ParkListComboBox.Name = "ParkListComboBox";
            this.ParkListComboBox.Size = new System.Drawing.Size(216, 29);
            this.ParkListComboBox.TabIndex = 1;
            this.ParkListComboBox.SelectedIndexChanged += new System.EventHandler(this.ParkListComboBox_SelectedIndexChanged);
            // 
            // gateGroupViewBT
            // 
            this.gateGroupViewBT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(84)))), ((int)(((byte)(136)))));
            this.gateGroupViewBT.FlatAppearance.BorderSize = 0;
            this.gateGroupViewBT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gateGroupViewBT.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gateGroupViewBT.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gateGroupViewBT.Location = new System.Drawing.Point(698, 178);
            this.gateGroupViewBT.Name = "gateGroupViewBT";
            this.gateGroupViewBT.Size = new System.Drawing.Size(194, 51);
            this.gateGroupViewBT.TabIndex = 2;
            this.gateGroupViewBT.Text = "게이트 그룹";
            this.gateGroupViewBT.UseVisualStyleBackColor = false;
            this.gateGroupViewBT.Click += new System.EventHandler(this.GateGroupViewBT_Click);
            // 
            // gateViewBT
            // 
            this.gateViewBT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(84)))), ((int)(((byte)(136)))));
            this.gateViewBT.FlatAppearance.BorderSize = 0;
            this.gateViewBT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gateViewBT.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gateViewBT.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.gateViewBT.Location = new System.Drawing.Point(698, 235);
            this.gateViewBT.Name = "gateViewBT";
            this.gateViewBT.Size = new System.Drawing.Size(194, 51);
            this.gateViewBT.TabIndex = 3;
            this.gateViewBT.Text = "게이트";
            this.gateViewBT.UseVisualStyleBackColor = false;
            this.gateViewBT.Click += new System.EventHandler(this.GateViewBT_Click);
            // 
            // SognoPCButton
            // 
            this.SognoPCButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(124)))), ((int)(((byte)(113)))));
            this.SognoPCButton.FlatAppearance.BorderSize = 0;
            this.SognoPCButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SognoPCButton.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.SognoPCButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.SognoPCButton.Location = new System.Drawing.Point(698, 46);
            this.SognoPCButton.Name = "SognoPCButton";
            this.SognoPCButton.Size = new System.Drawing.Size(194, 51);
            this.SognoPCButton.TabIndex = 4;
            this.SognoPCButton.Text = "PC";
            this.SognoPCButton.UseVisualStyleBackColor = false;
            this.SognoPCButton.Click += new System.EventHandler(this.SognoPCButton_Click);
            // 
            // cameraViewBT
            // 
            this.cameraViewBT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(49)))), ((int)(((byte)(140)))));
            this.cameraViewBT.FlatAppearance.BorderSize = 0;
            this.cameraViewBT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cameraViewBT.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cameraViewBT.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.cameraViewBT.Location = new System.Drawing.Point(698, 307);
            this.cameraViewBT.Name = "cameraViewBT";
            this.cameraViewBT.Size = new System.Drawing.Size(194, 51);
            this.cameraViewBT.TabIndex = 5;
            this.cameraViewBT.Text = "카메라";
            this.cameraViewBT.UseVisualStyleBackColor = false;
            this.cameraViewBT.Click += new System.EventHandler(this.cameraViewBT_Click);
            // 
            // barViewBT
            // 
            this.barViewBT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(49)))), ((int)(((byte)(140)))));
            this.barViewBT.FlatAppearance.BorderSize = 0;
            this.barViewBT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.barViewBT.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.barViewBT.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.barViewBT.Location = new System.Drawing.Point(698, 364);
            this.barViewBT.Name = "barViewBT";
            this.barViewBT.Size = new System.Drawing.Size(194, 51);
            this.barViewBT.TabIndex = 6;
            this.barViewBT.Text = "차단기";
            this.barViewBT.UseVisualStyleBackColor = false;
            this.barViewBT.Click += new System.EventHandler(this.BarViewBT_Click);
            // 
            // screenViewBT
            // 
            this.screenViewBT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(49)))), ((int)(((byte)(140)))));
            this.screenViewBT.FlatAppearance.BorderSize = 0;
            this.screenViewBT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.screenViewBT.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.screenViewBT.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.screenViewBT.Location = new System.Drawing.Point(698, 421);
            this.screenViewBT.Name = "screenViewBT";
            this.screenViewBT.Size = new System.Drawing.Size(194, 51);
            this.screenViewBT.TabIndex = 7;
            this.screenViewBT.Text = "전광판";
            this.screenViewBT.UseVisualStyleBackColor = false;
            this.screenViewBT.Click += new System.EventHandler(this.ScreenViewBT_Click);
            // 
            // treeRefreshBT
            // 
            this.treeRefreshBT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(49)))), ((int)(((byte)(83)))));
            this.treeRefreshBT.FlatAppearance.BorderSize = 0;
            this.treeRefreshBT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.treeRefreshBT.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.treeRefreshBT.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.treeRefreshBT.Location = new System.Drawing.Point(698, 551);
            this.treeRefreshBT.Name = "treeRefreshBT";
            this.treeRefreshBT.Size = new System.Drawing.Size(194, 51);
            this.treeRefreshBT.TabIndex = 8;
            this.treeRefreshBT.Text = "새로고침";
            this.treeRefreshBT.UseVisualStyleBackColor = false;
            this.treeRefreshBT.Click += new System.EventHandler(this.TreeRefreshBT_Click);
            // 
            // systemViewBT
            // 
            this.systemViewBT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(124)))), ((int)(((byte)(113)))));
            this.systemViewBT.FlatAppearance.BorderSize = 0;
            this.systemViewBT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.systemViewBT.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.systemViewBT.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.systemViewBT.Location = new System.Drawing.Point(698, 103);
            this.systemViewBT.Name = "systemViewBT";
            this.systemViewBT.Size = new System.Drawing.Size(194, 51);
            this.systemViewBT.TabIndex = 9;
            this.systemViewBT.Text = "시스템";
            this.systemViewBT.UseVisualStyleBackColor = false;
            this.systemViewBT.Click += new System.EventHandler(this.systemViewBT_Click);
            // 
            // RpiViewBT
            // 
            this.RpiViewBT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(49)))), ((int)(((byte)(140)))));
            this.RpiViewBT.FlatAppearance.BorderSize = 0;
            this.RpiViewBT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RpiViewBT.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.RpiViewBT.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.RpiViewBT.Location = new System.Drawing.Point(698, 478);
            this.RpiViewBT.Name = "RpiViewBT";
            this.RpiViewBT.Size = new System.Drawing.Size(194, 51);
            this.RpiViewBT.TabIndex = 15;
            this.RpiViewBT.Text = "라즈베리파이";
            this.RpiViewBT.UseVisualStyleBackColor = false;
            this.RpiViewBT.Click += new System.EventHandler(this.RpiViewBT_Click);
            // 
            // SognoTreeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.ClientSize = new System.Drawing.Size(904, 616);
            this.Controls.Add(this.RpiViewBT);
            this.Controls.Add(this.systemViewBT);
            this.Controls.Add(this.treeRefreshBT);
            this.Controls.Add(this.screenViewBT);
            this.Controls.Add(this.barViewBT);
            this.Controls.Add(this.cameraViewBT);
            this.Controls.Add(this.SognoPCButton);
            this.Controls.Add(this.gateViewBT);
            this.Controls.Add(this.gateGroupViewBT);
            this.Controls.Add(this.ParkListComboBox);
            this.Controls.Add(this.GateTreeView);
            this.Name = "SognoTreeView";
            this.Text = "SognoTreeView";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView GateTreeView;
        private System.Windows.Forms.ComboBox ParkListComboBox;
        private System.Windows.Forms.Button gateGroupViewBT;
        private System.Windows.Forms.Button gateViewBT;
        private System.Windows.Forms.Button SognoPCButton;
        private System.Windows.Forms.Button cameraViewBT;
        private System.Windows.Forms.Button barViewBT;
        private System.Windows.Forms.Button screenViewBT;
        private System.Windows.Forms.Button treeRefreshBT;
        private System.Windows.Forms.Button systemViewBT;
        private System.Windows.Forms.Button RpiViewBT;
    }
}