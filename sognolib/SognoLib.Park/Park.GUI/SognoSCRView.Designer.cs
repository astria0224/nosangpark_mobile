﻿namespace SognoLib.Park.GUI
{
    partial class SognoSCRView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UpTextTextBox = new System.Windows.Forms.TextBox();
            this.SCRTimeTextBox = new System.Windows.Forms.TextBox();
            this.SCRNameTextBox = new System.Windows.Forms.TextBox();
            this.SCRPortTextBox = new System.Windows.Forms.TextBox();
            this.SCRIPTextBox = new System.Windows.Forms.TextBox();
            this.SCRTypeTextBox = new System.Windows.Forms.TextBox();
            this.SCRNoTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ParkComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.GateComboBox = new System.Windows.Forms.ComboBox();
            this.UpColorTextBox = new System.Windows.Forms.TextBox();
            this.Reserve0TxtBox = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.DownTextTextBox = new System.Windows.Forms.TextBox();
            this.DownColorTextBox = new System.Windows.Forms.TextBox();
            this.deleteButton = new System.Windows.Forms.Button();
            this.updateButton = new System.Windows.Forms.Button();
            this.SCRListView = new System.Windows.Forms.ListView();
            this.addButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // UpTextTextBox
            // 
            this.UpTextTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.UpTextTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UpTextTextBox.Location = new System.Drawing.Point(179, 407);
            this.UpTextTextBox.Name = "UpTextTextBox";
            this.UpTextTextBox.Size = new System.Drawing.Size(273, 36);
            this.UpTextTextBox.TabIndex = 41;
            // 
            // SCRTimeTextBox
            // 
            this.SCRTimeTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SCRTimeTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.SCRTimeTextBox.Location = new System.Drawing.Point(179, 357);
            this.SCRTimeTextBox.Name = "SCRTimeTextBox";
            this.SCRTimeTextBox.Size = new System.Drawing.Size(273, 36);
            this.SCRTimeTextBox.TabIndex = 40;
            // 
            // SCRNameTextBox
            // 
            this.SCRNameTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SCRNameTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.SCRNameTextBox.Location = new System.Drawing.Point(179, 307);
            this.SCRNameTextBox.Name = "SCRNameTextBox";
            this.SCRNameTextBox.Size = new System.Drawing.Size(273, 36);
            this.SCRNameTextBox.TabIndex = 39;
            // 
            // SCRPortTextBox
            // 
            this.SCRPortTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SCRPortTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.SCRPortTextBox.Location = new System.Drawing.Point(179, 257);
            this.SCRPortTextBox.Name = "SCRPortTextBox";
            this.SCRPortTextBox.Size = new System.Drawing.Size(273, 36);
            this.SCRPortTextBox.TabIndex = 38;
            // 
            // SCRIPTextBox
            // 
            this.SCRIPTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SCRIPTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.SCRIPTextBox.Location = new System.Drawing.Point(179, 207);
            this.SCRIPTextBox.Name = "SCRIPTextBox";
            this.SCRIPTextBox.Size = new System.Drawing.Size(273, 36);
            this.SCRIPTextBox.TabIndex = 37;
            // 
            // SCRTypeTextBox
            // 
            this.SCRTypeTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SCRTypeTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.SCRTypeTextBox.Location = new System.Drawing.Point(179, 157);
            this.SCRTypeTextBox.Name = "SCRTypeTextBox";
            this.SCRTypeTextBox.Size = new System.Drawing.Size(273, 36);
            this.SCRTypeTextBox.TabIndex = 36;
            // 
            // SCRNoTextBox
            // 
            this.SCRNoTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SCRNoTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.SCRNoTextBox.Location = new System.Drawing.Point(179, 107);
            this.SCRNoTextBox.Name = "SCRNoTextBox";
            this.SCRNoTextBox.Size = new System.Drawing.Size(273, 36);
            this.SCRNoTextBox.TabIndex = 35;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(28, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "소속 주차장";
            // 
            // ParkComboBox
            // 
            this.ParkComboBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ParkComboBox.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ParkComboBox.FormattingEnabled = true;
            this.ParkComboBox.Location = new System.Drawing.Point(179, 7);
            this.ParkComboBox.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.ParkComboBox.Name = "ParkComboBox";
            this.ParkComboBox.Size = new System.Drawing.Size(273, 32);
            this.ParkComboBox.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.SystemColors.Control;
            this.label4.Location = new System.Drawing.Point(28, 64);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 21);
            this.label4.TabIndex = 24;
            this.label4.Text = "소속 게이트";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.SystemColors.Control;
            this.label5.Location = new System.Drawing.Point(28, 114);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 21);
            this.label5.TabIndex = 25;
            this.label5.Text = "전광판 번호";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.SystemColors.Control;
            this.label6.Location = new System.Drawing.Point(28, 164);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(111, 21);
            this.label6.TabIndex = 26;
            this.label6.Text = "전광판 타입";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.ForeColor = System.Drawing.SystemColors.Control;
            this.label7.Location = new System.Drawing.Point(19, 214);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(130, 21);
            this.label7.TabIndex = 27;
            this.label7.Text = "전광판 아이피";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.ForeColor = System.Drawing.SystemColors.Control;
            this.label8.Location = new System.Drawing.Point(28, 264);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 21);
            this.label8.TabIndex = 28;
            this.label8.Text = "전광판 포트";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.ForeColor = System.Drawing.SystemColors.Control;
            this.label9.Location = new System.Drawing.Point(41, 314);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 21);
            this.label9.TabIndex = 29;
            this.label9.Text = "전광판명";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.ForeColor = System.Drawing.SystemColors.Control;
            this.label10.Location = new System.Drawing.Point(28, 364);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(111, 21);
            this.label10.TabIndex = 30;
            this.label10.Text = "전광판 타임";
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.ForeColor = System.Drawing.SystemColors.Control;
            this.label11.Location = new System.Drawing.Point(41, 414);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 21);
            this.label11.TabIndex = 31;
            this.label11.Text = "상단문구";
            // 
            // GateComboBox
            // 
            this.GateComboBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.GateComboBox.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.GateComboBox.FormattingEnabled = true;
            this.GateComboBox.Location = new System.Drawing.Point(179, 57);
            this.GateComboBox.Margin = new System.Windows.Forms.Padding(3, 7, 3, 3);
            this.GateComboBox.Name = "GateComboBox";
            this.GateComboBox.Size = new System.Drawing.Size(273, 32);
            this.GateComboBox.TabIndex = 43;
            // 
            // UpColorTextBox
            // 
            this.UpColorTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.UpColorTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UpColorTextBox.Location = new System.Drawing.Point(179, 457);
            this.UpColorTextBox.Name = "UpColorTextBox";
            this.UpColorTextBox.Size = new System.Drawing.Size(273, 36);
            this.UpColorTextBox.TabIndex = 42;
            // 
            // Reserve0TxtBox
            // 
            this.Reserve0TxtBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Reserve0TxtBox.AutoSize = true;
            this.Reserve0TxtBox.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Reserve0TxtBox.ForeColor = System.Drawing.SystemColors.Control;
            this.Reserve0TxtBox.Location = new System.Drawing.Point(38, 464);
            this.Reserve0TxtBox.Name = "Reserve0TxtBox";
            this.Reserve0TxtBox.Size = new System.Drawing.Size(92, 21);
            this.Reserve0TxtBox.TabIndex = 32;
            this.Reserve0TxtBox.Text = "상단 색상";
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.ForeColor = System.Drawing.SystemColors.Control;
            this.label12.Location = new System.Drawing.Point(38, 567);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(92, 21);
            this.label12.TabIndex = 45;
            this.label12.Text = "하단 색상";
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.SystemColors.Control;
            this.label3.Location = new System.Drawing.Point(41, 514);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 21);
            this.label3.TabIndex = 44;
            this.label3.Text = "하단문구";
            // 
            // DownTextTextBox
            // 
            this.DownTextTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.DownTextTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.DownTextTextBox.Location = new System.Drawing.Point(179, 507);
            this.DownTextTextBox.Name = "DownTextTextBox";
            this.DownTextTextBox.Size = new System.Drawing.Size(273, 36);
            this.DownTextTextBox.TabIndex = 49;
            // 
            // DownColorTextBox
            // 
            this.DownColorTextBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.DownColorTextBox.Font = new System.Drawing.Font("제주고딕", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.DownColorTextBox.Location = new System.Drawing.Point(179, 560);
            this.DownColorTextBox.Name = "DownColorTextBox";
            this.DownColorTextBox.Size = new System.Drawing.Size(273, 36);
            this.DownColorTextBox.TabIndex = 50;
            // 
            // deleteButton
            // 
            this.deleteButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(108)))), ((int)(((byte)(49)))), ((int)(((byte)(83)))));
            this.deleteButton.FlatAppearance.BorderSize = 0;
            this.deleteButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deleteButton.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.deleteButton.ForeColor = System.Drawing.SystemColors.Control;
            this.deleteButton.Location = new System.Drawing.Point(579, 637);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(112, 45);
            this.deleteButton.TabIndex = 22;
            this.deleteButton.Text = "삭제";
            this.deleteButton.UseVisualStyleBackColor = false;
            this.deleteButton.Click += new System.EventHandler(this.DeleteButton_Click);
            // 
            // updateButton
            // 
            this.updateButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(62)))), ((int)(((byte)(84)))), ((int)(((byte)(136)))));
            this.updateButton.FlatAppearance.BorderSize = 0;
            this.updateButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.updateButton.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.updateButton.ForeColor = System.Drawing.SystemColors.Control;
            this.updateButton.Location = new System.Drawing.Point(348, 637);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(112, 45);
            this.updateButton.TabIndex = 21;
            this.updateButton.Text = "수정";
            this.updateButton.UseVisualStyleBackColor = false;
            this.updateButton.Click += new System.EventHandler(this.UpdateButton_Click);
            // 
            // SCRListView
            // 
            this.SCRListView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.SCRListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SCRListView.ForeColor = System.Drawing.SystemColors.Control;
            this.SCRListView.HideSelection = false;
            this.SCRListView.Location = new System.Drawing.Point(12, 12);
            this.SCRListView.Name = "SCRListView";
            this.SCRListView.Size = new System.Drawing.Size(190, 325);
            this.SCRListView.TabIndex = 20;
            this.SCRListView.UseCompatibleStateImageBehavior = false;
            this.SCRListView.SelectedIndexChanged += new System.EventHandler(this.SCRListView_SelectedIndexChanged);
            // 
            // addButton
            // 
            this.addButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(124)))), ((int)(((byte)(113)))));
            this.addButton.FlatAppearance.BorderSize = 0;
            this.addButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addButton.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.addButton.ForeColor = System.Drawing.SystemColors.Control;
            this.addButton.Location = new System.Drawing.Point(230, 637);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(112, 45);
            this.addButton.TabIndex = 19;
            this.addButton.Text = "추가";
            this.addButton.UseVisualStyleBackColor = false;
            this.addButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.46789F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.53211F));
            this.tableLayoutPanel1.Controls.Add(this.UpTextTextBox, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.SCRTimeTextBox, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.SCRNameTextBox, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.SCRPortTextBox, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.SCRIPTextBox, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.SCRTypeTextBox, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.SCRNoTextBox, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ParkComboBox, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.GateComboBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.UpColorTextBox, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.Reserve0TxtBox, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.DownTextTextBox, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.DownColorTextBox, 1, 11);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(228, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 12;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 6.666667F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(463, 606);
            this.tableLayoutPanel1.TabIndex = 18;
            // 
            // SognoSCRView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.ClientSize = new System.Drawing.Size(709, 694);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.updateButton);
            this.Controls.Add(this.SCRListView);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "SognoSCRView";
            this.Text = "SognoSCRView";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox UpTextTextBox;
        private System.Windows.Forms.TextBox SCRTimeTextBox;
        private System.Windows.Forms.TextBox SCRNameTextBox;
        private System.Windows.Forms.TextBox SCRPortTextBox;
        private System.Windows.Forms.TextBox SCRIPTextBox;
        private System.Windows.Forms.TextBox SCRTypeTextBox;
        private System.Windows.Forms.TextBox SCRNoTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox ParkComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox GateComboBox;
        private System.Windows.Forms.TextBox UpColorTextBox;
        private System.Windows.Forms.Label Reserve0TxtBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox DownTextTextBox;
        private System.Windows.Forms.TextBox DownColorTextBox;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.ListView SCRListView;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}