﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SognoLib.Park.GUI
{
    public partial class ParkAddform : Form
    {
        ParkInfoRecord InsertRecord = new ParkInfoRecord();
        SogNo_ParkDBAccessor dbAccessor;
        ParkViewer parkViewer;
        public ParkAddform(ParkViewer parkViewer, SogNo_ParkDBAccessor dbAccessor)
        {
            this.parkViewer = parkViewer;
            this.dbAccessor = dbAccessor;
            this.parkViewer = parkViewer;
            InitializeComponent();
            this.InsertRecord.ParkNo = 0; // 주차장 고유번호
            this.InsertRecord.ParkName = "기본주차장"; //주차장명
            this.InsertRecord.ParkOwner = ""; //운영자
            this.InsertRecord.ParkRegNum = ""; //사업자등록번호
            this.InsertRecord.ParkPhone = ""; //전화번호
            this.InsertRecord.ParkAddr = ""; //주차장 주소
            this.InsertRecord.ParkNote = ""; //주차장 전화번호
            this.InsertRecord.ParkIPAddr = "127.0.0.1"; //주차장 메인IP
            this.InsertRecord.ParkDomain = ""; //주차장 도메인
            this.InsertRecord.ParkPort = 3306; //주차장 포트
            this.InsertRecord.DefaultTrfNo = 1; //기본 요금체계 번호
            this.InsertRecord.Reserve0 = null;
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            try{
                if (parkNoTxtBox.Text != "") this.InsertRecord.ParkNo = int.Parse(parkNoTxtBox.Text); //주차장명
                if (parkNameTxtBox.Text != "") this.InsertRecord.ParkName = parkNameTxtBox.Text; //주차장명
                if (parkOwnerTxtBox.Text != "") this.InsertRecord.ParkOwner = parkOwnerTxtBox.Text; //운영자
                if (parkRegNumTxtBox.Text != "") this.InsertRecord.ParkRegNum = parkRegNumTxtBox.Text; //사업자등록번호
                if (parkPhoneTxtBox.Text != "") this.InsertRecord.ParkPhone = parkPhoneTxtBox.Text; //전화번호
                if (parkAddrTxtBox.Text != "") this.InsertRecord.ParkAddr = parkAddrTxtBox.Text; //주차장 주소
                if (parkNoteTxtBox.Text != "") this.InsertRecord.ParkNote = parkNoteTxtBox.Text; //주차장 전화번호
                if (parkIPAddrTxtBox.Text != "") this.InsertRecord.ParkIPAddr = parkIPAddrTxtBox.Text; //주차장 메인IP
                if (parkDomainTxtBox.Text != "") this.InsertRecord.ParkDomain = parkDomainTxtBox.Text; //주차장 도메인
                if (parkPortTxtBox.Text != "") this.InsertRecord.ParkPort = Int32.Parse(parkPortTxtBox.Text); //주차장 포트
                if (parkDefaultTrfNoTxtBox.Text != "") this.InsertRecord.DefaultTrfNo = Int32.Parse(parkDefaultTrfNoTxtBox.Text); //기본 요금체계 번호
                //if (Reserve0.Text != "") this.InsertRecord.Reserve0 = Reserve0.Text;

                this.dbAccessor.InsertParkInfo(InsertRecord);
                MessageBox.Show("추가를 진행했습니다.");
                parkViewer.RefreshParkList();
                this.Close();
            }
            catch
            {
                MessageBox.Show("추가 실패 (중복값, 특수문자 등이 잘못 들어갔을 수 있음)");
            }
        }

        private void Exitbutton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
