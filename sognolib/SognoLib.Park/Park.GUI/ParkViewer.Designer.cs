﻿namespace SognoLib.Park.GUI
{
    partial class ParkViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.parkListView = new System.Windows.Forms.ListView();
            this.newParkAddButton = new System.Windows.Forms.Button();
            this.ParkUpdateBt = new System.Windows.Forms.Button();
            this.ParkDeleteBt = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // parkListView
            // 
            this.parkListView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.parkListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.parkListView.Font = new System.Drawing.Font("제주고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.parkListView.ForeColor = System.Drawing.SystemColors.Desktop;
            this.parkListView.FullRowSelect = true;
            this.parkListView.HideSelection = false;
            this.parkListView.Location = new System.Drawing.Point(12, 12);
            this.parkListView.Name = "parkListView";
            this.parkListView.Size = new System.Drawing.Size(1210, 366);
            this.parkListView.TabIndex = 0;
            this.parkListView.UseCompatibleStateImageBehavior = false;
            this.parkListView.SelectedIndexChanged += new System.EventHandler(this.ParkListView_SelectedIndexChanged);
            this.parkListView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ParkListView_MouseDoubleClick);
            // 
            // newParkAddButton
            // 
            this.newParkAddButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.newParkAddButton.FlatAppearance.BorderSize = 0;
            this.newParkAddButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.newParkAddButton.Font = new System.Drawing.Font("제주고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.newParkAddButton.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.newParkAddButton.Location = new System.Drawing.Point(12, 384);
            this.newParkAddButton.Name = "newParkAddButton";
            this.newParkAddButton.Size = new System.Drawing.Size(131, 49);
            this.newParkAddButton.TabIndex = 1;
            this.newParkAddButton.Text = "신규추가";
            this.newParkAddButton.UseVisualStyleBackColor = false;
            this.newParkAddButton.Click += new System.EventHandler(this.NewParkAddButton_Click);
            // 
            // ParkUpdateBt
            // 
            this.ParkUpdateBt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(117)))), ((int)(((byte)(189)))));
            this.ParkUpdateBt.FlatAppearance.BorderSize = 0;
            this.ParkUpdateBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ParkUpdateBt.Font = new System.Drawing.Font("제주고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ParkUpdateBt.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.ParkUpdateBt.Location = new System.Drawing.Point(159, 384);
            this.ParkUpdateBt.Name = "ParkUpdateBt";
            this.ParkUpdateBt.Size = new System.Drawing.Size(131, 49);
            this.ParkUpdateBt.TabIndex = 2;
            this.ParkUpdateBt.Text = "수정";
            this.ParkUpdateBt.UseVisualStyleBackColor = false;
            this.ParkUpdateBt.Click += new System.EventHandler(this.Advance_View_Click);
            // 
            // ParkDeleteBt
            // 
            this.ParkDeleteBt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(103)))), ((int)(((byte)(37)))), ((int)(((byte)(122)))));
            this.ParkDeleteBt.FlatAppearance.BorderSize = 0;
            this.ParkDeleteBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ParkDeleteBt.Font = new System.Drawing.Font("제주고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ParkDeleteBt.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.ParkDeleteBt.Location = new System.Drawing.Point(1091, 384);
            this.ParkDeleteBt.Name = "ParkDeleteBt";
            this.ParkDeleteBt.Size = new System.Drawing.Size(131, 49);
            this.ParkDeleteBt.TabIndex = 3;
            this.ParkDeleteBt.Text = "삭제";
            this.ParkDeleteBt.UseVisualStyleBackColor = false;
            this.ParkDeleteBt.Click += new System.EventHandler(this.ParkDeleteBt_Click);
            // 
            // ParkViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.ClientSize = new System.Drawing.Size(1234, 445);
            this.Controls.Add(this.ParkDeleteBt);
            this.Controls.Add(this.ParkUpdateBt);
            this.Controls.Add(this.newParkAddButton);
            this.Controls.Add(this.parkListView);
            this.Name = "ParkViewer";
            this.Text = "ParkViewer";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView parkListView;
        private System.Windows.Forms.Button newParkAddButton;
        private System.Windows.Forms.Button ParkUpdateBt;
        private System.Windows.Forms.Button ParkDeleteBt;
    }
}