﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SognoLib.Core;
using SognoLib.Park;

namespace SognoLib.Park.GUI
{
    public partial class SognoTreeView : Form
    {
        // 현재 선택된 주차장 조회에 쓰는 레코드
        private ParkInfoRecord selectedparkinfo = null;

        // 특정 리스트 조회에 쓰는 레코드
        private GateGroupInfoRecord selectedGateGroupInfo = null;
        private GateInfoRecord selectedGateInfo = null;
        private CamInfoRecord selectedCamInfo = null;
        private BarInfoRecord selectedBarInfo = null;
        private ScrInfoRecord selectedScrInfo = null;
        private PCInfoRecord selectedPcInfo = null;
        private RpiInfoRecord selectedRpiInfo = null;
        private SysInfoRecord selectedSysInfo = null;

        // UI 왼쪽의 Treeview 조회에 쓰는 레코드배열
        private ParkInfoRecord[] parkInfo = null;
        private GateGroupInfoRecord[] gategroupInfo = null;
        private GateInfoRecord[] gateInfo = null;
        private CamInfoRecord[] camInfo = null;
        private BarInfoRecord[] barInfo = null;
        private ScrInfoRecord[] scrInfo = null;
        private PCInfoRecord[] pcInfo = null;
        private RpiInfoRecord[] rpiInfo = null;
        private SysInfoRecord[] sysInfo = null;

        private SogNo_ParkDBAccessor dbAcessor = null;

        private int selectedParkNo = 0;
        private int selectedGateGroupNo;
        private int selectedGateNo;
        private int selectedPcNo;
        private int selectedSysNo;
        private int selectedCamNo;
        private int selectedScrNo;
        private int selectedBarNo;
        private int selectedRPiNo;

        private int selectedNodeNo;

        private string selectedDataType = "GATEGROUP"; // 지금 TreeView에서 고른게 뭔지 저장해둠 (추가,수정,삭제 시 사용)

        public SognoTreeView(SogNo_ParkDBAccessor dbAccessor, ParkInfoRecord parkInfo = null)
        {
            InitializeComponent();

            this.dbAcessor = dbAccessor;
            if (parkInfo != null)
            {
                this.selectedParkNo = parkInfo.ParkNo.GetValueOrDefault();
            }
           
            //전체 주차장 명칭을 콤보박스에 삽입
            this.parkInfo = dbAcessor.SelectParkInfo();
            this.ParkListComboBox.DataSource = this.parkInfo;
            this.ParkListComboBox.DisplayMember = ParkInfoFields.ParkName.ToString();
            this.ParkListComboBox.ValueMember = ParkInfoFields.ParkNo.ToString();

            //트리뷰 이미지 설정
            ImageList GateImageList = new ImageList();
            GateImageList.Images.Add(SognoLib.Park.Properties.Resources.gate);
            GateImageList.Images.Add(SognoLib.Park.Properties.Resources.cam);
            GateImageList.Images.Add(SognoLib.Park.Properties.Resources.bar);
            GateImageList.Images.Add(SognoLib.Park.Properties.Resources.scr);
            GateImageList.Images.Add(SognoLib.Park.Properties.Resources.pc);
            GateTreeView.ImageList = GateImageList;
            if (parkInfo != null)
            {
                this.ParkListComboBox.SelectedValue = selectedParkNo;
            }
            else
            {
                this.ParkListComboBox.SelectedIndex = -1;
            }

            SpreadDataInTreeView();
        }

        private void SpreadDataInTreeView() // 트리뷰에 데이터 뿌려주기
        {
            if (ParkListComboBox.SelectedIndex == -1)
            {
                return;
            }

            GateTreeView.Nodes.Clear();


            //총 2개의 트리 뷰로 구성되어 있음f

            /***********
             * 이미지 인덱스 정리
             * node level 0
             *     PC : 4
             *     GATEGROUP : 0
             * node level 1
             *     SYSTEM : 0
             *     GATE : 3
             * node level 2
             *     CAM : 1
             *     BAR : 2
             *     SCR : 3
             *     RPI : 0
             * *********/

            //여기부터 PC단위 트리 뷰 구현
            pcInfo = dbAcessor.SelectPCInfo(this.selectedParkNo);
            if (pcInfo.Length >= 0)
            {
                for (int pc_i = 0; pc_i < pcInfo.Length; pc_i++) //pc 개별로 불러내는 for문
                {
                    TreeNode PCNodes = GateTreeView.Nodes.Add(pcInfo[pc_i].PCNo.ToString(), pcInfo[pc_i].PCName, 4, 4);

                    //우클릭 메뉴 추가
                    ContextMenuStrip PCNodesConMenu = new ContextMenuStrip();
                    ToolStripMenuItem newSysAddLabel = new ToolStripMenuItem();
                    newSysAddLabel.Text = "시스템 신규 추가...";
                    newSysAddLabel.Click += new EventHandler((sender, e) => SognoSysUpdate(sender,e,selectedPcNo,null));


                    ToolStripMenuItem PCrenameLabel = new ToolStripMenuItem();
                    PCrenameLabel.Text = "수정";
                    PCrenameLabel.Click += new EventHandler((sender, e) => SognoPCUpdate(sender, e, null, null, selectedPcNo, 0));

                    ToolStripMenuItem PCdeleteLabel = new ToolStripMenuItem();
                    PCdeleteLabel.Text = "삭제";
                    PCdeleteLabel.Click += new EventHandler((sender, e) => SognoPCUpdate(sender, e, null, null, selectedPcNo, 1));

                    PCNodesConMenu.Items.AddRange(new ToolStripMenuItem[] { newSysAddLabel, PCrenameLabel, PCdeleteLabel });
                    PCNodes.ContextMenuStrip = PCNodesConMenu;
                    //우클릭 메뉴 추가 끝

                    try
                    {
                        sysInfo = dbAcessor.SelectSysInfo(this.selectedParkNo, pcInfo[pc_i].PCNo);
                    }
                    catch { }
                    if (sysInfo.Length >= 0)
                    {
                        for (int i = 0; i < sysInfo.Length; i++)
                        {
                            TreeNode sysnode = PCNodes.Nodes.Add(sysInfo[i].SysNo.ToString(), sysInfo[i].SysName, 0, 0);
                            //우클릭 메뉴 추가
                            ContextMenuStrip sysNodesConMenu = new ContextMenuStrip();
                            
                            ToolStripMenuItem sysrenameLabel = new ToolStripMenuItem();
                            sysrenameLabel.Text = "수정";
                            sysrenameLabel.Click += new EventHandler((sender, e) => SognoSysUpdate(sender, e, selectedPcNo, selectedSysNo, 0));

                            ToolStripMenuItem sysdeleteLabel = new ToolStripMenuItem();
                            sysdeleteLabel.Text = "삭제";
                            sysdeleteLabel.Click += new EventHandler((sender, e) => SognoSysUpdate(sender, e, selectedPcNo, selectedSysNo, 1));

                            sysNodesConMenu.Items.AddRange(new ToolStripMenuItem[] { sysrenameLabel, sysdeleteLabel });
                            sysnode.ContextMenuStrip = sysNodesConMenu;
                            //우클릭 메뉴 추가 끝
                        }
                    }
                }
            }

            //여기부터 게이트 그룹 단위 트리 뷰 구현

            gategroupInfo = dbAcessor.SelectGateGroupInfo(this.selectedParkNo);
            if (gategroupInfo.Length >= 0)
            {
                for (int gategroup_i = 0; gategroup_i < gategroupInfo.Length; gategroup_i++) // 게이트 그룹 개별 불러내는 for문
                {
                    TreeNode gateGroupNodes = GateTreeView.Nodes.Add(gategroupInfo[gategroup_i].GateGroupNo.ToString(), gategroupInfo[gategroup_i].GateGroupName, 0, 0);
                    
                    //우클릭 메뉴 추가
                    ContextMenuStrip gateGroupNodesConMenu = new ContextMenuStrip();
                    ToolStripMenuItem newGateAddLabel = new ToolStripMenuItem();
                    newGateAddLabel.Text = "게이트 신규 추가...";
                    newGateAddLabel.Click += new EventHandler((sender, e) => SognoGateUpdate(sender, e, selectedGateGroupNo, null));

                    ToolStripMenuItem gateGroupRenameLabel = new ToolStripMenuItem();
                    gateGroupRenameLabel.Text = "수정";
                    gateGroupRenameLabel.Click += new EventHandler((sender, e) => SognoGateGroupUpdate(sender, e, selectedGateGroupNo, 0));

                    ToolStripMenuItem gateGroupDeleteLabel = new ToolStripMenuItem();
                    gateGroupDeleteLabel.Text = "삭제";
                    gateGroupDeleteLabel.Click += new EventHandler((sender, e) => SognoGateGroupUpdate(sender, e, selectedGateGroupNo, 1));

                    gateGroupNodesConMenu.Items.AddRange(new ToolStripMenuItem[] { newGateAddLabel, gateGroupRenameLabel, gateGroupDeleteLabel });
                    gateGroupNodes.ContextMenuStrip = gateGroupNodesConMenu;
                    //우클릭 메뉴 추가 끝

                    try
                    {
                        gateInfo = dbAcessor.SelectGateInfo(this.selectedParkNo, gategroupInfo[gategroup_i].GateGroupNo);
                    }
                    catch { }

                    if (gateInfo.Length >= 0)
                    {
                        for (int i = 0; i < gateInfo.Length; i++)
                        {
                            TreeNode GateNodes = gateGroupNodes.Nodes.Add(gateInfo[i].GateNo.ToString(), gateInfo[i].GateName, 3, 3);

                            //우클릭 메뉴 추가
                            ContextMenuStrip gateNodesConMenu = new ContextMenuStrip();
                            ToolStripMenuItem newCamAddLabel = new ToolStripMenuItem();
                            newCamAddLabel.Text = "카메라 신규 추가...";
                            newCamAddLabel.Click += new EventHandler((sender, e) => SognoCamUpdate(sender, e, selectedGateNo, null));

                            ToolStripMenuItem newBarAddLabel = new ToolStripMenuItem();
                            newBarAddLabel.Text = "차단기 신규 추가...";
                            newBarAddLabel.Click += new EventHandler((sender, e) => SognoBarUpdate(sender, e, selectedGateNo, null));

                            ToolStripMenuItem newScrAddLabel = new ToolStripMenuItem();
                            newScrAddLabel.Text = "전광판 신규 추가...";
                            newScrAddLabel.Click += new EventHandler((sender, e) => SognoScrUpdate(sender, e, selectedGateNo, null));

                            ToolStripMenuItem newRpiAddLabel = new ToolStripMenuItem();
                            newRpiAddLabel.Text = "라즈베리파이 신규 추가...";
                            newRpiAddLabel.Click += new EventHandler((sender, e) => SognoRpiUpdate(sender, e, selectedGateNo, null));

                            ToolStripMenuItem gateRenameLabel = new ToolStripMenuItem();
                            gateRenameLabel.Text = "수정";
                            gateRenameLabel.Click += new EventHandler((sender, e) => SognoGateUpdate(sender, e, selectedGateNo, 0));

                            ToolStripMenuItem gateDeleteLabel = new ToolStripMenuItem();
                            gateDeleteLabel.Text = "삭제";
                            gateDeleteLabel.Click += new EventHandler((sender, e) => SognoGateUpdate(sender, e, selectedGateNo, 1));

                            gateNodesConMenu.Items.AddRange(new ToolStripMenuItem[] { newCamAddLabel, newBarAddLabel, newScrAddLabel, newRpiAddLabel, gateRenameLabel, gateDeleteLabel });
                            GateNodes.ContextMenuStrip = gateNodesConMenu;
                            //우클릭 메뉴 추가 끝

                            // 데이터 조회
                            camInfo = dbAcessor.SelectCamInfo(this.selectedParkNo, gateInfo[i].GateNo); // 카메라
                            barInfo = dbAcessor.SelectBarInfo(this.selectedParkNo, gateInfo[i].GateNo); // 차단기
                            scrInfo = dbAcessor.SelectSCRInfo(this.selectedParkNo, gateInfo[i].GateNo); // 전광판
                            rpiInfo = dbAcessor.SelectRpiInfo(this.selectedParkNo, gateInfo[i].GateNo); // 라즈베리파이

                            //카메라
                            for (int cam_i = 0; cam_i < camInfo.Length; cam_i++)
                            {
                                TreeNode camNode = GateNodes.Nodes.Add(camInfo[cam_i].CamNo.ToString(), camInfo[cam_i].CamName, 1, 1);
                                //우클릭 메뉴 추가
                                ContextMenuStrip camNodesConMenu = new ContextMenuStrip();

                                ToolStripMenuItem camUpdateLabel = new ToolStripMenuItem();
                                camUpdateLabel.Text = "수정";
                                camUpdateLabel.Click += new EventHandler((sender, e) => SognoCamUpdate(sender, e, selectedCamNo, 0));

                                ToolStripMenuItem camDeleteLabel = new ToolStripMenuItem();
                                camDeleteLabel.Text = "삭제";
                                camDeleteLabel.Click += new EventHandler((sender, e) => SognoCamUpdate(sender, e, selectedCamNo, 1));

                                camNodesConMenu.Items.AddRange(new ToolStripMenuItem[] { camUpdateLabel, camDeleteLabel});
                                camNode.ContextMenuStrip = camNodesConMenu;
                                //우클릭 메뉴 추가 끝

                            }
                            //차단기
                            for (int bar_i = 0; bar_i < barInfo.Length; bar_i++)
                            {
                                TreeNode barNode = GateNodes.Nodes.Add(barInfo[bar_i].BarNo.ToString(), barInfo[bar_i].BarName, 2, 2);
                                //우클릭 메뉴 추가
                                ContextMenuStrip barNodesConMenu = new ContextMenuStrip();

                                ToolStripMenuItem barUpdateLabel = new ToolStripMenuItem();
                                barUpdateLabel.Text = "수정";
                                barUpdateLabel.Click += new EventHandler((sender, e) => SognoBarUpdate(sender, e, selectedBarNo, 0));

                                ToolStripMenuItem barDeleteLabel = new ToolStripMenuItem();
                                barDeleteLabel.Text = "삭제";
                                barDeleteLabel.Click += new EventHandler((sender, e) => SognoBarUpdate(sender, e, selectedBarNo, 1));

                                barNodesConMenu.Items.AddRange(new ToolStripMenuItem[] { barUpdateLabel, barDeleteLabel });
                                barNode.ContextMenuStrip = barNodesConMenu;
                                //우클릭 메뉴 추가 끝
                            }
                            //전광판
                            for (int scr_i = 0; scr_i < scrInfo.Length; scr_i++)
                            {
                                TreeNode scrNode = GateNodes.Nodes.Add(scrInfo[scr_i].SCRNo.ToString(), scrInfo[scr_i].SCRName, 3, 3);
                                //우클릭 메뉴 추가
                                ContextMenuStrip scrNodesConMenu = new ContextMenuStrip();

                                ToolStripMenuItem scrUpdateLabel = new ToolStripMenuItem();
                                scrUpdateLabel.Text = "수정";
                                scrUpdateLabel.Click += new EventHandler((sender, e) => SognoScrUpdate(sender, e, selectedScrNo, 0));

                                ToolStripMenuItem scrDeleteLabel = new ToolStripMenuItem();
                                scrDeleteLabel.Text = "삭제";
                                scrDeleteLabel.Click += new EventHandler((sender, e) => SognoScrUpdate(sender, e, selectedScrNo, 1));

                                scrNodesConMenu.Items.AddRange(new ToolStripMenuItem[] { scrUpdateLabel, scrDeleteLabel });
                                scrNode.ContextMenuStrip = scrNodesConMenu;
                                //우클릭 메뉴 추가 끝

                            }
                            //라즈베리파이
                            for (int rpi_i = 0; rpi_i < rpiInfo.Length; rpi_i++)
                            {
                                TreeNode rpiNode = GateNodes.Nodes.Add(rpiInfo[rpi_i].RpiNo.ToString(), rpiInfo[rpi_i].RpiName, 0, 0);
                                
                                //우클릭 메뉴 추가
                                ContextMenuStrip rpiNodesConMenu = new ContextMenuStrip();

                                ToolStripMenuItem rpiUpdateLabel = new ToolStripMenuItem();
                                rpiUpdateLabel.Text = "수정";
                                rpiUpdateLabel.Click += new EventHandler((sender, e) => SognoRpiUpdate(sender, e, selectedRPiNo, 0));

                                ToolStripMenuItem rpiDeleteLabel = new ToolStripMenuItem();
                                rpiDeleteLabel.Text = "삭제";
                                rpiDeleteLabel.Click += new EventHandler((sender, e) => SognoRpiUpdate(sender, e, selectedRPiNo, 1));

                                rpiNodesConMenu.Items.AddRange(new ToolStripMenuItem[] { rpiUpdateLabel, rpiDeleteLabel });
                                rpiNode.ContextMenuStrip = rpiNodesConMenu;
                                //우클릭 메뉴 추가 끝
                            }

                        }
                        // 모든 트리 노드를 보여준다
                        
                    }
                }
            }
            GateTreeView.ExpandAll();
        }

        private void GateTreeView_AfterSelect(object sender, TreeViewEventArgs e) // 트리뷰에서 하나 골랐을 때 
        {
            
        }
        //상단 콤보박스에서 주차장 골랐을 때 이벤트
        private void ParkListComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ParkListComboBox.SelectedIndex == -1)
            {
                return;
            }
            int num;
            bool isNum = int.TryParse(ParkListComboBox.SelectedValue.ToString(), out num);
            if (isNum)
            {
                selectedParkNo = Int32.Parse(ParkListComboBox.SelectedValue.ToString());
                SpreadDataInTreeView();
            }
        }

        private void TreeRefreshBT_Click(object sender, EventArgs e)
        {
            SpreadDataInTreeView();
        }
        private void SognoPCButton_Click(object sender, EventArgs e ) // request 0 : update, 1 : delete
        {
            SognoPCView pCView = new SognoPCView(this.dbAcessor, selectedParkNo, parkInfo);
            pCView.ShowDialog();
        }

        private void SognoPCUpdate(
    object sender, EventArgs e,
    int? selectedGateNo,
    GateInfoRecord[] gateinfos = null,
    int? selectedPcNo = null,
    int? reQuest = null) // request 0 : update, 1 : delete
        {
            SognoPCView pCView = new SognoPCView(this.dbAcessor, selectedParkNo, parkInfo, selectedGateNo, gateinfos, selectedPcNo, reQuest);
            try
            {
                pCView.ShowDialog();
            }
            catch { }
            SpreadDataInTreeView();
        }
        private void systemViewBT_Click(object sender, EventArgs e)
        {
            SognoSysView sysView = new SognoSysView(this.dbAcessor, selectedParkNo, parkInfo);
            sysView.ShowDialog();
        }

        private void SognoSysUpdate(object sender, EventArgs e,
            int? selectedPCNo = null,
            int? selectedSysNo = null,
            int? reQuest = null
            )
        {
            SognoSysView sysView = new SognoSysView(this.dbAcessor, selectedParkNo, parkInfo, selectedPCNo, selectedSysNo, reQuest);
            try
            {
                sysView.ShowDialog();
            }
            catch { }
            SpreadDataInTreeView();
        }

        private void GateGroupViewBT_Click(object sender, EventArgs e)
        {
            SognoGateGroupView gateGroupView = new SognoGateGroupView(this.dbAcessor, selectedParkNo, parkInfo);
            gateGroupView.ShowDialog();
        }
        private void SognoGateGroupUpdate(object sender, EventArgs e,
            int? selectedGateGroupNo,
            int? reQuest
            )
        {
            SognoGateGroupView gateGroupView = new SognoGateGroupView(this.dbAcessor, selectedParkNo, parkInfo, selectedGateGroupNo, reQuest);
            gateGroupView.ShowDialog();
        }

        private void GateViewBT_Click(object sender, EventArgs e)
        {
            SognoGateView gateView = new SognoGateView(this.dbAcessor, selectedParkNo, parkInfo, gategroupInfo);
            gateView.ShowDialog();
        }

        private void SognoGateUpdate(object sender, EventArgs e,
            int? selectedGateNo,
            int? reQuest
            )
        {
            SognoGateView gateView = new SognoGateView(this.dbAcessor, selectedParkNo, parkInfo, gategroupInfo, selectedGateGroupNo, selectedGateNo, reQuest);
            gateView.ShowDialog();
        }

        private void cameraViewBT_Click(object sender, EventArgs e)
        {
            SognoCamView camView = new SognoCamView(this.dbAcessor, selectedParkNo, parkInfo);
            camView.ShowDialog();
        }
        private void SognoCamUpdate(object sender, EventArgs e,  int? selectedCamNo, int? reQuest)
        {
            SognoCamView camView = new SognoCamView(this.dbAcessor, selectedParkNo, parkInfo, selectedGateNo, selectedCamNo, reQuest);
            camView.ShowDialog();
        }



        private void BarViewBT_Click(object sender, EventArgs e)
        {
            SognoBarView BarView = new SognoBarView(this.dbAcessor, selectedParkNo, parkInfo);
            BarView.ShowDialog();
        }
        
        private void SognoBarUpdate(object sender, EventArgs e, int? selectedBarNo, int? reQuest)
        {
            SognoBarView BarView = new SognoBarView(this.dbAcessor, selectedParkNo, parkInfo, selectedGateNo, selectedBarNo, reQuest);
            BarView.ShowDialog();
        }

        private void ScreenViewBT_Click(object sender, EventArgs e)
        {
            SognoSCRView ScrView = new SognoSCRView(this.dbAcessor, selectedParkNo, parkInfo);
            ScrView.ShowDialog();
        }
        
        private void SognoScrUpdate(object sender, EventArgs e,int? selectedScrNo, int? reQuest)
        {
            SognoSCRView ScrView = new SognoSCRView(this.dbAcessor, selectedParkNo, parkInfo, selectedGateNo, selectedScrNo, reQuest);
            ScrView.ShowDialog();
        }

        private void RpiViewBT_Click(object sender, EventArgs e)
        {
            SognoRpiView RpiView = new SognoRpiView(this.dbAcessor, selectedParkNo, parkInfo);
            RpiView.ShowDialog();
        }
        
        private void SognoRpiUpdate(object sender, EventArgs e,int? selectedRpiNo, int? reQuest)
        {
            SognoRpiView RpiView = new SognoRpiView(this.dbAcessor, selectedParkNo, parkInfo, selectedGateNo, selectedRpiNo, reQuest);
            RpiView.ShowDialog();
        }
        private void GateTreeView_DoubleClick(object sender, EventArgs e)
        {

        }

        private void GateTreeView_AfterSelect(object sender, TreeViewCancelEventArgs e)
        {

        }

        private void GateTreeView_Click(object sender, EventArgs e)
        {
            
        }

        private void GateTreeView_MouseDown(object sender, MouseEventArgs e)
        {
            try
            { 
                TreeNode tn = GateTreeView.GetNodeAt(e.Location);
                GateTreeView.SelectedNode = tn;

                switch (GateTreeView.SelectedNode.Level)
                {
                case 0: //(게이트 그룹 개별, 혹은 PC 개별)
                    if (GateTreeView.SelectedNode.ImageIndex == 0)
                    {
                        selectedGateGroupNo = Int32.Parse(GateTreeView.SelectedNode.Name);
                        selectedGateGroupInfo = dbAcessor.SelectGateGroupInfo(selectedParkNo, selectedGateGroupNo)[0];
                        selectedDataType = "GATEGROUP";
                    }
                    else
                    {
                        selectedPcNo = Int32.Parse(GateTreeView.SelectedNode.Name);
                        selectedPcInfo = dbAcessor.SelectPCInfo(selectedParkNo, null, selectedPcNo)[0];
                        selectedDataType = "PC";
                    }
                    break;

                case 1: // (게이트 개별, 혹은 시스템 개별)
                    if (GateTreeView.SelectedNode.ImageIndex == 3)
                    {
                        selectedGateGroupNo = Int32.Parse(GateTreeView.SelectedNode.Parent.Name);
                        selectedGateNo = Int32.Parse(GateTreeView.SelectedNode.Name);
                        selectedGateInfo = dbAcessor.SelectGateInfo(selectedParkNo, selectedGateGroupNo, selectedGateNo)[0];
                        selectedDataType = "GATE";
                    }
                    else
                    {
                        selectedPcNo = Int32.Parse(GateTreeView.SelectedNode.Parent.Name);
                        selectedSysNo = Int32.Parse(GateTreeView.SelectedNode.Name);
                        selectedSysInfo = dbAcessor.SelectSysInfo(selectedParkNo, selectedPcNo, selectedSysNo)[0];
                        selectedDataType = "SYS";
                    }
                    break;

                case 2: // 소분류 (중분류(카메라,전광판 등)의 각 항목)
                    selectedGateNo = Int32.Parse(GateTreeView.SelectedNode.Parent.Name);
                    selectedNodeNo = Int32.Parse(GateTreeView.SelectedNode.Name);
                    switch (GateTreeView.SelectedNode.ImageIndex)
                    {
                        case 1:
                            selectedCamNo = selectedNodeNo;
                            selectedCamInfo = dbAcessor.SelectCamInfo(selectedParkNo, selectedGateNo, selectedCamNo)[0];
                            selectedDataType = "CAM";
                            break;

                        case 2:
                            selectedBarNo = selectedNodeNo;
                            selectedBarInfo = dbAcessor.SelectBarInfo(selectedParkNo, selectedGateNo, selectedBarNo)[0];
                            selectedDataType = "BAR";
                            break;

                        case 3:
                            selectedScrNo = selectedNodeNo;
                            selectedScrInfo = dbAcessor.SelectSCRInfo(selectedParkNo, selectedGateNo, selectedScrNo)[0];
                            selectedDataType = "SCR";
                            break;

                        case 0:
                            selectedRPiNo = selectedNodeNo;
                            selectedRpiInfo = dbAcessor.SelectRpiInfo(selectedParkNo, selectedGateNo, selectedRPiNo)[0];
                            selectedDataType = "Rpi";
                            break;

                        default:

                            break;
                    }
                    break;
                }
            }
            catch { }
        }
    }
}
