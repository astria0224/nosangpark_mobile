﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SognoLib.Park.Communication
{
    public enum EParkMsgID
    {
        Status,
        GetStatus,
        LprResult,
        LprResult_ACK,
        BarCtrl,
        ScrCtrl,
        Count,
        IpsImage,
        IpsResult,
        LwvImage,
        LwvResult,
        MatchImage,
        MatchResult
    }


    public enum EImageDataType
    {
        Jpeg,
        Raw,
        LocalPath,
        RemotePath
    }


}
