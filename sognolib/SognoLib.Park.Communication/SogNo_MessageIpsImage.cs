﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SognoLib.Core;


namespace SognoLib.Park.Communication
{
    public class SogNo_MessageIpsImage : SogNo_Message
    {
        public EImageDataType ImageType { get; set; } = EImageDataType.Raw;
        public byte[] ImageData { get; set; } = null;
        public string ImagePath { get; set; }
        public int ImageWidth { get; set; } = 0;
        public int ImageHeight { get; set; } = 0;
        public int ImageChannels { get; set; } = 1;
        public string ImageNo { get; set; }

        public override byte[] Pack()
        {
            string imgDesc = string.Format("{0}#{1}#{2}#{3}#{4}#{5}#{6}#{7}#", EParkMsgID.IpsImage, ImageNo, ImageType, ImagePath, ImageWidth, ImageHeight, ImageChannels, (ImageData?.Length ?? 0));
            byte[] imgDescBytes = Encoding.Default.GetBytes(imgDesc);
            int descLen = imgDescBytes.Length;

            int bodyLen = descLen + ImageData.Length;
            string msgHeader = string.Format("{0}{1}{2:D8}#", CharSTX, CharBinarySign, bodyLen);
            byte[] msgHeaderBytes = Encoding.Default.GetBytes(msgHeader);

            byte[] message = msgHeaderBytes.Concat(imgDescBytes).Concat(ImageData).Concat(new byte[] { ETX }).ToArray();

            return message;
        }

        public override void UnPack(string messageBody)
        {
            throw new NotImplementedException();
        }

        public override void UnPack(byte[] messageBody)
        {
            string msgID = GetMsgID(messageBody);
            if (msgID != EParkMsgID.IpsImage.ToString())
            {
                throw new Exception("메시지 ID가 일치하지 않음.");
            }

            int lastSeperatorIndex = -1;
            string[] tokens = new string[8];
            for (int i = 0; i < 8; i++)
            {
                int nextSeperatorIndex = Array.IndexOf(messageBody, Seperator, lastSeperatorIndex + 1);
                if (nextSeperatorIndex < 0)
                {
                    throw new Exception("토큰의 갯수가 일치하지 않음.");
                }

                tokens[i] = Encoding.Default.GetString(messageBody, lastSeperatorIndex + 1, nextSeperatorIndex - lastSeperatorIndex - 1);
                lastSeperatorIndex = nextSeperatorIndex;
            }

            try
            {
                this.ImageNo = tokens[1];
                this.ImageType = (EImageDataType)Enum.Parse(typeof(EImageDataType), tokens[2]);
                this.ImagePath = tokens[3];
                this.ImageWidth = Int32.Parse(tokens[4]);
                this.ImageHeight = Int32.Parse(tokens[5]);
                this.ImageChannels = Int32.Parse(tokens[6]);
                int imageDataLength = Int32.Parse(tokens[7]);

                if (imageDataLength != messageBody.Length - (lastSeperatorIndex + 1))
                {
                    throw new Exception();
                }

                this.ImageData = new byte[imageDataLength];
                Array.Copy(messageBody, lastSeperatorIndex + 1, this.ImageData, 0, imageDataLength);
            }
            catch (Exception)
            {
                throw new Exception("메시지 형식이 일치하지 않습니다.");
            }
        }
    }
}
