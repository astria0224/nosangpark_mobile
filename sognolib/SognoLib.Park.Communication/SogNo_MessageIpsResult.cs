﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SognoLib.Core;


namespace SognoLib.Park.Communication
{
    public class SogNo_MessageIpsResult : SogNo_Message
    {
        public string ImageNo { get; set; }
        public string CarNumber { get; set; }
        public double RecError { get; set; }
        public int RecTime { get; set; }

        public Point[] Corners { get; set; } = null;
        public Point[] CornersForMatching { get; set; } = null;
        public Point[] CornersForLWV { get; set; } = null;

        public override byte[] Pack()
        {
            string location = CornersToString(this.Corners);
            string locationMatching = CornersToString(this.CornersForMatching);
            string locationLWV = CornersToString(this.CornersForLWV);

            string msgBody = string.Format("{0}#{1}#{2}#{3}#{4}#{5}#{6:F6}#{7}", EParkMsgID.IpsResult, this.ImageNo, this.CarNumber, location, locationMatching, locationLWV, this.RecError, this.RecTime);
            int bodyLen = Encoding.Default.GetBytes(msgBody).Length;

            string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, CharTextSign, bodyLen, msgBody, CharETX);
            return Encoding.Default.GetBytes(message);
        }

        private string CornersToString(Point[] corners)
        {
            string location = "";
            if (corners != null)
            {
                foreach (Point pt in corners)
                {
                    if (location.Length > 0)
                        location += ",";

                    location += string.Format("{0},{1}", (int)pt.x, (int)pt.y);
                }
            }

            return location;
        }

        public override void UnPack(string messageBody)
        {
            string msgID = GetMsgID(messageBody);
            if (msgID != EParkMsgID.IpsResult.ToString())
            {
                throw new Exception("메시지 ID가 일치하지 않음.");
            }

            string[] tokens = messageBody.Split(new char[1] { '#' });
            if (tokens.Length != 8)
            {
                throw new Exception("토큰의 갯수가 일치하지 않음.");
            }

            try
            {
                this.ImageNo = tokens[1];
                this.CarNumber = tokens[2];
                string location = tokens[3];
                string locationMatching = tokens[4];
                string locationLWV = tokens[5];
                this.RecError = double.Parse(tokens[6]);
                this.RecTime = int.Parse(tokens[7]);

                this.Corners = StringToCorners(location);
                this.CornersForMatching = StringToCorners(locationMatching);
                this.CornersForLWV = StringToCorners(locationLWV);
            }
            catch (Exception)
            {
                throw new Exception("메시지 형식이 일치하지 않습니다.");
            }
        }


        private Point[] StringToCorners(string location)
        {
            Point[] corners = null;

            if (location.Length > 0)
            {
                string[] locTokens = location.Split(new char[1] { ',' });
                if (locTokens.Length != 8)
                {
                    throw new Exception("번호판의 네 모서리 점 정보가 부적절함.");
                }

                corners = new Point[4];
                for (int i = 0; i < locTokens.Length; i += 2)
                {
                    Point pt;
                    pt.x = int.Parse(locTokens[i]);
                    pt.y = int.Parse(locTokens[i + 1]);

                    corners[i / 2] = pt;
                }
            }

            return corners;
        }
    }
}
