﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SognoLib.Core;


namespace SognoLib.Park.Communication
{
    public class SogNo_MessageLwvImage : SogNo_Message
    {
        public EImageDataType ImageType { get; set; } = EImageDataType.Raw;
        public byte[] ImageData { get; set; } = null;
        public string ImagePath { get; set; }
        public int ImageWidth { get; set; } = 0;
        public int ImageHeight { get; set; } = 0;
        public int ImageChannels { get; set; } = 1;
        public string ImageNo { get; set; }
        public string CarNumber { get; set; }
        public Point[] CornersForLWV { get; set; } = null;

        public override byte[] Pack()
        {
            string locationLWV = CornersToString(this.CornersForLWV);

            string imgDesc = string.Format("{0}#{1}#{2}#{3}#{4}#{5}#{6}#{7}#{8}#{9}#", EParkMsgID.LwvImage, ImageNo, ImageType, ImagePath, ImageWidth, ImageHeight, ImageChannels, (ImageData?.Length ?? 0), CarNumber, locationLWV);
            byte[] imgDescBytes = Encoding.Default.GetBytes(imgDesc);
            int descLen = imgDescBytes.Length;

            int bodyLen = descLen + ImageData.Length;
            string msgHeader = string.Format("{0}{1}{2:D8}#", CharSTX, CharBinarySign, bodyLen);
            byte[] msgHeaderBytes = Encoding.Default.GetBytes(msgHeader);

            byte[] message = msgHeaderBytes.Concat(imgDescBytes).Concat(ImageData).Concat(new byte[] { ETX }).ToArray();

            return message;
        }

        private string CornersToString(Point[] corners)
        {
            string location = "";
            if (corners != null)
            {
                foreach (Point pt in corners)
                {
                    if (location.Length > 0)
                        location += ",";

                    location += string.Format("{0},{1}", (int)pt.x, (int)pt.y);
                }
            }

            return location;
        }

        public override void UnPack(string messageBody)
        {
            throw new NotImplementedException();
        }

        public override void UnPack(byte[] messageBody)
        {
            string msgID = GetMsgID(messageBody);
            if (msgID != EParkMsgID.LwvImage.ToString())
            {
                throw new Exception("메시지 ID가 일치하지 않음.");
            }

            int lastSeperatorIndex = -1;
            string[] tokens = new string[10];
            for (int i = 0; i < 10; i++)
            {
                int nextSeperatorIndex = Array.IndexOf(messageBody, Seperator, lastSeperatorIndex + 1);
                if (nextSeperatorIndex < 0)
                {
                    throw new Exception("토큰의 갯수가 일치하지 않음.");
                }

                tokens[i] = Encoding.Default.GetString(messageBody, lastSeperatorIndex + 1, nextSeperatorIndex - lastSeperatorIndex - 1);
                lastSeperatorIndex = nextSeperatorIndex;
            }

            try
            {
                this.ImageNo = tokens[1];
                this.ImageType = (EImageDataType)Enum.Parse(typeof(EImageDataType), tokens[2]);
                this.ImagePath = tokens[3];
                this.ImageWidth = Int32.Parse(tokens[4]);
                this.ImageHeight = Int32.Parse(tokens[5]);
                this.ImageChannels = Int32.Parse(tokens[6]);
                int imageDataLength = Int32.Parse(tokens[7]);
                this.CarNumber = tokens[8];
                string locationLWV = tokens[9];

                if (imageDataLength != messageBody.Length - (lastSeperatorIndex + 1))
                {
                    throw new Exception();
                }

                this.CornersForLWV = StringToCorners(locationLWV);

                this.ImageData = new byte[imageDataLength];
                Array.Copy(messageBody, lastSeperatorIndex + 1, this.ImageData, 0, imageDataLength);
            }
            catch (Exception)
            {
                throw new Exception("메시지 형식이 일치하지 않습니다.");
            }
        }

        private Point[] StringToCorners(string location)
        {
            Point[] corners = null;

            if (location.Length > 0)
            {
                string[] locTokens = location.Split(new char[1] { ',' });
                if (locTokens.Length != 8)
                {
                    throw new Exception("번호판의 네 모서리 점 정보가 부적절함.");
                }

                corners = new Point[4];
                for (int i = 0; i < locTokens.Length; i += 2)
                {
                    Point pt;
                    pt.x = int.Parse(locTokens[i]);
                    pt.y = int.Parse(locTokens[i + 1]);

                    corners[i / 2] = pt;
                }
            }

            return corners;
        }
    }
}
