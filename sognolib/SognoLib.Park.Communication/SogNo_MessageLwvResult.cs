﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SognoLib.Core;


namespace SognoLib.Park.Communication
{
    public class SogNo_MessageLwvResult : SogNo_Message
    {
        public string ImageNo { get; set; }
        public bool IsLightVehicle { get; set; } = false;
        public double RecScore { get; set; } = -1.0;
        public int RecTime { get; set; } = 0;


        public override byte[] Pack()
        {
            string msgBody = string.Format("{0}#{1}#{2}#{3:F3}#{4}",
                EParkMsgID.LwvResult, this.ImageNo, (this.IsLightVehicle ? 1 : 0), this.RecScore, this.RecTime);
            int bodyLen = Encoding.Default.GetBytes(msgBody).Length;

            string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, CharTextSign, bodyLen, msgBody, CharETX);
            return Encoding.Default.GetBytes(message);
        }

        public override void UnPack(string messageBody)
        {
            string msgID = GetMsgID(messageBody);
            if (msgID != EParkMsgID.LwvResult.ToString())
            {
                throw new Exception("메시지 ID가 일치하지 않음.");
            }

            string[] tokens = messageBody.Split(new char[1] { '#' });
            if (tokens.Length != 5)
            {
                throw new Exception("토큰의 갯수가 일치하지 않음.");
            }

            try
            {
                this.ImageNo = tokens[1];
                this.IsLightVehicle = (int.Parse(tokens[2]) == 0 ? false : true);
                this.RecScore = double.Parse(tokens[3]);
                this.RecTime = int.Parse(tokens[4]);
            }
            catch (Exception)
            {
                throw new Exception("메시지 형식이 일치하지 않습니다.");
            }
        }
    }
}
