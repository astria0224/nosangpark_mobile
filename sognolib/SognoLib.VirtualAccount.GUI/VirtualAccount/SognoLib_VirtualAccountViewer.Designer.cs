﻿namespace SognoLib.VirtualAccount.GUI.VirtualAccount
{
    partial class SognoLib_VirtualAccountViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.btnAcc_Update = new System.Windows.Forms.Button();
            this.btnAcc_Del = new System.Windows.Forms.Button();
            this.btnAcc_New = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.cboxSearch_States = new System.Windows.Forms.ComboBox();
            this.cboxSearch_Assign = new System.Windows.Forms.ComboBox();
            this.tboxSearch_CarNum = new System.Windows.Forms.TextBox();
            this.tboxSearch_Name = new System.Windows.Forms.TextBox();
            this.cboxSearch_ParkName = new System.Windows.Forms.ComboBox();
            this.dtpSearch_CreateDate = new System.Windows.Forms.DateTimePicker();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btnSearch_Clear = new System.Windows.Forms.Button();
            this.btnSearch_Run = new System.Windows.Forms.Button();
            this.dviewAcc = new System.Windows.Forms.DataGridView();
            this.No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Assign = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Account = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.States = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Deadline = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Createtime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParkName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CarNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CustName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.labelStat = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dviewAcc)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(153, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "주차장 : ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(3, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(153, 39);
            this.label2.TabIndex = 1;
            this.label2.Text = "차량번호 : ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(3, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(153, 40);
            this.label3.TabIndex = 2;
            this.label3.Text = "이름 : ";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(400, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(153, 39);
            this.label4.TabIndex = 3;
            this.label4.Text = "부여여부 : ";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(400, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(153, 39);
            this.label5.TabIndex = 4;
            this.label5.Text = "상태 : ";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(400, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(153, 40);
            this.label6.TabIndex = 5;
            this.label6.Text = "생성일 : ";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label6.Visible = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dviewAcc, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelStat, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1252, 584);
            this.tableLayoutPanel1.TabIndex = 7;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 64.36597F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.98395F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.73034F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel5, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1246, 124);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.btnAcc_Update, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.btnAcc_Del, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.btnAcc_New, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(1052, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 3;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(191, 118);
            this.tableLayoutPanel5.TabIndex = 2;
            // 
            // btnAcc_Update
            // 
            this.btnAcc_Update.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAcc_Update.Location = new System.Drawing.Point(3, 81);
            this.btnAcc_Update.Name = "btnAcc_Update";
            this.btnAcc_Update.Size = new System.Drawing.Size(185, 34);
            this.btnAcc_Update.TabIndex = 2;
            this.btnAcc_Update.Text = "가상계좌 상태갱신";
            this.btnAcc_Update.UseVisualStyleBackColor = true;
            this.btnAcc_Update.Visible = false;
            // 
            // btnAcc_Del
            // 
            this.btnAcc_Del.BackColor = System.Drawing.Color.OrangeRed;
            this.btnAcc_Del.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAcc_Del.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAcc_Del.Font = new System.Drawing.Font("제주고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAcc_Del.ForeColor = System.Drawing.Color.White;
            this.btnAcc_Del.Location = new System.Drawing.Point(3, 42);
            this.btnAcc_Del.Name = "btnAcc_Del";
            this.btnAcc_Del.Size = new System.Drawing.Size(185, 33);
            this.btnAcc_Del.TabIndex = 1;
            this.btnAcc_Del.Text = "가상계좌 해지요청";
            this.btnAcc_Del.UseVisualStyleBackColor = false;
            this.btnAcc_Del.Visible = false;
            // 
            // btnAcc_New
            // 
            this.btnAcc_New.BackColor = System.Drawing.Color.OrangeRed;
            this.btnAcc_New.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAcc_New.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAcc_New.Font = new System.Drawing.Font("제주고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAcc_New.ForeColor = System.Drawing.Color.White;
            this.btnAcc_New.Location = new System.Drawing.Point(3, 3);
            this.btnAcc_New.Name = "btnAcc_New";
            this.btnAcc_New.Size = new System.Drawing.Size(185, 33);
            this.btnAcc_New.TabIndex = 0;
            this.btnAcc_New.Text = "가상계좌 추가요청";
            this.btnAcc_New.UseVisualStyleBackColor = false;
            this.btnAcc_New.Visible = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel2.Controls.Add(this.cboxSearch_States, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.cboxSearch_Assign, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label6, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label5, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.label4, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.tboxSearch_CarNum, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.tboxSearch_Name, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.cboxSearch_ParkName, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.dtpSearch_CreateDate, 3, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(795, 118);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // cboxSearch_States
            // 
            this.cboxSearch_States.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboxSearch_States.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxSearch_States.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cboxSearch_States.FormattingEnabled = true;
            this.cboxSearch_States.Location = new System.Drawing.Point(559, 42);
            this.cboxSearch_States.Name = "cboxSearch_States";
            this.cboxSearch_States.Size = new System.Drawing.Size(233, 32);
            this.cboxSearch_States.TabIndex = 10;
            // 
            // cboxSearch_Assign
            // 
            this.cboxSearch_Assign.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboxSearch_Assign.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxSearch_Assign.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cboxSearch_Assign.FormattingEnabled = true;
            this.cboxSearch_Assign.Location = new System.Drawing.Point(559, 3);
            this.cboxSearch_Assign.Name = "cboxSearch_Assign";
            this.cboxSearch_Assign.Size = new System.Drawing.Size(233, 32);
            this.cboxSearch_Assign.TabIndex = 9;
            // 
            // tboxSearch_CarNum
            // 
            this.tboxSearch_CarNum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tboxSearch_CarNum.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxSearch_CarNum.Location = new System.Drawing.Point(162, 42);
            this.tboxSearch_CarNum.Name = "tboxSearch_CarNum";
            this.tboxSearch_CarNum.Size = new System.Drawing.Size(232, 31);
            this.tboxSearch_CarNum.TabIndex = 6;
            // 
            // tboxSearch_Name
            // 
            this.tboxSearch_Name.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tboxSearch_Name.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxSearch_Name.Location = new System.Drawing.Point(162, 81);
            this.tboxSearch_Name.Name = "tboxSearch_Name";
            this.tboxSearch_Name.Size = new System.Drawing.Size(232, 31);
            this.tboxSearch_Name.TabIndex = 7;
            // 
            // cboxSearch_ParkName
            // 
            this.cboxSearch_ParkName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboxSearch_ParkName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxSearch_ParkName.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cboxSearch_ParkName.FormattingEnabled = true;
            this.cboxSearch_ParkName.Location = new System.Drawing.Point(162, 3);
            this.cboxSearch_ParkName.Name = "cboxSearch_ParkName";
            this.cboxSearch_ParkName.Size = new System.Drawing.Size(232, 32);
            this.cboxSearch_ParkName.TabIndex = 8;
            // 
            // dtpSearch_CreateDate
            // 
            this.dtpSearch_CreateDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtpSearch_CreateDate.Font = new System.Drawing.Font("굴림", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dtpSearch_CreateDate.Location = new System.Drawing.Point(559, 81);
            this.dtpSearch_CreateDate.Name = "dtpSearch_CreateDate";
            this.dtpSearch_CreateDate.Size = new System.Drawing.Size(233, 25);
            this.dtpSearch_CreateDate.TabIndex = 11;
            this.dtpSearch_CreateDate.Visible = false;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.btnSearch_Clear, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.btnSearch_Run, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(804, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(242, 118);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // btnSearch_Clear
            // 
            this.btnSearch_Clear.BackColor = System.Drawing.Color.Tan;
            this.btnSearch_Clear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSearch_Clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch_Clear.Font = new System.Drawing.Font("제주고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnSearch_Clear.ForeColor = System.Drawing.Color.Black;
            this.btnSearch_Clear.Location = new System.Drawing.Point(3, 62);
            this.btnSearch_Clear.Name = "btnSearch_Clear";
            this.btnSearch_Clear.Size = new System.Drawing.Size(236, 53);
            this.btnSearch_Clear.TabIndex = 1;
            this.btnSearch_Clear.Text = "초기화";
            this.btnSearch_Clear.UseVisualStyleBackColor = false;
            // 
            // btnSearch_Run
            // 
            this.btnSearch_Run.BackColor = System.Drawing.Color.BurlyWood;
            this.btnSearch_Run.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSearch_Run.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch_Run.Font = new System.Drawing.Font("제주고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnSearch_Run.ForeColor = System.Drawing.Color.Black;
            this.btnSearch_Run.Location = new System.Drawing.Point(3, 3);
            this.btnSearch_Run.Name = "btnSearch_Run";
            this.btnSearch_Run.Size = new System.Drawing.Size(236, 53);
            this.btnSearch_Run.TabIndex = 0;
            this.btnSearch_Run.Text = "검 색";
            this.btnSearch_Run.UseVisualStyleBackColor = false;
            // 
            // dviewAcc
            // 
            this.dviewAcc.AllowUserToAddRows = false;
            this.dviewAcc.AllowUserToDeleteRows = false;
            this.dviewAcc.AllowUserToOrderColumns = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("제주고딕", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dviewAcc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dviewAcc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dviewAcc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.No,
            this.Assign,
            this.Account,
            this.States,
            this.Amount,
            this.Deadline,
            this.Createtime,
            this.ParkName,
            this.CarNum,
            this.CustName});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("제주고딕", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dviewAcc.DefaultCellStyle = dataGridViewCellStyle4;
            this.dviewAcc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dviewAcc.Location = new System.Drawing.Point(3, 183);
            this.dviewAcc.Name = "dviewAcc";
            this.dviewAcc.ReadOnly = true;
            this.dviewAcc.RowHeadersVisible = false;
            this.dviewAcc.RowTemplate.Height = 30;
            this.dviewAcc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dviewAcc.Size = new System.Drawing.Size(1246, 398);
            this.dviewAcc.TabIndex = 2;
            // 
            // No
            // 
            this.No.HeaderText = "일련번호";
            this.No.Name = "No";
            this.No.ReadOnly = true;
            this.No.Width = 80;
            // 
            // Assign
            // 
            this.Assign.HeaderText = "부여여부";
            this.Assign.Name = "Assign";
            this.Assign.ReadOnly = true;
            this.Assign.Width = 80;
            // 
            // Account
            // 
            this.Account.HeaderText = "계좌번호";
            this.Account.Name = "Account";
            this.Account.ReadOnly = true;
            this.Account.Width = 120;
            // 
            // States
            // 
            this.States.HeaderText = "상태";
            this.States.Name = "States";
            this.States.ReadOnly = true;
            // 
            // Amount
            // 
            this.Amount.HeaderText = "금액";
            this.Amount.Name = "Amount";
            this.Amount.ReadOnly = true;
            // 
            // Deadline
            // 
            this.Deadline.FillWeight = 200F;
            this.Deadline.HeaderText = "마감시간";
            this.Deadline.Name = "Deadline";
            this.Deadline.ReadOnly = true;
            this.Deadline.Width = 200;
            // 
            // Createtime
            // 
            this.Createtime.FillWeight = 200F;
            this.Createtime.HeaderText = "생성일";
            this.Createtime.Name = "Createtime";
            this.Createtime.ReadOnly = true;
            this.Createtime.Width = 200;
            // 
            // ParkName
            // 
            this.ParkName.HeaderText = "주차장";
            this.ParkName.Name = "ParkName";
            this.ParkName.ReadOnly = true;
            this.ParkName.Width = 150;
            // 
            // CarNum
            // 
            this.CarNum.HeaderText = "차량번호";
            this.CarNum.Name = "CarNum";
            this.CarNum.ReadOnly = true;
            // 
            // CustName
            // 
            this.CustName.HeaderText = "이름";
            this.CustName.Name = "CustName";
            this.CustName.ReadOnly = true;
            this.CustName.Width = 80;
            // 
            // labelStat
            // 
            this.labelStat.AutoSize = true;
            this.labelStat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelStat.Font = new System.Drawing.Font("제주고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelStat.ForeColor = System.Drawing.Color.White;
            this.labelStat.Location = new System.Drawing.Point(3, 130);
            this.labelStat.Name = "labelStat";
            this.labelStat.Size = new System.Drawing.Size(1246, 50);
            this.labelStat.TabIndex = 3;
            this.labelStat.Text = "label7";
            this.labelStat.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SognoLib_VirtualAccountViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1252, 584);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "SognoLib_VirtualAccountViewer";
            this.Text = "SognoLib_VirtualAccountViewer";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dviewAcc)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public System.Windows.Forms.TextBox tboxSearch_CarNum;
        public System.Windows.Forms.ComboBox cboxSearch_States;
        public System.Windows.Forms.ComboBox cboxSearch_Assign;
        public System.Windows.Forms.TextBox tboxSearch_Name;
        public System.Windows.Forms.ComboBox cboxSearch_ParkName;
        public System.Windows.Forms.DataGridView dviewAcc;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        public System.Windows.Forms.Button btnAcc_Update;
        public System.Windows.Forms.Button btnAcc_Del;
        public System.Windows.Forms.Button btnAcc_New;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        public System.Windows.Forms.Button btnSearch_Clear;
        public System.Windows.Forms.Button btnSearch_Run;
        public System.Windows.Forms.DateTimePicker dtpSearch_CreateDate;
        public System.Windows.Forms.Label labelStat;
        private System.Windows.Forms.DataGridViewTextBoxColumn No;
        private System.Windows.Forms.DataGridViewTextBoxColumn Assign;
        private System.Windows.Forms.DataGridViewTextBoxColumn Account;
        private System.Windows.Forms.DataGridViewTextBoxColumn States;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Deadline;
        private System.Windows.Forms.DataGridViewTextBoxColumn Createtime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParkName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CarNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustName;
    }
}