﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SognoLib.VirtualAccount.GUI.VirtualAccount
{
    public partial class SognoLib_VirtualAccountViewer : Form
    {
        public SognoLib_VirtualAccountViewer()
        {
            InitializeComponent();
        }

        private void VirtualAccountViewer_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.ExitThread(); Environment.Exit(0);
        }
    }
}
