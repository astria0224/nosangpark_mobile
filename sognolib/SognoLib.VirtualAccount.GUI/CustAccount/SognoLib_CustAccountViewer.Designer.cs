﻿namespace SognoLib.VirtualAccount.GUI.CustAccount
{
    partial class SognoLib_CustAccountViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.btnAcc_Update = new System.Windows.Forms.Button();
            this.btnAcc_AssignDel = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.cboxSearch_States = new System.Windows.Forms.ComboBox();
            this.cboxSearch_Assign = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tboxSearch_CarNum = new System.Windows.Forms.TextBox();
            this.tboxSearch_Name = new System.Windows.Forms.TextBox();
            this.cboxSearch_ParkName = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btnSearch_Clear = new System.Windows.Forms.Button();
            this.btnSearch_Run = new System.Windows.Forms.Button();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.dviewAccount = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dviewCust = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.btnAcc_Assign = new System.Windows.Forms.Button();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.btnVirtualAccount = new System.Windows.Forms.Button();
            this.checkAcc = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Assign = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.No = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ParkName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CustName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CarNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TellNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dviewAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dviewCust)).BeginInit();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel6, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel7, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1302, 684);
            this.tableLayoutPanel1.TabIndex = 7;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 61.65123F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.35802F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.0679F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel5, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1296, 144);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.btnAcc_Update, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.btnAcc_AssignDel, 0, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(1012, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 3;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(281, 138);
            this.tableLayoutPanel5.TabIndex = 2;
            // 
            // btnAcc_Update
            // 
            this.btnAcc_Update.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAcc_Update.Location = new System.Drawing.Point(3, 95);
            this.btnAcc_Update.Name = "btnAcc_Update";
            this.btnAcc_Update.Size = new System.Drawing.Size(275, 40);
            this.btnAcc_Update.TabIndex = 2;
            this.btnAcc_Update.Text = "가상계좌 상태갱신";
            this.btnAcc_Update.UseVisualStyleBackColor = true;
            this.btnAcc_Update.Visible = false;
            // 
            // btnAcc_AssignDel
            // 
            this.btnAcc_AssignDel.BackColor = System.Drawing.Color.OrangeRed;
            this.btnAcc_AssignDel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAcc_AssignDel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAcc_AssignDel.Font = new System.Drawing.Font("제주고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAcc_AssignDel.ForeColor = System.Drawing.Color.White;
            this.btnAcc_AssignDel.Location = new System.Drawing.Point(3, 49);
            this.btnAcc_AssignDel.Name = "btnAcc_AssignDel";
            this.btnAcc_AssignDel.Size = new System.Drawing.Size(275, 40);
            this.btnAcc_AssignDel.TabIndex = 1;
            this.btnAcc_AssignDel.Text = "가상계좌 부여해지";
            this.btnAcc_AssignDel.UseVisualStyleBackColor = false;
            this.btnAcc_AssignDel.Visible = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.7393F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.19585F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel2.Controls.Add(this.cboxSearch_States, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.cboxSearch_Assign, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label5, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.label4, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.tboxSearch_CarNum, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.tboxSearch_Name, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.cboxSearch_ParkName, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(792, 138);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // cboxSearch_States
            // 
            this.cboxSearch_States.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboxSearch_States.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cboxSearch_States.FormattingEnabled = true;
            this.cboxSearch_States.Location = new System.Drawing.Point(556, 49);
            this.cboxSearch_States.Name = "cboxSearch_States";
            this.cboxSearch_States.Size = new System.Drawing.Size(233, 32);
            this.cboxSearch_States.TabIndex = 10;
            this.cboxSearch_States.Visible = false;
            // 
            // cboxSearch_Assign
            // 
            this.cboxSearch_Assign.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboxSearch_Assign.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxSearch_Assign.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cboxSearch_Assign.FormattingEnabled = true;
            this.cboxSearch_Assign.Location = new System.Drawing.Point(556, 3);
            this.cboxSearch_Assign.Name = "cboxSearch_Assign";
            this.cboxSearch_Assign.Size = new System.Drawing.Size(233, 32);
            this.cboxSearch_Assign.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 46);
            this.label1.TabIndex = 0;
            this.label1.Text = "주차장 : ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(3, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(152, 46);
            this.label2.TabIndex = 1;
            this.label2.Text = "차량번호 : ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(3, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 46);
            this.label3.TabIndex = 2;
            this.label3.Text = "이름 : ";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(404, 46);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(146, 46);
            this.label5.TabIndex = 4;
            this.label5.Text = "상태 : ";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label5.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(404, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(146, 46);
            this.label4.TabIndex = 3;
            this.label4.Text = "부여여부 : ";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tboxSearch_CarNum
            // 
            this.tboxSearch_CarNum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tboxSearch_CarNum.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxSearch_CarNum.Location = new System.Drawing.Point(161, 49);
            this.tboxSearch_CarNum.Name = "tboxSearch_CarNum";
            this.tboxSearch_CarNum.Size = new System.Drawing.Size(237, 31);
            this.tboxSearch_CarNum.TabIndex = 6;
            // 
            // tboxSearch_Name
            // 
            this.tboxSearch_Name.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tboxSearch_Name.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxSearch_Name.Location = new System.Drawing.Point(161, 95);
            this.tboxSearch_Name.Name = "tboxSearch_Name";
            this.tboxSearch_Name.Size = new System.Drawing.Size(237, 31);
            this.tboxSearch_Name.TabIndex = 7;
            // 
            // cboxSearch_ParkName
            // 
            this.cboxSearch_ParkName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboxSearch_ParkName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxSearch_ParkName.Font = new System.Drawing.Font("제주고딕", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cboxSearch_ParkName.FormattingEnabled = true;
            this.cboxSearch_ParkName.Location = new System.Drawing.Point(161, 3);
            this.cboxSearch_ParkName.Name = "cboxSearch_ParkName";
            this.cboxSearch_ParkName.Size = new System.Drawing.Size(237, 32);
            this.cboxSearch_ParkName.TabIndex = 8;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.btnSearch_Clear, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.btnSearch_Run, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(801, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(205, 138);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // btnSearch_Clear
            // 
            this.btnSearch_Clear.BackColor = System.Drawing.Color.Tan;
            this.btnSearch_Clear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSearch_Clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch_Clear.Font = new System.Drawing.Font("제주고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnSearch_Clear.ForeColor = System.Drawing.Color.Black;
            this.btnSearch_Clear.Location = new System.Drawing.Point(3, 72);
            this.btnSearch_Clear.Name = "btnSearch_Clear";
            this.btnSearch_Clear.Size = new System.Drawing.Size(199, 63);
            this.btnSearch_Clear.TabIndex = 1;
            this.btnSearch_Clear.Text = "초기화";
            this.btnSearch_Clear.UseVisualStyleBackColor = false;
            // 
            // btnSearch_Run
            // 
            this.btnSearch_Run.BackColor = System.Drawing.Color.BurlyWood;
            this.btnSearch_Run.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSearch_Run.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch_Run.Font = new System.Drawing.Font("제주고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnSearch_Run.ForeColor = System.Drawing.Color.Black;
            this.btnSearch_Run.Location = new System.Drawing.Point(3, 3);
            this.btnSearch_Run.Name = "btnSearch_Run";
            this.btnSearch_Run.Size = new System.Drawing.Size(199, 63);
            this.btnSearch_Run.TabIndex = 0;
            this.btnSearch_Run.Text = "검 색";
            this.btnSearch_Run.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.68827F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.31173F));
            this.tableLayoutPanel6.Controls.Add(this.dviewAccount, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.dviewCust, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 163);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1296, 468);
            this.tableLayoutPanel6.TabIndex = 4;
            // 
            // dviewAccount
            // 
            this.dviewAccount.AllowUserToAddRows = false;
            this.dviewAccount.AllowUserToDeleteRows = false;
            this.dviewAccount.AllowUserToOrderColumns = true;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("제주고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dviewAccount.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dviewAccount.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dviewAccount.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("제주고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dviewAccount.DefaultCellStyle = dataGridViewCellStyle6;
            this.dviewAccount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dviewAccount.Location = new System.Drawing.Point(633, 3);
            this.dviewAccount.Name = "dviewAccount";
            this.dviewAccount.ReadOnly = true;
            this.dviewAccount.RowHeadersVisible = false;
            this.dviewAccount.RowTemplate.Height = 23;
            this.dviewAccount.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dviewAccount.Size = new System.Drawing.Size(660, 462);
            this.dviewAccount.TabIndex = 3;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "계좌번호";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 150;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "금액";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Width = 120;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "상태";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 80;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "입금시간";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 150;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "마감시간";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.Width = 150;
            // 
            // dviewCust
            // 
            this.dviewCust.AllowUserToAddRows = false;
            this.dviewCust.AllowUserToDeleteRows = false;
            this.dviewCust.AllowUserToOrderColumns = true;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("제주고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dviewCust.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dviewCust.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dviewCust.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.checkAcc,
            this.Assign,
            this.No,
            this.ParkName,
            this.CustName,
            this.CarNum,
            this.TellNum});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("제주고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dviewCust.DefaultCellStyle = dataGridViewCellStyle8;
            this.dviewCust.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dviewCust.Location = new System.Drawing.Point(3, 3);
            this.dviewCust.Name = "dviewCust";
            this.dviewCust.ReadOnly = true;
            this.dviewCust.RowHeadersVisible = false;
            this.dviewCust.RowTemplate.Height = 30;
            this.dviewCust.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dviewCust.Size = new System.Drawing.Size(624, 462);
            this.dviewCust.TabIndex = 2;
            this.dviewCust.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dviewCust_CellContentClick);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 48.61111F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 51.38889F));
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel8, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel9, 1, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 637);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(1296, 44);
            this.tableLayoutPanel7.TabIndex = 5;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70.94703F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.05297F));
            this.tableLayoutPanel8.Controls.Add(this.btnAcc_Assign, 1, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(623, 38);
            this.tableLayoutPanel8.TabIndex = 0;
            // 
            // btnAcc_Assign
            // 
            this.btnAcc_Assign.BackColor = System.Drawing.Color.OrangeRed;
            this.btnAcc_Assign.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAcc_Assign.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAcc_Assign.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnAcc_Assign.ForeColor = System.Drawing.Color.White;
            this.btnAcc_Assign.Location = new System.Drawing.Point(444, 3);
            this.btnAcc_Assign.Name = "btnAcc_Assign";
            this.btnAcc_Assign.Size = new System.Drawing.Size(176, 32);
            this.btnAcc_Assign.TabIndex = 0;
            this.btnAcc_Assign.Text = "가상계좌 부여";
            this.btnAcc_Assign.UseVisualStyleBackColor = false;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 72.65745F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.34255F));
            this.tableLayoutPanel9.Controls.Add(this.btnVirtualAccount, 1, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(632, 3);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(661, 38);
            this.tableLayoutPanel9.TabIndex = 1;
            // 
            // btnVirtualAccount
            // 
            this.btnVirtualAccount.BackColor = System.Drawing.Color.OrangeRed;
            this.btnVirtualAccount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnVirtualAccount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVirtualAccount.Font = new System.Drawing.Font("제주고딕", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnVirtualAccount.ForeColor = System.Drawing.Color.White;
            this.btnVirtualAccount.Location = new System.Drawing.Point(483, 3);
            this.btnVirtualAccount.Name = "btnVirtualAccount";
            this.btnVirtualAccount.Size = new System.Drawing.Size(175, 32);
            this.btnVirtualAccount.TabIndex = 1;
            this.btnVirtualAccount.Text = "가상계좌 현황";
            this.btnVirtualAccount.UseVisualStyleBackColor = false;
            // 
            // checkAcc
            // 
            this.checkAcc.HeaderText = "선택";
            this.checkAcc.Name = "checkAcc";
            this.checkAcc.ReadOnly = true;
            this.checkAcc.Width = 60;
            // 
            // Assign
            // 
            this.Assign.HeaderText = "부여여부";
            this.Assign.Name = "Assign";
            this.Assign.ReadOnly = true;
            // 
            // No
            // 
            this.No.HeaderText = "고객번호";
            this.No.Name = "No";
            this.No.ReadOnly = true;
            this.No.Visible = false;
            // 
            // ParkName
            // 
            this.ParkName.HeaderText = "주차장";
            this.ParkName.Name = "ParkName";
            this.ParkName.ReadOnly = true;
            this.ParkName.Visible = false;
            this.ParkName.Width = 170;
            // 
            // CustName
            // 
            this.CustName.HeaderText = "이름";
            this.CustName.Name = "CustName";
            this.CustName.ReadOnly = true;
            this.CustName.Width = 120;
            // 
            // CarNum
            // 
            this.CarNum.HeaderText = "차량번호";
            this.CarNum.Name = "CarNum";
            this.CarNum.ReadOnly = true;
            this.CarNum.Width = 150;
            // 
            // TellNum
            // 
            this.TellNum.HeaderText = "전화번호";
            this.TellNum.Name = "TellNum";
            this.TellNum.ReadOnly = true;
            this.TellNum.Width = 170;
            // 
            // SognoLib_CustAccountViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1302, 684);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "SognoLib_CustAccountViewer";
            this.Text = "SognoLib_CustAccountViewer";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dviewAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dviewCust)).EndInit();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        public System.Windows.Forms.DataGridView dviewCust;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        public System.Windows.Forms.Button btnAcc_Update;
        public System.Windows.Forms.Button btnAcc_AssignDel;
        public System.Windows.Forms.Button btnAcc_Assign;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        public System.Windows.Forms.Button btnSearch_Clear;
        public System.Windows.Forms.Button btnSearch_Run;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        public System.Windows.Forms.DataGridView dviewAccount;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        public System.Windows.Forms.Button btnVirtualAccount;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public System.Windows.Forms.ComboBox cboxSearch_States;
        public System.Windows.Forms.ComboBox cboxSearch_Assign;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox tboxSearch_CarNum;
        public System.Windows.Forms.TextBox tboxSearch_Name;
        public System.Windows.Forms.ComboBox cboxSearch_ParkName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewCheckBoxColumn checkAcc;
        private System.Windows.Forms.DataGridViewTextBoxColumn Assign;
        private System.Windows.Forms.DataGridViewTextBoxColumn No;
        private System.Windows.Forms.DataGridViewTextBoxColumn ParkName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CarNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn TellNum;
    }
}