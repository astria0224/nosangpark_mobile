﻿using System;
using System.Data;
using System.Text;
using System.Reflection;
using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace SogNoLib.DataAccess
{
    public class SogNo_DBManager
    {
        private static NLog.Logger _log = NLog.LogManager.GetCurrentClassLogger();

        private readonly MySqlConnection mysqlConnection = null;
        private object thisLock = new object();


        public SogNo_DBManager(DBInfo MyDBInfo)
		{
            string ConnStr = String.Format("Server={0};Database={1};Uid={2};Pwd={3};",
                MyDBInfo.DBIP, MyDBInfo.DBName, MyDBInfo.DBUser, MyDBInfo.DBPW);

            try
            {
                this.mysqlConnection = new MySqlConnection(ConnStr);
                this.mysqlConnection.Open();

                MySqlCommand mysqlCommand = new MySqlCommand("set names utf8;", this.mysqlConnection);
                mysqlCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                _log.Error(ex, "DB> DB Connection Error!");
                this.mysqlConnection = null;
                throw;
            }
        }

        public void Reconnect()
        {
            try {
				if (this.mysqlConnection.State == ConnectionState.Open)
				{
					this.mysqlConnection.Close();
				}

				this.mysqlConnection.Open();
			}
            catch (Exception ex)
			{
                _log.Error(ex, "DB> DB Reconnect Error!");
                throw;
            }
        }

        public void Insert(string query, bool verbose = true)
        {
            lock (thisLock)
            {
                _log.Debug("Query: " + query);

                MySqlCommand mysqlCommand = new MySqlCommand(query, this.mysqlConnection);

                try
                {
                    int ret = mysqlCommand.ExecuteNonQuery();
                    _log.Debug("Query Ret: {0}", ret);
                }
                catch (Exception ex)
                {
                    if (verbose)
                    {
                        _log.Error(ex, "DB> Insert Error!!");
                    }

                    throw;
                }
            }
        }

        public void Insert(object obj, string table, string autoField = "", bool verbose = true)
        {
            string query = InsertQuery(obj, table, autoField);

            Insert(query, verbose);
        }

        private string InsertQuery(object obj, string table, string autoField)
        {
            string variableList = "";
            string valueList = "";

            PropertyInfo[] prop = obj.GetType().GetProperties();

            for (int i = 0; i < prop.Length; i++)
            {
                bool isNull = (prop[i].GetValue(obj) == null);
                bool isNullable = IsNullable(prop[i].PropertyType);

                //_log.Debug("prop.Name={0}, prop.Type={1}, prop.IsNullable={2}, prop.IsNull={3}", 
                //    prop[i].Name, prop[i].PropertyType.Name, isNullable, isNull);
                
                if (!isNull)
                {
                    if (variableList.Length > 0)
                    {
                        variableList += ", ";
                        valueList += ", ";
                    }

                    variableList += string.Format("`{0}`", prop[i].Name);
                    if (prop[i].PropertyType.Name == "DateTime" || 
                        (isNullable && prop[i].PropertyType.GetGenericArguments()[0].Name == "DateTime"))
                    {
                        valueList += string.Format("\'{0}\'", ((DateTime)prop[i].GetValue(obj)).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                    }
                    else if (prop[i].PropertyType.Name == "TimeSpan" ||
                        (isNullable && prop[i].PropertyType.GetGenericArguments()[0].Name == "TimeSpan"))
                    {
                        TimeSpan sapn = (TimeSpan)prop[i].GetValue(obj);
                        valueList += string.Format("\'{0}\'", string.Format("{0}:{1:mm}:{1:ss}", (int)sapn.TotalHours, sapn));
                    }
                    else
                    {
                        valueList += string.Format("\'{0}\'", prop[i].GetValue(obj).ToString());
                    }
                }
            }

            if (variableList.Length == 0)
            {
                variableList = string.Format("`{0}`", autoField);
                valueList = "null";
            }

            if (variableList.Length == 0)
            {
                _log.Error("Insert 쿼리의 설정 값이 없습니다.");
            }

            string query = string.Format("INSERT INTO `{0}` ({1}) VALUES ({2})", table, variableList, valueList);

            return query;
        }

        public void Update(string query, bool verbose = true)
        {
            lock (thisLock)
            {
                _log.Debug("Query: " + query);

                MySqlCommand mysqlCommand = new MySqlCommand(query, this.mysqlConnection);
                try
                {
                    int ret = mysqlCommand.ExecuteNonQuery();
                    _log.Debug("Query Ret: {0}", ret);
                }
                catch (Exception ex)
                {
                    if (verbose)
                    {
                        _log.Error(ex, "DB> Update Error!!");
                    }

                    throw;
                }
            }
        }

        public void Update(object obj, string table, string[] setFields, string[] whereFields, bool verbose = true)
        {
            string query = UpdateQuery(obj, table, setFields, whereFields);

            Update(query, verbose);
        }

        private string UpdateQuery(object obj, string table, string[] setFields, string[] whereField)
        {
            string whereList = "";
            string setList = "";

            PropertyInfo[] prop = obj.GetType().GetProperties();

            for (int i = 0; i < prop.Length; i++)
            {
                bool isNull = (prop[i].GetValue(obj) == null);
                bool isNullable = IsNullable(prop[i].PropertyType);

                bool isWhereField = Array.Exists(whereField, element => element == prop[i].Name);
                bool isSetField;
                if (setFields == null || setFields.Length == 0)
                {
                    isSetField = true;
                }
                else
                {
                    isSetField = Array.Exists(setFields, element => element == prop[i].Name);
                }               

                if (isWhereField || isSetField)
                {
                    string appendString;
                    if (isNull)
                    {
                        if (isWhereField)
                        {
                            appendString = string.Format("`{0}` is null", prop[i].Name);
                        }
                        else
                        {
                            appendString = string.Format("`{0}`=null", prop[i].Name);
                        }
                    }
                    else if (prop[i].PropertyType.Name == "DateTime" ||
                        (isNullable && prop[i].PropertyType.GetGenericArguments()[0].Name == "DateTime"))
                    {
                        appendString = string.Format("`{0}`=\'{1}\'", prop[i].Name, ((DateTime)prop[i].GetValue(obj)).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                    }
                    else if (prop[i].PropertyType.Name == "TimeSpan" ||
                        (isNullable && prop[i].PropertyType.GetGenericArguments()[0].Name == "TimeSpan"))
                    {
                        TimeSpan sapn = (TimeSpan)prop[i].GetValue(obj);
                        appendString = string.Format("`{0}`=\'{1}\'", prop[i].Name, string.Format("{0}:{1:mm}:{1:ss}", (int)sapn.TotalHours, sapn));
                    }
                    else
                    {
                        appendString = string.Format("`{0}`=\'{1}\'", prop[i].Name, prop[i].GetValue(obj).ToString());
                    }

                    if (isWhereField)
                    {
                        if (whereList.Length > 0)
                        {
                            whereList += " AND ";
                        }
                        whereList += appendString;
                    }
                    else if (isSetField)
                    {
                        if (setList.Length > 0)
                        {
                            setList += ", ";
                        }
                        setList += appendString;
                    }                 
                }
            }

            string query = string.Format("UPDATE `{0}` SET {1} WHERE {2}", table, setList, whereList);

            return query;
        }

        /// DELETE
        public void Delete(string query, bool verbose = true)
        {
            lock (thisLock)
            {
                _log.Debug("Query: " + query);

                MySqlCommand mysqlCommand = new MySqlCommand(query, this.mysqlConnection);
                try
                {
                    int ret = mysqlCommand.ExecuteNonQuery();
                    _log.Debug("Query Ret: {0}", ret);
                }
                catch (Exception ex)
                {
                    if (verbose)
                    {
                        _log.Error(ex, "DB> Delete Error!!");
                    }

                    throw;
                }
            }
        }


        public void Delete(object obj, string table, string[] whereFields, bool verbose = true)
        {
            string query = DeleteQuery(obj, table, whereFields);

            Delete(query);
        }

        private string DeleteQuery(object obj, string table, string[] whereField)
        {
            string whereList = "";

            PropertyInfo[] prop = obj.GetType().GetProperties();

            for (int i = 0; i < prop.Length; i++)
            {
                bool isNull = (prop[i].GetValue(obj) == null);
                bool isNullable = IsNullable(prop[i].PropertyType);

                bool isWhereField = Array.Exists(whereField, element => element == prop[i].Name);

                if (isWhereField)
                {
                    string appendString;
                    if (isNull)
                    {
                        if (isWhereField)
                        {
                            appendString = string.Format("`{0}` is null", prop[i].Name);
                        }
                        else
                        {
                            appendString = string.Format("`{0}`=null", prop[i].Name);
                        }
                    }
                    else if (prop[i].PropertyType.Name == "DateTime" ||
                        (isNullable && prop[i].PropertyType.GetGenericArguments()[0].Name == "DateTime"))
                    {
                        appendString = string.Format("`{0}`=\'{1}\'", prop[i].Name, ((DateTime)prop[i].GetValue(obj)).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                    }
                    else if (prop[i].PropertyType.Name == "TimeSpan" ||
                        (isNullable && prop[i].PropertyType.GetGenericArguments()[0].Name == "TimeSpan"))
                    {
                        TimeSpan sapn = (TimeSpan)prop[i].GetValue(obj);
                        appendString = string.Format("`{0}`=\'{1}\'", prop[i].Name, string.Format("{0}:{1:mm}:{1:ss}", (int)sapn.TotalHours, sapn));
                    }
                    else
                    {
                        appendString = string.Format("`{0}`=\'{1}\'", prop[i].Name, prop[i].GetValue(obj).ToString());
                    }

                    if (isWhereField)
                    {
                        if (whereList.Length > 0)
                        {
                            whereList += " AND ";
                        }
                        whereList += appendString;
                    }
                }
            }

            string query = string.Format("DELETE FROM `{0}` WHERE {1}", table, whereList);

            return query;
        }
               
        public int Count(string query, bool verbose = true)
        {
            lock (thisLock)
            {
                _log.Debug("Query: " + query);

                MySqlCommand mysqlCommand = new MySqlCommand(query, this.mysqlConnection);

                int count = 0;
                try
                {
                    count = int.Parse(mysqlCommand.ExecuteScalar().ToString());
                    _log.Debug("Query Ret: {0}", count);
                }
                catch (Exception ex)
                {
                    if (verbose)
                    {
                        _log.Error(ex, "DB> Count Error!!");
                    }

                    throw;
                }

                return count;
            }
        }

        public int Count(object obj, string table, string[] whereFields, bool verbose = true)
        {
            string query = CountQuery(obj, table, whereFields);

            return Count(query);
        }

        private string CountQuery(object obj, string table, string[] whereField)
        {
            string whereList = "";

            PropertyInfo[] prop = obj.GetType().GetProperties();

            for (int i = 0; i < prop.Length; i++)
            {
                bool isNull = (prop[i].GetValue(obj) == null);
                bool isNullable = IsNullable(prop[i].PropertyType);

                bool isWhereField = Array.Exists(whereField, element => element == prop[i].Name);

                if (isWhereField)
                {
                    string appendString;
                    if (isNull)
                    {
                        if (isWhereField)
                        {
                            appendString = string.Format("`{0}` is null", prop[i].Name);
                        }
                        else
                        {
                            appendString = string.Format("`{0}`=null", prop[i].Name);
                        }
                    }
                    else if (prop[i].PropertyType.Name == "DateTime" ||
                        (isNullable && prop[i].PropertyType.GetGenericArguments()[0].Name == "DateTime"))
                    {
                        appendString = string.Format("`{0}`=\'{1}\'", prop[i].Name, ((DateTime)prop[i].GetValue(obj)).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                    }
                    else if (prop[i].PropertyType.Name == "TimeSpan" ||
                        (isNullable && prop[i].PropertyType.GetGenericArguments()[0].Name == "TimeSpan"))
                    {
                        TimeSpan sapn = (TimeSpan)prop[i].GetValue(obj);
                        appendString = string.Format("`{0}`=\'{1}\'", prop[i].Name, string.Format("{0}:{1:mm}:{1:ss}", (int)sapn.TotalHours, sapn));
                    }
                    else
                    {
                        appendString = string.Format("`{0}`=\'{1}\'", prop[i].Name, prop[i].GetValue(obj).ToString());
                    }

                    if (isWhereField)
                    {
                        if (whereList.Length > 0)
                        {
                            whereList += " AND ";
                        }
                        whereList += appendString;
                    }
                }
            }

            string query = string.Format("SELECT COUNT(*) FROM `{0}` WHERE {1}", table, whereList);

            return query;
        }

        /// SELECT
        public T[] GetRecords<T>(string query) where T : new()
        {
            _log.Debug("Query: " + query);
            lock (thisLock)
            {
                List<T> records = new List<T>();
                MySqlCommand mysqlCommand = new MySqlCommand(query, this.mysqlConnection);
                MySqlDataReader dataReader = mysqlCommand.ExecuteReader();
                while (dataReader.Read())
                {
                    T record = new T();
                    SetDataFromReader(record, dataReader);
                    records.Add(record);
                }
                dataReader.Close();
                return records.ToArray();
            }
        }

		private void SetDataFromReader(object myobject, MySqlDataReader dataReader)
        {
            foreach (var prop in myobject.GetType().GetProperties())
            {
                try
                {
                    int columnIndex = dataReader.GetOrdinal(prop.Name);
                    Type propertyType = prop.PropertyType;
                    if (IsNullable(propertyType))
                    {
                        propertyType = propertyType.GetGenericArguments()[0];
                    }

                    if (dataReader.IsDBNull(columnIndex))
                    {
                        prop.SetValue(myobject, null);
                    }
                    else if (propertyType.Name == "Int32")
                    {
                        prop.SetValue(myobject, Convert.ToInt32(dataReader[prop.Name]));
                    }
                    else if (propertyType.Name == "Double")
                    {
                        prop.SetValue(myobject, Convert.ToDouble(dataReader[prop.Name]));
                    }
                    else if (propertyType.Name == "DateTime")
                    {
                        prop.SetValue(myobject, DateTime.Parse(dataReader[prop.Name].ToString()));
                    }
                    else if (propertyType.Name == "TimeSpan")
                    {
                        prop.SetValue(myobject, TimeSpan.Parse(dataReader[prop.Name].ToString()));
                    }
                    else
                    {
                        prop.SetValue(myobject, dataReader[prop.Name].ToString());
                    }
                }
                catch (IndexOutOfRangeException ex)
                {
                    _log.Error(ex, "DB> {0} Column을 찾을 수 없음", prop.Name);
                    prop.SetValue(myobject, null);
                }
                catch (Exception ex)
				{
                    _log.Error(ex, "DB> Data Read Error! {0}", prop.PropertyType.Name);
                    throw;
                }
            }
        }

        bool IsNullable(Type type) => Nullable.GetUnderlyingType(type) != null;
    }
}
