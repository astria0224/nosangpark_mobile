﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace SogNoLib.Communication
{
	public class SogNo_TcpClient
	{
		private static NLog.Logger _log = NLog.LogManager.GetCurrentClassLogger();

		class ConnectState
		{
			public Socket sock = null; 
			public string endType;
			public int endNo = 0;
		}

		public bool IsConnected
		{
			get
			{
				return this.comm.IsConnected;
			}
		}

		public delegate void ConnectDelegate(string ip, int port);
		public event ConnectDelegate ConnectEvent;
		public delegate void DisconnectDelegate();
		public event DisconnectDelegate DisconnectEvent;

		public delegate void ReceiveDelegate(string message);
		public event ReceiveDelegate ReceiveEvent;

        public delegate void ReceiveBytesDelegate(byte[] message);
        public event ReceiveBytesDelegate ReceiveBytesEvent;

        private string ip;
		private int port;

		private string myType;
		private int myNo = 0;
		private string MyName
		{
			get
			{
				return string.Format("{0}#{1}", this.myType, this.myNo);
			}
		}
		private SogNo_TcpCommunication comm = new SogNo_TcpCommunication();

		private bool connectPending = false;
        private bool autoReconnect = false;
		

		public SogNo_TcpClient(string myType, int myNo = 0)
		{
			this.myType = myType;
			this.myNo = myNo;

			this.comm.DisconnectEvent += OnDisconnectEvent;
			this.comm.ReceiveEvent += OnReceiveEvent;
            this.comm.ReceiveBytesEvent += OnReceiveBytesEvent;
		}

		~SogNo_TcpClient()
		{
			Close();
		}

        public void Connect(string server, int port, string endType = "", int endNo = 0, bool autoReconnect = false)
		{
			if(this.connectPending || comm.IsConnected)
			{
				return;
			}

			_log.Debug("<{0} - {1}#{2}> Connect Start {3}:{4}", this.MyName, endType, endNo, server, port);

			this.ip = server;
			this.port = port;
            this.autoReconnect = autoReconnect;

            try
            {
                IPAddress ipaddr = IPAddress.Parse(this.ip);
                IPEndPoint remoteEP = new IPEndPoint(ipaddr, this.port);

                Socket sock = new Socket(ipaddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                ConnectState state = new ConnectState 
                {
                    sock = sock, 
                    endType = endType, 
                    endNo = endNo 
                };

                sock.BeginConnect(remoteEP, new AsyncCallback(ConnectCallback), state);

                this.connectPending = true;
            }
            catch (Exception e)
            {
                _log.Error(e, "<{0} - {1}#{2}> Connect Exception!", this.MyName, endType, endNo);

                Close();

                this.connectPending = false;

                if (this.autoReconnect) 
                {
                    Reconnect(endType, endNo);
                }
			}

			_log.Trace("<{0} - {1}#{2}> Connect End {3}:{4}", this.MyName, endType, endNo, this.ip, this.port);
		}

		private void ConnectCallback(IAsyncResult ar)
		{
			_log.Trace("<{0}> ConnectCallback Start", this.MyName);

			try
			{
				ConnectState state = (ConnectState)ar.AsyncState; 

				state.sock.EndConnect(ar); 

				_log.Info("<{0} - {1}#{2}> Socket connected to {3}", this.MyName, state.endType, state.endNo, state.sock.RemoteEndPoint.ToString()); 

                BeginInvokeConnectHandler(this.ip, this.port); 
                
				comm.StartCommunication(state.sock, state.endType, state.endNo);

                this.connectPending = false;
            }
            catch (Exception ex)
			{
                if (this.autoReconnect)
                {
                    _log.Debug("<{0}> Connect Fail!", this.MyName);
                }
                else
                {
                    _log.Error(ex, "<{0}> ConnectCallback Exception", this.MyName);
                }				

				Close();

                BeginInvokeConnectHandler(null, 0);

                this.connectPending = false;

                if (this.autoReconnect)
                {
                    ConnectState state = (ConnectState)ar.AsyncState;
                    Reconnect(state.endType, state.endNo);
                }
            }

            if (this.comm.IsConnected)
            {
                SendIntroMessage();
            }

            _log.Trace("<{0} - {1}> ConnectCallback End", this.MyName, this.comm.EndName);
		}

		public void Send(byte[] msg, bool containsBinary = false)
		{
			if (this.comm.IsConnected)
			{
				this.comm.Send(msg, containsBinary);
			}
		}

		public void Send(string msg)
		{
			if(this.comm.IsConnected)
			{
				this.comm.Send(msg);
			}
		}

		public void Close() 
		{
			_log.Trace("<{0}> Close Start", this.MyName);

			comm.CloseCommunication(); 

			_log.Trace("<{0}> Close End", this.MyName);
		}

		private void SendIntroMessage()
		{
			try
			{
				SogNo_MessageIntro message = new SogNo_MessageIntro()
				{
					EndType = this.myType,
					EndNo = this.myNo
				};

				this.comm.Send(message.Pack(), false);
			}
			catch (Exception ex)
			{
				_log.Error(ex, "<{0}> SendIntroMessage Exception", this.MyName);

				Close();
			}
		}

        private void Reconnect(string endType, int endNo)
        {
            System.Threading.Thread.Sleep(5000);

            Connect(this.ip, this.port, endType, endNo, this.autoReconnect);
        }

		private void OnDisconnectEvent(object sender)
		{
			_log.Info("<{0}> {1}와 연결이 끊어졌습니다.", this.MyName, this.comm.EndName);

            BeginInvokeDisconnectHandler();

            if (this.autoReconnect)
            {
                Reconnect(this.comm.EndType, this.comm.EndNo);
            }
        }

		private void OnReceiveEvent(object sender, string message)
		{
            BeginInvokeReceiveHandler(message);
		}

        private void OnReceiveBytesEvent(object sender, byte[] message)
        {
            BeginInvokeReceiveBytesHandler(message);
        }

        private void BeginInvokeConnectHandler(string ip, int port)
        {
            if (ConnectEvent != null)
            {
                foreach (ConnectDelegate handler in ConnectEvent.GetInvocationList())
                {
                    handler.BeginInvoke(ip, port, handler.EndInvoke, null);
                }
            }
        }

        private void BeginInvokeDisconnectHandler()
        {
            if (DisconnectEvent != null)
            {
                foreach (DisconnectDelegate handler in DisconnectEvent.GetInvocationList())
                {
                    handler.BeginInvoke(handler.EndInvoke, null);
                }
            }
        }

        private void BeginInvokeReceiveHandler(string message)
        {
            if (ReceiveEvent != null)
            {
                foreach (ReceiveDelegate handler in ReceiveEvent.GetInvocationList())
                {
                    handler.BeginInvoke(message, handler.EndInvoke, null);
                }
            }
        }

        private void BeginInvokeReceiveBytesHandler(byte[] message)
        {
            if (ReceiveBytesEvent != null)
            {
                foreach (ReceiveBytesDelegate handler in ReceiveBytesEvent.GetInvocationList())
                {
                    handler.BeginInvoke(message, handler.EndInvoke, null);
                }
            }
        }
    }
}
