﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using SogNoLib;


namespace SogNoLib.Communication
{
    public enum EMsgID
    {
        Unknown,
        Intro,
        Status,
        GetStatus,
        LprResult,
        LprResult_ACK,
        BarCtrl,
        ScrCtrl,
        Count,
        IpsImage,
        IpsResult,
        LwvImage,
        LwvResult,
        MatchImage,
        MatchResult
    };

    public enum EBoardConnection : int
    {
        Connected = Park.Constant.LPRBoard_Connected,
        Disconnected = Park.Constant.LPRBoard_Disconnected
    };

    public enum ECamConntection : int
    {
        Disconnected = Park.Constant.Cam_Disconnected,
        Connected = Park.Constant.Cam_Connected
    };

    public enum ECamInOut : int
    {
        In = Park.Constant.CamIO_In,
        Out = Park.Constant.CamIO_Out
    };

    public enum ECamOrder : int
    {
        Front = Park.Constant.CamOrder_Front,
        Back = Park.Constant.CamOrder_Back
    };

    public enum EBarStatus : int
    {
        Disconnected = Park.Constant.Bar_Disconnected,
        Opened = Park.Constant.Bar_Opened,
        Opening = Park.Constant.Bar_Opening,
        Closed = Park.Constant.Bar_Closed,
        Closing = Park.Constant.Bar_Closing,
        Count = Park.Constant.Bar_Count,
        Stop = Park.Constant.Bar_Stop,
        Initialize = Park.Constant.Bar_Initialize,
        Connecting = Park.Constant.Bar_Connecting
    };

    public enum ECarInOut : int
    {
        In = Park.Constant.CamIO_In,
        Out = Park.Constant.CamIO_Out
    }

    public enum EBarCommand
    {
        BarOpen,
        BarClose
    }

    public enum EScreenCommand
    {
        NMSG,   // 평상시 항상 표시되는 메시지
        EMSG    // 잠시 표시되었다 사라지는 메시지
    }

    public enum EMatchCommand
    {
        InCar,
        OutCar,
        Verify,
        Match
    }


    public abstract class SogNo_Message
	{
        public struct Point
        {
            public int x, y;
        }

        public const int HeaderLen = 11;  // (STX)T00000000#
		public const int FooterLen = 1;   // (ETX)

		public const byte STX = 0x02;
		public const byte ETX = 0x03;
		public const byte Seperator = (byte)'#';
        public const byte TextSign = (byte)'T';
        public const byte BinarySign = (byte)'B';

        public char CharSTX
		{
			get
			{
				return Convert.ToChar(STX);
			}
		}
		public char CharETX
		{
			get
			{
				return Convert.ToChar(ETX);
			}
		}
        public char CharTextSign
        {
            get
            {
                return Convert.ToChar(TextSign);
            }
        }
        public char CharBinarySign
        {
            get
            {
                return Convert.ToChar(BinarySign);
            }
        }

		public static EMsgID ParseMsgID(string messageBody)
		{
			EMsgID msgID = EMsgID.Unknown;

			int idx = messageBody.IndexOf(Convert.ToChar(Seperator));
			if(idx > 0)
			{
				msgID = StringToMsgID(messageBody.Substring(0, idx));
			}
			else
			{
				msgID = StringToMsgID(messageBody);
			}

			return msgID;
		}

        public static EMsgID ParseMsgID(byte[] messageBody)
        {
            EMsgID msgID = EMsgID.Unknown;

            int idx = Array.IndexOf(messageBody, Seperator);
            if (idx > 0)
            {
                msgID = StringToMsgID(Encoding.Default.GetString(messageBody, 0, idx));
            }
            else
            {
                msgID = StringToMsgID(Encoding.Default.GetString(messageBody));
            }

            return msgID;
        }

        private static EMsgID StringToMsgID(string strID)
		{
			EMsgID msgID = EMsgID.Unknown;
			
			if (Enum.IsDefined(typeof(EMsgID), strID))
			{
				msgID = (EMsgID)Enum.Parse(typeof(EMsgID), strID);
			}

			return msgID;
		}


        private const string DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff";
        public static string DateTimeToString(DateTime? dateTime)
        {
            return dateTime?.ToString(DateTimeFormat);
        }

        public static DateTime? StringToDateTime(string dateTimeString)
        {
            DateTime dateTime = new DateTime();
            if (DateTime.TryParseExact(dateTimeString, DateTimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime))
            {
                return dateTime;
            }

            return null;
        }

		abstract public byte[] Pack();
		abstract public void UnPack(string message);
        virtual public void UnPack(byte[] message)
        {
            UnPack(Encoding.Default.GetString(message));
        }
    }


	public class SogNo_MessageUnknown : SogNo_Message
	{
		public string MessageBody { get; set; }

		public override byte[] Pack()
		{
			int bodyLen = Encoding.Default.GetBytes(MessageBody).Length;

			string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, CharTextSign, bodyLen, MessageBody, CharETX);
			return Encoding.Default.GetBytes(message);
		}

		public override void UnPack(string messageBody)
		{
			this.MessageBody = messageBody;
		}
	}


	public class SogNo_MessageIntro : SogNo_Message
	{
		public string EndType { get; set; }
		public int EndNo { get; set; } = 0;

		public override byte[] Pack()
		{
			string msgBody = string.Format("{0}#{1}#{2}", EMsgID.Intro, this.EndType, this.EndNo);
			int bodyLen = Encoding.Default.GetBytes(msgBody).Length;

			string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, CharTextSign, bodyLen, msgBody, CharETX);
			return Encoding.Default.GetBytes(message);
		}

		public override void UnPack(string messageBody)
		{
			EMsgID msgID = ParseMsgID(messageBody);
			if (msgID != EMsgID.Intro)
			{
				throw new Exception("메시지 ID가 일치하지 않음.");
			}

			string[] tokens = messageBody.Split(new char[1] { '#' });
			if (tokens.Length != 3)
			{
				throw new Exception("토큰의 갯수가 일치하지 않음.");
			}

			this.EndType = tokens[1];
			this.EndNo = int.Parse(tokens[2]);
		}
	}


	public class SogNo_MessageGetStatus : SogNo_Message
	{
		public override byte[] Pack()
		{
			string msgBody = string.Format("{0}", EMsgID.GetStatus);
			int bodyLen = Encoding.Default.GetBytes(msgBody).Length;

			string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, CharTextSign, bodyLen, msgBody, CharETX);
			return Encoding.Default.GetBytes(message);
		}

		public override void UnPack(string messageBody)
		{
			EMsgID msgID = ParseMsgID(messageBody);
			if (msgID != EMsgID.GetStatus)
			{
				throw new Exception("메시지 ID가 일치하지 않음.");
			}
		}
	}


	public class SogNo_MessageStatus : SogNo_Message
	{
		public class CamStatus
		{
			public string CamID { get; set; }
			public ECamConntection CamConn { get; set; } = ECamConntection.Disconnected;
			public ECamInOut CamInOut { get; set; } = ECamInOut.In;
			public ECamOrder CamOrder { get; set; } = ECamOrder.Front;
			public EBarStatus BarStatus { get; set; } = EBarStatus.Disconnected;

			public CamStatus(string status)
			{
				if(status.Length != 7)
				{
					throw new Exception("Cam의 상태 문자열 길이가 일치하지 않습니다.");
				}

				CamID = status.Substring(0, 3);
				CamConn = (status[3] == '1' ? ECamConntection.Connected : ECamConntection.Disconnected);
				CamInOut = (status[4] == '1' ? ECamInOut.Out : ECamInOut.In);
				CamOrder = (status[5] == '1' ? ECamOrder.Back : ECamOrder.Front);
				BarStatus = (EBarStatus)Enum.Parse(typeof(EBarStatus), string.Format("{0}", status[6]));
			}

			public CamStatus(string camID, ECamConntection camConn, ECamInOut camInOut, ECamOrder camOrder, EBarStatus barStatus)
			{
				this.CamID = camID;
				this.CamConn = camConn;
				this.CamInOut = camInOut;
				this.CamOrder = camOrder;
				this.BarStatus = barStatus;
			}

			public override string ToString()
			{
				return string.Format("{0}{1}{2}{3}{4}", CamID, CamConn.ToString("D"), CamInOut.ToString("D"), CamOrder.ToString("D"), BarStatus.ToString("D"));
			}
		}


		public EBoardConnection BoardStatus { get; set; }
		public List<CamStatus> CamList { get; set; } = new List<CamStatus>();


		public override byte[] Pack()
		{
			string msgBody = string.Format("{0}#{1}", EMsgID.Status, this.BoardStatus.ToString("D"));
			foreach(var status in this.CamList)
			{
				msgBody += string.Format("#{0}", status.ToString());
			}

			int bodyLen = Encoding.Default.GetBytes(msgBody).Length;

            string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, CharTextSign, bodyLen, msgBody, CharETX);
            return Encoding.Default.GetBytes(message);
		}

		public override void UnPack(string messageBody)
		{
			EMsgID msgID = ParseMsgID(messageBody);
			if (msgID != EMsgID.Status)
			{
				throw new Exception("메시지 ID가 일치하지 않음.");
			}

			string[] tokens = messageBody.Split(new char[1] { '#' });
			if (tokens.Length <= 2)
			{
				throw new Exception("토큰의 갯수가 일치하지 않음.");
			}

			this.BoardStatus = (tokens[1] == "0" ? EBoardConnection.Connected : EBoardConnection.Disconnected);

			try
			{
				for (int i = 2; i < tokens.Length; i++)
				{
					CamStatus status = new CamStatus(tokens[i]);
					this.CamList.Add(status);
				}
			}
			catch (Exception)
			{
				throw new Exception("메시지 형식이 일치하지 않습니다.");
			}
		}
	}


	public class SogNo_MessageLPRResult : SogNo_Message
	{
        public int CamNo { get; set; } = 0;
		public ECarInOut CarInOut { get; set; } = ECarInOut.In;
		public ECamOrder CamOrder { get; set; } = ECamOrder.Front;
		public string CarNumber { get; set; }
		public string ImagePath { get; set; }
        public DateTime? OccurTime { get; set; } = null;

        public Point[] Corners { get; set; } = null;


        public override byte[] Pack()
		{
            string location = "";
            if (this.Corners != null)
            {
                foreach (Point pt in this.Corners)
                {
                    if (location.Length > 0)
                        location += ",";

                    location += string.Format("{0},{1}", (int)pt.x, (int)pt.y);
                }
            }

            string msgBody = string.Format("{0}#{1}#{2}#{3}#{4}#{5}#{6}#{7}", EMsgID.LprResult, this.CamNo, this.CarInOut, this.CamOrder, this.CarNumber, location, this.ImagePath, DateTimeToString(this.OccurTime));

            int bodyLen = Encoding.Default.GetBytes(msgBody).Length;

            string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, CharTextSign, bodyLen, msgBody, CharETX);
            return Encoding.Default.GetBytes(message);
		}

		public override void UnPack(string messageBody)
		{
			EMsgID msgID = ParseMsgID(messageBody);
			if (msgID != EMsgID.LprResult)
			{
				throw new Exception("메시지 ID가 일치하지 않음.");
			}

			string[] tokens = messageBody.Split(new char[1] { '#' });
			if (tokens.Length != 8)
			{
				throw new Exception("토큰의 갯수가 일치하지 않음.");
			}

			try
			{
				this.CamNo = Int32.Parse(tokens[1]);
				this.CarInOut = (ECarInOut)Enum.Parse(typeof(ECarInOut), tokens[2]);
				this.CamOrder = (ECamOrder)Enum.Parse(typeof(ECamOrder), tokens[3]);
				this.CarNumber = tokens[4];
                string location = tokens[5];
				this.ImagePath = tokens[6];
                this.OccurTime = StringToDateTime(tokens[7]);
                
                if (location.Length > 0)
                {
                    string[] locTokens = location.Split(new char[1] { ',' });
                    if (locTokens.Length != 8)
                    {
                        throw new Exception("번호판의 네 모서리 점 정보가 부적절함.");
                    }

                    this.Corners = new Point[4];
                    for (int i = 0; i < locTokens.Length; i += 2)
                    {
                        Point pt;
                        pt.x = int.Parse(locTokens[i]);
                        pt.y = int.Parse(locTokens[i + 1]);

                        this.Corners[i / 2] = pt;
                    }
                }
            }
			catch (Exception)
			{
				throw new Exception("메시지 형식이 일치하지 않습니다.");
			}
		}
	}

    public class SogNo_MessageLprResultACK : SogNo_Message
    {
        public override byte[] Pack()
        {
            string msgBody = string.Format("{0}", EMsgID.LprResult_ACK);
            int bodyLen = Encoding.Default.GetBytes(msgBody).Length;

            string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, CharTextSign, bodyLen, msgBody, CharETX);
            return Encoding.Default.GetBytes(message);
        }

        public override void UnPack(string messageBody)
        {
            EMsgID msgID = ParseMsgID(messageBody);
            if (msgID != EMsgID.LprResult_ACK)
            {
                throw new Exception("메시지 ID가 일치하지 않음.");
            }
        }
    }

    public class SogNo_MessageBarControl : SogNo_Message
    {
        public int CamNo = 0;
        public EBarCommand BarCommand { get; set; } = EBarCommand.BarOpen;

        public override byte[] Pack()
        {
            string msgBody = string.Format("{0}#{1}#{2}", EMsgID.BarCtrl, this.CamNo, this.BarCommand);
            int bodyLen = Encoding.Default.GetBytes(msgBody).Length;

            string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, CharTextSign, bodyLen, msgBody, CharETX);
            return Encoding.Default.GetBytes(message);
        }

        public override void UnPack(string messageBody)
        {
            EMsgID msgID = ParseMsgID(messageBody);
            if (msgID != EMsgID.BarCtrl)
            {
                throw new Exception("메시지 ID가 일치하지 않음.");
            }

            string[] tokens = messageBody.Split(new char[1] { '#' });
            if (tokens.Length != 3)
            {
                throw new Exception("토큰의 갯수가 일치하지 않음.");
            }

            try
            {
                this.CamNo = Int32.Parse(tokens[1]);
                this.BarCommand = (EBarCommand)Enum.Parse(typeof(EBarCommand), tokens[2]);
            }
            catch (Exception)
            {
                throw new Exception("메시지 형식이 일치하지 않습니다.");
            }
        }
    }

    public class SogNo_MessageScreenControl : SogNo_Message
    {
        public EScreenCommand ScrCommand { get; set; } = EScreenCommand.EMSG;
        public int CamNo { get; set; } = 0;
        public string UpperMessage { get; set; }
        public string LowerMessage { get; set; }


        public override byte[] Pack()
        {
            string msgBody = string.Format("{0}#{1}#{2}#{3}#{4}", EMsgID.ScrCtrl, this.ScrCommand, this.CamNo, this.UpperMessage, this.LowerMessage);
            int bodyLen = Encoding.Default.GetBytes(msgBody).Length;

            string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, CharTextSign, bodyLen, msgBody, CharETX);
            return Encoding.Default.GetBytes(message);
        }

        public override void UnPack(string messageBody)
        {
            EMsgID msgID = ParseMsgID(messageBody);
            if (msgID != EMsgID.ScrCtrl)
            {
                throw new Exception("메시지 ID가 일치하지 않음.");
            }

            string[] tokens = messageBody.Split(new char[1] { '#' });
            if (tokens.Length != 5)
            {
                throw new Exception("토큰의 갯수가 일치하지 않음.");
            }

            try
            {
                this.ScrCommand = (EScreenCommand)Enum.Parse(typeof(EScreenCommand), tokens[1]);
                this.CamNo = Int32.Parse(tokens[2]);
                this.UpperMessage = tokens[3];
                this.LowerMessage = tokens[4];
            }
            catch (Exception)
            {
                throw new Exception("메시지 형식이 일치하지 않습니다.");
            }
        }
    }

    /*
	public class SogNo_MessageCount : SogNo_Message
	{
		public enum ECarInOut
		{
			CCarIn,
			CCarOut
		}

		public ECarInOut CarInOut { get; set; } = ECarInOut.CCarIn;
		
		public override byte[] Pack()
		{
			string msgBody = string.Format("{0}#{1}", EMsgID.Count, this.CarInOut);
			int bodyLen = Encoding.Default.GetBytes(msgBody).Length;

			string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, TextSign, bodyLen, msgBody, CharETX);
			return Encoding.Default.GetBytes(message);
		}

		public override void UnPack(string messageBody)
		{
			EMsgID msgID = ParseMsgID(messageBody);
			if (msgID != EMsgID.Count)
			{
				throw new Exception("메시지 ID가 일치하지 않음.");
			}

			string[] tokens = messageBody.Split(new char[1] { '#' });
			if (tokens.Length != 2)
			{
				throw new Exception("토큰의 갯수가 일치하지 않음.");
			}

			try
			{
				this.CarInOut = (ECarInOut)Enum.Parse(typeof(ECarInOut), tokens[1]);
			}
			catch (Exception)
			{
				throw new Exception("메시지 형식이 일치하지 않습니다.");
			}
		}
	}
    */


    public enum EImageDataType
    {
        Jpeg,
        Raw,
        LocalPath,
        RemotePath
    }


    public class SogNo_MessageIpsImage : SogNo_Message
    {
        public EImageDataType ImageType { get; set; } = EImageDataType.Raw;
        public byte[] ImageData { get; set; } = null;
        public string ImagePath { get; set; }
        public int ImageWidth { get; set; } = 0;
        public int ImageHeight { get; set; } = 0;
        public int ImageChannels { get; set; } = 1;
        public string ImageNo { get; set; }

        public override byte[] Pack()
        {
            string imgDesc = string.Format("{0}#{1}#{2}#{3}#{4}#{5}#{6}#{7}#", EMsgID.IpsImage, ImageNo, ImageType, ImagePath, ImageWidth, ImageHeight, ImageChannels, (ImageData?.Length ?? 0));
            byte[] imgDescBytes = Encoding.Default.GetBytes(imgDesc);
            int descLen = imgDescBytes.Length;

            int bodyLen = descLen + ImageData.Length;
            string msgHeader = string.Format("{0}{1}{2:D8}#", CharSTX, CharBinarySign, bodyLen);
            byte[] msgHeaderBytes = Encoding.Default.GetBytes(msgHeader);

            byte[] message = msgHeaderBytes.Concat(imgDescBytes).Concat(ImageData).Concat(new byte[] { ETX }).ToArray();

            return message;
        }

        public override void UnPack(string messageBody)
        {
            throw new NotImplementedException();
        }

        public override void UnPack(byte[] messageBody)
        {
            EMsgID msgID = ParseMsgID(messageBody);
            if (msgID != EMsgID.IpsImage)
            {
                throw new Exception("메시지 ID가 일치하지 않음.");
            }

            int lastSeperatorIndex = -1;
            string[] tokens = new string[8];
            for (int i = 0; i < 8; i++)
            {
                int nextSeperatorIndex = Array.IndexOf(messageBody, Seperator, lastSeperatorIndex + 1);
                if (nextSeperatorIndex < 0)
                {
                    throw new Exception("토큰의 갯수가 일치하지 않음.");
                }

                tokens[i] = Encoding.Default.GetString(messageBody, lastSeperatorIndex + 1, nextSeperatorIndex - lastSeperatorIndex - 1);
                lastSeperatorIndex = nextSeperatorIndex;
            }

            try
            {
                this.ImageNo = tokens[1];
                this.ImageType = (EImageDataType)Enum.Parse(typeof(EImageDataType), tokens[2]);
                this.ImagePath = tokens[3];
                this.ImageWidth = Int32.Parse(tokens[4]);
                this.ImageHeight = Int32.Parse(tokens[5]);
                this.ImageChannels = Int32.Parse(tokens[6]);
                int imageDataLength = Int32.Parse(tokens[7]);

                if (imageDataLength != messageBody.Length - (lastSeperatorIndex + 1))
                {
                    throw new Exception();
                }

                this.ImageData = new byte[imageDataLength];
                Array.Copy(messageBody, lastSeperatorIndex + 1, this.ImageData, 0, imageDataLength);
            }
            catch (Exception)
            {
                throw new Exception("메시지 형식이 일치하지 않습니다.");
            }
        }
    }

    public class SogNo_MessageIpsResult : SogNo_Message
    {
        public string ImageNo { get; set; }
        public string CarNumber { get; set; }
        public double RecError { get; set; }
        public int RecTime { get; set; }

        public Point[] Corners { get; set; } = null;
        public Point[] CornersForMatching { get; set; } = null;
        public Point[] CornersForLWV { get; set; } = null;

        public override byte[] Pack()
        {
            string location = CornersToString(this.Corners);
            string locationMatching = CornersToString(this.CornersForMatching);
            string locationLWV = CornersToString(this.CornersForLWV);

            string msgBody = string.Format("{0}#{1}#{2}#{3}#{4}#{5}#{6:F6}#{7}", EMsgID.IpsResult, this.ImageNo, this.CarNumber, location, locationMatching, locationLWV, this.RecError, this.RecTime);
            int bodyLen = Encoding.Default.GetBytes(msgBody).Length;

            string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, CharTextSign, bodyLen, msgBody, CharETX);
            return Encoding.Default.GetBytes(message);
        }

        private string CornersToString(Point[] corners)
        {
            string location = "";
            if (corners != null)
            {
                foreach (Point pt in corners)
                {
                    if (location.Length > 0)
                        location += ",";

                    location += string.Format("{0},{1}", (int)pt.x, (int)pt.y);
                }
            }

            return location;
        }

        public override void UnPack(string messageBody)
        {
            EMsgID msgID = ParseMsgID(messageBody);
            if (msgID != EMsgID.IpsResult)
            {
                throw new Exception("메시지 ID가 일치하지 않음.");
            }

            string[] tokens = messageBody.Split(new char[1] { '#' });
            if (tokens.Length != 8)
            {
                throw new Exception("토큰의 갯수가 일치하지 않음.");
            }

            try
            {
                this.ImageNo = tokens[1];
                this.CarNumber = tokens[2];
                string location = tokens[3];
                string locationMatching = tokens[4];
                string locationLWV = tokens[5];
                this.RecError = double.Parse(tokens[6]);
                this.RecTime = int.Parse(tokens[7]);

                this.Corners = StringToCorners(location);
                this.CornersForMatching = StringToCorners(locationMatching);
                this.CornersForLWV = StringToCorners(locationLWV);
            }
            catch (Exception)
            {
                throw new Exception("메시지 형식이 일치하지 않습니다.");
            }
        }


        private Point[] StringToCorners(string location)
        {
            Point[] corners = null;

            if (location.Length > 0)
            {
                string[] locTokens = location.Split(new char[1] { ',' });
                if (locTokens.Length != 8)
                {
                    throw new Exception("번호판의 네 모서리 점 정보가 부적절함.");
                }

                corners = new Point[4];
                for (int i = 0; i < locTokens.Length; i += 2)
                {
                    Point pt;
                    pt.x = int.Parse(locTokens[i]);
                    pt.y = int.Parse(locTokens[i + 1]);

                    corners[i / 2] = pt;
                }
            }

            return corners;
        }
    }



    public class SogNo_MessageLwvImage : SogNo_Message
    {
        public EImageDataType ImageType { get; set; } = EImageDataType.Raw;
        public byte[] ImageData { get; set; } = null;
        public string ImagePath { get; set; }
        public int ImageWidth { get; set; } = 0;
        public int ImageHeight { get; set; } = 0;
        public int ImageChannels { get; set; } = 1;
        public string ImageNo { get; set; }
        public string CarNumber { get; set; }
        public Point[] CornersForLWV { get; set; } = null;

        public override byte[] Pack()
        {
            string locationLWV = CornersToString(this.CornersForLWV);

            string imgDesc = string.Format("{0}#{1}#{2}#{3}#{4}#{5}#{6}#{7}#{8}#{9}#", EMsgID.LwvImage, ImageNo, ImageType, ImagePath, ImageWidth, ImageHeight, ImageChannels, (ImageData?.Length ?? 0), CarNumber, locationLWV);
            byte[] imgDescBytes = Encoding.Default.GetBytes(imgDesc);
            int descLen = imgDescBytes.Length;

            int bodyLen = descLen + ImageData.Length;
            string msgHeader = string.Format("{0}{1}{2:D8}#", CharSTX, CharBinarySign, bodyLen);
            byte[] msgHeaderBytes = Encoding.Default.GetBytes(msgHeader);

            byte[] message = msgHeaderBytes.Concat(imgDescBytes).Concat(ImageData).Concat(new byte[] { ETX }).ToArray();

            return message;
        }

        private string CornersToString(Point[] corners)
        {
            string location = "";
            if (corners != null)
            {
                foreach (Point pt in corners)
                {
                    if (location.Length > 0)
                        location += ",";

                    location += string.Format("{0},{1}", (int)pt.x, (int)pt.y);
                }
            }

            return location;
        }

        public override void UnPack(string messageBody)
        {
            throw new NotImplementedException();
        }

        public override void UnPack(byte[] messageBody)
        {
            EMsgID msgID = ParseMsgID(messageBody);
            if (msgID != EMsgID.LwvImage)
            {
                throw new Exception("메시지 ID가 일치하지 않음.");
            }

            int lastSeperatorIndex = -1;
            string[] tokens = new string[10];
            for (int i = 0; i < 10; i++)
            {
                int nextSeperatorIndex = Array.IndexOf(messageBody, Seperator, lastSeperatorIndex + 1);
                if (nextSeperatorIndex < 0)
                {
                    throw new Exception("토큰의 갯수가 일치하지 않음.");
                }

                tokens[i] = Encoding.Default.GetString(messageBody, lastSeperatorIndex + 1, nextSeperatorIndex - lastSeperatorIndex - 1);
                lastSeperatorIndex = nextSeperatorIndex;
            }

            try
            {
                this.ImageNo = tokens[1];
                this.ImageType = (EImageDataType)Enum.Parse(typeof(EImageDataType), tokens[2]);
                this.ImagePath = tokens[3];
                this.ImageWidth = Int32.Parse(tokens[4]);
                this.ImageHeight = Int32.Parse(tokens[5]);
                this.ImageChannels = Int32.Parse(tokens[6]);
                int imageDataLength = Int32.Parse(tokens[7]);
                this.CarNumber = tokens[8];
                string locationLWV = tokens[9];

                if (imageDataLength != messageBody.Length - (lastSeperatorIndex + 1))
                {
                    throw new Exception();
                }

                this.CornersForLWV = StringToCorners(locationLWV);

                this.ImageData = new byte[imageDataLength];
                Array.Copy(messageBody, lastSeperatorIndex + 1, this.ImageData, 0, imageDataLength);
            }
            catch (Exception)
            {
                throw new Exception("메시지 형식이 일치하지 않습니다.");
            }
        }

        private Point[] StringToCorners(string location)
        {
            Point[] corners = null;

            if (location.Length > 0)
            {
                string[] locTokens = location.Split(new char[1] { ',' });
                if (locTokens.Length != 8)
                {
                    throw new Exception("번호판의 네 모서리 점 정보가 부적절함.");
                }

                corners = new Point[4];
                for (int i = 0; i < locTokens.Length; i += 2)
                {
                    Point pt;
                    pt.x = int.Parse(locTokens[i]);
                    pt.y = int.Parse(locTokens[i + 1]);

                    corners[i / 2] = pt;
                }
            }

            return corners;
        }
    }

    public class SogNo_MessageLwvResult : SogNo_Message
    {
        public string ImageNo { get; set; }
        public bool IsLightVehicle { get; set; } = false;
        public double RecScore { get; set; } = -1.0;
        public int RecTime { get; set; } = 0;


        public override byte[] Pack()
        {
            string msgBody = string.Format("{0}#{1}#{2}#{3:F3}#{4}", 
                EMsgID.LwvResult, this.ImageNo, (this.IsLightVehicle ? 1 : 0), this.RecScore, this.RecTime);
            int bodyLen = Encoding.Default.GetBytes(msgBody).Length;

            string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, CharTextSign, bodyLen, msgBody, CharETX);
            return Encoding.Default.GetBytes(message);
        }
        
        public override void UnPack(string messageBody)
        {
            EMsgID msgID = ParseMsgID(messageBody);
            if (msgID != EMsgID.LwvResult)
            {
                throw new Exception("메시지 ID가 일치하지 않음.");
            }

            string[] tokens = messageBody.Split(new char[1] { '#' });
            if (tokens.Length != 5)
            {
                throw new Exception("토큰의 갯수가 일치하지 않음.");
            }

            try
            {
                this.ImageNo = tokens[1];
                this.IsLightVehicle = (int.Parse(tokens[2]) == 0 ? false : true);
                this.RecScore = double.Parse(tokens[3]);
                this.RecTime = int.Parse(tokens[4]);
            }
            catch (Exception)
            {
                throw new Exception("메시지 형식이 일치하지 않습니다.");
            }
        }
    }


    public class SogNo_MessageMatchImage : SogNo_Message
    {
        public EMatchCommand MatchCommand { get; set; }
        public Point[] CornersForMatch { get; set; } = null;
        public string CarNumber { get; set; }
        public string ExParam1 { get; set; }
        public string ExParam2 { get; set; }

        public string ImageNo { get; set; }
        public EImageDataType ImageType { get; set; } = EImageDataType.Raw;
        public string ImagePath { get; set; }
        public byte[] ImageData { get; set; } = null;
        public int ImageWidth { get; set; } = 0;
        public int ImageHeight { get; set; } = 0;
        public int ImageChannels { get; set; } = 1;


        public override byte[] Pack()
        {
            string locationMatch = CornersToString(this.CornersForMatch);

            string imgDesc = string.Format("{0}#{1}#{2}#{3}#{4}#{5}#{6}#{7}#{8}#{9}#{10}#{11}#{12}#", 
                EMsgID.MatchImage, MatchCommand, locationMatch, CarNumber, ExParam1, ExParam2, ImageNo, ImageType, ImagePath, ImageWidth, ImageHeight, ImageChannels, (ImageData?.Length ?? 0));
            byte[] imgDescBytes = Encoding.Default.GetBytes(imgDesc);
            int descLen = imgDescBytes.Length;

            int bodyLen = descLen + (ImageData?.Length ?? 0);
            string msgHeader = string.Format("{0}{1}{2:D8}#", CharSTX, CharBinarySign, bodyLen);
            byte[] msgHeaderBytes = Encoding.Default.GetBytes(msgHeader);

            byte[] message = msgHeaderBytes.Concat(imgDescBytes).Concat(ImageData ?? new byte[0]).Concat(new byte[] { ETX }).ToArray();

            return message;
        }

        private string CornersToString(Point[] corners)
        {
            string location = "";
            if (corners != null)
            {
                foreach (Point pt in corners)
                {
                    if (location.Length > 0)
                        location += ",";

                    location += string.Format("{0},{1}", (int)pt.x, (int)pt.y);
                }
            }

            return location;
        }

        public override void UnPack(string messageBody)
        {
            throw new NotImplementedException();
        }

        public override void UnPack(byte[] messageBody)
        {
            EMsgID msgID = ParseMsgID(messageBody);
            if (msgID != EMsgID.MatchImage)
            {
                throw new Exception("메시지 ID가 일치하지 않음.");
            }

            int lastSeperatorIndex = -1;
            string[] tokens = new string[13];
            for (int i = 0; i < 13; i++)
            {
                int nextSeperatorIndex = Array.IndexOf(messageBody, Seperator, lastSeperatorIndex + 1);
                if (nextSeperatorIndex < 0)
                {
                    throw new Exception("토큰의 갯수가 일치하지 않음.");
                }

                tokens[i] = Encoding.Default.GetString(messageBody, lastSeperatorIndex + 1, nextSeperatorIndex - lastSeperatorIndex - 1);
                lastSeperatorIndex = nextSeperatorIndex;
            }

            try
            {
                this.MatchCommand = (EMatchCommand)Enum.Parse(typeof(EMatchCommand), tokens[1]);
                string locationMatch = tokens[2];
                this.CarNumber = tokens[3];
                this.ExParam1 = tokens[4];
                this.ExParam2 = tokens[5];
                this.ImageNo = tokens[6];
                this.ImageType = (EImageDataType)Enum.Parse(typeof(EImageDataType), tokens[7]);
                this.ImagePath = tokens[8];
                this.ImageWidth = Int32.Parse(tokens[9]);
                this.ImageHeight = Int32.Parse(tokens[10]);
                this.ImageChannels = Int32.Parse(tokens[11]);
                int imageDataLength = Int32.Parse(tokens[12]);
                
                if (imageDataLength != messageBody.Length - (lastSeperatorIndex + 1))
                {
                    throw new Exception();
                }

                this.CornersForMatch = StringToCorners(locationMatch);

                this.ImageData = new byte[imageDataLength];
                Array.Copy(messageBody, lastSeperatorIndex + 1, this.ImageData, 0, imageDataLength);
            }
            catch (Exception)
            {
                throw new Exception("메시지 형식이 일치하지 않습니다.");
            }
        }

        private Point[] StringToCorners(string location)
        {
            Point[] corners = null;

            if (location.Length > 0)
            {
                string[] locTokens = location.Split(new char[1] { ',' });
                if (locTokens.Length != 8)
                {
                    throw new Exception("번호판의 네 모서리 점 정보가 부적절함.");
                }

                corners = new Point[4];
                for (int i = 0; i < locTokens.Length; i += 2)
                {
                    Point pt;
                    pt.x = int.Parse(locTokens[i]);
                    pt.y = int.Parse(locTokens[i + 1]);

                    corners[i / 2] = pt;
                }
            }

            return corners;
        }
    }

    public class SogNo_MessageMatchResult : SogNo_Message
    {
        public class _MatchResult
        {
            public string MatchedTKNo { get; set; }
            public double Similarity { get; set; } = 0.0;
            public double Probability { get; set; } = 0.0;
            public string CarNumber { get; set; }
        }
        
        public string ImageNo { get; set; }
        public List<_MatchResult> Matches { get; set; } = new List<_MatchResult>();
        public int RecTime { get; set; } = 0;


        public override byte[] Pack()
        {
            string matches = "";
            for (int i = 0; i < 3; i++)
            {
                if (i > 0)
                    matches += "#";

                string tkno = "";
                string carnum = "";
                string sim = "";
                string prob = "";

                if (i < Matches.Count)
                {
                    tkno = Matches[i].MatchedTKNo;
                    carnum = Matches[i].CarNumber;
                    sim = string.Format("{0:F3}", Matches[i].Similarity);
                    prob = string.Format("{0:F3}", Matches[i].Probability);
                }

                matches += string.Format("{0}#{1}#{2}#{3}", tkno, carnum, sim, prob);
            }

            string msgBody = string.Format("{0}#{1}#{2}#{3}",
                EMsgID.MatchResult, this.ImageNo, matches, RecTime);
            int bodyLen = Encoding.Default.GetBytes(msgBody).Length;

            string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, CharTextSign, bodyLen, msgBody, CharETX);
            return Encoding.Default.GetBytes(message);
        }

        public override void UnPack(string messageBody)
        {
            EMsgID msgID = ParseMsgID(messageBody);
            if (msgID != EMsgID.MatchResult)
            {
                throw new Exception("메시지 ID가 일치하지 않음.");
            }

            string[] tokens = messageBody.Split(new char[1] { '#' });
            if (tokens.Length != 15)
            {
                throw new Exception("토큰의 갯수가 일치하지 않음.");
            }

            try
            {
                this.ImageNo = tokens[1];

                for (int i = 0; i < 3; i++)
                {
                    string tkno = tokens[i * 4 + 2];
                    string carnum = tokens[i * 4 + 3];
                    string sim = tokens[i * 4 + 4];
                    string prob = tokens[i * 4 + 5];

                    if (tkno?.Length > 0)
                    {
                        _MatchResult match = new _MatchResult();
                        match.MatchedTKNo = tkno;
                        match.CarNumber = carnum;
                        match.Similarity = double.Parse(sim);
                        match.Probability = double.Parse(prob);
                        this.Matches.Add(match);
                    }
                }

                this.RecTime = int.Parse(tokens[14]);
            }
            catch (Exception)
            {
                throw new Exception("메시지 형식이 일치하지 않습니다.");
            }
        }
    }
}
