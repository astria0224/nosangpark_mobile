﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace SogNoLib.Communication
{
	class SogNo_TcpCommunication : IEquatable<SogNo_TcpCommunication>
	{
		private static NLog.Logger _log = NLog.LogManager.GetCurrentClassLogger();

		class StateObject
		{
			public StateObject(Socket sock, int size) 
			{
				this.sock = sock;
				this.buffer_size = size;
				this.buffer = new byte[this.buffer_size];
				this.last = 0;
			}

			public bool IsComplete
			{
				get
				{
					return (this.last == this.buffer_size);
				}
			}

			public int Remaining
			{
				get
				{
					return (this.buffer_size - last);
				}
			}

            public bool ContainsBinary { get; set; } = false;
			public Socket sock = null;
			public int buffer_size = 0;
			public byte[] buffer = null;
			public int last = 0;
		}

		public bool IsConnected
		{
			get
			{
				return (this.sock != null && sock.Connected);
			}
		}

		public const string Unknown = "Unknown";

		public delegate void DisconnectedDelegate(object sender);
		public event DisconnectedDelegate DisconnectEvent;

		public delegate void ReceiveDelegate(object sender, string message);
		public event ReceiveDelegate ReceiveEvent;

        public delegate void ReceiveBytesDelegate(object sender, byte[] message);
        public event ReceiveBytesDelegate ReceiveBytesEvent;

        private Socket sock = null;
		public string EndType { get; set; } = Unknown;
		public int EndNo { get; set; } = 0;

		public string EndName
		{
			get
			{
				return string.Format("{0}#{1}", this.EndType, this.EndNo);
			}
		}
		public string EndIP
		{
			get
			{
				if(this.sock != null)
				{
					return ((IPEndPoint)this.sock.RemoteEndPoint).Address.ToString();
				}
				return "";
			}
		}

		private readonly object _lockerStartClose = new object();

	
		public void StartCommunication(Socket sock, string endType = Unknown, int endNo = 0)
		{
			lock (this._lockerStartClose)
			{
				if (this.sock != null)
				{
					return;
				}

				this.sock = sock;
				this.EndType = endType;
				this.EndNo = endNo;

				_log.Debug("<{0}> StartCommunication", this.EndName);

				ReceiveHeader();
			}
		}

		private void ReceiveHeader()
		{
			_log.Trace("<{0}> ReceiveHeader Start", this.EndName);

			try
			{
				StateObject state = new StateObject(this.sock, SogNo_Message.HeaderLen);

				this.sock.BeginReceive(state.buffer, state.last, state.Remaining, SocketFlags.None,
					new AsyncCallback(ReceiveHeaderCallback), state);
			}
			catch (Exception ex)
			{
				_log.Error(ex, "<{0}> ReceiveHeader Exception", this.EndName);

				CloseCommunication();
			}

			_log.Trace("<{0}> ReceiveHeader End", this.EndName);
		}

		private void ReceiveHeaderCallback(IAsyncResult ar)
		{
			_log.Trace("<{0}> ReceiveHeaderCallback Start", this.EndName);

			try
			{
				StateObject state = (StateObject)ar.AsyncState;

				int bytesRead = state.sock.EndReceive(ar);

				if (bytesRead > 0)
				{
					string recv_msg = Encoding.Default.GetString(state.buffer, state.last, bytesRead);
					_log.Debug("<{0}> [RECV] ({1}) {2}", this.EndName, bytesRead, recv_msg);
					state.last += bytesRead;

					if (state.IsComplete)
					{
						if (state.buffer[0] != SogNo_Message.STX || state.buffer[state.last - 1] != SogNo_Message.Seperator)
						{
							throw new Exception("Header가 STX로 시작하지 않거나 #으로 끝나지 않습니다.");
						}

                        if (state.buffer[1] != SogNo_Message.TextSign && state.buffer[1] != SogNo_Message.BinarySign)
                        {
                            throw new Exception("메세지의 데이터 형식 지정자가 'B'나 'T'가 아닙니다.");
                        }

                        bool containsBinary = (state.buffer[1] == SogNo_Message.BinarySign);

						string sizeStr = Encoding.Default.GetString(state.buffer, 2, 8);
						int bodyLen = Int32.Parse(sizeStr);

						ReceiveBody(bodyLen + SogNo_Message.FooterLen, containsBinary);
					}
					else
					{
						state.sock.BeginReceive(state.buffer, state.last, state.Remaining, SocketFlags.None,
							new AsyncCallback(ReceiveHeaderCallback), state);
					}
				}
				else
				{
					// 반대편 소켓 shutdown 됨.
					CloseCommunication();
				}
			}
			catch (ObjectDisposedException)
			{ // Close()한 소켓의 pending BeginReceive로 무시함 (MSDN doc에 기술됨)
			}
			catch (Exception ex)
			{
				_log.Error(ex, "<{0}> ReceiveHeaderCallback Exception", this.EndName);

				CloseCommunication();
			}

			_log.Trace("<{0}> ReceiveHeaderCallback End", this.EndName);
		}


		public void ReceiveBody(int bodySize, bool containsBinary)
		{
			_log.Trace("<{0}> ReceiveBody Start", this.EndName);

			try
			{
				StateObject state = new StateObject(this.sock, bodySize);
                state.ContainsBinary = containsBinary;

				this.sock.BeginReceive(state.buffer, state.last, state.Remaining, SocketFlags.None,
					new AsyncCallback(ReceiveBodyCallback), state);
			}
			catch (Exception e)
			{
				_log.Error(e, "<{0}> ReceiveBody Exception", this.EndName);

				CloseCommunication();
			}

			_log.Trace("<{0}> ReceiveBody End", this.EndName);
		}

		private void ReceiveBodyCallback(IAsyncResult ar)
		{
			_log.Trace("<{0}> ReceiveBodyCallback Start", this.EndName);

			try
			{
				StateObject state = (StateObject)ar.AsyncState;

				int bytesRead = state.sock.EndReceive(ar);
                _log.Debug("<{0}> [RECV] {1} bytes", this.EndName, bytesRead);

                if (bytesRead > 0)
				{
					string recv_msg = Encoding.Default.GetString(state.buffer, state.last, bytesRead);
					if (!state.ContainsBinary) _log.Debug("<{0}> [RECV] ({1}) {2}", this.EndName, bytesRead, recv_msg);
					state.last += bytesRead;

					if (state.IsComplete)
					{
						if (state.buffer[state.last - 1] != SogNo_Message.ETX)
						{
							throw new Exception("Header가 ETX로 끝나지 않습니다.");
						}

                        if (!state.ContainsBinary)
                        {
                            string msg = Encoding.Default.GetString(state.buffer, 0, state.buffer_size - SogNo_Message.FooterLen);

                            _log.Info("<{0}> {1}", this.EndName, msg);

                            ReceiveEvent?.Invoke(this, msg);

                            ReceiveHeader();
                        }
                        else
                        {
                            byte[] msg = new byte[state.buffer_size - SogNo_Message.FooterLen];
                            Array.Copy(state.buffer, msg, msg.Length);

                            ReceiveBytesEvent?.Invoke(this, msg);

                            ReceiveHeader();
                        }

                        _log.Debug("<{0}> [RECV] Done.", this.EndName);
                    }
					else
					{
						state.sock.BeginReceive(state.buffer, state.last, state.Remaining, SocketFlags.None,
							new AsyncCallback(ReceiveBodyCallback), state);
					}
				}
				else
				{
					// 반대편 소켓 shutdown 됨.
					CloseCommunication();
				}
			}
			catch (ObjectDisposedException)
			{ // Close()한 소켓의 pending BeginReceive로 무시함 (MSDN doc에 기술됨)
			}
			catch (Exception ex)
			{
				_log.Error(ex, "<{0}> ReceiveBodyCallback Exception", this.EndName);
				_log.Debug("<{0}> Connected: {1}, Bound: {2}", this.EndName, this.sock.Connected, this.sock.IsBound);


				CloseCommunication();
			}

			_log.Trace("<{0}> ReceiveBodyCallback End", this.EndName);
		}

		public void Send(byte[] msg, bool containsBinary)
		{
			if (this.IsConnected)
			{
				try
				{
					StateObject state = new StateObject(this.sock, 0);
					state.buffer = msg;
					state.buffer_size = state.buffer.Length;
                    state.ContainsBinary = true;

					this.sock.BeginSend(state.buffer, state.last, state.Remaining, SocketFlags.None,
						new AsyncCallback(SendCallback), state);

					//_log.Info("<{0}> [SEND] {1}", this.EndName, Encoding.Default.GetString(msg));
				}
				catch (Exception ex)
				{
					_log.Error(ex, "<{0}> Send Exception", this.EndName);

					CloseCommunication();
				}
			}
		}

		public void Send(string msg)
		{
			if (this.IsConnected)
			{
				try
				{
					byte[] byteData = Encoding.Default.GetBytes(msg);

					Send(byteData, false);
				}
				catch (Exception ex)
				{
					_log.Error(ex, "<{0}> Send Exception", this.EndName);
				}
			}
		}

		private void SendCallback(IAsyncResult ar)
		{
			try
			{
				StateObject state = (StateObject)ar.AsyncState;

				int bytesSent = state.sock.EndSend(ar);
				_log.Debug("<{0}> [SENT] {1} bytes", this.EndName, bytesSent);
				state.last += bytesSent;

				if (state.IsComplete)
				{
					_log.Debug("<{0}> [SEND] Done.", this.EndName);
				}
				else
				{
					this.sock.BeginSend(state.buffer, state.last, state.Remaining, SocketFlags.None,
						new AsyncCallback(SendCallback), state);
				}
			}
			catch (ObjectDisposedException)
			{ // Close()한 소켓의 pending BeginReceive로 무시함 (MSDN doc에 기술됨)
			}
			catch (Exception ex)
			{
				_log.Error(ex, "<{0}> SendCallback Exception", this.EndName);

				CloseCommunication();
			}
		}

		public void CloseCommunication()
		{
			lock (this._lockerStartClose)
			{
				_log.Trace("<{0}> CloseCommunication Start", this.EndName);
				if (this.sock != null)
				{
					_log.Debug("<{0}> Connected: {1}, Bound: {2}", this.EndName, this.sock.Connected, this.sock.IsBound);

					try
					{
						if (this.sock.Connected)
						{
							this.sock.Shutdown(SocketShutdown.Both);
						}

						this.sock.Close();
						this.sock = null;

						DisconnectEvent?.Invoke(this);
					}
					catch (Exception ex)
					{
						_log.Debug(ex, "<{0}> CloseCommunication Exception", this.EndName);
					}
				}
				_log.Trace("<{0}> CloseCommunication End", this.EndName);
			}
		}

		public bool Equals(SogNo_TcpCommunication other)
		{
			if (other == null)
			{
				return false;
			}

			return ReferenceEquals(this, other);
		}
	}
}
