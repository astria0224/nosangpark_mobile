﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// 일케 세개 선언 필요
using System.Net;
using Newtonsoft.Json.Linq;
using SogNoLib.Park;
using SogNoLib.DataAccess;
using Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;


namespace CheckLightCar
{
    public partial class CheckLightCar_MainForm : Form
    {
        private static NLog.Logger _log = NLog.LogManager.GetCurrentClassLogger();

        private SogNo_DBAccessor DBAcessor = null;
        
        private SogNo_DBAccessor source_accessor = null;
        private string DB_IP = "172.16.10.71";
        private DBInfo Select_DBInfo = new DBInfo();

        // 여러 대의 차량 데이터를 등록할 때 사용하는 변수들

        // private string[] carPLNames; 
        //private string[] carUrls;
        //private string[] cartypes;

        object[,] carsheet;
        private int totalcounts;

        public CheckLightCar_MainForm()
        {
            InitializeComponent();
            DBAcessor = new SogNo_DBAccessor(DB_IP, Select_DBInfo.DBPort, Select_DBInfo.DBName);
            //carPLNames = ReadDB(); //db에서 읽는 방법

            //carPLNames = ReadExcel(@"C:\exl\data3.xlsx", 1);
            //cartypes = ReadExcel(@"C:\exl\data3.xlsx", 2);

            //checkLightCars(carPLNames); // DB입력 진행할 때 활성화


            //checkLightCars(carsheet);
            
        }

        public void lightcar_score(string url)
        {
            float all_lightcar = 0;
            float all_car = 0;
            float score = 0;
            float nodetec_score = 0;
            string searchedcartype;

            ReadExcel(url);

            for (int i = 1; i < totalcounts; i++)
            {
                //MessageBox.Show(carsheet[i, 1].ToString());
                searchedcartype = null;
                if (carsheet[i, 1] != null) searchedcartype = SearchCarType_BYCarPLNo(carsheet[i, 1].ToString());
                
                if (searchedcartype != null)
                {
                    all_car++;
                    if (carsheet[i, 2].ToString() == "경차")
                    {
                        all_lightcar++;
                        if (carsheet[i, 2].ToString() == searchedcartype) score++;
                    }
                    else
                    {
                        if (searchedcartype == "경차") nodetec_score++;
                    }
                }
            }
            MessageBox.Show("전체 차량 수 : " + all_car + "\r" +
                            "검지된 경차 수 : " + all_lightcar + "\r" +
                            "일치 수 : " + score + "\r" +
                            "미인식 경차 수 : " + nodetec_score + "\r" +
                            "경차라고 인식한것 중 일치율(P) : " + (score / all_lightcar) * 100 + "%" + "\r" +
                            "전체경차 중 경차 비율 (R) : " + (score / (score + nodetec_score)) * 100 + "%");

        }

        public void init()
        {
            progressBar1.Style = ProgressBarStyle.Continuous;
            progressBar1.Minimum = 0;
            progressBar1.Maximum = 100;
            progressBar1.Step = 1;
            progressBar1.Value = 0;
        }


        public string[] ReadDB()
        {
            this.source_accessor = new SogNo_DBAccessor(DB_IP, Select_DBInfo.DBPort, Select_DBInfo.DBName);
            IondataRecord[] cardatadb = this.source_accessor.SelectIONdata();
            
            string[] cardata = new string[cardatadb.Length];

            for (int i = 0; i < cardatadb.Length - 1; i++)
            {
                cardata[i] = cardatadb[i].InCarNum1;
            }

            return cardata;
        }

        public string SearchCarType(string carname)
        {
            //확인디비에 검색
            string cartype = null;
            CartypeinfoRecord[] selectedcar = DBAcessor.SearchLightCarByCarName(carname);

            if (selectedcar.Length == 0)
            {
                return null;
            }
            else
            {
                cartype = selectedcar[0].CarType;
            }
            return cartype;
        }

        public string SearchCarType_BYCarPLNo(string carname)
        {
            //확인디비에 검색
            string cartype = null;
            LightCarRecord[] selectedcar = DBAcessor.SearchLightCarByCarPLNo(carname);

            if (selectedcar.Length == 0)
            {
                return null;
            }
            else
            {
                cartype = selectedcar[0].CarType;
            }
            return cartype;
        }

        public void InsertCarData(string carPLno = null, string carname = null, string imageurl = null)
        {
            //MessageBox.Show("데이터가 존재하지 않아 DB에 등록을 진행합니다.");
            
            CheckLightCar_AddForm insertform = new CheckLightCar_AddForm(DBAcessor, carPLno, carname, imageurl);
            insertform.Show();
        }

        public void checkLightCars(object[,] carList)
        {
            // carList만큼 반복시킬거임
            for (int i = 1; i < totalcounts; i++)
            {
                Delay(10);
                this.label5.Text = i.ToString();
                progressBar1.Value = i;

                //번호판명이 db에 있는지 확인
                LightCarRecord[] carRecord = DBAcessor.SearchLightCarByCarPLNo(carList[i,8].ToString());
                if (carRecord.Length == 0)
                {
                    //번호판명으로 차종 확인
                    LightCarRecord lightcar = dustgradeCheck(carList[i,8].ToString());
                    CartypeinfoRecord cartypeinfo = new CartypeinfoRecord();
                    if (lightcar != null)
                    {
                        string cartype = SearchCarType(lightcar.CarName);
                        switch (cartype)
                        {
                            case null:
                                //db에 입력
                                Delay(10);
                                lightcar.CarPLNo = carList[i, 8].ToString();

                                cartypeinfo.CarName = lightcar.CarName;
                                
                                DBAcessor.InsertLightCarInfo(lightcar);
                                DBAcessor.Insertcartypeinfo(cartypeinfo);
                                break;

                            case "경차":
                                //_log.Info("경차입니다.");
                                Delay(10);
                                lightcar.CarPLNo = carList[i, 8].ToString();
                                lightcar.CarType = cartype;

                                DBAcessor.InsertLightCarInfo(lightcar);
                                break;

                            default:
                                Delay(10);
                                //_log.Info("경차가 아닙니다.");
                                lightcar.CarPLNo = carList[i, 8].ToString();
                                lightcar.CarType = cartype;

                                DBAcessor.InsertLightCarInfo(lightcar);
                                break;
                        }
                    }

                }
                else
                {
                    if (carRecord[0].CarType == "경차")
                    {
                        //db에 검색해봤더니 경차일 때
                        _log.Info("경차입니다.");
                    }
                    else
                    {
                        //db에 검색해봤더니 경차가 아닐 때
                        _log.Info("경차가 아닙니다.");
                    }
                }
                

            }
        }

        public bool checkLightCar(string carname)
        {
            // carList만큼 반복시킬거임
            bool is_lightcar = false;
            Delay(10);

            //번호판명이 db에 있는지 확인
            LightCarRecord[] carRecord = DBAcessor.SearchLightCarByCarPLNo(carname);
            if (carRecord.Length == 0)
            {
                //번호판명으로 차종 확인
                LightCarRecord lightcar = dustgradeCheck(carname);
                CartypeinfoRecord cartypeinfo = new CartypeinfoRecord();
                if (lightcar != null)
                {
                    string cartype = SearchCarType(lightcar.CarName);
                    switch (cartype)
                    {
                        case null:
                            //db에 입력
                            is_lightcar = false;
                            Delay(10);
                            lightcar.CarPLNo = carname;

                            cartypeinfo.CarName = lightcar.CarName;

                            DBAcessor.InsertLightCarInfo(lightcar);
                            DBAcessor.Insertcartypeinfo(cartypeinfo);
                            break;

                        case "경차":
                            is_lightcar = true;
                            Delay(10);
                            lightcar.CarPLNo = carname;
                            lightcar.CarType = cartype;

                            DBAcessor.InsertLightCarInfo(lightcar);
                            break;

                        default:
                            Delay(10);
                            is_lightcar = false;
                            lightcar.CarPLNo = carname;
                            lightcar.CarType = cartype;

                            DBAcessor.InsertLightCarInfo(lightcar);
                            break;
                    }
                }
            }
            else
            {
                if (carRecord[0].CarType == "경차")
                {
                    //db에 검색해봤더니 경차일 때
                    is_lightcar = true;
                }
                else
                {
                    //db에 검색해봤더니 경차가 아닐 때
                    is_lightcar = false;
                }
            }
            
            return is_lightcar;
        }
        private object[,] GetTotalValue(Worksheet sheet)
        {
            //사용중인 범위(한번도 사용하지 않은 범위는 포함되지 않음)
            Range usedRange = sheet.UsedRange;
            //마지막 Cell
            Range lastCell = usedRange.SpecialCells(XlCellType.xlCellTypeLastCell);
            //전체 범위 (왼쪽 상단의 Cell부터 사용한 맨마지막 범위까지)
            Range totalRange = sheet.get_Range(sheet.get_Range("A1"), lastCell);
            return (object[,])totalRange.get_Value();
        }

        public void ReadExcel(string strFilePath)
        {
            Microsoft.Office.Interop.Excel.Application application = new Microsoft.Office.Interop.Excel.Application();
            Workbook workbook = application.Workbooks.Open(Filename: strFilePath);
            Worksheet worksheet = workbook.Worksheets.Item[workbook.Worksheets.Count];
            totalcounts = worksheet.UsedRange.Rows.Count;
            this.carsheet = GetTotalValue(worksheet);
            this.label6.Text = totalcounts.ToString();
            progressBar1.Maximum = totalcounts;

            application.Quit();
            
            Marshal.ReleaseComObject(worksheet);
            Marshal.ReleaseComObject(workbook);

            /*
            this.carPLNames = new string[worksheet.UsedRange.Rows.Count - 1];
            this.carUrls = new string[worksheet.UsedRange.Rows.Count - 1];
            

            for (int i = 1; i < worksheet.UsedRange.Rows.Count - 1; i++)
            {
                carPLNames[i] = carsheet[i, 8].ToString();
                carUrls[i] = carsheet[i, 9].ToString();
                
            }
            */
            MessageBox.Show("전체 차량 이름 수 " + totalcounts);
        }

        private void button1_Click(object sender, EventArgs e)
        {

            //ReadExcel(@"C:\exl\jesus.csv");
            //checkLightCars(carsheet);


            string carname;
            carname = textBox1.Text;

            if (checkLightCar(carname))
            {
                MessageBox.Show("경차가 맞습니다.");
            }
            else
            {
                MessageBox.Show("경차가 아닙니다.");
            }
        }

        private LightCarRecord dustgradeCheck(string search_key)
        {
            //5등급 확인하는 URL로 연결
            string query = "searchValue=" + WebUtility.UrlEncode(search_key) + "&searchCondition=1";
            string uri = "https://emissiongrade.mecar.or.kr/grade/userGet.do";
            string responseJSON = null;
            Delay(10);
            
            WebClient webClient = new WebClient();
            webClient.Headers[HttpRequestHeader.Host] = "emissiongrade.mecar.or.kr";
            webClient.Headers[HttpRequestHeader.Referer] = "emissiongrade.mecar.or.kr/";
            webClient.Headers[HttpRequestHeader.Accept] = "application/json";
            webClient.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded;";
            webClient.Headers[HttpRequestHeader.UserAgent] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36";
            webClient.Headers[HttpRequestHeader.KeepAlive] = "true";
            webClient.Encoding = Encoding.UTF8;

            try
            {
                responseJSON = webClient.UploadString(uri, query);
            }
            catch(WebException e)
            {
                while(responseJSON != null)
                {
                    Delay(100);
                    responseJSON = null;
                    WebClient webClient2 = new WebClient();
                    webClient2.Headers[HttpRequestHeader.Host] = "emissiongrade.mecar.or.kr";
                    webClient2.Headers[HttpRequestHeader.Referer] = "emissiongrade.mecar.or.kr/";
                    webClient2.Headers[HttpRequestHeader.Accept] = "application/json";
                    webClient2.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded;";
                    webClient2.Headers[HttpRequestHeader.UserAgent] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36";
                    webClient2.Headers[HttpRequestHeader.KeepAlive] = "true";
                    webClient2.Encoding = Encoding.UTF8;

                    responseJSON = webClient2.UploadString(uri, query);
                }
            }
                //  MessageBox.Show(responseJSON.ToString()); // 전체 데이터 확인용
            JObject resultJson = JObject.Parse(responseJSON);

            if (resultJson["chk"].ToString() == "0")
            {
                //MessageBox.Show("결과가없어요");
                return null;
            }
            else if (resultJson["chk"].ToString() == "1")
            {
                // MessageBox.Show(resultJson["grade"][0]["carNm"].ToString()); // 차량명
                // MessageBox.Show(resultJson["grade"][0]["carYear"].ToString()); // 차량 년형
                // MessageBox.Show(resultJson["grade"][0]["grade"].ToString()); // 5등급여부
                // MessageBox.Show(resultJson["grade"][0]["emiType"].ToString()); // 저감조치여부
                /*
               string caris = "차량 년형 : " + resultJson["grade"][0]["carYear"].ToString() + " 년형 \n 차량명 : " + resultJson["grade"][0]["carNm"].ToString() + "\n";

              string gradeis;
              string emiType = "";

               * 룰 체크용 코드
              if (resultJson["grade"][0]["grade"].ToString() == "5")
              {
                  gradeis = "미세먼지 등급 : 미세먼지 배출등급 5등급 차량 \n";
                  if (resultJson["grade"][0]["emiType"].ToString() == "Y")
                  {
                      emiType = "저감조치 : 완료됨";
                  }
                  else
                  {
                      emiType = "저감조치 : 완료되지 않음(운행불가)";
                  }
              }
              else if (resultJson["grade"][0]["grade"].ToString() == "5X")
              {
                  gradeis = "5등급 여부 : 검증중";
              }
              else
              {
                  gradeis = "5등급 여부 : 5등급이 아님";
              }
              MessageBox.Show(caris + gradeis + emiType);
          }
          
          */
                LightCarRecord returnRecord = new LightCarRecord();
                returnRecord.CarName = resultJson["grade"][0]["carNm"].ToString();
                returnRecord.YearType = resultJson["grade"][0]["carYear"].ToString();
                returnRecord.Grade5 = resultJson["grade"][0]["grade"].ToString();
                returnRecord.Mitigation = resultJson["grade"][0]["emiType"].ToString();

                return returnRecord;
            }
            else
            {
                _log.Info("결과가 2개 이상입니다.");
                return null;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {


            // 정답률 평가용 : lightcar_score(@"C:\exl\data3.xlsx");
        }
        private static DateTime Delay(int MS)
        {
            DateTime ThisMoment = DateTime.Now;
            TimeSpan duration = new TimeSpan(0, 0, 0, 0, MS);
            DateTime AfterWards = ThisMoment.Add(duration);

            while (AfterWards >= ThisMoment)
            {
                System.Windows.Forms.Application.DoEvents();
                ThisMoment = DateTime.Now;
            }

            return DateTime.Now;
        }

        private void new_data_add(object sender, EventArgs e)
        {
            InsertCarData();
        }
    }
}
