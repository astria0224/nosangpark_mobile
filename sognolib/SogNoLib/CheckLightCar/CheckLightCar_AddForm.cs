﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SogNoLib.Park;
using SogNoLib.DataAccess;
using System.Media;

namespace CheckLightCar
{
    public partial class CheckLightCar_AddForm : Form
    {
       

        private SogNo_DBAccessor DBAcessor = null;
        public CheckLightCar_AddForm(SogNo_DBAccessor DBAccessor, string carPLno = null, string carname = null, string url = null)
        {
          
            InitializeComponent();


            this.DBAcessor = DBAccessor;
            textBox1.Text = carPLno;
            textBox2.Text = carname;
            

            SystemSounds.Beep.Play();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            insertcardata();
        }

        private void insertcardata()
        {
            LightCarRecord lightcar = new LightCarRecord();
            CartypeinfoRecord cartypeinfo = new CartypeinfoRecord();
            lightcar.CarPLNo = textBox1.Text;
            lightcar.CarName = textBox2.Text;
            lightcar.CarType = textBox3.Text;
            cartypeinfo.CarName = textBox2.Text;
            cartypeinfo.CarType = textBox3.Text;

            DBAcessor.InsertLightCarInfo(lightcar);
            DBAcessor.Insertcartypeinfo(cartypeinfo);

            this.Close();
            
        }
        
    }
}
