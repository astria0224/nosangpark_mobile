﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Drawing.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SogNoLib.Park;
using SogNoLib.DataAccess;

namespace SogNoLib
{
	
	public partial class SogNoLib_ParkDBViewer : Form
	{
		private SogNo_DBAccessor DBAcessor = null;
		private DBInfo Select_DBInfo = new DBInfo();
		public string ListViewSelectedParkNo;
		public ParkInfoRecord[] ParklistData = null;

		SogNoLib_ParkDB_AdvanceViewer advance = null;		
		public int SelectedRowsNumber = 0;
		public SogNoLib_ParkDBViewer()
		{
			InitializeComponent();

			// 리스트뷰에 열을 먼저 만듬
			ParkListView.View = View.Details;

			ParkListView.BeginUpdate();
			ParkListView.Columns.Add(ParkInfoFields.ParkNo.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkType.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkName.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkOwner.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkRegNum.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkPhone.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkAddr.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkNote.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkIPAddr.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkDomain.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkPort.ToString());
			ParkListView.Columns.Add(ParkInfoFields.DefaultTrfNo.ToString());


		}

		private void SogNoLib_ParkDBControl_Load(object sender, EventArgs e)
		{

		}

		private void RefreshParkList_Click(object sender, EventArgs e)
		{

			ParkListView.Clear(); // 전체 지우기

			ParkListView.BeginUpdate(); // 열 입력 시작
			ParkListView.Columns.Add(ParkInfoFields.ParkNo.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkType.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkName.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkOwner.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkRegNum.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkPhone.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkAddr.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkNote.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkIPAddr.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkDomain.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkPort.ToString());
			ParkListView.Columns.Add(ParkInfoFields.DefaultTrfNo.ToString());

			DBAcessor = new SogNo_DBAccessor(Select_DBInfo.DBIP, Select_DBInfo.DBPort, Select_DBInfo.DBName);
			ParklistData = DBAcessor.SelectParkInfo(); // 레코드에 데이터 넣기

			int n = ParklistData.Count();

			// 폼에 데이터 뿌리기
			for (int i = 0;  n > i; i++)
			{
				ListViewItem a = new ListViewItem(ParklistData[i].ParkNo.ToString());
				a.SubItems.Add(ParklistData[i].ParkType.ToString());
				a.SubItems.Add(ParklistData[i].ParkName.ToString());
				a.SubItems.Add(ParklistData[i].ParkOwner.ToString());
				a.SubItems.Add(ParklistData[i].ParkRegNum.ToString());
				a.SubItems.Add(ParklistData[i].ParkPhone.ToString());
				a.SubItems.Add(ParklistData[i].ParkAddr.ToString());
				a.SubItems.Add(ParklistData[i].ParkNote.ToString());
				a.SubItems.Add(ParklistData[i].ParkIPAddr.ToString());
				a.SubItems.Add(ParklistData[i].ParkDomain.ToString());
				a.SubItems.Add(ParklistData[i].ParkPort.ToString());
				a.SubItems.Add(ParklistData[i].DefaultTrfNo.ToString());
				ParkListView.Items.Add(a);
			}
					   
			ParkListView.EndUpdate();

		}

		private void Advance_View_Click(object sender, EventArgs e) // 수정버튼 클릭
		{
			// 선택되어있는 행의 데이터를 보내줘서 해당 화면에 뿌릴 수 있게 해줌
			if (ParklistData != null)
			{
				advance = new SogNoLib_ParkDB_AdvanceViewer(ListViewSelectedParkNo);
				advance.Show();
			}
		}

		private void ParkListView_SelectedIndexChanged(object sender, EventArgs e) // 셀 선택될 때 해당 번호 가져오기
		{
			int indexnum;
			indexnum = ParkListView.FocusedItem.Index; //선택된 아이템 인덱스 번호 얻기
			ListViewSelectedParkNo = ParkListView.Items[indexnum].SubItems[0].Text; //인덱스 번호의 n번째 아이템 얻기
		}

		private void NewParkAdd_Click(object sender, EventArgs e) // 추가 버튼 클릭
		{
			SogNoLib_ParkDB_AdvanceViewer AddNewPark = new SogNoLib_ParkDB_AdvanceViewer();
			AddNewPark.Show();
		}

		private void TKViewBt_Click(object sender, EventArgs e)
		{
			MessageBox.Show("현재 미구현된 기능입니다.");
		}

		

		private void ParkSearch_Click(object sender, EventArgs e)
		{
			// 리스트뷰 초기화 진행 및 열 입력
			ParkListView.Clear();
			ParkListView.BeginUpdate();
			ParkListView.Columns.Add(ParkInfoFields.ParkNo.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkType.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkName.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkOwner.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkRegNum.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkPhone.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkAddr.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkNote.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkIPAddr.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkDomain.ToString());
			ParkListView.Columns.Add(ParkInfoFields.ParkPort.ToString());
			ParkListView.Columns.Add(ParkInfoFields.DefaultTrfNo.ToString());
			/// 검색어를 받아오기
			if (ParkSearchTextBox.Text != "")
			{
				DBAcessor = new SogNo_DBAccessor(Select_DBInfo.DBIP, Select_DBInfo.DBPort, Select_DBInfo.DBName);
				
				ParklistData = DBAcessor.SearchParkList(ParkSearchTextBox.Text); // 레코드에 데이터 넣기

				int n = ParklistData.Count();

				// 폼에 데이터 뿌리기
				for (int i = 0; n > i; i++)
				{
					ListViewItem a = new ListViewItem(ParklistData[i].ParkNo.ToString());
					a.SubItems.Add(ParklistData[i].ParkType.ToString());
					a.SubItems.Add(ParklistData[i].ParkName.ToString());
					a.SubItems.Add(ParklistData[i].ParkOwner.ToString());
					a.SubItems.Add(ParklistData[i].ParkRegNum.ToString());
					a.SubItems.Add(ParklistData[i].ParkPhone.ToString());
					a.SubItems.Add(ParklistData[i].ParkAddr.ToString());
					a.SubItems.Add(ParklistData[i].ParkNote.ToString());
					a.SubItems.Add(ParklistData[i].ParkIPAddr.ToString());
					a.SubItems.Add(ParklistData[i].ParkDomain.ToString());
					a.SubItems.Add(ParklistData[i].ParkPort.ToString());
					a.SubItems.Add(ParklistData[i].DefaultTrfNo.ToString());
					ParkListView.Items.Add(a);
				}
			}
			else { MessageBox.Show("검색어가 없습니다."); }
			ParkListView.EndUpdate();

			
		}

		private void GateViewBt_Click(object sender, EventArgs e)
		{
			if (ListViewSelectedParkNo != null) { 
			SogNoLib_GateDBViewer GateDBview = new SogNoLib_GateDBViewer(ListViewSelectedParkNo);
			GateDBview.Show();
			}
			else
			{
			SogNoLib_GateDBViewer GateDBview = new SogNoLib_GateDBViewer();
			GateDBview.Show();

			}



		}
	}
}
