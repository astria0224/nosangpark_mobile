﻿namespace SogNoLib
{
	partial class SogNoLib_ParkDB_AdvanceViewer
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.Adv_ParkName = new System.Windows.Forms.TextBox();
			this.Adv_ParkType = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.Adv_ParkOwner = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.Adv_ParkRegNum = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.Adv_ParkPhone = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.Adv_ParkAddr = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.Adv_ParkNote = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.Adv_ParkIPAddr = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.Adv_ParkDomain = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.Adv_ParkPort = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.Adv_DefaultTrfNo = new System.Windows.Forms.TextBox();
			this.Just_TEXT = new System.Windows.Forms.Label();
			this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
			this.ParkDataDeleteBt = new System.Windows.Forms.Button();
			this.UpdateBt = new System.Windows.Forms.CheckBox();
			this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
			this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
			this.label21 = new System.Windows.Forms.Label();
			this.label20 = new System.Windows.Forms.Label();
			this.label19 = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.tableLayoutPanel1.SuspendLayout();
			this.tableLayoutPanel3.SuspendLayout();
			this.tableLayoutPanel2.SuspendLayout();
			this.tableLayoutPanel4.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.label1.ForeColor = System.Drawing.Color.Black;
			this.label1.Location = new System.Drawing.Point(85, 52);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(106, 21);
			this.label1.TabIndex = 0;
			this.label1.Text = "▶ 주차장명 :";
			// 
			// Adv_ParkName
			// 
			this.Adv_ParkName.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.Adv_ParkName.Enabled = false;
			this.Adv_ParkName.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.Adv_ParkName.Location = new System.Drawing.Point(197, 48);
			this.Adv_ParkName.Name = "Adv_ParkName";
			this.Adv_ParkName.Size = new System.Drawing.Size(278, 29);
			this.Adv_ParkName.TabIndex = 2;
			// 
			// Adv_ParkType
			// 
			this.Adv_ParkType.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.Adv_ParkType.Enabled = false;
			this.Adv_ParkType.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.Adv_ParkType.Location = new System.Drawing.Point(197, 6);
			this.Adv_ParkType.Name = "Adv_ParkType";
			this.Adv_ParkType.Size = new System.Drawing.Size(42, 29);
			this.Adv_ParkType.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.label2.ForeColor = System.Drawing.Color.Black;
			this.label2.Location = new System.Drawing.Point(63, 10);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(128, 21);
			this.label2.TabIndex = 2;
			this.label2.Text = "▶ 주차장 타입 :";
			// 
			// Adv_ParkOwner
			// 
			this.Adv_ParkOwner.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.Adv_ParkOwner.Enabled = false;
			this.Adv_ParkOwner.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.Adv_ParkOwner.Location = new System.Drawing.Point(197, 90);
			this.Adv_ParkOwner.Name = "Adv_ParkOwner";
			this.Adv_ParkOwner.Size = new System.Drawing.Size(146, 29);
			this.Adv_ParkOwner.TabIndex = 3;
			// 
			// label3
			// 
			this.label3.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.label3.ForeColor = System.Drawing.Color.Black;
			this.label3.Location = new System.Drawing.Point(101, 94);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(90, 21);
			this.label3.TabIndex = 4;
			this.label3.Text = "▶ 운영자 :";
			// 
			// Adv_ParkRegNum
			// 
			this.Adv_ParkRegNum.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.Adv_ParkRegNum.Enabled = false;
			this.Adv_ParkRegNum.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.Adv_ParkRegNum.Location = new System.Drawing.Point(197, 132);
			this.Adv_ParkRegNum.Name = "Adv_ParkRegNum";
			this.Adv_ParkRegNum.Size = new System.Drawing.Size(278, 29);
			this.Adv_ParkRegNum.TabIndex = 4;
			// 
			// label4
			// 
			this.label4.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.label4.ForeColor = System.Drawing.Color.Black;
			this.label4.Location = new System.Drawing.Point(37, 136);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(154, 21);
			this.label4.TabIndex = 6;
			this.label4.Text = "▶ 사업자등록번호 :";
			// 
			// Adv_ParkPhone
			// 
			this.Adv_ParkPhone.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.Adv_ParkPhone.Enabled = false;
			this.Adv_ParkPhone.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.Adv_ParkPhone.Location = new System.Drawing.Point(197, 174);
			this.Adv_ParkPhone.Name = "Adv_ParkPhone";
			this.Adv_ParkPhone.Size = new System.Drawing.Size(228, 29);
			this.Adv_ParkPhone.TabIndex = 5;
			// 
			// label5
			// 
			this.label5.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.label5.ForeColor = System.Drawing.Color.Black;
			this.label5.Location = new System.Drawing.Point(85, 178);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(106, 21);
			this.label5.TabIndex = 8;
			this.label5.Text = "▶ 전화번호 :";
			// 
			// Adv_ParkAddr
			// 
			this.Adv_ParkAddr.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.Adv_ParkAddr.Enabled = false;
			this.Adv_ParkAddr.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.Adv_ParkAddr.Location = new System.Drawing.Point(197, 213);
			this.Adv_ParkAddr.Multiline = true;
			this.Adv_ParkAddr.Name = "Adv_ParkAddr";
			this.Adv_ParkAddr.Size = new System.Drawing.Size(278, 36);
			this.Adv_ParkAddr.TabIndex = 6;
			// 
			// label6
			// 
			this.label6.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.label6.ForeColor = System.Drawing.Color.Black;
			this.label6.Location = new System.Drawing.Point(63, 220);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(128, 21);
			this.label6.TabIndex = 10;
			this.label6.Text = "▶ 주차장 주소 :";
			// 
			// Adv_ParkNote
			// 
			this.Adv_ParkNote.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.Adv_ParkNote.Enabled = false;
			this.Adv_ParkNote.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.Adv_ParkNote.Location = new System.Drawing.Point(197, 258);
			this.Adv_ParkNote.Name = "Adv_ParkNote";
			this.Adv_ParkNote.Size = new System.Drawing.Size(228, 29);
			this.Adv_ParkNote.TabIndex = 7;
			// 
			// label7
			// 
			this.label7.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.label7.AutoSize = true;
			this.label7.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.label7.ForeColor = System.Drawing.Color.Black;
			this.label7.Location = new System.Drawing.Point(31, 262);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(160, 21);
			this.label7.TabIndex = 12;
			this.label7.Text = "▶ 주차장 전화번호 :";
			// 
			// Adv_ParkIPAddr
			// 
			this.Adv_ParkIPAddr.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.Adv_ParkIPAddr.Enabled = false;
			this.Adv_ParkIPAddr.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.Adv_ParkIPAddr.Location = new System.Drawing.Point(197, 300);
			this.Adv_ParkIPAddr.Name = "Adv_ParkIPAddr";
			this.Adv_ParkIPAddr.Size = new System.Drawing.Size(278, 29);
			this.Adv_ParkIPAddr.TabIndex = 8;
			// 
			// label8
			// 
			this.label8.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.label8.AutoSize = true;
			this.label8.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.label8.ForeColor = System.Drawing.Color.Black;
			this.label8.Location = new System.Drawing.Point(80, 304);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(111, 21);
			this.label8.TabIndex = 14;
			this.label8.Text = "▶ 주차장 IP :";
			// 
			// Adv_ParkDomain
			// 
			this.Adv_ParkDomain.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.Adv_ParkDomain.Enabled = false;
			this.Adv_ParkDomain.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.Adv_ParkDomain.Location = new System.Drawing.Point(197, 342);
			this.Adv_ParkDomain.Name = "Adv_ParkDomain";
			this.Adv_ParkDomain.Size = new System.Drawing.Size(278, 29);
			this.Adv_ParkDomain.TabIndex = 9;
			// 
			// label9
			// 
			this.label9.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.label9.AutoSize = true;
			this.label9.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.label9.ForeColor = System.Drawing.Color.Black;
			this.label9.Location = new System.Drawing.Point(47, 346);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(144, 21);
			this.label9.TabIndex = 16;
			this.label9.Text = "▶ 주차장 도메인 :";
			// 
			// Adv_ParkPort
			// 
			this.Adv_ParkPort.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.Adv_ParkPort.Enabled = false;
			this.Adv_ParkPort.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.Adv_ParkPort.Location = new System.Drawing.Point(197, 384);
			this.Adv_ParkPort.Name = "Adv_ParkPort";
			this.Adv_ParkPort.Size = new System.Drawing.Size(278, 29);
			this.Adv_ParkPort.TabIndex = 10;
			// 
			// label10
			// 
			this.label10.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.label10.AutoSize = true;
			this.label10.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.label10.ForeColor = System.Drawing.Color.Black;
			this.label10.Location = new System.Drawing.Point(63, 388);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(128, 21);
			this.label10.TabIndex = 18;
			this.label10.Text = "▶ 주차장 포트 :";
			// 
			// Adv_DefaultTrfNo
			// 
			this.Adv_DefaultTrfNo.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.Adv_DefaultTrfNo.Enabled = false;
			this.Adv_DefaultTrfNo.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.Adv_DefaultTrfNo.Location = new System.Drawing.Point(197, 430);
			this.Adv_DefaultTrfNo.Name = "Adv_DefaultTrfNo";
			this.Adv_DefaultTrfNo.Size = new System.Drawing.Size(278, 29);
			this.Adv_DefaultTrfNo.TabIndex = 11;
			// 
			// Just_TEXT
			// 
			this.Just_TEXT.Anchor = System.Windows.Forms.AnchorStyles.Right;
			this.Just_TEXT.AutoSize = true;
			this.Just_TEXT.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.Just_TEXT.ForeColor = System.Drawing.Color.Black;
			this.Just_TEXT.Location = new System.Drawing.Point(41, 434);
			this.Just_TEXT.Name = "Just_TEXT";
			this.Just_TEXT.Size = new System.Drawing.Size(150, 21);
			this.Just_TEXT.TabIndex = 20;
			this.Just_TEXT.Text = "▶ 기본 요금 체계 :";
			// 
			// tableLayoutPanel1
			// 
			this.tableLayoutPanel1.ColumnCount = 2;
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 392F));
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 1);
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
			this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 1, 0);
			this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
			this.tableLayoutPanel1.Name = "tableLayoutPanel1";
			this.tableLayoutPanel1.RowCount = 2;
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 91.16161F));
			this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
			this.tableLayoutPanel1.Size = new System.Drawing.Size(880, 530);
			this.tableLayoutPanel1.TabIndex = 22;
			// 
			// tableLayoutPanel3
			// 
			this.tableLayoutPanel3.ColumnCount = 2;
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
			this.tableLayoutPanel3.Controls.Add(this.ParkDataDeleteBt, 1, 0);
			this.tableLayoutPanel3.Controls.Add(this.UpdateBt, 0, 0);
			this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 478);
			this.tableLayoutPanel3.Name = "tableLayoutPanel3";
			this.tableLayoutPanel3.RowCount = 1;
			this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel3.Size = new System.Drawing.Size(482, 49);
			this.tableLayoutPanel3.TabIndex = 24;
			// 
			// ParkDataDeleteBt
			// 
			this.ParkDataDeleteBt.BackColor = System.Drawing.Color.Red;
			this.ParkDataDeleteBt.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.ParkDataDeleteBt.FlatAppearance.BorderSize = 0;
			this.ParkDataDeleteBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.ParkDataDeleteBt.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.ParkDataDeleteBt.ForeColor = System.Drawing.Color.White;
			this.ParkDataDeleteBt.Location = new System.Drawing.Point(244, 3);
			this.ParkDataDeleteBt.Name = "ParkDataDeleteBt";
			this.ParkDataDeleteBt.Size = new System.Drawing.Size(235, 43);
			this.ParkDataDeleteBt.TabIndex = 10;
			this.ParkDataDeleteBt.Text = "삭제";
			this.ParkDataDeleteBt.UseVisualStyleBackColor = false;
			this.ParkDataDeleteBt.Click += new System.EventHandler(this.ParkDataDeleteBT_Click);
			// 
			// UpdateBt
			// 
			this.UpdateBt.Appearance = System.Windows.Forms.Appearance.Button;
			this.UpdateBt.AutoSize = true;
			this.UpdateBt.BackColor = System.Drawing.Color.DodgerBlue;
			this.UpdateBt.Dock = System.Windows.Forms.DockStyle.Fill;
			this.UpdateBt.FlatAppearance.BorderSize = 0;
			this.UpdateBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.UpdateBt.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
			this.UpdateBt.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
			this.UpdateBt.Location = new System.Drawing.Point(3, 3);
			this.UpdateBt.Name = "UpdateBt";
			this.UpdateBt.Size = new System.Drawing.Size(235, 43);
			this.UpdateBt.TabIndex = 12;
			this.UpdateBt.Text = "수정";
			this.UpdateBt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.UpdateBt.UseVisualStyleBackColor = false;
			this.UpdateBt.CheckedChanged += new System.EventHandler(this.UpdateChangeBt);
			// 
			// tableLayoutPanel2
			// 
			this.tableLayoutPanel2.ColumnCount = 2;
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 194F));
			this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel2.Controls.Add(this.Adv_ParkType, 1, 0);
			this.tableLayoutPanel2.Controls.Add(this.Just_TEXT, 0, 10);
			this.tableLayoutPanel2.Controls.Add(this.Adv_DefaultTrfNo, 1, 10);
			this.tableLayoutPanel2.Controls.Add(this.label10, 0, 9);
			this.tableLayoutPanel2.Controls.Add(this.Adv_ParkName, 1, 1);
			this.tableLayoutPanel2.Controls.Add(this.label9, 0, 8);
			this.tableLayoutPanel2.Controls.Add(this.Adv_ParkOwner, 1, 2);
			this.tableLayoutPanel2.Controls.Add(this.label8, 0, 7);
			this.tableLayoutPanel2.Controls.Add(this.Adv_ParkPort, 1, 9);
			this.tableLayoutPanel2.Controls.Add(this.label7, 0, 6);
			this.tableLayoutPanel2.Controls.Add(this.Adv_ParkRegNum, 1, 3);
			this.tableLayoutPanel2.Controls.Add(this.label6, 0, 5);
			this.tableLayoutPanel2.Controls.Add(this.Adv_ParkPhone, 1, 4);
			this.tableLayoutPanel2.Controls.Add(this.label5, 0, 4);
			this.tableLayoutPanel2.Controls.Add(this.Adv_ParkDomain, 1, 8);
			this.tableLayoutPanel2.Controls.Add(this.label4, 0, 3);
			this.tableLayoutPanel2.Controls.Add(this.Adv_ParkAddr, 1, 5);
			this.tableLayoutPanel2.Controls.Add(this.label3, 0, 2);
			this.tableLayoutPanel2.Controls.Add(this.Adv_ParkNote, 1, 6);
			this.tableLayoutPanel2.Controls.Add(this.Adv_ParkIPAddr, 1, 7);
			this.tableLayoutPanel2.Controls.Add(this.label1, 0, 1);
			this.tableLayoutPanel2.Controls.Add(this.label2, 0, 0);
			this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
			this.tableLayoutPanel2.Name = "tableLayoutPanel2";
			this.tableLayoutPanel2.RowCount = 11;
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
			this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
			this.tableLayoutPanel2.Size = new System.Drawing.Size(482, 469);
			this.tableLayoutPanel2.TabIndex = 0;
			// 
			// tableLayoutPanel4
			// 
			this.tableLayoutPanel4.ColumnCount = 1;
			this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
			this.tableLayoutPanel4.Controls.Add(this.label21, 0, 10);
			this.tableLayoutPanel4.Controls.Add(this.label20, 0, 9);
			this.tableLayoutPanel4.Controls.Add(this.label19, 0, 8);
			this.tableLayoutPanel4.Controls.Add(this.label18, 0, 7);
			this.tableLayoutPanel4.Controls.Add(this.label17, 0, 6);
			this.tableLayoutPanel4.Controls.Add(this.label16, 0, 5);
			this.tableLayoutPanel4.Controls.Add(this.label15, 0, 4);
			this.tableLayoutPanel4.Controls.Add(this.label14, 0, 3);
			this.tableLayoutPanel4.Controls.Add(this.label13, 0, 2);
			this.tableLayoutPanel4.Controls.Add(this.label12, 0, 1);
			this.tableLayoutPanel4.Controls.Add(this.label11, 0, 0);
			this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tableLayoutPanel4.Location = new System.Drawing.Point(491, 3);
			this.tableLayoutPanel4.Name = "tableLayoutPanel4";
			this.tableLayoutPanel4.RowCount = 11;
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
			this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.090909F));
			this.tableLayoutPanel4.Size = new System.Drawing.Size(386, 469);
			this.tableLayoutPanel4.TabIndex = 25;
			// 
			// label21
			// 
			this.label21.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label21.AutoSize = true;
			this.label21.Location = new System.Drawing.Point(3, 438);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(308, 12);
			this.label21.TabIndex = 10;
			this.label21.Text = "주차장의 기본요금체계를 입력해주세요. / 1: ~~~ 2:~~";
			// 
			// label20
			// 
			this.label20.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label20.AutoSize = true;
			this.label20.Location = new System.Drawing.Point(3, 393);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(256, 12);
			this.label20.TabIndex = 9;
			this.label20.Text = "주차장의 포트 번호를 입력해 주세요. ex) 6690";
			// 
			// label19
			// 
			this.label19.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label19.AutoSize = true;
			this.label19.Location = new System.Drawing.Point(3, 351);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(193, 12);
			this.label19.TabIndex = 8;
			this.label19.Text = "주차장의 도메인을 입력해 주세요. ";
			// 
			// label18
			// 
			this.label18.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(3, 309);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(276, 12);
			this.label18.TabIndex = 7;
			this.label18.Text = "주차장의 아이피를 입력해 주세요. ex) 192.168.0.1";
			// 
			// label17
			// 
			this.label17.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(3, 267);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(300, 12);
			this.label17.TabIndex = 6;
			this.label17.Text = "주차장의 전화번호를 입력해 주세요. ex) 063-000-0000";
			// 
			// label16
			// 
			this.label16.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(3, 219);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(358, 24);
			this.label16.TabIndex = 5;
			this.label16.Text = "주차장의 주소를 입력해 주세요. ex) 전라북도 전주시 팔복동 4가 241-11";
			// 
			// label15
			// 
			this.label15.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(3, 183);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(334, 12);
			this.label15.TabIndex = 4;
			this.label15.Text = "핸드폰번호 등의 연락처를 입력해 주세요. ex) 010-0000-0000";
			// 
			// label14
			// 
			this.label14.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(3, 141);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(282, 12);
			this.label14.TabIndex = 3;
			this.label14.Text = "- 사업자등록번호를 입력해주세요 ex)000-00-00000";
			// 
			// label13
			// 
			this.label13.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(3, 99);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(258, 12);
			this.label13.TabIndex = 2;
			this.label13.Text = "- 주차장 운영자명을 입력해 주세요 ex) 홍길동";
			// 
			// label12
			// 
			this.label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(3, 57);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(294, 12);
			this.label12.TabIndex = 1;
			this.label12.Text = "- 주차장의 이름을 입력해 주세요 ex) 에스티원주차장";
			// 
			// label11
			// 
			this.label11.Anchor = System.Windows.Forms.AnchorStyles.Left;
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(3, 15);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(247, 12);
			this.label11.TabIndex = 0;
			this.label11.Text = "- 0 : 일반주차장, 1 : 등록사용자 전용 주차장";
			// 
			// SogNoLib_ParkDB_AdvanceViewer
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
			this.ClientSize = new System.Drawing.Size(880, 530);
			this.Controls.Add(this.tableLayoutPanel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.Name = "SogNoLib_ParkDB_AdvanceViewer";
			this.Text = "주차장 정보 상세보기";
			this.tableLayoutPanel1.ResumeLayout(false);
			this.tableLayoutPanel3.ResumeLayout(false);
			this.tableLayoutPanel3.PerformLayout();
			this.tableLayoutPanel2.ResumeLayout(false);
			this.tableLayoutPanel2.PerformLayout();
			this.tableLayoutPanel4.ResumeLayout(false);
			this.tableLayoutPanel4.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox Adv_ParkName;
		private System.Windows.Forms.TextBox Adv_ParkType;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox Adv_ParkOwner;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox Adv_ParkRegNum;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox Adv_ParkPhone;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox Adv_ParkAddr;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox Adv_ParkNote;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox Adv_ParkIPAddr;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox Adv_ParkDomain;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox Adv_ParkPort;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox Adv_DefaultTrfNo;
		private System.Windows.Forms.Label Just_TEXT;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
		private System.Windows.Forms.Button ParkDataDeleteBt;
		private System.Windows.Forms.CheckBox UpdateBt;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label11;
	}
}