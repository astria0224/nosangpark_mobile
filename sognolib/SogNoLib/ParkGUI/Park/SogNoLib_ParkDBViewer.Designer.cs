﻿namespace SogNoLib
{
	partial class SogNoLib_ParkDBViewer
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.ParkSearchTextBox = new System.Windows.Forms.TextBox();
            this.ParkSearch = new System.Windows.Forms.Button();
            this.GateViewBt = new System.Windows.Forms.Button();
            this.RefreshParkList = new System.Windows.Forms.Button();
            this.Advance_View = new System.Windows.Forms.Button();
            this.TKViewBt = new System.Windows.Forms.Button();
            this.ParkListView = new System.Windows.Forms.ListView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.NewParkAdd = new System.Windows.Forms.Button();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // ParkSearchTextBox
            // 
            this.ParkSearchTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ParkSearchTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ParkSearchTextBox.Font = new System.Drawing.Font("굴림", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ParkSearchTextBox.Location = new System.Drawing.Point(3, 3);
            this.ParkSearchTextBox.Name = "ParkSearchTextBox";
            this.ParkSearchTextBox.Size = new System.Drawing.Size(264, 41);
            this.ParkSearchTextBox.TabIndex = 0;
            // 
            // ParkSearch
            // 
            this.ParkSearch.AccessibleRole = System.Windows.Forms.AccessibleRole.Border;
            this.ParkSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(126)))), ((int)(((byte)(50)))));
            this.ParkSearch.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ParkSearch.Dock = System.Windows.Forms.DockStyle.Left;
            this.ParkSearch.FlatAppearance.BorderSize = 0;
            this.ParkSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ParkSearch.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ParkSearch.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ParkSearch.Location = new System.Drawing.Point(273, 3);
            this.ParkSearch.Name = "ParkSearch";
            this.ParkSearch.Size = new System.Drawing.Size(102, 43);
            this.ParkSearch.TabIndex = 1;
            this.ParkSearch.Text = "검색";
            this.ParkSearch.UseVisualStyleBackColor = false;
            this.ParkSearch.Click += new System.EventHandler(this.ParkSearch_Click);
            // 
            // GateViewBt
            // 
            this.GateViewBt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(147)))), ((int)(((byte)(235)))));
            this.GateViewBt.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.GateViewBt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GateViewBt.FlatAppearance.BorderSize = 0;
            this.GateViewBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GateViewBt.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.GateViewBt.ForeColor = System.Drawing.Color.White;
            this.GateViewBt.Location = new System.Drawing.Point(3, 3);
            this.GateViewBt.Name = "GateViewBt";
            this.GateViewBt.Size = new System.Drawing.Size(224, 37);
            this.GateViewBt.TabIndex = 5;
            this.GateViewBt.Text = "게이트";
            this.GateViewBt.UseVisualStyleBackColor = false;
            this.GateViewBt.Click += new System.EventHandler(this.GateViewBt_Click);
            // 
            // RefreshParkList
            // 
            this.RefreshParkList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(126)))), ((int)(((byte)(50)))));
            this.RefreshParkList.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.RefreshParkList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RefreshParkList.FlatAppearance.BorderSize = 0;
            this.RefreshParkList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.RefreshParkList.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.RefreshParkList.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.RefreshParkList.Location = new System.Drawing.Point(3, 3);
            this.RefreshParkList.Name = "RefreshParkList";
            this.RefreshParkList.Size = new System.Drawing.Size(181, 43);
            this.RefreshParkList.TabIndex = 6;
            this.RefreshParkList.Text = "새로고침(F5)";
            this.RefreshParkList.UseVisualStyleBackColor = false;
            this.RefreshParkList.Click += new System.EventHandler(this.RefreshParkList_Click);
            // 
            // Advance_View
            // 
            this.Advance_View.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(254)))), ((int)(((byte)(126)))), ((int)(((byte)(50)))));
            this.Advance_View.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Advance_View.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Advance_View.FlatAppearance.BorderSize = 0;
            this.Advance_View.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Advance_View.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Advance_View.ForeColor = System.Drawing.Color.White;
            this.Advance_View.Location = new System.Drawing.Point(3, 3);
            this.Advance_View.Name = "Advance_View";
            this.Advance_View.Size = new System.Drawing.Size(228, 37);
            this.Advance_View.TabIndex = 8;
            this.Advance_View.Text = "상세보기 및 수정,삭제";
            this.Advance_View.UseVisualStyleBackColor = false;
            this.Advance_View.Click += new System.EventHandler(this.Advance_View_Click);
            // 
            // TKViewBt
            // 
            this.TKViewBt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(39)))), ((int)(((byte)(39)))));
            this.TKViewBt.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.TKViewBt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TKViewBt.FlatAppearance.BorderSize = 0;
            this.TKViewBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.TKViewBt.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.TKViewBt.ForeColor = System.Drawing.Color.White;
            this.TKViewBt.Location = new System.Drawing.Point(233, 3);
            this.TKViewBt.Name = "TKViewBt";
            this.TKViewBt.Size = new System.Drawing.Size(221, 37);
            this.TKViewBt.TabIndex = 9;
            this.TKViewBt.Text = "정기권";
            this.TKViewBt.UseVisualStyleBackColor = false;
            this.TKViewBt.Click += new System.EventHandler(this.TKViewBt_Click);
            // 
            // ParkListView
            // 
            this.ParkListView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ParkListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ParkListView.FullRowSelect = true;
            this.ParkListView.GridLines = true;
            this.ParkListView.Location = new System.Drawing.Point(3, 64);
            this.ParkListView.Name = "ParkListView";
            this.ParkListView.Size = new System.Drawing.Size(935, 398);
            this.ParkListView.TabIndex = 10;
            this.ParkListView.UseCompatibleStateImageBehavior = false;
            this.ParkListView.SelectedIndexChanged += new System.EventHandler(this.ParkListView_SelectedIndexChanged);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.5F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ParkListView, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.11828F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 86.88172F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(941, 520);
            this.tableLayoutPanel1.TabIndex = 11;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 193F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel7, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(935, 55);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.82065F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.17935F));
            this.tableLayoutPanel4.Controls.Add(this.ParkSearchTextBox, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.ParkSearch, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(736, 49);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.RefreshParkList, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(745, 3);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(187, 49);
            this.tableLayoutPanel7.TabIndex = 13;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 472F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel6, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel5, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 468);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(935, 49);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 232F));
            this.tableLayoutPanel6.Controls.Add(this.NewParkAdd, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.Advance_View, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(466, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(466, 43);
            this.tableLayoutPanel6.TabIndex = 12;
            // 
            // NewParkAdd
            // 
            this.NewParkAdd.BackColor = System.Drawing.Color.LightSeaGreen;
            this.NewParkAdd.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.NewParkAdd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.NewParkAdd.FlatAppearance.BorderSize = 0;
            this.NewParkAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NewParkAdd.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.NewParkAdd.ForeColor = System.Drawing.Color.White;
            this.NewParkAdd.Location = new System.Drawing.Point(237, 3);
            this.NewParkAdd.Name = "NewParkAdd";
            this.NewParkAdd.Size = new System.Drawing.Size(226, 37);
            this.NewParkAdd.TabIndex = 9;
            this.NewParkAdd.Text = "신규 주차장 추가";
            this.NewParkAdd.UseVisualStyleBackColor = false;
            this.NewParkAdd.Click += new System.EventHandler(this.NewParkAdd_Click);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.4F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.6F));
            this.tableLayoutPanel5.Controls.Add(this.GateViewBt, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.TKViewBt, 1, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(457, 43);
            this.tableLayoutPanel5.TabIndex = 1;
            // 
            // SogNoLib_ParkDBViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Bisque;
            this.ClientSize = new System.Drawing.Size(941, 520);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "SogNoLib_ParkDBViewer";
            this.Text = "에스티원 주차장 DB 관리 프로그램";
            this.Load += new System.EventHandler(this.SogNoLib_ParkDBControl_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TextBox ParkSearchTextBox;
		private System.Windows.Forms.Button ParkSearch;
		private System.Windows.Forms.Button RefreshParkList;
		private System.Windows.Forms.ListView ParkListView;
		private System.Windows.Forms.Button GateViewBt;
		private System.Windows.Forms.Button Advance_View;
		private System.Windows.Forms.Button TKViewBt;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
		private System.Windows.Forms.Button NewParkAdd;
	}
}