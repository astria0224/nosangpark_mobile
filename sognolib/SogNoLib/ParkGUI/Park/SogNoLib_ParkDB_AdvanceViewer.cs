﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SogNoLib.Park;
using SogNoLib.DataAccess;

namespace SogNoLib
{
	
	public partial class SogNoLib_ParkDB_AdvanceViewer : Form
	{

		private int UpdateState = 0; // 업데이트 폼의 수정 개시 및 확정에 쓰임. 0 : 기본 상태, 1 : 수정 진행 중
		private int UpdateBtnHandler = 0; // 업데이트인지, 신규추가인지 구분하는 용도. 0 : 수정 모드, 1 : 추가 모드

		private string ParkNo_to_Update = null;
		private SogNo_DBAccessor DBAcessor = null;
		private DBInfo Select_DBInfo = new DBInfo();
        public ParkInfoRecord[] SelectedParkInfoRecord = null;
		public SogNoLib_ParkDB_AdvanceViewer(string ParkNo = null)
		{
			InitializeComponent();


			// 주차장 키값(ParkNo)이 있는 경우 수정 모드로 진행
			if (ParkNo != null)
			{
				ParkNo_to_Update = ParkNo;
				DBAcessor = new SogNo_DBAccessor(Select_DBInfo.DBIP, Select_DBInfo.DBPort, Select_DBInfo.DBName);
				SelectedParkInfoRecord = DBAcessor.SelectParkInfo(int.Parse(ParkNo));

				// 해당하는 주차장 정보를 가져옵니다.
				Adv_ParkType.Text = SelectedParkInfoRecord[0].ParkType.ToString();
				Adv_ParkName.Text = SelectedParkInfoRecord[0].ParkName.ToString();
				Adv_ParkOwner.Text = SelectedParkInfoRecord[0].ParkOwner.ToString();
				Adv_ParkRegNum.Text = SelectedParkInfoRecord[0].ParkRegNum.ToString();
				Adv_ParkPhone.Text = SelectedParkInfoRecord[0].ParkPhone.ToString();
				Adv_ParkAddr.Text = SelectedParkInfoRecord[0].ParkAddr.ToString();
				Adv_ParkNote.Text = SelectedParkInfoRecord[0].ParkNote.ToString();
				Adv_ParkIPAddr.Text = SelectedParkInfoRecord[0].ParkIPAddr.ToString();
				Adv_ParkDomain.Text = SelectedParkInfoRecord[0].ParkDomain.ToString();
				Adv_ParkPort.Text = SelectedParkInfoRecord[0].ParkPort.ToString();
				Adv_DefaultTrfNo.Text = SelectedParkInfoRecord[0].DefaultTrfNo.ToString();

			}
			// 주차장 키값(ParkNo)이 없는 경우 추가 모드로 진행
			else
			{
				//모드 변경 (추가모드)
				UpdateBtnHandler = 1;

				//DB 접속
				DBAcessor = new SogNo_DBAccessor(Select_DBInfo.DBIP, Select_DBInfo.DBPort, Select_DBInfo.DBName);

				//버튼 색상및 텍스트 재설정
				UpdateBt.BackColor = Color.Crimson;
				UpdateBt.Text = "추가";

				ParkDataDeleteBt.BackColor = Color.RosyBrown;
				ParkDataDeleteBt.Text = "닫기";

				// TEXTBOX들에 입력이 가능하게 상태 변경
				Adv_ParkType.Enabled = true;
				Adv_ParkName.Enabled = true;
				Adv_ParkOwner.Enabled = true;
				Adv_ParkRegNum.Enabled = true;
				Adv_ParkPhone.Enabled = true;
				Adv_ParkAddr.Enabled = true;
				Adv_ParkNote.Enabled = true;
				Adv_ParkIPAddr.Enabled = true;
				Adv_ParkDomain.Enabled = true;
				Adv_ParkPort.Enabled = true;
				Adv_DefaultTrfNo.Enabled = true;

				
			}

			

		
		}

		private void UpdateChangeBt(object sender, EventArgs e)
		{
			// 수정 모드일 때(UpdateBtnHandler == 0)는 수정 버튼으로 기능합니다.
			if (UpdateBtnHandler == 0) 
			{
				// 편집 진행중이 아닐 때 (UpdateState == 0)는 수정 버튼으로 기능합니다.
				if (UpdateState == 0)
				{
					UpdateBt.BackColor = Color.Coral;
					UpdateBt.Text = "수정 완료";
					UpdateState = 1;
					ParkDataDeleteBt.BackColor = Color.Gray;
					ParkDataDeleteBt.Text = "취소";

					// 선택 가능하도록 변경
					Adv_ParkType.Enabled = true;
					Adv_ParkName.Enabled = true;
					Adv_ParkOwner.Enabled = true;
					Adv_ParkRegNum.Enabled = true;
					Adv_ParkPhone.Enabled = true;
					Adv_ParkAddr.Enabled = true;
					Adv_ParkNote.Enabled = true;
					Adv_ParkIPAddr.Enabled = true;
					Adv_ParkDomain.Enabled = true;
					Adv_ParkPort.Enabled = true;
					Adv_DefaultTrfNo.Enabled = true;
				}
				// 편집 진행중일 때 (UpdateState == 1)는 수정 완료 버튼으로 기능합니다.
				else
				{
					UpdateBt.BackColor = Color.DodgerBlue;
					UpdateState = 0;
					UpdateBt.Text = "수정";
					ParkDataDeleteBt.BackColor = Color.Red;
					ParkDataDeleteBt.Text = "삭제";

					// 선택 못하게 변경
					Adv_ParkType.Enabled = false;
					Adv_ParkName.Enabled = false;
					Adv_ParkOwner.Enabled = false;
					Adv_ParkRegNum.Enabled = false;
					Adv_ParkPhone.Enabled = false;
					Adv_ParkAddr.Enabled = false;
					Adv_ParkNote.Enabled = false;
					Adv_ParkIPAddr.Enabled = false;
					Adv_ParkDomain.Enabled = false;
					Adv_ParkPort.Enabled = false;
					Adv_DefaultTrfNo.Enabled = false;

					

					////// 여기부터 수정 진행
					ParkInfoRecord UpdateRecord = new ParkInfoRecord();
					UpdateRecord.ParkNo = Int32.Parse(ParkNo_to_Update);
					UpdateRecord.ParkType = Int32.Parse(Adv_ParkType.Text); //주차장 타입 (0: 일반 주차장, 1: 등록 사용자 전용)
					UpdateRecord.ParkName = Adv_ParkName.Text; //주차장명
					UpdateRecord.ParkOwner = Adv_ParkOwner.Text; //운영자
					UpdateRecord.ParkRegNum = Adv_ParkRegNum.Text; //사업자등록번호
					UpdateRecord.ParkPhone = Adv_ParkPhone.Text; //전화번호
					UpdateRecord.ParkAddr = Adv_ParkAddr.Text; //주차장 주소
					UpdateRecord.ParkNote = Adv_ParkNote.Text; //주차장 전화번호
					UpdateRecord.ParkIPAddr = Adv_ParkIPAddr.Text; //주차장 메인IP
					UpdateRecord.ParkDomain = Adv_ParkDomain.Text; //주차장 도메인
					UpdateRecord.ParkPort = Int32.Parse(Adv_ParkPort.Text); //주차장 포트
					UpdateRecord.DefaultTrfNo = Int32.Parse(Adv_DefaultTrfNo.Text); //기본 요금체계 번호
					UpdateRecord.Reserve0 = null;

					DBAcessor.UpdateParkInfo(UpdateRecord);

					MessageBox.Show("수정이 진행되었습니다.");

				}
			}
			// 추가 모드일 때(UpdateBtnHandler == 1)는 입력버튼으로 기능합니다.  
			if (UpdateBtnHandler == 1)
			{
				
				ParkInfoRecord InsertRecord = new ParkInfoRecord();

				InsertRecord.ParkType = Int32.Parse(Adv_ParkType.Text); //주차장 타입 (0: 일반 주차장, 1: 등록 사용자 전용)
				InsertRecord.ParkName = Adv_ParkName.Text; //주차장명
				InsertRecord.ParkOwner = Adv_ParkOwner.Text; //운영자
				InsertRecord.ParkRegNum = Adv_ParkRegNum.Text; //사업자등록번호
				InsertRecord.ParkPhone = Adv_ParkPhone.Text; //전화번호
				InsertRecord.ParkAddr = Adv_ParkAddr.Text; //주차장 주소
				InsertRecord.ParkNote = Adv_ParkNote.Text; //주차장 전화번호
				InsertRecord.ParkIPAddr = Adv_ParkIPAddr.Text; //주차장 메인IP
				InsertRecord.ParkDomain = Adv_ParkDomain.Text; //주차장 도메인
				InsertRecord.ParkPort = Int32.Parse(Adv_ParkPort.Text); //주차장 포트
				InsertRecord.DefaultTrfNo = Int32.Parse(Adv_DefaultTrfNo.Text); //기본 요금체계 번호
				InsertRecord.Reserve0 = null;

				DBAcessor.InsertParkInfo(InsertRecord);
				MessageBox.Show("추가를 진행했습니다.");

				this.Close();
			}


		}

		private void ParkDataDeleteBT_Click(object sender, EventArgs e) 
		{
			if (UpdateBtnHandler == 0) // 삭제 버튼(수정모드)
			{
				// 수정중이 아닐 때(Updatestate == 0) 삭제 버튼으로 기능합니다. 
				if (UpdateState == 0)
				{ 
					////// 여기부터 삭제기능 구현예정

					DialogResult dialog = MessageBox.Show("정말 삭제하시겠습니까?", "삭제확인메시지", MessageBoxButtons.YesNo);
					if (dialog == DialogResult.Yes)
					{
						DBAcessor = new SogNo_DBAccessor(Select_DBInfo.DBIP, Select_DBInfo.DBPort, Select_DBInfo.DBName);
						DBAcessor.DeleteParkInfo(SelectedParkInfoRecord[0]);
						MessageBox.Show("삭제를 진행하였습니다.");
						this.Close();
					}
					else if (dialog == DialogResult.No)
					{
						MessageBox.Show("삭제를 취소하였습니다.");
					}
				}
				// 수정이 진행중일 때(Updatestate == 1)는 되돌리기 버튼으로 기능합니다. 
				else if (UpdateState == 1) 
				{
					UpdateBt.BackColor = Color.DodgerBlue;
					UpdateState = 0;
					UpdateBt.Text = "수정";
					ParkDataDeleteBt.BackColor = Color.Red;
					ParkDataDeleteBt.Text = "삭제";


					// 주차장 정보를 다시 가져옵니다.
					Adv_ParkType.Text = SelectedParkInfoRecord[0].ParkType.ToString();
					Adv_ParkName.Text = SelectedParkInfoRecord[0].ParkName.ToString();
					Adv_ParkOwner.Text = SelectedParkInfoRecord[0].ParkOwner.ToString();
					Adv_ParkRegNum.Text = SelectedParkInfoRecord[0].ParkRegNum.ToString();
					Adv_ParkPhone.Text = SelectedParkInfoRecord[0].ParkPhone.ToString();
					Adv_ParkAddr.Text = SelectedParkInfoRecord[0].ParkAddr.ToString();
					Adv_ParkNote.Text = SelectedParkInfoRecord[0].ParkNote.ToString();
					Adv_ParkIPAddr.Text = SelectedParkInfoRecord[0].ParkIPAddr.ToString();
					Adv_ParkDomain.Text = SelectedParkInfoRecord[0].ParkDomain.ToString();
					Adv_ParkPort.Text = SelectedParkInfoRecord[0].ParkPort.ToString();
					Adv_DefaultTrfNo.Text = SelectedParkInfoRecord[0].DefaultTrfNo.ToString();

					// 선택 못하게 변경
					Adv_ParkType.Enabled = false;
					Adv_ParkName.Enabled = false;
					Adv_ParkOwner.Enabled = false;
					Adv_ParkRegNum.Enabled = false;
					Adv_ParkPhone.Enabled = false;
					Adv_ParkAddr.Enabled = false;
					Adv_ParkNote.Enabled = false;
					Adv_ParkIPAddr.Enabled = false;
					Adv_ParkDomain.Enabled = false;
					Adv_ParkPort.Enabled = false;
					Adv_DefaultTrfNo.Enabled = false;
				}
				else { }
			}
			// 추가가 진행중일 때(UpdateBtnHandler == 1)는 닫기 버튼으로 기능합니다. 
			else if (UpdateBtnHandler == 1) 
			{
				this.Close();
			}
			else { }

		}
	}
}
