﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SogNoLib.Park;
using SogNoLib.DataAccess;

namespace SogNoLib
{
    public partial class SogNoLib_GateDBViewer : Form
    {
        // 현재 선택된 주차장 조회에 쓰는 레코드
        private ParkInfoRecord thisparkinfo = null;

        // 특정 리스트 조회에 쓰는 레코드
        private GateInfoRecord selectedGateInfo = null;
        private CamInfoRecord selectedCamInfo = null;
        private BarInfoRecord selectedBarInfo = null;
        private ScrInfoRecord selectedScrInfo = null;
        private PCInfoRecord selectedPcInfo = null;

        // Import에 사용하는 레코드
        private GateInfoRecord importGateInfo = null;
        private CamInfoRecord importCamInfo = null;
        private BarInfoRecord importBarInfo = null;
        private ScrInfoRecord importScrInfo = null;
        private PCInfoRecord importPcInfo = null;

        // UI 왼쪽의 Treeview 조회에 쓰는 레코드배열
        private ParkInfoRecord[] parkInfo = null;
        private GateInfoRecord[] gateInfo = null;
        private CamInfoRecord[] camInfo = null;
        private BarInfoRecord[] barInfo = null;
        private ScrInfoRecord[] scrInfo = null;
        private PCInfoRecord[] pcInfo = null;



        private SogNo_DBAccessor dbAcessor = null;
        private DBInfo select_DBInfo = new DBInfo();

        private string updateState = null; // IMPORT : 추가 모드 / UPDATE : 수정 모드 / UPDATING : 수정 진행중
        private string selectedDataType = "GATE"; // 지금 TreeView에서 고른게 뭔지 저장해둠 (추가,수정,삭제 시 사용)

        private string selectedParkID = null;
        private int selectedGateNo;
        private int selectedNodeNo;

        // 추가 레코드 초기값 설정
        private void InitializeImportRecords()
        {
            importGateInfo = new GateInfoRecord();
            importCamInfo = new CamInfoRecord();
            importBarInfo = new BarInfoRecord();
            importScrInfo = new ScrInfoRecord();
            importPcInfo = new PCInfoRecord();

            // 게이트 기본값(자문 구해서 넣을 예정)
            this.importGateInfo.GateName = "기본 게이트";
            this.importGateInfo.GateIP = "192.168.0.1";
            this.importGateInfo.GatePort = 1234;
            this.importGateInfo.LPRBoardIP = "192.168.0.1";
            this.importGateInfo.LPRBoardPort = 1234;

            // 카메라 기본값(자문 구해서 넣을 예정)
            this.importCamInfo.CamName = "기본카메라";
            this.importCamInfo.Target = 0;
            this.importCamInfo.EMax = 0;
            this.importCamInfo.EMin = 0;
            this.importCamInfo.AutoCCount = 0;
            this.importCamInfo.CapCount = 0;
            this.importCamInfo.FrameRate = "5";
            this.importCamInfo.CamIP = "127.0.0.1";
            this.importCamInfo.CamIO = 0;
            this.importCamInfo.CamOrder = 0;
            this.importCamInfo.Cmd = "cmd";
            this.importCamInfo.PreBarOpen = 0;
            this.importCamInfo.BarNo = null;
            this.importCamInfo.ScrNo = null;
            this.importCamInfo.FourCamNo = null;
            this.importCamInfo.RecnArea = "area";
            this.importCamInfo.RecnSet = "set";
            this.importCamInfo.Reserve0 = "";

            // 차단기 기본값(자문 구해서 넣을 예정)
            this.importBarInfo.BarName = "기본차단기";
            this.importBarInfo.BarIP = "127.0.0.1";
            this.importBarInfo.BarPort = 1234;
            this.importBarInfo.BarACTime = 10;
            this.importBarInfo.BarConnType = 10;
            this.importBarInfo.BarOpenCmd = "Open";
            this.importBarInfo.BarCloseCmd = "Close";
            this.importBarInfo.BarOneWay = 10;

            // 전광판 기본값(자문구해서 넣을 예정)
            this.importScrInfo.SCRName = "기본전광판";
            this.importScrInfo.SCRIP = "127.0.0.1";
            this.importScrInfo.SCRPort = 1234;
            this.importScrInfo.SCRTime = 10;
            this.importScrInfo.UpText = "전광판기본문구";
            this.importScrInfo.UpColor = 1;
            this.importScrInfo.DownText = "12가1234";
            this.importScrInfo.DownColor = 1;

            // PC 기본값(자문 구하여 넣을 예정)
            this.importPcInfo.Name = "기본 제어PC";
            this.importPcInfo.IP = "127.0.0.1";
            this.importPcInfo.InternetIP = "192.168.0.1";
            this.importPcInfo.Memo = "PC 기능정보";
        }

        public SogNoLib_GateDBViewer(string ListViewSelectedParkNo = null)
        {
            InitializeComponent();
            InitializeImportRecords();

            if (ListViewSelectedParkNo != null)
            {
                selectedParkID = ListViewSelectedParkNo;
            }

            //DB연결
            dbAcessor = new SogNo_DBAccessor(select_DBInfo.DBIP, select_DBInfo.DBPort, select_DBInfo.DBName);

            //전체 주차장 명칭을 콤보박스에 삽입
            this.parkInfo = dbAcessor.SelectParkInfo();
            this.ParkListComboBox.DataSource = this.parkInfo;
            this.ParkListComboBox.DisplayMember = ParkInfoFields.ParkName.ToString();
            this.ParkListComboBox.ValueMember = ParkInfoFields.ParkNo.ToString();

            //트리뷰 이미지 설정
            ImageList GateImageList = new ImageList();
            GateImageList.Images.Add(Properties.Resources.gate);
            GateImageList.Images.Add(Properties.Resources.cam);
            GateImageList.Images.Add(Properties.Resources.bar);
            GateImageList.Images.Add(Properties.Resources.scr);
            GateImageList.Images.Add(Properties.Resources.pc);
            GateTreeView.ImageList = GateImageList;
            if (ListViewSelectedParkNo != null)
            {
                thisparkinfo = dbAcessor.SelectParkInfo(Int32.Parse(ListViewSelectedParkNo))[0];
                this.ParkListComboBox.SelectedValue = thisparkinfo.ParkNo;
            }
            else
            {
                this.ParkListComboBox.SelectedIndex = -1;
            }

            UpdateOrImportBt.Hide();
            DeleteOrCancelBt.Hide();

        }

        // 트리 뷰에 데이터 뿌리기
        private void SpreadDataInTreeView()
        {
            if (ParkListComboBox.SelectedIndex == -1)
            {
                return;
            }

            GateTreeView.Nodes.Clear();
            dbAcessor = new SogNo_DBAccessor(select_DBInfo.DBIP, select_DBInfo.DBPort, select_DBInfo.DBName);
            gateInfo = dbAcessor.SelectGateInfo(Int32.Parse(this.selectedParkID));

            for (int i = 0; i < gateInfo.Length; i++)
            {
                TreeNode GateNodes = GateTreeView.Nodes.Add(gateInfo[i].GateNo.ToString(), gateInfo[i].GateName, 0, 0);
                TreeNode CamNodes = GateNodes.Nodes.Add("CAM", "카메라", 1, 1);
                TreeNode BarNodes = GateNodes.Nodes.Add("BAR", "차단기", 2, 2);
                TreeNode SCRNodes = GateNodes.Nodes.Add("SCR", "전광판", 3, 3);
                TreeNode PCNodes = GateNodes.Nodes.Add("PC", "제어PC", 4, 4);

                // 데이터 조회
                camInfo = dbAcessor.SelectCamInfo(Int32.Parse(this.selectedParkID), gateInfo[i].GateNo); // 카메라
                barInfo = dbAcessor.SelectBarInfo(Int32.Parse(this.selectedParkID), gateInfo[i].GateNo); // 차단기
                scrInfo = dbAcessor.SelectScreenInfo(Int32.Parse(this.selectedParkID), gateInfo[i].GateNo); // 전광판
                pcInfo = dbAcessor.SelectPCInfo(Int32.Parse(this.selectedParkID), gateInfo[i].GateNo); // pc

                //카메라
                for (int cam_i = 0; cam_i < camInfo.Length; cam_i++)
                {
                    CamNodes.Nodes.Add(camInfo[cam_i].CamNo.ToString(), camInfo[cam_i].CamName, 1, 1);
                }
                //차단기
                for (int bar_i = 0; bar_i < barInfo.Length; bar_i++)
                {
                    BarNodes.Nodes.Add(barInfo[bar_i].BarNo.ToString(), barInfo[bar_i].BarName, 2, 2);
                }
                //전광판
                for (int scr_i = 0; scr_i < scrInfo.Length; scr_i++)
                {
                    SCRNodes.Nodes.Add(scrInfo[scr_i].SCRNo.ToString(), scrInfo[scr_i].SCRName, 3, 3);
                }
                // 제어피씨
                for (int pc_i = 0; pc_i < pcInfo.Length; pc_i++)
                {
                    PCNodes.Nodes.Add(pcInfo[pc_i].PCNo.ToString(), pcInfo[pc_i].Name, 4, 4);
                }
            }
            // 모든 트리 노드를 보여준다
            GateTreeView.ExpandAll();
        }

        // 트리 뷰에서 하나 골랐을 때
        private void GateListEleSelect(object sender, TreeViewEventArgs e)
        {
            int selectedParkNo = Int32.Parse(selectedParkID);

            switch (GateTreeView.SelectedNode.Level)
            {
                case 0: // 대분류 (게이트)
                    UpdateOrDeleteMode();

                    selectedGateNo = Int32.Parse(GateTreeView.SelectedNode.Name);
                    selectedGateInfo = dbAcessor.SelectGateInfo(selectedParkNo, selectedGateNo)[0];
                    DisplayGateForm(selectedGateInfo);
                    selectedDataType = "GATE";
                    break;

                case 1: // 중분류 (카메라, 전광판, 차단기, PC로 분류)
                    ImportMode();
                    selectedGateNo = Int32.Parse(GateTreeView.SelectedNode.Parent.Name);
                    switch (GateTreeView.SelectedNode.Name)
                    {
                        case "CAM":
                            DisplayCameraForm();
                            selectedDataType = "CAM";
                            break;

                        case "BAR":
                            DisplayBarForm();
                            selectedDataType = "BAR";
                            break;

                        case "SCR":
                            DisplaySCRForm();
                            selectedDataType = "SCR";
                            break;

                        case "PC":
                            DisplayPCForm();
                            selectedDataType = "PC";
                            break;

                        default:
                            HideForm();
                            break;
                    }
                    break;

                case 2: // 소분류 (중분류(카메라,전광판 등)의 각 항목)
                    HideForm();
                    UpdateOrDeleteMode();
                    selectedGateNo = Int32.Parse(GateTreeView.SelectedNode.Parent.Parent.Name);
                    selectedNodeNo = Int32.Parse(GateTreeView.SelectedNode.Name);

                    switch (GateTreeView.SelectedNode.Parent.Name)
                    {
                        case "CAM":
                            selectedCamInfo = dbAcessor.SelectCamInfo(selectedParkNo, selectedGateNo, selectedNodeNo)[0];
                            DisplayCameraForm(selectedCamInfo);
                            selectedDataType = "CAM";
                            break;

                        case "BAR":
                            selectedBarInfo = dbAcessor.SelectBarInfo(selectedParkNo, selectedGateNo, selectedNodeNo)[0];
                            DisplayBarForm(selectedBarInfo);
                            selectedDataType = "BAR";
                            break;

                        case "SCR":
                            selectedScrInfo = dbAcessor.SelectScreenInfo(selectedParkNo, selectedGateNo, selectedNodeNo)[0];
                            DisplaySCRForm(selectedScrInfo);
                            selectedDataType = "SCR";
                            break;

                        case "PC":
                            selectedPcInfo = dbAcessor.SelectPCInfo(selectedParkNo, selectedGateNo, selectedNodeNo)[0];
                            DisplayPCForm(selectedPcInfo);
                            selectedDataType = "PC";
                            break;

                        default:
                            HideForm();
                            break;
                    }
                    break;
            }
        }

        // 폼 보이게 하기
        private void DisplayGateForm(GateInfoRecord gateInfo = null)
        {
            HideForm();

            depth3Label0.Visible = true;
            depth3Label1.Visible = true;
            depth3Label2.Visible = true;
            depth3Label3.Visible = true;
            depth3Label4.Visible = true;

            textBox0.Visible = true;
            textBox1.Visible = true;
            textBox2.Visible = true;
            textBox3.Visible = true;
            textBox4.Visible = true;

            depth3Label0.Text = "▶ 게이트명";
            depth3Label1.Text = "▷ 게이트 IP";
            depth3Label2.Text = "▷ 게이트 Port";
            depth3Label3.Text = "▷ LPR보드 IP";
            depth3Label4.Text = "▷ LPR보드 Port";

            if (gateInfo != null)
            {
                textBox0.Text = gateInfo.GateName;
                textBox1.Text = gateInfo.GateIP;
                textBox2.Text = gateInfo.GatePort.ToString();
                textBox3.Text = gateInfo.LPRBoardIP;
                textBox4.Text = gateInfo.LPRBoardPort.ToString();
            }
        }
        private void DisplayCameraForm(CamInfoRecord camInfo = null)
        {
            HideForm();

            depth3Label0.Visible = true;
            depth3Label1.Visible = true;
            depth3Label2.Visible = true;
            depth3Label3.Visible = true;
            depth3Label4.Visible = true;
            depth3Label5.Visible = true;
            depth3Label6.Visible = true;
            depth3Label7.Visible = true;
            depth3Label8.Visible = true;
            depth3Label9.Visible = true;
            depth3Label10.Visible = true;
            depth3Label11.Visible = true;
            depth3Label12.Visible = true;
            depth3Label13.Visible = true;
            depth3Label14.Visible = true;
            depth3Label15.Visible = true;
            depth3Label16.Visible = true;

            textBox0.Visible = true;
            textBox1.Visible = true;
            textBox2.Visible = true;
            textBox3.Visible = true;
            textBox4.Visible = true;
            textBox5.Visible = true;
            textBox6.Visible = true;
            textBox7.Visible = true;
            textBox8.Visible = true;
            textBox9.Visible = true;
            textBox10.Visible = true;
            textBox11.Visible = true;
            textBox12.Visible = true;
            comboBox1.Visible = true;
            comboBox2.Visible = true;
            comboBox3.Visible = true;
            textBox13.Visible = true;

            depth3Label0.Text = "▶ 카메라명";
            depth3Label1.Text = "▷ 밝기기준";
            depth3Label2.Text = "▷ 밝기최대";
            depth3Label3.Text = "▷ 밝기최소";
            depth3Label4.Text = "▷ 밝기조절 캡처";
            depth3Label5.Text = "▷ 이미지 캡처 개수";
            depth3Label6.Text = "▷ 카메라 프레임";
            depth3Label7.Text = "▷ 카메라 아이피";
            depth3Label8.Text = "▷ 입출차";
            depth3Label9.Text = "▷ 전후방";
            depth3Label10.Text = "▷ 카메라 동작 명령";
            depth3Label11.Text = "▷ PreBarOpen";
            depth3Label12.Text = "▷ 연동 차단기 번호";
            depth3Label13.Text = "▷ 연동 전광판 번호";
            depth3Label14.Text = "▷ 연동 4면 카메라 번호";
            depth3Label15.Text = "▷ 번호인식영역";
            depth3Label16.Text = "▷ 번호인식설정";

            if (camInfo != null)
            {
                textBox0.Text = camInfo.CamName;
                textBox1.Text = camInfo.Target.ToString();
                textBox2.Text = camInfo.EMax.ToString();
                textBox3.Text = camInfo.EMin.ToString();
                textBox4.Text = camInfo.AutoCCount.ToString();
                textBox5.Text = camInfo.CapCount.ToString();
                textBox6.Text = camInfo.FrameRate;
                textBox7.Text = camInfo.CamIP;
                textBox8.Text = camInfo.CamIO.ToString();
                textBox9.Text = camInfo.CamOrder.ToString();
                textBox10.Text = camInfo.Cmd;
                textBox11.Text = camInfo.PreBarOpen.ToString();

                // 해당 게이트 내 차단기 다 조회
                barInfo = dbAcessor.SelectBarInfo(Int32.Parse(this.selectedParkID), selectedGateNo);

                comboBox1.DataSource = this.barInfo;
                comboBox1.DisplayMember = BarInfoFields.BarName.ToString();
                comboBox1.ValueMember = BarInfoFields.BarNo.ToString();
                if (camInfo.BarNo != null) comboBox1.SelectedValue = camInfo.BarNo;

                // 해당 게이트 내 전광판 다 조회
                scrInfo = dbAcessor.SelectScreenInfo(Int32.Parse(this.selectedParkID), selectedGateNo);

                comboBox2.DataSource = this.scrInfo;
                comboBox2.DisplayMember = ScrInfoFields.SCRName.ToString();
                comboBox2.ValueMember = ScrInfoFields.SCRNo.ToString();
                if (camInfo.ScrNo != null) comboBox2.SelectedValue = camInfo.ScrNo;

                // 해당 게이트 내 4면카메라 다 조회(4면카메라 데이터베이스 확인불가하여 일단 냅둠)
                //comboBox3.Text = camInfo.FourCamNo.ToString();

                textBox12.Text = camInfo.RecnArea;
                textBox13.Text = camInfo.RecnSet;
            }
        }
        private void DisplayBarForm(BarInfoRecord barInfo = null)
        {
            HideForm();

            depth3Label0.Visible = true;
            depth3Label1.Visible = true;
            depth3Label2.Visible = true;
            depth3Label3.Visible = true;
            depth3Label4.Visible = true;
            depth3Label5.Visible = true;
            depth3Label6.Visible = true;
            depth3Label7.Visible = true;

            textBox0.Visible = true;
            textBox1.Visible = true;
            textBox2.Visible = true;
            textBox3.Visible = true;
            textBox4.Visible = true;
            textBox5.Visible = true;
            textBox6.Visible = true;
            textBox7.Visible = true;

            depth3Label0.Text = "▶ 차단기명";
            depth3Label1.Text = "▷ 차단기아이피";
            depth3Label2.Text = "▷ 차단기포트";
            depth3Label3.Text = "▷ 자동닫힘시간";
            depth3Label4.Text = "▷ 차단기 연결형태";
            depth3Label5.Text = "▷ 접점시 차단기 열기 명령";
            depth3Label6.Text = "▷ 접점시 차단기 닫기 명령";
            depth3Label7.Text = "▷ BarOneWay";

            if (barInfo != null)
            {
                textBox0.Text = barInfo.BarName;
                textBox1.Text = barInfo.BarIP;
                textBox2.Text = barInfo.BarPort.ToString();
                textBox3.Text = barInfo.BarACTime.ToString();
                textBox4.Text = barInfo.BarConnType.ToString();
                textBox5.Text = barInfo.BarOpenCmd;
                textBox6.Text = barInfo.BarCloseCmd;
                textBox7.Text = barInfo.BarOneWay.ToString();
            }
        }
        private void DisplaySCRForm(ScrInfoRecord scrInfo = null)
        {
            HideForm();

            depth3Label0.Visible = true;
            depth3Label1.Visible = true;
            depth3Label2.Visible = true;
            depth3Label3.Visible = true;
            depth3Label4.Visible = true;
            depth3Label5.Visible = true;
            depth3Label6.Visible = true;
            depth3Label7.Visible = true;

            depth3Label0.Text = "▶ 전광판이름";
            depth3Label1.Text = "▷ 전광판아이피";
            depth3Label2.Text = "▷ 전광판포트";
            depth3Label3.Text = "▷ SCRTime";
            depth3Label4.Text = "▷ 상단문구";
            depth3Label5.Text = "▷ 상단색상";
            depth3Label6.Text = "▷ 하단문구";
            depth3Label7.Text = "▷ 하단색상";

            textBox0.Visible = true;
            textBox1.Visible = true;
            textBox2.Visible = true;
            textBox3.Visible = true;
            textBox4.Visible = true;
            textBox5.Visible = true;
            textBox6.Visible = true;
            textBox7.Visible = true;

            if (scrInfo != null)
            {
                textBox0.Text = scrInfo.SCRName;
                textBox1.Text = scrInfo.SCRIP;
                textBox2.Text = scrInfo.SCRPort.ToString();
                textBox3.Text = scrInfo.SCRTime.ToString();
                textBox4.Text = scrInfo.UpText;
                textBox5.Text = scrInfo.UpColor.ToString();
                textBox6.Text = scrInfo.DownText;
                textBox7.Text = scrInfo.DownColor.ToString();
            }
        }
        private void DisplayPCForm(PCInfoRecord pcInfo = null)
        {
            HideForm();

            depth3Label0.Visible = true;
            depth3Label1.Visible = true;
            depth3Label2.Visible = true;
            depth3Label3.Visible = true;

            depth3Label0.Text = "▶ PC이름";
            depth3Label1.Text = "▷ PC IP";
            depth3Label2.Text = "▷ PC 인터넷 IP";
            depth3Label3.Text = "▷ PC 기능정보";

            textBox0.Visible = true;
            textBox1.Visible = true;
            textBox2.Visible = true;
            textBox3.Visible = true;

            if (pcInfo != null)
            {
                textBox0.Text = pcInfo.Name;
                textBox1.Text = pcInfo.IP;
                textBox3.Text = pcInfo.InternetIP;
                textBox2.Text = pcInfo.Memo;
            }
        }

        // 폼 내 텍스트 UI 제어
        private void HideForm()// 폼 안보이게 하기
        {
            ResetForm();

            depth3Label0.Visible = false;
            depth3Label1.Visible = false;
            depth3Label2.Visible = false;
            depth3Label3.Visible = false;
            depth3Label4.Visible = false;
            depth3Label5.Visible = false;
            depth3Label6.Visible = false;
            depth3Label7.Visible = false;
            depth3Label8.Visible = false;
            depth3Label9.Visible = false;
            depth3Label10.Visible = false;
            depth3Label11.Visible = false;
            depth3Label12.Visible = false;
            depth3Label13.Visible = false;
            depth3Label14.Visible = false;
            depth3Label15.Visible = false;
            depth3Label16.Visible = false;
            depth3Label17.Visible = false;

            textBox0.Visible = false;
            textBox1.Visible = false;
            textBox2.Visible = false;
            textBox3.Visible = false;
            textBox4.Visible = false;
            textBox5.Visible = false;
            textBox6.Visible = false;
            textBox7.Visible = false;
            textBox8.Visible = false;
            textBox9.Visible = false;
            textBox10.Visible = false;
            textBox11.Visible = false;
            textBox12.Visible = false;
            comboBox1.Visible = false;
            comboBox2.Visible = false;
            comboBox3.Visible = false;
            textBox13.Visible = false;
            textBox17.Visible = false;
        }
        private void ResetForm()// 폼 클리어
        {
            textBox0.Text = "";
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
            textBox8.Text = "";
            textBox9.Text = "";
            textBox10.Text = "";
            textBox11.Text = "";
            textBox12.Text = "";
            comboBox1.SelectedIndex = -1;
            comboBox2.SelectedIndex = -1;
            comboBox3.SelectedIndex = -1;
            textBox13.Text = "";
        }

        //상단 콤보박스에서 주차장 골랐을 때 이벤트
        private void SelectParkIndex(object sender, EventArgs e)
        {
            if (ParkListComboBox.SelectedIndex == -1)
            {
                return;
            }

            selectedParkID = ParkListComboBox.SelectedValue.ToString();
            SpreadDataInTreeView();
        }

        // 버튼 클릭 관련
        private void AddNewGateBt_Click(object sender, EventArgs e)
        {
            DisplayGateForm();
            ImportMode();
        }
        private void UpdateOrImportBt_Click(object sender, EventArgs e)
        {
            switch (updateState)
            {
                case "IMPORT":
                    switch (selectedDataType)
                    {
                        case "GATE":
                            ImportGateDataset();
                            break;
                        case "CAM":
                            ImportCamDataset();
                            break;
                        case "BAR":
                            ImportBarDataset();
                            break;
                        case "SCR":
                            ImportScrDataset();
                            break;
                        case "PC":
                            ImportPcDataset();
                            break;
                    }
                    SpreadDataInTreeView();
                    break;

                case "UPDATE":
                    UpdatingMode();
                    break;

                case "UPDATING":
                    switch (selectedDataType)
                    {
                        case "GATE":
                            UpdateGateDataset();
                            break;
                        case "CAM":
                            UpdateCamDataset();
                            break;
                        case "BAR":
                            UpdateBarDataset();
                            break;
                        case "SCR":
                            UpdateScrDataset();
                            break;
                        case "PC":
                            UpdatePcDataset();
                            break;
                    }
                    SpreadDataInTreeView();
                    UpdateOrDeleteMode();
                    break;
            }
        }
        private void DeleteOrCancelBt_Click(object sender, EventArgs e)
        {
            switch (updateState)
            {
                case "IMPORT":
                    HideForm();
                    UpdateOrImportBt.Hide();
                    DeleteOrCancelBt.Hide();
                    break;

                case "UPDATE":
                    switch (selectedDataType)
                    {
                        case "GATE":
                            DeleteGateDataset();
                            break;
                        case "CAM":
                            DeleteCamDataset();
                            break;
                        case "BAR":
                            DeleteBarDataset();
                            break;
                        case "SCR":
                            DeleteScrDataset();
                            break;
                        case "PC":
                            DeletePcDataset();
                            break;
                    }
                    break;

                case "UPDATING":
                    UpdateOrDeleteMode();
                    SpreadDataInTreeView();
                    HideForm();
                    break;
            }
            SpreadDataInTreeView();

            UpdateOrImportBt.Hide();
            DeleteOrCancelBt.Hide();
        }

        // DB에 데이터 입력하는 기능
        private void ImportGateDataset()
        {
            // 메인에서 입력받기
            this.importGateInfo.ParkNo = Int32.Parse(selectedParkID);

            if (textBox0.Text != "") this.importGateInfo.GateName = textBox0.Text;
            if (textBox1.Text != "") this.importGateInfo.GateIP = textBox1.Text;
            if (textBox2.Text != "") this.importGateInfo.GatePort = Int32.Parse(textBox2.Text.ToString());
            if (textBox3.Text != "") this.importGateInfo.LPRBoardIP = textBox3.Text;
            if (textBox4.Text != "") this.importGateInfo.LPRBoardPort = Int32.Parse(textBox4.Text.ToString());

            // 인서트 진행
            this.dbAcessor.InsertGateInfo(importGateInfo);
        }
        private void ImportCamDataset()
        {
            this.importCamInfo.ParkNo = Int32.Parse(selectedParkID);
            this.importCamInfo.GateNo = selectedGateNo;

            if (textBox0.Text != "") this.importCamInfo.CamName = textBox0.Text;
            if (textBox1.Text != "") this.importCamInfo.Target = Int32.Parse(textBox1.Text);
            if (textBox2.Text != "") this.importCamInfo.EMax = Int32.Parse(textBox2.Text);
            if (textBox3.Text != "") this.importCamInfo.EMin = Int32.Parse(textBox3.Text);
            if (textBox4.Text != "") this.importCamInfo.AutoCCount = Int32.Parse(textBox4.Text);
            if (textBox5.Text != "") this.importCamInfo.CapCount = Int32.Parse(textBox5.Text);
            if (textBox6.Text != "") this.importCamInfo.FrameRate = textBox6.Text;
            if (textBox7.Text != "") this.importCamInfo.CamIP = textBox7.Text;
            if (textBox8.Text != "") this.importCamInfo.CamIO = Int32.Parse(textBox8.Text);
            if (textBox9.Text != "") this.importCamInfo.CamOrder = Int32.Parse(textBox9.Text);
            if (textBox10.Text != "") this.importCamInfo.Cmd = textBox10.Text;
            if (textBox11.Text != "") this.importCamInfo.PreBarOpen = Int32.Parse(textBox11.Text);
            if (comboBox1.SelectedIndex != -1)
                this.importCamInfo.BarNo = Int32.Parse(comboBox1.SelectedValue.ToString());
            if (comboBox2.SelectedIndex != -1)
                this.importCamInfo.ScrNo = Int32.Parse(comboBox2.SelectedValue.ToString());
            if (comboBox3.SelectedIndex != -1)
                this.importCamInfo.FourCamNo = Int32.Parse(comboBox3.SelectedValue.ToString());
            if (textBox12.Text != "") this.importCamInfo.RecnArea = textBox12.Text;
            if (textBox13.Text != "") this.importCamInfo.RecnSet = textBox13.Text;

            // 인서트 진행
            this.dbAcessor.InsertCamInfo(importCamInfo);
        }
        private void ImportBarDataset()
        {
            this.importBarInfo.ParkNo = Int32.Parse(selectedParkID);
            this.importBarInfo.GateNo = selectedGateNo;

            if (textBox0.Text != "") this.importBarInfo.BarName = textBox0.Text;
            if (textBox1.Text != "") this.importBarInfo.BarIP = textBox1.Text;
            if (textBox2.Text != "") this.importBarInfo.BarPort = Int32.Parse(textBox2.Text);
            if (textBox3.Text != "") this.importBarInfo.BarACTime = Int32.Parse(textBox3.Text);
            if (textBox4.Text != "") this.importBarInfo.BarConnType = Int32.Parse(textBox4.Text);
            if (textBox5.Text != "") this.importBarInfo.BarOpenCmd = textBox5.Text;
            if (textBox6.Text != "") this.importBarInfo.BarCloseCmd = textBox6.Text;
            if (textBox7.Text != "") this.importBarInfo.BarOneWay = Int32.Parse(textBox7.Text);

            this.dbAcessor.InsertBarInfo(importBarInfo);
        }
        private void ImportScrDataset()
        {
            this.importScrInfo.ParkNo = Int32.Parse(selectedParkID);
            this.importScrInfo.GateNo = selectedGateNo;

            if (textBox0.Text != "") this.importScrInfo.SCRName = textBox0.Text;
            if (textBox1.Text != "") this.importScrInfo.SCRIP = textBox1.Text;
            if (textBox2.Text != "") this.importScrInfo.SCRPort = Int32.Parse(textBox2.Text);
            if (textBox3.Text != "") this.importScrInfo.SCRTime = Int32.Parse(textBox3.Text);
            if (textBox4.Text != "") this.importScrInfo.UpText = textBox4.Text;
            if (textBox5.Text != "") this.importScrInfo.UpColor = Int32.Parse(textBox5.Text);
            if (textBox6.Text != "") this.importScrInfo.DownText = textBox6.Text;
            if (textBox7.Text != "") this.importScrInfo.DownColor = Int32.Parse(textBox7.Text);

            this.dbAcessor.InsertScrInfo(importScrInfo);
        }
        private void ImportPcDataset()
        {
            this.importPcInfo.ParkNo = Int32.Parse(selectedParkID);
            this.importPcInfo.GateNo = selectedGateNo;

            if (textBox0.Text != "") this.importPcInfo.Name = textBox0.Text;
            if (textBox1.Text != "") this.importPcInfo.IP = textBox1.Text;
            if (textBox2.Text != "") this.importPcInfo.InternetIP = textBox2.Text;
            if (textBox3.Text != "") this.importPcInfo.Memo = textBox3.Text;

            this.dbAcessor.InsertPcInfo(importPcInfo);
        }

        // DB 데이터 업데이트하는 기능
        private void UpdateGateDataset()
        {
            //메인에서 검색된거에 지금 써있는걸 덮어씌움
            this.selectedGateInfo.GateName = textBox0.Text;
            this.selectedGateInfo.GateIP = textBox1.Text;
            this.selectedGateInfo.GatePort = Int32.Parse(textBox2.Text.ToString());
            this.selectedGateInfo.LPRBoardIP = textBox3.Text;
            this.selectedGateInfo.LPRBoardPort = Int32.Parse(textBox4.Text.ToString());

            // 업데이트 진행
            this.dbAcessor.UpdateGateInfo(selectedGateInfo);
        }
        private void UpdateCamDataset()
        {
            this.selectedCamInfo.CamName = textBox0.Text;
            this.selectedCamInfo.Target = Int32.Parse(textBox1.Text);
            this.selectedCamInfo.EMax = Int32.Parse(textBox2.Text);
            this.selectedCamInfo.EMin = Int32.Parse(textBox3.Text);
            this.selectedCamInfo.AutoCCount = Int32.Parse(textBox4.Text);
            this.selectedCamInfo.CapCount = Int32.Parse(textBox5.Text);
            this.selectedCamInfo.FrameRate = textBox6.Text;
            this.selectedCamInfo.CamIP = textBox7.Text;
            this.selectedCamInfo.CamIO = Int32.Parse(textBox8.Text);
            this.selectedCamInfo.CamOrder = Int32.Parse(textBox9.Text);
            this.selectedCamInfo.Cmd = textBox10.Text;
            this.selectedCamInfo.PreBarOpen = Int32.Parse(textBox11.Text);
            if (comboBox1.SelectedIndex != -1)
                this.selectedCamInfo.BarNo = Int32.Parse(comboBox1.SelectedValue.ToString());
            if (comboBox2.SelectedIndex != -1)
                this.selectedCamInfo.ScrNo = Int32.Parse(comboBox2.SelectedValue.ToString());
            if (comboBox3.SelectedIndex != -1)
                this.selectedCamInfo.FourCamNo = Int32.Parse(comboBox3.SelectedValue.ToString());
            this.selectedCamInfo.RecnArea = textBox12.Text;
            this.selectedCamInfo.RecnSet = textBox13.Text;

            this.dbAcessor.UpdateCamInfo(selectedCamInfo);
        }
        private void UpdateBarDataset()
        {
            this.selectedBarInfo.ParkNo = Int32.Parse(selectedParkID);
            this.selectedBarInfo.GateNo = selectedGateNo;

            this.selectedBarInfo.BarName = textBox0.Text;
            this.selectedBarInfo.BarIP = textBox1.Text;
            this.selectedBarInfo.BarPort = Int32.Parse(textBox2.Text);
            this.selectedBarInfo.BarACTime = Int32.Parse(textBox3.Text);
            this.selectedBarInfo.BarConnType = Int32.Parse(textBox4.Text);
            this.selectedBarInfo.BarOpenCmd = textBox5.Text;
            this.selectedBarInfo.BarCloseCmd = textBox6.Text;
            this.selectedBarInfo.BarOneWay = Int32.Parse(textBox7.Text);

            this.dbAcessor.UpdateBarInfo(selectedBarInfo);
        }
        private void UpdateScrDataset()
        {
            this.selectedScrInfo.ParkNo = Int32.Parse(selectedParkID);
            this.selectedScrInfo.GateNo = selectedGateNo;

            this.selectedScrInfo.SCRName = textBox0.Text;
            this.selectedScrInfo.SCRIP = textBox1.Text;
            this.selectedScrInfo.SCRPort = Int32.Parse(textBox2.Text);
            this.selectedScrInfo.SCRTime = Int32.Parse(textBox3.Text);
            this.selectedScrInfo.UpText = textBox4.Text;
            this.selectedScrInfo.UpColor = Int32.Parse(textBox5.Text);
            this.selectedScrInfo.DownText = textBox6.Text;
            this.selectedScrInfo.DownColor = Int32.Parse(textBox7.Text);

            this.dbAcessor.UpdateScrInfo(selectedScrInfo);
        }
        private void UpdatePcDataset()
        {
            this.selectedPcInfo.ParkNo = Int32.Parse(selectedParkID);
            this.selectedPcInfo.GateNo = selectedGateNo;

            this.selectedPcInfo.Name = textBox0.Text;
            this.selectedPcInfo.IP = textBox1.Text;
            this.selectedPcInfo.InternetIP = textBox2.Text;
            this.selectedPcInfo.Memo = textBox3.Text;

            this.dbAcessor.UpdatePcInfo(selectedPcInfo);
        }

        // DB 데이터 삭제하는 기능
        private void DeleteGateDataset()
        {
            DialogResult dialog = MessageBox.Show(selectedGateInfo.GateName + " 을/를 정말 삭제하시겠습니까?", "삭제확인메시지", MessageBoxButtons.YesNo);
            if (dialog == DialogResult.Yes)
            {
                this.dbAcessor.DeleteGateInfo(selectedGateInfo);
            }
        }
        private void DeleteCamDataset()
        {
            DialogResult dialog = MessageBox.Show(selectedCamInfo.CamName + " 을/를 정말 삭제하시겠습니까?", "삭제확인메시지", MessageBoxButtons.YesNo);
            if (dialog == DialogResult.Yes)
            {
                this.dbAcessor.DeleteCamInfo(selectedCamInfo);
            }
        }
        private void DeleteBarDataset()
        {
            DialogResult dialog = MessageBox.Show(selectedBarInfo.BarName + " 을/를 정말 삭제하시겠습니까?", "삭제확인메시지", MessageBoxButtons.YesNo);
            if (dialog == DialogResult.Yes)
            {
                this.dbAcessor.DeleteBarInfo(selectedBarInfo);
            }
        }
        private void DeleteScrDataset()
        {
            DialogResult dialog = MessageBox.Show(selectedScrInfo.SCRName + " 을/를 정말 삭제하시겠습니까?", "삭제확인메시지", MessageBoxButtons.YesNo);
            if (dialog == DialogResult.Yes)
            {
                this.dbAcessor.DeleteScrInfo(selectedScrInfo);
            }
        }
        private void DeletePcDataset()
        {
            DialogResult dialog = MessageBox.Show(selectedPcInfo.Name + " 을/를 정말 삭제하시겠습니까?", "삭제확인메시지", MessageBoxButtons.YesNo);
            if (dialog == DialogResult.Yes)
            {
                this.dbAcessor.DeletePcInfo(selectedPcInfo);
            }
        }


        // 버튼 컬러 및 텍스트 제어
        private void ImportMode()
        {
            UpdateOrImportBt.Show();
            DeleteOrCancelBt.Show();

            UpdateOrImportBt.Text = "추가";
            UpdateOrImportBt.BackColor = Color.FromArgb(17, 41, 123);

            DeleteOrCancelBt.Text = "취소";
            DeleteOrCancelBt.BackColor = Color.FromArgb(50, 50, 50);

            updateState = "IMPORT";
        }
        private void UpdateOrDeleteMode()
        {
            UpdateOrImportBt.Show();
            DeleteOrCancelBt.Show();

            UpdateOrImportBt.Text = "수정";
            UpdateOrImportBt.BackColor = Color.OrangeRed;

            DeleteOrCancelBt.Text = "삭제";
            DeleteOrCancelBt.BackColor = Color.FromArgb(192, 0, 0);

            updateState = "UPDATE";
        }
        private void UpdatingMode()
        {
            UpdateOrImportBt.Show();
            DeleteOrCancelBt.Show();
            UpdateOrImportBt.Text = "수정완료";
            UpdateOrImportBt.BackColor = Color.LightSeaGreen;

            DeleteOrCancelBt.Text = "취소";
            DeleteOrCancelBt.BackColor = Color.FromArgb(50, 50, 50);
            updateState = "UPDATING";
        }
    }
}
