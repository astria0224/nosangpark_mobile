﻿namespace SogNoLib
{
	partial class SogNoLib_GateDBViewer
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.depth3Label1 = new System.Windows.Forms.Label();
            this.depth3Label2 = new System.Windows.Forms.Label();
            this.depth3Label3 = new System.Windows.Forms.Label();
            this.depth3Label4 = new System.Windows.Forms.Label();
            this.depth3Label5 = new System.Windows.Forms.Label();
            this.depth3Label6 = new System.Windows.Forms.Label();
            this.depth3Label7 = new System.Windows.Forms.Label();
            this.depth3Label8 = new System.Windows.Forms.Label();
            this.depth3Label9 = new System.Windows.Forms.Label();
            this.depth3Label10 = new System.Windows.Forms.Label();
            this.depth3Label11 = new System.Windows.Forms.Label();
            this.depth3Label12 = new System.Windows.Forms.Label();
            this.depth3Label13 = new System.Windows.Forms.Label();
            this.depth3Label14 = new System.Windows.Forms.Label();
            this.depth3Label15 = new System.Windows.Forms.Label();
            this.depth3Label16 = new System.Windows.Forms.Label();
            this.depth3Label17 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.depth3Label0 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox0 = new System.Windows.Forms.TextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.GateTreeView = new System.Windows.Forms.TreeView();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.AddNewGateBt = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.UpdateOrImportBt = new System.Windows.Forms.Button();
            this.DeleteOrCancelBt = new System.Windows.Forms.Button();
            this.ParkListComboBox = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel5, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.ParkListComboBox, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.83744F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 89.16256F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 63F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(917, 673);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 544F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 69);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(911, 537);
            this.tableLayoutPanel2.TabIndex = 50;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.603774F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 93.39622F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 257F));
            this.tableLayoutPanel4.Controls.Add(this.depth3Label1, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.depth3Label2, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.depth3Label3, 1, 3);
            this.tableLayoutPanel4.Controls.Add(this.depth3Label4, 1, 4);
            this.tableLayoutPanel4.Controls.Add(this.depth3Label5, 1, 5);
            this.tableLayoutPanel4.Controls.Add(this.depth3Label6, 1, 6);
            this.tableLayoutPanel4.Controls.Add(this.depth3Label7, 1, 7);
            this.tableLayoutPanel4.Controls.Add(this.depth3Label8, 1, 8);
            this.tableLayoutPanel4.Controls.Add(this.depth3Label9, 1, 9);
            this.tableLayoutPanel4.Controls.Add(this.depth3Label10, 1, 10);
            this.tableLayoutPanel4.Controls.Add(this.depth3Label11, 1, 11);
            this.tableLayoutPanel4.Controls.Add(this.depth3Label12, 1, 12);
            this.tableLayoutPanel4.Controls.Add(this.depth3Label13, 1, 13);
            this.tableLayoutPanel4.Controls.Add(this.depth3Label14, 1, 14);
            this.tableLayoutPanel4.Controls.Add(this.depth3Label15, 1, 15);
            this.tableLayoutPanel4.Controls.Add(this.depth3Label16, 1, 16);
            this.tableLayoutPanel4.Controls.Add(this.depth3Label17, 1, 17);
            this.tableLayoutPanel4.Controls.Add(this.textBox1, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.textBox2, 2, 2);
            this.tableLayoutPanel4.Controls.Add(this.textBox3, 2, 3);
            this.tableLayoutPanel4.Controls.Add(this.textBox4, 2, 4);
            this.tableLayoutPanel4.Controls.Add(this.textBox7, 2, 7);
            this.tableLayoutPanel4.Controls.Add(this.textBox8, 2, 8);
            this.tableLayoutPanel4.Controls.Add(this.textBox9, 2, 9);
            this.tableLayoutPanel4.Controls.Add(this.textBox10, 2, 10);
            this.tableLayoutPanel4.Controls.Add(this.textBox11, 2, 11);
            this.tableLayoutPanel4.Controls.Add(this.textBox13, 2, 16);
            this.tableLayoutPanel4.Controls.Add(this.textBox17, 2, 17);
            this.tableLayoutPanel4.Controls.Add(this.depth3Label0, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.textBox5, 2, 5);
            this.tableLayoutPanel4.Controls.Add(this.textBox6, 2, 6);
            this.tableLayoutPanel4.Controls.Add(this.textBox0, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.comboBox1, 2, 12);
            this.tableLayoutPanel4.Controls.Add(this.comboBox2, 2, 13);
            this.tableLayoutPanel4.Controls.Add(this.comboBox3, 2, 14);
            this.tableLayoutPanel4.Controls.Add(this.textBox12, 2, 15);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(370, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 18;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263158F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263158F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263158F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263158F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263158F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263158F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263158F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263158F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263158F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263158F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263158F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263158F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263158F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263158F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263158F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263158F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263158F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.263158F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(538, 531);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // depth3Label1
            // 
            this.depth3Label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.depth3Label1.AutoSize = true;
            this.depth3Label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.depth3Label1.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.depth3Label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.depth3Label1.Location = new System.Drawing.Point(21, 33);
            this.depth3Label1.Name = "depth3Label1";
            this.depth3Label1.Size = new System.Drawing.Size(32, 21);
            this.depth3Label1.TabIndex = 59;
            this.depth3Label1.Text = "▷ ";
            this.depth3Label1.Visible = false;
            // 
            // depth3Label2
            // 
            this.depth3Label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.depth3Label2.AutoSize = true;
            this.depth3Label2.Cursor = System.Windows.Forms.Cursors.Default;
            this.depth3Label2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.depth3Label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.depth3Label2.Location = new System.Drawing.Point(21, 62);
            this.depth3Label2.Name = "depth3Label2";
            this.depth3Label2.Size = new System.Drawing.Size(32, 21);
            this.depth3Label2.TabIndex = 60;
            this.depth3Label2.Text = "▷ ";
            this.depth3Label2.Visible = false;
            // 
            // depth3Label3
            // 
            this.depth3Label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.depth3Label3.AutoSize = true;
            this.depth3Label3.Cursor = System.Windows.Forms.Cursors.Default;
            this.depth3Label3.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.depth3Label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.depth3Label3.Location = new System.Drawing.Point(21, 91);
            this.depth3Label3.Name = "depth3Label3";
            this.depth3Label3.Size = new System.Drawing.Size(32, 21);
            this.depth3Label3.TabIndex = 61;
            this.depth3Label3.Text = "▷ ";
            this.depth3Label3.Visible = false;
            // 
            // depth3Label4
            // 
            this.depth3Label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.depth3Label4.AutoSize = true;
            this.depth3Label4.Cursor = System.Windows.Forms.Cursors.Default;
            this.depth3Label4.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.depth3Label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.depth3Label4.Location = new System.Drawing.Point(21, 120);
            this.depth3Label4.Name = "depth3Label4";
            this.depth3Label4.Size = new System.Drawing.Size(32, 21);
            this.depth3Label4.TabIndex = 62;
            this.depth3Label4.Text = "▷ ";
            this.depth3Label4.Visible = false;
            // 
            // depth3Label5
            // 
            this.depth3Label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.depth3Label5.AutoSize = true;
            this.depth3Label5.Cursor = System.Windows.Forms.Cursors.Default;
            this.depth3Label5.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.depth3Label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.depth3Label5.Location = new System.Drawing.Point(21, 149);
            this.depth3Label5.Name = "depth3Label5";
            this.depth3Label5.Size = new System.Drawing.Size(32, 21);
            this.depth3Label5.TabIndex = 63;
            this.depth3Label5.Text = "▷ ";
            this.depth3Label5.Visible = false;
            // 
            // depth3Label6
            // 
            this.depth3Label6.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.depth3Label6.AutoSize = true;
            this.depth3Label6.Cursor = System.Windows.Forms.Cursors.Default;
            this.depth3Label6.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.depth3Label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.depth3Label6.Location = new System.Drawing.Point(21, 178);
            this.depth3Label6.Name = "depth3Label6";
            this.depth3Label6.Size = new System.Drawing.Size(32, 21);
            this.depth3Label6.TabIndex = 64;
            this.depth3Label6.Text = "▷ ";
            this.depth3Label6.Visible = false;
            // 
            // depth3Label7
            // 
            this.depth3Label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.depth3Label7.AutoSize = true;
            this.depth3Label7.Cursor = System.Windows.Forms.Cursors.Default;
            this.depth3Label7.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.depth3Label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.depth3Label7.Location = new System.Drawing.Point(21, 207);
            this.depth3Label7.Name = "depth3Label7";
            this.depth3Label7.Size = new System.Drawing.Size(32, 21);
            this.depth3Label7.TabIndex = 65;
            this.depth3Label7.Text = "▷ ";
            this.depth3Label7.Visible = false;
            // 
            // depth3Label8
            // 
            this.depth3Label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.depth3Label8.AutoSize = true;
            this.depth3Label8.Cursor = System.Windows.Forms.Cursors.Default;
            this.depth3Label8.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.depth3Label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.depth3Label8.Location = new System.Drawing.Point(21, 236);
            this.depth3Label8.Name = "depth3Label8";
            this.depth3Label8.Size = new System.Drawing.Size(32, 21);
            this.depth3Label8.TabIndex = 66;
            this.depth3Label8.Text = "▷ ";
            this.depth3Label8.Visible = false;
            // 
            // depth3Label9
            // 
            this.depth3Label9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.depth3Label9.AutoSize = true;
            this.depth3Label9.Cursor = System.Windows.Forms.Cursors.Default;
            this.depth3Label9.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.depth3Label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.depth3Label9.Location = new System.Drawing.Point(21, 265);
            this.depth3Label9.Name = "depth3Label9";
            this.depth3Label9.Size = new System.Drawing.Size(26, 21);
            this.depth3Label9.TabIndex = 67;
            this.depth3Label9.Text = "▷";
            this.depth3Label9.Visible = false;
            // 
            // depth3Label10
            // 
            this.depth3Label10.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.depth3Label10.AutoSize = true;
            this.depth3Label10.Cursor = System.Windows.Forms.Cursors.Default;
            this.depth3Label10.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.depth3Label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.depth3Label10.Location = new System.Drawing.Point(21, 294);
            this.depth3Label10.Name = "depth3Label10";
            this.depth3Label10.Size = new System.Drawing.Size(32, 21);
            this.depth3Label10.TabIndex = 68;
            this.depth3Label10.Text = "▷ ";
            this.depth3Label10.Visible = false;
            // 
            // depth3Label11
            // 
            this.depth3Label11.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.depth3Label11.AutoSize = true;
            this.depth3Label11.Cursor = System.Windows.Forms.Cursors.Default;
            this.depth3Label11.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.depth3Label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.depth3Label11.Location = new System.Drawing.Point(21, 323);
            this.depth3Label11.Name = "depth3Label11";
            this.depth3Label11.Size = new System.Drawing.Size(32, 21);
            this.depth3Label11.TabIndex = 69;
            this.depth3Label11.Text = "▷ ";
            this.depth3Label11.Visible = false;
            // 
            // depth3Label12
            // 
            this.depth3Label12.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.depth3Label12.AutoSize = true;
            this.depth3Label12.Cursor = System.Windows.Forms.Cursors.Default;
            this.depth3Label12.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.depth3Label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.depth3Label12.Location = new System.Drawing.Point(21, 352);
            this.depth3Label12.Name = "depth3Label12";
            this.depth3Label12.Size = new System.Drawing.Size(32, 21);
            this.depth3Label12.TabIndex = 70;
            this.depth3Label12.Text = "▷ ";
            this.depth3Label12.Visible = false;
            // 
            // depth3Label13
            // 
            this.depth3Label13.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.depth3Label13.AutoSize = true;
            this.depth3Label13.Cursor = System.Windows.Forms.Cursors.Default;
            this.depth3Label13.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.depth3Label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.depth3Label13.Location = new System.Drawing.Point(21, 381);
            this.depth3Label13.Name = "depth3Label13";
            this.depth3Label13.Size = new System.Drawing.Size(32, 21);
            this.depth3Label13.TabIndex = 71;
            this.depth3Label13.Text = "▷ ";
            this.depth3Label13.Visible = false;
            // 
            // depth3Label14
            // 
            this.depth3Label14.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.depth3Label14.AutoSize = true;
            this.depth3Label14.Cursor = System.Windows.Forms.Cursors.Default;
            this.depth3Label14.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.depth3Label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.depth3Label14.Location = new System.Drawing.Point(21, 410);
            this.depth3Label14.Name = "depth3Label14";
            this.depth3Label14.Size = new System.Drawing.Size(32, 21);
            this.depth3Label14.TabIndex = 72;
            this.depth3Label14.Text = "▷ ";
            this.depth3Label14.Visible = false;
            // 
            // depth3Label15
            // 
            this.depth3Label15.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.depth3Label15.AutoSize = true;
            this.depth3Label15.Cursor = System.Windows.Forms.Cursors.Default;
            this.depth3Label15.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.depth3Label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.depth3Label15.Location = new System.Drawing.Point(21, 439);
            this.depth3Label15.Name = "depth3Label15";
            this.depth3Label15.Size = new System.Drawing.Size(32, 21);
            this.depth3Label15.TabIndex = 73;
            this.depth3Label15.Text = "▷ ";
            this.depth3Label15.Visible = false;
            // 
            // depth3Label16
            // 
            this.depth3Label16.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.depth3Label16.AutoSize = true;
            this.depth3Label16.Cursor = System.Windows.Forms.Cursors.Default;
            this.depth3Label16.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.depth3Label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.depth3Label16.Location = new System.Drawing.Point(21, 468);
            this.depth3Label16.Name = "depth3Label16";
            this.depth3Label16.Size = new System.Drawing.Size(32, 21);
            this.depth3Label16.TabIndex = 74;
            this.depth3Label16.Text = "▷ ";
            this.depth3Label16.Visible = false;
            // 
            // depth3Label17
            // 
            this.depth3Label17.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.depth3Label17.AutoSize = true;
            this.depth3Label17.Cursor = System.Windows.Forms.Cursors.Default;
            this.depth3Label17.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.depth3Label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.depth3Label17.Location = new System.Drawing.Point(21, 501);
            this.depth3Label17.Name = "depth3Label17";
            this.depth3Label17.Size = new System.Drawing.Size(32, 21);
            this.depth3Label17.TabIndex = 75;
            this.depth3Label17.Text = "▷ ";
            this.depth3Label17.Visible = false;
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("굴림", 14F);
            this.textBox1.Location = new System.Drawing.Point(283, 32);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(237, 22);
            this.textBox1.TabIndex = 2;
            this.textBox1.Visible = false;
            // 
            // textBox2
            // 
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Font = new System.Drawing.Font("굴림", 14F);
            this.textBox2.Location = new System.Drawing.Point(283, 61);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(237, 22);
            this.textBox2.TabIndex = 3;
            this.textBox2.Visible = false;
            // 
            // textBox3
            // 
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.Font = new System.Drawing.Font("굴림", 14F);
            this.textBox3.Location = new System.Drawing.Point(283, 90);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(237, 22);
            this.textBox3.TabIndex = 4;
            this.textBox3.Visible = false;
            // 
            // textBox4
            // 
            this.textBox4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox4.Font = new System.Drawing.Font("굴림", 14F);
            this.textBox4.Location = new System.Drawing.Point(283, 119);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(237, 22);
            this.textBox4.TabIndex = 5;
            this.textBox4.Visible = false;
            // 
            // textBox7
            // 
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox7.Font = new System.Drawing.Font("굴림", 14F);
            this.textBox7.Location = new System.Drawing.Point(283, 206);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(237, 22);
            this.textBox7.TabIndex = 8;
            this.textBox7.Visible = false;
            // 
            // textBox8
            // 
            this.textBox8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox8.Font = new System.Drawing.Font("굴림", 14F);
            this.textBox8.Location = new System.Drawing.Point(283, 235);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(237, 22);
            this.textBox8.TabIndex = 9;
            this.textBox8.Visible = false;
            // 
            // textBox9
            // 
            this.textBox9.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox9.Font = new System.Drawing.Font("굴림", 14F);
            this.textBox9.Location = new System.Drawing.Point(283, 264);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(237, 22);
            this.textBox9.TabIndex = 10;
            this.textBox9.Visible = false;
            // 
            // textBox10
            // 
            this.textBox10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox10.Font = new System.Drawing.Font("굴림", 14F);
            this.textBox10.Location = new System.Drawing.Point(283, 293);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(237, 22);
            this.textBox10.TabIndex = 11;
            this.textBox10.Visible = false;
            // 
            // textBox11
            // 
            this.textBox11.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox11.Font = new System.Drawing.Font("굴림", 14F);
            this.textBox11.Location = new System.Drawing.Point(283, 322);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(237, 22);
            this.textBox11.TabIndex = 12;
            this.textBox11.Visible = false;
            // 
            // textBox13
            // 
            this.textBox13.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox13.Font = new System.Drawing.Font("굴림", 14F);
            this.textBox13.Location = new System.Drawing.Point(283, 467);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(237, 22);
            this.textBox13.TabIndex = 17;
            this.textBox13.Visible = false;
            // 
            // textBox17
            // 
            this.textBox17.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox17.Font = new System.Drawing.Font("굴림", 14F);
            this.textBox17.Location = new System.Drawing.Point(283, 496);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(237, 22);
            this.textBox17.TabIndex = 18;
            this.textBox17.Visible = false;
            // 
            // depth3Label0
            // 
            this.depth3Label0.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.depth3Label0.AutoSize = true;
            this.depth3Label0.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.depth3Label0.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.depth3Label0.Location = new System.Drawing.Point(21, 2);
            this.depth3Label0.Name = "depth3Label0";
            this.depth3Label0.Size = new System.Drawing.Size(38, 25);
            this.depth3Label0.TabIndex = 51;
            this.depth3Label0.Text = "▶ ";
            this.depth3Label0.Visible = false;
            // 
            // textBox5
            // 
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox5.Font = new System.Drawing.Font("굴림", 14F);
            this.textBox5.Location = new System.Drawing.Point(283, 148);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(237, 22);
            this.textBox5.TabIndex = 6;
            this.textBox5.Visible = false;
            // 
            // textBox6
            // 
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox6.Font = new System.Drawing.Font("굴림", 14F);
            this.textBox6.Location = new System.Drawing.Point(283, 177);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(237, 22);
            this.textBox6.TabIndex = 7;
            this.textBox6.Visible = false;
            // 
            // textBox0
            // 
            this.textBox0.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox0.Font = new System.Drawing.Font("굴림", 14F);
            this.textBox0.Location = new System.Drawing.Point(283, 3);
            this.textBox0.Name = "textBox0";
            this.textBox0.Size = new System.Drawing.Size(237, 22);
            this.textBox0.TabIndex = 1;
            this.textBox0.Visible = false;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox1.Font = new System.Drawing.Font("굴림", 12F);
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(283, 351);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(237, 24);
            this.comboBox1.TabIndex = 76;
            this.comboBox1.Visible = false;
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox2.Font = new System.Drawing.Font("굴림", 12F);
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(283, 380);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(237, 24);
            this.comboBox2.TabIndex = 77;
            this.comboBox2.Visible = false;
            // 
            // comboBox3
            // 
            this.comboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBox3.Font = new System.Drawing.Font("굴림", 12F);
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(283, 409);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(237, 24);
            this.comboBox3.TabIndex = 78;
            this.comboBox3.Visible = false;
            // 
            // textBox12
            // 
            this.textBox12.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox12.Font = new System.Drawing.Font("굴림", 14F);
            this.textBox12.Location = new System.Drawing.Point(283, 438);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(237, 22);
            this.textBox12.TabIndex = 13;
            this.textBox12.Visible = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.GateTreeView, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 503F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(361, 531);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label7.Location = new System.Drawing.Point(3, 1);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(131, 25);
            this.label7.TabIndex = 53;
            this.label7.Text = "▶ GATE LIST";
            // 
            // GateTreeView
            // 
            this.GateTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GateTreeView.Location = new System.Drawing.Point(3, 31);
            this.GateTreeView.Name = "GateTreeView";
            this.GateTreeView.Size = new System.Drawing.Size(355, 497);
            this.GateTreeView.TabIndex = 54;
            this.GateTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.GateListEleSelect);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 5;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.Controls.Add(this.AddNewGateBt, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.button12, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.button13, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.UpdateOrImportBt, 3, 0);
            this.tableLayoutPanel5.Controls.Add(this.DeleteOrCancelBt, 4, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 612);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(911, 58);
            this.tableLayoutPanel5.TabIndex = 51;
            // 
            // AddNewGateBt
            // 
            this.AddNewGateBt.BackColor = System.Drawing.Color.DodgerBlue;
            this.AddNewGateBt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AddNewGateBt.FlatAppearance.BorderSize = 0;
            this.AddNewGateBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddNewGateBt.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.AddNewGateBt.ForeColor = System.Drawing.Color.White;
            this.AddNewGateBt.Location = new System.Drawing.Point(3, 3);
            this.AddNewGateBt.Name = "AddNewGateBt";
            this.AddNewGateBt.Size = new System.Drawing.Size(176, 52);
            this.AddNewGateBt.TabIndex = 19;
            this.AddNewGateBt.Text = "게이트 추가";
            this.AddNewGateBt.UseVisualStyleBackColor = false;
            this.AddNewGateBt.Click += new System.EventHandler(this.AddNewGateBt_Click);
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.SlateGray;
            this.button12.FlatAppearance.BorderSize = 0;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button12.ForeColor = System.Drawing.Color.White;
            this.button12.Location = new System.Drawing.Point(185, 3);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(176, 52);
            this.button12.TabIndex = 20;
            this.button12.Text = "시스템 정보";
            this.button12.UseVisualStyleBackColor = false;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.button13.FlatAppearance.BorderSize = 0;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button13.ForeColor = System.Drawing.Color.White;
            this.button13.Location = new System.Drawing.Point(367, 3);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(176, 52);
            this.button13.TabIndex = 21;
            this.button13.Text = "유저 관리";
            this.button13.UseVisualStyleBackColor = false;
            // 
            // UpdateOrImportBt
            // 
            this.UpdateOrImportBt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(41)))), ((int)(((byte)(123)))));
            this.UpdateOrImportBt.FlatAppearance.BorderSize = 0;
            this.UpdateOrImportBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UpdateOrImportBt.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.UpdateOrImportBt.ForeColor = System.Drawing.Color.White;
            this.UpdateOrImportBt.Location = new System.Drawing.Point(549, 3);
            this.UpdateOrImportBt.Name = "UpdateOrImportBt";
            this.UpdateOrImportBt.Size = new System.Drawing.Size(176, 52);
            this.UpdateOrImportBt.TabIndex = 22;
            this.UpdateOrImportBt.Text = "추가<->수정";
            this.UpdateOrImportBt.UseVisualStyleBackColor = false;
            this.UpdateOrImportBt.Click += new System.EventHandler(this.UpdateOrImportBt_Click);
            // 
            // DeleteOrCancelBt
            // 
            this.DeleteOrCancelBt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.DeleteOrCancelBt.FlatAppearance.BorderSize = 0;
            this.DeleteOrCancelBt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DeleteOrCancelBt.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.DeleteOrCancelBt.ForeColor = System.Drawing.Color.White;
            this.DeleteOrCancelBt.Location = new System.Drawing.Point(731, 3);
            this.DeleteOrCancelBt.Name = "DeleteOrCancelBt";
            this.DeleteOrCancelBt.Size = new System.Drawing.Size(176, 52);
            this.DeleteOrCancelBt.TabIndex = 23;
            this.DeleteOrCancelBt.Text = "삭제<->취소";
            this.DeleteOrCancelBt.UseVisualStyleBackColor = false;
            this.DeleteOrCancelBt.Click += new System.EventHandler(this.DeleteOrCancelBt_Click);
            // 
            // ParkListComboBox
            // 
            this.ParkListComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.ParkListComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ParkListComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ParkListComboBox.Font = new System.Drawing.Font("맑은 고딕", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.ParkListComboBox.FormattingEnabled = true;
            this.ParkListComboBox.Location = new System.Drawing.Point(3, 9);
            this.ParkListComboBox.Name = "ParkListComboBox";
            this.ParkListComboBox.Size = new System.Drawing.Size(361, 48);
            this.ParkListComboBox.TabIndex = 0;
            this.ParkListComboBox.SelectedIndexChanged += new System.EventHandler(this.SelectParkIndex);
            // 
            // SogNoLib_GateDBViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(917, 673);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "SogNoLib_GateDBViewer";
            this.Text = "SogNoLib_GateDBViewer";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TreeView GateTreeView;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
		private System.Windows.Forms.Label depth3Label1;
		private System.Windows.Forms.Label depth3Label2;
		private System.Windows.Forms.Label depth3Label3;
		private System.Windows.Forms.Label depth3Label4;
		private System.Windows.Forms.Label depth3Label5;
		private System.Windows.Forms.Label depth3Label6;
		private System.Windows.Forms.Label depth3Label7;
		private System.Windows.Forms.Label depth3Label8;
		private System.Windows.Forms.Label depth3Label9;
		private System.Windows.Forms.Label depth3Label10;
		private System.Windows.Forms.Label depth3Label11;
		private System.Windows.Forms.Label depth3Label12;
		private System.Windows.Forms.Label depth3Label13;
		private System.Windows.Forms.Label depth3Label14;
		private System.Windows.Forms.Label depth3Label15;
		private System.Windows.Forms.Label depth3Label16;
		private System.Windows.Forms.Label depth3Label17;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.TextBox textBox4;
		private System.Windows.Forms.TextBox textBox6;
		private System.Windows.Forms.TextBox textBox5;
		private System.Windows.Forms.TextBox textBox7;
		private System.Windows.Forms.TextBox textBox8;
		private System.Windows.Forms.TextBox textBox9;
		private System.Windows.Forms.TextBox textBox10;
		private System.Windows.Forms.TextBox textBox11;
		private System.Windows.Forms.TextBox textBox12;
		private System.Windows.Forms.TextBox textBox13;
		private System.Windows.Forms.TextBox textBox17;
		private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
		private System.Windows.Forms.Button button13;
		private System.Windows.Forms.Button AddNewGateBt;
		private System.Windows.Forms.Button UpdateOrImportBt;
		private System.Windows.Forms.ComboBox ParkListComboBox;
		private System.Windows.Forms.Button button12;
		private System.Windows.Forms.Label depth3Label0;
		private System.Windows.Forms.TextBox textBox0;
        private System.Windows.Forms.Button DeleteOrCancelBt;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox3;
    }
}