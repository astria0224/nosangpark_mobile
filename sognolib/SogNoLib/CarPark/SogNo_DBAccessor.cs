﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using SogNoLib.DataAccess;
using System.Text.RegularExpressions;

namespace SogNoLib.Park
{
    public class CarIOSearchParams
    {
        public bool SearchByInDate { get; set; } = false;
        public bool SearchByOutDate { get; set; } = false;
        public DateTime SearchDateStart { get; set; }
        public DateTime SearchDateEnd { get; set; }
        public string SearchCarNumber { get; set; }
        public bool SearchNotMember { get; set; } = false;
        public bool SearchFreeMember { get; set; } = false;
        public bool SearchTollMember { get; set; } = false;
        public bool SearchBlackList { get; set; } = false;
        public bool SearchNotOut { get; set; } = false;
        public bool SearchNoread { get; set; } = false;
        public bool SearchTollFree { get; set; } = false;
    }


    public class SogNo_DBAccessor
    {
        private static NLog.Logger _log = NLog.LogManager.GetCurrentClassLogger();

        private SogNo_DBManager dbManager = null;

        public SogNo_DBAccessor(string ip, int port, string db)
        {
            try
            {
                DBInfo dbInfo = new DBInfo();
                dbInfo.DBIP = ip;
                dbInfo.DBPort = port;
                dbInfo.DBName = db;
                this.dbManager = new SogNo_DBManager(dbInfo);
            }
            catch
            {
                this.dbManager = null;
                throw;
            }
        }


        // **************** Park Info *********************
        public ParkInfoRecord[] SelectParkInfo(int? parkNo = null)
        {
            if (this.dbManager == null)
            {
                return null;
            }
            
            var where = new List<Tuple<string, string>>();
            if (parkNo != null)
            {
                where.Add(Tuple.Create(ParkInfoFields.ParkNo.ToString(), parkNo.ToString()));
            }

            string query = MakeNormalSelectQuery(Tables.parkinfo, where);
            return this.dbManager.GetRecords<ParkInfoRecord>(query);
        }

        public ParkInfoRecord[] SearchParkList(string SearchWord) /// 검색
		{
            if (this.dbManager == null) { return null; }

            string query = string.Format("SELECT * from `{0}` WHERE `ParkName` LIKE \'%{1}%\'", Tables.parkinfo, SearchWord);
            return this.dbManager.GetRecords<ParkInfoRecord>(query);
        }

        public void InsertParkInfo(ParkInfoRecord record) /// 주차장 정보를 DB에 INSERT
		{
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.parkinfo.ToString());
            }
        }

        public void UpdateParkInfo(ParkInfoRecord record) /// 주차장 정보를 DB에 UPDATE
		{
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    ParkInfoFields.ParkNo.ToString(),
                };
                string[] setFields = {
                    ParkInfoFields.ParkType.ToString(),
                    ParkInfoFields.ParkName.ToString(),
                    ParkInfoFields.ParkOwner.ToString(),
                    ParkInfoFields.ParkRegNum.ToString(),
                    ParkInfoFields.ParkPhone.ToString(),
                    ParkInfoFields.ParkAddr.ToString(),
                    ParkInfoFields.ParkNote.ToString(),
                    ParkInfoFields.ParkIPAddr.ToString(),
                    ParkInfoFields.ParkDomain.ToString(),
                    ParkInfoFields.ParkPort.ToString(),
                    ParkInfoFields.DefaultTrfNo.ToString()
                };

                this.dbManager.Update(record, Tables.parkinfo.ToString(), setFields, whereFields);
            }
        }

        public void DeleteParkInfo(ParkInfoRecord record) /// 주차장 정보를 DB에서 Delete
		{
            string[] whereFields = {
                ParkInfoFields.ParkNo.ToString()
            };

            if (this.dbManager != null)
            {
                this.dbManager.Delete(record, Tables.parkinfo.ToString(), whereFields);
            }
        }


        // **************** Gate Info *********************
        public GateInfoRecord[] SelectGateInfo(int parkNo, int? gateNo = null)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            where.Add(Tuple.Create(GateInfoFields.ParkNo.ToString(), parkNo.ToString()));
            if (gateNo != null)
            {
                where.Add(Tuple.Create(GateInfoFields.GateNo.ToString(), gateNo?.ToString()));
            }

            string query = MakeNormalSelectQuery(Tables.gateinfo, where);
            return this.dbManager.GetRecords<GateInfoRecord>(query);
        }


        // **************** PC Info *********************
        public PCInfoRecord[] SelectPCInfo(int parkNo, int? gateNo = null, int? pcNo = null)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            where.Add(Tuple.Create(PCInfoFields.ParkNo.ToString(), parkNo.ToString()));
            if (gateNo != null)
            {
                where.Add(Tuple.Create(PCInfoFields.GateNo.ToString(), gateNo?.ToString()));
            }
            if (pcNo != null)
            {
                where.Add(Tuple.Create(PCInfoFields.PCNo.ToString(), pcNo?.ToString()));
            }

            string query = MakeNormalSelectQuery(Tables.pcinfo, where);
            return this.dbManager.GetRecords<PCInfoRecord>(query);
        }


        // **************** Sys Info *********************
        public SysInfoRecord[] SelectSysInfo(int parkNo, int? pcNo = null, int? sysNo = null)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            where.Add(Tuple.Create(SysInfoFields.ParkNo.ToString(), parkNo.ToString()));
            if (pcNo != null)
            {
                where.Add(Tuple.Create(SysInfoFields.PCNo.ToString(), pcNo?.ToString()));
            }
            if (sysNo != null)
            {
                where.Add(Tuple.Create(SysInfoFields.SysNo.ToString(), sysNo?.ToString()));
            }

            string query = MakeNormalSelectQuery(Tables.sysinfo, where);
            return this.dbManager.GetRecords<SysInfoRecord>(query);
        }


        // **************** Cam Info *********************
        public CamInfoRecord[] SelectCamInfo(int parkNo,int? gateNo = null, int? camNo = null)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            where.Add(Tuple.Create(CamInfoFields.ParkNo.ToString(), parkNo.ToString()));
            if(gateNo != null)
            {
                where.Add(Tuple.Create(CamInfoFields.GateNo.ToString(), gateNo?.ToString()));
            }
            if (camNo != null)
            {
                where.Add(Tuple.Create(CamInfoFields.CamNo.ToString(), camNo?.ToString()));
            }
            string query = MakeNormalSelectQuery(Tables.caminfo, where);
            return this.dbManager.GetRecords<CamInfoRecord>(query);
        }


        // **************** Screen Info *********************
        public ScrInfoRecord[] SelectScreenInfo(int parkNo, int? gateNo = null, int? scrNo = null)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            where.Add(Tuple.Create(ScrInfoFields.ParkNo.ToString(), parkNo.ToString()));
            if (gateNo != null)
            {
                where.Add(Tuple.Create(ScrInfoFields.GateNo.ToString(), gateNo?.ToString()));
            }
            if (scrNo != null)
            {
                where.Add(Tuple.Create(ScrInfoFields.SCRNo.ToString(), scrNo?.ToString()));
            }

            string query = MakeNormalSelectQuery(Tables.scrinfo, where);
            return this.dbManager.GetRecords<ScrInfoRecord>(query);
        }


        // **************** Bar Info *********************
        public BarInfoRecord[] SelectBarInfo(int parkNo, int? gateNo = null, int? barNo = null)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            where.Add(Tuple.Create(BarInfoFields.ParkNo.ToString(), parkNo.ToString()));
            if (gateNo != null)
            {
                where.Add(Tuple.Create(BarInfoFields.GateNo.ToString(), gateNo?.ToString()));
            }
            if (barNo != null)
            {
                where.Add(Tuple.Create(BarInfoFields.BarNo.ToString(), barNo?.ToString()));
            }

            string query = MakeNormalSelectQuery(Tables.barinfo, where);
            return this.dbManager.GetRecords<BarInfoRecord>(query);
        }


        // **************** Customer Info *********************
        public bool CanInsertCustomerInfo(int parkNo, string carNumber)
        {
            string query = string.Format("SELECT COUNT(*) from `custinfo` WHERE `ParkNo`=\'{0}\' AND `CarNum`=\'{1}\'", parkNo, carNumber);

            int count = this.dbManager.Count(query);

            return count == 0;
        }

        public void InsertCustomerInfo(CustomerInfoRecord record)
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.custinfo.ToString());
            }
        }

        public CustomerInfoRecord[] SelectCustomerInfo(int parkNo, int? custNo = null, string carNumber = null)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            where.Add(Tuple.Create(CustomerInfoFields.ParkNo.ToString(), parkNo.ToString()));
            if (custNo != null)
            {
                where.Add(Tuple.Create(CustomerInfoFields.CustNo.ToString(), custNo?.ToString()));
            }
            if (carNumber != null)
            {
                where.Add(Tuple.Create(CustomerInfoFields.CarNum.ToString(), carNumber));
            }

            string query = MakeNormalSelectQuery(Tables.custinfo, where);
            return this.dbManager.GetRecords<CustomerInfoRecord>(query);
        }

        public CustomerInfoRecord[] SearchCustomerInfo(int parkNo, int? groupNo, string name, string carNumber)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            string query = string.Format("SELECT * from `{0}` WHERE `ParkNo`=\'{1}\'", Tables.custinfo, parkNo);

            if (groupNo != null)
            {
                query += string.Format(" AND `{0}`=\'{1}\'", CustomerInfoFields.GroupNo, groupNo);
            }

            if (name != null && name.Length > 0)
            {
                query += string.Format(" AND `{0}`=\'{1}\'", CustomerInfoFields.Name, name);
            }

            if (carNumber != null && carNumber.Length > 0)
            {
                query += string.Format(" AND `{0}` LIKE \'%{1}%\'", CustomerInfoFields.CarNum, carNumber);
            }

            return this.dbManager.GetRecords<CustomerInfoRecord>(query);
        }

        public void UpdateCustomerInfo(CustomerInfoRecord record)
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    CustomerInfoFields.ParkNo.ToString(),
                    CustomerInfoFields.CustNo.ToString()
                };
                string[] setFields = {
                    CustomerInfoFields.GroupNo.ToString(),
                    CustomerInfoFields.Name.ToString(),
                    CustomerInfoFields.TelNum.ToString(),
                    CustomerInfoFields.CarNum.ToString(),
                    CustomerInfoFields.Comp.ToString(),
                    CustomerInfoFields.Dept.ToString(),
                    CustomerInfoFields.Address.ToString(),
                    CustomerInfoFields.StartDate.ToString(),
                    CustomerInfoFields.EndDate.ToString(),
                    CustomerInfoFields.LimitDay.ToString(),
                    CustomerInfoFields.Memo.ToString()
                };

                this.dbManager.Update(record, Tables.custinfo.ToString(), setFields, whereFields);
            }
        }

        public void DeleteCustomerInfo(CustomerInfoRecord record)
        {
            string[] whereFields = {
                CustomerInfoFields.ParkNo.ToString(),
                CustomerInfoFields.CustNo.ToString()
            };

            this.dbManager.Delete(record, Tables.custinfo.ToString(), whereFields);
        }


        // **************** Group Info *********************
        public void InsertGroupInfo(GroupInfoRecord record)
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.groupinfo.ToString());
            }
        }

        public GroupInfoRecord[] SelectGroupInfo(int parkNo, int? groupNo = null)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            where.Add(Tuple.Create(GroupInfoFields.ParkNo.ToString(), parkNo.ToString()));
            if (groupNo != null)
            {
                where.Add(Tuple.Create(CustomerInfoFields.GroupNo.ToString(), groupNo?.ToString()));
            }

            string query = MakeNormalSelectQuery(Tables.groupinfo, where);
            return this.dbManager.GetRecords<GroupInfoRecord>(query);
        }

        public bool CanDeleteGroupInfo(int parkNo, int groupNo)
        {
            string query = string.Format("SELECT COUNT(*) from `custinfo` WHERE `ParkNo`=\'{0}\' AND `GroupNo`=\'{1}\'", parkNo, groupNo);

            int count = this.dbManager.Count(query);

            return count == 0;
        }

        public void DeleteGroupInfo(GroupInfoRecord groupInfo)
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    GroupInfoFields.ParkNo.ToString(),
                    GroupInfoFields.GroupNo.ToString()
                };

                this.dbManager.Delete(groupInfo, Tables.groupinfo.ToString(), whereFields);
            }
        }

        public void UpdateGroupInfo(GroupInfoRecord groupInfo)
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    GroupInfoFields.ParkNo.ToString(),
                    GroupInfoFields.GroupNo.ToString()
                };
                string[] setFields = {
                    GroupInfoFields.GroupName.ToString(),
                    GroupInfoFields.GroupType.ToString(),
                    GroupInfoFields.Amount.ToString(),
                    GroupInfoFields.TrfNo.ToString(),
                    GroupInfoFields.DCNo.ToString(),
                    GroupInfoFields.UseTime.ToString(),
                    GroupInfoFields.Memo.ToString()
                };

                this.dbManager.Update(groupInfo, Tables.groupinfo.ToString(), setFields, whereFields);
            }
        }

        public void SelectRegisterInfo(int parkNo, string carNumber, ref CustomerInfoRecord customerInfo, ref GroupInfoRecord groupInfo)
        {
            customerInfo = null;
            groupInfo = null;
            if (Functions.IsNoread(carNumber))
            {
                return;
            }

            // 등록 여부 검색
            CustomerInfoRecord[] customerInfoRecords = SelectCustomerInfo(parkNo, null, carNumber);

            if (customerInfoRecords.Length > 0)
            {
                customerInfo = customerInfoRecords[0];

                GroupInfoRecord[] groupInfoRecords = SelectGroupInfo(parkNo, customerInfo.GroupNo.Value);
                if (groupInfoRecords.Length > 0)
                {
                    groupInfo = groupInfoRecords[0];

                    _log.Info("등록 사용자: 번호({0}), 이름({1}), 그룹({2}:{3})", customerInfo.CarNum, customerInfo.Name, groupInfo.GroupNo, groupInfo.GroupName);
                }
                else
                {
                    _log.Warn("정기권 사용자(CustNo={0})의 정기권 그룹(GroupNo={1})을 조회할 수 없습니다.", customerInfo.CustNo, customerInfo.GroupNo);
                }
            }
        }


        // **************** Customer Amt Info *********************
        public void InsertCustAmtInfo(CustomerAmtInfoRecord record)
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.custamtinfo.ToString());
            }
        }


        // **************** Tariff *********************
        public void InsertTariff(TariffRecord record)
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.tariff.ToString());
            }
        }

        public TariffRecord[] SelectTariff(int parkNo)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            where.Add(Tuple.Create(TariffFields.ParkNo.ToString(), parkNo.ToString()));

            string query = MakeNormalSelectQuery(Tables.tariff, where);
            return this.dbManager.GetRecords<TariffRecord>(query);
        }

        public void UpdateTariff(TariffRecord record)
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    TariffFields.ParkNo.ToString(),
                    TariffFields.TrfNo.ToString()
                };
                string[] setFields = {
                    TariffFields.TrfName.ToString(),
                    TariffFields.MaxRates.ToString(),
                    TariffFields.DailyMaxRates.ToString(),
                    TariffFields.DailyCriterion.ToString(),
                    TariffFields.RoundType.ToString(),
                    TariffFields.RoundDecimal.ToString(),
                    TariffFields.WeekdaySetting.ToString(),
                    TariffFields.SaturdaySetting.ToString(),
                    TariffFields.SaturdayPrior.ToString(),
                    TariffFields.SundaySetting.ToString(),
                    TariffFields.SundayPrior.ToString(),
                    TariffFields.HolidaySetting.ToString(),
                    TariffFields.Reserve0.ToString()
                };

                this.dbManager.Update(record, Tables.tariff.ToString(), setFields, whereFields);
            }
        }

        public void DeleteTariff(TariffRecord record)
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    TariffFields.ParkNo.ToString(),
                    TariffFields.TrfNo.ToString()
                };

                this.dbManager.Delete(record, Tables.tariff.ToString(), whereFields);
            }
        }

        public void DeleteTariff(int parkNo, int trfNo)
        {
            TariffRecord record = new TariffRecord()
            {
                ParkNo = parkNo,
                TrfNo = trfNo
            };

            DeleteTariff(record);
        }


        // **************** SubTariff *********************
        public void InsertSubTariff(SubTariffRecord record)
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.subtariff.ToString());
            }
        }

        public SubTariffRecord[] SelectSubTariff(int parkNo)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            where.Add(Tuple.Create(SubTariffFields.ParkNo.ToString(), parkNo.ToString()));

            string query = MakeNormalSelectQuery(Tables.subtariff, where);
            return this.dbManager.GetRecords<SubTariffRecord>(query);
        }

        public void UpdateSubTariff(SubTariffRecord record)
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    SubTariffFields.ParkNo.ToString(),
                    SubTariffFields.SubTrfNo.ToString()
                };
                string[] setFields = {
                    SubTariffFields.SubTrfType.ToString(),
                    SubTariffFields.StartTime.ToString(),
                    SubTariffFields.EndTime.ToString(),
                    SubTariffFields.TurnTime.ToString(),
                    SubTariffFields.BaseTime.ToString(),
                    SubTariffFields.BaseRates.ToString(),
                    SubTariffFields.AddTime.ToString(),
                    SubTariffFields.AddRates.ToString()
                };

                this.dbManager.Update(record, Tables.subtariff.ToString(), setFields, whereFields);
            }
        }

        public void DeleteSubTariff(SubTariffRecord record)
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    SubTariffFields.ParkNo.ToString(),
                    SubTariffFields.SubTrfNo.ToString()
                };

                this.dbManager.Delete(record, Tables.subtariff.ToString(), whereFields);
            }
        }

        public void DeleteSubTariff(int parkNo, int subTrfNo)
        {
            SubTariffRecord record = new SubTariffRecord()
            {
                ParkNo = parkNo,
                SubTrfNo = subTrfNo
            };

            DeleteSubTariff(record);
        }
        
        public void DeleteAllSubTariffs(int parkNo, int trfNo)
        {
            if (this.dbManager != null)
            {
                SubTariffRecord record = new SubTariffRecord()
                {
                    ParkNo = parkNo,
                    TrfNo = trfNo
                };

                string[] whereFields = {
                    SubTariffFields.ParkNo.ToString(),
                    SubTariffFields.TrfNo.ToString()
                };

                this.dbManager.Delete(record, Tables.subtariff.ToString(), whereFields);
            }
        }


        // **************** DCInfo *********************
        public void InsertDCInfo(DCInfoRecord record)
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.dcinfo.ToString());
            }
        }

        public DCInfoRecord[] SelectDCInfo(int parkNo)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            where.Add(Tuple.Create(DCInfoFields.ParkNo.ToString(), parkNo.ToString()));

            string query = MakeNormalSelectQuery(Tables.dcinfo, where);
            return this.dbManager.GetRecords<DCInfoRecord>(query);
        }

        public void UpdateDCInfo(DCInfoRecord record)
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    DCInfoFields.ParkNo.ToString(),
                    DCInfoFields.DCNo.ToString()
                };
                string[] setFields = {
                    DCInfoFields.DCName.ToString(),
                    DCInfoFields.DCValue.ToString(),
                    DCInfoFields.DCType.ToString(),
                    DCInfoFields.DCBIndex.ToString()
                };

                this.dbManager.Update(record, Tables.dcinfo.ToString(), setFields, whereFields);
            }
        }

        public void DeleteDCInfo(DCInfoRecord record)
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    DCInfoFields.ParkNo.ToString(),
                    DCInfoFields.DCNo.ToString()
                };

                this.dbManager.Delete(record, Tables.dcinfo.ToString(), whereFields);
            }
        }


        // **************** TKProc *********************
        public void InsertTKProc(TKProcRecord record)
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.tkproc.ToString());
            }
        }


        // **************** CarIOData *********************
        public void InsertCarInData(CarIODataRecord record)
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.cariodata.ToString());
            }
        }

        public void InsertCarOutData(CarIODataRecord record)
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.cariodata.ToString());
            }
        }

        public void UpdateCarInData_AtEnd(CarIODataRecord record)
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    CarIODataFields.ParkNo.ToString(),
                    CarIODataFields.TKNo.ToString()
                };
                string[] setFields = {
                    CarIODataFields.TKType.ToString(),
                    CarIODataFields.GroupNo.ToString(),
                    CarIODataFields.GroupName.ToString(),
                    CarIODataFields.Comp.ToString(),
                    CarIODataFields.Dept.ToString(),
                    CarIODataFields.Name.ToString(),
                    CarIODataFields.InType.ToString(),
                    CarIODataFields.InCarNum1.ToString(),
                    CarIODataFields.OutType.ToString()
                };

                this.dbManager.Update(record, Tables.cariodata.ToString(), setFields, whereFields);
            }
        }

        public void UpdateCarInData_AtBack(CarIODataRecord record)
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    CarIODataFields.ParkNo.ToString(),
                    CarIODataFields.TKNo.ToString()
                };
                string[] setFields = {
                    CarIODataFields.TKType.ToString(),
                    CarIODataFields.GroupNo.ToString(),
                    CarIODataFields.GroupName.ToString(),
                    CarIODataFields.Comp.ToString(),
                    CarIODataFields.Dept.ToString(),
                    CarIODataFields.Name.ToString(),
                    CarIODataFields.InCarNum2.ToString(),
                    CarIODataFields.InImage2.ToString()
                };

                this.dbManager.Update(record, Tables.cariodata.ToString(), setFields, whereFields);
            }
        }

        public void UpdateCarOutData(CarIODataRecord record)
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    CarIODataFields.ParkNo.ToString(),
                    CarIODataFields.TKNo.ToString()
                };
                string[] setFields = {
                    CarIODataFields.TKType.ToString(),
                    CarIODataFields.GroupNo.ToString(),
                    CarIODataFields.GroupName.ToString(),
                    CarIODataFields.Comp.ToString(),
                    CarIODataFields.Dept.ToString(),
                    CarIODataFields.Name.ToString(),
                    CarIODataFields.OutDate.ToString(),
                    CarIODataFields.OutType.ToString(),
                    CarIODataFields.OutCamNo.ToString(),
                    CarIODataFields.OutCarNum1.ToString(),
                    CarIODataFields.OutImage1.ToString(),
                    CarIODataFields.ParkingTime.ToString(),
                    CarIODataFields.ParkingRates.ToString()
                };

                this.dbManager.Update(record, Tables.cariodata.ToString(), setFields, whereFields);
            }
        }

        public void UpdateCarInOutType(CarIODataRecord record)
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    CarIODataFields.ParkNo.ToString(),
                    CarIODataFields.TKNo.ToString()
                };
                string[] setFields = {
                    CarIODataFields.InType.ToString(),
                    CarIODataFields.OutType.ToString()
                };

                this.dbManager.Update(record, Tables.cariodata.ToString(), setFields, whereFields);
            }
        }

        public void UpdateCarOutType(CarIODataRecord record)
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    CarIODataFields.ParkNo.ToString(),
                    CarIODataFields.TKNo.ToString()
                };
                string[] setFields = {
                    CarIODataFields.OutType.ToString()
                };

                this.dbManager.Update(record, Tables.cariodata.ToString(), setFields, whereFields);
            }
        }

        public void UpdateCarOutType_ForMissing(int parkNo, string ticketNo, DateTime datetime)
        {
            CarIODataRecord ioData = new CarIODataRecord();
            ioData.ParkNo = parkNo;
            ioData.TKNo = ticketNo;
            ioData.OutDate = datetime;
            ioData.OutType = Constant.OutType_Missing;

            UpdateCarOutType_ForMissing(ioData);
        }

        public void UpdateCarOutType_ForMissing(CarIODataRecord ioData)
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    CarIODataFields.ParkNo.ToString(),
                    CarIODataFields.TKNo.ToString()
                };
                string[] setFields = {
                    CarIODataFields.OutDate.ToString(),
                    CarIODataFields.OutType.ToString()
                };

                this.dbManager.Update(ioData, Tables.cariodata.ToString(), setFields, whereFields);
            }
        }

        public void ClearCarOutMissingRecords(int parkNo, string carNumber, DateTime localtime, string exceptTKNo)
        {
            //CarIODataRecord[] carIODataRecords = this.parkDB.SelectLastCarIOData_Car(this.ParkNo, carNumber, 3);
            CarIODataRecord[] carIODataRecords = SelectCarOutMissingRecords(parkNo, carNumber);

            foreach (var record in carIODataRecords)
            {
                if (record.TKNo == exceptTKNo)
                {
                    continue;
                }
                else if (record.OutType == null || record.OutType.Value == Constant.OutType_NotOut)
                {
                    _log.Warn("** 미출차 데이터 처리. 번호({0}, {1}), 입차시간({2})", record.InCarNum1, record.InCarNum2, record.InDate);

                    record.OutDate = localtime;
                    record.OutType = Constant.OutType_Missing;

                    UpdateCarOutType_ForMissing(record);
                }
            }
        }

        public IondataRecord[] SelectIONdata()
        {
            if (this.dbManager == null) { return null; }

            string query = string.Format("SELECT * from `{0}` group by {1}", Tables.iondata,IondataFields.InCarNum1);
            return this.dbManager.GetRecords<IondataRecord>(query);
        }
        
        public CartypeinfoRecord[] SearchLightCarByCarName(string carname)
        {
            if (this.dbManager == null) { return null; }

            string str_carname = carname.Substring(0,2);
            string query = string.Format(@"SELECT * from `{0}` WHERE `CarName` LIKE '{1}%'", Tables.cartypeinfo, carname);
            return this.dbManager.GetRecords<CartypeinfoRecord>(query);
        }

        public LightCarRecord[] SearchLightCarByCarPLNo(string carplno)
        {
            if (this.dbManager == null) { return null; }

            string query = string.Format("SELECT * from `{0}` WHERE `CarPLNo` LIKE \'{1}\'", Tables.lightcar, carplno);
            return this.dbManager.GetRecords<LightCarRecord>(query);
        }

        public void Insertcartypeinfo(CartypeinfoRecord record) /// 경차여부테이블에 인서트
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.cartypeinfo.ToString());
            }
        }

        public void InsertLightCarInfo(LightCarRecord record) /// 차량별확인DB 테이블에 인서트
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.lightcar.ToString());
            }
        }

        public CarIODataRecord[] SelectLastCarIOData_Car(int parkNo, string carNumber, int max_records)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            string query = string.Format("SELECT * from `{0}` WHERE `ParkNo`=\'{1}\' AND (`InCarNum1`=\'{2}\' OR `InCarNum2`=\'{2}\') ORDER BY `InDate` DESC LIMIT {3}", Tables.cariodata, parkNo, carNumber, max_records);

            return this.dbManager.GetRecords<CarIODataRecord>(query);
        }

        public CarIODataRecord[] SelectLastCarIOData_Cam(int parkNo, int camID, int max_records)
        {
            if (this.dbManager == null)
            {
                return null;
            }

        string query = string.Format("SELECT * from `{0}` WHERE `ParkNo`=\'{1}\' AND `InCamNo`=\'{2}\' ORDER BY `InDate` DESC LIMIT {3}", Tables.cariodata, parkNo, camID, max_records);

        return this.dbManager.GetRecords<CarIODataRecord>(query);
        }


        public void InsertGateInfo(GateInfoRecord record) /// 해당 주차장의 게이트 정보를 DB에 INSERT
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.gateinfo.ToString());
            }
        }

        public void InsertCamInfo(CamInfoRecord record) /// 해당 주차장의 카메라 정보를 DB에 INSERT
        {
            if (this.dbManager != null)
            {
               this.dbManager.Insert(record, Tables.caminfo.ToString());
            }
        }

        public void InsertBarInfo(BarInfoRecord record)/// 해당 주차장의 차단기 정보를 DB에 INSERT
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.barinfo.ToString());
            }
        }

        public void InsertScrInfo(ScrInfoRecord record)/// 해당 주차장의 전광판 정보를 DB에 INSERT
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.scrinfo.ToString());
            }
        }

        public void InsertPcInfo(PCInfoRecord record)/// 해당 주차장의 컴퓨터 정보를 DB에 INSERT
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.pcinfo.ToString());
            }
        }

        public CarIODataRecord[] SelectCarOutMissingRecords(int parkNo, string carNumber)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            string query = string.Format("SELECT * from `{0}` WHERE `ParkNo`=\'{1}\' AND (`InCarNum1`=\'{2}\' OR `InCarNum2`=\'{2}\') AND (`OutType` is null OR `OutType`=\'{3}\')", Tables.cariodata, parkNo, carNumber, Constant.OutType_NotOut);

            return this.dbManager.GetRecords<CarIODataRecord>(query);
        }

        public CarIODataRecord[] SelectNotOutCarsLikeExp(int parkNo, string like)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            string query;
            if (like.Length == 0)
            {
                query = string.Format("SELECT * from `{0}` WHERE `ParkNo`=\'{1}\' AND (`OutType` is null OR `OutType`=\'{2}\')", Tables.cariodata, parkNo, Constant.OutType_NotOut);
            }
            else
            {
                string likeExp = string.Format("%{0}%", like).Replace('x', '_').Replace('X', '_');

                query = string.Format("SELECT * from `{0}` WHERE `ParkNo`=\'{1}\' AND (`OutType` is null OR `OutType`=\'{3}\') AND (`InCarNum1` LIKE \'{2}\' OR `InCarNum2` LIKE \'{2}\')", Tables.cariodata, parkNo, likeExp, Constant.OutType_NotOut);
            }

            return this.dbManager.GetRecords<CarIODataRecord>(query);
        }
        
        public void UpdateGateInfo(GateInfoRecord record) /// 게이트 정보를 DB에 UPDATE
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    GateInfoFields.ParkNo.ToString(),
                    GateInfoFields.GateNo.ToString()
                };
                string[] setFields = {
                    GateInfoFields.GateName.ToString(), 
                    GateInfoFields.GateIP.ToString(),
                    GateInfoFields.GatePort.ToString(),
                    GateInfoFields.LPRBoardIP.ToString(),
                    GateInfoFields.LPRBoardPort.ToString()
                };

                this.dbManager.Update(record, Tables.gateinfo.ToString(), setFields, whereFields);
            }
        }

        public void UpdateCamInfo(CamInfoRecord record) /// 카메라 정보를 DB에 UPDATE
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    CamInfoFields.ParkNo.ToString(),
                    CamInfoFields.GateNo.ToString(),
                    CamInfoFields.CamNo.ToString()
                };
                string[] setFields = {
                    CamInfoFields.CamName.ToString(),
                    CamInfoFields.Target.ToString(),
                    CamInfoFields.EMax.ToString(),
                    CamInfoFields.EMin.ToString(),
                    CamInfoFields.AutoCCount.ToString(),
                    CamInfoFields.CapCount.ToString(),
                    CamInfoFields.FrameRate.ToString(),
                    CamInfoFields.CamIP.ToString(),
                    CamInfoFields.CamIO.ToString(),
                    CamInfoFields.CamOrder.ToString(),
                    CamInfoFields.Cmd.ToString(),
                    CamInfoFields.PreBarOpen.ToString(),
                    CamInfoFields.BarNo.ToString(),
                    CamInfoFields.ScrNo.ToString(),
                    CamInfoFields.FourCamNo.ToString(),
                    CamInfoFields.RecnArea.ToString(),
                    CamInfoFields.RecnSet.ToString(),
                };

                this.dbManager.Update(record, Tables.caminfo.ToString(), setFields, whereFields);
            }
        }

        public void UpdateBarInfo(BarInfoRecord record) /// 차단기 정보를 DB에 UPDATE
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    BarInfoFields.ParkNo.ToString(),
                    BarInfoFields.GateNo.ToString(),
                    BarInfoFields.BarNo.ToString()
                };

                string[] setFields = {
                    BarInfoFields.BarIP.ToString(),
                    BarInfoFields.BarPort.ToString(),
                    BarInfoFields.BarName.ToString(),
                    BarInfoFields.BarACTime.ToString(),
                    BarInfoFields.BarConnType.ToString(),
                    BarInfoFields.BarOpenCmd.ToString(),
                    BarInfoFields.BarCloseCmd.ToString(),
                    BarInfoFields.BarOneWay.ToString()
                };

                this.dbManager.Update(record, Tables.barinfo.ToString(), setFields, whereFields);
            }
         }

        public void UpdateScrInfo(ScrInfoRecord record) /// 전광판 정보를 DB에 업데이트
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    ScrInfoFields.ParkNo.ToString(),
                    ScrInfoFields.GateNo.ToString(),
                    ScrInfoFields.SCRNo.ToString()
                };

                string[] setFields = {
                    ScrInfoFields.SCRIP.ToString(),
                    ScrInfoFields.SCRPort.ToString(),
                    ScrInfoFields.SCRName.ToString(),
                    ScrInfoFields.SCRTime.ToString(),
                    ScrInfoFields.UpText.ToString(),
                    ScrInfoFields.UpColor.ToString(),
                    ScrInfoFields.DownText.ToString(),
                    ScrInfoFields.DownColor.ToString(),
                };

                this.dbManager.Update(record, Tables.scrinfo.ToString(), setFields, whereFields);
            }
        }

        public void UpdatePcInfo(PCInfoRecord record)
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    PCInfoFields.ParkNo.ToString(),
                    PCInfoFields.GateNo.ToString(),
                    PCInfoFields.PCNo.ToString()
                };
                string[] setFields = {
                    PCInfoFields.IP.ToString(),
                    PCInfoFields.InternetIP.ToString(),
                    PCInfoFields.Name.ToString(),
                    PCInfoFields.Memo.ToString()
                };

                this.dbManager.Update(record, Tables.pcinfo.ToString(), setFields, whereFields);
            }
        }

        public CarIODataRecord[] SearchCarIOData(CarIOSearchParams prms)
        {
            string where = "";

            if (prms.SearchByInDate || prms.SearchByOutDate)
            {
                if (where.Length > 0) where += " AND ";

                if (prms.SearchByInDate && prms.SearchByOutDate)
                {
                    where += "(";
                }

                if (prms.SearchByInDate)
                {
                    where += string.Format("(DATE(`InDate`)>=\'{0}\' AND DATE(`InDate`)<=\'{1}\')", prms.SearchDateStart.ToString("yyyy-MM-dd"), prms.SearchDateEnd.ToString("yyyy-MM-dd"));
                }

                if (prms.SearchByInDate && prms.SearchByOutDate)
                {
                    where += " OR ";
                }

                if (prms.SearchByOutDate)
                {
                    where += string.Format("(DATE(`OutDate`)>=\'{0}\' AND DATE(`OutDate`)<=\'{1}\')", prms.SearchDateStart.ToString("yyyy-MM-dd"), prms.SearchDateEnd.ToString("yyyy-MM-dd"));
                }

                if (prms.SearchByInDate && prms.SearchByOutDate)
                {
                    where += ")";
                }
            }

            if (prms.SearchCarNumber != null && prms.SearchCarNumber.Length > 0)
            {
                if (where.Length > 0) where += " AND ";
                where += string.Format("(`InCarNum1` LIKE \'%{0}%\' OR `InCarNum2` LIKE \'%{0}%\' OR `OutCarNum1` LIKE \'%{0}%\')", prms.SearchCarNumber);
            }

            if (prms.SearchNotMember || prms.SearchFreeMember || prms.SearchTollMember || prms.SearchBlackList)
            {
                if (where.Length > 0) where += " AND ";
                where += "(";

                if (prms.SearchNotMember)
                {
                    where += string.Format("`TKType` is null OR `TKType`=\'{0}\'", Constant.TKType_NotRegistered);
                }

                if (prms.SearchFreeMember)
                {
                    if (prms.SearchNotMember)
                    {
                        where += " OR ";
                    }

                    where += string.Format("`TKType`=\'{0}\'", Constant.TKType_Registered_Free);
                }

                if (prms.SearchTollMember)
                {
                    if (prms.SearchNotMember || prms.SearchFreeMember)
                    {
                        where += " OR ";
                    }

                    where += string.Format("`TKType`=\'{0}\'", Constant.TKType_Registered_Toll);
                }

                if (prms.SearchBlackList)
                {
                    if (prms.SearchNotMember || prms.SearchFreeMember || prms.SearchTollMember)
                    {
                        where += " OR ";
                    }

                    where += string.Format("`TKType`=\'{0}\'", Constant.TKType_Registered_BlackList);
                }

                where += ")";
            }

            if (prms.SearchNotOut)
            {
                if (where.Length > 0) where += " AND ";
                where += string.Format("(`OutType` is null OR `OutType`=\'{0}\')", Constant.OutType_NotOut);
            }

            if (prms.SearchNoread)
            {
                if (where.Length > 0) where += " AND ";
                where += string.Format("(`InCarNum1` LIKE \'%x%\' OR `InCarNum2` LIKE \'%x%\' OR `OutCarNum1` LIKE \'%x%\')");
            }

            if (prms.SearchTollFree)
            {
                if (where.Length > 0) where += " AND ";
                where += string.Format("`ParkingRates`=\'0\'");
            }

            string query = "SELECT * FROM `cariodata`";
            if (where.Length > 0)
            {
                query += string.Format(" WHERE {0}", where);
            }

            if (prms.SearchByOutDate && !prms.SearchByInDate)
            {
                query += " ORDER BY `OutDate` DESC";
            }
            else
            {
                query += " ORDER BY `InDate` DESC";
            }            

            return this.dbManager.GetRecords<CarIODataRecord>(query);
        }


        private string MakeNormalSelectQuery(Tables table, List<Tuple<string, string>> where)
        {
            string query = string.Format("SELECT * FROM `{0}`", table);

            if (where != null && where.Count > 0)
            {
                for (int i = 0; i < where.Count; i++)
                {
                    if (i == 0)
                    {
                        query += " WHERE ";
                    }
                    else
                    {
                        query += " AND ";
                    }

                    if (where[i].Item2 == null)
                    {
                        query += string.Format("`{0}` is null", where[i].Item1);
                    }
                    else
                    {
                        query += string.Format("`{0}`=\'{1}\'", where[i].Item1, where[i].Item2);
                    }
                }
            }

            return query;
        }
        
        public void DeleteGateInfo(GateInfoRecord record)
        {
            string query = string.Format("DELETE from `{0}` WHERE `GateNo` = '{1}' and `ParkNo` = '{2}'", Tables.gateinfo, record.GateNo, record.ParkNo);

            if (this.dbManager != null)
            {
                this.dbManager.Delete(query);
            }
        }

        public void DeleteCamInfo(CamInfoRecord record)
        {
            string query = string.Format("DELETE from `{0}` WHERE `CamNo` = '{1}' and `GateNo` = '{2}' and `ParkNo` = '{3}'", Tables.caminfo, record.CamNo, record.GateNo, record.ParkNo);

            if (this.dbManager != null)
            {
               this.dbManager.Delete(query);
            }
        }

        public void DeleteBarInfo(BarInfoRecord record)
        {
            string query = string.Format("DELETE from `{0}` WHERE `BarNo` = '{1}' and `GateNo` = '{2}' and `ParkNo` = '{3}'", Tables.barinfo, record.BarNo, record.GateNo, record.ParkNo);
            if (this.dbManager != null)
            {
                this.dbManager.Delete(query);
            }
        }

        public void DeleteScrInfo(ScrInfoRecord record)
        {
            string query = string.Format("DELETE from `{0}` WHERE `SCRNo` = '{1}' and `GateNo` = '{2}' and `ParkNo` = '{3}'", Tables.scrinfo, record.SCRNo, record.GateNo, record.ParkNo);
            if (this.dbManager != null)
            {
                this.dbManager.Delete(query);
            }
        }

        public void DeletePcInfo(PCInfoRecord record)
        {
            string query = string.Format("DELETE from `{0}` WHERE `PCNo` = '{1}' and `GateNo` = '{2}' and `ParkNo` = '{3}'", Tables.pcinfo, record.PCNo, record.GateNo, record.ParkNo);
            if (this.dbManager != null)
            {
                this.dbManager.Delete(query);
            }
        }

        public HolidayRecord[] SelectHolidayInfo()
        {
            if (this.dbManager == null)
            {
                return null;
            }

            string query = MakeNormalSelectQuery(Tables.holiday, null);
            return this.dbManager.GetRecords<HolidayRecord>(query);
        }
    }
}
