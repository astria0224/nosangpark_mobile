﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SogNoLib.Park
{
    public class Constant
    {
        public const int LPRBoard_Connected = 0;    // LPR 보드 연결됨
        public const int LPRBoard_Disconnected = 1; // LPR 보드 연결 끊김

        public const int Cam_Disconnected = 0;      // 캠 연결 끊김
        public const int Cam_Connected = 1;         // 캠 연결됨

        public const int BarOneWay_False = 0;
        public const int BarOneWay_True = 1;        // 입출차 공용 차단기

        public const int Bar_Disconnected = 0;
        public const int Bar_Opened = 1;
        public const int Bar_Opening = 2;
        public const int Bar_Closed = 3;
        public const int Bar_Closing = 4;
        public const int Bar_Count = 5;
        public const int Bar_Stop = 6;
        public const int Bar_Initialize = 7;
        public const int Bar_Connecting = 8;

        public const int ParkType_Normal = 0;       // 일반 주차장
        public const int ParkType_MemberOnly = 1;   // 맴버 전용 주차장

        public const int SysType_Unknown = 0;
        public const int SysType_LPR = 1;           // LPR
        public const int SysType_Unmanned = 2;      // 무인정산
        public const int SysType_Manned = 3;        // 유인정산

        public const int CamIO_In = 0;              // 입차 카메라
        public const int CamIO_Out = 1;             // 출차 카메라

        public const int CamOrder_Front = 0;        // 전방 카메라
        public const int CamOrder_Back = 1;         // 후방 카메라

        public const int PreBarOpen_False = 0;
        public const int PreBarOpen_True = 1;       // 카메라 촬영 전 차단기 열기

        public const int CarType_Unknown = 0;       // 
        public const int CarType_BigsizeCar = 1;    // 대형차
        public const int CarType_MidsizeCar = 3;    // 중형차
        public const int CarType_CompactCar = 4;    // 경차

        public const int GroupType_Free = 1;                // 무료 출입
        public const int GroupType_Toll = 2;                // 요금 출입
        public const int GroupType_BlackList = 3;

        public const int TKType_NotRegistered = 0;  // 등록되지 않은 사용자
        public const int TKType_Registered_Free = GroupType_Free;     
        public const int TKType_Registered_Toll = GroupType_Toll;
        public const int TKType_Registered_BlackList = GroupType_BlackList;

        public const int InType_Deny = 0;           // 입차 거부
        public const int InType_Auto = 1;           // 자동 입차
        public const int InType_Manual = 2;         // 수동 입차 (팝업 등)
        public const int InType_AwayMode = 3;       // 자리비움 입차
        public const int InType_Missing = 4;        // 입차 누락 

        public const int OutType_NotOut = 0;        // 입차 상태
        public const int OutType_Auto = 1;          // 자동 출차
        public const int OutType_Manual = 2;        // 수동 출차 (요금계산 등)
        public const int OutType_AwayMode = 3;      // 자리비움 출차
        public const int OutType_Missing = 4;       // 출차 누락 (강제 설정됨)

        public const int DailyCriterion_Day = 0;    // 날짜 기준
        public const int DailyCriterion_InTime = 1; // 입차 시간

        public const int RoundType_None = 0;        // 반올림 하지 않음
        public const int RoundType_Ceil = 1;        // 올림
        public const int RoundType_Floor = 2;       // 내림   
        public const int RoundType_Round = 3;       // 반올림

        public const int WeekDaySettings_Free = 0;          // 무료
        public const int WeekdaySettings_Specific = 1;      // 평일 요금 별도 설정

        public const int SaturdaySettings_Free = 0;         // 무료
        public const int SaturdaySettings_Specific = 1;     // 토요일 요금 별도 설정
        public const int SaturdaySettings_Weekday = 2;      // 평일 요금으로

        public const int SaturdayPrior_Holiday = 0;         // 토요일에 공휴일이 들 경우 공휴일 요금으로
        public const int SaturdayPrior_Saturday = 1;        // 토요일 요금으로

        public const int SundaySettings_Free = 0;           // 무료
        public const int SundaySettings_Specific = 1;       // 일요일 요금 별도 설정
        public const int SundaySettings_Weekday = 2;        // 평일 요금으로
        public const int SundaySettings_Saturday = 3;       // 토요일 요금으로
        
        public const int SundayPrior_Holiday = 0;           // 일요일에 공휴일이 들 경우 공휴일 요금으로
        public const int SundayPrior_Sunday = 1;            // 일요일 요금으로

        public const int HolidaySettings_Free = 0;          // 무료
        public const int HolidaySettings_Specific = 1;      // 공휴일 요금 별도 설정
        public const int HolidaySettings_Weekday = 2;       // 평일 요금으로
        public const int HolidaySettings_Saturday = 3;      // 토요일 요금으로
        public const int HolidaySettings_Sunday = 4;        // 일요일 요금으로
        
        public const int SubTrfType_Weekday = 0;            // 평일 요금
        public const int SubTrfType_Saturday = 1;           // 토요일 요금
        public const int SubTrfType_Sunday = 2;             // 일요일 요금
        public const int SubTrfType_Holiday = 3;            // 공휴일 요금

        public const int DCType_Price = 0;
        public const int DCType_Time = 1;
        public const int DCType_Percentage = 2;
        public const int DCType_TimePeriod = 3;
        
        public const int PayType_Cash = 0;
        public const int PayType_Card = 1;

        public const int HolidayType_NotHoliday = 0;
        public const int HolidayType_Holiday = 1;
    }
}
