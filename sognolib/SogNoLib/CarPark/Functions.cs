﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SogNoLib.Park
{
    public class Functions
    {
        public static string CreateTKNo(int camNo, DateTime datetime)
        {
            return string.Format("C{0:D3}{1}", camNo, datetime.ToString("yyyyMMddHHmmssf"));
        }

        public static bool IsRead(string carNumber)
        {
            return !IsNoread(carNumber);
        }

        public static bool IsNoread(string carNumber)
        {
            char[] chars = { 'x', 'X' };
            return (carNumber == null || carNumber.Length == 0 || carNumber.IndexOfAny(chars) >= 0);
        }

        public static string CarTypeString(int? carType)
        {
            if (carType == Constant.CarType_CompactCar)
            {
                return "경차";
            }
            else if (carType == Constant.CarType_MidsizeCar)
            {
                return "일반";
            }
            else if (carType == Constant.CarType_BigsizeCar)
            {
                return "대형/화물";
            }

            return "";
        }

        public static string GetGroupTypeString(int? type)
        {
            if (type == Constant.GroupType_Toll)
            {
                return "유료 입출차";
            }
            else if (type == Constant.GroupType_Free)
            {
                return "무료 입출차";
            }
            else if (type == Constant.GroupType_BlackList)
            {
                return "블랙리스트";
            }

            return "";
        }

        public static string GetDCTypeString(int? dcType)
        {
            if (dcType == Constant.DCType_Price)
            {
                return "요금 할인";
            }
            else if (dcType == Constant.DCType_Time)
            {
                return "시간 할인";
            }
            else if (dcType == Constant.DCType_Percentage)
            {
                return "퍼센트 할인";
            }
            else if (dcType == Constant.DCType_TimePeriod)
            {
                return "구간 할인";
            }

            return "";
        }

        public static int CeilInt(int value, int deci)
        {
            if (value >= 0)
            {
                int mod = value % deci;
                if (mod != 0)
                {
                    value += deci - mod;
                }
            }
            else
            {
                int mod = value % deci;
                value -= mod;
            }

            return value;
        }

        public static int FloorInt(int value, int deci)
        {
            if (value > 0)
            {
                int mod = value % deci;
                value -= mod;
            }
            else
            {
                int mod = value % deci;
                if (mod != 0)
                {
                    value -= deci + mod;
                }
            }

            return value;
        }

        public static int RoundInt(int value, int deci)
        {
            double half = deci / 2.0;

            if (value > 0)
            {
                int mod = value % deci;
                if (mod >= half)
                {
                    return CeilInt(value, deci);
                }
                else
                {
                    return FloorInt(value, deci);
                }
            }
            else
            {
                int mod = value % deci;
                if (mod >= -half)
                {
                    return CeilInt(value, deci);
                }
                else
                {
                    return FloorInt(value, deci);
                }
            }
        }

        public static TimeSpan? ParseTimeSpan(string str)
        {
            try
            {
                string[] tokens = str.Split(new char[] { ':' });

                int hours = 0;
                int minutes = 0;
                int seconds = 0;

                if (tokens.Length > 0)
                {
                    hours = int.Parse(tokens[0]);
                }

                if (tokens.Length > 1)
                {
                    minutes = int.Parse(tokens[1]);
                }

                if (tokens.Length > 2)
                {
                    seconds = int.Parse(tokens[2]);
                }

                return new TimeSpan(hours, minutes, seconds);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
