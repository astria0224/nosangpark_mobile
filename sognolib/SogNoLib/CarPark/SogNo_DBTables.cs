﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SogNoLib.Park
{
    public enum Tables
    {
        parkinfo,
        gateinfo,
        pcinfo,
        sysinfo,
        caminfo,
        scrinfo,
        barinfo,
        custinfo,
        custamtinfo,
        groupinfo,
        iondata,
        cariodata,
        tariff,
        subtariff,
        dcinfo,
        tkproc,
        holiday,
        lightcar,
        cartypeinfo
    }

    public enum IondataFields
    {
        ParkNo,
        TKNo,
        TKType,
        CarType,
        InUnitID,
        InDate,
        InImage1,
        InCarNum1,
        InImage2,
        InCarNum2,
        OutUnitID,
        OutDate,
        OutImage1,
        OutCarNum1,
        OutImage2,
        OutCarNum2,
        Status,
        OutChk,
        ParkingTime,
        Reserve0,
        Reserve1,
        Reserve2,
    }

    public class IondataRecord
    {
        public int? ParkNo { get; set; } = null;
        public string TKNo { get; set; } = null;
        public int? TKType { get; set; } = null;
        public int? CarType { get; set; } = null;
        public string InUnitID { get; set; } = null;
        public DateTime? InDate { get; set; } = null;
        public string InImage1 { get; set; } = null;
        public string InCarNum1 { get; set; } = null;
        public string InImage2 { get; set; } = null;
        public string InCarNum2 { get; set; } = null;
        public string OutUnitID { get; set; } = null;
        public DateTime? OutDate { get; set; } = null;
        public string OutImage1 { get; set; } = null;
        public string OutCarNum1 { get; set; } = null;
        public string OutImage2 { get; set; } = null;
        public string OutCarNum2 { get; set; } = null;
        public int? Status { get; set; } = null;
        public int? OutChk { get; set; } = null;
        public int? ParkingTime { get; set; } = null;
        public string Reserve0 { get; set; } = null;
        public string Reserve1 { get; set; } = null;
        public string Reserve2 { get; set; } = null;
    }
    
    public enum CartypeinfoFields
    {
        LightCarNo,
        CarName,
        CarType
    }

    public class CartypeinfoRecord
    {
        public int? LightCarNo { get; set; } = null;
        public string CarName { get; set; } = null;
        public string CarType { get; set; } = null;
    }

    public enum LightcarFields 
    {
        LightCarNo,
        CarPLNo,
        CarName,
        CarType,
        Grade5,
        Mitigation,
        YearType
    }

    public class LightCarRecord 
    {
        public int? LightCarNo { get; set; } = null;
        public string CarPLNo { get; set; } = null;
        public string CarName { get; set; } = null;
        public string CarType { get; set; } = null;
        public string Grade5 { get; set; } = null;
        public string Mitigation { get; set; } = null;
        public string YearType { get; set; } = null;
    }
    
    // ~~~~~
    public enum ParkInfoFields
    {
        ParkNo,
        ParkType,
        ParkName,
        ParkOwner,
        ParkRegNum,
        ParkPhone,
        ParkAddr,
        ParkNote,
        ParkIPAddr,
        ParkDomain,
        ParkPort,
        DefaultTrfNo,
        Reserve0
    }

    public class ParkInfoRecord
    {
        public int? ParkNo { get; set; } = null;            // 주차장 번호: auto_increment
        public int? ParkType { get; set; } = null;          // 주차장 타입 (0: 일반 주차장, 1: 등록 사용자 전용)
        public string ParkName { get; set; } = null;        // 주차장 이름
        public string ParkOwner { get; set; } = null;       // 운영자
        public string ParkRegNum { get; set; } = null;      // 사업자등록번호
        public string ParkPhone { get; set; } = null;       // 전화번호
        public string ParkAddr { get; set; } = null;        // 주차장 주소
        public string ParkNote { get; set; } = null;        // 주차장 전화번호
        public string ParkIPAddr { get; set; } = null;      // 주차장 메인 IP
        public string ParkDomain { get; set; } = null;      // 주차장 도메인
        public int? ParkPort { get; set; } = null;          // 주차장 포트
        public int? DefaultTrfNo { get; set; } = null;      // 기본 요금 체계
        public string Reserve0 { get; set; } = null;
    }

    // ~~~~~
    public enum GateInfoFields
    {
        ParkNo,
        GateNo,
        GateName,
        GateIP,
        GatePort,
        LPRBoardIP,
        LPRBoardPort,
        Reserve0
    }

    public class GateInfoRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? GateNo { get; set; } = null;
        public string GateName { get; set; } = null;        // 게이트 이름
        public string GateIP { get; set; } = null;
        public int? GatePort { get; set; } = null;
        public string LPRBoardIP { get; set; } = null;
        public int? LPRBoardPort { get; set; } = null;
        public string Reserve0 { get; set; } = null;
    }

    // ~~~~~
    public enum PCInfoFields
    {
        ParkNo,
        GateNo,
        PCNo,
        IP,
        InternetIP,
        Name,
        Memo,
        Reserve0
    }

    public class PCInfoRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? GateNo { get; set; } = null;
        public int? PCNo { get; set; } = null;
        public string IP { get; set; } = null;
        public string InternetIP { get; set; } = null;
        public string Name { get; set; } = null;
        public string Memo { get; set; } = null;
        public string Reserve0 { get; set; } = null;
    }

    // ~~~~~
    public enum SysInfoFields
    {
        ParkNo,
        PCNo,
        SysNo,
        SysType,
        SysName,
        SysPort,
        Reserve0
    }

    public class SysInfoRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? PCNo { get; set; } = null;
        public int? SysNo { get; set; } = null;
        public int? SysType { get; set; } = null;
        public string SysName { get; set; } = null;
        public int? SysPort { get; set; } = null;
        public string Reserve0 { get; set; } = null;
    }

    // ~~~~~
    public enum CamInfoFields
    {
        ParkNo,
        GateNo,
        CamNo,
        CamName,
        Target,
        EMax,
        EMin,
        AutoCCount,
        CapCount,
        FrameRate,
        CamIP,
        CamIO,
        CamOrder,
        Cmd,
        PreBarOpen,
        BarNo,
        ScrNo,
        FourCamNo,
        RecnArea,
        RecnSet,
        Reserve0
    }

    public class CamInfoRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? GateNo { get; set; } = null;
        public int? CamNo { get; set; } = null;
        public string CamName { get; set; } = null;         // 카메라 이름
        public int? Target { get; set; } = null;            // 밝기 기준
        public int? EMax { get; set; } = null;              // 밝기 최대
        public int? EMin { get; set; } = null;              // 밝기 최소
        public int? AutoCCount { get; set; } = null;        // 밝기조절 캡쳐
        public int? CapCount { get; set; } = null;          // 이미지 캡쳐 개수
        public string FrameRate { get; set; } = null;       // 카메라 프레임
        public string CamIP { get; set; } = null;
        public int? CamIO { get; set; } = null;             // 입출차 설정 (0: 입차, 1: 출차)
        public int? CamOrder { get; set; } = null;          // 전후방 설정 (0: 전방, 1: 후방)
        public string Cmd { get; set; } = null;             // 카메라 동작 명령
        public int? PreBarOpen { get; set; } = null;        // (0: false, 1: true)
        public int? BarNo { get; set; } = null;             // 연동 차단기 번호
        public int? ScrNo { get; set; } = null;             // 연동 전광판 번호
        public int? FourCamNo { get; set; } = null;         // 연동 4면 카메라 번호
        public string RecnArea { get; set; } = null;        // 번호 인식 영역
        public string RecnSet { get; set; } = null;         // 번호 인식 설정
        public string Reserve0 { get; set; } = null;
    }

    // ~~~~~
    public enum BarInfoFields
    {
        ParkNo,
        BarNo,
        GateNo,
        BarIP,
        BarPort,
        BarName,
        BarACTime,
        BarConnType,
        BarOpenCmd,
        BarCloseCmd,
        BarOneWay,
        Reserve0
    }

    public class BarInfoRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? BarNo { get; set; } = null;
        public int? GateNo { get; set; } = null;
        public string BarIP { get; set; } = null;
        public int? BarPort { get; set; } = null;
        public string BarName { get; set; } = null;
        public int? BarACTime { get; set; } = null;         // 차단기 자동 닫힘 시간
        public int? BarConnType { get; set; } = null;       // 차단기 연결 형태 (0: RS232, 1: 접점)
        public string BarOpenCmd { get; set; } = null;      // 접점시 차단기 열기 명령
        public string BarCloseCmd { get; set; } = null;     // 접점시 차단기 닫기 명령
        public int? BarOneWay { get; set; } = null;         // 0: false, 1: true
        public string Reserve0 { get; set; } = null;
    }

    // ~~~~~
    public enum ScrInfoFields
    {
        ParkNo,
        GateNo,
        SCRNo,
        SCRIP,
        SCRPort,
        SCRName,
        SCRTime,
        UpText,
        UpColor,
        DownText,
        DownColor,
        Reserve0
    }

    public class ScrInfoRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? GateNo { get; set; } = null;
        public int? SCRNo { get; set; } = null;
        public string SCRIP { get; set; } = null;
        public int? SCRPort { get; set; } = null;
        public string SCRName { get; set; } = null;
        public int? SCRTime { get; set; } = null;
        public string UpText { get; set; } = null;          // 전광판 상단 문구
        public int? UpColor { get; set; } = null;
        public string DownText { get; set; } = null;        // 전광판 하단 문구
        public int? DownColor { get; set; } = null;
        public string Reserve0 { get; set; } = null;
    }

    // ~~~~~
    public enum CarIODataFields
    {
        ParkNo,
        TKNo,
        TKType,
        CarType,
        GroupNo,
        GroupName,
        Comp,
        Dept,
        Name,
        InDate,
        InType,
        InCamNo,
        InImage1,
        InCarNum1,
        InImage2,
        InCarNum2,
        OutDate,
        OutType,
        OutCamNo,
        OutImage1,
        OutCarNum1,
        ParkingTime,
        ParkingRates,
        DCName,
        Reserve0
    }

    public class CarIODataRecord
    {
        public int? ParkNo { get; set; } = null;
        public string TKNo { get; set; } = null;
        public int? TKType { get; set; } = null;            // 0: 일반차량, 1: 정기차량
        public int? CarType { get; set; } = null;           // 0:, 1: 대형차, 3: 중형차, 4: 경차
        public int? GroupNo { get; set; } = null;
        public string GroupName { get; set; } = null;       // 그룹이름
        public string Comp { get; set; } = null;            // 회사
        public string Dept { get; set; } = null;            // 부서
        public string Name { get; set; } = null;            // 이름
        public DateTime? InDate { get; set; } = null;       // 입차일시
        public int? InType { get; set; } = null;            // 입차타입 (0: 거부, 1: 자동, 2: 팝업, 3: 자리비움, 4: 입차누락)
        public int? InCamNo { get; set; } = null;
        public string InImage1 { get; set; } = null;
        public string InCarNum1 { get; set; } = null;
        public string InImage2 { get; set; } = null;
        public string InCarNum2 { get; set; } = null;
        public DateTime? OutDate { get; set; } = null;      // 출차일시
        public int? OutType { get; set; } = null;           // 출차타입 (0: 미출차, 1: 자동, 2: 팝업, 3: 자리비움, 4: 출차누락)
        public int? OutCamNo { get; set; } = null;
        public string OutImage1 { get; set; } = null;
        public string OutCarNum1 { get; set; } = null;
        public int? ParkingTime { get; set; } = null;       // 주차시간
        public int? ParkingRates { get; set; } = null;      // 주차요금
        public string DCName { get; set; } = null;          // 할인권이름
        public string Reserve0 { get; set; } = null;

        public bool IsValid()
        {
            return (this.ParkNo != null && this.TKNo != null && this.TKNo.Length > 0 && this.CarType != null && this.InDate != null);
        }

        public string GetValidCarNumber()
        {
            if (Functions.IsRead(this.InCarNum1))
            {
                return this.InCarNum1;
            }
            else if (Functions.IsRead(this.InCarNum2))
            {
                return this.InCarNum2;
            }
            else if (Functions.IsRead(this.OutCarNum1))
            {
                return this.OutCarNum1;
            }

            return null;
        }
    }

    // ~~~~~
    public enum CustomerInfoFields
    {
        CustNo,
        ParkNo,
        GroupNo,
        Name,
        TelNum,
        CarNum,
        Comp,
        Dept,
        Address,
        StartDate,
        EndDate,
        LimitDay,
        Memo,
        Reserve0
    }

    public class CustomerInfoRecord
    {
        public int? CustNo { get; set; } = null;
        public int? ParkNo { get; set; } = null;
        public int? GroupNo { get; set; } = null;
        public string Name { get; set; } = null;            // 이름
        public string TelNum { get; set; } = null;          // 전화번호
        public string CarNum { get; set; } = null;          // 차량번호
        public string Comp { get; set; } = null;            // 회사
        public string Dept { get; set; } = null;            // 부서
        public string Address { get; set; } = null;         // 주소
        public DateTime? StartDate { get; set; } = null;    // 시작일시
        public DateTime? EndDate { get; set; } = null;      // 종료일시
        public string LimitDay { get; set; } = null;        // 요일제, 10부제 제한일
        public string Memo { get; set; } = null;
        public string Reserve0 { get; set; } = null;
    }

    // ~~~~~
    public enum CustomerAmtInfoFields
    {
        AmtNo,
        CustNo,
        ParkNo,
        UserNo,
        StartDate,
        EndDate,
        Amount,
        IssueDate,
        Reserve0
    }

    public class CustomerAmtInfoRecord
    {
        public int? AmtNo { get; set; } = null;
        public int? CustNo { get; set; } = null;
        public int? ParkNo { get; set; } = null;
        public int? UserNo { get; set; } = null;
        public DateTime? StartDate { get; set; } = null;
        public DateTime? EndDate { get; set; } = null;
        public int? Amount { get; set; } = null;
        public DateTime? IssueDate { get; set; } = null;
        public string Reserve0 { get; set; } = null;
    }

    // ~~~~~
    public enum GroupInfoFields
    {
        ParkNo,
        GroupNo,
        GroupName,
        GroupType,
        Amount,
        TrfNo,
        DCNo,
        UseTime,
        Memo,
        Reserve0
    }

    public class GroupInfoRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? GroupNo { get; set; } = null;
        public string GroupName { get; set; } = null;       // 그룹이름
        public int? GroupType { get; set; } = null;         // 0: 일반, 1: 요금, 2: 블랙리스트
        public int? Amount { get; set; } = null;
        public int? TrfNo { get; set; } = null;             // 사용 주차요금
        public int? DCNo { get; set; } = null;              // 사용 할인권
        public string UseTime { get; set; } = null;         // 사용 제한시간 (07:00~09:00)
        public string Memo { get; set; } = null;
        public string Reserve0 { get; set; } = null;
    }

    // ~~~~~
    public enum TariffFields
    {
        ParkNo,
        TrfNo,
        TrfName,
        MaxRates,
        DailyMaxRates,
        DailyCriterion,
        RoundType,
        RoundDecimal,
        WeekdaySetting,
        SaturdaySetting,
        SaturdayPrior,
        SundaySetting,
        SundayPrior,
        HolidaySetting,
        Reserve0
    }

    public class TariffRecord
    {
        public int? ParkNo { get; set; } = null;            
        public int? TrfNo { get; set; } = null;             // 요금 체계 번호
        public string TrfName { get; set; } = null;         // 요금 체계 이름
        public int? MaxRates { get; set; } = null;          // 전체 최대 요금
        public int? DailyMaxRates { get; set; } = null;     // 일일 최대 요금
        public int? DailyCriterion { get; set; } = null;
        public int? RoundType { get; set; } = null;         // 반올림 방식, 0: 없음, 1: 올림, 2: 내름, 3: 반올림
        public int? RoundDecimal { get; set; } = null;      // 반올림 자릿수, 예: 100
        public int? WeekdaySetting { get; set; } = null;    // 평일 요금 설정, 0: 별도 설정, 4: 무료
        public int? SaturdaySetting { get; set; } = null;   // 토요일 요금 설정, 0: 별도 설정, 1: 평일 요금으로, 4: 무료
        public int? SaturdayPrior { get; set; } = null;     // 토요일 요금 우선순위 (0: 공휴일 요금 우선, 1: 토요일 요금 우선)
        public int? SundaySetting { get; set; } = null;     // 일요일 요금 설정, 0: 별도 설정, 1: 평일 요금으로, 2: 토요일 요금으로, 4: 무료
        public int? SundayPrior { get; set; } = null;       // 일요일 요금 우선순위 결정 (0: 공휴일 요금 우선, 1: 일요일 요금 우선)
        public int? HolidaySetting { get; set; } = null;    // 공휴일 요금 설정, 0: 공휴일 요금 별도 설정, 1: 평일 요금으로, 2: 토요일 요금으로, 3: 일요일 요금으로, 4: 무료
        public string Reserve0 { get; set; } = null;
    }

    // ~~~~~
    public enum SubTariffFields
    {
        ParkNo,
        SubTrfNo,
        TrfNo,
        SubTrfType,
        StartTime,
        EndTime,
        TurnTime,
        BaseTime,
        BaseRates,
        AddTime,
        AddRates,
        Reserve0
    }

    public class SubTariffRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? SubTrfNo { get; set; } = null;
        public int? TrfNo { get; set; } = null;
        public int? SubTrfType { get; set; } = null;
        public TimeSpan? StartTime { get; set; } = null;
        public TimeSpan? EndTime { get; set; } = null;
        public int? TurnTime { get; set; } = null;
        public int? BaseTime { get; set; } = null;
        public int? BaseRates { get; set; } = null;
        public int? AddTime { get; set; } = null;
        public int? AddRates { get; set; } = null;
        public string Reserve0 { get; set; } = null;
    }

    // ~~~~~
    public enum DCInfoFields
    {
        ParkNo,
        DCNo,
        DCName,
        DCValue,
        DCType,
        DCBIndex,
        Reserve0
    }

    public class DCInfoRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? DCNo { get; set; } = null;
        public string DCName { get; set; } = null;
        public string DCValue { get; set; } = null;
        public int? DCType { get; set; } = null;
        public int? DCBIndex { get; set; } = null;
        public string Reserve0 { get; set; } = null;
    }

    // ~~~~~
    public enum TKProcFields
    {
        ParkNo,
        SysNo,
        ProcDate,
        TKNo,
        TKType,
        CarType,
        TrfNo,
        CarNum,
        InDate,
        ParkingTime,
        TotalRates,
        RealRates,
        DCNo,
        UserNo,
        TKPCNo,
        PayType,
        Reserve0,
        Reserve1
    }

    public class TKProcRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? SysNo { get; set; } = null;
        public DateTime? ProcDate { get; set; } = null;
        public string TKNo { get; set; } = null;
        public int? TKType { get; set; } = null;
        public int? CarType { get; set; } = null;
        public int? TrfNo { get; set; } = null;
        public string CarNum { get; set; } = null;
        public DateTime? InDate { get; set; } = null;
        public int? ParkingTime { get; set; } = null;
        public int? TotalRates { get; set; } = null;
        public int? RealRates { get; set; } = null;
        public int? DCNo { get; set; } = null;
        public int? UserNo { get; set; } = null;
        public int? TKPCNo { get; set; } = null;
        public int? PayType { get; set; } = null;
        public string Reserve0 { get; set; } = null;
        public string Reserve1 { get; set; } = null;
    }

    // ~~~~~
    public enum HolidayFields
    {
        Date,
        Type,
        Note
    }

    public class HolidayRecord
    {
        public DateTime? Date { get; set; } = null;
        public int? Type { get; set; } = null;
        public string Note { get; set; } = null;
    }
}
