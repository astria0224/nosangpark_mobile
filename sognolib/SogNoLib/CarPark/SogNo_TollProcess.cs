﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;


namespace SogNoLib.Park
{
    public class SogNo_TollProcess
    {
        private static NLog.Logger _log = NLog.LogManager.GetCurrentClassLogger();

        private SogNo_DBAccessor parkDB = null;
        private ParkInfoRecord parkInfo = null;

        private int ParkNo { get { return this.parkInfo.ParkNo.Value; } }

        public TariffRecord[] TrfRecords { get; private set; } = null;
        public SubTariffRecord[] SubTrfRecords { get; private set; } = null;
        public DCInfoRecord[] DCInfoRecords { get; private set; } = null;
        public HolidayRecord[] HolidayRecords { get; private set; } = null;

        private SogNo_Tariff[] tariffs = null;
        private SogNo_Tariff DefaultTariff = null;

        public int DefaultDrfNo { get { return this.DefaultTariff.TrfNo.Value; } }

        private object mutex = new object();

        public SogNo_TollProcess(SogNo_DBAccessor parkDB, ParkInfoRecord parkInfo)
        {
            this.parkDB = parkDB;
            this.parkInfo = parkInfo;
        }

        public void Initialzize()
        {
            lock (this.mutex)
            {
                // 요금 체계 정보
                this.TrfRecords = this.parkDB.SelectTariff(this.ParkNo);
                this.SubTrfRecords = this.parkDB.SelectSubTariff(this.ParkNo);
                this.DCInfoRecords = this.parkDB.SelectDCInfo(this.ParkNo);
                this.HolidayRecords = this.parkDB.SelectHolidayInfo();
                
                if (this.TrfRecords.Length == 0)
                {
                    throw new Exception("데이터베이스에 요금 체계 정보가 없습니다.");
                }

                this.tariffs = new SogNo_Tariff[this.TrfRecords.Length];
                for (int i = 0; i < this.TrfRecords.Length; i++)
                {
                    var trf = this.TrfRecords[i];
                    SubTariffRecord[] subtrfs = Array.FindAll(this.SubTrfRecords, item => item.TrfNo == trf.TrfNo);

                    _log.Debug("요금체계 정보 획득: TrfNo={0}, TrfName={1}, DailyMaxRates={2}", trf.TrfNo, trf.TrfName, trf.DailyMaxRates);
                    foreach (var subtrf in subtrfs)
                    {
                        _log.Debug("  - 세부 요금체계: SubTrfNo={0}, SubTrfType={1}, StartTime={2}, EndTime={3}", subtrf.SubTrfNo, subtrf.SubTrfType, subtrf.StartTime, subtrf.EndTime);
                    }

                    this.tariffs[i] = new SogNo_Tariff();
                    this.tariffs[i].Init(trf, subtrfs, this.HolidayRecords);

                    if (this.tariffs[i].TrfNo == this.parkInfo.DefaultTrfNo)
                    {
                        this.DefaultTariff = this.tariffs[i];
                    }
                }

                if (this.DefaultTariff == null)
                {
                    throw new Exception(string.Format("TrfNo={0}인 요금 체계가 없습니다.", this.parkInfo.DefaultTrfNo));
                }
            }
        }

        public string GetDCName(int? dcNo)
        {
            if (dcNo != null)
            {
                int dcIndex = Array.FindIndex(this.DCInfoRecords, item => item.DCNo == dcNo);
                if (dcIndex >= 0)
                {
                    return this.DCInfoRecords[dcIndex].DCName;
                }
            }

            return null;
        }

        public Tuple<int, int> Toll(CarIODataRecord ioData, CustomerInfoRecord custInfo, int? trfNo, int? dcNo, out List<Tuple<string, int>> dailyFee)
        {
            _log.Debug("Toll {0}", ioData.GetValidCarNumber());
            Debug.Assert(ioData.InDate != null && ioData.OutDate != null);

            DCInfoRecord dcInfo = null;
            if (dcNo != null)
            {
                int dcIndex = Array.FindIndex(this.DCInfoRecords, item => item.DCNo == dcNo);
                if (dcIndex < 0)
                {
                    throw new Exception(string.Format("DCNo={0}인 할인 체계가 없습니다.", dcNo));
                }

                dcInfo = this.DCInfoRecords[dcIndex];
            }


            dailyFee = new List<Tuple<string, int>>();
            SogNo_Tariff lastTrf = this.DefaultTariff;
        
            DateTime inCarDateTime = ioData.InDate.Value;
            DateTime outCarDateTime = ioData.OutDate.Value;


            // 회차 체크
            {
                TimeSpan parkingTime = outCarDateTime - inCarDateTime;
                SogNo_Tariff trf = this.DefaultTariff;
                if (custInfo != null && trfNo != null)
                {
                    int trfIndex = Array.FindIndex(this.tariffs, item => item.TrfNo == trfNo);
                    trf = this.tariffs[trfIndex];
                }
                int turnTime = trf.GetSubTariff(inCarDateTime)?.TurnTime ?? 0;

                if (parkingTime.TotalMinutes <= turnTime)
                {
                    return Tuple.Create(0, trf.TrfNo.Value);
                }
            }


            // 시간 할인 적용
            if (dcInfo?.DCType == Constant.DCType_Time)
            {
                int dcMinute = 0;
                if (Int32.TryParse(dcInfo.DCValue, out dcMinute))
                {
                    outCarDateTime = outCarDateTime.AddMinutes(-dcMinute);
                }
            }



            if (custInfo == null || trfNo == null)
            {
                this.DefaultTariff.Toll(inCarDateTime, outCarDateTime, dcInfo, dailyFee, inCarDateTime);
            }
            else
            {
                int trfIndex = Array.FindIndex(this.tariffs, item => item.TrfNo == trfNo);
                if (trfIndex < 0)
                {
                    throw new Exception(string.Format("TrfNo={0}인 요금 체계가 없습니다.", trfNo));
                }

                SogNo_Tariff trf = this.tariffs[trfIndex];
                if (inCarDateTime >= custInfo.StartDate && outCarDateTime <= custInfo.EndDate)
                {
                    trf.Toll(inCarDateTime, outCarDateTime, dcInfo, dailyFee, inCarDateTime);
                    lastTrf = trf;
                }
                else if (inCarDateTime < custInfo.StartDate && outCarDateTime > custInfo.StartDate && outCarDateTime <= custInfo.EndDate)
                {
                    DateTime tollEndTime = this.DefaultTariff.Toll(inCarDateTime, custInfo.StartDate.Value, dcInfo, dailyFee, inCarDateTime);
                    tollEndTime = trf.Toll(tollEndTime, outCarDateTime, dcInfo, dailyFee, inCarDateTime);
                    lastTrf = trf;
                }
                else if (inCarDateTime >= custInfo.StartDate && inCarDateTime < custInfo.EndDate && outCarDateTime > custInfo.EndDate)
                {
                    DateTime tollEndTime = trf.Toll(inCarDateTime, custInfo.EndDate.Value, dcInfo, dailyFee, inCarDateTime);
                    tollEndTime = this.DefaultTariff.Toll(tollEndTime, outCarDateTime, dcInfo, dailyFee, inCarDateTime);
                }
                else if (inCarDateTime < custInfo.StartDate && outCarDateTime > custInfo.EndDate)
                {
                    DateTime tollEndTime = this.DefaultTariff.Toll(inCarDateTime, custInfo.StartDate.Value, dcInfo, dailyFee, inCarDateTime);
                    tollEndTime = trf.Toll(tollEndTime, custInfo.EndDate.Value, dcInfo, dailyFee, inCarDateTime);
                    tollEndTime = this.DefaultTariff.Toll(tollEndTime, outCarDateTime, dcInfo, dailyFee, inCarDateTime);
                }
                else
                {
                    this.DefaultTariff.Toll(inCarDateTime, outCarDateTime, dcInfo, dailyFee, inCarDateTime);
                }
            }

            int totalParkingRates = 0;
            foreach (var tuple in dailyFee)
            {
                totalParkingRates += tuple.Item2;
            }

            // DC 금액 할인 적용
            if (dcInfo?.DCType == Constant.DCType_Price)
            {
                int dcPrice = 0;
                if (Int32.TryParse(dcInfo.DCValue, out dcPrice))
                {
                    totalParkingRates -= dcPrice;
                    if (totalParkingRates < 0)
                    {
                        totalParkingRates = 0;
                    }
                }
            }
            // DC 퍼센트 할인 적용
            else if (dcInfo?.DCType == Constant.DCType_Percentage)
            {
                int dcPerc = 0;
                if (Int32.TryParse(dcInfo.DCValue, out dcPerc))
                {
                    totalParkingRates = totalParkingRates - (totalParkingRates * dcPerc / 100);
                }
            }


            // 반올림 처리
            if (lastTrf.TrfRecord.RoundType == Constant.RoundType_Ceil)
            {
                totalParkingRates = Functions.CeilInt(totalParkingRates, lastTrf.TrfRecord.RoundDecimal.Value);
            }
            else if (lastTrf.TrfRecord.RoundType == Constant.RoundType_Floor)
            {
                totalParkingRates = Functions.FloorInt(totalParkingRates, lastTrf.TrfRecord.RoundDecimal.Value);
            }
            else if (lastTrf.TrfRecord.RoundType == Constant.RoundType_Round)
            {
                totalParkingRates = Functions.RoundInt(totalParkingRates, lastTrf.TrfRecord.RoundDecimal.Value);
            }


            // 천체 제한 요금
            if (lastTrf.TrfRecord.MaxRates != null && lastTrf.TrfRecord.MaxRates.Value > 0 && lastTrf.TrfRecord.MaxRates.Value < totalParkingRates)
            {
                totalParkingRates = lastTrf.TrfRecord.MaxRates.Value;
            }

            return Tuple.Create(totalParkingRates, lastTrf.TrfNo.Value);
        }
    }
}
