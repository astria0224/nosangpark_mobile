﻿using System;
using System.Text;
using System.Runtime.InteropServices;

namespace SognoLib.Core
{
    public class SogNo_InIManager
    {
        private static NLog.Logger _log = NLog.LogManager.GetCurrentClassLogger();

        public string FilePath { get; }

		public SogNo_InIManager(string filePath)
		{
            this.FilePath = "./" + filePath;
		}

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        /// Get ini Properties
        private string GetInIValue(string section, string key)
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString(section, key, "", temp, 255, this.FilePath);
            return temp.ToString();
        }

        /// Set ini Properties
        private void SetInIValue(string section, string key, string value)
        {
            WritePrivateProfileString(section, key, value, this.FilePath);
        }

        public void GetInIProperties(Object myObject)
        {
            RealGetInIProperties(myObject, myObject.GetType().Name);
        }

        public void GetInIProperties(Object myObject, int no)
        {
            RealGetInIProperties(myObject, myObject.GetType().Name + " " + no);
        }

        public void GetInIProperties(Object myObject, string section, int no)
        {
            RealGetInIProperties(myObject, section + " " + no);
        }

        private void RealGetInIProperties(Object myObject, string section)
        {
            foreach (var prop in myObject.GetType().GetProperties())
            {
                try
                {
                    if (GetInIValue(section, prop.Name) == "") continue;

                    if (prop.PropertyType.Name == "Int32")
                        prop.SetValue(myObject, Convert.ToInt32(GetInIValue(section, prop.Name)));
                    else if (prop.PropertyType.Name == "Double")
                        prop.SetValue(myObject, Convert.ToDouble(GetInIValue(section, prop.Name)));
                    else if (prop.PropertyType.Name == "Boolean")
                        prop.SetValue(myObject, Convert.ToBoolean(GetInIValue(section, prop.Name)));
                    else
                        prop.SetValue(myObject, GetInIValue(section, prop.Name));
                }
                catch (Exception ex)
                {
                    myObject = null;

                    _log.Error(ex, "RealGetInIProperties Error!");
                }
            }
        }

        public void SetInIProperties(Object myObject)
        {
            RealSetInIProperties(myObject, myObject.GetType().Name);
        }

        public void SetInIProperties(Object myObject, int no)
        {
            RealSetInIProperties(myObject, myObject.GetType().Name + " " + no);
        }

        public void SetInIProperties(Object myObject, string section, int no)
        {
            RealSetInIProperties(myObject, section + " " + no);
        }

        private void RealSetInIProperties(Object myObject, string section)
        {
            foreach (var prop in myObject.GetType().GetProperties())
            {
                try
                {
                    SetInIValue(section, prop.Name, prop.GetValue(myObject).ToString());
                }
                catch (Exception ex)
				{
                    _log.Error(ex, "RealSetInIProperties Error!");
                }
            }
        }
    }
}
