﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace SognoLib.Core
{
	public class SogNo_TcpServer
	{
		private static NLog.Logger _log = NLog.LogManager.GetCurrentClassLogger();

		public delegate void ListenDelegate();
		public event ListenDelegate ListenEvent;
		public delegate void CloseDelegate();
		public event CloseDelegate CloseEvent;
		public delegate void ConnectDelegate(string type, int no, string ip);
		public event ConnectDelegate ConnectEvent;
		public delegate void DisconnectDelegate(string type, int no);
		public event DisconnectDelegate DisconnectEvent;

		public delegate void ReceiveDelegate(string type, int no, string message);
		public event ReceiveDelegate ReceiveEvent;

        public delegate void ReceiveBytesDelegate(string type, int no, byte[] message);
        public event ReceiveBytesDelegate ReceiveBytesEvent;

        private Socket listener = null;

		private int port;
		private string myType;
		private int myNo = 0;
		private string MyName
		{
			get
			{
				return string.Format("{0}#{1}", this.myType, this.myNo);
			}
		}

		private CommListManager sockListManager = new CommListManager();


		public SogNo_TcpServer(string myType, int myNo = 0)
		{
			this.myType = myType;
			this.myNo = myNo;
		}

		~SogNo_TcpServer()
		{
			Close();
		}

		public void Listen(string ip, int port)
		{
			if (this.listener != null)
			{
				return;
			}
			
			_log.Info("<{0}> Listen Start {1}:{2}", this.MyName, ip, port);

			this.port = port;

			try
			{
				IPAddress ipaddr = (ip.Length == 0 ? IPAddress.Any : IPAddress.Parse(ip));
				IPEndPoint localEP = new IPEndPoint(ipaddr, this.port);

				this.listener = new Socket(ipaddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
				this.listener.Bind(localEP);
				this.listener.Listen(10);

                BeginInvokeListenHandler();

				listener.BeginAccept(new AsyncCallback(AcceptCallback), this.listener);
			}
			catch (Exception ex)
			{
				_log.Error(ex, "<{0}> Listen Exception!", this.MyName);

				Close();
			}
		}

		private void AcceptCallback(IAsyncResult ar)
		{
			_log.Trace("<{0}> AcceptCallback Start", this.MyName);

			try
			{
				Socket listener = (Socket)ar.AsyncState;
				Socket sock = listener.EndAccept(ar);

				_log.Info("<{0}> Socket connected from {1}", this.MyName, sock.RemoteEndPoint.ToString());

				SogNo_TcpCommunication tcpComm = new SogNo_TcpCommunication();
				tcpComm.StartCommunication(sock);

				if(!this.sockListManager.Add(tcpComm))
				{
					_log.Error("<{0}> ??? 새 연결이 이미 리스트에 있음.", this.MyName);
					tcpComm.CloseCommunication();
				}

				int tempCount = this.sockListManager.Size();
				_log.Info("<{0}> Connected Sockets {1} -> {2}", this.MyName, tempCount - 1, tempCount);

				tcpComm.DisconnectEvent += OnDisconnectEvent;
				tcpComm.ReceiveEvent += OnReceiveEvent;
                tcpComm.ReceiveBytesEvent += OnReceiveBytesEvent;

				listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);
			}
			catch (ObjectDisposedException)
			{ // Close()한 소켓의 pending BeginReceive로 무시함 (MSDN doc에 기술됨)
                _log.Info("<{0}> ObjectDisposedException", this.MyName);
            }
			catch (Exception ex)
			{
				_log.Error(ex, "<{0}> AcceptCallback Exception!", this.MyName);
                listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);
            }

			_log.Trace("<{0}> AcceptCallback End", this.MyName);
		}

		public void Disconnect(string endType)
		{
			SogNo_TcpCommunication[] conns;
			if (endType == null || endType.Length == 0)
			{
				conns = this.sockListManager.All();
			}
			else
			{
				conns = this.sockListManager.Find(endType);
			}

			foreach (var conn in conns)
			{
				conn.CloseCommunication();
			}
		}

		public void Disconnect(string endType, int endNo)
		{
			SogNo_TcpCommunication[] conns;
			if (endType == null || endType.Length == 0)
			{
				conns = this.sockListManager.All(endNo);
			}
			else
			{
				conns = this.sockListManager.Find(endType, endNo);
			}

			foreach (var conn in conns)
			{
				conn.CloseCommunication();
			}
		}

		public void Send(string endType, byte[] msg, bool containsBinary = false)
		{
			SogNo_TcpCommunication[] conns;
			if (endType == null || endType.Length == 0)
			{
				conns = this.sockListManager.All();
			}
			else
			{
				conns = this.sockListManager.Find(endType);
			}

			foreach (var conn in conns)
			{
				conn.Send(msg, containsBinary);
			}
		}

		public void Send(string endType, int endNo, byte[] msg, bool containsBinary = false)
		{
			SogNo_TcpCommunication[] conns;
			if (endType == null || endType.Length == 0)
			{
				conns = this.sockListManager.All(endNo);
			}
			else
			{
				conns = this.sockListManager.Find(endType, endNo);
			}

			foreach (var conn in conns)
			{
				conn.Send(msg, containsBinary);
			}
		}

		public void Close()
		{
			_log.Trace("<{0}> Close Start", this.MyName);

			if (this.listener != null)
			{
				try
				{
					_log.Debug("<{0}> Connected: {1}, Bound: {2}", this.MyName, this.listener.Connected, this.listener.IsBound);
					this.listener.Close();

					SogNo_TcpCommunication[] conns = this.sockListManager.All();
					foreach(var conn in conns)
					{
						conn.CloseCommunication();
					}

                    BeginInvokeCloseHandler();
				}
				catch (Exception ex)
				{
					_log.Error(ex, "<{0}> Close Exception!", this.MyName);
				}

				this.listener = null;
			}

			_log.Trace("<{0}> Close End", this.MyName);
		}

		private void OnDisconnectEvent(object sender)
		{
			SogNo_TcpCommunication tcpComm = (SogNo_TcpCommunication)sender;

			try
			{
				_log.Info("<{0}> {1}와 연결이 끊어졌습니다.", this.MyName, tcpComm.EndName);

				if (tcpComm.EndType.Length > 0)
				{
                    BeginInvokeDisconnectHandler(tcpComm.EndType, tcpComm.EndNo);
                }

				if(!this.sockListManager.Remove(tcpComm))
				{
					_log.Error("<{0}> ??? {1} 연결을 리스트에서 삭제할 수 없음.", this.MyName, tcpComm.EndName);
				}

				int tempCount = this.sockListManager.Size();
				_log.Debug("<{0}> Connected Sockets {1} -> {2}", this.MyName, tempCount+1, tempCount);
			}
			catch (Exception ex)
			{
				_log.Error(ex, "<{0}> OnDisconnectEvent Exception!", this.MyName);
			}
		}

		private void OnReceiveEvent(object sender, string message)
		{
			_log.Debug("<{0}> OnReceiveEvent", this.MyName);

			SogNo_TcpCommunication tcpComm = (SogNo_TcpCommunication)sender;
			
			if (SogNo_Message.GetMsgID(message) == EMsgID.Unknown.ToString())
			{
                BeginInvokeReceiveHandler(tcpComm.EndType, tcpComm.EndNo, message);
            }
			else if (SogNo_Message.GetMsgID(message) == EMsgID.Intro.ToString())
			{
				SogNo_MessageIntro intro = new SogNo_MessageIntro();

				try
				{
					intro.UnPack(message);

					string prevName = tcpComm.EndName;
					tcpComm.EndType = intro.EndType;
					tcpComm.EndNo = intro.EndNo;
					_log.Info("<{0}> Name Changed {1} -> {2}", this.MyName, prevName, tcpComm.EndName);

                    BeginInvokeConnectHandler(intro.EndType, tcpComm.EndNo, tcpComm.EndIP);
                }
				catch (Exception ex)
				{
					_log.Error(ex, "<{0}> SogNo_MessageIntro.Unpack fail!", this.MyName);
				}
			}
			else if (tcpComm.EndType.Length > 0)
			{
                BeginInvokeReceiveHandler(tcpComm.EndType, tcpComm.EndNo, message);
			}
			else
			{
				_log.Warn("<{0}> Message Ignored!", this.MyName);
			}
		}

        private void OnReceiveBytesEvent(object sender, byte[] message)
        {
            _log.Debug("<{0}> OnReceiveBytesEvent", this.MyName);

            SogNo_TcpCommunication tcpComm = (SogNo_TcpCommunication)sender;

            if (SogNo_Message.GetMsgID(message) == EMsgID.Unknown.ToString())
            {
                BeginInvokeReceiveBytesHandler(tcpComm.EndType, tcpComm.EndNo, message);
            }
            else if (SogNo_Message.GetMsgID(message) == EMsgID.Intro.ToString())
            {
                SogNo_MessageIntro intro = new SogNo_MessageIntro();

                try
                {
                    intro.UnPack(message);

                    string prevName = tcpComm.EndName;
                    tcpComm.EndType = intro.EndType;
                    tcpComm.EndNo = intro.EndNo;
                    _log.Info("<{0}> Name Changed {1} -> {2}", this.MyName, prevName, tcpComm.EndName);

                    BeginInvokeConnectHandler(intro.EndType, tcpComm.EndNo, tcpComm.EndIP);
                }
                catch (Exception ex)
                {
                    _log.Error(ex, "<{0}> SogNo_MessageIntro.Unpack fail!", this.MyName);
                }
            }
            else if (tcpComm.EndType.Length > 0)
            {
                BeginInvokeReceiveBytesHandler(tcpComm.EndType, tcpComm.EndNo, message);
            }
            else
            {
                _log.Warn("<{0}> Message Ignored!", this.MyName);
            }
        }

        private void BeginInvokeListenHandler()
        {
            if (ListenEvent != null)
            {
                foreach (ListenDelegate handler in ListenEvent.GetInvocationList())
                {
                    handler.BeginInvoke(handler.EndInvoke, null);
                }
            }
        }

        private void BeginInvokeCloseHandler()
        {
            if (CloseEvent != null)
            {
                foreach (CloseDelegate handler in CloseEvent.GetInvocationList())
                {
                    handler.BeginInvoke(handler.EndInvoke, null);
                }
            }
        }

        private void BeginInvokeConnectHandler(string type, int no, string ip)
        {
            if (ConnectEvent != null)
            {
                foreach (ConnectDelegate handler in ConnectEvent.GetInvocationList())
                {
                    handler.BeginInvoke(type, no, ip, handler.EndInvoke, null);
                }
            }
        }

        private void BeginInvokeDisconnectHandler(string type, int no)
        {
            if (DisconnectEvent != null)
            {
                foreach (DisconnectDelegate handler in DisconnectEvent.GetInvocationList())
                {
                    handler.BeginInvoke(type, no, handler.EndInvoke, null);
                }
            }
        }

        private void BeginInvokeReceiveHandler(string type, int no, string message)
        {
            if (ReceiveEvent != null)
            {
                foreach (ReceiveDelegate handler in ReceiveEvent.GetInvocationList())
                {
                    handler.BeginInvoke(type, no, message, handler.EndInvoke, null);
                }
            }
        }

        private void BeginInvokeReceiveBytesHandler(string type, int no, byte[] message)
        {
            if (ReceiveBytesEvent != null)
            {
                foreach (ReceiveBytesDelegate handler in ReceiveBytesEvent.GetInvocationList())
                {
                    handler.BeginInvoke(type, no, message, handler.EndInvoke, null);
                }
            }
        }


        class CommListManager
		{
			private LinkedList<SogNo_TcpCommunication> list = new LinkedList<SogNo_TcpCommunication>();

			private readonly object _locker = new object();

			public bool Add(SogNo_TcpCommunication item)
			{
				lock (this._locker)
				{
					if (this.list.Find(item) == null)
					{
						this.list.AddLast(item);
						return true;
					}

					return false;
				}
			}

			public bool Remove(SogNo_TcpCommunication item)
			{
				lock (this._locker)
				{
					return this.list.Remove(item);
				}
			}

			public int Size()
			{
				lock(this._locker)
				{
					return this.list.Count;
				}
			}

			public SogNo_TcpCommunication[] All()
			{
				lock (this._locker)
				{
					SogNo_TcpCommunication[] all = new SogNo_TcpCommunication[this.list.Count];

					LinkedListNode<SogNo_TcpCommunication> node = this.list.First;
					for (int i = 0; i < all.Length; i++)
					{
						all[i] = node.Value;
						node = node.Next;
					}

					return all;
				}
			}

			public SogNo_TcpCommunication[] All(int no)
			{
				lock (this._locker)
				{
					List<SogNo_TcpCommunication> conns = new List<SogNo_TcpCommunication>();

					LinkedListNode<SogNo_TcpCommunication> node = this.list.First;
					while (node != null)
					{
						if (node.Value.EndNo == no)
						{
							conns.Add(node.Value);
						}

						node = node.Next;
					}

					return conns.ToArray();
				}
			}

			public SogNo_TcpCommunication[] Find(string type)
			{
				lock (this._locker)
				{
					List<SogNo_TcpCommunication> conns = new List<SogNo_TcpCommunication>();

					LinkedListNode<SogNo_TcpCommunication> node = this.list.First;
					while(node != null)
					{
						if(node.Value.EndType == type)
						{
							conns.Add(node.Value);
						}

						node = node.Next;
					}

					return conns.ToArray();
				}
			}

			public SogNo_TcpCommunication[] Find(string type, int no)
			{
				lock (this._locker)
				{
					List<SogNo_TcpCommunication> conns = new List<SogNo_TcpCommunication>();

					LinkedListNode<SogNo_TcpCommunication> node = this.list.First;
					while (node != null)
					{
						if (node.Value.EndType == type && node.Value.EndNo == no)
						{
							conns.Add(node.Value);
						}

						node = node.Next;
					}

					return conns.ToArray();
				}
			}
		}
	}
}
