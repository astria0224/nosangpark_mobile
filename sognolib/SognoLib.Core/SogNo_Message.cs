﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;


namespace SognoLib.Core
{
    public enum EMsgID
    {
        Unknown,
        Intro
    };


    public abstract class SogNo_Message
	{
        public struct Point
        {
            public int x, y;
        }

        public const int HeaderLen = 11;  // (STX)T00000000#
		public const int FooterLen = 1;   // (ETX)

		public const byte STX = 0x02;
		public const byte ETX = 0x03;
		public const byte Seperator = (byte)'#';
        public const byte TextSign = (byte)'T';
        public const byte BinarySign = (byte)'B';

        public char CharSTX
		{
			get
			{
				return Convert.ToChar(STX);
			}
		}
		public char CharETX
		{
			get
			{
				return Convert.ToChar(ETX);
			}
		}
        public char CharTextSign
        {
            get
            {
                return Convert.ToChar(TextSign);
            }
        }
        public char CharBinarySign
        {
            get
            {
                return Convert.ToChar(BinarySign);
            }
        }
        
        
		public static string GetMsgID(string messageBody)
		{
			int idx = messageBody.IndexOf(Convert.ToChar(Seperator));

			if(idx > 0)
			{
				return messageBody.Substring(0, idx);
			}
			else
			{
				return messageBody;
			}
		}

        public static string GetMsgID(byte[] messageBody)
        {
            int idx = Array.IndexOf(messageBody, Seperator);

            if (idx > 0)
            {
                return Encoding.Default.GetString(messageBody, 0, idx);
            }
            else
            {
                return Encoding.Default.GetString(messageBody);
            }
        }


        private const string DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff";
        public static string DateTimeToString(DateTime? dateTime)
        {
            return dateTime?.ToString(DateTimeFormat);
        }

        public static DateTime? StringToDateTime(string dateTimeString)
        {
            DateTime dateTime = new DateTime();
            if (DateTime.TryParseExact(dateTimeString, DateTimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime))
            {
                return dateTime;
            }

            return null;
        }

		abstract public byte[] Pack();
		abstract public void UnPack(string message);
        virtual public void UnPack(byte[] message)
        {
            UnPack(Encoding.Default.GetString(message));
        }
    }


	public class SogNo_MessageUnknown : SogNo_Message
	{
		public string MessageBody { get; set; }

		public override byte[] Pack()
		{
			int bodyLen = Encoding.Default.GetBytes(MessageBody).Length;

			string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, CharTextSign, bodyLen, MessageBody, CharETX);
			return Encoding.Default.GetBytes(message);
		}

		public override void UnPack(string messageBody)
		{
			this.MessageBody = messageBody;
		}
	}


	public class SogNo_MessageIntro : SogNo_Message
	{
		public string EndType { get; set; }
		public int EndNo { get; set; } = 0;

		public override byte[] Pack()
		{
			string msgBody = string.Format("{0}#{1}#{2}", EMsgID.Intro, this.EndType, this.EndNo);
			int bodyLen = Encoding.Default.GetBytes(msgBody).Length;

			string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, CharTextSign, bodyLen, msgBody, CharETX);
			return Encoding.Default.GetBytes(message);
		}

		public override void UnPack(string messageBody)
		{
			string msgID = GetMsgID(messageBody);
			if (msgID != EMsgID.Intro.ToString())
			{
				throw new Exception("메시지 ID가 일치하지 않음.");
			}

			string[] tokens = messageBody.Split(new char[1] { '#' });
			if (tokens.Length != 3)
			{
				throw new Exception("토큰의 갯수가 일치하지 않음.");
			}

			this.EndType = tokens[1];
			this.EndNo = int.Parse(tokens[2]);
		}
	}


}
