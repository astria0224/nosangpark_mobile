﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SognoLib.Customer.Core
{
    public enum Tables
    {
        parkinfo,
        tariff,
        subtariff,
        groupinfo,        
        dcinfo,
        caruserinfo,
        custgroupinfo,
        custgrouphistory,
        custgrouppayhistory
    }
    public enum ParkInfoFields
    {
        ParkNo,
        ParkType,
        ParkName,
        ParkOwner,
        ParkRegNum,
        ParkPhone,
        ParkAddr,
        ParkNote,
        ParkIPAddr,
        ParkDomain,
        ParkPort,
        DefaultTrfNo,
        Reserve0
    }

    public class ParkInfoRecord
    {
        public int? ParkNo { get; set; } = null;            // 주차장 번호: auto_increment
        public int? ParkType { get; set; } = null;          // 주차장 타입 (0: 일반 주차장, 1: 등록 사용자 전용)
        public string ParkName { get; set; } = null;        // 주차장 이름
        public string ParkOwner { get; set; } = null;       // 운영자
        public string ParkRegNum { get; set; } = null;      // 사업자등록번호
        public string ParkPhone { get; set; } = null;       // 전화번호
        public string ParkAddr { get; set; } = null;        // 주차장 주소
        public string ParkNote { get; set; } = null;        // 주차장 전화번호
        public string ParkIPAddr { get; set; } = null;      // 주차장 메인 IP
        public string ParkDomain { get; set; } = null;      // 주차장 도메인
        public int? ParkPort { get; set; } = null;          // 주차장 포트
        public int? DefaultTrfNo { get; set; } = null;      // 기본 요금 체계
        public string Reserve0 { get; set; } = null;
    }

    public enum GroupInfoFields
    {
        ParkNo,
        GroupNo,
        GroupName,
        GroupType,
        Amount,
        TrfNo,
        DCNo,
        UseTime,
        Memo,
        Reserve0
    }

    public class GroupInfoRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? GroupNo { get; set; } = null;
        public string GroupName { get; set; } = null;       // 그룹이름
        public int? GroupType { get; set; } = null;         // 0: 일반, 1: 요금, 2: 블랙리스트
        public int? Amount { get; set; } = null;
        public int? TrfNo { get; set; } = null;             // 사용 주차요금
        public int? DCNo { get; set; } = null;              // 사용 할인권
        public string UseTime { get; set; } = null;         // 사용 제한시간 (07:00~09:00)
        public string Memo { get; set; } = null;
        public string Reserve0 { get; set; } = null;
    }

    public enum DCInfoFields
    {
        ParkNo,
        DCNo,
        DCName,
        DCValue,
        DCType,
        DCBIndex,
        Reserve0
    }

    public class DCInfoRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? DCNo { get; set; } = null;
        public string DCName { get; set; } = null;
        public string DCValue { get; set; } = null;
        public int? DCType { get; set; } = null;
        public int? DCBIndex { get; set; } = null;
        public string Reserve0 { get; set; } = null;
    }

    public enum TariffFields
    {
        ParkNo,
        TrfNo,
        TrfName,
        MaxRates,
        DailyMaxRates,
        DailyCriterion,
        RoundType,
        RoundDecimal,
        WeekdaySetting,
        SaturdaySetting,
        SaturdayPrior,
        SundaySetting,
        SundayPrior,
        HolidaySetting,
        Reserve0
    }

    public class TariffRecord
    {
        public int? ParkNo { get; set; } = null;
        public int? TrfNo { get; set; } = null;             // 요금 체계 번호
        public string TrfName { get; set; } = null;         // 요금 체계 이름
        public int? MaxRates { get; set; } = null;          // 전체 최대 요금
        public int? DailyMaxRates { get; set; } = null;     // 일일 최대 요금
        public int? DailyCriterion { get; set; } = null;
        public int? RoundType { get; set; } = null;         // 반올림 방식, 0: 없음, 1: 올림, 2: 내름, 3: 반올림
        public int? RoundDecimal { get; set; } = null;      // 반올림 자릿수, 예: 100
        public int? WeekdaySetting { get; set; } = null;    // 평일 요금 설정, 0: 별도 설정, 4: 무료
        public int? SaturdaySetting { get; set; } = null;   // 토요일 요금 설정, 0: 별도 설정, 1: 평일 요금으로, 4: 무료
        public int? SaturdayPrior { get; set; } = null;     // 토요일 요금 우선순위 (0: 공휴일 요금 우선, 1: 토요일 요금 우선)
        public int? SundaySetting { get; set; } = null;     // 일요일 요금 설정, 0: 별도 설정, 1: 평일 요금으로, 2: 토요일 요금으로, 4: 무료
        public int? SundayPrior { get; set; } = null;       // 일요일 요금 우선순위 결정 (0: 공휴일 요금 우선, 1: 일요일 요금 우선)
        public int? HolidaySetting { get; set; } = null;    // 공휴일 요금 설정, 0: 공휴일 요금 별도 설정, 1: 평일 요금으로, 2: 토요일 요금으로, 3: 일요일 요금으로, 4: 무료
        public string Reserve0 { get; set; } = null;
    }

    public enum CarUserInfoFields
    {
        CarUserNo,
        CarNum,
        CarType,
        CarName,
        Name,
        TelNum,
        Comp,
        Dept,
        Address,
        Reserve0,
        Reserve1
    }

    public class CarUserInfoRecord
    {
        public int? CarUserNo { get; set; } = null;
        public string CarNum { get; set; } = null;
        public int? CarType { get; set; } = null;
        public string CarName { get; set; } = null;
        public string Name { get; set; } = null;
        public string TelNum { get; set; } = null;
        public string Comp { get; set; } = null;
        public string Dept { get; set; } = null;
        public string Address { get; set; } = null;
        public string Reserve0 { get; set; } = null;
        public string Reserve1 { get; set; } = null;
    }

    public enum CustGroupInfoFields
    {
        CustGroupInfoNo,
        CarUserNo,
        ParkNo,
        GroupNo,
        GroupType,
        StartDate,
        EndDate,
        CurrentState,
        DCNo,
        LimitDay,
        Memo,
        Reserve0,
        Reserve1
    }

    public class CustGroupInfoRecord
    {
        public int? CustGroupInfoNo { get; set; } = null;
        public int? CarUserNo { get; set; } = null;
        public int? ParkNo { get; set; } = null;
        public int? GroupNo { get; set; } = null;
        public int? GroupType { get; set; } = null;
        public DateTime? StartDate { get; set; } = null;
        public DateTime? EndDate { get; set; } = null;
        public int? CurrentState { get; set; } = null;
        public int? DCNo { get; set; } = null;
        public string LimitDay { get; set; } = null;
        public string Memo { get; set; } = null;
        public string Reserve0 { get; set; } = null;
        public string Reserve1 { get; set; } = null;
    }

    public enum CustGroupHistoryFields
    {
        CustGroupHistoryNo,
        CustGroupInfoNo,
        StartDate,
        EndDate,        
        CurrentState,
        IssueDate,
        LimitDay,
        Reserve0,
        Reserve1
    }

    public class CustGroupHistoryRecord
    {
        public int? CustGroupHistoryNo { get; set; } = null;
        public int? CustGroupInfoNo { get; set; } = null;        
        public DateTime? StartDate { get; set; } = null;
        public DateTime? EndDate { get; set; } = null;
        public int? CurrentState { get; set; } = null;
        public DateTime? IssueDate { get; set; } = null;
        public string LimitDay { get; set; } = null;
        public string Reserve0 { get; set; } = null;
        public string Reserve1 { get; set; } = null;
    }

    public enum CustGroupPayHistoryFields
    {
        CustGroupPayHistoryNo,
        CustGroupInfoNo,
        StartDate,
        EndDate,
        Amount,
        PayedDate,
        PayType,
        CurrentState,
        IssueDate,
        DCNo,
        LimitDay,
        Reserve0,
        Reserve1
    }

    public class CustGroupPayHistoryRecord
    {
        public int? CustGroupPayHistoryNo { get; set; } = null;
        public int? CustGroupInfoNo { get; set; } = null;
        public DateTime? StartDate { get; set; } = null;
        public DateTime? EndDate { get; set; } = null;
        public int? Amount { get; set; } = null;
        public DateTime? PayedDate { get; set; } = null;
        public int? PayType { get; set; } = null;        
        public int? CurrentState { get; set; } = null;
        public DateTime? IssueDate { get; set; } = null;
        public int? DCNo { get; set; } = null;
        public string Reserve0 { get; set; } = null;
        public string Reserve1 { get; set; } = null;
    }

    public enum CustomerFields
    {
        CarUserNo,
        CustGroupInfoNo,
        CarNum,
        CarType,
        CarName,
        Name,
        ParkNo,
        GroupNo,
        GroupType,
        StartDate,
        EndDate,
        CurrentState,
        DCNo,
        LimitDay
    }

    public class CustomerRecord
    {
        public int? CarUserNo { get; set; } = null;
        public int? CustGroupInfoNo { get; set; } = null;
        public string CarNum { get; set; } = null;
        public int? CarType { get; set; } = null;
        public string CarName { get; set; } = null;
        public string Name { get; set; } = null;
        public string TelNum { get; set; } = null;
        public string Comp { get; set; } = null;
        public string Dept { get; set; } = null;
        public string Address { get; set; } = null;
        public int? ParkNo { get; set; } = null;
        public int? GroupNo { get; set; } = null;
        public int? GroupType { get; set; } = null;
        public DateTime? StartDate { get; set; } = null;
        public DateTime? EndDate { get; set; } = null;
        public int? CurrentState { get; set; } = null;
        public int? DCNo { get; set; } = null;
        public string LimitDay { get; set; } = null;
        public string Memo { get; set; } = null;
    }
}
