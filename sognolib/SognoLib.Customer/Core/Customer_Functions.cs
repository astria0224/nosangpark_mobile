﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SognoLib.Customer.Core
{
    public class Customer_Functions
    {
        public static string GetCarTypeString(int? carType)
        {
            if (carType == Customer_Constant.CarType_CompactCar)
            {
                return "경차";
            }
            else if (carType == Customer_Constant.CarType_MidsizeCar)
            {
                return "일반";
            }
            else if (carType == Customer_Constant.CarType_BigsizeCar)
            {
                return "대형/화물";
            }
            else if (carType == Customer_Constant.CarType_Unknown)
            {
                return "알수없음";
            }

            return "";
        }
               
        public static string GetGroupTypeString(int? type)
        {
            if (type == Customer_Constant.GroupType_Free)
            {
                return "무료입출차";
            }
            else if (type == Customer_Constant.GroupType_Toll)
            {
                return "유료입출차";
            }
            else if (type == Customer_Constant.GroupType_BlackList)
            {
                return "블랙리스트";
            }

            return "";
        }

        public static string GetCurrentStateString(int? state)
        {
            if (state == Customer_Constant.CurrentState_New)
            {
                return "신규";
            }
            else if (state == Customer_Constant.CurrentState_Update)
            {
                return "수정";
            }
            else if (state == Customer_Constant.CurrentState_Extension)
            {
                return "연장";
            }
            else if (state == Customer_Constant.CurrentState_Close)
            {
                return "해지";
            }
            else if (state == Customer_Constant.CurrentState_Return)
            {
                return "환급";
            }
            else if (state == Customer_Constant.CurrentState_Delete)
            {
                return "삭제";
            }
            else if (state == Customer_Constant.CurrentState_End)
            {
                return "종료";
            }

            return "";
        }

        public static string GetPayTypeString(int? payType)
        {
            if (payType == Customer_Constant.PayType_Cash)
            {
                return "현금";
            }
            else if (payType == Customer_Constant.PayType_Card)
            {
                return "카드";
            }
            else if (payType == Customer_Constant.PayType_Bank)
            {
                return "계좌";
            }
            else if (payType == Customer_Constant.PayType_Return)
            {
                return "환급";
            }

            return "";
        }
    }
}
