﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SognoLib.Customer.Core
{
    public class Customer_Constant
    {
        public const string DefaultTitle = "정기 차량 관리 프로그램";

        public const int OneStop_ParkNo = -1;
        public const int FreePass_ParkNo = -2;

        // 차량 종류
        public const int CarType_Unknown = -1;
        public const int CarType_MidsizeCar = 0;    // 중형차, 일반
        public const int CarType_BigsizeCar = 1;    // 대형차        
        public const int CarType_CompactCar = 4;    // 경차

        // 그룹 타입
        public const int GroupType_Free = 1;        // 무료 출입 (정기권)
        public const int GroupType_Toll = 2;        // 요금 출입 (사전등록)
        public const int GroupType_FreePass = 3;    // 완전 무료 (관계자 차량)        
        public const int GroupType_LongTime = 4;    // 장기 주차        
        public const int GroupType_BlackList = 5;   
        

        // DC 타입
        public const int DCType_Price = 0;
        public const int DCType_Time = 1;
        public const int DCType_Percentage = 2;
        public const int DCType_TimePeriod = 3;

        // 결제 타입
        public const int PayType_Cash = 0;
        public const int PayType_Card = 1;
        public const int PayType_Bank = 2;
        public const int PayType_Return = 3;
        public const int PayType_OneStop = 4;

        // 현재 상태
        public const int CurrentState_New = 0;          // 신규
        public const int CurrentState_Update = 1;       // 연장 및 수정
        public const int CurrentState_Extension = 2;    // 연장 (사용 보류)
        public const int CurrentState_Close = 3;        // 해지
        public const int CurrentState_Return = 4;       // 환급
        public const int CurrentState_Delete = 5;       // 완전 삭제
        public const int CurrentState_End = 6;          // 종료 (종료일이 지난 것)
    }
}
