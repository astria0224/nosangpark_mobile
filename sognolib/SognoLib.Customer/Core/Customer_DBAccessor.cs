﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SognoLib.Core;

namespace SognoLib.Customer.Core
{
    public class Customer_DBAccessor
    {
        private static NLog.Logger _log = NLog.LogManager.GetCurrentClassLogger();

        private SogNo_DBManager dbManager = null;
        public Customer_DBAccessor(string ip, int port, string db)
        {
            try
            {
                SogNo_DBInfo dbInfo = new SogNo_DBInfo();
                dbInfo.DBIP = ip;
                dbInfo.DBPort = port;
                dbInfo.DBName = db;
                this.dbManager = new SogNo_DBManager(dbInfo);
            }
            catch
            {
                this.dbManager = null;
                throw;
            }
        }

        // **************** Park Info *********************
        public ParkInfoRecord[] SelectParkInfos(int? parkNo = null)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            if (parkNo != null)
            {
                where.Add(Tuple.Create(ParkInfoFields.ParkNo.ToString(), parkNo.ToString()));
            }

            string query = MakeSelectQuery(null, "AND", Tables.parkinfo, where);
            return this.dbManager.GetRecords<ParkInfoRecord>(query);
        }

        // **************** Tariff *********************
        public TariffRecord[] SelectTariffs(int? parkNo)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            where.Add(Tuple.Create(TariffFields.ParkNo.ToString(), parkNo.ToString()));

            string query = MakeSelectQuery(null, "AND", Tables.tariff, where);
            return this.dbManager.GetRecords<TariffRecord>(query);
        }

        // **************** Group Info *********************        
        public void InsertGroupInfo(GroupInfoRecord record)
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.groupinfo.ToString());
            }
        }

        public GroupInfoRecord[] SelectGroupInfos(int? parkNo = null, int? groupNo = null, int? groupType = null)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            if (parkNo != null)
            {
                where.Add(Tuple.Create(GroupInfoFields.ParkNo.ToString(), parkNo.ToString()));
            }
            if (groupNo != null)
            {
                where.Add(Tuple.Create(GroupInfoFields.GroupNo.ToString(), groupNo?.ToString()));
            }
            if (groupType != null)
            {
                where.Add(Tuple.Create(GroupInfoFields.GroupType.ToString(), groupType?.ToString()));
            }

            string query = MakeSelectQuery(null, "AND", Tables.groupinfo, where);
            return this.dbManager.GetRecords<GroupInfoRecord>(query);
        }

        public bool CanDeleteGroupInfo(int parkNo, int groupNo)
        {
            string query = string.Format("SELECT COUNT(*) from `custinfo` WHERE `ParkNo`=\'{0}\' AND `GroupNo`=\'{1}\'", parkNo, groupNo);

            int count = this.dbManager.Count(query);

            return count == 0;
        }

        public void DeleteGroupInfo(GroupInfoRecord groupInfo)
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    GroupInfoFields.ParkNo.ToString(),
                    GroupInfoFields.GroupNo.ToString()
                };

                this.dbManager.Delete(groupInfo, Tables.groupinfo.ToString(), whereFields);
            }
        }

        public void UpdateGroupInfo(GroupInfoRecord groupInfo)
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    GroupInfoFields.ParkNo.ToString(),
                    GroupInfoFields.GroupNo.ToString()
                };
                string[] setFields = {
                    GroupInfoFields.GroupName.ToString(),
                    GroupInfoFields.GroupType.ToString(),
                    GroupInfoFields.Amount.ToString(),
                    GroupInfoFields.TrfNo.ToString(),
                    GroupInfoFields.DCNo.ToString(),
                    GroupInfoFields.UseTime.ToString(),
                    GroupInfoFields.Memo.ToString()
                };

                this.dbManager.Update(groupInfo, Tables.groupinfo.ToString(), setFields, whereFields);
            }
        }


        // **************** DC Info *********************
        public DCInfoRecord[] SelectDCInfos(int? parkNo, int? dcType = null, int? dcValue = null)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            if (parkNo != null)
            {
                where.Add(Tuple.Create(DCInfoFields.ParkNo.ToString(), parkNo.ToString()));
            }
            if (dcType != null)
            {
                where.Add(Tuple.Create(DCInfoFields.DCType.ToString(), dcType?.ToString()));
            }
            if (dcValue != null)
            {
                where.Add(Tuple.Create(DCInfoFields.DCValue.ToString(), dcValue?.ToString()));
            }
            
            string query = MakeSelectQuery(null, "AND", Tables.dcinfo, where);
            return this.dbManager.GetRecords<DCInfoRecord>(query);
        }


        // **************** Car User Info *********************
        public void InsertCarUserInfo(CarUserInfoRecord record)
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.caruserinfo.ToString());
            }
        }

        public CarUserInfoRecord[] SelectCarUserInfos(string carNum)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            if (carNum != null)
            {
                where.Add(Tuple.Create(CarUserInfoFields.CarNum.ToString(), carNum.ToString()));
            }

            string query = MakeSelectQuery(null, "AND", Tables.caruserinfo, where);
            return this.dbManager.GetRecords<CarUserInfoRecord>(query);
        }

        public void UpdateCarUserInfo(CarUserInfoRecord record)
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    CarUserInfoFields.CarUserNo.ToString(),
                };
                string[] setFields = {
                    CarUserInfoFields.CarNum.ToString(),
                    CarUserInfoFields.CarType.ToString(),
                    CarUserInfoFields.CarName.ToString(),
                    CarUserInfoFields.Name.ToString(),
                    CarUserInfoFields.TelNum.ToString(),
                    CarUserInfoFields.Comp.ToString(),
                    CarUserInfoFields.Dept.ToString(),
                    CarUserInfoFields.Address.ToString()
                };

                this.dbManager.Update(record, Tables.caruserinfo.ToString(), setFields, whereFields);
            }
        }

        public bool CanInsertCarUserInfo(string carNumber)
        {   
            string query = string.Format("SELECT COUNT(*) FROM `{0}` WHERE `CarNum`=\'{1}\'", Tables.caruserinfo, carNumber);

            int count = this.dbManager.Count(query);

            return count == 0;
        }

        public void UpdateCarUser()
        {

        }

        // **************** Customer Group Info *********************

        public void InsertCustGroupInfo(CustGroupInfoRecord record)
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.custgroupinfo.ToString());
            }
        }

        public void UpdateCustGroupInfo(CustGroupInfoRecord record)
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    CustGroupInfoFields.CustGroupInfoNo.ToString(),
                    CustGroupInfoFields.CarUserNo.ToString(),                    
                };
                string[] setFields = {
                    CustGroupInfoFields.GroupType.ToString(),
                    CustGroupInfoFields.ParkNo.ToString(),
                    CustGroupInfoFields.GroupNo.ToString(),                    
                    CustGroupInfoFields.StartDate.ToString(),
                    CustGroupInfoFields.EndDate.ToString(),
                    CustGroupInfoFields.CurrentState.ToString(),
                    CustGroupInfoFields.DCNo.ToString(),
                    CustGroupInfoFields.LimitDay.ToString(),
                    CustGroupInfoFields.Memo.ToString()
                };

                this.dbManager.Update(record, Tables.custgroupinfo.ToString(), setFields, whereFields);
            }
        }

        public CustomerRecord[] SelectCustGroupInfos(int? parkNo, int? carUserNo, int? groupNo)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            if (parkNo != null)
            {
                where.Add(Tuple.Create("`B`.`ParkNo`", parkNo?.ToString()));
            }
            if (carUserNo != null)
            {
                where.Add(Tuple.Create("`A`.`CarUserNo`", carUserNo?.ToString()));
            }
            if (groupNo != null)
            {
                where.Add(Tuple.Create("`B`.`GroupNo`", groupNo?.ToString()));
            }

            string query = MakeSelectQuery("SELECT `A`.`CarUserNo`, `B`.`CustGroupInfoNo`, `A`.`CarNum`, `A`.`CarName`,`A`.`CarType`, `A`.`Name`, `B`.`ParkNo`, `B`.`GroupNo`, `B`.`GroupType`, `B`.`StartDate`, `B`.`EndDate`, `B`.`CurrentState`, `B`.`DCNo`, `B`.`LimitDay`" +
                " FROM caruserinfo AS A LEFT JOIN custgroupinfo AS B ON `A`.`CarUserNo` = `B`.`CarUserNo`", "AND", Tables.custgroupinfo, where);
            return this.dbManager.GetRecords<CustomerRecord>(query);
        }

        public bool CanInsertCustGroupInfo(int parkNo, string carNumber, int groupNo)
        {            
            string query = string.Format("SELECT COUNT(*) FROM `{0}` AS A LEFT JOIN `{1}` AS B ON `A`.`CarUserNo` = `B`.`CarUserNo` WHERE `B`.`ParkNo`=\'{2}\' AND `A`.`CarNum` LIKE \'%{3}%\' AND `B`.`GroupNo`=\'{4}\' AND `B`.CurrentState != " + Customer_Constant.CurrentState_Delete,
                Tables.caruserinfo, Tables.custgroupinfo, parkNo, carNumber, groupNo);

            int count = this.dbManager.Count(query);

            return count == 0;
        }

        public CustomerRecord[] SearchCustomerInfos(int? parkNo, int? groupNo, int? groupType, int? state, string name, string carNumber, string stateQuery = null)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            string query = string.Format("SELECT `A`.`CarUserNo`, `B`.`CustGroupInfoNo`, `A`.`CarNum`, `A`.`CarName`, `A`.`CarType`, `A`.`Name`, `A`.`TelNum`, `A`.`Comp`, `A`.`Dept`, `A`.`Address`, `B`.`ParkNo`, `B`.`GroupNo`, `B`.`GroupType`, `B`.`StartDate`, `B`.`EndDate`, `B`.`CurrentState`, `B`.`DCNo`, `B`.`LimitDay`, `B`.`Memo` FROM caruserinfo AS A LEFT JOIN custgroupinfo AS B ON `A`.`CarUserNo` = `B`.`CarUserNo` WHERE");

           
            query += string.Format(" `{0}`=\'{1}\'", CustomerFields.GroupType, groupType);
            
            if (parkNo != null)
            {
                query += string.Format(" AND `{0}`=\'{1}\'", CustomerFields.ParkNo, parkNo);
            }

            if (groupNo != null)
            {
                query += string.Format(" AND `{0}`=\'{1}\'", CustomerFields.GroupNo, groupNo);
            }

            if (state != null)
            {
                query += string.Format(" AND `{0}`=\'{1}\'", CustomerFields.CurrentState, state);
            }

            if (name != null && name.Length > 0)
            {
                query += string.Format(" AND `{0}`=\'{1}\'", CustomerFields.Name, name);
            }

            if (carNumber != null && carNumber.Length > 0)
            {
                query += string.Format(" AND `{0}` LIKE \'%{1}%\'", CustomerFields.CarNum, carNumber);
            }

            if (stateQuery != null)
            {
                query += stateQuery;
            }

            return this.dbManager.GetRecords<CustomerRecord>(query);
        }

        public CustomerRecord[] SearchCustomerInfos(int? parkNo, string carNumber)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            string query = string.Format("SELECT `A`.`CarUserNo`, `B`.`CustGroupInfoNo`, `A`.`CarNum`, `A`.`CarName`, `A`.`CarType`, `A`.`Name`, `A`.`TelNum`, `A`.`Comp`, `A`.`Dept`, `A`.`Address`, `B`.`ParkNo`, `B`.`GroupNo`, `B`.`GroupType`, `B`.`StartDate`, `B`.`EndDate`, `B`.`CurrentState`, `B`.`DCNo`, `B`.`LimitDay`, `B`.`Memo` FROM caruserinfo AS A LEFT JOIN custgroupinfo AS B ON `A`.`CarUserNo` = `B`.`CarUserNo` WHERE");            
            query += string.Format(" `{0}`=\'{1}\'", CustomerFields.ParkNo, parkNo);

            if (carNumber != null && carNumber.Length > 0)
            {
                query += string.Format(" AND `{0}` LIKE \'%{1}%\'", CustomerFields.CarNum, carNumber);
            }

            return this.dbManager.GetRecords<CustomerRecord>(query);
        }


        // **************** Customer Group History *********************

        public void InsertCustGroupHistory(CustGroupHistoryRecord record)
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.custgrouphistory.ToString());
            }
        }

        public CustGroupHistoryRecord[] SelectCustGroupHistory(int custGroupInfoNo)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            where.Add(Tuple.Create(CustGroupHistoryFields.CustGroupInfoNo.ToString(), custGroupInfoNo.ToString()));

            string query = MakeSelectQuery(null, "AND", Tables.custgrouphistory, where) + "ORDER BY IssueDate DESC";
            return this.dbManager.GetRecords<CustGroupHistoryRecord>(query);
        }


        // **************** Customer Group Pay History *********************

        public void InsertCustGroupPayHistory(CustGroupPayHistoryRecord record)
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.custgrouppayhistory.ToString());
            }
        }

        public CustGroupPayHistoryRecord[] SelectCustGroupPayHistory(int custGroupInfoNo)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            where.Add(Tuple.Create(CustGroupPayHistoryFields.CustGroupInfoNo.ToString(), custGroupInfoNo.ToString()));

            string query = MakeSelectQuery(null, "AND", Tables.custgrouppayhistory, where) +" AND (" + CustGroupHistoryFields.CurrentState.ToString() + " != " + Customer_Constant.CurrentState_Delete + ") ORDER BY IssueDate DESC";
            return this.dbManager.GetRecords<CustGroupPayHistoryRecord>(query);
        }

        public void UpdateCustGroupPayHistory(CustGroupPayHistoryRecord record)
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    CustGroupPayHistoryFields.CustGroupPayHistoryNo.ToString(),
                    CustGroupPayHistoryFields.CustGroupInfoNo.ToString()
                };
                string[] setFields = {
                    CustGroupPayHistoryFields.StartDate.ToString(),
                    CustGroupPayHistoryFields.EndDate.ToString(),
                    CustGroupPayHistoryFields.Amount.ToString(),
                    CustGroupPayHistoryFields.PayedDate.ToString(),
                    CustGroupPayHistoryFields.PayType.ToString(),
                    CustGroupPayHistoryFields.CurrentState.ToString(),
                    CustGroupPayHistoryFields.IssueDate.ToString(),
                    CustGroupPayHistoryFields.DCNo.ToString(),
                    CustGroupPayHistoryFields.Reserve0.ToString(),
                    CustGroupPayHistoryFields.Reserve1.ToString()
                };

                this.dbManager.Update(record, Tables.custgrouppayhistory.ToString(), setFields, whereFields);
            }
        }

        public T[] MergeRecords<T>(T[] a, T[] b) where T : new()
        {
            T[] mergeArray = new T[a.Length + b.Length];                         
            a.CopyTo(mergeArray, 0);
            b.CopyTo(mergeArray, a.Length);
            return mergeArray;
        }


        private string MakeSelectQuery(string query, string opt, Tables table, List<Tuple<string, string>> where)
        {
            if (query == null)
            {
                query = string.Format("SELECT * FROM `{0}`", table);
            }
            //string query = string.Format("SELECT * FROM `{0}`", table);

            if (where != null && where.Count > 0)
            {
                for (int i = 0; i < where.Count; i++)
                {
                    if (i == 0)
                    {
                        query += " WHERE ";
                    }
                    else
                    {
                        query += " "+ opt +" ";
                    }

                    if (query == null)
                    {
                        if (where[i].Item2 == null)
                        {
                            query += string.Format("`{0}` is null", where[i].Item1);
                        }
                        else
                        {
                            query += string.Format("`{0}`=\'{1}\'", where[i].Item1, where[i].Item2);
                        }
                    }
                    else
                    {
                        if (where[i].Item2 == null)
                        {
                            query += string.Format("{0} is null", where[i].Item1);
                        }
                        else
                        {
                            query += string.Format("{0}=\'{1}\'", where[i].Item1, where[i].Item2);
                        }
                    }
                }
            }

            return query;
        }
    }
}
