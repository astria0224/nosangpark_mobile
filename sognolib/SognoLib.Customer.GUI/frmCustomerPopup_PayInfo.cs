﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SognoLib.Core;
using SognoLib.Customer.Core;
using SognoLib.Customer.GUI;

namespace SognoLib.Customer.GUI
{
    public partial class frmCustomerPopup_PayInfo : SognoLib_CustomerPayInfoViewer
    {
        public Customer_DBAccessor parkDB = null;
        public CustomerRecord customerRecord = null;
        public CustGroupPayHistoryRecord[] custGroupPayHistoryRecord = null;
        public frmCustomerPopup_SE mainFrm = null;

        class PayType
        {
            public int? TypeID { get; set; } = null;
            public string TypeName { get; set; } = null;

            public PayType(int typeID, string typeName)
            {
                this.TypeID = typeID;
                this.TypeName = typeName;
            }
        }
        PayType[] payTypes = null;

        public frmCustomerPopup_PayInfo(Customer_DBAccessor parkDB, frmCustomerPopup_SE main)
        {
            this.payTypes = new PayType[]
            {
                new PayType(Customer_Constant.PayType_Cash, Customer_Functions.GetPayTypeString(Customer_Constant.PayType_Cash)),
                new PayType(Customer_Constant.PayType_Card, Customer_Functions.GetPayTypeString(Customer_Constant.PayType_Card)),
                new PayType(Customer_Constant.PayType_Bank, Customer_Functions.GetPayTypeString(Customer_Constant.PayType_Bank)),
                new PayType(Customer_Constant.PayType_Return, Customer_Functions.GetPayTypeString(Customer_Constant.PayType_Return))
            };

            this.parkDB = parkDB;
            this.mainFrm = main;
            this.InitializeCustomerPayInfo();

            this.lviewPayInfoList.ItemSelectionChanged += OnCustomerPayInfoListSelectChanged;
            this.dtpPayInfo_StartDate.ValueChanged += OnCustomerPayInfoStartDateChanged;
            this.dtpPayInfo_EndDate.ValueChanged += OnCustomerPayInfoEndDateChanged;
            this.cboxPayInfo_PayType.SelectedIndexChanged += OnCustomerPayInfoPayTypeChanged;
            this.tboxPayInfo_ReturnAmount.TextChanged += OnCustomerPayInfoReturnAmountChanged;

            this.btnPayInfo_Clear.MouseClick += OnClearCustomerPayInfoButtonClicked;
            this.btnPayInfo_Create.MouseClick += OnCreateCustomerPayInfoButtonClicked;
            this.btnPayInfo_Delete.MouseClick += OnDeleteCustomerPayInfoButtonClicked;
            this.btnPayInfo_Update.MouseClick += OnUpdateCustomerPayInfoButtonClicked;

            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CustomerPayInfoViewer_FormClosing);
        }

        private void InitializeCustomerPayInfo()
        {
            this.lviewPayInfoList.Columns.Add("No", 70, HorizontalAlignment.Center);
            this.lviewPayInfoList.Columns.Add("시작일", 180, HorizontalAlignment.Center);
            this.lviewPayInfoList.Columns.Add("종료일", 180, HorizontalAlignment.Center);
            this.lviewPayInfoList.Columns.Add("결제 요금", 130, HorizontalAlignment.Center);
            this.lviewPayInfoList.Columns.Add("결제 형식", 90, HorizontalAlignment.Center);

            this.cboxPayInfo_PayType.BindingContext = new BindingContext();
            this.cboxPayInfo_PayType.DataSource = this.payTypes;
            this.cboxPayInfo_PayType.DisplayMember = "TypeName";
            this.cboxPayInfo_PayType.ValueMember = "TypeID";
            this.cboxPayInfo_PayType.SelectedIndex = -1;

            this.btnPayInfo_Delete.Enabled = false;
            this.btnPayInfo_Update.Enabled = false;
        }

        private void OnClearCustomerPayInfoButtonClicked(object sender, MouseEventArgs args)
        {
            this.lviewPayInfoList.SelectedIndices.Clear();

            this.cboxPayInfo_PayType.BackColor = Color.White;
            this.tboxPayInfo_Amount.BackColor = Color.White;

            this.dtpPayInfo_StartDate.Value = this.TranslateDate(this.dtpPayInfo_StartDate); //new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0); ;
            this.dtpPayInfo_EndDate.Value = this.TranslateDate(this.dtpPayInfo_EndDate); //new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59); ;

            this.tboxPayInfo_Amount.Text = "";
            this.tboxPayInfo_Memo.Text = "";

            this.cboxPayInfo_PayType.SelectedIndex = -1;
        }

        private void OnCustomerPayInfoPayTypeChanged(object sender, EventArgs args)
        {
            this.tboxPayInfo_Amount.Text = "";
            this.tboxPayInfo_ReturnAmount.Text = "";

            if(this.cboxPayInfo_PayType.SelectedValue as int? == Customer_Constant.PayType_Return)
            {
                this.label_Amount.Enabled = false;
                this.tboxPayInfo_Amount.Enabled = false;

                this.label_ReturnAmount.Enabled = true;
                this.tboxPayInfo_ReturnAmount.Enabled = true;
            }
            else
            {
                this.label_Amount.Enabled = true;
                this.tboxPayInfo_Amount.Enabled = true;

                this.label_ReturnAmount.Enabled = false;
                this.tboxPayInfo_ReturnAmount.Enabled = false;
            }
        }

        private void OnCustomerPayInfoReturnAmountChanged(object sender, EventArgs args)
        {
            try
            {
                this.tboxPayInfo_Amount.Text = (-Convert.ToInt32(this.tboxPayInfo_ReturnAmount.Text)).ToString();
            }
            catch
            {
                this.tboxPayInfo_Amount.Text = "";
            }
        }

        private void OnCreateCustomerPayInfoButtonClicked(object sender, MouseEventArgs args)
        {
            if (CheckCustomerPayInfoForms())
            {
                CustGroupPayHistoryRecord record = new CustGroupPayHistoryRecord();
                this.ReadCustomerPayInfo(record);
                record.CurrentState = Customer_Constant.CurrentState_New;
                this.parkDB.InsertCustGroupPayHistory(record);

                this.RefreshCustomerPayInfoList(this.customerRecord);
                this.mainFrm.OnUpdateCustomerButtonClicked(null, null);
            }
        }

        private void OnDeleteCustomerPayInfoButtonClicked(object sender, MouseEventArgs args)
        {
            CustGroupPayHistoryRecord record = new CustGroupPayHistoryRecord();
            this.ReadCustomerPayInfo(record);
            int payHistoryNo = Convert.ToInt32(this.lviewPayInfoList.SelectedItems[0].SubItems[0].Text);
            record.CurrentState = Customer_Constant.CurrentState_Delete;
            record.CustGroupPayHistoryNo = payHistoryNo;
            this.parkDB.UpdateCustGroupPayHistory(record);
            
            this.RefreshCustomerPayInfoList(this.customerRecord);

            if (this.lviewPayInfoList.SelectedIndices[0] == 0)
            {
                this.mainFrm.OnUpdateCustomerButtonClicked(null, null);
            }
        }

        private void OnUpdateCustomerPayInfoButtonClicked(object sender, MouseEventArgs args)
        {
            CustGroupPayHistoryRecord record = new CustGroupPayHistoryRecord();
            this.ReadCustomerPayInfo(record);
            int payHistoryNo = Convert.ToInt32(this.lviewPayInfoList.SelectedItems[0].SubItems[0].Text);
            record.CurrentState = Customer_Constant.CurrentState_Update;
            record.CustGroupPayHistoryNo = payHistoryNo;
            this.parkDB.UpdateCustGroupPayHistory(record);
            
            this.RefreshCustomerPayInfoList(this.customerRecord);

            if (this.lviewPayInfoList.SelectedIndices[0] == 0)
            {
                this.mainFrm.OnUpdateCustomerButtonClicked(null, null);
            }
        }

        private void ReadCustomerPayInfo(CustGroupPayHistoryRecord custGroupPayHistoryRecord)
        {
            custGroupPayHistoryRecord.CustGroupInfoNo = this.customerRecord.CustGroupInfoNo;
            custGroupPayHistoryRecord.StartDate = this.TranslateDate(this.dtpPayInfo_StartDate); //new DateTime(this.dtpPayInfo_StartDate.Value.Year, this.dtpPayInfo_StartDate.Value.Month, this.dtpPayInfo_StartDate.Value.Day, 0, 0, 0);
            custGroupPayHistoryRecord.EndDate = this.TranslateDate(this.dtpPayInfo_EndDate); // new DateTime(this.dtpPayInfo_EndDate.Value.Year, this.dtpPayInfo_EndDate.Value.Month, this.dtpPayInfo_EndDate.Value.Day, 23, 59, 59);
            //custGroupPayHistoryRecord.Amount = Int32.Parse(this.tboxPayInfo_Amount.Text, System.Globalization.NumberStyles.AllowThousands);
            custGroupPayHistoryRecord.Amount = Convert.ToInt32(this.tboxPayInfo_Amount.Text.Replace(",", ""));
            custGroupPayHistoryRecord.PayedDate = DateTime.Now;
            custGroupPayHistoryRecord.PayType = this.cboxPayInfo_PayType.SelectedValue as int?;
            custGroupPayHistoryRecord.IssueDate = DateTime.Now;
        }

        private bool CheckCustomerPayInfoForms()
        {
            bool retval = true;

            this.cboxPayInfo_PayType.BackColor = Color.White;
            this.tboxPayInfo_Amount.BackColor = Color.White;
            this.tboxPayInfo_ReturnAmount.BackColor = Color.White;

            if (this.cboxPayInfo_PayType.SelectedIndex < 0)
            {
                this.cboxPayInfo_PayType.BackColor = Color.LightPink;
                retval = false;
            }

            if (this.tboxPayInfo_Amount.Text.Length == 0)
            {
                this.tboxPayInfo_Amount.BackColor = Color.LightPink;
                retval = false;
            }

            if ((this.cboxPayInfo_PayType.SelectedValue as int? == Customer_Constant.PayType_Return) && this.tboxPayInfo_ReturnAmount.Text.Length == 0)
            {
                this.tboxPayInfo_ReturnAmount.BackColor = Color.LightPink;
                retval = false;
            }

            return retval;
        }

        public void RefreshCustomerPayInfoList(CustomerRecord customerRecord)
        {
            //this.customerRecord = customerRecord;
            this.customerRecord = this.parkDB.SelectCustGroupInfos(customerRecord.ParkNo, customerRecord.CarUserNo, customerRecord.GroupNo)[0];
            this.gbox_PayInfo.Text = this.customerRecord.CarNum + "차량 정기권 결제 관리";
            this.lviewPayInfoList.Items.Clear();

            int total = 0;
            this.custGroupPayHistoryRecord = this.parkDB.SelectCustGroupPayHistory((int)this.customerRecord.CustGroupInfoNo);

            foreach (var items in this.custGroupPayHistoryRecord)
            {
                ListViewItem item = new ListViewItem(items.CustGroupPayHistoryNo.ToString());
                item.SubItems.Add(string.Format("{0:yyy-MM-dd HH:mm:ss}", items.StartDate));
                item.SubItems.Add(string.Format("{0:yyy-MM-dd HH:mm:ss}", items.EndDate));
                item.SubItems.Add(string.Format("{0:#,###}", items.Amount));
                total += Int32.Parse(items.Amount.ToString());

                PayType payType = Array.Find(this.payTypes, it => it.TypeID == items.PayType);
                item.SubItems.Add(payType.TypeName);

                this.lviewPayInfoList.Items.Add(item);
            }

            this.label_PayInfoTotal.Text = string.Format("합계: {0:#,###} 원", total);

            if (this.lviewPayInfoList.Items.Count > 0)
            {
                this.lviewPayInfoList.Items[0].Focused = true;
                this.lviewPayInfoList.Items[0].Selected = true;
            }
        }

        private void RefreshCustomerPayInfo(CustGroupPayHistoryRecord record)
        {
            this.dtpPayInfo_StartDate.Value = (DateTime)record.StartDate;
            this.dtpPayInfo_EndDate.Value = (DateTime)record.EndDate;

            this.cboxPayInfo_PayType.SelectedValue = record.PayType;

            this.tboxPayInfo_Amount.Text = string.Format("{0:#,###}", record.Amount);
        }

        private void OnCustomerPayInfoStartDateChanged(object sender, EventArgs args)
        {
            this.mainFrm.dtpCustomer_StartDate.Value = this.dtpPayInfo_StartDate.Value;
        }

        private void OnCustomerPayInfoEndDateChanged(object sender, EventArgs args)
        {
            this.mainFrm.dtpCustomer_EndDate.Value = this.dtpPayInfo_EndDate.Value;
        }

        private void CustomerPayInfoViewer_FormClosing(object sender, FormClosingEventArgs e)
        {
            //this.custGroupPayHistoryRecord = null;
            this.mainFrm.payInfoViewer = null;
        }

        private void OnCustomerPayInfoListSelectChanged(object sender, ListViewItemSelectionChangedEventArgs args)
        {
            if (args.IsSelected)
            {
                this.RefreshCustomerPayInfo(this.custGroupPayHistoryRecord[SearchIndex()]);

                this.btnPayInfo_Clear.Enabled = true;
                this.btnPayInfo_Create.Enabled = false;
                this.btnPayInfo_Update.Enabled = true;
                this.btnPayInfo_Delete.Enabled = true;

                //if (this.lviewPayInfoList.SelectedIndices[0] == 0)
                //{
                //    this.label_ReturnAmount.Enabled = true;
                //    this.tboxPayInfo_ReturnAmount.Enabled = true;
                //}
                //bool[] enabledArray = new bool[] { true, false, true, true };
                //this.TriggerEnalbed(enabledArray);
            }
            else
            {
                this.btnPayInfo_Clear.Enabled = true;
                this.btnPayInfo_Create.Enabled = true;
                this.btnPayInfo_Update.Enabled = false;
                this.btnPayInfo_Delete.Enabled = false;

                //this.label_ReturnAmount.Enabled = false;
                //this.tboxPayInfo_ReturnAmount.Enabled = false;
                //bool[] enabledArray = new bool[] { true, true, false, false };
                //this.TriggerEnalbed(enabledArray);
            }
        }

        private void TriggerEnalbed(bool[] arr)
        {
            this.btnPayInfo_Clear.Enabled = arr[0];
            this.btnPayInfo_Create.Enabled = arr[1];
            this.btnPayInfo_Update.Enabled = arr[2];
            this.btnPayInfo_Delete.Enabled = arr[3];
        }

        private DateTime TranslateDate(DateTimePicker dateTimePicker)
        {
            DateTime dateTime;

            if (dateTimePicker.Tag.ToString() == "start")
            {
                dateTime = new DateTime(this.dtpPayInfo_StartDate.Value.Year, this.dtpPayInfo_StartDate.Value.Month, this.dtpPayInfo_StartDate.Value.Day, 0, 0, 0);
            }
            else
            {
                dateTime = new DateTime(this.dtpPayInfo_EndDate.Value.Year, this.dtpPayInfo_EndDate.Value.Month, this.dtpPayInfo_EndDate.Value.Day, 23, 59, 59);
            }

            return dateTime;
        }

        private int SearchIndex()
        {
            string tmpCustPayNo = this.lviewPayInfoList.SelectedItems[0].SubItems[0].Text;
            return Array.FindIndex(this.custGroupPayHistoryRecord, it => it.CustGroupPayHistoryNo == Convert.ToInt32(tmpCustPayNo));
        }
    }
}
