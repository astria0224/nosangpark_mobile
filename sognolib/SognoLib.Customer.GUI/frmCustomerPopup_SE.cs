﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using SognoLib.Core;
using SognoLib.Customer.Core;
using SognoLib.Customer.GUI;

namespace SognoLib.Customer.GUI
{
    public partial class frmCustomerPopup_SE : SognoLib_CustomerViewer_SE
    {
        static NLog.Logger _log = NLog.LogManager.GetCurrentClassLogger();
        private SogNo_InIManager iniManager = new SogNo_InIManager(AppDomain.CurrentDomain.FriendlyName + ".ini");

        public Customer_DBAccessor parkDB = null;
        public frmCustomerPopup_PayInfo payInfoViewer = null;

        public string ip;
        public int port;
        public string dbName;

        public ParkInfoRecord[] parkInfos = null;
        public GroupInfoRecord[] groupInfos = null;
        public GroupInfoRecord[] groupInfosTotal = null;
        TariffRecord[] tariffs = null;
        DCInfoRecord[] dcInfos = null;

        ParkInfoRecord selectedPark = null;
        GroupInfoRecord selectedGroup = null;
        SearchState selectedState = null;
        CustomerRecord[] searchedCustomer = null;
        CustGroupHistoryRecord[] custHistoryInfo = null;

        public bool startDateChange = false;

        class CurrentState
        {
            public int? TypeID { get; set; } = null;
            public string TypeName { get; set; } = null;

            public CurrentState(int? typeID, string typeName)
            {
                this.TypeID = typeID;
                this.TypeName = typeName;
            }
        }
        CurrentState[] currentState = null;

        class SearchState
        {
            public string TypeName { get; set; } = null;
            public string TypeQuery { get; set; } = null;

            public SearchState(string typeName, string typeQuery)
            {               
                this.TypeName = typeName;
                this.TypeQuery = typeQuery;
            }
        }
        SearchState[] searchState = null;

        class CarType
        {
            public int? TypeID { get; set; } = null;
            public string TypeName { get; set; } = null;

            public CarType(int typeID, string typeName)
            {
                this.TypeID = typeID;
                this.TypeName = typeName;
            }
        }
        CarType[] carTypes = null;

        class GroupType
        {
            public int? TypeID { get; set; } = null;
            public string TypeName { get; set; } = null;

            public GroupType(int typeID, string typeName)
            {
                this.TypeID = typeID;
                this.TypeName = typeName;
            }
        }
        GroupType[] groupTypes = null;

        public frmCustomerPopup_SE(string ip, int port, string dbName)
        {
            this.currentState = new CurrentState[]
            {
                new CurrentState(Customer_Constant.CurrentState_New, Customer_Functions.GetCurrentStateString(Customer_Constant.CurrentState_New)),
                new CurrentState(Customer_Constant.CurrentState_Update, Customer_Functions.GetCurrentStateString(Customer_Constant.CurrentState_Update)),              
                new CurrentState(Customer_Constant.CurrentState_Delete, Customer_Functions.GetCurrentStateString(Customer_Constant.CurrentState_Delete))
            };

            this.searchState = new SearchState[]
            {
                new SearchState("전체", null),
                new SearchState("사용중", string.Format(" AND (`{0}`={1} OR `{0}`={2}) AND `{3}`>=now()", CustomerFields.CurrentState, Customer_Constant.CurrentState_New,  Customer_Constant.CurrentState_Update, CustomerFields.EndDate)),
                new SearchState("종료", string.Format(" AND (`{0}`={1} OR `{0}`={2} OR `{0}`={3} OR `{0}`={4} OR `{5}`<now())", CustomerFields.CurrentState, Customer_Constant.CurrentState_Return, Customer_Constant.CurrentState_Close, Customer_Constant.CurrentState_Delete, Customer_Constant.CurrentState_End, CustomerFields.EndDate)),
                new SearchState("1주내 종료", string.Format(" AND (now() <=`{0}` AND `{0}`<=date_add(now(),interval +1 week))", CustomerFields.EndDate)),
                new SearchState("2주내 종료", string.Format(" AND (now() <=`{0}` AND `{0}`<=date_add(now(),interval +2 week))", CustomerFields.EndDate))
            };

            this.carTypes = new CarType[]
            {
                new CarType(Customer_Constant.CarType_MidsizeCar, Customer_Functions.GetCarTypeString(Customer_Constant.CarType_MidsizeCar)),
                new CarType(Customer_Constant.CarType_CompactCar, Customer_Functions.GetCarTypeString(Customer_Constant.CarType_CompactCar)),
                new CarType(Customer_Constant.CarType_BigsizeCar, Customer_Functions.GetCarTypeString(Customer_Constant.CarType_BigsizeCar)),
                new CarType(Customer_Constant.CarType_Unknown, Customer_Functions.GetCarTypeString(Customer_Constant.CarType_Unknown))
            };

            this.groupTypes = new GroupType[]
            {
                new GroupType(Customer_Constant.GroupType_Free, Customer_Functions.GetGroupTypeString(Customer_Constant.GroupType_Free)),
                new GroupType(Customer_Constant.GroupType_Toll, Customer_Functions.GetGroupTypeString(Customer_Constant.GroupType_Toll)),
                new GroupType(Customer_Constant.GroupType_FreePass, Customer_Functions.GetGroupTypeString(Customer_Constant.GroupType_FreePass)),
                new GroupType(Customer_Constant.GroupType_LongTime, Customer_Functions.GetGroupTypeString(Customer_Constant.GroupType_LongTime)),
                new GroupType(Customer_Constant.GroupType_BlackList, Customer_Functions.GetGroupTypeString(Customer_Constant.GroupType_BlackList))
            };

            this.CustomerViewer_LoadSettings();

            this.ip = ip;
            this.port = port;
            this.dbName = dbName;
            this.CustomerViewer_InitDB(this.ip, this.port, this.dbName);

            this.CustomerViewer_Init();
            this.InitializeCustomerList();
            this.InitializeCustomerInfo();
            this.InitializeGroupInfo();

            this.btnSearch_Run.MouseClick += OnRunSearchButtonClicked;
            this.btnSearch_Clear.MouseClick += OnClearSearchButtonClicked;
            this.tboxSearch_CarNum.KeyDown += Enter_KeyDown;
            this.tboxSearch_Name.KeyDown += Enter_KeyDown;
            this.cboxSearch_Group.SelectedIndexChanged += cboxSearchGroupChanged;
            this.cboxSearch_ParkName.SelectedIndexChanged += cboxSearchParkNameChanged;
            this.cboxSearch_State.SelectedIndexChanged += cboxSearchStateChanged;

            this.lviewCustList.ItemSelectionChanged += OnCustomerListSelectChanged;
            this.lviewCustList.ColumnClick += lvUp_ColumnClick;

            this.btnGroup_Clear.MouseClick += OnClearGroupButtonClicked;
            this.btnGroup_Create.MouseClick += OnCreateGroupButtonClicked;
            this.btnGroup_Delete.MouseClick += OnDeleteGroupButtonClicked;
            this.btnGroup_Update.MouseClick += OnUpdateGroupButtonClicked;
            //this.btnGroup_Export.MouseClick += OnExportGroupButtonClicked;
            this.lviewGroupList.ItemSelectionChanged += OnGroupListSelectChanged;

            this.cboxCustomer_GroupName.SelectedIndexChanged += OnCustomerGroupNameChanged;
            this.tboxCustomer_CarNum.TextChanged += OnCustomerCarNumberChanged;
            this.btnCustomer_CheckCarNum.MouseClick += OnCheckCustomerCarNumber_ButtonClicked;

            this.btnCustomer_Extend_1Mon.MouseClick += OnExtendCustomerEndDate1Mon_ButtonClicked;
            this.btnCustomer_Extend_6Mon.MouseClick += OnExtendCustomerEndDate6Mon_ButtonClicked;
            this.btnCustomer_Extend_1Year.MouseClick += OnExtendCustomerEndDate1Year_ButtonClicked;
            this.btnCustomer_Extend_Clear.MouseClick += OnClearExtendCustomerEndDate_ButtonClicked;
            this.dtpCustomer_EndDate.ValueChanged += OnCustomerEndDateChanged;
            this.dtpCustomer_StartDate.ValueChanged += OnCustomerStartDateChanged;

            this.btnCustomer_Clear.MouseClick += OnClearCustomerButtonClicked;
            this.btnCustomer_Create.MouseClick += OnCreateCustomerButtonClicked;
            this.btnCustomer_Update.MouseClick += OnUpdateCustomerButtonClicked;
            this.btnCustomer_Delete.MouseClick += OnDeleteCustomerButtonClicked;
            //this.btnCustomer_Export.MouseClick += OnExportCustomerButtonClicked;
            this.btnCustomer_Import.MouseClick += OnImportCustomerButtonClicked;
            this.btnCustomer_PayInfo.MouseClick += OnShowCustomerPayInfo;

            this.OnRunSearchButtonClicked(null, null);
        }


        private void CustomerViewer_LoadSettings()
        {
            //_log.Debug("LoadSettings {0}", this.iniManager.FilePath);

            //try
            //{
            //    iniManager.GetInIProperties(this.dbConnectSettings);
            //    _log.Debug("{0}.IP = {1}", this.dbConnectSettings, this.dbConnectSettings.IpAddr);
            //    _log.Debug("{0}.Port = {1}", this.dbConnectSettings, this.dbConnectSettings.Port);
            //}
            //catch (Exception ex)
            //{
            //    _log.Error(ex, "LoadSettings Error!");
            //    Debug.Assert(false, "LoadSettings Error!");
            //}
        }

        private void CustomerViewer_InitDB()
        {
            try
            {
                this.parkDB = new Customer_DBAccessor("172.16.10.177", 3306, "sogno");
                _log.Info("DB 연결 성공!");
            }
            catch (Exception ex)
            {
                _log.Error(ex, "DB Connection Error!");
                Debug.Assert(false, "DB Connection Error!");
            }
        }

        private void CustomerViewer_InitDB(string ip, int port, string dbName)
        {
            try
            {
                this.parkDB = new Customer_DBAccessor(ip, port, dbName);
                //this.parkDB = new Customer_DBAccessor("172.16.10.177", 3306, "sogno");
                _log.Info("DB 연결 성공!");
            }
            catch (Exception ex)
            {
                _log.Error(ex, "DB Connection Error!");
                Debug.Assert(false, "DB Connection Error!");
            }
        }

        private void CustomerViewer_Init()
        {
            try
            {
                this.parkInfos = this.parkDB.SelectParkInfos();
                this.groupInfosTotal = this.parkDB.SelectGroupInfos(null, null, Customer_Constant.GroupType_Free);

                if (this.parkInfos == null || this.parkInfos.Length == 0)
                {
                    MessageBox.Show("데이터베이스에 주차장 정보가 없습니다. 설정파일의 주차장ID와 데이터베이스를 확인 후 다시 실행하세요!", "Error!!", MessageBoxButtons.OK);
                    return;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex, "주차장 정보 초기화 실패!!");
                throw;
            }
        }

        private void InitializeCustomerList()
        {
            if (this.parkInfos.Length > 1)
            {
                List<ParkInfoRecord> parkInfosList = new List<ParkInfoRecord>();
                parkInfosList = this.parkInfos.ToList();
                ParkInfoRecord allParkOpt = new ParkInfoRecord();
                allParkOpt.ParkName = "전체";

                parkInfosList.Insert(0, allParkOpt);
                this.parkInfos = parkInfosList.ToArray();

                this.cboxSearch_Group.Enabled = false;
            }

            this.cboxSearch_ParkName.BindingContext = new BindingContext();
            this.cboxSearch_ParkName.DataSource = this.parkInfos;
            this.cboxSearch_ParkName.DisplayMember = ParkInfoFields.ParkName.ToString();
            this.cboxSearch_ParkName.ValueMember = ParkInfoFields.ParkNo.ToString();
            this.cboxSearch_ParkName.SelectedIndex = 0;

            this.cboxSearch_State.BindingContext = new BindingContext();
            this.cboxSearch_State.DataSource = this.searchState;
            this.cboxSearch_State.DisplayMember = "TypeName";
            this.cboxSearch_State.ValueMember = "TypeQuery";
            this.cboxSearch_State.SelectedIndex = 0;

            this.lviewCustList.Columns.Add("No", 50, HorizontalAlignment.Center);
            this.lviewCustList.Columns.Add("그룹명", 100, HorizontalAlignment.Center);
            this.lviewCustList.Columns.Add("차량번호", 120, HorizontalAlignment.Center);
            this.lviewCustList.Columns.Add("성명", 80, HorizontalAlignment.Center);
            this.lviewCustList.Columns.Add("시작일", 120, HorizontalAlignment.Center);
            this.lviewCustList.Columns.Add("종료일", 120, HorizontalAlignment.Center);
            this.lviewCustList.Columns.Add("주차장명", 100, HorizontalAlignment.Center);

            this.lviewHistoryList.Columns.Add("No", 40, HorizontalAlignment.Center);
            this.lviewHistoryList.Columns.Add("시작일", 180, HorizontalAlignment.Center);
            this.lviewHistoryList.Columns.Add("종료일", 180, HorizontalAlignment.Center);
            this.lviewHistoryList.Columns.Add("수정일", 180, HorizontalAlignment.Center);
            this.lviewHistoryList.Columns.Add("상태", 60, HorizontalAlignment.Center);
        }

        private void InitializeCustomerInfo()
        {
            this.cboxCustomer_CarType.BindingContext = new BindingContext();
            this.cboxCustomer_CarType.DataSource = this.carTypes;
            this.cboxCustomer_CarType.DisplayMember = "TypeName";
            this.cboxCustomer_CarType.ValueMember = "TypeID";
            this.cboxCustomer_CarType.SelectedIndex = 0;

            DateTime now = DateTime.Now;

            this.btnCustomer_Delete.Enabled = false;
            this.btnCustomer_Update.Enabled = false;

            this.btnCustomer_CheckCarNum.Enabled = false;
            this.dtpCustomer_StartDate.Value = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
            this.dtpCustomer_EndDate.Value = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59);
        }

        private void InitializeGroupInfo()
        {
            this.lviewGroupList.Columns.Add("No", 50, HorizontalAlignment.Center);
            this.lviewGroupList.Columns.Add("그룹 유형", 110, HorizontalAlignment.Center);
            this.lviewGroupList.Columns.Add("그룹 이름", 110, HorizontalAlignment.Center);
            this.lviewGroupList.Columns.Add("요금 체계", 110, HorizontalAlignment.Center);

            this.cboxGroup_Type.DataSource = this.groupTypes;
            this.cboxGroup_Type.DisplayMember = "TypeName";
            this.cboxGroup_Type.ValueMember = "TypeID";
            this.cboxGroup_Type.SelectedIndex = -1;            

            this.btnGroup_Delete.Enabled = false;
            this.btnGroup_Update.Enabled = false;

            this.btnCustomer_Delete.Enabled = false;
            this.btnCustomer_Update.Enabled = false;
        }

        private void RefreshCustomerList()
        {
            this.lviewCustList.Items.Clear();
            this.groupInfos = this.parkDB.SelectGroupInfos(this.selectedPark?.ParkNo, null, Customer_Constant.GroupType_Free);

            foreach (var record in this.searchedCustomer)
            {
                ParkInfoRecord parkInfo = Array.Find(this.parkInfos, it => it.ParkNo == record.ParkNo);
                GroupInfoRecord[] groupInfoGroupNo = Array.FindAll(this.groupInfosTotal, it => it.GroupNo == record.GroupNo);
                GroupInfoRecord groupInfoParkNo = Array.Find(groupInfoGroupNo, it => it.ParkNo == record.ParkNo);

                ListViewItem item = new ListViewItem(record.CustGroupInfoNo.ToString());
                item.SubItems.Add(string.Format("{0}", groupInfoParkNo?.GroupName));
                item.SubItems.Add(record.CarNum);
                item.SubItems.Add(record.Name);
                item.SubItems.Add(string.Format("{0:yyy-MM-dd}", record.StartDate));
                item.SubItems.Add(string.Format("{0:yyy-MM-dd}", record.EndDate));
                item.SubItems.Add(string.Format("{0}", parkInfo == null ? "전체" : parkInfo.ParkName));

                this.lviewCustList.Items.Add(item);
            }
        }        

        private void RefreshGroupHistoryList(CustomerRecord record)
        {
            this.lviewHistoryList.Items.Clear();
            if (record != null)
            {
                CustomerRecord custInfo = this.searchedCustomer[SearchIndex()];
                this.custHistoryInfo = this.parkDB.SelectCustGroupHistory((int)custInfo.CustGroupInfoNo);

                if (this.custHistoryInfo.Length == 0 || this.custHistoryInfo == null) return;

                CustGroupHistoryRecord firstHistoryDate = this.custHistoryInfo[this.custHistoryInfo.Length - 1];

                foreach (var items in this.custHistoryInfo)
                {
                    CurrentState state = Array.Find(this.currentState, it => it.TypeID == items.CurrentState);

                    ListViewItem item = new ListViewItem(items.CustGroupHistoryNo.ToString());
                    item.SubItems.Add(string.Format("{0:yyy-MM-dd HH:mm:ss}", items.StartDate));
                    item.SubItems.Add(string.Format("{0:yyy-MM-dd HH:mm:ss}", items.EndDate));
                    item.SubItems.Add(string.Format("{0:yyy-MM-dd HH:mm:ss}", items.IssueDate));
                    item.SubItems.Add(state.TypeName);
                    this.lviewHistoryList.Items.Add(item);
                }
            }
        }

        private void RefreshCustomerInfo(CustomerRecord custInfo)
        {
            this.tboxCustomer_CarNum.Text = custInfo?.CarNum;
            this.tboxCustomer_Name.Text = custInfo?.Name;
            this.tboxCustomer_PhoneNum.Text = custInfo?.TelNum;
            this.tboxCustomer_Comp.Text = custInfo?.Comp;
            this.tboxCustomer_Dept.Text = custInfo?.Dept;
            this.tboxCustomer_Memo.Text = custInfo?.Memo;
            this.tboxCustomer_CarName.Text = custInfo?.CarName;

            DateTime now = DateTime.Now;
            now = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second);

            if (custInfo?.StartDate != null)
            {
                this.dtpCustomer_StartDate.Value = custInfo.StartDate.Value;
            }
            else
            {
                this.dtpCustomer_StartDate.Value = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0); ;
            }

            if (custInfo?.EndDate != null)
            {
                this.dtpCustomer_EndDate.Value = custInfo.EndDate.Value;
            }
            else
            {
                this.dtpCustomer_EndDate.Value = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59); ;
            }

            if (custInfo == null)
            {
                this.cboxCustomer_GroupName.SelectedIndex = -1;
            }
            else
            {
                this.RefreshCustomerGroupInfos(custInfo.ParkNo);
                this.cboxCustomer_GroupName.SelectedValue = custInfo.GroupNo;
            }

            this.startDateChange = false;
        }

        private void OnCustomerListSelectChanged(object sender, ListViewItemSelectionChangedEventArgs args)
        {
            if (args.IsSelected)
            {
                this.btnCustomer_PayInfo.Enabled = true; 

                this.RefreshCustomerInfo(this.searchedCustomer[SearchIndex()]);
                this.RefreshGroupHistoryList(this.searchedCustomer[SearchIndex()]);

                if (this.searchedCustomer[SearchIndex()].CurrentState == Customer_Constant.CurrentState_Close || this.searchedCustomer[SearchIndex()].CurrentState == Customer_Constant.CurrentState_Return || this.searchedCustomer[SearchIndex()].CurrentState == Customer_Constant.CurrentState_Delete)
                {
                    this.btnCustomer_Create.Enabled = false;
                    this.btnCustomer_Delete.Enabled = false;
                    this.btnCustomer_Update.Enabled = true;
                }
                else
                {
                    this.btnCustomer_Create.Enabled = false;
                    this.btnCustomer_Delete.Enabled = true;
                    this.btnCustomer_Update.Enabled = true;
                }

                ParkInfoRecord parkInfoRecord = Array.Find(this.parkInfos, it => it.ParkNo == this.searchedCustomer[SearchIndex()].ParkNo);
                this.selectedPark = parkInfoRecord;
                
                if (this.payInfoViewer != null)
                {                    
                    this.payInfoViewer.RefreshCustomerPayInfoList(this.searchedCustomer[SearchIndex()]);
                    this.payInfoViewer.Refresh();
                }
            }
            else
            {
                this.btnCustomer_PayInfo.Enabled = false;

                this.RefreshCustomerInfo(null);
                this.RefreshGroupHistoryList(null);

                this.btnCustomer_Create.Enabled = true;
                this.btnCustomer_Delete.Enabled = false;
                this.btnCustomer_Update.Enabled = false;

                this.tboxCustomer_CarNum.BackColor = Color.White;
                this.cboxCustomer_GroupName.BackColor = Color.White;
                this.tboxCustomer_Name.BackColor = Color.White;
            }
        }

        private void OnRunSearchButtonClicked(object sender, MouseEventArgs args)
        {
            string nameKeyword = this.tboxSearch_Name.Text;
            string carnumKeyword = this.tboxSearch_CarNum.Text;
            this.selectedGroup = this.cboxSearch_Group.SelectedItem as GroupInfoRecord;
            this.selectedState = this.cboxSearch_State.SelectedItem as SearchState;
            this.selectedPark = this.cboxSearch_ParkName.SelectedItem as ParkInfoRecord;

            this.searchedCustomer = this.parkDB.SearchCustomerInfos(this.selectedPark?.ParkNo, this.selectedGroup?.GroupNo, Customer_Constant.GroupType_Free, null, nameKeyword, carnumKeyword, this.selectedState?.TypeQuery);

            this.RefreshCustomerList();

            if (this.lviewCustList.Items.Count > 0)
            {
                this.lviewCustList.Items[0].Focused = true;
                this.lviewCustList.Items[0].Selected = true;
            }
        }

        private void OnShowCustomerPayInfo(object sender, MouseEventArgs args)
        {
            if (this.payInfoViewer == null)
            {
                this.payInfoViewer = new frmCustomerPopup_PayInfo(this.parkDB, this);
                this.payInfoViewer.Owner = this;

                this.payInfoViewer.RefreshCustomerPayInfoList(this.searchedCustomer[SearchIndex()]);
                this.payInfoViewer.Show();
            }
        }

        private void OnClearSearchButtonClicked(object sender, MouseEventArgs args)
        {
            this.tboxSearch_CarNum.Text = "";
            this.tboxSearch_Name.Text = "";
            this.cboxSearch_ParkName.SelectedIndex = 0;
            this.cboxSearch_State.SelectedIndex = 0;

            this.OnRunSearchButtonClicked(null, null);
        }

        private void cboxSearchGroupChanged(object sender, EventArgs args)
        {
            this.OnRunSearchButtonClicked(null, null);
        }

        private void cboxSearchStateChanged(object sender, EventArgs args)
        {
            this.OnRunSearchButtonClicked(null, null);
        }

        private void RefreshSearchGroupInfos(int? parkNo)
        {
            this.cboxSearch_Group.DataSource = null;
            this.cboxSearch_Group.Items.Clear();

            this.groupInfos = this.parkDB.SelectGroupInfos(parkNo, null, Customer_Constant.GroupType_Free);

            List<GroupInfoRecord> tmpGroupRecord = new List<GroupInfoRecord>();
            tmpGroupRecord = this.groupInfos.ToList();
            GroupInfoRecord nullGroup = new GroupInfoRecord();
            nullGroup.GroupName = "전체";
            tmpGroupRecord.Insert(0, nullGroup);
            this.groupInfos = tmpGroupRecord.ToArray();
            this.cboxSearch_Group.BindingContext = new BindingContext();
            this.cboxSearch_Group.DataSource = this.groupInfos;
            this.cboxSearch_Group.DisplayMember = GroupInfoFields.GroupName.ToString();
            this.cboxSearch_Group.ValueMember = GroupInfoFields.GroupNo.ToString();
        }

        private void RefreshCustomerGroupInfos(int? parkNo)
        {
            this.cboxCustomer_GroupName.DataSource = null;
            this.cboxCustomer_GroupName.Text = string.Empty;
            this.cboxCustomer_GroupName.Items.Clear();

            this.groupInfos = this.parkDB.SelectGroupInfos(parkNo, null, Customer_Constant.GroupType_Free);
            this.cboxCustomer_GroupName.DataSource = this.groupInfos;
            this.cboxCustomer_GroupName.DisplayMember = GroupInfoFields.GroupName.ToString();
            this.cboxCustomer_GroupName.ValueMember = GroupInfoFields.GroupNo.ToString();
            this.cboxCustomer_GroupName.SelectedIndex = -1;
        }

        private void RefreshGroupList(int? parkNo)
        {
            this.cboxGroup_Trf.DataSource = null;
            this.cboxGroup_Trf.Items.Clear();
            this.cboxGroup_Dc.DataSource = null;
            this.cboxGroup_Dc.Items.Clear();

            this.tariffs = this.parkDB.SelectTariffs(parkNo);
            this.dcInfos = this.parkDB.SelectDCInfos(parkNo, null, null);

            this.cboxGroup_Trf.DataSource = this.tariffs;
            this.cboxGroup_Trf.DisplayMember = TariffFields.TrfName.ToString();
            this.cboxGroup_Trf.ValueMember = TariffFields.TrfNo.ToString();
            this.cboxGroup_Trf.SelectedIndex = -1;

            this.cboxGroup_Dc.DataSource = this.dcInfos;
            this.cboxGroup_Dc.DisplayMember = DCInfoFields.DCName.ToString();
            this.cboxGroup_Dc.ValueMember = DCInfoFields.DCNo.ToString();
            this.cboxGroup_Dc.SelectedIndex = -1;

            this.lviewGroupList.Items.Clear();
            this.groupInfos = this.parkDB.SelectGroupInfos(parkNo, null, Customer_Constant.GroupType_Free);

            foreach (var record in this.groupInfos)
            {
                ListViewItem item = new ListViewItem(record.GroupNo.ToString());
                item.SubItems.Add(Customer_Functions.GetGroupTypeString(record.GroupType));
                item.SubItems.Add(record.GroupName);
                item.SubItems.Add(Array.Find(this.tariffs, it => it.TrfNo == record.TrfNo)?.TrfName);

                this.lviewGroupList.Items.Add(item);
            }
        }

        private void cboxSearchParkNameChanged(object sender, EventArgs args)
        {
            this.selectedPark = this.cboxSearch_ParkName.SelectedItem as ParkInfoRecord;

            this.cboxSearch_Group.DataSource = null;
            this.cboxSearch_Group.Items.Clear();

            this.cboxCustomer_GroupName.DataSource = null;
            this.cboxCustomer_GroupName.Items.Clear();

            this.lviewGroupList.Items.Clear();

            if (this.selectedPark.ParkNo == null)
            {
                this.cboxSearch_Group.Enabled = false;

            }
            else
            {
                this.cboxSearch_Group.Enabled = true;

                this.RefreshSearchGroupInfos(this.selectedPark.ParkNo);
                this.RefreshCustomerGroupInfos(this.selectedPark.ParkNo);
                this.RefreshGroupList(this.selectedPark.ParkNo);
            }

            this.OnClearCustomerButtonClicked(null, null);
            this.OnRunSearchButtonClicked(null, null);
        }

        private void OnExtendCustomerEndDate1Mon_ButtonClicked(object sender, MouseEventArgs args)
        {
            this.dtpCustomer_EndDate.Value = this.dtpCustomer_EndDate.Value.AddMonths(1);
        }

        private void OnExtendCustomerEndDate6Mon_ButtonClicked(object sender, MouseEventArgs args)
        {
            this.dtpCustomer_EndDate.Value = this.dtpCustomer_EndDate.Value.AddMonths(6);
        }

        private void OnExtendCustomerEndDate1Year_ButtonClicked(object sender, MouseEventArgs args)
        {
            this.dtpCustomer_EndDate.Value = this.dtpCustomer_EndDate.Value.AddYears(1);
        }

        private void OnClearExtendCustomerEndDate_ButtonClicked(object sender, MouseEventArgs args)
        {
            if (this.lviewCustList.SelectedIndices.Count > 0)
            {
                try
                {
                    CustomerRecord custInfo = this.searchedCustomer[SearchIndex()];
                    this.dtpCustomer_StartDate.Value = custInfo.StartDate.Value;
                    this.dtpCustomer_EndDate.Value = custInfo.EndDate.Value;
                }
                catch (Exception ex)
                {
                    throw;
                    _log.Warn(ex, "** 오류!!");
                    Debug.Assert(false, "차량 관리 파트 오류");
                }
            }
            else
            {
                this.dtpCustomer_EndDate.Value = this.dtpCustomer_StartDate.Value;
            }
        }

        private void OnCustomerCarNumberChanged(object sender, EventArgs args)
        {
            if (this.tboxCustomer_CarNum.Text.Length == 0)
            {
                this.btnCustomer_CheckCarNum.Enabled = false;
            }
            else if (this.lviewCustList.SelectedIndices.Count > 0)
            {
                try
                {
                    CustomerRecord custInfo = this.searchedCustomer[SearchIndex()];

                    this.btnCustomer_CheckCarNum.Enabled = (custInfo.CarNum != this.tboxCustomer_CarNum.Text);
                }
                catch (Exception ex)
                {
                    throw;
                    _log.Warn(ex, "** 오류!!");
                }
            }
            else if (this.tboxCustomer_CarNum.Text.Length != 0 && this.cboxCustomer_GroupName.SelectedValue == null)
            {
                this.btnCustomer_CheckCarNum.Enabled = false;
            }
            else
            {
                this.btnCustomer_CheckCarNum.Enabled = true;
            }
        }

        private void OnCheckCustomerCarNumber_ButtonClicked(object sender, EventArgs args)
        {
            if (this.parkDB.CanInsertCustGroupInfo((int)this.selectedPark.ParkNo, this.tboxCustomer_CarNum.Text, (int)this.cboxCustomer_GroupName.SelectedValue))
            {
                MessageBox.Show("사용가능한 차량 번호입니다.", "중복 확인", MessageBoxButtons.OK);
                this.tboxCustomer_CarNum.BackColor = Color.White;
                this.btnCustomer_CheckCarNum.Enabled = false;
            }
            else
            {
                MessageBox.Show("이미 등록된 차량 번호입니다.", "중복 확인", MessageBoxButtons.OK);
                this.tboxCustomer_CarNum.BackColor = Color.LightPink;
            }
        }

        private void OnCustomerStartDateChanged(object sender, EventArgs args)
        {
            this.startDateChange = true;
        }

        private void OnCustomerEndDateChanged(object sender, EventArgs args)
        {
            //DateTime originalDate = this.dtpCustomer_StartDate.Value;
            //DateTime changedDate = this.dtpCustomer_EndDate.Value;

            //if (this.lviewCustList.SelectedIndices.Count > 0)
            //{
            //    try
            //    {
            //        CustomerRecord custInfo = this.searchedCustomer[SearchIndex()];
            //        originalDate = custInfo.EndDate.Value;
            //    }
            //    catch (Exception ex)
            //    {
            //        throw;
            //        _log.Warn(ex, "** 정기권 연장 요금 계산 중 오류!!");
            //    }
            //}
        }

        private void OnCustomerGroupNameChanged(object sender, EventArgs args)
        {
            
            if (this.lviewCustList.SelectedIndices.Count > 0 && this.cboxCustomer_GroupName.SelectedIndex != -1 && this.tboxCustomer_CarNum.Text.Length != 0)
            {
                CustomerRecord custInfo = this.searchedCustomer[SearchIndex()];
                if (custInfo.GroupNo.ToString() != this.cboxCustomer_GroupName.SelectedValue.ToString())
                {
                    this.btnCustomer_CheckCarNum.Enabled = true;
                }
                else
                {
                    this.btnCustomer_CheckCarNum.Enabled = false;
                }
            }
            else if (this.tboxCustomer_CarNum.Text.Length == 0)
            {
                this.btnCustomer_CheckCarNum.Enabled = false;
            }
            else
            {
                this.btnCustomer_CheckCarNum.Enabled = true;
            }
        }

        private bool CheckCustomerInfoForms()
        {
            bool retval = true;

            this.tboxCustomer_CarNum.BackColor = Color.White;
            this.cboxCustomer_GroupName.BackColor = Color.White;

            if (this.tboxCustomer_CarNum.Text.Length == 0)
            {
                this.tboxCustomer_CarNum.BackColor = Color.LightPink;
                retval = false;
            }

            if (this.btnCustomer_CheckCarNum.Enabled)
            {
                this.tboxCustomer_CarNum.BackColor = Color.LightPink;
                retval = false;

                MessageBox.Show("차량 번호 중복 확인을 먼저 실행하세요.", "오류", MessageBoxButtons.OK);
            }

            if (this.cboxCustomer_GroupName.SelectedIndex < 0)
            {
                this.cboxCustomer_GroupName.BackColor = Color.LightPink;
                retval = false;
            }

            return retval;
        }

        private void ReadCustomerInfo(CustGroupInfoRecord custGroupInfoRecord, int? carUserNo, int currentState)
        {
            custGroupInfoRecord.CarUserNo = carUserNo;
            custGroupInfoRecord.ParkNo = this.selectedPark.ParkNo;
            custGroupInfoRecord.GroupNo = this.cboxCustomer_GroupName.SelectedValue as int?;

            GroupInfoRecord ifFreePass = this.cboxCustomer_GroupName.SelectedItem as GroupInfoRecord;
            custGroupInfoRecord.GroupType = ifFreePass.GroupType == Customer_Constant.GroupType_FreePass ? Customer_Constant.GroupType_FreePass : Customer_Constant.GroupType_Free;

            custGroupInfoRecord.StartDate = new DateTime(this.dtpCustomer_StartDate.Value.Year, this.dtpCustomer_StartDate.Value.Month, this.dtpCustomer_StartDate.Value.Day, 0, 0, 0);
            custGroupInfoRecord.EndDate = new DateTime(this.dtpCustomer_EndDate.Value.Year, this.dtpCustomer_EndDate.Value.Month, this.dtpCustomer_EndDate.Value.Day, 23, 59, 59);
            custGroupInfoRecord.CurrentState = currentState;
            custGroupInfoRecord.Memo = this.tboxCustomer_Memo.Text.Length > 0 ? this.tboxCustomer_Memo.Text : null;
        }

        private void ReadCarUserInfo(CarUserInfoRecord carUserInfoRecord)
        {
            carUserInfoRecord.CarNum = this.tboxCustomer_CarNum.Text;
            carUserInfoRecord.CarType = this.cboxCustomer_CarType.SelectedValue as int?;
            carUserInfoRecord.CarName = this.tboxCustomer_CarName.Text.Length > 0 ? this.tboxCustomer_CarName.Text : null;
            carUserInfoRecord.Name = this.tboxCustomer_Name.Text.Length > 0 ? this.tboxCustomer_Name.Text : null;
            carUserInfoRecord.TelNum = this.tboxCustomer_PhoneNum.Text.Length > 0 ? this.tboxCustomer_PhoneNum.Text : null;
            carUserInfoRecord.Comp = this.tboxCustomer_Comp.Text.Length > 0 ? this.tboxCustomer_Comp.Text : null;
            carUserInfoRecord.Dept = this.tboxCustomer_Dept.Text.Length > 0 ? this.tboxCustomer_Dept.Text : null;
        }

        // 정기권 신규
        public void OnCreateCustomerButtonClicked(object sender, MouseEventArgs args)
        {
            if (CheckCustomerInfoForms())
            {
                try
                {
                    if (this.parkDB.CanInsertCustGroupInfo((int)this.selectedPark.ParkNo, this.tboxCustomer_CarNum.Text, (int)this.cboxCustomer_GroupName.SelectedValue))
                    {
                        CarUserInfoRecord carUserInfo = new CarUserInfoRecord();
                        this.ReadCarUserInfo(carUserInfo);
                        if (this.parkDB.CanInsertCarUserInfo(carUserInfo.CarNum))
                        {
                            this.parkDB.InsertCarUserInfo(carUserInfo);
                        }

                        CustGroupInfoRecord custGroupInfo = new CustGroupInfoRecord();
                        CarUserInfoRecord carUserInfoCarUserNo = new CarUserInfoRecord();

                        carUserInfoCarUserNo = this.parkDB.SelectCarUserInfos(carUserInfo.CarNum)[0];

                        this.ReadCustomerInfo(custGroupInfo, carUserInfoCarUserNo.CarUserNo, Customer_Constant.CurrentState_New);
                        this.parkDB.InsertCustGroupInfo(custGroupInfo);

                        CustomerRecord records = this.parkDB.SelectCustGroupInfos(this.selectedPark.ParkNo, carUserInfoCarUserNo.CarUserNo, this.cboxCustomer_GroupName.SelectedValue as int?)[0];

                        CustGroupHistoryRecord custGroupHistory = new CustGroupHistoryRecord();
                        custGroupHistory.CustGroupInfoNo = records.CustGroupInfoNo;
                        custGroupHistory.StartDate = records.StartDate;
                        custGroupHistory.EndDate = records.EndDate;
                        custGroupHistory.CurrentState = records.CurrentState;
                        custGroupHistory.IssueDate = DateTime.Now;
                        custGroupHistory.LimitDay = records.LimitDay;

                        this.parkDB.InsertCustGroupHistory(custGroupHistory);

                        this.cboxSearch_Group.SelectedValue = custGroupInfo.GroupNo;
                        this.tboxSearch_CarNum.Text = carUserInfo.CarNum;

                        this.OnRunSearchButtonClicked(null, null);
                    }
                    else
                    {
                        MessageBox.Show(string.Format("\"{0}\" 차량이 이미 \"{1}\"에 등록되어 있습니다.", this.tboxCustomer_CarNum.Text, this.selectedPark.ParkName), "차량 중복", MessageBoxButtons.OK);
                    }

                }
                catch (Exception ex)
                {
                    throw;
                    _log.Warn(ex, "** Customer Insert Fail.");
                }
            }
        }

        public void OnUpdateCustomerButtonClicked(object sender, MouseEventArgs args)
        {
            if (CheckCustomerInfoForms())
            {
                try
                {
                    CustomerRecord custInfo = this.searchedCustomer[SearchIndex()];
                    DateTime originEndDate = custInfo.EndDate.Value;

                    CustGroupInfoRecord custGroupInfo = new CustGroupInfoRecord();
                    this.ReadCustomerInfo(custGroupInfo, custInfo.CarUserNo, Customer_Constant.CurrentState_Update);
                    custGroupInfo.CustGroupInfoNo = custInfo.CustGroupInfoNo;
                    this.parkDB.UpdateCustGroupInfo(custGroupInfo);

                    CarUserInfoRecord carUserInfo = new CarUserInfoRecord();
                    this.ReadCarUserInfo(carUserInfo);
                    carUserInfo.CarUserNo = custInfo.CarUserNo;
                    this.parkDB.UpdateCarUserInfo(carUserInfo);

                    CustomerRecord records = this.parkDB.SelectCustGroupInfos(this.selectedPark.ParkNo, custInfo.CarUserNo, custGroupInfo.GroupNo)[0];

                    CustGroupHistoryRecord custGroupHistory = new CustGroupHistoryRecord();
                    custGroupHistory.CustGroupInfoNo = records.CustGroupInfoNo;

                    if (this.startDateChange) custGroupHistory.StartDate = records.StartDate;
                    else custGroupHistory.StartDate = originEndDate.AddSeconds(1);

                    custGroupHistory.EndDate = records.EndDate;
                    custGroupHistory.CurrentState = records.CurrentState;
                    custGroupHistory.IssueDate = DateTime.Now;
                    custGroupHistory.LimitDay = records.LimitDay;

                    this.parkDB.InsertCustGroupHistory(custGroupHistory);

                    this.cboxSearch_Group.SelectedValue = custGroupInfo.GroupNo;
                    this.tboxSearch_CarNum.Text = custInfo.CarNum;

                    this.OnRunSearchButtonClicked(null, null);
                }
                catch (Exception ex)
                {
                    throw;
                    _log.Warn(ex, "** Customer Update Fail.");
                }
            }
        }

        private void OnDeleteCustomerButtonClicked(object sender, MouseEventArgs args)
        {
            try
            {
                var button = (Button)sender;
                CustomerRecord custInfo = this.searchedCustomer[SearchIndex()];

                if (MessageBox.Show(string.Format("\"{0}\" 차량의 정기권 정보를 {1}하시겠습니까?", custInfo.CarNum, button.Text), "정기권 정보 삭제", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    CustGroupInfoRecord custGroupInfo = new CustGroupInfoRecord();
                    this.ReadCustomerInfo(custGroupInfo, custInfo.CarUserNo, Int16.Parse(button.Tag.ToString()));
                    custGroupInfo.CustGroupInfoNo = custInfo.CustGroupInfoNo;
                    DateTime now = DateTime.Now;
                    now = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59);
                    custGroupInfo.EndDate = now;

                    this.parkDB.UpdateCustGroupInfo(custGroupInfo);

                    CustomerRecord records = this.parkDB.SelectCustGroupInfos(this.selectedPark.ParkNo, custInfo.CarUserNo, custGroupInfo.GroupNo)[0];
                    CustGroupHistoryRecord custGroupHistory = new CustGroupHistoryRecord();
                    custGroupHistory.CustGroupInfoNo = records.CustGroupInfoNo;
                    custGroupHistory.StartDate = records.StartDate;
                    custGroupHistory.EndDate = records.EndDate;
                    custGroupHistory.CurrentState = records.CurrentState;
                    custGroupHistory.IssueDate = DateTime.Now;
                    custGroupHistory.LimitDay = records.LimitDay;

                    this.parkDB.InsertCustGroupHistory(custGroupHistory);

                    this.lviewCustList.SelectedIndices.Clear();
                    this.cboxCustomer_GroupName.Enabled = true;

                    OnRunSearchButtonClicked(null, null);
                    this.lviewHistoryList.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                throw;
                _log.Warn(ex, "** Customer Delete Fail.");
                Debug.Assert(false, "정기권 차량 삭제 파트 오류");
            }
        }

        private void OnCustomerInfoUpdated()
        {
            this.lviewCustList.SelectedIndices.Clear();
            this.OnRunSearchButtonClicked(null, null);
        }

        private void OnClearCustomerButtonClicked(object sender, MouseEventArgs args)
        {
            this.tboxCustomer_CarNum.BackColor = Color.White;
            this.cboxCustomer_GroupName.BackColor = Color.White;
            this.btnCustomer_Create.Enabled = true;
            this.btnCustomer_Update.Enabled = false;

            this.lviewCustList.SelectedIndices.Clear();
            RefreshCustomerInfo(null);
        }

        private void OnCheckBoxColorChange(object sender, EventArgs e)
        {
            CheckBox btn = sender as CheckBox;
            if (!btn.Checked)
            {
                btn.ForeColor = Color.WhiteSmoke;
                btn.Font = new Font(btn.Font, FontStyle.Regular);
            }
            else
            {
                btn.ForeColor = Color.Turquoise;
                btn.Font = new Font(btn.Font, FontStyle.Bold);
            }
        }

        private int SearchIndex()
        {
            string tmpCustNo = this.lviewCustList.SelectedItems[0].SubItems[0].Text;
            return Array.FindIndex(this.searchedCustomer, it => it.CustGroupInfoNo == Convert.ToInt32(tmpCustNo));
        }        

        private void RefreshGroupInfo(GroupInfoRecord groupInfo)
        {
            this.tboxGroup_Name.Text = groupInfo?.GroupName;
            this.tboxGroup_UseTime.Text = groupInfo?.UseTime;
            this.tboxGroup_Memo.Text = groupInfo?.Memo;
            this.tboxGroup_Amount.Text = groupInfo?.Amount.ToString();

            if (groupInfo?.GroupType == null)
            {
                this.cboxGroup_Type.SelectedIndex = -1;
            }
            else
            {
                this.cboxGroup_Type.SelectedValue = groupInfo.GroupType;
            }

            if (groupInfo?.TrfNo == null)
            {
                this.cboxGroup_Trf.SelectedIndex = -1;
            }
            else
            {
                this.cboxGroup_Trf.SelectedValue = groupInfo.TrfNo;
            }

            if (groupInfo?.DCNo == null)
            {
                this.cboxGroup_Dc.SelectedIndex = -1;
            }
            else
            {
                this.cboxGroup_Dc.SelectedValue = groupInfo.DCNo;
            }
        }

        private void OnGroupListSelectChanged(object sender, ListViewItemSelectionChangedEventArgs args)
        {
            if (args.IsSelected)
            {
                RefreshGroupInfo(this.groupInfos[args.ItemIndex]);

                this.btnGroup_Create.Enabled = false;
                this.btnGroup_Delete.Enabled = true;
                this.btnGroup_Update.Enabled = true;
            }
            else
            {
                RefreshGroupInfo(null);

                this.btnGroup_Create.Enabled = true;
                this.btnGroup_Delete.Enabled = false;
                this.btnGroup_Update.Enabled = false;

                this.tboxGroup_Name.BackColor = Color.White;
                this.cboxGroup_Type.BackColor = Color.White;
                this.tboxGroup_Amount.BackColor = Color.White;
                this.cboxGroup_Trf.BackColor = Color.White;
                this.cboxGroup_Dc.BackColor = Color.White;
                this.tboxGroup_UseTime.BackColor = Color.White;
            }
        }

        private bool CheckGroupInfoForms()
        {
            if (this.selectedPark?.ParkNo == null)
            {
                MessageBox.Show(string.Format("주차장을 먼저 선택해 주세요."), "주차장 선택 오류");
                return false;
            }

            bool retval = true;

            this.tboxGroup_Name.BackColor = Color.White;
            this.cboxGroup_Type.BackColor = Color.White;
            this.tboxGroup_Amount.BackColor = Color.White;
            this.cboxGroup_Trf.BackColor = Color.White;
            this.cboxGroup_Dc.BackColor = Color.White;
            this.tboxGroup_UseTime.BackColor = Color.White;

            if (this.tboxGroup_Name.Text.Length == 0)
            {
                this.tboxGroup_Name.BackColor = Color.LightPink;
                retval = false;
            }

            if (this.cboxGroup_Type.SelectedIndex < 0)
            {
                this.cboxGroup_Type.BackColor = Color.LightPink;
                retval = false;
            }

            if (!int.TryParse(this.tboxGroup_Amount.Text, out int amount))
            {
                this.tboxGroup_Amount.BackColor = Color.LightPink;
                retval = false;
            }

            if (this.cboxGroup_Trf.SelectedIndex < 0)
            {
                this.cboxGroup_Trf.BackColor = Color.LightPink;
                retval = false;
            }

            return retval;
        }

        private void OnClearGroupButtonClicked(object sender, MouseEventArgs args)
        {
            this.lviewGroupList.SelectedIndices.Clear();
            RefreshGroupInfo(null);
        }

        private void OnCreateGroupButtonClicked(object sender, MouseEventArgs args)
        {
            if (CheckGroupInfoForms())
            {
                try
                {
                    GroupInfoRecord groupInfo = new GroupInfoRecord();
                    groupInfo.ParkNo = this.selectedPark.ParkNo;
                    groupInfo.GroupName = this.tboxGroup_Name.Text;
                    groupInfo.GroupType = this.cboxGroup_Type.SelectedValue as int?;
                    groupInfo.Amount = int.Parse(this.tboxGroup_Amount.Text);
                    groupInfo.TrfNo = this.cboxGroup_Trf.SelectedValue as int?;
                    groupInfo.DCNo = this.cboxGroup_Dc.SelectedIndex >= 0 ? (this.cboxGroup_Dc.SelectedValue as int?) : null;
                    groupInfo.UseTime = this.tboxGroup_UseTime.MaskCompleted ? this.tboxGroup_UseTime.Text : null;
                    groupInfo.Memo = this.tboxGroup_Memo.Text.Length > 0 ? this.tboxGroup_Memo.Text : null;

                    this.parkDB.InsertGroupInfo(groupInfo);

                    OnGroupInfoUpdated();
                    RefreshGroupInfo(null);
                }
                catch (Exception ex)
                {
                    throw;
                    _log.Warn(ex, "** GroupInfo Insert Fail.");
                }
            }
        }

        private void OnDeleteGroupButtonClicked(object sender, MouseEventArgs args)
        {
            try
            {
                int index = this.lviewGroupList.SelectedIndices[0];
                GroupInfoRecord groupInfo = this.groupInfos[index];

                bool canDelete = this.parkDB.CanDeleteGroupInfo((int)this.selectedPark.ParkNo, groupInfo.GroupNo.Value);
                if (canDelete)
                {
                    if (MessageBox.Show(string.Format("\"{0}\" 그룹을 정말로 삭제하시겠습니까?", groupInfo.GroupName), "정기권 그룹 삭제", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    {
                        this.parkDB.DeleteGroupInfo(groupInfo);

                        OnGroupInfoUpdated();
                    }
                }
                else
                {
                    MessageBox.Show("그룹에 포함된 정기권 사용자가 있습니다. 사용자를 먼저 삭제한 후 다시 시도하세요.", "Error!!", MessageBoxButtons.OK);
                    return;
                }
            }
            catch (Exception ex)
            {
                throw;
                _log.Warn(ex, "** GroupInfo Delete Fail.");
            }
        }

        private void OnUpdateGroupButtonClicked(object sender, MouseEventArgs args)
        {
            if (CheckGroupInfoForms())
            {
                try
                {
                    int index = this.lviewGroupList.SelectedIndices[0];
                    GroupInfoRecord groupInfo = this.groupInfos[index];
                    groupInfo.GroupName = this.tboxGroup_Name.Text;
                    groupInfo.GroupType = this.cboxGroup_Type.SelectedValue as int?;
                    groupInfo.Amount = int.Parse(this.tboxGroup_Amount.Text);
                    groupInfo.TrfNo = this.cboxGroup_Trf.SelectedValue as int?;
                    groupInfo.DCNo = this.cboxGroup_Dc.SelectedIndex >= 0 ? (this.cboxGroup_Dc.SelectedValue as int?) : null;
                    groupInfo.UseTime = this.tboxGroup_UseTime.MaskCompleted ? this.tboxGroup_UseTime.Text : null;
                    groupInfo.Memo = this.tboxGroup_Memo.Text.Length > 0 ? this.tboxGroup_Memo.Text : null;

                    this.parkDB.UpdateGroupInfo(groupInfo);

                    OnGroupInfoUpdated();
                }
                catch (Exception ex)
                {
                    throw;
                    _log.Warn(ex, "** GroupInfo Update Fail.");
                }
            }
        }

        private void OnGroupInfoUpdated()
        {
            this.lviewCustList.SelectedIndices.Clear();
            this.lviewGroupList.SelectedIndices.Clear();

            this.groupInfos = this.parkDB.SelectGroupInfos(this.selectedPark.ParkNo);
            RefreshGroupList(this.selectedPark.ParkNo);

            this.RefreshSearchGroupInfos(this.selectedPark.ParkNo);
            this.RefreshCustomerGroupInfos(this.selectedPark.ParkNo);
        }

        private void OnImportCustomerButtonClicked(object sender, MouseEventArgs args)
        {
            if(this.selectedPark.ParkNo == null)
            {
                MessageBox.Show("선택된 주차장이 없습니다.", "오류!!", MessageBoxButtons.OK);
                return;
            }

            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "정기권 차량 정보 불러오기";
            dialog.Filter = "CSV 파일|*.csv";

            dialog.ShowDialog();

            if (dialog.FileName.Length > 0)
            {
                CustomerRecord[] custInfos = LoadCustomerInfoFromFile(dialog.FileName);

                bool completed = true;
                int nInsert = 0;
                int nUpdate = 0;

                for (int i = 0; i < custInfos.Length; i++)
                {
                    try
                    {
                        CustomerRecord custInfo = custInfos[i];

                        CarUserInfoRecord carUserInfo = new CarUserInfoRecord();
                        carUserInfo.CarNum = custInfo.CarNum;
                        carUserInfo.Name = custInfo.Name;
                        carUserInfo.TelNum = custInfo.TelNum;
                        carUserInfo.Comp = custInfo.Comp;
                        carUserInfo.Dept = custInfo.Dept;
                        carUserInfo.Address = custInfo.Address;

                        if (this.parkDB.CanInsertCarUserInfo(carUserInfo.CarNum))
                        {
                            this.parkDB.InsertCarUserInfo(carUserInfo);
                        }

                        carUserInfo.CarUserNo = this.parkDB.SelectCarUserInfos(carUserInfo.CarNum)[0].CarUserNo;

                        CustGroupInfoRecord custGroupInfo = new CustGroupInfoRecord();
                        custGroupInfo.CarUserNo = carUserInfo.CarUserNo;
                        custGroupInfo.ParkNo = custInfo.ParkNo;
                        custGroupInfo.GroupNo = custInfo.GroupNo;
                        custGroupInfo.GroupType = custInfo.GroupType;
                        custGroupInfo.StartDate = custInfo.StartDate;
                        custGroupInfo.EndDate = custInfo.EndDate;
                        custGroupInfo.Memo = custInfo.Memo;

                        CustomerRecord[] custGroupInfoRecord = this.parkDB.SelectCustGroupInfos(custInfo.ParkNo, carUserInfo.CarUserNo, custInfo.GroupNo);
                        if (custGroupInfoRecord.Length == 0)
                        {
                            nInsert++;
                            custGroupInfo.CurrentState = Customer_Constant.CurrentState_New;
                            this.parkDB.InsertCustGroupInfo(custGroupInfo);
                        }
                        else
                        {
                            nUpdate++;
                            custGroupInfo.CustGroupInfoNo = custGroupInfoRecord[0].CustGroupInfoNo;
                            custGroupInfo.CurrentState = Customer_Constant.CurrentState_Update;
                            this.parkDB.UpdateCustGroupInfo(custGroupInfo);
                        }
                    }
                    catch (Exception ex)
                    {
                        completed = false;
                        MessageBox.Show(string.Format("{0} 번째 샘플 추가 시 오류!!", i + 1), "오류!!", MessageBoxButtons.OK);

                        _log.Error("{0} 번째 샘플 추가 시 오류!!", i + 1);
                        break;
                    }
                }

                OnCustomerInfoUpdated();
                RefreshCustomerInfo(null);

                if (completed)
                {
                    MessageBox.Show(string.Format("{0} 개의 정보를 추가, {1} 개의 정보를 업데이트하였습니다.", nInsert, nUpdate), "가져오기 완료", MessageBoxButtons.OK);
                }
            }
        }

        private CustomerRecord[] LoadCustomerInfoFromFile(string csv)
        {
            FileStream fs = null;
            List<CustomerRecord> list = new List<CustomerRecord>();

            try
            {
                fs = new FileStream(csv, FileMode.Open);
                using (StreamReader reader = new StreamReader(fs, Encoding.Default))
                {
                    reader.ReadLine();
                    int lineNo = 0;

                    while (!reader.EndOfStream)
                    {
                        lineNo++;
                        string[] tokens = reader.ReadLine().Split(',');
                        if (tokens.Length == 0)
                        {
                            break;
                        }

                        CustomerRecord custInfo = new CustomerRecord();
                        custInfo.ParkNo = this.selectedPark.ParkNo;
                        custInfo.GroupType = Customer_Constant.GroupType_Free;

                        for (int i = 0; i < tokens.Length; i++)
                        {
                            string token = tokens[i].Trim();
                            if (token.Length == 0)
                            {
                                continue;
                            }

                            try
                            {
                                if (i == 0) custInfo.GroupNo = Convert.ToInt32(token);
                                else if (i == 1) custInfo.CarNum = token;
                                else if (i == 2) custInfo.Name = token;
                                else if (i == 3) custInfo.StartDate = DateTime.Parse(token);
                                else if (i == 4)
                                {
                                    DateTime tempTime = DateTime.Parse(token);
                                    custInfo.EndDate = new DateTime(tempTime.Year, tempTime.Month, tempTime.Day, 23, 59, 59);
                                }
                                else if (i == 5) custInfo.Comp = token;
                                else if (i == 6) custInfo.Dept = token;
                                else if (i == 7) custInfo.Address = token;
                                else if (i == 8) custInfo.TelNum = token;
                                else if (i == 9) custInfo.Memo = token;
                            }
                            catch (Exception ex)
                            {
                                _log.Error(ex, "** {0} 번째 샘플, {1} 번째 필드 오류!!", lineNo, i + 1);
                                throw;
                            }
                        }

                        if (custInfo.CarNum == null || custInfo.StartDate == null || custInfo.EndDate == null)
                        {
                            _log.Error("** {0} 번째 샘플 오류!!", lineNo);
                            throw new Exception("필수 입력 항목 누락");
                        }

                        list.Add(custInfo);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Warn(ex, "** 정기권 차량 정보 파일을 읽어오는 과정에서 오류!");

                if (fs != null)
                {
                    fs.Dispose();
                }

                list.Clear();
            }

            return list.ToArray();
        }

        //private void OnExportCustomerButtonClicked(object sender, MouseEventArgs args)
        //{
        //    if (this.selectedPark.ParkNo == null)
        //    {
        //        MessageBox.Show("선택된 주차장이 없습니다.", "오류!!", MessageBoxButtons.OK);
        //        return;
        //    }

        //    SaveFileDialog dialog = new SaveFileDialog();
        //    dialog.Filter = "CSV 파일|*.csv";
        //    dialog.Title = "정기권 차량 정보 저장";
        //    dialog.ShowDialog();

        //    _log.Debug(dialog.FileName);

        //    if (dialog.FileName.Length > 0)
        //    {
        //        FileStream fs = null;

        //        try
        //        {
        //            CustomerInfoRecord[] records = this.parkDB.SelectCustomerInfo(this.parkNo);

        //            fs = new FileStream(dialog.FileName, FileMode.Create);
        //            using (StreamWriter writer = new StreamWriter(fs, Encoding.Default))
        //            {
        //                writer.WriteLine("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}",
        //                    CustomerInfoFields.ParkNo,
        //                    CustomerInfoFields.CustNo,
        //                    CustomerInfoFields.GroupNo,
        //                    CustomerInfoFields.Name,
        //                    CustomerInfoFields.TelNum,
        //                    CustomerInfoFields.CarNum,
        //                    CustomerInfoFields.Comp,
        //                    CustomerInfoFields.Dept,
        //                    CustomerInfoFields.Address,
        //                    CustomerInfoFields.StartDate,
        //                    CustomerInfoFields.EndDate,
        //                    CustomerInfoFields.LimitDay,
        //                    CustomerInfoFields.Memo,
        //                    CustomerInfoFields.Reserve0);

        //                foreach (var record in records)
        //                {
        //                    writer.WriteLine("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}",
        //                        record.ParkNo,
        //                        record.CustNo,
        //                        record.GroupNo,
        //                        record.Name,
        //                        record.TelNum,
        //                        record.CarNum,
        //                        record.Comp,
        //                        record.Dept,
        //                        record.Address,
        //                        record.StartDate?.ToString("yyyy-MM-dd HH:mm"),
        //                        record.EndDate?.ToString("yyyy-MM-dd HH:mm"),
        //                        record.LimitDay,
        //                        record.Memo,
        //                        record.Reserve0);
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            _log.Warn(ex, "정기권 차량 정보를 파일에 저장하는 과정에서 오류!");

        //            if (fs != null)
        //            {
        //                fs.Dispose();
        //            }
        //        }
        //    }
        //}

        //private void OnExportGroupButtonClicked(object sender, MouseEventArgs args)
        //{
        //    SaveFileDialog dialog = new SaveFileDialog();
        //    dialog.Filter = "CSV 파일|*.csv";
        //    dialog.Title = "정기권 그룹 정보 저장";
        //    dialog.ShowDialog();

        //    _log.Debug(dialog.FileName);

        //    if (dialog.FileName.Length > 0)
        //    {
        //        FileStream fs = null;

        //        try
        //        {
        //            GroupInfoRecord[] records = this.parkDB.SelectGroupInfo(this.parkNo);

        //            fs = new FileStream(dialog.FileName, FileMode.Create);
        //            using (StreamWriter writer = new StreamWriter(fs, Encoding.Default))
        //            {
        //                writer.WriteLine("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}",
        //                    GroupInfoFields.ParkNo,
        //                    GroupInfoFields.GroupNo,
        //                    GroupInfoFields.GroupName,
        //                    GroupInfoFields.GroupType,
        //                    GroupInfoFields.Amount,
        //                    GroupInfoFields.TrfNo,
        //                    GroupInfoFields.DCNo,
        //                    GroupInfoFields.UseTime,
        //                    GroupInfoFields.Memo,
        //                    GroupInfoFields.Reserve0);

        //                foreach (var record in records)
        //                {
        //                    writer.WriteLine("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}",
        //                        record.ParkNo,
        //                        record.GroupNo,
        //                        record.GroupName,
        //                        record.GroupType,
        //                        record.Amount,
        //                        record.TrfNo,
        //                        record.DCNo,
        //                        record.UseTime,
        //                        record.Memo,
        //                        record.Reserve0);
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            _log.Warn(ex, "정기권 그룹 정보를 파일에 저장하는 과정에서 오류!");

        //            if (fs != null)
        //            {
        //                fs.Dispose();
        //            }
        //        }
        //    }
        //}

        private void Enter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.OnRunSearchButtonClicked(null, null);
            }
        }

        private void lvUp_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (this.lviewCustList.Sorting == SortOrder.Ascending)
                this.lviewCustList.Sorting = SortOrder.Descending;
            else
                this.lviewCustList.Sorting = SortOrder.Ascending;

            this.lviewCustList.ListViewItemSorter = new Sorter();
            Sorter s = (Sorter)this.lviewCustList.ListViewItemSorter;
            s.Order = this.lviewCustList.Sorting;
            s.Column = e.Column;
            this.lviewCustList.Sort();
        }
    }

    class Sorter : System.Collections.IComparer
    {
        public int Column = 0;
        public System.Windows.Forms.SortOrder Order = SortOrder.Ascending;
        public int Compare(object x, object y)
        {
            if (!(x is ListViewItem))
                return 0;
            if (!(y is ListViewItem))
                return 0;

            ListViewItem l1 = (ListViewItem)x;
            ListViewItem l2 = (ListViewItem)y;

            if (l1.ListView.Columns[Column].Tag == null)
            {
                l1.ListView.Columns[Column].Tag = "Text";
            }

            if (l1.ListView.Columns[Column].Tag.ToString() == "Numeric")
            {
                string str1 = l1.SubItems[Column].Text;
                string str2 = l2.SubItems[Column].Text;

                if (str1 == "")
                {
                    str1 = "99999";
                }
                if (str2 == "")
                {
                    str2 = "99999";
                }

                float fl1 = float.Parse(str1);
                float fl2 = float.Parse(str2);

                if (Order == SortOrder.Ascending)
                {
                    return fl1.CompareTo(fl2);
                }
                else
                {
                    return fl2.CompareTo(fl1);
                }
            }
            else
            {
                string str1 = l1.SubItems[Column].Text;
                string str2 = l2.SubItems[Column].Text;

                if (Order == SortOrder.Ascending)
                {
                    return str1.CompareTo(str2);
                }
                else
                {
                    return str2.CompareTo(str1);
                }
            }
        }
    }
}
