﻿namespace SognoLib.Customer.GUI
{
    partial class SognoLib_CustomerPayInfoViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutMain = new System.Windows.Forms.TableLayoutPanel();
            this.gbox_PayInfo = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lviewPayInfoList = new System.Windows.Forms.ListView();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.dtpPayInfo_EndDate = new System.Windows.Forms.DateTimePicker();
            this.label_Amount = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tboxPayInfo_Amount = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.dtpPayInfo_StartDate = new System.Windows.Forms.DateTimePicker();
            this.cboxPayInfo_PayType = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tboxPayInfo_Memo = new System.Windows.Forms.TextBox();
            this.label_ReturnAmount = new System.Windows.Forms.Label();
            this.tboxPayInfo_ReturnAmount = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label_PayInfoTotal = new System.Windows.Forms.Label();
            this.btnPayInfo_Delete = new System.Windows.Forms.Button();
            this.btnPayInfo_Update = new System.Windows.Forms.Button();
            this.btnPayInfo_Create = new System.Windows.Forms.Button();
            this.btnPayInfo_Clear = new System.Windows.Forms.Button();
            this.layoutMain.SuspendLayout();
            this.gbox_PayInfo.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // layoutMain
            // 
            this.layoutMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.layoutMain.ColumnCount = 1;
            this.layoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layoutMain.Controls.Add(this.gbox_PayInfo, 0, 0);
            this.layoutMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutMain.Location = new System.Drawing.Point(0, 0);
            this.layoutMain.Name = "layoutMain";
            this.layoutMain.Padding = new System.Windows.Forms.Padding(5);
            this.layoutMain.RowCount = 1;
            this.layoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 417F));
            this.layoutMain.Size = new System.Drawing.Size(1122, 425);
            this.layoutMain.TabIndex = 0;
            // 
            // gbox_PayInfo
            // 
            this.gbox_PayInfo.BackColor = System.Drawing.Color.Transparent;
            this.gbox_PayInfo.Controls.Add(this.tableLayoutPanel1);
            this.gbox_PayInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbox_PayInfo.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_PayInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(133)))), ((int)(((byte)(0)))));
            this.gbox_PayInfo.Location = new System.Drawing.Point(10, 10);
            this.gbox_PayInfo.Margin = new System.Windows.Forms.Padding(5);
            this.gbox_PayInfo.Name = "gbox_PayInfo";
            this.gbox_PayInfo.Padding = new System.Windows.Forms.Padding(10);
            this.gbox_PayInfo.Size = new System.Drawing.Size(1102, 407);
            this.gbox_PayInfo.TabIndex = 4;
            this.gbox_PayInfo.TabStop = false;
            this.gbox_PayInfo.Text = "10너7022 차량 정기권 결제 관리";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 62.33522F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.66478F));
            this.tableLayoutPanel1.Controls.Add(this.lviewPayInfoList, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 36);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1082, 361);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lviewPayInfoList
            // 
            this.lviewPayInfoList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lviewPayInfoList.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lviewPayInfoList.FullRowSelect = true;
            this.lviewPayInfoList.GridLines = true;
            this.lviewPayInfoList.HideSelection = false;
            this.lviewPayInfoList.Location = new System.Drawing.Point(6, 6);
            this.lviewPayInfoList.Margin = new System.Windows.Forms.Padding(6);
            this.lviewPayInfoList.MultiSelect = false;
            this.lviewPayInfoList.Name = "lviewPayInfoList";
            this.lviewPayInfoList.Size = new System.Drawing.Size(662, 294);
            this.lviewPayInfoList.TabIndex = 0;
            this.lviewPayInfoList.TabStop = false;
            this.lviewPayInfoList.UseCompatibleStateImageBehavior = false;
            this.lviewPayInfoList.View = System.Windows.Forms.View.Details;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.12658F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 69.87342F));
            this.tableLayoutPanel3.Controls.Add(this.dtpPayInfo_EndDate, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label_Amount, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label16, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label20, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label23, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.dtpPayInfo_StartDate, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.cboxPayInfo_PayType, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.label6, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.tboxPayInfo_Memo, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.label_ReturnAmount, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.tboxPayInfo_ReturnAmount, 1, 4);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(677, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 7;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(402, 300);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // dtpPayInfo_EndDate
            // 
            this.dtpPayInfo_EndDate.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dtpPayInfo_EndDate.Font = new System.Drawing.Font("맑은 고딕", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dtpPayInfo_EndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPayInfo_EndDate.Location = new System.Drawing.Point(124, 43);
            this.dtpPayInfo_EndDate.MaxDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dtpPayInfo_EndDate.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.dtpPayInfo_EndDate.Name = "dtpPayInfo_EndDate";
            this.dtpPayInfo_EndDate.Size = new System.Drawing.Size(236, 26);
            this.dtpPayInfo_EndDate.TabIndex = 62;
            this.dtpPayInfo_EndDate.TabStop = false;
            this.dtpPayInfo_EndDate.Tag = "end";
            // 
            // label_Amount
            // 
            this.label_Amount.AutoSize = true;
            this.label_Amount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_Amount.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label_Amount.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label_Amount.Location = new System.Drawing.Point(3, 80);
            this.label_Amount.Name = "label_Amount";
            this.label_Amount.Size = new System.Drawing.Size(115, 40);
            this.label_Amount.TabIndex = 18;
            this.label_Amount.Text = "결제 요금(*) :";
            this.label_Amount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label16.Location = new System.Drawing.Point(3, 120);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(115, 40);
            this.label16.TabIndex = 7;
            this.label16.Text = "결제 형식(*) :";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label20.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label20.Location = new System.Drawing.Point(3, 40);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(115, 40);
            this.label20.TabIndex = 5;
            this.label20.Text = "종료일(*) :";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label23.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label23.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label23.Location = new System.Drawing.Point(3, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(115, 40);
            this.label23.TabIndex = 3;
            this.label23.Text = "시작일(*) :";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75.20661F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.79339F));
            this.tableLayoutPanel4.Controls.Add(this.tboxPayInfo_Amount, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label24, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(121, 80);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(281, 40);
            this.tableLayoutPanel4.TabIndex = 19;
            // 
            // tboxPayInfo_Amount
            // 
            this.tboxPayInfo_Amount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tboxPayInfo_Amount.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxPayInfo_Amount.Location = new System.Drawing.Point(3, 6);
            this.tboxPayInfo_Amount.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.tboxPayInfo_Amount.Name = "tboxPayInfo_Amount";
            this.tboxPayInfo_Amount.Size = new System.Drawing.Size(205, 29);
            this.tboxPayInfo_Amount.TabIndex = 6;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label24.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label24.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label24.Location = new System.Drawing.Point(214, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(64, 40);
            this.label24.TabIndex = 0;
            this.label24.Text = " 원";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dtpPayInfo_StartDate
            // 
            this.dtpPayInfo_StartDate.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dtpPayInfo_StartDate.Font = new System.Drawing.Font("맑은 고딕", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dtpPayInfo_StartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpPayInfo_StartDate.Location = new System.Drawing.Point(124, 3);
            this.dtpPayInfo_StartDate.MaxDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dtpPayInfo_StartDate.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.dtpPayInfo_StartDate.Name = "dtpPayInfo_StartDate";
            this.dtpPayInfo_StartDate.Size = new System.Drawing.Size(238, 26);
            this.dtpPayInfo_StartDate.TabIndex = 61;
            this.dtpPayInfo_StartDate.TabStop = false;
            this.dtpPayInfo_StartDate.Tag = "start";
            // 
            // cboxPayInfo_PayType
            // 
            this.cboxPayInfo_PayType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboxPayInfo_PayType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxPayInfo_PayType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboxPayInfo_PayType.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cboxPayInfo_PayType.FormattingEnabled = true;
            this.cboxPayInfo_PayType.Location = new System.Drawing.Point(124, 123);
            this.cboxPayInfo_PayType.Name = "cboxPayInfo_PayType";
            this.cboxPayInfo_PayType.Size = new System.Drawing.Size(275, 29);
            this.cboxPayInfo_PayType.TabIndex = 63;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label6.Location = new System.Drawing.Point(3, 200);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(115, 40);
            this.label6.TabIndex = 9;
            this.label6.Text = "메모 :";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tboxPayInfo_Memo
            // 
            this.tboxPayInfo_Memo.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxPayInfo_Memo.Location = new System.Drawing.Point(124, 206);
            this.tboxPayInfo_Memo.Margin = new System.Windows.Forms.Padding(3, 6, 3, 6);
            this.tboxPayInfo_Memo.Multiline = true;
            this.tboxPayInfo_Memo.Name = "tboxPayInfo_Memo";
            this.tableLayoutPanel3.SetRowSpan(this.tboxPayInfo_Memo, 2);
            this.tboxPayInfo_Memo.Size = new System.Drawing.Size(275, 88);
            this.tboxPayInfo_Memo.TabIndex = 10;
            // 
            // label_ReturnAmount
            // 
            this.label_ReturnAmount.AutoSize = true;
            this.label_ReturnAmount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_ReturnAmount.Enabled = false;
            this.label_ReturnAmount.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label_ReturnAmount.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label_ReturnAmount.Location = new System.Drawing.Point(3, 160);
            this.label_ReturnAmount.Name = "label_ReturnAmount";
            this.label_ReturnAmount.Size = new System.Drawing.Size(115, 40);
            this.label_ReturnAmount.TabIndex = 64;
            this.label_ReturnAmount.Text = "환급 금액(*)";
            this.label_ReturnAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tboxPayInfo_ReturnAmount
            // 
            this.tboxPayInfo_ReturnAmount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tboxPayInfo_ReturnAmount.Enabled = false;
            this.tboxPayInfo_ReturnAmount.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxPayInfo_ReturnAmount.Location = new System.Drawing.Point(124, 163);
            this.tboxPayInfo_ReturnAmount.Name = "tboxPayInfo_ReturnAmount";
            this.tboxPayInfo_ReturnAmount.Size = new System.Drawing.Size(275, 29);
            this.tboxPayInfo_ReturnAmount.TabIndex = 65;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel1.SetColumnSpan(this.tableLayoutPanel2, 2);
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.00049F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0005F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.0005F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.9985F));
            this.tableLayoutPanel2.Controls.Add(this.label_PayInfoTotal, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnPayInfo_Delete, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnPayInfo_Update, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnPayInfo_Create, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnPayInfo_Clear, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 312);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(6);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1070, 43);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // label_PayInfoTotal
            // 
            this.label_PayInfoTotal.AutoSize = true;
            this.label_PayInfoTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_PayInfoTotal.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label_PayInfoTotal.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label_PayInfoTotal.Location = new System.Drawing.Point(3, 0);
            this.label_PayInfoTotal.Name = "label_PayInfoTotal";
            this.label_PayInfoTotal.Size = new System.Drawing.Size(208, 43);
            this.label_PayInfoTotal.TabIndex = 63;
            this.label_PayInfoTotal.Text = "합계 :";
            this.label_PayInfoTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnPayInfo_Delete
            // 
            this.btnPayInfo_Delete.BackColor = System.Drawing.Color.SlateGray;
            this.btnPayInfo_Delete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPayInfo_Delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPayInfo_Delete.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnPayInfo_Delete.Location = new System.Drawing.Point(862, 3);
            this.btnPayInfo_Delete.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnPayInfo_Delete.Name = "btnPayInfo_Delete";
            this.btnPayInfo_Delete.Size = new System.Drawing.Size(202, 37);
            this.btnPayInfo_Delete.TabIndex = 3;
            this.btnPayInfo_Delete.TabStop = false;
            this.btnPayInfo_Delete.Text = "삭 제";
            this.btnPayInfo_Delete.UseVisualStyleBackColor = false;
            // 
            // btnPayInfo_Update
            // 
            this.btnPayInfo_Update.BackColor = System.Drawing.Color.SlateGray;
            this.btnPayInfo_Update.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPayInfo_Update.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPayInfo_Update.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnPayInfo_Update.Location = new System.Drawing.Point(648, 3);
            this.btnPayInfo_Update.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnPayInfo_Update.Name = "btnPayInfo_Update";
            this.btnPayInfo_Update.Size = new System.Drawing.Size(202, 37);
            this.btnPayInfo_Update.TabIndex = 2;
            this.btnPayInfo_Update.TabStop = false;
            this.btnPayInfo_Update.Text = "수정 / 저장";
            this.btnPayInfo_Update.UseVisualStyleBackColor = false;
            // 
            // btnPayInfo_Create
            // 
            this.btnPayInfo_Create.BackColor = System.Drawing.Color.SlateGray;
            this.btnPayInfo_Create.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPayInfo_Create.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPayInfo_Create.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnPayInfo_Create.Location = new System.Drawing.Point(434, 3);
            this.btnPayInfo_Create.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnPayInfo_Create.Name = "btnPayInfo_Create";
            this.btnPayInfo_Create.Size = new System.Drawing.Size(202, 37);
            this.btnPayInfo_Create.TabIndex = 1;
            this.btnPayInfo_Create.TabStop = false;
            this.btnPayInfo_Create.Text = "추가";
            this.btnPayInfo_Create.UseVisualStyleBackColor = false;
            // 
            // btnPayInfo_Clear
            // 
            this.btnPayInfo_Clear.BackColor = System.Drawing.Color.SlateGray;
            this.btnPayInfo_Clear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnPayInfo_Clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPayInfo_Clear.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnPayInfo_Clear.Location = new System.Drawing.Point(220, 3);
            this.btnPayInfo_Clear.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnPayInfo_Clear.Name = "btnPayInfo_Clear";
            this.btnPayInfo_Clear.Size = new System.Drawing.Size(202, 37);
            this.btnPayInfo_Clear.TabIndex = 5;
            this.btnPayInfo_Clear.TabStop = false;
            this.btnPayInfo_Clear.Text = "항목 초기화";
            this.btnPayInfo_Clear.UseVisualStyleBackColor = false;
            // 
            // SognoLib_CustomerPayInfoViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1122, 425);
            this.Controls.Add(this.layoutMain);
            this.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "SognoLib_CustomerPayInfoViewer";
            this.Text = "정기권 결제 관리";
            this.TopMost = true;
            this.layoutMain.ResumeLayout(false);
            this.gbox_PayInfo.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel layoutMain;
        public System.Windows.Forms.GroupBox gbox_PayInfo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.ListView lviewPayInfoList;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        public System.Windows.Forms.Label label_Amount;
        public System.Windows.Forms.TextBox tboxPayInfo_Memo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        public System.Windows.Forms.TextBox tboxPayInfo_Amount;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public System.Windows.Forms.Button btnPayInfo_Clear;
        public System.Windows.Forms.Button btnPayInfo_Delete;
        public System.Windows.Forms.Button btnPayInfo_Update;
        public System.Windows.Forms.Button btnPayInfo_Create;
        public System.Windows.Forms.DateTimePicker dtpPayInfo_EndDate;
        public System.Windows.Forms.DateTimePicker dtpPayInfo_StartDate;
        public System.Windows.Forms.Label label_PayInfoTotal;
        public System.Windows.Forms.ComboBox cboxPayInfo_PayType;
        public System.Windows.Forms.Label label_ReturnAmount;
        public System.Windows.Forms.TextBox tboxPayInfo_ReturnAmount;
    }
}