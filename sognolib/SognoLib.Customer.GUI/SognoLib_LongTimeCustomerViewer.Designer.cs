﻿namespace SognoLib.Customer.GUI
{
    partial class SognoLib_LongTimeCustomerViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutMain = new System.Windows.Forms.TableLayoutPanel();
            this.gbox_CustomerInfo = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.chboxCustomer_Limit_Sunday = new System.Windows.Forms.CheckBox();
            this.chboxCustomer_Limit_Saturday = new System.Windows.Forms.CheckBox();
            this.chboxCustomer_Limit_Friday = new System.Windows.Forms.CheckBox();
            this.chboxCustomer_Limit_Thursday = new System.Windows.Forms.CheckBox();
            this.chboxCustomer_Limit_Wednesday = new System.Windows.Forms.CheckBox();
            this.chboxCustomer_Limit_Tuesday = new System.Windows.Forms.CheckBox();
            this.chboxCustomer_Limit_Monday = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.tboxCustomer_CarNum = new System.Windows.Forms.TextBox();
            this.btnCustomer_CheckCarNum = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.tboxCustomer_Memo = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.chboxCustomer_Limit_Every7 = new System.Windows.Forms.CheckBox();
            this.chboxCustomer_Limit_Every6 = new System.Windows.Forms.CheckBox();
            this.chboxCustomer_Limit_Every8 = new System.Windows.Forms.CheckBox();
            this.chboxCustomer_Limit_Every0 = new System.Windows.Forms.CheckBox();
            this.chboxCustomer_Limit_Every9 = new System.Windows.Forms.CheckBox();
            this.chboxCustomer_Limit_Every2 = new System.Windows.Forms.CheckBox();
            this.chboxCustomer_Limit_Every1 = new System.Windows.Forms.CheckBox();
            this.chboxCustomer_Limit_Every3 = new System.Windows.Forms.CheckBox();
            this.chboxCustomer_Limit_Every5 = new System.Windows.Forms.CheckBox();
            this.chboxCustomer_Limit_Every4 = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.dtpCustomer_StartDate = new System.Windows.Forms.DateTimePicker();
            this.label18 = new System.Windows.Forms.Label();
            this.dtpCustomer_EndDate = new System.Windows.Forms.DateTimePicker();
            this.label19 = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCustomer_Extend_Clear = new System.Windows.Forms.Button();
            this.btnCustomer_Extend_1Year = new System.Windows.Forms.Button();
            this.btnCustomer_Extend_6Mon = new System.Windows.Forms.Button();
            this.btnCustomer_Extend_1Mon = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.cboxCustomer_DCName = new System.Windows.Forms.ComboBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label25 = new System.Windows.Forms.Label();
            this.tboxCustomer_Extend_Rates = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.tboxCustomer_Extend_Monthes = new System.Windows.Forms.TextBox();
            this.chboxCustomer_Extend_Log = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cboxCustomer_PayType = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpCustomer_PayedDate = new System.Windows.Forms.DateTimePicker();
            this.label16 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tboxCustomer_Name = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tboxCustomer_PhoneNum = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tboxCustomer_Comp = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tboxCustomer_Dept = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cboxCustomer_CarType = new System.Windows.Forms.ComboBox();
            this.tboxCustomer_Addr = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.btnTotalPop = new System.Windows.Forms.Button();
            this.btnCustomer_Clear = new System.Windows.Forms.Button();
            this.btnCustomer_Update = new System.Windows.Forms.Button();
            this.btnCustomer_Create = new System.Windows.Forms.Button();
            this.btnCustomer_Delete = new System.Windows.Forms.Button();
            this.btnCustomer_Close = new System.Windows.Forms.Button();
            this.btnCustomer_Import = new System.Windows.Forms.Button();
            this.btnCustomer_Export = new System.Windows.Forms.Button();
            this.tboxCustomer_ReturnRates = new System.Windows.Forms.TextBox();
            this.btnCustomer_Return = new System.Windows.Forms.Button();
            this.gboxGroupInfo = new System.Windows.Forms.GroupBox();
            this.layoutGroupInfo = new System.Windows.Forms.TableLayoutPanel();
            this.lviewHistoryList = new System.Windows.Forms.ListView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.layoutMainLeft = new System.Windows.Forms.TableLayoutPanel();
            this.layoutCustSearch = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.tboxSearch_CarNum = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.cboxSearch_ParkName = new System.Windows.Forms.ComboBox();
            this.layoutSearchButtons = new System.Windows.Forms.TableLayoutPanel();
            this.btnSearch_Clear = new System.Windows.Forms.Button();
            this.btnSearch_Run = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tboxSearch_Name = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cboxSearch_State = new System.Windows.Forms.ComboBox();
            this.lviewCustList = new System.Windows.Forms.ListView();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnOneStopView = new System.Windows.Forms.Button();
            this.btnCustomerView = new System.Windows.Forms.Button();
            this.layoutMain.SuspendLayout();
            this.gbox_CustomerInfo.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.gboxGroupInfo.SuspendLayout();
            this.layoutGroupInfo.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.layoutMainLeft.SuspendLayout();
            this.layoutCustSearch.SuspendLayout();
            this.layoutSearchButtons.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // layoutMain
            // 
            this.layoutMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.layoutMain.ColumnCount = 2;
            this.layoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.78312F));
            this.layoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.21688F));
            this.layoutMain.Controls.Add(this.gbox_CustomerInfo, 1, 2);
            this.layoutMain.Controls.Add(this.gboxGroupInfo, 1, 1);
            this.layoutMain.Controls.Add(this.groupBox1, 0, 1);
            this.layoutMain.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.layoutMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutMain.Location = new System.Drawing.Point(0, 0);
            this.layoutMain.Name = "layoutMain";
            this.layoutMain.Padding = new System.Windows.Forms.Padding(5);
            this.layoutMain.RowCount = 3;
            this.layoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.48951F));
            this.layoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 89.51049F));
            this.layoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 707F));
            this.layoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.layoutMain.Size = new System.Drawing.Size(1361, 1061);
            this.layoutMain.TabIndex = 0;
            // 
            // gbox_CustomerInfo
            // 
            this.gbox_CustomerInfo.Controls.Add(this.tableLayoutPanel3);
            this.gbox_CustomerInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbox_CustomerInfo.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CustomerInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(236)))), ((int)(((byte)(219)))));
            this.gbox_CustomerInfo.Location = new System.Drawing.Point(597, 363);
            this.gbox_CustomerInfo.Margin = new System.Windows.Forms.Padding(15);
            this.gbox_CustomerInfo.Name = "gbox_CustomerInfo";
            this.gbox_CustomerInfo.Padding = new System.Windows.Forms.Padding(10);
            this.gbox_CustomerInfo.Size = new System.Drawing.Size(744, 678);
            this.gbox_CustomerInfo.TabIndex = 2;
            this.gbox_CustomerInfo.TabStop = false;
            this.gbox_CustomerInfo.Text = "3. 장기주차 차량 관리";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel5, 1, 9);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel8, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.label21, 0, 12);
            this.tableLayoutPanel3.Controls.Add(this.tboxCustomer_Memo, 1, 12);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel7, 1, 10);
            this.tableLayoutPanel3.Controls.Add(this.label20, 0, 9);
            this.tableLayoutPanel3.Controls.Add(this.label10, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label17, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.dtpCustomer_StartDate, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label18, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.dtpCustomer_EndDate, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.label19, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.label4, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.cboxCustomer_DCName, 3, 3);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel1, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.cboxCustomer_PayType, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.label6, 2, 5);
            this.tableLayoutPanel3.Controls.Add(this.dtpCustomer_PayedDate, 3, 5);
            this.tableLayoutPanel3.Controls.Add(this.label16, 0, 8);
            this.tableLayoutPanel3.Controls.Add(this.label12, 0, 6);
            this.tableLayoutPanel3.Controls.Add(this.tboxCustomer_Name, 1, 6);
            this.tableLayoutPanel3.Controls.Add(this.label13, 2, 6);
            this.tableLayoutPanel3.Controls.Add(this.tboxCustomer_PhoneNum, 3, 6);
            this.tableLayoutPanel3.Controls.Add(this.label14, 0, 7);
            this.tableLayoutPanel3.Controls.Add(this.tboxCustomer_Comp, 1, 7);
            this.tableLayoutPanel3.Controls.Add(this.label15, 2, 7);
            this.tableLayoutPanel3.Controls.Add(this.tboxCustomer_Dept, 3, 7);
            this.tableLayoutPanel3.Controls.Add(this.label7, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.cboxCustomer_CarType, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.tboxCustomer_Addr, 1, 8);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel6, 0, 13);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(10, 36);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 15;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 53F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 86F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(724, 632);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 7;
            this.tableLayoutPanel3.SetColumnSpan(this.tableLayoutPanel5, 3);
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel5.Controls.Add(this.chboxCustomer_Limit_Sunday, 6, 0);
            this.tableLayoutPanel5.Controls.Add(this.chboxCustomer_Limit_Saturday, 5, 0);
            this.tableLayoutPanel5.Controls.Add(this.chboxCustomer_Limit_Friday, 4, 0);
            this.tableLayoutPanel5.Controls.Add(this.chboxCustomer_Limit_Thursday, 3, 0);
            this.tableLayoutPanel5.Controls.Add(this.chboxCustomer_Limit_Wednesday, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.chboxCustomer_Limit_Tuesday, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.chboxCustomer_Limit_Monday, 0, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(147, 368);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(574, 33);
            this.tableLayoutPanel5.TabIndex = 39;
            // 
            // chboxCustomer_Limit_Sunday
            // 
            this.chboxCustomer_Limit_Sunday.AutoSize = true;
            this.chboxCustomer_Limit_Sunday.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chboxCustomer_Limit_Sunday.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.chboxCustomer_Limit_Sunday.Location = new System.Drawing.Point(489, 3);
            this.chboxCustomer_Limit_Sunday.Name = "chboxCustomer_Limit_Sunday";
            this.chboxCustomer_Limit_Sunday.Size = new System.Drawing.Size(77, 25);
            this.chboxCustomer_Limit_Sunday.TabIndex = 6;
            this.chboxCustomer_Limit_Sunday.TabStop = false;
            this.chboxCustomer_Limit_Sunday.Text = "일요일";
            this.chboxCustomer_Limit_Sunday.UseVisualStyleBackColor = true;
            // 
            // chboxCustomer_Limit_Saturday
            // 
            this.chboxCustomer_Limit_Saturday.AutoSize = true;
            this.chboxCustomer_Limit_Saturday.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chboxCustomer_Limit_Saturday.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.chboxCustomer_Limit_Saturday.Location = new System.Drawing.Point(408, 3);
            this.chboxCustomer_Limit_Saturday.Name = "chboxCustomer_Limit_Saturday";
            this.chboxCustomer_Limit_Saturday.Size = new System.Drawing.Size(75, 25);
            this.chboxCustomer_Limit_Saturday.TabIndex = 5;
            this.chboxCustomer_Limit_Saturday.TabStop = false;
            this.chboxCustomer_Limit_Saturday.Text = "토요일";
            this.chboxCustomer_Limit_Saturday.UseVisualStyleBackColor = true;
            // 
            // chboxCustomer_Limit_Friday
            // 
            this.chboxCustomer_Limit_Friday.AutoSize = true;
            this.chboxCustomer_Limit_Friday.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chboxCustomer_Limit_Friday.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.chboxCustomer_Limit_Friday.Location = new System.Drawing.Point(327, 3);
            this.chboxCustomer_Limit_Friday.Name = "chboxCustomer_Limit_Friday";
            this.chboxCustomer_Limit_Friday.Size = new System.Drawing.Size(75, 25);
            this.chboxCustomer_Limit_Friday.TabIndex = 4;
            this.chboxCustomer_Limit_Friday.TabStop = false;
            this.chboxCustomer_Limit_Friday.Text = "금요일";
            this.chboxCustomer_Limit_Friday.UseVisualStyleBackColor = true;
            // 
            // chboxCustomer_Limit_Thursday
            // 
            this.chboxCustomer_Limit_Thursday.AutoSize = true;
            this.chboxCustomer_Limit_Thursday.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chboxCustomer_Limit_Thursday.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.chboxCustomer_Limit_Thursday.Location = new System.Drawing.Point(246, 3);
            this.chboxCustomer_Limit_Thursday.Name = "chboxCustomer_Limit_Thursday";
            this.chboxCustomer_Limit_Thursday.Size = new System.Drawing.Size(75, 25);
            this.chboxCustomer_Limit_Thursday.TabIndex = 3;
            this.chboxCustomer_Limit_Thursday.TabStop = false;
            this.chboxCustomer_Limit_Thursday.Text = "목요일";
            this.chboxCustomer_Limit_Thursday.UseVisualStyleBackColor = true;
            // 
            // chboxCustomer_Limit_Wednesday
            // 
            this.chboxCustomer_Limit_Wednesday.AutoSize = true;
            this.chboxCustomer_Limit_Wednesday.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chboxCustomer_Limit_Wednesday.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.chboxCustomer_Limit_Wednesday.Location = new System.Drawing.Point(165, 3);
            this.chboxCustomer_Limit_Wednesday.Name = "chboxCustomer_Limit_Wednesday";
            this.chboxCustomer_Limit_Wednesday.Size = new System.Drawing.Size(75, 25);
            this.chboxCustomer_Limit_Wednesday.TabIndex = 2;
            this.chboxCustomer_Limit_Wednesday.TabStop = false;
            this.chboxCustomer_Limit_Wednesday.Text = "수요일";
            this.chboxCustomer_Limit_Wednesday.UseVisualStyleBackColor = true;
            // 
            // chboxCustomer_Limit_Tuesday
            // 
            this.chboxCustomer_Limit_Tuesday.AutoSize = true;
            this.chboxCustomer_Limit_Tuesday.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chboxCustomer_Limit_Tuesday.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.chboxCustomer_Limit_Tuesday.Location = new System.Drawing.Point(84, 3);
            this.chboxCustomer_Limit_Tuesday.Name = "chboxCustomer_Limit_Tuesday";
            this.chboxCustomer_Limit_Tuesday.Size = new System.Drawing.Size(75, 25);
            this.chboxCustomer_Limit_Tuesday.TabIndex = 1;
            this.chboxCustomer_Limit_Tuesday.TabStop = false;
            this.chboxCustomer_Limit_Tuesday.Text = "화요일";
            this.chboxCustomer_Limit_Tuesday.UseVisualStyleBackColor = true;
            // 
            // chboxCustomer_Limit_Monday
            // 
            this.chboxCustomer_Limit_Monday.AutoSize = true;
            this.chboxCustomer_Limit_Monday.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chboxCustomer_Limit_Monday.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.chboxCustomer_Limit_Monday.Location = new System.Drawing.Point(3, 3);
            this.chboxCustomer_Limit_Monday.Name = "chboxCustomer_Limit_Monday";
            this.chboxCustomer_Limit_Monday.Size = new System.Drawing.Size(75, 25);
            this.chboxCustomer_Limit_Monday.TabIndex = 0;
            this.chboxCustomer_Limit_Monday.TabStop = false;
            this.chboxCustomer_Limit_Monday.Text = "월요일";
            this.chboxCustomer_Limit_Monday.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel8.Controls.Add(this.tboxCustomer_CarNum, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.btnCustomer_CheckCarNum, 1, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(505, 0);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(219, 40);
            this.tableLayoutPanel8.TabIndex = 34;
            // 
            // tboxCustomer_CarNum
            // 
            this.tboxCustomer_CarNum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tboxCustomer_CarNum.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxCustomer_CarNum.Location = new System.Drawing.Point(3, 6);
            this.tboxCustomer_CarNum.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tboxCustomer_CarNum.Name = "tboxCustomer_CarNum";
            this.tboxCustomer_CarNum.Size = new System.Drawing.Size(113, 29);
            this.tboxCustomer_CarNum.TabIndex = 12;
            // 
            // btnCustomer_CheckCarNum
            // 
            this.btnCustomer_CheckCarNum.BackColor = System.Drawing.Color.SteelBlue;
            this.btnCustomer_CheckCarNum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCustomer_CheckCarNum.FlatAppearance.BorderSize = 0;
            this.btnCustomer_CheckCarNum.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomer_CheckCarNum.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCustomer_CheckCarNum.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomer_CheckCarNum.Location = new System.Drawing.Point(122, 3);
            this.btnCustomer_CheckCarNum.Name = "btnCustomer_CheckCarNum";
            this.btnCustomer_CheckCarNum.Size = new System.Drawing.Size(94, 34);
            this.btnCustomer_CheckCarNum.TabIndex = 13;
            this.btnCustomer_CheckCarNum.Text = "중복확인";
            this.btnCustomer_CheckCarNum.UseVisualStyleBackColor = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label21.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label21.Location = new System.Drawing.Point(3, 478);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(102, 21);
            this.label21.TabIndex = 26;
            this.label21.Text = "메모(비고) : ";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tboxCustomer_Memo
            // 
            this.tableLayoutPanel3.SetColumnSpan(this.tboxCustomer_Memo, 3);
            this.tboxCustomer_Memo.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxCustomer_Memo.Location = new System.Drawing.Point(147, 484);
            this.tboxCustomer_Memo.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tboxCustomer_Memo.Multiline = true;
            this.tboxCustomer_Memo.Name = "tboxCustomer_Memo";
            this.tboxCustomer_Memo.Size = new System.Drawing.Size(574, 23);
            this.tboxCustomer_Memo.TabIndex = 22;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 10;
            this.tableLayoutPanel3.SetColumnSpan(this.tableLayoutPanel7, 3);
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel7.Controls.Add(this.chboxCustomer_Limit_Every7, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.chboxCustomer_Limit_Every6, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.chboxCustomer_Limit_Every8, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.chboxCustomer_Limit_Every0, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.chboxCustomer_Limit_Every9, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.chboxCustomer_Limit_Every2, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.chboxCustomer_Limit_Every1, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.chboxCustomer_Limit_Every3, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.chboxCustomer_Limit_Every5, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.chboxCustomer_Limit_Every4, 0, 0);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(147, 409);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(574, 32);
            this.tableLayoutPanel7.TabIndex = 38;
            // 
            // chboxCustomer_Limit_Every7
            // 
            this.chboxCustomer_Limit_Every7.AutoSize = true;
            this.chboxCustomer_Limit_Every7.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chboxCustomer_Limit_Every7.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.chboxCustomer_Limit_Every7.Location = new System.Drawing.Point(345, 3);
            this.chboxCustomer_Limit_Every7.Name = "chboxCustomer_Limit_Every7";
            this.chboxCustomer_Limit_Every7.Size = new System.Drawing.Size(51, 25);
            this.chboxCustomer_Limit_Every7.TabIndex = 10;
            this.chboxCustomer_Limit_Every7.TabStop = false;
            this.chboxCustomer_Limit_Every7.Text = "7일";
            this.chboxCustomer_Limit_Every7.UseVisualStyleBackColor = true;
            // 
            // chboxCustomer_Limit_Every6
            // 
            this.chboxCustomer_Limit_Every6.AutoSize = true;
            this.chboxCustomer_Limit_Every6.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chboxCustomer_Limit_Every6.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.chboxCustomer_Limit_Every6.Location = new System.Drawing.Point(288, 3);
            this.chboxCustomer_Limit_Every6.Name = "chboxCustomer_Limit_Every6";
            this.chboxCustomer_Limit_Every6.Size = new System.Drawing.Size(51, 25);
            this.chboxCustomer_Limit_Every6.TabIndex = 9;
            this.chboxCustomer_Limit_Every6.TabStop = false;
            this.chboxCustomer_Limit_Every6.Text = "6일";
            this.chboxCustomer_Limit_Every6.UseVisualStyleBackColor = true;
            // 
            // chboxCustomer_Limit_Every8
            // 
            this.chboxCustomer_Limit_Every8.AutoSize = true;
            this.chboxCustomer_Limit_Every8.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chboxCustomer_Limit_Every8.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.chboxCustomer_Limit_Every8.Location = new System.Drawing.Point(402, 3);
            this.chboxCustomer_Limit_Every8.Name = "chboxCustomer_Limit_Every8";
            this.chboxCustomer_Limit_Every8.Size = new System.Drawing.Size(51, 25);
            this.chboxCustomer_Limit_Every8.TabIndex = 8;
            this.chboxCustomer_Limit_Every8.TabStop = false;
            this.chboxCustomer_Limit_Every8.Text = "8일";
            this.chboxCustomer_Limit_Every8.UseVisualStyleBackColor = true;
            // 
            // chboxCustomer_Limit_Every0
            // 
            this.chboxCustomer_Limit_Every0.AutoSize = true;
            this.chboxCustomer_Limit_Every0.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chboxCustomer_Limit_Every0.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.chboxCustomer_Limit_Every0.Location = new System.Drawing.Point(516, 3);
            this.chboxCustomer_Limit_Every0.Name = "chboxCustomer_Limit_Every0";
            this.chboxCustomer_Limit_Every0.Size = new System.Drawing.Size(54, 25);
            this.chboxCustomer_Limit_Every0.TabIndex = 7;
            this.chboxCustomer_Limit_Every0.TabStop = false;
            this.chboxCustomer_Limit_Every0.Text = "0일";
            this.chboxCustomer_Limit_Every0.UseVisualStyleBackColor = true;
            // 
            // chboxCustomer_Limit_Every9
            // 
            this.chboxCustomer_Limit_Every9.AutoSize = true;
            this.chboxCustomer_Limit_Every9.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chboxCustomer_Limit_Every9.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.chboxCustomer_Limit_Every9.Location = new System.Drawing.Point(459, 3);
            this.chboxCustomer_Limit_Every9.Name = "chboxCustomer_Limit_Every9";
            this.chboxCustomer_Limit_Every9.Size = new System.Drawing.Size(51, 25);
            this.chboxCustomer_Limit_Every9.TabIndex = 6;
            this.chboxCustomer_Limit_Every9.TabStop = false;
            this.chboxCustomer_Limit_Every9.Text = "9일";
            this.chboxCustomer_Limit_Every9.UseVisualStyleBackColor = true;
            // 
            // chboxCustomer_Limit_Every2
            // 
            this.chboxCustomer_Limit_Every2.AutoSize = true;
            this.chboxCustomer_Limit_Every2.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chboxCustomer_Limit_Every2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.chboxCustomer_Limit_Every2.Location = new System.Drawing.Point(60, 3);
            this.chboxCustomer_Limit_Every2.Name = "chboxCustomer_Limit_Every2";
            this.chboxCustomer_Limit_Every2.Size = new System.Drawing.Size(51, 25);
            this.chboxCustomer_Limit_Every2.TabIndex = 5;
            this.chboxCustomer_Limit_Every2.TabStop = false;
            this.chboxCustomer_Limit_Every2.Text = "2일";
            this.chboxCustomer_Limit_Every2.UseVisualStyleBackColor = true;
            // 
            // chboxCustomer_Limit_Every1
            // 
            this.chboxCustomer_Limit_Every1.AutoSize = true;
            this.chboxCustomer_Limit_Every1.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chboxCustomer_Limit_Every1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.chboxCustomer_Limit_Every1.Location = new System.Drawing.Point(3, 3);
            this.chboxCustomer_Limit_Every1.Name = "chboxCustomer_Limit_Every1";
            this.chboxCustomer_Limit_Every1.Size = new System.Drawing.Size(51, 25);
            this.chboxCustomer_Limit_Every1.TabIndex = 4;
            this.chboxCustomer_Limit_Every1.TabStop = false;
            this.chboxCustomer_Limit_Every1.Text = "1일";
            this.chboxCustomer_Limit_Every1.UseVisualStyleBackColor = true;
            // 
            // chboxCustomer_Limit_Every3
            // 
            this.chboxCustomer_Limit_Every3.AutoSize = true;
            this.chboxCustomer_Limit_Every3.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chboxCustomer_Limit_Every3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.chboxCustomer_Limit_Every3.Location = new System.Drawing.Point(117, 3);
            this.chboxCustomer_Limit_Every3.Name = "chboxCustomer_Limit_Every3";
            this.chboxCustomer_Limit_Every3.Size = new System.Drawing.Size(51, 25);
            this.chboxCustomer_Limit_Every3.TabIndex = 3;
            this.chboxCustomer_Limit_Every3.TabStop = false;
            this.chboxCustomer_Limit_Every3.Text = "3일";
            this.chboxCustomer_Limit_Every3.UseVisualStyleBackColor = true;
            // 
            // chboxCustomer_Limit_Every5
            // 
            this.chboxCustomer_Limit_Every5.AutoSize = true;
            this.chboxCustomer_Limit_Every5.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chboxCustomer_Limit_Every5.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.chboxCustomer_Limit_Every5.Location = new System.Drawing.Point(231, 3);
            this.chboxCustomer_Limit_Every5.Name = "chboxCustomer_Limit_Every5";
            this.chboxCustomer_Limit_Every5.Size = new System.Drawing.Size(51, 25);
            this.chboxCustomer_Limit_Every5.TabIndex = 2;
            this.chboxCustomer_Limit_Every5.TabStop = false;
            this.chboxCustomer_Limit_Every5.Text = "5일";
            this.chboxCustomer_Limit_Every5.UseVisualStyleBackColor = true;
            // 
            // chboxCustomer_Limit_Every4
            // 
            this.chboxCustomer_Limit_Every4.AutoSize = true;
            this.chboxCustomer_Limit_Every4.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chboxCustomer_Limit_Every4.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.chboxCustomer_Limit_Every4.Location = new System.Drawing.Point(174, 3);
            this.chboxCustomer_Limit_Every4.Name = "chboxCustomer_Limit_Every4";
            this.chboxCustomer_Limit_Every4.Size = new System.Drawing.Size(51, 25);
            this.chboxCustomer_Limit_Every4.TabIndex = 1;
            this.chboxCustomer_Limit_Every4.TabStop = false;
            this.chboxCustomer_Limit_Every4.Text = "4일";
            this.chboxCustomer_Limit_Every4.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label20.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label20.Location = new System.Drawing.Point(3, 365);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(112, 21);
            this.label20.TabIndex = 37;
            this.label20.Text = "입차 제한일 : ";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label10.Location = new System.Drawing.Point(364, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(115, 21);
            this.label10.TabIndex = 0;
            this.label10.Text = "차량 번호(*) : ";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label17.Location = new System.Drawing.Point(3, 40);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(115, 21);
            this.label17.TabIndex = 17;
            this.label17.Text = "시작 일시(*) : ";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpCustomer_StartDate
            // 
            this.dtpCustomer_StartDate.CustomFormat = "yyyy-MM-dd (dddd)";
            this.dtpCustomer_StartDate.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dtpCustomer_StartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCustomer_StartDate.Location = new System.Drawing.Point(147, 43);
            this.dtpCustomer_StartDate.MaxDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dtpCustomer_StartDate.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.dtpCustomer_StartDate.Name = "dtpCustomer_StartDate";
            this.dtpCustomer_StartDate.Size = new System.Drawing.Size(211, 29);
            this.dtpCustomer_StartDate.TabIndex = 32;
            this.dtpCustomer_StartDate.TabStop = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label18.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label18.Location = new System.Drawing.Point(364, 40);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(115, 21);
            this.label18.TabIndex = 19;
            this.label18.Text = "종료 일시(*) : ";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpCustomer_EndDate
            // 
            this.dtpCustomer_EndDate.CustomFormat = "yyyy-MM-dd (dddd)";
            this.dtpCustomer_EndDate.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dtpCustomer_EndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCustomer_EndDate.Location = new System.Drawing.Point(508, 43);
            this.dtpCustomer_EndDate.MaxDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dtpCustomer_EndDate.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.dtpCustomer_EndDate.Name = "dtpCustomer_EndDate";
            this.dtpCustomer_EndDate.Size = new System.Drawing.Size(213, 29);
            this.dtpCustomer_EndDate.TabIndex = 33;
            this.dtpCustomer_EndDate.TabStop = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label19.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label19.Location = new System.Drawing.Point(3, 81);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(112, 21);
            this.label19.TabIndex = 30;
            this.label19.Text = "종료일 연장 : ";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel3.SetColumnSpan(this.tableLayoutPanel4, 3);
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.btnCustomer_Extend_Clear, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnCustomer_Extend_1Year, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnCustomer_Extend_6Mon, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnCustomer_Extend_1Mon, 0, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(147, 84);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(574, 28);
            this.tableLayoutPanel4.TabIndex = 24;
            // 
            // btnCustomer_Extend_Clear
            // 
            this.btnCustomer_Extend_Clear.BackColor = System.Drawing.Color.Black;
            this.btnCustomer_Extend_Clear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCustomer_Extend_Clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomer_Extend_Clear.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCustomer_Extend_Clear.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomer_Extend_Clear.Location = new System.Drawing.Point(432, 3);
            this.btnCustomer_Extend_Clear.Name = "btnCustomer_Extend_Clear";
            this.btnCustomer_Extend_Clear.Size = new System.Drawing.Size(139, 22);
            this.btnCustomer_Extend_Clear.TabIndex = 3;
            this.btnCustomer_Extend_Clear.TabStop = false;
            this.btnCustomer_Extend_Clear.Text = "초기화";
            this.btnCustomer_Extend_Clear.UseVisualStyleBackColor = false;
            // 
            // btnCustomer_Extend_1Year
            // 
            this.btnCustomer_Extend_1Year.BackColor = System.Drawing.Color.Black;
            this.btnCustomer_Extend_1Year.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCustomer_Extend_1Year.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomer_Extend_1Year.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCustomer_Extend_1Year.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomer_Extend_1Year.Location = new System.Drawing.Point(289, 3);
            this.btnCustomer_Extend_1Year.Name = "btnCustomer_Extend_1Year";
            this.btnCustomer_Extend_1Year.Size = new System.Drawing.Size(137, 22);
            this.btnCustomer_Extend_1Year.TabIndex = 2;
            this.btnCustomer_Extend_1Year.TabStop = false;
            this.btnCustomer_Extend_1Year.Text = "1 년 연장";
            this.btnCustomer_Extend_1Year.UseVisualStyleBackColor = false;
            // 
            // btnCustomer_Extend_6Mon
            // 
            this.btnCustomer_Extend_6Mon.BackColor = System.Drawing.Color.Black;
            this.btnCustomer_Extend_6Mon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCustomer_Extend_6Mon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomer_Extend_6Mon.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCustomer_Extend_6Mon.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomer_Extend_6Mon.Location = new System.Drawing.Point(146, 3);
            this.btnCustomer_Extend_6Mon.Name = "btnCustomer_Extend_6Mon";
            this.btnCustomer_Extend_6Mon.Size = new System.Drawing.Size(137, 22);
            this.btnCustomer_Extend_6Mon.TabIndex = 1;
            this.btnCustomer_Extend_6Mon.TabStop = false;
            this.btnCustomer_Extend_6Mon.Text = "6 개월 연장";
            this.btnCustomer_Extend_6Mon.UseVisualStyleBackColor = false;
            // 
            // btnCustomer_Extend_1Mon
            // 
            this.btnCustomer_Extend_1Mon.BackColor = System.Drawing.Color.Black;
            this.btnCustomer_Extend_1Mon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCustomer_Extend_1Mon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomer_Extend_1Mon.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCustomer_Extend_1Mon.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomer_Extend_1Mon.Location = new System.Drawing.Point(3, 3);
            this.btnCustomer_Extend_1Mon.Name = "btnCustomer_Extend_1Mon";
            this.btnCustomer_Extend_1Mon.Size = new System.Drawing.Size(137, 22);
            this.btnCustomer_Extend_1Mon.TabIndex = 0;
            this.btnCustomer_Extend_1Mon.TabStop = false;
            this.btnCustomer_Extend_1Mon.Text = "1 개월 연장";
            this.btnCustomer_Extend_1Mon.UseVisualStyleBackColor = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label4.Location = new System.Drawing.Point(364, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(115, 21);
            this.label4.TabIndex = 35;
            this.label4.Text = "감면 사항(*) : ";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboxCustomer_DCName
            // 
            this.cboxCustomer_DCName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxCustomer_DCName.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cboxCustomer_DCName.FormattingEnabled = true;
            this.cboxCustomer_DCName.Location = new System.Drawing.Point(508, 124);
            this.cboxCustomer_DCName.Name = "cboxCustomer_DCName";
            this.cboxCustomer_DCName.Size = new System.Drawing.Size(211, 29);
            this.cboxCustomer_DCName.TabIndex = 36;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel3.SetColumnSpan(this.tableLayoutPanel1, 3);
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.tableLayoutPanel1.Controls.Add(this.label25, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.tboxCustomer_Extend_Rates, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label24, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.tboxCustomer_Extend_Monthes, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.chboxCustomer_Extend_Log, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(144, 160);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(580, 38);
            this.tableLayoutPanel1.TabIndex = 31;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label25.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label25.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label25.Location = new System.Drawing.Point(253, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(94, 38);
            this.label25.TabIndex = 8;
            this.label25.Text = " 개월";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tboxCustomer_Extend_Rates
            // 
            this.tboxCustomer_Extend_Rates.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tboxCustomer_Extend_Rates.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxCustomer_Extend_Rates.ForeColor = System.Drawing.Color.Black;
            this.tboxCustomer_Extend_Rates.Location = new System.Drawing.Point(353, 6);
            this.tboxCustomer_Extend_Rates.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tboxCustomer_Extend_Rates.Name = "tboxCustomer_Extend_Rates";
            this.tboxCustomer_Extend_Rates.Size = new System.Drawing.Size(119, 29);
            this.tboxCustomer_Extend_Rates.TabIndex = 21;
            this.tboxCustomer_Extend_Rates.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label24.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label24.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label24.Location = new System.Drawing.Point(478, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(99, 38);
            this.label24.TabIndex = 6;
            this.label24.Text = " 원 납부";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tboxCustomer_Extend_Monthes
            // 
            this.tboxCustomer_Extend_Monthes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tboxCustomer_Extend_Monthes.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxCustomer_Extend_Monthes.ForeColor = System.Drawing.Color.Black;
            this.tboxCustomer_Extend_Monthes.Location = new System.Drawing.Point(128, 6);
            this.tboxCustomer_Extend_Monthes.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tboxCustomer_Extend_Monthes.Name = "tboxCustomer_Extend_Monthes";
            this.tboxCustomer_Extend_Monthes.Size = new System.Drawing.Size(119, 29);
            this.tboxCustomer_Extend_Monthes.TabIndex = 20;
            this.tboxCustomer_Extend_Monthes.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // chboxCustomer_Extend_Log
            // 
            this.chboxCustomer_Extend_Log.AutoSize = true;
            this.chboxCustomer_Extend_Log.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chboxCustomer_Extend_Log.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.chboxCustomer_Extend_Log.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.chboxCustomer_Extend_Log.Location = new System.Drawing.Point(3, 3);
            this.chboxCustomer_Extend_Log.Name = "chboxCustomer_Extend_Log";
            this.chboxCustomer_Extend_Log.Size = new System.Drawing.Size(119, 32);
            this.chboxCustomer_Extend_Log.TabIndex = 22;
            this.chboxCustomer_Extend_Log.Text = "납부내역 기록";
            this.chboxCustomer_Extend_Log.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label5.Location = new System.Drawing.Point(3, 206);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(115, 21);
            this.label5.TabIndex = 40;
            this.label5.Text = "결제 형식(*) : ";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboxCustomer_PayType
            // 
            this.cboxCustomer_PayType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxCustomer_PayType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboxCustomer_PayType.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cboxCustomer_PayType.FormattingEnabled = true;
            this.cboxCustomer_PayType.Location = new System.Drawing.Point(147, 209);
            this.cboxCustomer_PayType.Name = "cboxCustomer_PayType";
            this.cboxCustomer_PayType.Size = new System.Drawing.Size(211, 29);
            this.cboxCustomer_PayType.TabIndex = 41;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label6.Location = new System.Drawing.Point(364, 206);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(115, 21);
            this.label6.TabIndex = 42;
            this.label6.Text = "결제 일시(*) : ";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpCustomer_PayedDate
            // 
            this.dtpCustomer_PayedDate.CustomFormat = "yyyy-MM-dd (dddd)";
            this.dtpCustomer_PayedDate.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dtpCustomer_PayedDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCustomer_PayedDate.Location = new System.Drawing.Point(508, 209);
            this.dtpCustomer_PayedDate.MaxDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dtpCustomer_PayedDate.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.dtpCustomer_PayedDate.Name = "dtpCustomer_PayedDate";
            this.dtpCustomer_PayedDate.Size = new System.Drawing.Size(211, 29);
            this.dtpCustomer_PayedDate.TabIndex = 43;
            this.dtpCustomer_PayedDate.TabStop = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label16.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label16.Location = new System.Drawing.Point(3, 324);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(70, 21);
            this.label16.TabIndex = 48;
            this.label16.Text = "주  소 : ";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label12.Location = new System.Drawing.Point(3, 245);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(70, 21);
            this.label12.TabIndex = 7;
            this.label12.Text = "성  명 : ";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tboxCustomer_Name
            // 
            this.tboxCustomer_Name.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxCustomer_Name.Location = new System.Drawing.Point(147, 251);
            this.tboxCustomer_Name.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tboxCustomer_Name.Name = "tboxCustomer_Name";
            this.tboxCustomer_Name.Size = new System.Drawing.Size(211, 29);
            this.tboxCustomer_Name.TabIndex = 13;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label13.Location = new System.Drawing.Point(364, 245);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(90, 21);
            this.label13.TabIndex = 9;
            this.label13.Text = "전화번호 : ";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tboxCustomer_PhoneNum
            // 
            this.tboxCustomer_PhoneNum.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxCustomer_PhoneNum.Location = new System.Drawing.Point(508, 251);
            this.tboxCustomer_PhoneNum.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tboxCustomer_PhoneNum.Name = "tboxCustomer_PhoneNum";
            this.tboxCustomer_PhoneNum.Size = new System.Drawing.Size(213, 29);
            this.tboxCustomer_PhoneNum.TabIndex = 14;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label14.Location = new System.Drawing.Point(3, 286);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 21);
            this.label14.TabIndex = 44;
            this.label14.Text = "회 사 명 : ";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tboxCustomer_Comp
            // 
            this.tboxCustomer_Comp.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxCustomer_Comp.Location = new System.Drawing.Point(147, 292);
            this.tboxCustomer_Comp.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tboxCustomer_Comp.Name = "tboxCustomer_Comp";
            this.tboxCustomer_Comp.Size = new System.Drawing.Size(211, 29);
            this.tboxCustomer_Comp.TabIndex = 45;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label15.Location = new System.Drawing.Point(364, 286);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(86, 21);
            this.label15.TabIndex = 46;
            this.label15.Text = "부 서 명 : ";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tboxCustomer_Dept
            // 
            this.tboxCustomer_Dept.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxCustomer_Dept.Location = new System.Drawing.Point(508, 292);
            this.tboxCustomer_Dept.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tboxCustomer_Dept.Name = "tboxCustomer_Dept";
            this.tboxCustomer_Dept.Size = new System.Drawing.Size(213, 29);
            this.tboxCustomer_Dept.TabIndex = 47;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label7.Location = new System.Drawing.Point(3, 121);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(115, 21);
            this.label7.TabIndex = 50;
            this.label7.Text = "차량 종류(*) : ";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboxCustomer_CarType
            // 
            this.cboxCustomer_CarType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxCustomer_CarType.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cboxCustomer_CarType.FormattingEnabled = true;
            this.cboxCustomer_CarType.Location = new System.Drawing.Point(147, 124);
            this.cboxCustomer_CarType.Name = "cboxCustomer_CarType";
            this.cboxCustomer_CarType.Size = new System.Drawing.Size(211, 29);
            this.cboxCustomer_CarType.TabIndex = 51;
            // 
            // tboxCustomer_Addr
            // 
            this.tableLayoutPanel3.SetColumnSpan(this.tboxCustomer_Addr, 3);
            this.tboxCustomer_Addr.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxCustomer_Addr.Location = new System.Drawing.Point(147, 330);
            this.tboxCustomer_Addr.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tboxCustomer_Addr.Name = "tboxCustomer_Addr";
            this.tboxCustomer_Addr.Size = new System.Drawing.Size(574, 29);
            this.tboxCustomer_Addr.TabIndex = 49;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 7;
            this.tableLayoutPanel3.SetColumnSpan(this.tableLayoutPanel6, 4);
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel6.Controls.Add(this.btnTotalPop, 6, 0);
            this.tableLayoutPanel6.Controls.Add(this.btnCustomer_Clear, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.btnCustomer_Update, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.btnCustomer_Create, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.btnCustomer_Delete, 2, 1);
            this.tableLayoutPanel6.Controls.Add(this.btnCustomer_Close, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.btnCustomer_Import, 5, 1);
            this.tableLayoutPanel6.Controls.Add(this.btnCustomer_Export, 5, 0);
            this.tableLayoutPanel6.Controls.Add(this.tboxCustomer_ReturnRates, 3, 0);
            this.tableLayoutPanel6.Controls.Add(this.btnCustomer_Return, 4, 0);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(6, 537);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(6);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 2;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(712, 74);
            this.tableLayoutPanel6.TabIndex = 52;
            // 
            // btnTotalPop
            // 
            this.btnTotalPop.BackColor = System.Drawing.Color.SlateGray;
            this.btnTotalPop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTotalPop.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnTotalPop.Location = new System.Drawing.Point(612, 3);
            this.btnTotalPop.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnTotalPop.Name = "btnTotalPop";
            this.btnTotalPop.Size = new System.Drawing.Size(94, 32);
            this.btnTotalPop.TabIndex = 8;
            this.btnTotalPop.TabStop = false;
            this.btnTotalPop.Text = "전체조회";
            this.btnTotalPop.UseVisualStyleBackColor = false;
            // 
            // btnCustomer_Clear
            // 
            this.btnCustomer_Clear.BackColor = System.Drawing.Color.SlateGray;
            this.btnCustomer_Clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomer_Clear.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCustomer_Clear.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomer_Clear.Location = new System.Drawing.Point(6, 3);
            this.btnCustomer_Clear.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnCustomer_Clear.Name = "btnCustomer_Clear";
            this.btnCustomer_Clear.Size = new System.Drawing.Size(89, 32);
            this.btnCustomer_Clear.TabIndex = 6;
            this.btnCustomer_Clear.TabStop = false;
            this.btnCustomer_Clear.Text = "입력 초기화";
            this.btnCustomer_Clear.UseVisualStyleBackColor = false;
            // 
            // btnCustomer_Update
            // 
            this.btnCustomer_Update.BackColor = System.Drawing.Color.SlateGray;
            this.btnCustomer_Update.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomer_Update.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomer_Update.Location = new System.Drawing.Point(208, 3);
            this.btnCustomer_Update.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnCustomer_Update.Name = "btnCustomer_Update";
            this.btnCustomer_Update.Size = new System.Drawing.Size(89, 32);
            this.btnCustomer_Update.TabIndex = 2;
            this.btnCustomer_Update.TabStop = false;
            this.btnCustomer_Update.Text = "수정/연장";
            this.btnCustomer_Update.UseVisualStyleBackColor = false;
            // 
            // btnCustomer_Create
            // 
            this.btnCustomer_Create.BackColor = System.Drawing.Color.SlateGray;
            this.btnCustomer_Create.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomer_Create.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomer_Create.Location = new System.Drawing.Point(107, 3);
            this.btnCustomer_Create.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnCustomer_Create.Name = "btnCustomer_Create";
            this.btnCustomer_Create.Size = new System.Drawing.Size(89, 32);
            this.btnCustomer_Create.TabIndex = 1;
            this.btnCustomer_Create.TabStop = false;
            this.btnCustomer_Create.Text = "신규";
            this.btnCustomer_Create.UseVisualStyleBackColor = false;
            // 
            // btnCustomer_Delete
            // 
            this.btnCustomer_Delete.BackColor = System.Drawing.Color.SlateGray;
            this.btnCustomer_Delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomer_Delete.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomer_Delete.Location = new System.Drawing.Point(208, 41);
            this.btnCustomer_Delete.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnCustomer_Delete.Name = "btnCustomer_Delete";
            this.btnCustomer_Delete.Size = new System.Drawing.Size(89, 30);
            this.btnCustomer_Delete.TabIndex = 3;
            this.btnCustomer_Delete.TabStop = false;
            this.btnCustomer_Delete.Tag = "4";
            this.btnCustomer_Delete.Text = "삭제";
            this.btnCustomer_Delete.UseVisualStyleBackColor = false;
            // 
            // btnCustomer_Close
            // 
            this.btnCustomer_Close.BackColor = System.Drawing.Color.SlateGray;
            this.btnCustomer_Close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomer_Close.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomer_Close.Location = new System.Drawing.Point(107, 41);
            this.btnCustomer_Close.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnCustomer_Close.Name = "btnCustomer_Close";
            this.btnCustomer_Close.Size = new System.Drawing.Size(89, 30);
            this.btnCustomer_Close.TabIndex = 9;
            this.btnCustomer_Close.TabStop = false;
            this.btnCustomer_Close.Tag = "2";
            this.btnCustomer_Close.Text = "해지";
            this.btnCustomer_Close.UseVisualStyleBackColor = false;
            // 
            // btnCustomer_Import
            // 
            this.btnCustomer_Import.BackColor = System.Drawing.Color.SlateGray;
            this.btnCustomer_Import.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomer_Import.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomer_Import.Location = new System.Drawing.Point(511, 41);
            this.btnCustomer_Import.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnCustomer_Import.Name = "btnCustomer_Import";
            this.btnCustomer_Import.Size = new System.Drawing.Size(89, 30);
            this.btnCustomer_Import.TabIndex = 7;
            this.btnCustomer_Import.TabStop = false;
            this.btnCustomer_Import.Text = "가져오기";
            this.btnCustomer_Import.UseVisualStyleBackColor = false;
            this.btnCustomer_Import.Visible = false;
            // 
            // btnCustomer_Export
            // 
            this.btnCustomer_Export.BackColor = System.Drawing.Color.SlateGray;
            this.btnCustomer_Export.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomer_Export.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomer_Export.Location = new System.Drawing.Point(511, 3);
            this.btnCustomer_Export.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnCustomer_Export.Name = "btnCustomer_Export";
            this.btnCustomer_Export.Size = new System.Drawing.Size(89, 32);
            this.btnCustomer_Export.TabIndex = 5;
            this.btnCustomer_Export.TabStop = false;
            this.btnCustomer_Export.Text = "내보내기";
            this.btnCustomer_Export.UseVisualStyleBackColor = false;
            this.btnCustomer_Export.Visible = false;
            // 
            // tboxCustomer_ReturnRates
            // 
            this.tboxCustomer_ReturnRates.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxCustomer_ReturnRates.Location = new System.Drawing.Point(306, 6);
            this.tboxCustomer_ReturnRates.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tboxCustomer_ReturnRates.Multiline = true;
            this.tboxCustomer_ReturnRates.Name = "tboxCustomer_ReturnRates";
            this.tboxCustomer_ReturnRates.Size = new System.Drawing.Size(95, 23);
            this.tboxCustomer_ReturnRates.TabIndex = 53;
            // 
            // btnCustomer_Return
            // 
            this.btnCustomer_Return.BackColor = System.Drawing.Color.SlateGray;
            this.btnCustomer_Return.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomer_Return.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomer_Return.Location = new System.Drawing.Point(410, 3);
            this.btnCustomer_Return.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnCustomer_Return.Name = "btnCustomer_Return";
            this.btnCustomer_Return.Size = new System.Drawing.Size(89, 32);
            this.btnCustomer_Return.TabIndex = 10;
            this.btnCustomer_Return.TabStop = false;
            this.btnCustomer_Return.Tag = "3";
            this.btnCustomer_Return.Text = "환급";
            this.btnCustomer_Return.UseVisualStyleBackColor = false;
            // 
            // gboxGroupInfo
            // 
            this.gboxGroupInfo.BackColor = System.Drawing.Color.Transparent;
            this.gboxGroupInfo.Controls.Add(this.layoutGroupInfo);
            this.gboxGroupInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gboxGroupInfo.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gboxGroupInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(133)))), ((int)(((byte)(0)))));
            this.gboxGroupInfo.Location = new System.Drawing.Point(597, 56);
            this.gboxGroupInfo.Margin = new System.Windows.Forms.Padding(15);
            this.gboxGroupInfo.Name = "gboxGroupInfo";
            this.gboxGroupInfo.Padding = new System.Windows.Forms.Padding(10);
            this.gboxGroupInfo.Size = new System.Drawing.Size(744, 277);
            this.gboxGroupInfo.TabIndex = 1;
            this.gboxGroupInfo.TabStop = false;
            this.gboxGroupInfo.Text = "2. 장기주차 이력";
            // 
            // layoutGroupInfo
            // 
            this.layoutGroupInfo.ColumnCount = 1;
            this.layoutGroupInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.layoutGroupInfo.Controls.Add(this.lviewHistoryList, 0, 0);
            this.layoutGroupInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutGroupInfo.Location = new System.Drawing.Point(10, 36);
            this.layoutGroupInfo.Name = "layoutGroupInfo";
            this.layoutGroupInfo.RowCount = 1;
            this.layoutGroupInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layoutGroupInfo.Size = new System.Drawing.Size(724, 231);
            this.layoutGroupInfo.TabIndex = 0;
            // 
            // lviewHistoryList
            // 
            this.lviewHistoryList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lviewHistoryList.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lviewHistoryList.FullRowSelect = true;
            this.lviewHistoryList.GridLines = true;
            this.lviewHistoryList.HideSelection = false;
            this.lviewHistoryList.Location = new System.Drawing.Point(6, 6);
            this.lviewHistoryList.Margin = new System.Windows.Forms.Padding(6);
            this.lviewHistoryList.MultiSelect = false;
            this.lviewHistoryList.Name = "lviewHistoryList";
            this.lviewHistoryList.Size = new System.Drawing.Size(712, 219);
            this.lviewHistoryList.TabIndex = 2;
            this.lviewHistoryList.TabStop = false;
            this.lviewHistoryList.UseCompatibleStateImageBehavior = false;
            this.lviewHistoryList.View = System.Windows.Forms.View.Details;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.layoutMainLeft);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(236)))), ((int)(((byte)(219)))));
            this.groupBox1.Location = new System.Drawing.Point(20, 56);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(10);
            this.layoutMain.SetRowSpan(this.groupBox1, 2);
            this.groupBox1.Size = new System.Drawing.Size(547, 985);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "1. 장기주차 차량 조회";
            // 
            // layoutMainLeft
            // 
            this.layoutMainLeft.ColumnCount = 1;
            this.layoutMainLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layoutMainLeft.Controls.Add(this.layoutCustSearch, 0, 0);
            this.layoutMainLeft.Controls.Add(this.lviewCustList, 0, 1);
            this.layoutMainLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutMainLeft.Location = new System.Drawing.Point(10, 36);
            this.layoutMainLeft.Name = "layoutMainLeft";
            this.layoutMainLeft.RowCount = 2;
            this.layoutMainLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 165F));
            this.layoutMainLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layoutMainLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.layoutMainLeft.Size = new System.Drawing.Size(527, 939);
            this.layoutMainLeft.TabIndex = 0;
            // 
            // layoutCustSearch
            // 
            this.layoutCustSearch.ColumnCount = 5;
            this.layoutCustSearch.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.layoutCustSearch.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 223F));
            this.layoutCustSearch.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.layoutCustSearch.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.layoutCustSearch.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.layoutCustSearch.Controls.Add(this.label1, 0, 1);
            this.layoutCustSearch.Controls.Add(this.tboxSearch_CarNum, 1, 1);
            this.layoutCustSearch.Controls.Add(this.label26, 0, 0);
            this.layoutCustSearch.Controls.Add(this.cboxSearch_ParkName, 1, 0);
            this.layoutCustSearch.Controls.Add(this.layoutSearchButtons, 3, 2);
            this.layoutCustSearch.Controls.Add(this.label2, 3, 1);
            this.layoutCustSearch.Controls.Add(this.tboxSearch_Name, 4, 1);
            this.layoutCustSearch.Controls.Add(this.label8, 3, 0);
            this.layoutCustSearch.Controls.Add(this.cboxSearch_State, 4, 0);
            this.layoutCustSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutCustSearch.Location = new System.Drawing.Point(3, 3);
            this.layoutCustSearch.Name = "layoutCustSearch";
            this.layoutCustSearch.RowCount = 3;
            this.layoutCustSearch.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.layoutCustSearch.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.layoutCustSearch.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 59F));
            this.layoutCustSearch.Size = new System.Drawing.Size(521, 159);
            this.layoutCustSearch.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label1.Location = new System.Drawing.Point(3, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 50);
            this.label1.TabIndex = 0;
            this.label1.Text = "차량번호 : ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tboxSearch_CarNum
            // 
            this.tboxSearch_CarNum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tboxSearch_CarNum.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxSearch_CarNum.Location = new System.Drawing.Point(87, 56);
            this.tboxSearch_CarNum.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tboxSearch_CarNum.Name = "tboxSearch_CarNum";
            this.tboxSearch_CarNum.Size = new System.Drawing.Size(217, 33);
            this.tboxSearch_CarNum.TabIndex = 1;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label26.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label26.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label26.Location = new System.Drawing.Point(3, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(78, 50);
            this.label26.TabIndex = 7;
            this.label26.Text = "주차장명 :";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboxSearch_ParkName
            // 
            this.cboxSearch_ParkName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboxSearch_ParkName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxSearch_ParkName.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cboxSearch_ParkName.FormattingEnabled = true;
            this.cboxSearch_ParkName.Location = new System.Drawing.Point(87, 6);
            this.cboxSearch_ParkName.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.cboxSearch_ParkName.Name = "cboxSearch_ParkName";
            this.cboxSearch_ParkName.Size = new System.Drawing.Size(217, 33);
            this.cboxSearch_ParkName.TabIndex = 8;
            // 
            // layoutSearchButtons
            // 
            this.layoutSearchButtons.ColumnCount = 2;
            this.layoutCustSearch.SetColumnSpan(this.layoutSearchButtons, 2);
            this.layoutSearchButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layoutSearchButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layoutSearchButtons.Controls.Add(this.btnSearch_Clear, 1, 0);
            this.layoutSearchButtons.Controls.Add(this.btnSearch_Run, 0, 0);
            this.layoutSearchButtons.Location = new System.Drawing.Point(318, 103);
            this.layoutSearchButtons.Name = "layoutSearchButtons";
            this.layoutSearchButtons.RowCount = 1;
            this.layoutSearchButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layoutSearchButtons.Size = new System.Drawing.Size(200, 44);
            this.layoutSearchButtons.TabIndex = 6;
            // 
            // btnSearch_Clear
            // 
            this.btnSearch_Clear.BackColor = System.Drawing.Color.SlateGray;
            this.btnSearch_Clear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSearch_Clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch_Clear.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnSearch_Clear.Location = new System.Drawing.Point(106, 3);
            this.btnSearch_Clear.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnSearch_Clear.Name = "btnSearch_Clear";
            this.btnSearch_Clear.Size = new System.Drawing.Size(88, 38);
            this.btnSearch_Clear.TabIndex = 1;
            this.btnSearch_Clear.TabStop = false;
            this.btnSearch_Clear.Text = "초기화";
            this.btnSearch_Clear.UseVisualStyleBackColor = false;
            // 
            // btnSearch_Run
            // 
            this.btnSearch_Run.BackColor = System.Drawing.Color.SlateGray;
            this.btnSearch_Run.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSearch_Run.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch_Run.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnSearch_Run.Location = new System.Drawing.Point(6, 3);
            this.btnSearch_Run.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnSearch_Run.Name = "btnSearch_Run";
            this.btnSearch_Run.Size = new System.Drawing.Size(88, 38);
            this.btnSearch_Run.TabIndex = 0;
            this.btnSearch_Run.TabStop = false;
            this.btnSearch_Run.Text = "검 색";
            this.btnSearch_Run.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label2.Location = new System.Drawing.Point(318, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "성  명 : ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tboxSearch_Name
            // 
            this.tboxSearch_Name.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxSearch_Name.Location = new System.Drawing.Point(396, 56);
            this.tboxSearch_Name.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tboxSearch_Name.Name = "tboxSearch_Name";
            this.tboxSearch_Name.Size = new System.Drawing.Size(119, 33);
            this.tboxSearch_Name.TabIndex = 2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label8.Location = new System.Drawing.Point(318, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 23);
            this.label8.TabIndex = 9;
            this.label8.Text = "상태 : ";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboxSearch_State
            // 
            this.cboxSearch_State.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxSearch_State.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cboxSearch_State.FormattingEnabled = true;
            this.cboxSearch_State.Location = new System.Drawing.Point(396, 6);
            this.cboxSearch_State.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.cboxSearch_State.Name = "cboxSearch_State";
            this.cboxSearch_State.Size = new System.Drawing.Size(122, 33);
            this.cboxSearch_State.TabIndex = 10;
            // 
            // lviewCustList
            // 
            this.lviewCustList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lviewCustList.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lviewCustList.FullRowSelect = true;
            this.lviewCustList.GridLines = true;
            this.lviewCustList.HideSelection = false;
            this.lviewCustList.Location = new System.Drawing.Point(6, 171);
            this.lviewCustList.Margin = new System.Windows.Forms.Padding(6);
            this.lviewCustList.MultiSelect = false;
            this.lviewCustList.Name = "lviewCustList";
            this.lviewCustList.Size = new System.Drawing.Size(515, 762);
            this.lviewCustList.TabIndex = 0;
            this.lviewCustList.TabStop = false;
            this.lviewCustList.UseCompatibleStateImageBehavior = false;
            this.lviewCustList.View = System.Windows.Forms.View.Details;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.btnOneStopView, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnCustomerView, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(8, 8);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(571, 30);
            this.tableLayoutPanel2.TabIndex = 10;
            // 
            // btnOneStopView
            // 
            this.btnOneStopView.BackColor = System.Drawing.Color.SlateGray;
            this.btnOneStopView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnOneStopView.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOneStopView.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnOneStopView.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnOneStopView.Location = new System.Drawing.Point(148, 3);
            this.btnOneStopView.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnOneStopView.Name = "btnOneStopView";
            this.btnOneStopView.Size = new System.Drawing.Size(130, 24);
            this.btnOneStopView.TabIndex = 10;
            this.btnOneStopView.TabStop = false;
            this.btnOneStopView.Text = "사전등록";
            this.btnOneStopView.UseVisualStyleBackColor = false;
            // 
            // btnCustomerView
            // 
            this.btnCustomerView.BackColor = System.Drawing.Color.OrangeRed;
            this.btnCustomerView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCustomerView.Enabled = false;
            this.btnCustomerView.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomerView.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCustomerView.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomerView.Location = new System.Drawing.Point(6, 3);
            this.btnCustomerView.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnCustomerView.Name = "btnCustomerView";
            this.btnCustomerView.Size = new System.Drawing.Size(130, 24);
            this.btnCustomerView.TabIndex = 9;
            this.btnCustomerView.TabStop = false;
            this.btnCustomerView.Text = "정기권";
            this.btnCustomerView.UseVisualStyleBackColor = false;
            // 
            // SognoLib_LongTimeCustomerViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1361, 1061);
            this.Controls.Add(this.layoutMain);
            this.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "SognoLib_LongTimeCustomerViewer";
            this.Text = "장기주차 관리";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CustomerViewer_FormClosing);
            this.layoutMain.ResumeLayout(false);
            this.gbox_CustomerInfo.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.gboxGroupInfo.ResumeLayout(false);
            this.layoutGroupInfo.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.layoutMainLeft.ResumeLayout(false);
            this.layoutCustSearch.ResumeLayout(false);
            this.layoutCustSearch.PerformLayout();
            this.layoutSearchButtons.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel layoutMain;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel layoutMainLeft;
        public System.Windows.Forms.ListView lviewCustList;
        public System.Windows.Forms.GroupBox gboxGroupInfo;
        private System.Windows.Forms.TableLayoutPanel layoutGroupInfo;
        public System.Windows.Forms.GroupBox gbox_CustomerInfo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.TextBox tboxCustomer_PhoneNum;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.TextBox tboxCustomer_Name;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label21;
        public System.Windows.Forms.TextBox tboxCustomer_Memo;
        public System.Windows.Forms.Button btnCustomer_Extend_1Mon;
        public System.Windows.Forms.Button btnCustomer_Extend_Clear;
        public System.Windows.Forms.Button btnCustomer_Extend_1Year;
        public System.Windows.Forms.Button btnCustomer_Extend_6Mon;
        private System.Windows.Forms.TableLayoutPanel layoutCustSearch;
        public System.Windows.Forms.TextBox tboxSearch_Name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox tboxSearch_CarNum;
        private System.Windows.Forms.TableLayoutPanel layoutSearchButtons;
        public System.Windows.Forms.Button btnSearch_Clear;
        public System.Windows.Forms.Button btnSearch_Run;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.TextBox tboxCustomer_Extend_Monthes;
        private System.Windows.Forms.Label label25;
        public System.Windows.Forms.TextBox tboxCustomer_Extend_Rates;
        private System.Windows.Forms.Label label24;
        public System.Windows.Forms.DateTimePicker dtpCustomer_StartDate;
        public System.Windows.Forms.DateTimePicker dtpCustomer_EndDate;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        public System.Windows.Forms.TextBox tboxCustomer_CarNum;
        public System.Windows.Forms.Button btnCustomer_CheckCarNum;
        private System.Windows.Forms.Label label26;
        public System.Windows.Forms.ComboBox cboxSearch_ParkName;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.ComboBox cboxCustomer_DCName;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public System.Windows.Forms.Button btnCustomerView;
        public System.Windows.Forms.Button btnOneStopView;
        public System.Windows.Forms.CheckBox chboxCustomer_Extend_Log;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        public System.Windows.Forms.CheckBox chboxCustomer_Limit_Sunday;
        public System.Windows.Forms.CheckBox chboxCustomer_Limit_Saturday;
        public System.Windows.Forms.CheckBox chboxCustomer_Limit_Friday;
        public System.Windows.Forms.CheckBox chboxCustomer_Limit_Thursday;
        public System.Windows.Forms.CheckBox chboxCustomer_Limit_Wednesday;
        public System.Windows.Forms.CheckBox chboxCustomer_Limit_Tuesday;
        public System.Windows.Forms.CheckBox chboxCustomer_Limit_Monday;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        public System.Windows.Forms.CheckBox chboxCustomer_Limit_Every7;
        public System.Windows.Forms.CheckBox chboxCustomer_Limit_Every6;
        public System.Windows.Forms.CheckBox chboxCustomer_Limit_Every8;
        public System.Windows.Forms.CheckBox chboxCustomer_Limit_Every0;
        public System.Windows.Forms.CheckBox chboxCustomer_Limit_Every9;
        public System.Windows.Forms.CheckBox chboxCustomer_Limit_Every2;
        public System.Windows.Forms.CheckBox chboxCustomer_Limit_Every1;
        public System.Windows.Forms.CheckBox chboxCustomer_Limit_Every3;
        public System.Windows.Forms.CheckBox chboxCustomer_Limit_Every5;
        public System.Windows.Forms.CheckBox chboxCustomer_Limit_Every4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.ComboBox cboxCustomer_PayType;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.DateTimePicker dtpCustomer_PayedDate;
        private System.Windows.Forms.Label label14;
        public System.Windows.Forms.TextBox tboxCustomer_Comp;
        private System.Windows.Forms.Label label15;
        public System.Windows.Forms.TextBox tboxCustomer_Dept;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.TextBox tboxCustomer_Addr;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.ComboBox cboxCustomer_CarType;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.ComboBox cboxSearch_State;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        public System.Windows.Forms.Button btnTotalPop;
        public System.Windows.Forms.Button btnCustomer_Import;
        public System.Windows.Forms.Button btnCustomer_Clear;
        public System.Windows.Forms.Button btnCustomer_Export;
        public System.Windows.Forms.Button btnCustomer_Update;
        public System.Windows.Forms.Button btnCustomer_Create;
        public System.Windows.Forms.Button btnCustomer_Delete;
        public System.Windows.Forms.Button btnCustomer_Close;
        public System.Windows.Forms.ListView lviewHistoryList;
        public System.Windows.Forms.TextBox tboxCustomer_ReturnRates;
        public System.Windows.Forms.Button btnCustomer_Return;
    }
}