﻿namespace SognoLib.Customer.GUI
{
    partial class SognoLib_CustomerViewer_Neo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.layoutMain = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.layoutMainLeft = new System.Windows.Forms.TableLayoutPanel();
            this.layoutCustSearch = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cboxSearch_Group = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tboxSearch_CarNum = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.cboxSearch_ParkName = new System.Windows.Forms.ComboBox();
            this.layoutSearchButtons = new System.Windows.Forms.TableLayoutPanel();
            this.btnSearch_Clear = new System.Windows.Forms.Button();
            this.btnSearch_Run = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tboxSearch_Name = new System.Windows.Forms.TextBox();
            this.cboxSearch_State = new System.Windows.Forms.ComboBox();
            this.lviewCustList = new System.Windows.Forms.ListView();
            this.gbox_CustomerInfo = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tboxCustomer_CarName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cboxCustomer_CarType = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cboxCustomer_GroupName = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.tboxCustomer_CarNum = new System.Windows.Forms.TextBox();
            this.btnCustomer_CheckCarNum = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.dtpCustomer_StartDate = new System.Windows.Forms.DateTimePicker();
            this.label18 = new System.Windows.Forms.Label();
            this.dtpCustomer_EndDate = new System.Windows.Forms.DateTimePicker();
            this.label19 = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCustomer_Extend_Clear = new System.Windows.Forms.Button();
            this.btnCustomer_Extend_1Year = new System.Windows.Forms.Button();
            this.btnCustomer_Extend_6Mon = new System.Windows.Forms.Button();
            this.btnCustomer_Extend_1Mon = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.tboxCustomer_Memo = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tboxCustomer_Comp = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tboxCustomer_Dept = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tboxCustomer_Name = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tboxCustomer_PhoneNum = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.btnCustomer_Clear = new System.Windows.Forms.Button();
            this.btnCustomer_Update = new System.Windows.Forms.Button();
            this.btnCustomer_Create = new System.Windows.Forms.Button();
            this.btnCustomer_Export = new System.Windows.Forms.Button();
            this.btnCustomer_Import = new System.Windows.Forms.Button();
            this.btnCustomer_Delete = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.dviewPayInfos = new System.Windows.Forms.DataGridView();
            this.gboxGroupInfo = new System.Windows.Forms.GroupBox();
            this.layoutGroupInfo = new System.Windows.Forms.TableLayoutPanel();
            this.lviewHistoryList = new System.Windows.Forms.ListView();
            this.layoutMain.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.layoutMainLeft.SuspendLayout();
            this.layoutCustSearch.SuspendLayout();
            this.layoutSearchButtons.SuspendLayout();
            this.gbox_CustomerInfo.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dviewPayInfos)).BeginInit();
            this.gboxGroupInfo.SuspendLayout();
            this.layoutGroupInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // layoutMain
            // 
            this.layoutMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.layoutMain.ColumnCount = 2;
            this.layoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layoutMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layoutMain.Controls.Add(this.groupBox1, 0, 0);
            this.layoutMain.Controls.Add(this.gbox_CustomerInfo, 1, 0);
            this.layoutMain.Controls.Add(this.groupBox3, 1, 1);
            this.layoutMain.Controls.Add(this.gboxGroupInfo, 1, 2);
            this.layoutMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutMain.Location = new System.Drawing.Point(0, 0);
            this.layoutMain.Name = "layoutMain";
            this.layoutMain.Padding = new System.Windows.Forms.Padding(5);
            this.layoutMain.RowCount = 4;
            this.layoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 367F));
            this.layoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 327F));
            this.layoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 260F));
            this.layoutMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 96F));
            this.layoutMain.Size = new System.Drawing.Size(1584, 1009);
            this.layoutMain.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.layoutMainLeft);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(236)))), ((int)(((byte)(219)))));
            this.groupBox1.Location = new System.Drawing.Point(10, 10);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(10);
            this.layoutMain.SetRowSpan(this.groupBox1, 3);
            this.groupBox1.Size = new System.Drawing.Size(777, 944);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "정기권 차량 조회";
            // 
            // layoutMainLeft
            // 
            this.layoutMainLeft.ColumnCount = 1;
            this.layoutMainLeft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layoutMainLeft.Controls.Add(this.layoutCustSearch, 0, 0);
            this.layoutMainLeft.Controls.Add(this.lviewCustList, 0, 1);
            this.layoutMainLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutMainLeft.Location = new System.Drawing.Point(10, 36);
            this.layoutMainLeft.Name = "layoutMainLeft";
            this.layoutMainLeft.RowCount = 2;
            this.layoutMainLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 131F));
            this.layoutMainLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layoutMainLeft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.layoutMainLeft.Size = new System.Drawing.Size(757, 898);
            this.layoutMainLeft.TabIndex = 0;
            // 
            // layoutCustSearch
            // 
            this.layoutCustSearch.ColumnCount = 5;
            this.layoutCustSearch.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.layoutCustSearch.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.layoutCustSearch.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.layoutCustSearch.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.layoutCustSearch.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.layoutCustSearch.Controls.Add(this.label5, 0, 2);
            this.layoutCustSearch.Controls.Add(this.label3, 3, 0);
            this.layoutCustSearch.Controls.Add(this.cboxSearch_Group, 4, 0);
            this.layoutCustSearch.Controls.Add(this.label1, 0, 1);
            this.layoutCustSearch.Controls.Add(this.tboxSearch_CarNum, 1, 1);
            this.layoutCustSearch.Controls.Add(this.label26, 0, 0);
            this.layoutCustSearch.Controls.Add(this.cboxSearch_ParkName, 1, 0);
            this.layoutCustSearch.Controls.Add(this.layoutSearchButtons, 3, 2);
            this.layoutCustSearch.Controls.Add(this.label2, 3, 1);
            this.layoutCustSearch.Controls.Add(this.tboxSearch_Name, 4, 1);
            this.layoutCustSearch.Controls.Add(this.cboxSearch_State, 1, 2);
            this.layoutCustSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutCustSearch.Location = new System.Drawing.Point(3, 3);
            this.layoutCustSearch.Name = "layoutCustSearch";
            this.layoutCustSearch.RowCount = 3;
            this.layoutCustSearch.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.layoutCustSearch.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.layoutCustSearch.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.layoutCustSearch.Size = new System.Drawing.Size(751, 125);
            this.layoutCustSearch.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label5.Location = new System.Drawing.Point(3, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(138, 43);
            this.label5.TabIndex = 11;
            this.label5.Text = "상태 :";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label3.Location = new System.Drawing.Point(391, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(138, 41);
            this.label3.TabIndex = 2;
            this.label3.Text = "그룹명 : ";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboxSearch_Group
            // 
            this.cboxSearch_Group.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboxSearch_Group.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxSearch_Group.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cboxSearch_Group.FormattingEnabled = true;
            this.cboxSearch_Group.Location = new System.Drawing.Point(535, 6);
            this.cboxSearch_Group.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.cboxSearch_Group.Name = "cboxSearch_Group";
            this.cboxSearch_Group.Size = new System.Drawing.Size(213, 33);
            this.cboxSearch_Group.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label1.Location = new System.Drawing.Point(3, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 41);
            this.label1.TabIndex = 0;
            this.label1.Text = "차량번호 :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tboxSearch_CarNum
            // 
            this.tboxSearch_CarNum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tboxSearch_CarNum.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxSearch_CarNum.Location = new System.Drawing.Point(147, 47);
            this.tboxSearch_CarNum.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tboxSearch_CarNum.Name = "tboxSearch_CarNum";
            this.tboxSearch_CarNum.Size = new System.Drawing.Size(210, 33);
            this.tboxSearch_CarNum.TabIndex = 1;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label26.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label26.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label26.Location = new System.Drawing.Point(3, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(138, 41);
            this.label26.TabIndex = 7;
            this.label26.Text = "주차장명 :";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboxSearch_ParkName
            // 
            this.cboxSearch_ParkName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboxSearch_ParkName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxSearch_ParkName.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cboxSearch_ParkName.FormattingEnabled = true;
            this.cboxSearch_ParkName.Location = new System.Drawing.Point(147, 6);
            this.cboxSearch_ParkName.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.cboxSearch_ParkName.Name = "cboxSearch_ParkName";
            this.cboxSearch_ParkName.Size = new System.Drawing.Size(210, 33);
            this.cboxSearch_ParkName.TabIndex = 8;
            // 
            // layoutSearchButtons
            // 
            this.layoutSearchButtons.ColumnCount = 2;
            this.layoutCustSearch.SetColumnSpan(this.layoutSearchButtons, 2);
            this.layoutSearchButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layoutSearchButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layoutSearchButtons.Controls.Add(this.btnSearch_Clear, 1, 0);
            this.layoutSearchButtons.Controls.Add(this.btnSearch_Run, 0, 0);
            this.layoutSearchButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutSearchButtons.Location = new System.Drawing.Point(391, 85);
            this.layoutSearchButtons.Name = "layoutSearchButtons";
            this.layoutSearchButtons.RowCount = 1;
            this.layoutSearchButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.layoutSearchButtons.Size = new System.Drawing.Size(357, 37);
            this.layoutSearchButtons.TabIndex = 6;
            // 
            // btnSearch_Clear
            // 
            this.btnSearch_Clear.BackColor = System.Drawing.Color.SlateGray;
            this.btnSearch_Clear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSearch_Clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch_Clear.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnSearch_Clear.Location = new System.Drawing.Point(184, 3);
            this.btnSearch_Clear.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnSearch_Clear.Name = "btnSearch_Clear";
            this.btnSearch_Clear.Size = new System.Drawing.Size(167, 31);
            this.btnSearch_Clear.TabIndex = 1;
            this.btnSearch_Clear.TabStop = false;
            this.btnSearch_Clear.Text = "검색초기화";
            this.btnSearch_Clear.UseVisualStyleBackColor = false;
            // 
            // btnSearch_Run
            // 
            this.btnSearch_Run.BackColor = System.Drawing.Color.SlateGray;
            this.btnSearch_Run.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSearch_Run.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch_Run.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnSearch_Run.Location = new System.Drawing.Point(6, 3);
            this.btnSearch_Run.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnSearch_Run.Name = "btnSearch_Run";
            this.btnSearch_Run.Size = new System.Drawing.Size(166, 31);
            this.btnSearch_Run.TabIndex = 0;
            this.btnSearch_Run.TabStop = false;
            this.btnSearch_Run.Text = "검색";
            this.btnSearch_Run.UseVisualStyleBackColor = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label2.Location = new System.Drawing.Point(391, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(138, 41);
            this.label2.TabIndex = 1;
            this.label2.Text = "성  명 : ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tboxSearch_Name
            // 
            this.tboxSearch_Name.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tboxSearch_Name.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxSearch_Name.Location = new System.Drawing.Point(535, 47);
            this.tboxSearch_Name.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tboxSearch_Name.Name = "tboxSearch_Name";
            this.tboxSearch_Name.Size = new System.Drawing.Size(213, 33);
            this.tboxSearch_Name.TabIndex = 2;
            // 
            // cboxSearch_State
            // 
            this.cboxSearch_State.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboxSearch_State.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxSearch_State.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cboxSearch_State.FormattingEnabled = true;
            this.cboxSearch_State.Location = new System.Drawing.Point(147, 88);
            this.cboxSearch_State.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.cboxSearch_State.Name = "cboxSearch_State";
            this.cboxSearch_State.Size = new System.Drawing.Size(210, 33);
            this.cboxSearch_State.TabIndex = 10;
            // 
            // lviewCustList
            // 
            this.lviewCustList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lviewCustList.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lviewCustList.FullRowSelect = true;
            this.lviewCustList.GridLines = true;
            this.lviewCustList.HideSelection = false;
            this.lviewCustList.Location = new System.Drawing.Point(6, 137);
            this.lviewCustList.Margin = new System.Windows.Forms.Padding(6);
            this.lviewCustList.MultiSelect = false;
            this.lviewCustList.Name = "lviewCustList";
            this.lviewCustList.Size = new System.Drawing.Size(745, 755);
            this.lviewCustList.TabIndex = 0;
            this.lviewCustList.TabStop = false;
            this.lviewCustList.UseCompatibleStateImageBehavior = false;
            this.lviewCustList.View = System.Windows.Forms.View.Details;
            // 
            // gbox_CustomerInfo
            // 
            this.gbox_CustomerInfo.Controls.Add(this.tableLayoutPanel3);
            this.gbox_CustomerInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbox_CustomerInfo.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gbox_CustomerInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(236)))), ((int)(((byte)(219)))));
            this.gbox_CustomerInfo.Location = new System.Drawing.Point(797, 10);
            this.gbox_CustomerInfo.Margin = new System.Windows.Forms.Padding(5);
            this.gbox_CustomerInfo.Name = "gbox_CustomerInfo";
            this.gbox_CustomerInfo.Padding = new System.Windows.Forms.Padding(10);
            this.gbox_CustomerInfo.Size = new System.Drawing.Size(777, 357);
            this.gbox_CustomerInfo.TabIndex = 2;
            this.gbox_CustomerInfo.TabStop = false;
            this.gbox_CustomerInfo.Text = "정기권 차량 관리";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.36998F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.64691F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.06454F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.91856F));
            this.tableLayoutPanel3.Controls.Add(this.tboxCustomer_CarName, 3, 3);
            this.tableLayoutPanel3.Controls.Add(this.label4, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.cboxCustomer_CarType, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.label7, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label11, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.cboxCustomer_GroupName, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label10, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel8, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.label17, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.dtpCustomer_StartDate, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label18, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.dtpCustomer_EndDate, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.label19, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.label21, 0, 6);
            this.tableLayoutPanel3.Controls.Add(this.tboxCustomer_Memo, 1, 6);
            this.tableLayoutPanel3.Controls.Add(this.label14, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.tboxCustomer_Comp, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.label15, 2, 5);
            this.tableLayoutPanel3.Controls.Add(this.tboxCustomer_Dept, 3, 5);
            this.tableLayoutPanel3.Controls.Add(this.label12, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.tboxCustomer_Name, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.label13, 2, 4);
            this.tableLayoutPanel3.Controls.Add(this.tboxCustomer_PhoneNum, 3, 4);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel6, 0, 7);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(10, 36);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 8;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 47F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(757, 311);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // tboxCustomer_CarName
            // 
            this.tboxCustomer_CarName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tboxCustomer_CarName.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxCustomer_CarName.Location = new System.Drawing.Point(517, 123);
            this.tboxCustomer_CarName.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tboxCustomer_CarName.Name = "tboxCustomer_CarName";
            this.tboxCustomer_CarName.Size = new System.Drawing.Size(237, 29);
            this.tboxCustomer_CarName.TabIndex = 60;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label4.Location = new System.Drawing.Point(381, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 31);
            this.label4.TabIndex = 59;
            this.label4.Text = "차량명 :";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboxCustomer_CarType
            // 
            this.cboxCustomer_CarType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboxCustomer_CarType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxCustomer_CarType.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cboxCustomer_CarType.FormattingEnabled = true;
            this.cboxCustomer_CarType.Location = new System.Drawing.Point(142, 120);
            this.cboxCustomer_CarType.Name = "cboxCustomer_CarType";
            this.cboxCustomer_CarType.Size = new System.Drawing.Size(233, 29);
            this.cboxCustomer_CarType.TabIndex = 58;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label7.Location = new System.Drawing.Point(3, 117);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(133, 31);
            this.label7.TabIndex = 57;
            this.label7.Text = "차량 종류 :";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label11.Location = new System.Drawing.Point(3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(133, 35);
            this.label11.TabIndex = 5;
            this.label11.Text = "그룹명(*) :";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cboxCustomer_GroupName
            // 
            this.cboxCustomer_GroupName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cboxCustomer_GroupName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxCustomer_GroupName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboxCustomer_GroupName.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cboxCustomer_GroupName.FormattingEnabled = true;
            this.cboxCustomer_GroupName.Location = new System.Drawing.Point(142, 3);
            this.cboxCustomer_GroupName.Name = "cboxCustomer_GroupName";
            this.cboxCustomer_GroupName.Size = new System.Drawing.Size(233, 29);
            this.cboxCustomer_GroupName.TabIndex = 12;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label10.Location = new System.Drawing.Point(381, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(130, 35);
            this.label10.TabIndex = 0;
            this.label10.Text = "차량 번호(*) :";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 88F));
            this.tableLayoutPanel8.Controls.Add(this.tboxCustomer_CarNum, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.btnCustomer_CheckCarNum, 1, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(514, 0);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(243, 35);
            this.tableLayoutPanel8.TabIndex = 34;
            // 
            // tboxCustomer_CarNum
            // 
            this.tboxCustomer_CarNum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tboxCustomer_CarNum.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxCustomer_CarNum.Location = new System.Drawing.Point(3, 6);
            this.tboxCustomer_CarNum.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tboxCustomer_CarNum.Name = "tboxCustomer_CarNum";
            this.tboxCustomer_CarNum.Size = new System.Drawing.Size(149, 29);
            this.tboxCustomer_CarNum.TabIndex = 12;
            // 
            // btnCustomer_CheckCarNum
            // 
            this.btnCustomer_CheckCarNum.BackColor = System.Drawing.Color.SteelBlue;
            this.btnCustomer_CheckCarNum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCustomer_CheckCarNum.FlatAppearance.BorderSize = 0;
            this.btnCustomer_CheckCarNum.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomer_CheckCarNum.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCustomer_CheckCarNum.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomer_CheckCarNum.Location = new System.Drawing.Point(158, 3);
            this.btnCustomer_CheckCarNum.Name = "btnCustomer_CheckCarNum";
            this.btnCustomer_CheckCarNum.Size = new System.Drawing.Size(82, 29);
            this.btnCustomer_CheckCarNum.TabIndex = 13;
            this.btnCustomer_CheckCarNum.Text = "중복확인";
            this.btnCustomer_CheckCarNum.UseVisualStyleBackColor = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label17.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label17.Location = new System.Drawing.Point(3, 35);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(133, 35);
            this.label17.TabIndex = 17;
            this.label17.Text = "시작 일시(*) :";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpCustomer_StartDate
            // 
            this.dtpCustomer_StartDate.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dtpCustomer_StartDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtpCustomer_StartDate.Font = new System.Drawing.Font("맑은 고딕", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dtpCustomer_StartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCustomer_StartDate.Location = new System.Drawing.Point(142, 38);
            this.dtpCustomer_StartDate.MaxDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dtpCustomer_StartDate.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.dtpCustomer_StartDate.Name = "dtpCustomer_StartDate";
            this.dtpCustomer_StartDate.Size = new System.Drawing.Size(233, 26);
            this.dtpCustomer_StartDate.TabIndex = 32;
            this.dtpCustomer_StartDate.TabStop = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label18.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label18.Location = new System.Drawing.Point(381, 35);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(130, 35);
            this.label18.TabIndex = 19;
            this.label18.Text = "종료 일시(*) :";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpCustomer_EndDate
            // 
            this.dtpCustomer_EndDate.CustomFormat = "yyyy-MM-dd HH:mm:ss";
            this.dtpCustomer_EndDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtpCustomer_EndDate.Font = new System.Drawing.Font("맑은 고딕", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dtpCustomer_EndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpCustomer_EndDate.Location = new System.Drawing.Point(517, 38);
            this.dtpCustomer_EndDate.MaxDate = new System.DateTime(2999, 12, 31, 0, 0, 0, 0);
            this.dtpCustomer_EndDate.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.dtpCustomer_EndDate.Name = "dtpCustomer_EndDate";
            this.dtpCustomer_EndDate.Size = new System.Drawing.Size(237, 26);
            this.dtpCustomer_EndDate.TabIndex = 33;
            this.dtpCustomer_EndDate.TabStop = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label19.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label19.Location = new System.Drawing.Point(3, 70);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(133, 47);
            this.label19.TabIndex = 30;
            this.label19.Text = "종료일 연장 :";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel3.SetColumnSpan(this.tableLayoutPanel4, 3);
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel4.Controls.Add(this.btnCustomer_Extend_Clear, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnCustomer_Extend_1Year, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnCustomer_Extend_6Mon, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.btnCustomer_Extend_1Mon, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tableLayoutPanel4.Location = new System.Drawing.Point(142, 73);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(612, 41);
            this.tableLayoutPanel4.TabIndex = 24;
            // 
            // btnCustomer_Extend_Clear
            // 
            this.btnCustomer_Extend_Clear.BackColor = System.Drawing.Color.Black;
            this.btnCustomer_Extend_Clear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCustomer_Extend_Clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomer_Extend_Clear.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCustomer_Extend_Clear.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomer_Extend_Clear.Location = new System.Drawing.Point(462, 3);
            this.btnCustomer_Extend_Clear.Name = "btnCustomer_Extend_Clear";
            this.btnCustomer_Extend_Clear.Size = new System.Drawing.Size(147, 35);
            this.btnCustomer_Extend_Clear.TabIndex = 3;
            this.btnCustomer_Extend_Clear.TabStop = false;
            this.btnCustomer_Extend_Clear.Text = "초기화";
            this.btnCustomer_Extend_Clear.UseVisualStyleBackColor = false;
            // 
            // btnCustomer_Extend_1Year
            // 
            this.btnCustomer_Extend_1Year.BackColor = System.Drawing.Color.Black;
            this.btnCustomer_Extend_1Year.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCustomer_Extend_1Year.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomer_Extend_1Year.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCustomer_Extend_1Year.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomer_Extend_1Year.Location = new System.Drawing.Point(309, 3);
            this.btnCustomer_Extend_1Year.Name = "btnCustomer_Extend_1Year";
            this.btnCustomer_Extend_1Year.Size = new System.Drawing.Size(147, 35);
            this.btnCustomer_Extend_1Year.TabIndex = 2;
            this.btnCustomer_Extend_1Year.TabStop = false;
            this.btnCustomer_Extend_1Year.Text = "1 년 연장";
            this.btnCustomer_Extend_1Year.UseVisualStyleBackColor = false;
            // 
            // btnCustomer_Extend_6Mon
            // 
            this.btnCustomer_Extend_6Mon.BackColor = System.Drawing.Color.Black;
            this.btnCustomer_Extend_6Mon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCustomer_Extend_6Mon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomer_Extend_6Mon.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCustomer_Extend_6Mon.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomer_Extend_6Mon.Location = new System.Drawing.Point(156, 3);
            this.btnCustomer_Extend_6Mon.Name = "btnCustomer_Extend_6Mon";
            this.btnCustomer_Extend_6Mon.Size = new System.Drawing.Size(147, 35);
            this.btnCustomer_Extend_6Mon.TabIndex = 1;
            this.btnCustomer_Extend_6Mon.TabStop = false;
            this.btnCustomer_Extend_6Mon.Text = "6 개월 연장";
            this.btnCustomer_Extend_6Mon.UseVisualStyleBackColor = false;
            // 
            // btnCustomer_Extend_1Mon
            // 
            this.btnCustomer_Extend_1Mon.BackColor = System.Drawing.Color.Black;
            this.btnCustomer_Extend_1Mon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCustomer_Extend_1Mon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomer_Extend_1Mon.Font = new System.Drawing.Font("맑은 고딕", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCustomer_Extend_1Mon.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomer_Extend_1Mon.Location = new System.Drawing.Point(3, 3);
            this.btnCustomer_Extend_1Mon.Name = "btnCustomer_Extend_1Mon";
            this.btnCustomer_Extend_1Mon.Size = new System.Drawing.Size(147, 35);
            this.btnCustomer_Extend_1Mon.TabIndex = 0;
            this.btnCustomer_Extend_1Mon.TabStop = false;
            this.btnCustomer_Extend_1Mon.Text = "1 개월 연장";
            this.btnCustomer_Extend_1Mon.UseVisualStyleBackColor = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label21.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label21.Location = new System.Drawing.Point(3, 215);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(133, 38);
            this.label21.TabIndex = 26;
            this.label21.Text = "메모(비고) :";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tboxCustomer_Memo
            // 
            this.tableLayoutPanel3.SetColumnSpan(this.tboxCustomer_Memo, 3);
            this.tboxCustomer_Memo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tboxCustomer_Memo.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxCustomer_Memo.Location = new System.Drawing.Point(142, 221);
            this.tboxCustomer_Memo.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tboxCustomer_Memo.Multiline = true;
            this.tboxCustomer_Memo.Name = "tboxCustomer_Memo";
            this.tboxCustomer_Memo.Size = new System.Drawing.Size(612, 29);
            this.tboxCustomer_Memo.TabIndex = 22;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label14.Location = new System.Drawing.Point(3, 180);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(133, 35);
            this.label14.TabIndex = 55;
            this.label14.Text = "회사명 :";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tboxCustomer_Comp
            // 
            this.tboxCustomer_Comp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tboxCustomer_Comp.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxCustomer_Comp.Location = new System.Drawing.Point(142, 186);
            this.tboxCustomer_Comp.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tboxCustomer_Comp.Name = "tboxCustomer_Comp";
            this.tboxCustomer_Comp.Size = new System.Drawing.Size(233, 29);
            this.tboxCustomer_Comp.TabIndex = 53;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label15.Location = new System.Drawing.Point(381, 180);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(130, 35);
            this.label15.TabIndex = 56;
            this.label15.Text = "부서명 :";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tboxCustomer_Dept
            // 
            this.tboxCustomer_Dept.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tboxCustomer_Dept.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxCustomer_Dept.Location = new System.Drawing.Point(517, 186);
            this.tboxCustomer_Dept.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tboxCustomer_Dept.Name = "tboxCustomer_Dept";
            this.tboxCustomer_Dept.Size = new System.Drawing.Size(237, 29);
            this.tboxCustomer_Dept.TabIndex = 54;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label12.Location = new System.Drawing.Point(3, 148);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(133, 32);
            this.label12.TabIndex = 7;
            this.label12.Text = "성명 :";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tboxCustomer_Name
            // 
            this.tboxCustomer_Name.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tboxCustomer_Name.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxCustomer_Name.Location = new System.Drawing.Point(142, 154);
            this.tboxCustomer_Name.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tboxCustomer_Name.Name = "tboxCustomer_Name";
            this.tboxCustomer_Name.Size = new System.Drawing.Size(233, 29);
            this.tboxCustomer_Name.TabIndex = 13;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label13.Location = new System.Drawing.Point(381, 148);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(130, 32);
            this.label13.TabIndex = 9;
            this.label13.Text = "전화번호 :";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tboxCustomer_PhoneNum
            // 
            this.tboxCustomer_PhoneNum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tboxCustomer_PhoneNum.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.tboxCustomer_PhoneNum.Location = new System.Drawing.Point(517, 154);
            this.tboxCustomer_PhoneNum.Margin = new System.Windows.Forms.Padding(3, 6, 3, 3);
            this.tboxCustomer_PhoneNum.Name = "tboxCustomer_PhoneNum";
            this.tboxCustomer_PhoneNum.Size = new System.Drawing.Size(237, 29);
            this.tboxCustomer_PhoneNum.TabIndex = 14;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 6;
            this.tableLayoutPanel3.SetColumnSpan(this.tableLayoutPanel6, 4);
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel6.Controls.Add(this.btnCustomer_Clear, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.btnCustomer_Update, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.btnCustomer_Create, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.btnCustomer_Export, 5, 0);
            this.tableLayoutPanel6.Controls.Add(this.btnCustomer_Import, 4, 0);
            this.tableLayoutPanel6.Controls.Add(this.btnCustomer_Delete, 3, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(6, 259);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(6);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(745, 46);
            this.tableLayoutPanel6.TabIndex = 52;
            // 
            // btnCustomer_Clear
            // 
            this.btnCustomer_Clear.BackColor = System.Drawing.Color.SlateGray;
            this.btnCustomer_Clear.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCustomer_Clear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomer_Clear.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCustomer_Clear.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomer_Clear.Location = new System.Drawing.Point(6, 3);
            this.btnCustomer_Clear.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnCustomer_Clear.Name = "btnCustomer_Clear";
            this.btnCustomer_Clear.Size = new System.Drawing.Size(112, 40);
            this.btnCustomer_Clear.TabIndex = 6;
            this.btnCustomer_Clear.TabStop = false;
            this.btnCustomer_Clear.Text = "입력 초기화";
            this.btnCustomer_Clear.UseVisualStyleBackColor = false;
            // 
            // btnCustomer_Update
            // 
            this.btnCustomer_Update.BackColor = System.Drawing.Color.SlateGray;
            this.btnCustomer_Update.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCustomer_Update.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomer_Update.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCustomer_Update.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomer_Update.Location = new System.Drawing.Point(254, 3);
            this.btnCustomer_Update.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnCustomer_Update.Name = "btnCustomer_Update";
            this.btnCustomer_Update.Size = new System.Drawing.Size(112, 40);
            this.btnCustomer_Update.TabIndex = 2;
            this.btnCustomer_Update.TabStop = false;
            this.btnCustomer_Update.Text = "수정/저장";
            this.btnCustomer_Update.UseVisualStyleBackColor = false;
            // 
            // btnCustomer_Create
            // 
            this.btnCustomer_Create.BackColor = System.Drawing.Color.SlateGray;
            this.btnCustomer_Create.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCustomer_Create.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomer_Create.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCustomer_Create.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomer_Create.Location = new System.Drawing.Point(130, 3);
            this.btnCustomer_Create.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnCustomer_Create.Name = "btnCustomer_Create";
            this.btnCustomer_Create.Size = new System.Drawing.Size(112, 40);
            this.btnCustomer_Create.TabIndex = 1;
            this.btnCustomer_Create.TabStop = false;
            this.btnCustomer_Create.Text = "신규";
            this.btnCustomer_Create.UseVisualStyleBackColor = false;
            // 
            // btnCustomer_Export
            // 
            this.btnCustomer_Export.BackColor = System.Drawing.Color.SlateGray;
            this.btnCustomer_Export.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCustomer_Export.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomer_Export.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCustomer_Export.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomer_Export.Location = new System.Drawing.Point(626, 3);
            this.btnCustomer_Export.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnCustomer_Export.Name = "btnCustomer_Export";
            this.btnCustomer_Export.Size = new System.Drawing.Size(113, 40);
            this.btnCustomer_Export.TabIndex = 5;
            this.btnCustomer_Export.TabStop = false;
            this.btnCustomer_Export.Text = "내보내기";
            this.btnCustomer_Export.UseVisualStyleBackColor = false;
            // 
            // btnCustomer_Import
            // 
            this.btnCustomer_Import.BackColor = System.Drawing.Color.SlateGray;
            this.btnCustomer_Import.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCustomer_Import.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomer_Import.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCustomer_Import.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomer_Import.Location = new System.Drawing.Point(502, 3);
            this.btnCustomer_Import.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnCustomer_Import.Name = "btnCustomer_Import";
            this.btnCustomer_Import.Size = new System.Drawing.Size(112, 40);
            this.btnCustomer_Import.TabIndex = 7;
            this.btnCustomer_Import.TabStop = false;
            this.btnCustomer_Import.Text = "가져오기";
            this.btnCustomer_Import.UseVisualStyleBackColor = false;
            // 
            // btnCustomer_Delete
            // 
            this.btnCustomer_Delete.BackColor = System.Drawing.Color.SlateGray;
            this.btnCustomer_Delete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCustomer_Delete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCustomer_Delete.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btnCustomer_Delete.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnCustomer_Delete.Location = new System.Drawing.Point(378, 3);
            this.btnCustomer_Delete.Margin = new System.Windows.Forms.Padding(6, 3, 6, 3);
            this.btnCustomer_Delete.Name = "btnCustomer_Delete";
            this.btnCustomer_Delete.Size = new System.Drawing.Size(112, 40);
            this.btnCustomer_Delete.TabIndex = 3;
            this.btnCustomer_Delete.TabStop = false;
            this.btnCustomer_Delete.Tag = "5";
            this.btnCustomer_Delete.Text = "삭제";
            this.btnCustomer_Delete.UseVisualStyleBackColor = false;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.tableLayoutPanel5);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.groupBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(236)))), ((int)(((byte)(219)))));
            this.groupBox3.Location = new System.Drawing.Point(797, 377);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(5);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox3.Size = new System.Drawing.Size(777, 317);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "정기권 결제 정보 입력 및 이력";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel5.Controls.Add(this.dviewPayInfos, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(10, 36);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(757, 271);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // dviewPayInfos
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dviewPayInfos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dviewPayInfos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dviewPayInfos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dviewPayInfos.Location = new System.Drawing.Point(3, 3);
            this.dviewPayInfos.MultiSelect = false;
            this.dviewPayInfos.Name = "dviewPayInfos";
            this.dviewPayInfos.RowHeadersVisible = false;
            this.dviewPayInfos.RowTemplate.Height = 23;
            this.dviewPayInfos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dviewPayInfos.Size = new System.Drawing.Size(751, 265);
            this.dviewPayInfos.TabIndex = 0;
            // 
            // gboxGroupInfo
            // 
            this.gboxGroupInfo.BackColor = System.Drawing.Color.Transparent;
            this.gboxGroupInfo.Controls.Add(this.layoutGroupInfo);
            this.gboxGroupInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gboxGroupInfo.Font = new System.Drawing.Font("맑은 고딕", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gboxGroupInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(6)))), ((int)(((byte)(236)))), ((int)(((byte)(219)))));
            this.gboxGroupInfo.Location = new System.Drawing.Point(797, 704);
            this.gboxGroupInfo.Margin = new System.Windows.Forms.Padding(5);
            this.gboxGroupInfo.Name = "gboxGroupInfo";
            this.gboxGroupInfo.Padding = new System.Windows.Forms.Padding(10);
            this.gboxGroupInfo.Size = new System.Drawing.Size(777, 250);
            this.gboxGroupInfo.TabIndex = 1;
            this.gboxGroupInfo.TabStop = false;
            this.gboxGroupInfo.Text = "정기권 이력";
            // 
            // layoutGroupInfo
            // 
            this.layoutGroupInfo.ColumnCount = 1;
            this.layoutGroupInfo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.layoutGroupInfo.Controls.Add(this.lviewHistoryList, 0, 0);
            this.layoutGroupInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutGroupInfo.Location = new System.Drawing.Point(10, 36);
            this.layoutGroupInfo.Name = "layoutGroupInfo";
            this.layoutGroupInfo.RowCount = 1;
            this.layoutGroupInfo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layoutGroupInfo.Size = new System.Drawing.Size(757, 204);
            this.layoutGroupInfo.TabIndex = 0;
            // 
            // lviewHistoryList
            // 
            this.lviewHistoryList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lviewHistoryList.Font = new System.Drawing.Font("맑은 고딕", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.lviewHistoryList.FullRowSelect = true;
            this.lviewHistoryList.GridLines = true;
            this.lviewHistoryList.HideSelection = false;
            this.lviewHistoryList.Location = new System.Drawing.Point(6, 6);
            this.lviewHistoryList.Margin = new System.Windows.Forms.Padding(6);
            this.lviewHistoryList.MultiSelect = false;
            this.lviewHistoryList.Name = "lviewHistoryList";
            this.lviewHistoryList.Size = new System.Drawing.Size(745, 192);
            this.lviewHistoryList.TabIndex = 2;
            this.lviewHistoryList.TabStop = false;
            this.lviewHistoryList.UseCompatibleStateImageBehavior = false;
            this.lviewHistoryList.View = System.Windows.Forms.View.Details;
            // 
            // SognoLib_CustomerViewer_Neo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1584, 1009);
            this.Controls.Add(this.layoutMain);
            this.Font = new System.Drawing.Font("맑은 고딕", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.Name = "SognoLib_CustomerViewer_Neo";
            this.Text = "정기권 관리";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CustomerViewer_FormClosing);
            this.layoutMain.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.layoutMainLeft.ResumeLayout(false);
            this.layoutCustSearch.ResumeLayout(false);
            this.layoutCustSearch.PerformLayout();
            this.layoutSearchButtons.ResumeLayout(false);
            this.gbox_CustomerInfo.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dviewPayInfos)).EndInit();
            this.gboxGroupInfo.ResumeLayout(false);
            this.layoutGroupInfo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel layoutMain;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel layoutMainLeft;
        public System.Windows.Forms.ListView lviewCustList;
        public System.Windows.Forms.GroupBox gboxGroupInfo;
        private System.Windows.Forms.TableLayoutPanel layoutGroupInfo;
        public System.Windows.Forms.GroupBox gbox_CustomerInfo;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.ComboBox cboxCustomer_GroupName;
        private System.Windows.Forms.Label label12;
        public System.Windows.Forms.TextBox tboxCustomer_PhoneNum;
        private System.Windows.Forms.Label label13;
        public System.Windows.Forms.TextBox tboxCustomer_Name;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label21;
        public System.Windows.Forms.TextBox tboxCustomer_Memo;
        public System.Windows.Forms.Button btnCustomer_Extend_1Mon;
        public System.Windows.Forms.Button btnCustomer_Extend_Clear;
        public System.Windows.Forms.Button btnCustomer_Extend_1Year;
        public System.Windows.Forms.Button btnCustomer_Extend_6Mon;
        private System.Windows.Forms.TableLayoutPanel layoutCustSearch;
        public System.Windows.Forms.TextBox tboxSearch_Name;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox tboxSearch_CarNum;
        private System.Windows.Forms.TableLayoutPanel layoutSearchButtons;
        public System.Windows.Forms.Button btnSearch_Clear;
        public System.Windows.Forms.Button btnSearch_Run;
        public System.Windows.Forms.ComboBox cboxSearch_Group;
        private System.Windows.Forms.Label label19;
        public System.Windows.Forms.DateTimePicker dtpCustomer_StartDate;
        public System.Windows.Forms.DateTimePicker dtpCustomer_EndDate;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        public System.Windows.Forms.TextBox tboxCustomer_CarNum;
        public System.Windows.Forms.Button btnCustomer_CheckCarNum;
        private System.Windows.Forms.Label label26;
        public System.Windows.Forms.ComboBox cboxSearch_ParkName;
        public System.Windows.Forms.ComboBox cboxSearch_State;
        public System.Windows.Forms.ListView lviewHistoryList;
        public System.Windows.Forms.TextBox tboxCustomer_Comp;
        public System.Windows.Forms.TextBox tboxCustomer_Dept;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox tboxCustomer_CarName;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.ComboBox cboxCustomer_CarType;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        public System.Windows.Forms.Button btnCustomer_Clear;
        public System.Windows.Forms.Button btnCustomer_Update;
        public System.Windows.Forms.Button btnCustomer_Create;
        public System.Windows.Forms.Button btnCustomer_Export;
        public System.Windows.Forms.Button btnCustomer_Import;
        public System.Windows.Forms.Button btnCustomer_Delete;
        public System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        public System.Windows.Forms.DataGridView dviewPayInfos;
    }
}