﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Data;
using SognoLib.Core;

namespace SognoLib.VirtualAccount.VirtualAccount
{
    public class VirtualAccount
    {
        static NLog.Logger _log = NLog.LogManager.GetCurrentClassLogger();

        public SogNo_DBAccessor parkDB = null;
        public ParkInfoRecord[] parkInfos = null;

        public VirtualAccountRecodes[] virtualAccountRecodes = null;
        public VirtualAccountCustRecodes[] virtualAccountCustRecodes = null;
        public VirtualAccountInCustRecodes[] virtualAccountInCustRecodes = null;

        public VirtualAccount()
        {
            this.VirtualAccount_LoadSettings();
            this.VirtualAccount_InitDB();
            this.VirtualAccount_Init();
        }

        private void VirtualAccount_LoadSettings()
        {
            //_log.Debug("LoadSettings {0}", this.iniManager.FilePath);

            //try
            //{
            //    iniManager.GetInIProperties(this.dbConnectSettings);
            //    _log.Debug("{0}.IP = {1}", this.dbConnectSettings, this.dbConnectSettings.IpAddr);
            //    _log.Debug("{0}.Port = {1}", this.dbConnectSettings, this.dbConnectSettings.Port);
            //}
            //catch (Exception ex)
            //{
            //    _log.Error(ex, "LoadSettings Error!");
            //    Debug.Assert(false, "LoadSettings Error!");
            //}
        }

        private void VirtualAccount_InitDB()
        {
            try
            {
                this.parkDB = new SogNo_DBAccessor("192.168.0.7", 3306, "sogno");
                _log.Info("DB 연결 성공!");
            }
            catch (Exception ex)
            {
                _log.Error(ex, "DB Connection Error!");
                Debug.Assert(false, "DB Connection Error!");
            }
        }

        private void VirtualAccount_Init()
        {
            try
            {
                this.parkInfos = this.parkDB.SelectParkInfos();

                if (this.parkInfos == null || this.parkInfos.Length == 0)
                {
                    Debug.Assert(false, "데이터베이스에 주차장 정보가 없습니다.");
                    return;
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex, "주차장 정보 초기화 실패!!");
                throw;
            }
        }

        public void LoadVirtualAccount(string custNo, string assign, string states, DateTime? createDate)
        {
            this.virtualAccountRecodes = this.parkDB.SelectVirtualAccountInfos(custNo, assign, states, createDate);
        }

        public void LoadVirtualAccountCust(int? parkNo, string carNum, string name)
        {
            this.virtualAccountCustRecodes = this.parkDB.SelectVirtualAccountCustInfos(parkNo, carNum, name);
        }

        public VirtualAccountCustRecodes[] LoadVirtualAccountCust(int parkNo, int custNo)
        {
            return this.parkDB.SelectVirtualAccountCustInfos(parkNo, custNo);
        }

        public void LoadVirtualAccountInCust(int? parkNo, string carNum, string name, string assign, string states, DateTime? createDate)
        {
            this.virtualAccountInCustRecodes = this.parkDB.SelectVirtualAccountInCustInfos(parkNo, carNum, name, assign, states, createDate);
        }

        public void LoadCustInVirtualAccount(int? parkNo, string custNo, string carNum, string name, string assign, string states, DateTime? createDate)
        {
            this.virtualAccountInCustRecodes = this.parkDB.SelectCustInVirtualAccountInfos(parkNo, custNo, carNum, name, assign, states, createDate);
        }

        public void LoadCustInVirtualAccount(int? parkNo, string carNum, string name, string assign, string states, DateTime? createDate)
        {
            this.virtualAccountInCustRecodes = this.parkDB.SelectCustInVirtualAccountInfos(parkNo, carNum, name, assign, states, createDate);
        }

        public void TEST_RequestVitrualAccount()
        {
            Random r = new Random();

            string account = "";
            for(int i=0; i<10; i++)
            {
                account += r.Next(0, 10).ToString();
            }

            VirtualAccountRecodes virtualAccountRecodes = new VirtualAccountRecodes();
            virtualAccountRecodes.Account = account;
            virtualAccountRecodes.Assign = 0;
            virtualAccountRecodes.States = 0;
            virtualAccountRecodes.CreateDate = DateTime.Now;

            this.parkDB.InsertVirtualAccountInfo(virtualAccountRecodes);
        }

        public void InsertVitrualAccount(VirtualAccountRecodes virtualAccountRecodes)
        {
            this.parkDB.InsertVirtualAccountInfo(virtualAccountRecodes);
        }

        public void TEST_ReloadVitrualAccountStates()
        {
            Random r = new Random();

            foreach (var item in this.virtualAccountRecodes)
            {
                this.UpdateVirtualAccountStates(item, r.Next(0, 5));
            }
        }

        public void UpdateVirtualAccountStates(VirtualAccountRecodes recode, int delOpt)
        {
            // delOpt -> 0: 부여대기, 1: 입금대기, 2: 입금완료, 3: 자동해지, 4: 수동해지
            recode.States = delOpt;
            this.parkDB.UpdateVirtualAccountInfo(recode);
        }

        public void UpdateVirtualAccountCust(VirtualAccountCustRecodes recode)
        {
            this.parkDB.UpdateVirtualAccountCustInfos(recode);
        }
    }    
}
