﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SognoLib.Core;

namespace SognoLib.VirtualAccount.VirtualAccount
{
    public class SogNo_DBAccessor
    {
        private static NLog.Logger _log = NLog.LogManager.GetCurrentClassLogger();

        private SogNo_DBManager dbManager = null;
        public SogNo_DBAccessor(string ip, int port, string db)
        {
            try
            {
                SogNo_DBInfo dbInfo = new SogNo_DBInfo();
                dbInfo.DBIP = ip;
                dbInfo.DBPort = port;
                dbInfo.DBName = db;
                this.dbManager = new SogNo_DBManager(dbInfo);
            }
            catch
            {
                this.dbManager = null;
                throw;
            }
        }

        // **************** Park Info *********************
        public ParkInfoRecord[] SelectParkInfos(int? parkNo = null)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            if (parkNo != null)
            {
                where.Add(Tuple.Create(ParkInfoFields.ParkNo.ToString(), parkNo.ToString()));
            }

            string query = MakeNormalSelectQuery(Tables.parkinfo, where);
            return this.dbManager.GetRecords<ParkInfoRecord>(query);
        }        

        public VirtualAccountInCustRecodes[] SelectVirtualAccountInCustInfos(int? parkNo, string carNum, string name, string assign, string states, DateTime? createDate)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            string query = "SELECT " +
                "`A`.`VirtualAccInfoNo`," +
                "`A`.`CustNo`," +
                "`A`.`Account`," +
                "`A`.`Assign`," +
                "`A`.`AssignDate`," +
                "`A`.`States`," +
                "`A`.`Amount`," +
                "`A`.`Deadline`," +
                "`A`.`PayedDate`," +
                "`A`.`CreateDate`," +
                "`B`.`ParkNo`," +
                "`B`.`Name`," +
                "`B`.`TelNum`," +
                "`B`.`CarNum`," +
                "`B`.`StartDate`," +
                "`B`.`EndDate`" +
                " FROM `virtualaccinfo` AS A LEFT JOIN `virtualacccustinfo` AS B ON A.CustNo = B.CustNo";

            var where = new List<Tuple<string, string>>();
            if (parkNo != null)
            {
                where.Add(Tuple.Create(VirtualAccountCustFields.ParkNo.ToString(), parkNo.ToString()));
            }
            if (carNum != null)
            {
                where.Add(Tuple.Create(VirtualAccountCustFields.CarNum.ToString(), carNum.ToString()));
            }
            if (name != null)
            {
                where.Add(Tuple.Create(VirtualAccountCustFields.Name.ToString(), name.ToString()));
            }
            if (assign != null)
            {
                where.Add(Tuple.Create(VirtualAccountFields.Assign.ToString(), assign));
            }
            if (states != null)
            {
                where.Add(Tuple.Create(VirtualAccountFields.States.ToString(), states));
            }
            if (createDate != null)
            {
                where.Add(Tuple.Create(VirtualAccountFields.CreateDate.ToString(), createDate.ToString()));
            }

            string joinQuery = MakeNormalSelectQuery(query, where);

            return this.dbManager.GetRecords<VirtualAccountInCustRecodes>(joinQuery);
        }

        public VirtualAccountInCustRecodes[] SelectCustInVirtualAccountInfos(int? parkNo, string carNum, string name, string assign, string states, DateTime? createDate)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            string query = "SELECT " +
                "`A`.`VirtualAccInfoNo`," +
                "`B`.`CustNo`," +
                "`A`.`Account`," +
                "`A`.`Assign`," +
                "`A`.`AssignDate`," +
                "`A`.`States`," +
                "`A`.`Amount`," +
                "`A`.`Deadline`," +
                "`A`.`PayedDate`," +
                "`A`.`CreateDate`," +
                "`B`.`ParkNo`," +
                "`B`.`Name`," +
                "`B`.`TelNum`," +
                "`B`.`CarNum`," +
                "`B`.`StartDate`," +
                "`B`.`EndDate`" +
                " FROM `virtualacccustinfo` AS B LEFT JOIN `virtualaccinfo` AS A ON B.VirtualAccInfoNo = A.VirtualAccInfoNo";

            var where = new List<Tuple<string, string>>();
            if (parkNo != null)
            {
                where.Add(Tuple.Create(VirtualAccountCustFields.ParkNo.ToString(), parkNo.ToString()));
            }
            if (carNum != null)
            {
                where.Add(Tuple.Create(VirtualAccountCustFields.CarNum.ToString(), carNum.ToString()));
            }
            if (name != null)
            {
                where.Add(Tuple.Create(VirtualAccountCustFields.Name.ToString(), name.ToString()));
            }
            if (assign != null)
            {
                if (assign == "0")
                {
                    assign = "null";
                    where.Add(Tuple.Create(VirtualAccountFields.Assign.ToString(), assign.ToString()));
                }
                else
                {
                    where.Add(Tuple.Create(VirtualAccountFields.Assign.ToString(), assign));
                }
            }
            if (states != null)
            {
                where.Add(Tuple.Create(VirtualAccountFields.States.ToString(), states));
            }
            if (createDate != null)
            {
                where.Add(Tuple.Create(VirtualAccountFields.CreateDate.ToString(), createDate.ToString()));
            }

            string joinQuery = MakeNormalSelectQuery(query, where);

            return this.dbManager.GetRecords<VirtualAccountInCustRecodes>(joinQuery);
        }

        public VirtualAccountInCustRecodes[] SelectCustInVirtualAccountInfos(int? parkNo, string custNo, string carNum, string name, string assign, string states, DateTime? createDate)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            string query = "SELECT " +
                "`A`.`VirtualAccInfoNo`," +
                "`B`.`CustNo`," +
                "`A`.`Account`," +
                "`A`.`Assign`," +
                "`A`.`AssignDate`," +
                "`A`.`States`," +
                "`A`.`Amount`," +
                "`A`.`Deadline`," +
                "`A`.`PayedDate`," +
                "`A`.`CreateDate`," +
                "`B`.`ParkNo`," +
                "`B`.`Name`," +
                "`B`.`TelNum`," +
                "`B`.`CarNum`," +
                "`B`.`StartDate`," +
                "`B`.`EndDate`" +
                " FROM `virtualacccustinfo` AS B LEFT JOIN `virtualaccinfo` AS A ON B.VirtualAccInfoNo = A.VirtualAccInfoNo";

            var where = new List<Tuple<string, string>>();
            if (parkNo != null)
            {
                where.Add(Tuple.Create(VirtualAccountCustFields.ParkNo.ToString(), parkNo.ToString()));
            }
            //if (custNo != null)
            //{
            //    where.Add(Tuple.Create(VirtualAccountCustFields.CustNo.ToString(), custNo.ToString()));
            //}
            if (carNum != null)
            {
                where.Add(Tuple.Create(VirtualAccountCustFields.CarNum.ToString(), carNum.ToString()));
            }
            if (name != null)
            {
                where.Add(Tuple.Create(VirtualAccountCustFields.Name.ToString(), name.ToString()));
            }
            if (assign != null)
            {
                where.Add(Tuple.Create(VirtualAccountFields.Assign.ToString(), assign));
            }
            if (states != null)
            {
                where.Add(Tuple.Create(VirtualAccountFields.States.ToString(), states));
            }
            if (createDate != null)
            {
                where.Add(Tuple.Create(VirtualAccountFields.CreateDate.ToString(), createDate.ToString()));
            }

            string joinQuery = MakeNormalSelectQuery(query, where);
            joinQuery += String.Format("AND `B`.`CustNo`='{0}'", custNo);

            return this.dbManager.GetRecords<VirtualAccountInCustRecodes>(joinQuery);
        }

        // **************** VirtualAccount Info *********************  

        public VirtualAccountRecodes[] SelectVirtualAccountNo()
        {
            if (this.dbManager == null)
            {
                return null;
            }

            string query = String.Format("SELECT * FROM `{0}` ORDER BY `VirtualAccInfoNo` DESC", Tables.virtualaccinfo);
            return this.dbManager.GetRecords<VirtualAccountRecodes>(query);
        }
        public VirtualAccountRecodes[] SelectVirtualAccountInfos(string custNo, string assign, string states, DateTime? createDate)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            if (custNo != null)
            {
                where.Add(Tuple.Create(VirtualAccountFields.CustNo.ToString(), custNo));
            }
            if (assign != null)
            {
                where.Add(Tuple.Create(VirtualAccountFields.Assign.ToString(), assign));
            }
            if (states != null)
            {
                where.Add(Tuple.Create(VirtualAccountFields.States.ToString(), states));
            }
            if (createDate != null)
            {
                where.Add(Tuple.Create(VirtualAccountFields.CreateDate.ToString(), createDate.ToString()));
            }

            string query = MakeNormalSelectQuery(Tables.virtualaccinfo, where);
            return this.dbManager.GetRecords<VirtualAccountRecodes>(query);
        }

        public void InsertVirtualAccountInfo(VirtualAccountRecodes record)
        {
            if (this.dbManager != null)
            {
                this.dbManager.Insert(record, Tables.virtualaccinfo.ToString());
            }
        }

        public void UpdateVirtualAccountInfo(VirtualAccountRecodes record) 
		{
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    VirtualAccountFields.VirtualAccInfoNo.ToString(),
                };
                string[] setFields = {
                    VirtualAccountFields.CustNo.ToString(),
                    VirtualAccountFields.Account.ToString(),
                    VirtualAccountFields.Assign.ToString(),
                    VirtualAccountFields.AssignDate.ToString(),
                    VirtualAccountFields.States.ToString(),
                    VirtualAccountFields.Amount.ToString(),
                    VirtualAccountFields.Deadline.ToString(),
                    VirtualAccountFields.PayedDate.ToString(),
                    VirtualAccountFields.CreateDate.ToString()
                };

                this.dbManager.Update(record, Tables.virtualaccinfo.ToString(), setFields, whereFields);
            }
        }

        // **************** VirtualAccountCust Info *********************

        public VirtualAccountCustRecodes[] SelectVirtualAccountCustInfos(int? parkNo, string carNum, string name)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            if (parkNo != null)
            {
                where.Add(Tuple.Create(VirtualAccountCustFields.ParkNo.ToString(), parkNo.ToString()));
            }
            if (carNum != null)
            {
                where.Add(Tuple.Create(VirtualAccountCustFields.CarNum.ToString(), carNum.ToString()));
            }
            if (name != null)
            {
                where.Add(Tuple.Create(VirtualAccountCustFields.Name.ToString(), name.ToString()));
            }

            string query = MakeNormalSelectQuery(Tables.virtualacccustinfo, where);
            return this.dbManager.GetRecords<VirtualAccountCustRecodes>(query);
        }

        public VirtualAccountCustRecodes[] SelectVirtualAccountCustInfos(int parkNo, int custNo)
        {
            if (this.dbManager == null)
            {
                return null;
            }

            var where = new List<Tuple<string, string>>();
            where.Add(Tuple.Create(VirtualAccountCustFields.ParkNo.ToString(), parkNo.ToString()));
            where.Add(Tuple.Create(VirtualAccountCustFields.CustNo.ToString(), custNo.ToString()));

            string query = MakeNormalSelectQuery(Tables.virtualacccustinfo, where);
            return this.dbManager.GetRecords<VirtualAccountCustRecodes>(query);
        }

        public void UpdateVirtualAccountCustInfos(VirtualAccountCustRecodes record)
        {
            if (this.dbManager != null)
            {
                string[] whereFields = {
                    VirtualAccountCustFields.CustNo.ToString()
                };
                string[] setFields = {
                    VirtualAccountCustFields.VirtualAccInfoNo.ToString()
                };

                this.dbManager.Update(record, Tables.virtualacccustinfo.ToString(), setFields, whereFields);
            }
        }

        private string MakeNormalSelectQuery(Tables table, List<Tuple<string, string>> where)
        {
            string query = string.Format("SELECT * FROM `{0}`", table);

            if (where != null && where.Count > 0)
            {
                for (int i = 0; i < where.Count; i++)
                {
                    if (i == 0)
                    {
                        query += " WHERE ";
                    }
                    else
                    {
                        query += " AND ";
                    }

                    if (where[i].Item2 == null)
                    {
                        query += string.Format("`{0}` is null", where[i].Item1);
                    }
                    else
                    {
                        query += string.Format("`{0}`=\'{1}\'", where[i].Item1, where[i].Item2);
                    }
                }
            }

            return query;
        }

        private string MakeNormalSelectQuery(string joinQuery, List<Tuple<string, string>> where)
        {
            string query = joinQuery;

            if (where != null && where.Count > 0)
            {
                for (int i = 0; i < where.Count; i++)
                {
                    if (i == 0)
                    {
                        query += " WHERE ";
                    }
                    else
                    {
                        query += " AND ";
                    }

                    if (where[i].Item2 == null)
                    {
                        query += string.Format("`{0}` is null", where[i].Item1);
                    }
                    else if(where[i].Item2 == "null")
                    {
                        query += string.Format("`{0}` is null", where[i].Item1);
                    }                   
                    else
                    {
                        query += string.Format("`{0}`=\'{1}\'", where[i].Item1, where[i].Item2);
                    }
                }
            }

            return query;
        }
    }
}
