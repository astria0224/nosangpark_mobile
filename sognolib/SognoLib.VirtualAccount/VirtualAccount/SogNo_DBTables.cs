﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SognoLib.VirtualAccount.VirtualAccount
{
    public enum Tables
    {
        parkinfo,
        virtualaccinfo,
        virtualacccustinfo
    }

    public enum ParkInfoFields
    {
        ParkNo,
        ParkType,
        ParkName,
        ParkOwner,
        ParkRegNum,
        ParkPhone,
        ParkAddr,
        ParkNote,
        ParkIPAddr,
        ParkDomain,
        ParkPort,
        DefaultTrfNo,
        Reserve0
    }

    public class ParkInfoRecord
    {
        public int? ParkNo { get; set; } = null;            // 주차장 번호: auto_increment
        public int? ParkType { get; set; } = null;          // 주차장 타입 (0: 일반 주차장, 1: 등록 사용자 전용)
        public string ParkName { get; set; } = null;        // 주차장 이름
        public string ParkOwner { get; set; } = null;       // 운영자
        public string ParkRegNum { get; set; } = null;      // 사업자등록번호
        public string ParkPhone { get; set; } = null;       // 전화번호
        public string ParkAddr { get; set; } = null;        // 주차장 주소
        public string ParkNote { get; set; } = null;        // 주차장 전화번호
        public string ParkIPAddr { get; set; } = null;      // 주차장 메인 IP
        public string ParkDomain { get; set; } = null;      // 주차장 도메인
        public int? ParkPort { get; set; } = null;          // 주차장 포트
        public int? DefaultTrfNo { get; set; } = null;      // 기본 요금 체계
        public string Reserve0 { get; set; } = null;
    }

    public enum VirtualAccountFields
    {
        VirtualAccInfoNo,
        CustNo,
        Account,
        Assign,
        AssignDate,
        States,
        Amount,
        Deadline,
        PayedDate,
        CreateDate,
        Reserve0,
        Reserve1
    }

    public class VirtualAccountRecodes
    {
        public int? VirtualAccInfoNo { get; set; } = null; 
        public int? CustNo { get; set; } = null;         
        public string Account { get; set; } = null;       
        public int? Assign { get; set; } = null;      
        public DateTime? AssignDate { get; set; } = null;     
        public int? States { get; set; } = null;      
        public int? Amount { get; set; } = null;       
        public DateTime? Deadline { get; set; } = null;       
        public DateTime? PayedDate { get; set; } = null;     
        public DateTime? CreateDate { get; set; } = null; 
        public string Reserve0 { get; set; } = null;
        public string Reserve1 { get; set; } = null;
    }

    public enum VirtualAccountCustFields
    {
        CustNo,
        ParkNo,
        VirtualAccInfoNo,
        Name,
        TelNum,
        CarNum,
        StartDate,
        EndDate,
        Reserve0,
        Reserve1
    }

    public class VirtualAccountCustRecodes
    {
        public int? CustNo { get; set; } = null;
        public int? ParkNo { get; set; } = null;
        public int? VirtualAccInfoNo { get; set; } = null;
        public string Name { get; set; } = null;
        public string TelNum { get; set; } = null;
        public string CarNum { get; set; } = null;
        public DateTime? StartDate { get; set; } = null;
        public DateTime? EndDate { get; set; } = null;
        public string Reserve0 { get; set; } = null;
        public string Reserve1 { get; set; } = null;
    }

    public class VirtualAccountInCustRecodes
    {
        public int? VirtualAccInfoNo { get; set; } = null;
        public int? CustNo { get; set; } = null;
        public string Account { get; set; } = null;
        public int? Assign { get; set; } = null;
        public DateTime? AssignDate { get; set; } = null;
        public int? States { get; set; } = null;
        public int? Amount { get; set; } = null;
        public DateTime? Deadline { get; set; } = null;
        public DateTime? PayedDate { get; set; } = null;
        public DateTime? CreateDate { get; set; } = null;
        public int? ParkNo { get; set; } = null;
        public string Name { get; set; } = null;
        public string TelNum { get; set; } = null;
        public string CarNum { get; set; } = null;
        public DateTime? StartDate { get; set; } = null;
        public DateTime? EndDate { get; set; } = null;
    }
}
