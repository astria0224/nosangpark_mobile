﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO.Ports;

namespace SognoLib.Meca.MCRW
{
    public abstract class MCRWController
    {
        public static NLog.Logger _log = NLog.LogManager.GetCurrentClassLogger();
        public SerialPort serialPort;
        public Thread serialPortReadThread, serialPortWriteThread;

        public Dictionary<string, byte[]> MSRWCommand = new Dictionary<string, byte[]>();
        public string command;
        public byte[] bytesCommand;

        public string[] comList;
        public string[] baudRates = { "19200", "38400", "56000", "57600", "115200" };

        public int MAXByte = 1024;
        public bool isContinue = true;

        public bool ticketContinue = false;
        public bool autoEjectBack = false;

        public string returnData = "";
        
        //public bool OpenPort()
        //{
            //try
            //{
            //    serialPort.Open();
            //    returnData = "연결 성공";
            //    return true;
            //}
            //catch (System.IO.IOException)
            //{
            //    returnData = "연결 오류 (포트가 확인되지 않음)";
            //    return false;
            //}
            //catch (UnauthorizedAccessException)
            //{
            //    returnData = "연결 오류 (포트가 이미 열려 있음)";
            //    return false;
            //}
            //catch
            //{
            //    returnData = "연결 오류 (기타 오류)";
            //    return false;
            //}
        //}

        public void ClosePort()
        {
            serialPort.Close();
            returnData = "연결 종료";
        }

        public bool StartMCRW()
        {
            try
            {
                serialPortReadThread = new Thread(SerialPortRead);
                serialPortReadThread.Start();

                serialPortWriteThread = new Thread(SerialPortWrite);
                serialPortWriteThread.Start();

                return true;
            }
            catch (Exception ex)
            {
                _log.Warn("할인권 초기와 오류 " + ex.ToString());
                return false;
            }            
        }

        public void StopMCRW()
        {
            try
            {
                serialPortReadThread.Join();
            }
            catch { }
            try
            {
                serialPortWriteThread.Join();
            }
            catch { }
        }

        public void WriteCommand(string strTmp)
        {
            //if (strTmp != "START") command = strTmp;
            command = strTmp;
            try
            {
                bytesCommand = null;

                byte[] tmpBytes = MSRWCommand[strTmp];
                serialPort.Write(tmpBytes, 0, tmpBytes.Length);
            }
            catch { }
        }

        abstract public bool OpenPort();
        abstract public void SerialPortRead();
        abstract public void SerialPortWrite();

        abstract public void Read();
        abstract public void ReadEnd();
        abstract public void ReadCancel();
        abstract public void EjectBack();
        abstract public void EjectFront();
        abstract public void InitMeca();
        abstract public bool ReadData();
        abstract public void BuffClear();

        public delegate void MCRWEventHandler(object sender, MCRWController_EventArgs e);

        public event MCRWEventHandler MCRWHandler;

        public void PassMessageTo(string data)
        {
            if (MCRWHandler != null)
            {
                MCRWController_EventArgs MCRWArgs = new MCRWController_EventArgs(data);
                MCRWHandler(this, MCRWArgs);
            }
        }

        public void PassMessageTo(MCRWController_Message message)
        {
            if (MCRWHandler != null)
            {
                MCRWController_EventArgs MCRWArgs = new MCRWController_EventArgs(message);
                MCRWHandler(this, MCRWArgs);
            }
        }
    }

    public class TCM_R130B : MCRWController
    {
        public const byte STX = 0x7E;
        public const byte ETX = 0x7D;
        public const byte ReadCheck = 0x30;

        public const byte Track1_Seperator = 0x11;
        public const byte Track2_Seperator = 0x12;
        public const byte Track3_Seperator = 0x13;
        public const byte Blank = 0x00;

        public byte[] READ = { 0x7E, 0x06, 0x00, 0x61, 0x37, 0x38, 0x00, 0x30, 0x30, 0x6B, 0xA5, 0x7D };
        public byte[] READ_FAIL = { 0x7E, 0x03, 0x00, 0x61, 0x30, 0x33, 0x4F, 0x71, 0x7D };
        public byte[] READ_END = { 0x7E, 0x06, 0x00, 0x61, 0x37, 0x38, 0x00, 0x30, 0x31, 0x7B, 0x84, 0x7D };
        public byte[] READ_END_FAIL = { 0x7E, 0x03, 0x00, 0x61, 0x33, 0x30, 0x1C, 0x24, 0x7D };
        public byte[] READ_CANCEL = { 0x7E, 0x06, 0x00, 0x61, 0x37, 0x38, 0x00, 0x31, 0x32, 0x78, 0xD6, 0x7D };

        public byte[] EJECT_BACK = { 0x7E, 0x02, 0x00, 0x62, 0x30, 0xB6, 0x73, 0x7D };
        public byte[] EJECT_BACK_OK = { 0x7E, 0x02, 0x00, 0x62, 0x30, 0x73, 0xB6, 0x7D };

        public byte[] EJECT_FRONT = { 0x7E, 0x01, 0x00, 0x28, 0x92, 0x5A, 0x7D };
        public byte[] EJECT_FRONT_OK = { 0x7E, 0x02, 0x00, 0x28, 0x30, 0x74, 0x54, 0x7D };

        public byte[] BUFF_CLEAR = { 0x7E, 0x01, 0x00, 0x27, 0x63, 0xB5, 0x7D };
        public byte[] BUFF_CLEAR_OK = { 0x7E, 0x02, 0x00, 0x27, 0x30, 0x4A, 0x44, 0x7D };

        public byte[] INIT_MECA = { 0x7E, 0x02, 0x00, 0x63, 0x30, 0x85, 0x42, 0x7D };
        public byte[] INIT_OK = { 0x7E, 0x02, 0x00, 0x63, 0x30, 0x42, 0x85, 0x7D };
        //public byte[] INIT_FAIL = { 0x7E, 0x02, 0x00, 0x63, 0x30, 0x85, 0x42, 0x7D };

        //public TCM_R130B(SerialPort sPort)
        public TCM_R130B()
        {
            MSRWCommand["READ"] = READ;
            MSRWCommand["READ_FAIL"] = READ_FAIL;
            MSRWCommand["READ_END"] = READ_END;
            MSRWCommand["READ_END_FAIL"] = READ_END_FAIL;
            MSRWCommand["READ_CANCEL"] = READ_CANCEL;

            MSRWCommand["EJECT_BACK"] = EJECT_BACK;
            MSRWCommand["EJECT_BACK_OK"] = EJECT_BACK_OK;

            MSRWCommand["EJECT_FRONT"] = EJECT_FRONT;
            MSRWCommand["EJECT_FRONT_OK"] = EJECT_FRONT_OK;

            MSRWCommand["BUFF_CLEAR"] = BUFF_CLEAR;
            MSRWCommand["BUFF_CLEAR_OK"] = BUFF_CLEAR_OK;

            MSRWCommand["INIT_MECA"] = INIT_MECA;
            MSRWCommand["INIT_OK"] = INIT_OK;

            serialPort = new SerialPort();
            if (OpenPort())
            {
                StartMCRW();
            }
        }

        public override bool OpenPort()
        {
            bool ret = false;

            comList = System.IO.Ports.SerialPort.GetPortNames();
            if (comList.Length == 0) throw new Exception();
            
            for (int i=0; i<comList.Length; i++)
            {               

                for (int j=0; j<baudRates.Length; j++)
                {
                    serialPort.PortName = comList[i];
                    serialPort.BaudRate = Convert.ToInt32(baudRates[j]);

                    try
                    {
                        serialPort.Open();
                        InitMeca();
                        Thread.Sleep(1000);
                        string readBytes = serialPort.ReadExisting();

                        if (readBytes == "")
                        {
                            serialPort.Close();
                            ret = false;
                        }
                        else
                        {
                            ret = true;
                            break;
                        }
                    }
                    catch
                    {
                        serialPort.Close();
                        ret = false;
                        continue;
                    }
                }
            }

            return ret;
        }

        public override void Read()
        {
            WriteCommand("READ");
        }

        public override void ReadCancel()
        {
            WriteCommand("READ_CANCEL");
        }

        public override void ReadEnd()
        {
            WriteCommand("READ_END");
        }

        public override void EjectBack()
        {
            WriteCommand("EJECT_BACK");
        }

        public override void EjectFront()
        {
            WriteCommand("EJECT_FRONT");
        }

        public override void BuffClear()
        {
            WriteCommand("BUFF_CLEAR");
        }

        public override void InitMeca()
        {
            WriteCommand("INIT_MECA");
        }

        public override void SerialPortRead()
        {
            int tmpSPortRByteCount = 0; byte[] tmpSPortReadBytes = new byte[MAXByte];
            while (isContinue)
            {
                try
                {
                    tmpSPortReadBytes[tmpSPortRByteCount++] = Convert.ToByte(serialPort.ReadByte());
                    if (tmpSPortReadBytes[tmpSPortRByteCount - 1] == ETX)
                    {
                        bytesCommand = new byte[tmpSPortRByteCount];
                        Array.Copy(tmpSPortReadBytes, bytesCommand, bytesCommand.Length);
                        //Console.WriteLine(Encoding.Default.GetString(bytesCommand));

                        tmpSPortRByteCount = 0;
                    }
                }
                catch (TimeoutException) { }
                catch (Exception) { }
            }
        }

        public override void SerialPortWrite()
        {
            while (isContinue)
            {
                if (bytesCommand != null)
                {
                    try
                    {
                        if (bytesCommand[3] == 0x61 && bytesCommand[4] != 0x30 && command != "")
                        {
                            Read();
                        }
                        else if (bytesCommand[3] == 0x20 && bytesCommand[12] == 0x00 && bytesCommand[13] == 0x00 && command != "")
                        {
                            Read();
                        }
                        else
                        {
                            if (command == "READ")
                            {
                                if (bytesCommand.SequenceEqual(READ_FAIL))
                                {
                                    Read();
                                }
                                else
                                {
                                    ReadEnd();
                                }
                            }
                            else if (command == "READ_END")
                            {
                                if (bytesCommand.SequenceEqual(READ_END_FAIL))
                                {
                                    Read();
                                }
                                else
                                {
                                    ReadData();

                                    //if (!ReadData())
                                    //{
                                    //    Read();
                                    //}
                                    //else
                                    //{
                                    //    if (ticketContinue)
                                    //    {
                                    //        Read();
                                    //    }
                                    //}

                                    //if (!ReadData()) BuffClear(); // 데이터 읽은 후 버퍼를 비워야 함
                                    ////else Read();

                                    //if (autoEjectBack)
                                    //{
                                    //    EjectBack();
                                    //}

                                    //if (ticketContinue)
                                    //{                                        
                                    //    Read();
                                    //}

                                }
                            }
                            else if (command == "EJECT_FRONT" && !bytesCommand.SequenceEqual(EJECT_FRONT_OK))
                            {
                                EjectFront();

                                //if (ticketContinue)
                                //{
                                //    Thread.Sleep(4000);
                                //    Read();
                                //}
                            }
                            else if (command == "EJECT_BACK" && !bytesCommand.SequenceEqual(EJECT_BACK_OK))
                            {
                                EjectBack();
                            }
                            //else if (command == "EJECT_BACK")
                            //{
                            //    if (!bytesCommand.SequenceEqual(EJECT_BACK_OK))
                            //    {
                            //        EjectBack();
                            //    }
                            //    else
                            //    {
                            //        BuffClear();
                            //    }
                            //}
                            else if (command == "BUFF_CLEAR" && !bytesCommand.SequenceEqual(BUFF_CLEAR_OK))
                            {
                                BuffClear();
                            }
                            else if (command == "INIT_MECA" && !bytesCommand.SequenceEqual(INIT_OK))
                            {
                                InitMeca();
                            }
                        }
                    }
                    catch { }

                    bytesCommand = null;
                }
                Thread.Sleep(500);                
            }
        }

        public override bool ReadData()
        {            
            if (bytesCommand[4] != ReadCheck) return false;

            //string strTmp = "";
            //int CurIndex;
            //for (CurIndex = 7; CurIndex < bytesCommand.Length; CurIndex++)
            //{
            //    //if (bytesCommand[CurIndex] == ETX) break;
            //    //if (bytesCommand[CurIndex] != 0x00) strTmp += Convert.ToChar(bytesCommand[CurIndex]);                
            //    if (bytesCommand[CurIndex] == 0x00) break;
            //    strTmp += Convert.ToChar(bytesCommand[CurIndex]);

            //}
            //Console.WriteLine(strTmp);
            ////PassMessageTo(strTmp);
            ////MCRWController_Message message = new MCRWController_Message();
            ////message.Track1Data

            //string strTmp = "", strTmp2 = ""; int CurIndex = 6;

            string track1 = "", track2 = "", track3 = "";
            int track1Idx = 0, track2Idx = 0, track3Idx = 0;
            byte[] byteTmp = new byte [bytesCommand[1]];
            Array.Copy(bytesCommand, 1, byteTmp, 0, Convert.ToInt32(bytesCommand[1]));

            for (int i=0; i< byteTmp.Length; i++)
            {
                if (byteTmp[i] == Track1_Seperator) track1Idx = i;
                if (byteTmp[i] == Track2_Seperator) track2Idx = i;
                if (byteTmp[i] == Track3_Seperator)
                {
                    track3Idx = i;
                    break;
                }
            }

            for (int i=track1Idx+1; i<track2Idx-1; i++)
            {
                track1 += Convert.ToChar(byteTmp[i]);
            }

            for (int i = track2Idx + 1; i < track3Idx - 1; i++)
            {
                track2 += Convert.ToChar(byteTmp[i]);
            }

            for (int i = track3Idx + 1; i < byteTmp.Length; i++)
            {
                track3 += Convert.ToChar(byteTmp[i]);
                if (byteTmp[i] == Blank) break;
            }

            MCRWController_Message message = new MCRWController_Message();
            message.Track1Data = track1;
            message.Track2Data = track2;
            message.Track3Data = track3;

            PassMessageTo(message);

            return true;
        }
    }

    public enum EMCRWMsgID
    {
        Unknown,
        MCRW
    };

    public class MCRWController_Message
    {
        public const byte STX = 0x02;
        public const byte ETX = 0x03;
        public const byte Seperator = (byte)'#';
        public const byte TextSign = (byte)'T';
        public const byte BinarySign = (byte)'B';

        public string Track1Data { get; set; }
        public string Track2Data { get; set; }
        public string Track3Data { get; set; }

        public char CharSTX
        {
            get
            {
                return Convert.ToChar(STX);
            }
        }
        public char CharETX
        {
            get
            {
                return Convert.ToChar(ETX);
            }
        }
        public char CharTextSign
        {
            get
            {
                return Convert.ToChar(TextSign);
            }
        }

        public static EMCRWMsgID ParseMsgID(string messageBody)
        {
            EMCRWMsgID msgID = EMCRWMsgID.Unknown;

            int idx = messageBody.IndexOf(Convert.ToChar(Seperator));
            if (idx > 0)
            {
                msgID = StringToMsgID(messageBody.Substring(0, idx));
            }
            else
            {
                msgID = StringToMsgID(messageBody);
            }

            return msgID;
        }

        public static EMCRWMsgID ParseMsgID(byte[] messageBody)
        {
            EMCRWMsgID msgID = EMCRWMsgID.Unknown;

            int idx = Array.IndexOf(messageBody, Seperator);
            if (idx > 0)
            {
                msgID = StringToMsgID(Encoding.Default.GetString(messageBody, 0, idx));
            }
            else
            {
                msgID = StringToMsgID(Encoding.Default.GetString(messageBody));
            }

            return msgID;
        }

        private static EMCRWMsgID StringToMsgID(string strID)
        {
            EMCRWMsgID msgID = EMCRWMsgID.Unknown;

            if (Enum.IsDefined(typeof(EMCRWMsgID), strID))
            {
                msgID = (EMCRWMsgID)Enum.Parse(typeof(EMCRWMsgID), strID);
            }

            return msgID;
        }

        public byte[] Pack()
        {
            string msgBody = string.Format("{0}#{1}#{2}#{3}", EMCRWMsgID.MCRW, this.Track1Data, this.Track2Data, this.Track3Data);

            int bodyLen = Encoding.Default.GetBytes(msgBody).Length;

            string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, CharTextSign, bodyLen, msgBody, CharETX);
            return Encoding.Default.GetBytes(message);
        }

        public void UnPack(string messageBody)
        {
            EMCRWMsgID msgID = ParseMsgID(messageBody);
            if (msgID != EMCRWMsgID.MCRW)
            {
                throw new Exception("메시지 ID가 일치하지 않음.");
            }

            string[] tokens = messageBody.Split(new char[1] { '#' });
            if (tokens.Length != 4)
            {
                throw new Exception("토큰의 갯수가 일치하지 않음.");
            }

            try
            {
                this.Track1Data = tokens[1];
                this.Track2Data = tokens[2];
                this.Track3Data = tokens[3];
            }
            catch (Exception)
            {
                throw new Exception("메시지 형식이 일치하지 않습니다.");
            }
        }
    }

    public class MCRWController_EventArgs : System.EventArgs
    {
        public string data;
        public MCRWController_Message message;

        public MCRWController_EventArgs(string data)
        {
            this.data = data;
        }

        public MCRWController_EventArgs(MCRWController_Message message)
        {
            this.message = message;
        }
    }
}
