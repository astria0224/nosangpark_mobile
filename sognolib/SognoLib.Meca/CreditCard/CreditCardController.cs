﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;

namespace SognoLib.Meca.CreditCard
{
    public abstract class CreditCardController
    {
        public string cardMoney;
       
        abstract public void CardPayment(string money);
        abstract public void CardPaymentChange(string money);
        abstract public void CardPaymentStop();
        abstract public void CardPaymentCancel();
        abstract public void CardPaymentCancelNoCard();

        public delegate void CreditCardEventHandler(object sender, CreditCardController_EventArgs e);

        public event CreditCardEventHandler CreditCardHandler;

        public void PassMessageTo(byte[] message)
        {
            if (CreditCardHandler != null)
            {
                CreditCardController_EventArgs CreditCardArgs = new CreditCardController_EventArgs(Encoding.Default.GetString(message));
                CreditCardHandler(this, CreditCardArgs);
            }
        }

        public void PassMessageTo(CreditCardController_Message message)
        {
            if (CreditCardHandler != null)
            {
                CreditCardController_EventArgs CreditCardArgs = new CreditCardController_EventArgs(message);
                CreditCardHandler(this, CreditCardArgs);
            }
        }
    }

    public class EVCAT : CreditCardController
    {
        public AxDreampos_Ocx.AxDP_Certification_Ocx axDP_Certification_Ocx;
        public DreamposControl dreamposControl;
        
        
        public string fs = Strings.Chr(28).ToString();
        public string CR = Strings.Chr(13).ToString();

        public string txt_응답코드;
        public string txt_결제구분;
        public string txt_결제매체;
        public string txt_원거래일;
        public string txt_결제일시;
        public string txt_응답카드번호;
        public string txt_카드번호;
        public string txt_원승인번호;
        public string txt_응답승인번호;
        public string txt_가맹점번호;
        public string txt_매입구분;
        public string txt_발급사코드;
        public string txt_발급사명;
        public string txt_매입사코드;
        public string txt_매입사명;
        public string txt_응답내용;
        public string txt_선불카드잔액;
        public string txt_티머니자료;
        public string txt_요청사용자키;
        public string txt_결제금액;
        public string txt_결제부가세;

        public string txt_결제posid;
        public string txt_전문일련번호;
        public string txt_m전문일련번호;
        public string txt_Pg거래번호;

        public EVCAT()
        {
            this.dreamposControl = new DreamposControl();
            this.axDP_Certification_Ocx = this.dreamposControl.axDP_Certification_Ocx;
            this.axDP_Certification_Ocx.QueryResults += new AxDreampos_Ocx.@__DP_Certification_Ocx_QueryResultsEventHandler(this.dream_QueryResults);
        }

        private void dream_QueryResults(object sender, AxDreampos_Ocx.__DP_Certification_Ocx_QueryResultsEvent e)
        {
            CreditCardController_Message message = new CreditCardController_Message();
            message.Step = EEVCATMsgID.ReaderEventStep;
            message.ReturnData = e.returnData;

            try
            {
                //이벤트 응답 부분입니다.
                //리더기응답 = 카드가 삽입되고 카드정보를 읽혀을 경우 "리더기응답" 이라는 이벤트가 발생 됩니다.
                //             이 이벤트가 필요 없으면 EVCAT에서 해지 하세요.

                //FALL BACK 발생 = FALL BACK이 사용중이고 카드가 잘못 들었갔거나 IC카드가 손상 되었을 경우 이벤트로 전달 됩니다.
                //                 이 부분 수신 시 POS 화면에 "카드를 정상적인 방법으로 다시 넣어서 빼 주세요.." 라든지 "다른 카드로 다시 넣어 주세요" 라는 메세지를 뿌려 주시면 됩니다.

                if (e.returnData == "FALL BACK 발생" || e.returnData == "리더기응답" || e.returnData == "E이미 요청중입니다!")
                {
                    //rich_logTxt2.Text += Environment.NewLine + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " 이벤트 : " + e.returnData;
                    if (e.returnData == "FALL BACK 발생")
                    {                        
                        message.ReturnMsg = EEVCATMsgID.FallBack;                        
                    }
                    else if (e.returnData == "리더기응답")
                    {                      
                        message.ReturnMsg = EEVCATMsgID.RederResponse;                        
                    }

                    //EV-CAT 프로그램으로 명령을 전달 후 응답이 오지 않은 상태에서 명령어를 전송 한 경우 입니다.
                    //이런 경우는 이전 명령어가 리턴될 때까지 기다려야 합니다.
                    //테스트는 승인번튼을 두번 누루시면 이벤트가 발생 합니다.
                    else if (e.returnData == "E이미 요청중입니다!")
                    {
                        message.ReturnMsg = EEVCATMsgID.AllredyRequest;                     
                    }

                    PassMessageTo(message);
                    return;
                }

                //전문 체크 부분입니다.
                // char(7)과 char(3)이 전문의 시작과 끝을 의미 합니다.
                if (e.returnData.Contains((char)7) == true && e.returnData.Contains((char)3))
                {                    
                    string Snd = "";
                    Snd = e.returnData.Substring(e.returnData.IndexOf((char)7) + 1, e.returnData.IndexOf((char)3) - e.returnData.IndexOf((char)7) - 1);
                    string[] kk = Regex.Split(Snd, Convert.ToChar(6).ToString());
                    //rich_logTxt2.Text += Environment.NewLine + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + e.returnData;
                    전문분리작업(kk[0], kk[1], kk[2], kk[3]);
                }
                else
                {
                    //특이사항 에러가 발생 시 
                    //rich_logTxt2.Text += Environment.NewLine + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " 비 정상 응답:" + e.returnData;

                    //소켓 초기화
                    //bool bo = false;
                    //axDP_Certification_Ocx.set_xProcess(ref bo);
                }
            }
            catch (Exception ex)
            {
                //소켓 초기화
                bool bo = false;
                axDP_Certification_Ocx.set_xProcess(ref bo);
            }
        }

        private void 전문분리작업(string AccessNo, string SuccessYn, string MethodsofPayment, string ReturnData)
        {
            CreditCardController_Message message = new CreditCardController_Message();
            message.Step = EEVCATMsgID.ReaderResponseStep;
            message.ReturnData = ReturnData;

            try
            {
                if (ReturnData == "")
                {
                    //rich_logTxt2.Text += Environment.NewLine + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " 응답 값 없음";
                    return;
                }

                //리더기 RESET 요청 후 결과 코드 값 = "C"
                if (SuccessYn == "C") { return; }

                if (SuccessYn == "E")
                {                 
                    if (MethodsofPayment.Trim() == "TIMER")
                    {
                        message.ReturnMsg = EEVCATMsgID.TimeOut;
                    }
                    else
                    {
                        message.ReturnMsg = EEVCATMsgID.Error;
                    }
                    
                    PassMessageTo(message);

                    return;
                }

                //리더기 카드 감지 이벤트 or 체크.. EVCAT에서 "카드삽입이벤트" 체크 시 IC카드가 삽입 되면 발생("CARD IN")
                //카드 감지 후 카드 승인 호출은 다시 보내야 합니다.
                //카드가 언제 들어올지 모르는 경우 체크 시 사용 하세요. (무인키오스크, 할인권 처리 시 사용하세요..)

                if (SuccessYn == "W")
                {
                    //rich_logTxt2.Text += Environment.NewLine + "카드 감지 =" + ReturnData;

                    //이 부분이 카드가 감지 된 부분.  2289, 2288 모두 응답
                    if (ReturnData == "CARD IN")
                    {
                        //단 이벤트 호출 시 여기서 카드 승인 부분을 바로 호출해도 됨.
                        //여기에 타임머를 사용하여 결제 승인을 요청 하세요
                        //결제 함수를 직접 호출하지 마세요.

                        bool bo = false;
                        axDP_Certification_Ocx.set_xProcess(ref bo);

                        message.ReturnMsg = EEVCATMsgID.CardIn;                        
                        PassMessageTo(message);
                        return;
                    }

                    //2288 요청 시에만 응답
                    if (ReturnData == "NO CARD")
                    {
                        message.ReturnMsg = EEVCATMsgID.NoCard;
                        PassMessageTo(message);
                        return;
                    }

                    //2288 요청 시에만 응답
                    if (ReturnData == "ERROR")
                    {
                        message.ReturnMsg = EEVCATMsgID.CardInError;
                        PassMessageTo(message);
                        return;
                    }
                    return;
                }
                else if (SuccessYn == "Y")
                {
                    message.ReturnMsg = EEVCATMsgID.PaySuccess;                    
                }

                //전문 분리
                string[] retValue = Regex.Split(ReturnData, Convert.ToChar(28).ToString().ToString());

                switch (MethodsofPayment.Trim())
                {
                    //서명패드 암호화 워킹키 값 리턴 정보
                    //확인 용
                    case "WK":
                        break;


                    //*******************************************************************************
                    //이벤트 응답 or 에러 발생 시
                    case "TO":
                        //rich_logTxt2.Text += Environment.NewLine + ReturnData;
                        break;
                    //*******************************************************************************


                    //*******************************************************************************
                    //이 부분이 EV-CAT으로부터 카드 승인이나 취소 등 응답을 받는 부분입니다.
                    //retValue(0)가 "00"은 정상 값이며, 그 외는 무조건 에러입니다.
                    //*******************************************************************************
                    //GTF는 카드 BIN 체크 후 카드를 그냥 배출 할 경우 "6090"을 요청 후 배출이 되면
                    //응답코드로 "GT"라고 넘어옵니다.
                    default:
                        if (retValue.Length >= 18)
                        {
                            // 응답코드 =  retValue[0]
                            // 모든 거래의 카드승인/취소 정상 값  = "00"
                            // GTF 카드 배출 응답 = "GT", GTF 카드 승인 요청 후 정상 승인 값은 "00"으로 같음.
                            // 그외 코드는 모두 에러

                            message.txt_응답코드 = retValue[0];
                            message.txt_결제구분 = retValue[1];
                            message.txt_결제매체 = retValue[2];
                            message.txt_원거래일 = retValue[3];
                            message.txt_결제일시 = retValue[4];
                            message.txt_응답카드번호 = retValue[5];
                            message.txt_카드번호 = retValue[5];
                            message.txt_원승인번호 = retValue[6];
                            message.txt_응답승인번호 = retValue[6];
                            message.txt_가맹점번호 = retValue[7];
                            message.txt_매입구분 = retValue[8];
                            message.txt_발급사코드 = retValue[9];
                            message.txt_발급사명 = retValue[10];
                            message.txt_매입사코드 = retValue[11];
                            message.txt_매입사명 = retValue[12];
                            message.txt_응답내용 = retValue[13];
                            message.txt_선불카드잔액 = retValue[14];
                            message.txt_티머니자료 = retValue[15];
                            message.txt_요청사용자키 = retValue[16];
                            message.txt_결제금액 = retValue[17];
                            message.txt_결제부가세 = retValue[18];

                            txt_응답코드 = retValue[0];
                            txt_결제구분 = retValue[1];
                            txt_결제매체 = retValue[2];
                            txt_원거래일 = retValue[3];
                            txt_결제일시 = retValue[4];
                            txt_응답카드번호 = retValue[5];
                            txt_카드번호 = retValue[5];
                            txt_원승인번호 = retValue[6];
                            txt_응답승인번호 = retValue[6];
                            txt_가맹점번호 = retValue[7];
                            txt_매입구분 = retValue[8];
                            txt_발급사코드 = retValue[9];
                            txt_발급사명 = retValue[10];
                            txt_매입사코드 = retValue[11];
                            txt_매입사명 = retValue[12];
                            txt_응답내용 = retValue[13];
                            txt_선불카드잔액 = retValue[14];
                            txt_티머니자료 = retValue[15];
                            txt_요청사용자키 = retValue[16];
                            txt_결제금액 = retValue[17];
                            txt_결제부가세 = retValue[18];

                            if (retValue.Length >= 19)
                            {
                                message.txt_결제posid = retValue[19];
                                txt_결제posid = retValue[19];
                            }

                            if (retValue.Length >= 20)
                            {
                                message.txt_전문일련번호 = retValue[20];
                                txt_전문일련번호 = retValue[20];

                                //무카드 카드 취소를 위해서는 아래와 같이 전문 형태로 보내 주셔야 합니다
                                //DB에 retValue[2] , retValue[20] , retValue[19]의 값을 모두 저장 후 (char)9 를 구분자로 보내 주시면 됩니다.
                                message.txt_m전문일련번호 = retValue[2] + (char)9 + retValue[20] + (char)9 + retValue[19];
                                txt_m전문일련번호 = retValue[2] + (char)9 + retValue[20] + (char)9 + retValue[19];
                            }

                            //이 부분은 PG 사용 시 PG 거래번호가 넘어오는 부분입니다.
                            //21번쨰 배열이 있는 전문은 PG가 사용된 거래입니다.
                            if (retValue.Length >= 22)
                            {
                                message.txt_Pg거래번호 = retValue[21];
                                txt_Pg거래번호 = retValue[21];
                            }
                        }
                        break;
                }
                PassMessageTo(message);
            }
            catch (Exception ex)
            {

            }
        }

        public override void CardPayment(string money)
        {            
            try
            {
                cardMoney = money;
                //string space_chk = "";
                ////rich_logTxt1.Text = "";
                //if (txt_봉사료.Text.Trim() == "") { txt_봉사료.Text = "0"; }
                //if (txt_xHalbu.Text.Trim() == "") { txt_xHalbu.Text = "0"; }

                //결제 방법/제어 방법
                object a = "1000";
                this.axDP_Certification_Ocx.set_Set_Code(ref a);
                //결제 구분
                object b = "1100";
                this.axDP_Certification_Ocx.set_Set_CodeGu(ref b);
                //결제 금액
                object c = cardMoney;
                this.axDP_Certification_Ocx.set_Set_Gold(ref c);
                //봉사료 금액
                object d = 0;
                this.axDP_Certification_Ocx.set_Set_ServiceGold(ref d);

                object i = 0;
                this.axDP_Certification_Ocx.set_Set_VAT(ref d);

                //할부
                object f = 00;
                this.axDP_Certification_Ocx.set_Set_SaleMonth(ref f);

                //사용자 고유코드
                //object g = txt_사용자코드.Text.Trim();
                //axDP_Certification_Ocx1.set_Set_SetUserCode(ref g);

                ////**********************************
                ////*             필독               *
                ////**********************************
                ////PG 사용 시 PG정보
                ////모든 필드는 Byte 단위(한글은 2 byte)
                ////사용하지 않는 필드는 해당 크기만큼 Space 처리
                ////**********************************
                ////TID = 40 Byte
                ////구매자 = 30 Byte
                ////전화 = 20 Byte
                ////이메일 = 40 Byte
                ////HP = 20 Byte
                ////상품명 = 50 Byte
                ////주소 = 100 Byte
                ////수취인 메세지 = 50 Byte
                ////*********************************

                //// m전문일련번호 = 취소 시만 사용

                ////PG 사용 시 PG정보

                ////BL_TID 
                //string h = getWordByByte(PgBL_TID.Text + space_chk.PadLeft(40, ' '), 40);
                //axDP_Certification_Ocx1.set_set_Pg_BL_TID(ref h);

                ////PgName 
                //string l = getWordByByte(PgName.Text + space_chk.PadLeft(30, ' '), 30);
                //axDP_Certification_Ocx1.set_set_Pg_Name(ref l);

                ////PgTel 
                //string m = getWordByByte(PgTel.Text + space_chk.PadLeft(20, ' '), 20);
                //axDP_Certification_Ocx1.set_set_Pg_Tel(ref m);

                ////PgEmail 
                //string n = getWordByByte(PgEmail.Text + space_chk.PadLeft(40, ' '), 40);
                //axDP_Certification_Ocx1.set_set_Pg_Email(ref n);

                ////PgHP 
                //string o = getWordByByte(PgHP.Text + space_chk.PadLeft(20, ' '), 20);
                //axDP_Certification_Ocx1.set_set_Pg_HP(ref o);

                ////PgProduct 
                //string p = getWordByByte(PgProduct.Text + space_chk.PadLeft(50, ' '), 50);
                //axDP_Certification_Ocx1.set_set_Pg_Product(ref p);

                ////PgAddress 
                //string q = getWordByByte(PgAddress.Text + space_chk.PadLeft(100, ' '), 100);
                //axDP_Certification_Ocx1.set_set_Pg_Address(ref q);

                ////PgRecvMessage 
                //string r = getWordByByte(PgRecvMessage.Text + space_chk.PadLeft(50, ' '), 50);
                //axDP_Certification_Ocx1.set_set_Pg_RecvMessage(ref r);

                ////전문일련번호 
                string r1 = "";
                axDP_Certification_Ocx.set_set_Transer_Number(ref r1);

                string r2 = "";
                axDP_Certification_Ocx.set_set_Mutil_CATID(ref r2);

                object k = "OCX";
                this.axDP_Certification_Ocx.set_Set_OcxProcess(ref k);

                this.axDP_Certification_Ocx.SendRecv();
            }
            catch (Exception ex)
            {

            }
        }

        public override void CardPaymentChange(string money)
        {
            try
            {
                cardMoney = money;
                //rich_logTxt1.Text = "";
                //if (txt_봉사료.Text.Trim() == "") { txt_봉사료.Text = "0"; }
                //if (txt_xHalbu.Text.Trim() == "") { txt_xHalbu.Text = "0"; }

                //결제 방법/제어 방법
                object a = "5555";
                axDP_Certification_Ocx.set_Set_Code(ref a);
                //결제 구분
                object b = "2200";
                axDP_Certification_Ocx.set_Set_CodeGu(ref b);
                //결제 금액
                object c = money;
                axDP_Certification_Ocx.set_Set_Gold(ref c);

                object d = 0;
                axDP_Certification_Ocx.set_Set_VAT(ref d);

                //봉사료 금액
                object i = 0;
                axDP_Certification_Ocx.set_Set_ServiceGold(ref d);
                //할부
                object f = 00;
                axDP_Certification_Ocx.set_Set_SaleMonth(ref f);
                //사용자 고유코드
                //object g = txt_사용자코드.Text.Trim();
                //axDP_Certification_Ocx.set_Set_SetUserCode(ref g);

                //전문일련번호 
                string r1 = "";
                axDP_Certification_Ocx.set_set_Transer_Number(ref r1);
                string r2 = "";
                axDP_Certification_Ocx.set_set_Mutil_CATID(ref r2);

                object h = "OCX";
                axDP_Certification_Ocx.set_Set_OcxProcess(ref h);

                axDP_Certification_Ocx.SendRecv();
            }
            catch (Exception ex)
            {

            }
        }

        public override void CardPaymentStop()
        {
            try
            {
                //rich_logTxt1.Text = "";

                //결제 방법/제어 방법
                object a = "8888";
                axDP_Certification_Ocx.set_Set_Code(ref a);
                //결제 구분
                object b = "2200";
                axDP_Certification_Ocx.set_Set_CodeGu(ref b);

                //사용자 고유코드
                //object c = txt_사용자코드.Text.Trim();
                //dream.set_Set_SetUserCode(ref c);

                //전문일련번호 
                string r1 = "";
                axDP_Certification_Ocx.set_set_Transer_Number(ref r1);
                string r2 = "";
                axDP_Certification_Ocx.set_set_Mutil_CATID(ref r2);

                bool bo = false;
                axDP_Certification_Ocx.set_xProcess(ref bo);

                object k = "OCX";
                axDP_Certification_Ocx.set_Set_OcxProcess(ref k);

                axDP_Certification_Ocx.SendRecv();
            }
            catch (Exception ex)
            {
               
            }
        }

        public override void CardPaymentCancel()
        {
            try
            {
                //string space_chk = "";
                //rich_logTxt1.Text = "";
                //if (txt_봉사료.Text.Trim() == "") { txt_봉사료.Text = "0"; }
                //if (txt_xHalbu.Text.Trim() == "") { txt_xHalbu.Text = "0"; }

                //결제 방법/제어 방법
                object a = "1000";
                axDP_Certification_Ocx.set_Set_Code(ref a);
                //결제 구분
                object b = "2200";
                axDP_Certification_Ocx.set_Set_CodeGu(ref b);
                //결제 금액
                object c = txt_결제금액;
                axDP_Certification_Ocx.set_Set_Gold(ref c);
                //봉사료 금액
                object d = 0;
                axDP_Certification_Ocx.set_Set_ServiceGold(ref d);
                //부가세
                object k = txt_결제부가세;
                axDP_Certification_Ocx.set_Set_VAT(ref k);


                //할부
                object f = 00;
                axDP_Certification_Ocx.set_Set_SaleMonth(ref f);
                //원거래일자
                object g = txt_원거래일;
                axDP_Certification_Ocx.set_Set_WonYYMMDD(ref g);
                //원거래 승인번호
                object h = txt_원승인번호;
                axDP_Certification_Ocx.set_Set_WonGrantNo(ref h);

                string h11 = null;
                axDP_Certification_Ocx.set_set_Transer_Number(ref h11);

                //사용자 고유코드
                //object i = txt_사용자코드.Text.Trim();
                //axDP_Certification_Ocx.set_Set_SetUserCode(ref i);

                //**********************************
                //*             필독               *
                //**********************************
                //PG 사용 시 PG정보
                //모든 필드는 Byte 단위(한글은 2 byte)
                //사용하지 않는 필드는 해당 크기만큼 Space 처리
                //**********************************
                //TID = 40 Byte
                //구매자 = 30 Byte
                //전화 = 20 Byte
                //이메일 = 40 Byte
                //HP = 20 Byte
                //상품명 = 50 Byte
                //주소 = 100 Byte
                //수취인 메세지 = 50 Byte
                //*********************************

                // m전문일련번호 = 취소 시만 사용
                //PG 사용 시 PG정보
                ////BL_TID 
                //string h1 = getWordByByte(PgBL_TID.Text + space_chk.PadLeft(40, ' '), 40);
                //dream.set_set_Pg_BL_TID(ref h1);

                ////PgName 
                //string l = getWordByByte(PgName.Text + space_chk.PadLeft(30, ' '), 30);
                //dream.set_set_Pg_Name(ref l);

                ////PgTel 
                //string m = getWordByByte(PgTel.Text + space_chk.PadLeft(20, ' '), 20);
                //dream.set_set_Pg_Tel(ref m);

                ////PgEmail 
                //string n = getWordByByte(PgEmail.Text + space_chk.PadLeft(40, ' '), 40);
                //dream.set_set_Pg_Email(ref n);

                ////PgHP 
                //string o = getWordByByte(PgHP.Text + space_chk.PadLeft(20, ' '), 20);
                //dream.set_set_Pg_HP(ref o);

                ////PgProduct 
                //string p = getWordByByte(PgProduct.Text + space_chk.PadLeft(50, ' '), 50);
                //dream.set_set_Pg_Product(ref p);

                ////PgAddress 
                //string q = getWordByByte(PgAddress.Text + space_chk.PadLeft(100, ' '), 100);
                //dream.set_set_Pg_Address(ref q);

                ////PgRecvMessage 
                //string r = getWordByByte(PgRecvMessage.Text + space_chk.PadLeft(50, ' '), 50);
                //dream.set_set_Pg_RecvMessage(ref r);

                //전문일련번호 
                string r1 = "";
                axDP_Certification_Ocx.set_set_Transer_Number(ref r1);
                string r2 = "";
                axDP_Certification_Ocx.set_set_Mutil_CATID(ref r2);

                object j = "OCX";
                axDP_Certification_Ocx.set_Set_OcxProcess(ref j);

                axDP_Certification_Ocx.SendRecv();
            }
            catch (Exception ex)
            {
            }
        }

        public override void CardPaymentCancelNoCard()
        {
            try
            {
                //rich_logTxt1.Text = "";
                string txtJunmun = "";
                //string space_chk = "";
                //if (txt_SaleGold.Text.Trim() == "") { throw new Exception("결재금액을 입력해 주십시오"); }
                //if (txt_봉사료.Text.Trim() == "") { txt_봉사료.Text = "0"; }
                //if (txt_xHalbu.Text.Trim() == "") { txt_xHalbu.Text = "0"; }

                txtJunmun = "" + (char)7 + (char)6 + "1000" + fs + "9900" + fs + txt_결제금액 + fs + txt_결제부가세 + fs + "0" +
                            fs + "0" + fs + "" + fs + "" + fs + "" + fs + txt_원거래일 + fs + txt_원승인번호 +
                            fs + "" + fs + "" + fs + txt_m전문일련번호 + fs + "" + fs + "" + (char)6 + CR;

                //rich_logTxt1.Text = txtJunmun;
                axDP_Certification_Ocx.set_OcxHostSendJunMun(ref txtJunmun);

                axDP_Certification_Ocx.SendRecv();
            }
            catch (Exception ex)
            {
            }
        }
    }

    public enum EEVCATMsgID
    {
        ReaderEventStep,
        ReaderResponseStep,
        FallBack,
        RederResponse,
        AllredyRequest,
        PaySuccess,
        Error,
        TimeOut,
        CardIn,
        NoCard,
        CardInError
    };

    public enum ECreditCardMsgID
    {
        Unknown,
        EVCAT
    };

    public class CreditCardController_Message
    {
        public const byte STX = 0x02;
        public const byte ETX = 0x03;
        public const byte Seperator = (byte)'#';
        public const byte TextSign = (byte)'T';
        public const byte BinarySign = (byte)'B';
        
        public EEVCATMsgID Step { get; set; }
        public EEVCATMsgID ReturnMsg { get; set; }
        public string ReturnData { get; set; }

        public string txt_응답코드;
        public string txt_결제구분;
        public string txt_결제매체;
        public string txt_원거래일;
        public string txt_결제일시;
        public string txt_응답카드번호;
        public string txt_카드번호;
        public string txt_원승인번호;
        public string txt_응답승인번호;
        public string txt_가맹점번호;
        public string txt_매입구분;
        public string txt_발급사코드;
        public string txt_발급사명;
        public string txt_매입사코드;
        public string txt_매입사명;
        public string txt_응답내용;
        public string txt_선불카드잔액;
        public string txt_티머니자료;
        public string txt_요청사용자키;
        public string txt_결제금액;
        public string txt_결제부가세;

        public string txt_결제posid;
        public string txt_전문일련번호;
        public string txt_m전문일련번호;
        public string txt_Pg거래번호;

        public char CharSTX
        {
            get
            {
                return Convert.ToChar(STX);
            }
        }
        public char CharETX
        {
            get
            {
                return Convert.ToChar(ETX);
            }
        }
        public char CharTextSign
        {
            get
            {
                return Convert.ToChar(TextSign);
            }
        }

        public static ECreditCardMsgID ParseMsgID(string messageBody)
        {
            ECreditCardMsgID msgID = ECreditCardMsgID.Unknown;

            int idx = messageBody.IndexOf(Convert.ToChar(Seperator));
            if (idx > 0)
            {
                msgID = StringToMsgID(messageBody.Substring(0, idx));
            }
            else
            {
                msgID = StringToMsgID(messageBody);
            }

            return msgID;
        }

        public static ECreditCardMsgID ParseMsgID(byte[] messageBody)
        {
            ECreditCardMsgID msgID = ECreditCardMsgID.Unknown;

            int idx = Array.IndexOf(messageBody, Seperator);
            if (idx > 0)
            {
                msgID = StringToMsgID(Encoding.Default.GetString(messageBody, 0, idx));
            }
            else
            {
                msgID = StringToMsgID(Encoding.Default.GetString(messageBody));
            }

            return msgID;
        }

        private static ECreditCardMsgID StringToMsgID(string strID)
        {
            ECreditCardMsgID msgID = ECreditCardMsgID.Unknown;

            if (Enum.IsDefined(typeof(ECreditCardMsgID), strID))
            {
                msgID = (ECreditCardMsgID)Enum.Parse(typeof(ECreditCardMsgID), strID);
            }

            return msgID;
        }

        public byte[] Pack()
        {
            string msgBody = string.Format("{0}#{1}#{2}", ECreditCardMsgID.EVCAT, this.Step, this.ReturnMsg, this.ReturnData);

            int bodyLen = Encoding.Default.GetBytes(msgBody).Length;

            string message = string.Format("{0}{1}{2:D8}#{3}{4}", CharSTX, CharTextSign, bodyLen, msgBody, CharETX);
            return Encoding.Default.GetBytes(message);
        }

        public void UnPack(string messageBody)
        {
            ECreditCardMsgID msgID = ParseMsgID(messageBody);
            if (msgID != ECreditCardMsgID.EVCAT)
            {
                throw new Exception("메시지 ID가 일치하지 않음.");
            }

            string[] tokens = messageBody.Split(new char[1] { '#' });
            if (tokens.Length != 4)
            {
                throw new Exception("토큰의 갯수가 일치하지 않음.");
            }

            try
            {
                this.Step = (tokens[1] == "0" ? EEVCATMsgID.ReaderEventStep : EEVCATMsgID.ReaderResponseStep);
                if (tokens[3] == "2") this.ReturnMsg = EEVCATMsgID.FallBack;                
                else if (tokens[3] == "3") this.ReturnMsg = EEVCATMsgID.RederResponse;
                else if (tokens[3] == "4") this.ReturnMsg = EEVCATMsgID.AllredyRequest;
                else if (tokens[3] == "5") this.ReturnMsg = EEVCATMsgID.PaySuccess;
                else if (tokens[3] == "6") this.ReturnMsg = EEVCATMsgID.Error;
                else if (tokens[3] == "7") this.ReturnMsg = EEVCATMsgID.TimeOut;
                else if (tokens[3] == "8") this.ReturnMsg = EEVCATMsgID.CardIn;
                else if (tokens[3] == "9") this.ReturnMsg = EEVCATMsgID.NoCard;
                else if (tokens[3] == "10") this.ReturnMsg = EEVCATMsgID.CardInError;
                this.ReturnData = tokens[3];
            }
            catch (Exception)
            {
                throw new Exception("메시지 형식이 일치하지 않습니다.");
            }
        }
    }

    public class CreditCardController_EventArgs : System.EventArgs
    {
        public string messageBody;
        public CreditCardController_Message message;

        public CreditCardController_EventArgs(string message)
        {
            this.messageBody = message;
        }

        public CreditCardController_EventArgs(CreditCardController_Message message)
        {
            this.message = message;
        }
    }
}
