﻿namespace SognoLib.Meca.CreditCard
{
    partial class DreamposControl
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DreamposControl));
            this.axDP_Certification_Ocx = new AxDreampos_Ocx.AxDP_Certification_Ocx();
            ((System.ComponentModel.ISupportInitialize)(this.axDP_Certification_Ocx)).BeginInit();
            this.SuspendLayout();
            // 
            // axDP_Certification_Ocx
            // 
            this.axDP_Certification_Ocx.Enabled = true;
            this.axDP_Certification_Ocx.Location = new System.Drawing.Point(8, 8);
            this.axDP_Certification_Ocx.Name = "axDP_Certification_Ocx";
            this.axDP_Certification_Ocx.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axDP_Certification_Ocx.OcxState")));
            this.axDP_Certification_Ocx.Size = new System.Drawing.Size(109, 30);
            this.axDP_Certification_Ocx.TabIndex = 0;
            // 
            // DreamposControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.axDP_Certification_Ocx);
            this.Name = "DreamposControl";
            this.Size = new System.Drawing.Size(127, 47);
            ((System.ComponentModel.ISupportInitialize)(this.axDP_Certification_Ocx)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public AxDreampos_Ocx.AxDP_Certification_Ocx axDP_Certification_Ocx;
    }
}
