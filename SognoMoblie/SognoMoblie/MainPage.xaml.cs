﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

using System.Threading.Tasks;
using Xamarin.Forms;

//for sognolib
using System.Threading;
//using SognoLib.Core;
using System.Runtime.InteropServices;
using System.Net;
using System.IO;
using System.Timers;
using Plugin.Media;

namespace SognoMoblie
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {


        /*
        //private string dbip = "127.0.0.1"; // 통합관제 DB 아이피
        //private SogNo_TcpClient tcpClient;
        //private System.Timers.Timer timer;
        //private DateTime lastDateTime;
        //private parkConnecter param;

        public const byte STX = 0x02;
        public const byte ETX = 0x03;

        public char CharSTX
        {
            get
            {
                return Convert.ToChar(STX);
            }
        }
        public char CharETX
        {
            get
            {
                return Convert.ToChar(ETX);
            }
        }

        */
        public MainPage()
        {
            InitializeComponent();
            Plugin.Media.CrossMedia.Current.Initialize();
            


            hybridWebView.RegisterAction(data => selectFunction(data));
            //SognoSocketInit();
        }

        public void selectFunction(string data)
        {
//            DisplayAlert("Alert", "Phone : " + data, "OK");
            string[] dataSplit = data.Split('#');

            switch (dataSplit[0])
            {
                // protocol :"location#latitude#longitude#(변동)
                // (변동) 의 내용은 안드로이드일때 프로바이더, ios일때는 높이값이 옵니다
                case "location":
                    SetUserLocation(data);
                    break;

                // protocol : SognoSocket#요청내용#값1#값2...
                // 소켓 통신을 위한 영역입니다.
                case "SognoSocket":

                    break;

                // 라즈베리파이로 메시지 던져서 통신하는 부분
                /*case "SendingRPI":
                    if (param.isConnect)
                    {
                        string msg = CharSTX + dataSplit[1] + CharETX;
                        this.hybridWebView.EvaluateJavascript($"OnReceiveEvent('Rpi에 메시지 전송 : {msg}');");

                        this.tcpClient.Send(msg);
                    }
                    else
                    {
                        if(dataSplit[1] == "ConnectStart") SognoSocketClientStart();
                        else this.hybridWebView.EvaluateJavascript($"OnReceiveEvent('Rpi Sending Fail : 현재 소켓 연결되지 않음');");
                    }
                    break;
                    */

                default:
                    DisplayAlert("Alert", "처리할 메소드가 없는 요청입니다. 요청내용 : " + data, "OK");
                    break;
            }
            
        }

        public async Task GetUserLocation(string location)
        {
            var result = await this.hybridWebView.EvaluateJavascript($"setMarker({location});");

        }

        public void SetUserLocation(string location)
        {
            this.hybridWebView.EvaluateJavascript($"setMarker('{location}');");
        }


        ///
        /// Using SognoLib
        ///
        /*
        class parkConnecter
        {
            public SogNo_TcpClient tcp { get; set; }
            public string ip { get; set; }
            public int port { get; set; }
            public bool isConnect { get; set; }
        }


        public bool isSognoDBOK()
        {
            try
            {
                //this.mainDBAccessor = new SogNo_DBAccessor(Select_DBInfo.DBIP, Select_DBInfo.DBPort, Select_DBInfo.DBName);
                //this.parkRecords = mainDBAccessor.SelectParkInfo(); // 전체 주차장 리스트

                return true;
            }
            catch (MySql.Data.MySqlClient.MySqlException ex)
            {
                Console.WriteLine(ex + "데이터베이스 연결에 실패하였습니다.");

                return false;
            }
        }

        public void SognoSocketInit()
        {
            this.tcpClient = new SogNo_TcpClient("MobileApp");
            this.tcpClient.ConnectEvent += OnConnectEvent;
            this.tcpClient.DisconnectEvent += OnCloseEvent;
            this.tcpClient.ReceiveEvent += OnReceiveEvent;
            this.tcpClient.ReceiveBytesEvent += OnReceiveSCREvent;

            this.param = new parkConnecter();

            this.param.tcp = this.tcpClient;
            this.param.ip = "59.1.39.122";
            this.param.port = 7080;

            this.lastDateTime = DateTime.Now;
            this.timer = new System.Timers.Timer(5000);
        }


        public void SognoSocketClientStart()
        {
            this.tcpClient.Connect(param.ip, param.port, "", 0, true);
            this.timer.Elapsed += CheckSocketConntect;
            this.timer.Enabled = true;
        }

        private void OnConnectEvent(string ip, int port)
        {
            this.hybridWebView.EvaluateJavascript($"OnConnectEvent('{ip}','{port.ToString()}');");
            this.lastDateTime = DateTime.Now;
            param.isConnect = true;
        }

        private void OnCloseEvent()
        {
            this.hybridWebView.EvaluateJavascript($"OnCloseEvent();");
            this.lastDateTime = DateTime.Now;
            param.isConnect = false;
        }

        private void OnReceiveEvent(string msg)
        {
            this.hybridWebView.EvaluateJavascript($"OnReceiveEvent('{msg}');");
            this.lastDateTime = DateTime.Now;
        }
        private void OnReceiveSCREvent(byte[] theMessage)
        {
            this.hybridWebView.EvaluateJavascript($"OnReceiveSCREvent('{theMessage}');");
            this.lastDateTime = DateTime.Now;
        }

        private void CheckSocketConntect(Object source, ElapsedEventArgs e)
        {
            if (this.lastDateTime.AddSeconds(60) < DateTime.Now)
            {
                this.tcpClient.Close();
                this.tcpClient.Connect(param.ip, param.port, "", 0, true);
            }
        }
        */
    }
}
