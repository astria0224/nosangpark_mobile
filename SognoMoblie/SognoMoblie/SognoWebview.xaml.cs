﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SognoMoblie
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SognoWebview : ContentPage
    {
        public SognoWebview()
        {
            InitializeComponent();
        }
    }
}