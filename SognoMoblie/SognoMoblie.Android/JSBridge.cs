﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Android.Webkit;
using Java.Interop;

using Android.Content.PM;
using Android.Gms.Common;
using Android.Gms.Location;



namespace SognoMoblie.Droid
{
    public class JSBridge : Java.Lang.Object
    {
        static readonly int RC_LAST_LOCATION_PERMISSION_CHECK = 1000;

        readonly WeakReference<HybridWebViewRenderer> hybridWebViewRenderer;
        FusedLocationProviderClient fusedLocationProviderClient;

        public JSBridge(HybridWebViewRenderer hybridRenderer)
        {
            hybridWebViewRenderer = new WeakReference<HybridWebViewRenderer>(hybridRenderer);
            fusedLocationProviderClient = LocationServices.GetFusedLocationProviderClient(MainActivity.Instance);
        }

        [JavascriptInterface]
        [Export("invokeAction")]
        public void InvokeAction(string data)
        {
            HybridWebViewRenderer hybridRenderer;
            if (hybridWebViewRenderer != null && hybridWebViewRenderer.TryGetTarget(out hybridRenderer))
            {
                if (data == "GivemeUserLocation")
                {
                    string location = "확인할 수 없음";
                    MainActivity.Instance.RequestLocationPermission(RC_LAST_LOCATION_PERMISSION_CHECK);
                    if (MainActivity.Instance.location != null) location = MainActivity.Instance.location;
                    //((HybridWebView)hybridRenderer.Element).UserLocation = location;
                    ((HybridWebView)hybridRenderer.Element).InvokeAction(location);
                }
                else
                {
                    ((HybridWebView)hybridRenderer.Element).InvokeAction(data);
                }
            }
        }
    }
}