﻿using System;
using System.Threading.Tasks;

using Android;
using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;

using Android.OS;
using Android.Gms.Common;
using Android.Gms.Location;
using Android.Util;
using Android.Support.Design.Widget;
using Android.Widget;

using Android.Support.V4.App;
using Xamarin.Essentials;

namespace SognoMoblie.Droid
{
    [Activity(Label = "SognoMoblie", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]

    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {

        public static MainActivity Instance;

        public string location;
        const long ONE_MINUTE = 60 * 1000;
        const long FIVE_MINUTES = 5 * ONE_MINUTE;
        const long TWO_MINUTES = 2 * ONE_MINUTE;

        static readonly int RC_LAST_LOCATION_PERMISSION_CHECK = 1000;
        static readonly int RC_LOCATION_UPDATES_PERMISSION_CHECK = 1100;
        public static readonly int REQUEST_CAMERA_PERMISSION = 0;

        bool isRequestingLocationUpdates;

        LocationCallback locationCallback;
        LocationRequest locationRequest;
        FusedLocationProviderClient fusedLocationProviderClient;

        View rootLayout;


        protected override void OnCreate(Bundle savedInstanceState)
        {
            Log.Debug("FusedLocationProviderSample", "OnCreate On");

            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            Instance = this;

           // RequestLocationPermission(RC_LAST_LOCATION_PERMISSION_CHECK);
           // RequestLocationPermission(RC_LOCATION_UPDATES_PERMISSION_CHECK);
            

            LoadApplication(new App());
            RequestCameraPermission(REQUEST_CAMERA_PERMISSION);


            // 이거 구글플레이 테스트해보는 구문인데 함 테스트 해봐야됨 아직 안했음 to.과거의나            
            if (!IsGooglePlayServicesInstalled())
            {
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.SetTitle("경고");
                alert.SetMessage("구글 플레이 서비스를 업데이트하거나, 설치해주세요");
                Dialog dialog = alert.Create();
                dialog.Show();

                System.Diagnostics.Process.GetCurrentProcess().CloseMainWindow();
            }
            else
            {
                //구글 플레이가 설치되어 있으면 location request 구문 실행
                locationRequest = new LocationRequest()
                                  .SetPriority(LocationRequest.PriorityHighAccuracy)
                                  .SetInterval(FIVE_MINUTES)
                                  .SetFastestInterval(TWO_MINUTES);
                locationCallback = new FusedLocationProviderCallback(this);

                fusedLocationProviderClient = LocationServices.GetFusedLocationProviderClient(this);

            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Log.Debug("FusedLocationProviderSample", "OnRequestPermissionsResult On");
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            // 이거 구글플레이 테스트해보는 구문인데 함 테스트 해봐야됨 아직 안했음 to.과거의나            
            if (!IsGooglePlayServicesInstalled())
            {
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.SetTitle("경고");
                alert.SetMessage("구글 플레이 서비스를 업데이트하거나, 설치해주세요");
                Dialog dialog = alert.Create();
                dialog.Show();

                System.Diagnostics.Process.GetCurrentProcess().CloseMainWindow();

                
            }
            OnRequestPermisstionResultPlus(requestCode, permissions, grantResults);
        }

        public async void OnRequestPermisstionResultPlus(int requestCode, string[] permissions, Permission[] grantResults)
        {
            if (requestCode == RC_LAST_LOCATION_PERMISSION_CHECK || requestCode == RC_LOCATION_UPDATES_PERMISSION_CHECK)
            {
                if (grantResults.Length == 1 && grantResults[0] == Permission.Granted)
                {
                    if (requestCode == RC_LAST_LOCATION_PERMISSION_CHECK)
                    {
                        await GetLastLocationFromDevice();
                    }
                    else
                    {
                        await StartRequestingLocationUpdates();
                        isRequestingLocationUpdates = true;
                    }
                }
                else
                {
                }
            }
            else
            {
                Log.Debug("FusedLocationProviderSample", "Don't know how to handle requestCode " + requestCode);
            }

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }



        /// <summary>
        /// 구글 플레이 서비스가 깔려있는지 확인하는 코드입니다.
        /// </summary>
        /// <returns>bool</returns>
        bool IsGooglePlayServicesInstalled()
        {
            var queryResult = GoogleApiAvailability.Instance.IsGooglePlayServicesAvailable(this);
            if (queryResult == ConnectionResult.Success)
            {
                //Log.Info("MainActivity", "Google Play Services is installed on this device.");
                return true;
            }

            if (GoogleApiAvailability.Instance.IsUserResolvableError(queryResult))
            {
                // Check if there is a way the user can resolve the issue
                var errorString = GoogleApiAvailability.Instance.GetErrorString(queryResult);
                //Log.Error("MainActivity", "There is a problem with Google Play Services on this device: {0} - {1}", queryResult, errorString);

                // Alternately, display the error to the user.
            }
            return false;
        }

        public override void OnBackPressed()
        {
            Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this);

            builder.SetPositiveButton("확인", (senderAlert, args) => {
                Finish();
            });

            builder.SetNegativeButton("취소", (senderAlert, args) => {
                return;
            });

            Android.App.AlertDialog alterDialog = builder.Create();
            alterDialog.SetTitle("알림");
            alterDialog.SetMessage("프로그램을 종료 하시겠습니까?");
            alterDialog.Show();
        }

        protected override async void OnResume()
        {
            base.OnResume();
            if (CheckSelfPermission(Manifest.Permission.AccessFineLocation) == Permission.Granted)
            {
                if (isRequestingLocationUpdates)
                {
                    await StartRequestingLocationUpdates();
                }
            }
            else
            {
                //Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this);
                //Android.App.AlertDialog alterDialog = builder.Create();
                //alterDialog.SetTitle("알림");
                //alterDialog.SetMessage("위치 사용 권한 미허용 시 어플리케이션 사용이 불가능합니다.");
                //alterDialog.Show();

                RequestLocationPermission(RC_LAST_LOCATION_PERMISSION_CHECK);

            }
        }

        protected override void OnPause()
        {
            StopRequestLocationUpdates();
            base.OnPause();
        }

        async void StopRequestLocationUpdates()
        {

            if (isRequestingLocationUpdates)
            {
                await fusedLocationProviderClient.RemoveLocationUpdatesAsync(locationCallback);
            }
        }

        public async Task GetLastLocationFromDevice()
        {
            var location = await fusedLocationProviderClient.GetLastLocationAsync();

            if (location == null)
            {

            }
            else
            {
                string latitude = location.Latitude.ToString();
                string longitude = location.Longitude.ToString();
                string provider = location.Provider.ToString();

                this.location = "location#" + latitude + "#" + longitude + "#" + provider;                
            }
        }

        async Task StartRequestingLocationUpdates()
        {
            Log.Debug("FusedLocationProviderSample", "StartRequestingLocationUpdates On");
            await fusedLocationProviderClient.RequestLocationUpdatesAsync(locationRequest, locationCallback);
        }

        public void RequestLocationPermission(int requestCode)
        {
            if (ActivityCompat.ShouldShowRequestPermissionRationale(this, Manifest.Permission.AccessFineLocation))
            {
                Snackbar.Make(this.rootLayout, "permission_location_rationale", Snackbar.LengthIndefinite)
                        .SetAction("ok",
                                   delegate
                                   {
                                       ActivityCompat.RequestPermissions(this, new[] { Manifest.Permission.AccessFineLocation }, requestCode);

                                   })
                        .Show();
            }
            else
            {
                ActivityCompat.RequestPermissions(this, new[] { Manifest.Permission.AccessFineLocation }, requestCode);
            }
        }

        public void RequestFileSavePermission(int requestCode)
        {
            if (ActivityCompat.ShouldShowRequestPermissionRationale(this, Manifest.Permission.ReadExternalStorage))
            {
                Snackbar.Make(this.rootLayout, "readstoreage", Snackbar.LengthIndefinite)
                        .SetAction("ok",
                                   delegate
                                   {
                                       ActivityCompat.RequestPermissions(this, new[] { Manifest.Permission.ReadExternalStorage }, requestCode);

                                   })
                        .Show();
            }
            else
            {
                ActivityCompat.RequestPermissions(this, new[] { Manifest.Permission.ReadExternalStorage }, requestCode);
            }
            if (ActivityCompat.ShouldShowRequestPermissionRationale(this, Manifest.Permission.WriteExternalStorage))
            {
                Snackbar.Make(this.rootLayout, "writestoreage", Snackbar.LengthIndefinite)
                        .SetAction("ok",
                                   delegate
                                   {
                                       ActivityCompat.RequestPermissions(this, new[] { Manifest.Permission.WriteExternalStorage }, requestCode);
                                   })
                        .Show();
            }
            else
            {
                ActivityCompat.RequestPermissions(this, new[] { Manifest.Permission.WriteExternalStorage }, requestCode);
            }
        }

        public async void RequestCameraPermission(int requestCode)
        {

            try
            {
                var status = await Permissions.CheckStatusAsync<Permissions.Camera>();
                var status1 = await Permissions.CheckStatusAsync<Permissions.Media>();
                var status2 = await Permissions.CheckStatusAsync<Permissions.Microphone>();
                var status3 = await Permissions.CheckStatusAsync<Permissions.Photos>();
            }
           
            catch(PermissionException)
            {
                try
                {
                    var status = await Permissions.RequestAsync<Permissions.Camera>();
                    var status1 = await Permissions.RequestAsync<Permissions.Media>();
                    var status2 = await Permissions.RequestAsync<Permissions.Microphone>();
                    var status3 = await Permissions.RequestAsync<Permissions.Photos>();
                }
                catch (PermissionException)
                {
                    Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this);
                    Android.App.AlertDialog alterDialog = builder.Create();
                    alterDialog.SetTitle("알림");
                    alterDialog.SetMessage("권한 미허용시 어플리케이션 이용이 불가능합니다.");
                    alterDialog.Show();
                }
            }
            
            if (ActivityCompat.ShouldShowRequestPermissionRationale(this, Manifest.Permission.Camera))
            {
                Snackbar.Make(this.rootLayout, "camera Request", Snackbar.LengthIndefinite)
                        .SetAction("ok",
                                   delegate
                                   {
                                       ActivityCompat.RequestPermissions(this, new[] { Manifest.Permission.Camera }, requestCode);

                                   })
                        .Show();
            }
            else
            {
                ActivityCompat.RequestPermissions(this, new[] { Manifest.Permission.Camera }, requestCode);
            }


            if (ActivityCompat.ShouldShowRequestPermissionRationale(this, Manifest.Permission.MediaContentControl))
            {
                Snackbar.Make(this.rootLayout, "camera Request", Snackbar.LengthIndefinite)
                        .SetAction("ok",
                                   delegate
                                   {
                                       ActivityCompat.RequestPermissions(this, new[] { Manifest.Permission.MediaContentControl }, 1);

                                   })
                        .Show();
            }
            else
            {
                ActivityCompat.RequestPermissions(this, new[] { Manifest.Permission.MediaContentControl }, 1);
            }


            if (ActivityCompat.ShouldShowRequestPermissionRationale(this, Manifest.Permission.CaptureAudioOutput))
            {
                Snackbar.Make(this.rootLayout, "camera Request", Snackbar.LengthIndefinite)
                        .SetAction("ok",
                                   delegate
                                   {
                                       ActivityCompat.RequestPermissions(this, new[] { Manifest.Permission.CaptureAudioOutput }, 2);

                                   })
                        .Show();
            }
            else
            {
                ActivityCompat.RequestPermissions(this, new[] { Manifest.Permission.CaptureAudioOutput }, 2);
            }


            if (ActivityCompat.ShouldShowRequestPermissionRationale(this, Manifest.Permission.CaptureVideoOutput))
            {
                Snackbar.Make(this.rootLayout, "camera Request", Snackbar.LengthIndefinite)
                        .SetAction("ok",
                                   delegate
                                   {
                                       ActivityCompat.RequestPermissions(this, new[] { Manifest.Permission.CaptureVideoOutput }, 3);

                                   })
                        .Show();
            }
            else
            {
                ActivityCompat.RequestPermissions(this, new[] { Manifest.Permission.CaptureVideoOutput }, 3);
            }
        }


    }

}