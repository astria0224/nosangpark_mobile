﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SognoMoblie;
using SognoMoblie.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

using System.Threading;
using System.Threading.Tasks;
using Android.Webkit;
using Android.Annotation;

[assembly: ExportRenderer(typeof(HybridWebView), typeof(HybridWebViewRenderer))]
namespace SognoMoblie.Droid
{
    public class HybridWebViewRenderer : WebViewRenderer
    {
        const string JavascriptFunction = "function invokeCSharpAction(data){jsBridge.invokeAction(data);} ";
        Context _context;
        Activity mContext;

        public HybridWebViewRenderer(Context context) : base(context)
        {
            _context = context;
            mContext = context as Activity;
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.WebView> e)
        {
            base.OnElementChanged(e);
           

            if (e.OldElement != null)
            {
                Control.RemoveJavascriptInterface("jsBridge");
                ((HybridWebView)Element).Cleanup();
            }

            if (e.NewElement != null)
            {
                
                Control.Settings.SetPluginState(WebSettings.PluginState.On);
                Control.SetWebViewClient(new JavascriptWebViewClient($"javascript: {JavascriptFunction}"));
                Control.Settings.JavaScriptEnabled = true;
                Control.Settings.DomStorageEnabled = true;
                Control.Settings.JavaScriptCanOpenWindowsAutomatically = true;
                Control.Settings.DatabaseEnabled = true;
                Control.Settings.AllowFileAccessFromFileURLs = true;
                Control.Settings.AllowUniversalAccessFromFileURLs = true;
                Control.Settings.MediaPlaybackRequiresUserGesture = false;
                Control.Settings.AllowContentAccess = true;
                Control.Settings.AllowFileAccess = true;
                Control.Settings.AllowUniversalAccessFromFileURLs = true;
                Control.ClearCache(true);
                Control.SetWebChromeClient(new CustomChromeClient(mContext));
                Control.AddJavascriptInterface(new JSBridge(this), "jsBridge");
                Control.LoadUrl($"{((HybridWebView)Element).Uri}");
                
                ((HybridWebView)Element).EvaluateJavascript = async (js) =>
                {
                    var reset = new ManualResetEvent(false);
                    var response = string.Empty;
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        Control?.EvaluateJavascript(js, new JavascriptCallback((r) => { response = r; reset.Set(); }));

                    });
                    await Task.Run(() => { reset.WaitOne(); });
                    return response;
                };
            }
        }

        public class CustomChromeClient : WebChromeClient
        {
            Activity mContext;
            public CustomChromeClient(Activity context)
            {
                this.mContext = context;
            }

            public override void OnPermissionRequest(PermissionRequest request)
            {
                request.Grant(request.GetResources());
            }

        }
        




        internal class JavascriptCallback : Java.Lang.Object, IValueCallback
        {
            public JavascriptCallback(Action<string> callback)
            {
                _callback = callback;
            }

            private Action<string> _callback;
            public void OnReceiveValue(Java.Lang.Object value)
            {
                _callback?.Invoke(Convert.ToString(value));
            }
        }

    }


}